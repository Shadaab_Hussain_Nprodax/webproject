from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.pmpr.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
import csv
import os
import io
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
from pyrfc import *
from zenapp.modules.leave.lfunctions import *
from operator import itemgetter, attrgetter
import logging
import json
import subprocess
import decimal
import itertools
import time
from datetime import timedelta
from random import randint
from werkzeug import secure_filename
import pyexcel as pe
from pyexcel_xls import get_data
zen_pmpr = Blueprint('zenpmpr', __name__, url_prefix='/pmpr')

################################### New Developments ################################

def date_time_datetime(strdate,strtime):
	new_dt=strdate+' '+strtime
	result_datetime=datetime.datetime.strptime(new_dt,'%d-%m-%Y %H:%M:%S')
	return result_datetime

# debug profile for diesel consumption
@zen_pmpr.route('/diesel_consumption/', methods = ['GET', 'POST'])
def diesel_consumption():
	debug_flag=find_one_in_collection('miv_config',{'status':'debug'})
	if debug_flag:
		debug_flag=debug_flag['debug']['diesel_consumption']
	else:
		debug_flag="off"
	try:
		user_details=base()
		user_plant=user_details["plant_id"]
		dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
		dept = sorted(dept, key=itemgetter('dept'))
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})			
		workcenter=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
		work_center = sorted(workcenter, key=itemgetter('work_center_desc'))
		if request.method=='POST':
			form=request.form			
			error_count=0
			if form['submit']=='Upload':
				total_num_equipments=0
				files=request.files['fileupload']
				filename = secure_filename(files.filename)
				filename='/home/administrator/PCIL_MYPORTAL_PRODUCTION/PCIL_myPortal/zenapp/static/Diesel_consumption/'+filename
				files.save(filename)
				logging.info('Uploading Diesel Consumption Excel File '+filename+' By Employee : '+current_user.username +' Of '+user_plant+' ------ '+str(form))
				data = get_data(filename)
				field_data=data #field values
				fields=field_data['Sheet1'][0] #fields
				field1=fields[0].lower()
				field2=fields[1].lower()
				if field1 =='equipment_number' and field2 =='quantity':
					result=[]
					equip_error='No Data Available with equipment numbers : '
					equip_error_end='Please Check and Re-Apply.'
					for i in range(1,len(field_data['Sheet1'])):
						total_num_equipments=i
						try:
							equip_number=str(field_data['Sheet1'][i][0])
						except:
							equip_number=''
						try:
							quantity=field_data['Sheet1'][i][1]
						except:
							quantity=''
						equip_details=find_one_in_collection('equipmentmaster',{'equipment_number':'000000'+equip_number})
						material_details=find_one_in_collection('materialmaster',{'material':form['material']})
						if equip_details:
							equip_desc=equip_details['Desp_technical_object']
							def_cs=equip_details['cost_center']
							def_cs_desc=equip_details['Desc']
							result.append({field1:equip_number,'equip_desc':equip_desc,field2:quantity,'def_costcenter':def_cs,'def_costcenter_desc':def_cs_desc})
						else:
							error_count+=1
							if i+1 !=len(field_data['Sheet1']):
								equip_error=equip_error+equip_number+' ,'
							else:
								equip_error=equip_error+equip_number+'.'
					if error_count!=0:		
						flash(equip_error+equip_error_end,'alert-danger')
					else:
						pass
					if debug_flag=='on':
						logging.info('Diesel Consumption Excel Upload Sucessful -------- '+str(datetime.datetime.now())) 
					return render_template('diesel_consumption.html',user_details=user_details,result=result,num_equips=total_num_equipments,material=form['material'],quantity_av=form['quantity_av'],material_desc=material_details['material_desc'],uom=form['uom'],st_loc=form['st_loc'],matl_group=material_details['matl_group'],department_dup=form['dept'],wrk_center_dup=form['wrk_center'],title='Uploaded Equipments Details',user_plant=user_plant)
				else:
					logging.info('Diesel Consumption Excel Upload Field Name Error -------- '+str(datetime.datetime.now()))
					flash('Please Check The Field Name In Uploaded Excel is EQUIPMENT_NUMBER','alert-danger')
					return render_template('diesel_consumption.html',user_details=user_details,result=[],show=0,num_equips=0,work_center=work_center,dept=dept,user_plant=user_plant,title='Upload Equipments')
			else:
				return render_template('diesel_consumption.html',user_details=user_details,result=[],show=0,num_equips=0,work_center=work_center,dept=dept,user_plant=user_plant,title='Upload Equipments')
		return render_template('diesel_consumption.html',user_details=user_details,result=[],show=0,num_equips=0,work_center=work_center,dept=dept,user_plant=user_plant,title='Upload Equipments')
	except:
		return redirect(url_for('zenpmpr.diesel_consumption'))

@zen_pmpr.route('/miv_multi_equip_create/', methods = ['GET', 'POST'])
def miv_multi_equip_create():
	debug_flag=find_one_in_collection('miv_config',{'status':'debug'})
	if debug_flag:
		debug_flag=debug_flag['debug']['diesel_consumption_ajax']
	else:
		debug_flag="off"
	user_details=base()
	user_plant=user_details["plant_id"]
	msg_port_id=[]
	msg=''
	uid = current_user.username
	start_time=datetime.datetime.now()
	if request.method=='POST':
		form=request.form
		logging.info('Processing Diesel Consumption Ajax call From Employee '+uid+' Of Plant : '+user_plant+' ------- '+str(form)+' -- @ -- '+str(datetime.datetime.now()))
		orders=[]
		equip_duplicate=[]
		if form['submit_value']=='multi_applyajax':
			material_details=find_one_in_collection('materialmaster',{'material':form['material']})
			total_equips=form['equipment_count']
			for i in range(int(total_equips)):
				order_dict={}			
				if form['costcenter'+str(i)]=='exclude':
					pass
				else:
					if form['equipment'+str(i)]+form['costcenter'+str(i)] in equip_duplicate:
						pass
					else:
						order_dict={'equipment':form['equipment'+str(i)],'quantity':form['quantity'+str(i)],'material':form['material'],'uom':form['uom'],'mat_desc':material_details['material_desc'],'costcenter':form['costcenter'+str(i)],'st_loc':form['st_loc'],'matl_group':material_details['matl_group'],'work_center':form['wrk_center_dup'],'department':form['dept_dup'],'portalid':'portalid'}
						orders.append(order_dict)
						equip_duplicate.append(form['equipment'+str(i)]+form['costcenter'+str(i)])
			for order in orders:
				description=order['mat_desc'] #material description
				material=order['material']        #material number
				st_loc=order['st_loc']			#storage location
				uom=order['uom']					#unit of measure
				quantity=order['quantity'] 		#quantity
				dept_pid=order['department']
				workcenter=order['work_center']
				costcenter=order['costcenter']
				dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":dept_pid},{"dept_chief_pid":1,"req_desc":1,"pur_grp":1,"_id":0,"dept":1})	
				dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})			
				plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
				miv=[]
				miv1=[]
				result={}
				miv_permission=find_one_in_collection('miv_permission',{"plant":plant_pid[0]['plant_id1']})
				if miv_permission==None:
					data={}
					msg="Permission for your plant has not been setup."
					a=[{"check":"1","msg":msg}]
					data['report']=a
					return jsonify(data)

				array={}
				array1={}
				create_new=datetime.datetime.now()
				array={"PSTYP":"L","MATNR":material,"MATKL":material_details['matl_group'],"MEINS":uom,
				"MENGE":float(quantity),"EEIND":create_new,"LGORT":st_loc}
				array1={"PSTYP":"L","MAT_DESC":material_details['material_desc'],"MATNR":material,"MATKL":material_details['matl_group'],"MEINS":uom,"LGORT":st_loc,"MENGE":float(quantity),"EEIND":create_new}
				miv.append(array)
				miv1.append(array1)

				levels=[]
				dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
				plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
				dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})

				if "1" in miv_permission["level"]:
					levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
				if "2" in miv_permission["level"]:
					if "1" in miv_permission["level"]:
						levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})
					else:
						levels.append({"a_status":"current","a_id":plant_pid1[0]['username'],"a_time":"None"}) 
				if "3" in miv_permission["level"]:
					if "1" in miv_permission["level"] or "2" in miv_permission["level"]: 
						levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
					else:
						levels.append({"a_status":"current","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
				
				prname1=get_employee_name(current_user.username)
				prname=dept_chief_pid[0]['req_desc']
				port_id = int(datetime.datetime.now().strftime('%d%m%Y%H%M%S')) #portal id with date_time_sec will come from front end
				rand_num=random_with_N_digits(4)
				new_port_id=port_id + rand_num
				new_port_id=unicode(str(new_port_id),"utf-8") #final portal id
				P_BUDAT=datetime.datetime.now()
				stime_equip=datetime.datetime.now()
				# Equipment Header Inforamtion Sending through Dictionary
				IM_HEADER_EQUIP = {"PORTALID":new_port_id,"ACAT":"F","EQUNR":order['equipment'],"AUART":"PM04","STEUS":"PM01","ARBPL":workcenter,"EKGRP":dept_chief_pid[0]['pur_grp'],"WERKS":plant_pid[0]['plant_id1'],"PRNAME":prname,"BUDAT":P_BUDAT,"REJECT":""}
				
				sap_login=get_sap_user_credentials()
				try:
					stime_sap=datetime.datetime.now()
					logging.debug("MIV For Equipment Connecting to SAP Using Diesel Consumption")
					conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
					# conn = Connection(user='manasa', passwd='behappy', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
					result = conn.call('Z_MIV_CREATION',IM_HEADER=IM_HEADER_EQUIP,IT_ITEMS=miv)
					if debug_flag=='on':
						logging.info('Result From SAP Call --- '+str(result)+' -- @ -- '+str(datetime.datetime.now()))
					conn.close()
					logging.info("Connection Closed and Time taken for Fetching Result from SAP :"+" "+str(datetime.datetime.now()-stime_sap))
					close_sap_user_credentials(sap_login['_id'])
					
				except Exception,e:
					logging.debug("MIV creation error result:==%s",str(e))
					close_sap_user_credentials(sap_login['_id'])
					flash("Unable to connect Sap user Credential","alert-danger")

				costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":costcenter},{"cost_center_desc":1,"_id":0})
				cost_center_desc = costcenterdesc[0]['cost_center_desc']
				
				equip=find_and_filter('equipmentmaster',{"equipment_number":order['equipment']},{"functional_loc":1,"Desp_technical_object":1,"_id":0})
				for x in range(len(result['IT_ITEMS'])):
					# miv1[x]['MBLNR']=result['IT_ITEMS'][x]['MBLNR']
					miv1[x]['MJAHR']=result['IT_ITEMS'][x]['MJAHR']
					miv1[x]['ZEILE']=result['IT_ITEMS'][x]['ZEILE']
					# miv1[x]['DMBTR']=result['IT_PRCRT'][x]['DMBTR']
				
				mivarray={"P_Datetime":new_port_id,'diesel_consumption':'yes',"status":"submitted","functionlocation":"","equipment":order['equipment'],"costcenter":costcenter,"wbs_element":"","network":"","PR_dept":dept_pid,"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"voucher":miv1,"approval_levels":levels,"P_ACAT":"F","P_ARBPL":workcenter,"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"equip_funcloc":equip[0]["functional_loc"],"cost_center_desc":cost_center_desc,"eqp_desc":equip[0]["Desp_technical_object"]}

				mivarray['history']=[{"level1":quantity,"level2":"","level3":"","level4":""}]
				mivarray['P_BUDAT']=P_BUDAT
				save_collection("mivcreation",mivarray)
				if debug_flag=='on':
					logging.info('Saving Miv(Diesel Consumption) To Database --- '+str(mivarray)+' -- @ -- '+str(datetime.datetime.now()))	
				#if 3 Levels of Approval use 2740 msg1 else use 2741 msg1
				#msg="You have raised MIV and it is pending for approval at HOD of " +dept_chief_pid[0]['dept']+" Department. Document Reference Number is "+miv1[0]['MBLNR']
				msg_port_id.append(new_port_id)
				msg="You have raised MIV and it is pending for approval at STORES of " +dept_chief_pid[0]['dept']+" portal id is : "+str(new_port_id)	#notification msg
				msg_flash="You have raised MIV and it is pending for approval at STORES of " +dept_chief_pid[0]['dept']+" portal id is : "	#notification msg		
				#if 3 Levels of Approval use 2743 msg1 else use 2744 msg1
				#msg1=prname1+"has raised MIV and it is pending for your approval" 
				msg1=prname1+" has raised MIV and it is pending for"+ " "+dept_store_pid1[0]['username']+" "+"Approval With Portal Id "+str(new_port_id)
				notification(uid,msg)
				notification(dept_chief_pid1[0]['username'],msg1)
				logging.info("Total Execution Time for MIV Creation Using Diesel Consumption Is : "+str(datetime.datetime.now()-start_time))
			msg_str=''
			for p in range(len(msg_port_id)):
				if p+1!=len(msg_port_id):
					msg_str=msg_str+msg_port_id[p]+' , '
				else:
					msg_str=msg_str+msg_port_id[p]
			data={}
			a=[{"check":"0","msg":msg_str}]
			data['report']=a
		logging.info('End Of Diesel Consumption AJAX Function with Items '+str(p+int(1))+' -- @ -- ' +str(datetime.datetime.now()))
		flash(msg_flash+msg_str,'alert-success')
		return redirect(url_for('zenpmpr.diesel_consumption'))
# end of debug profile for diesel consumption

############
#get material & quantity for Diesel Consumption.
@zen_pmpr.route('/materialnew', methods = ['GET', 'POST'])
def materialnew():
	start_time=datetime.datetime.now()
	q = request.args.get('term')
	plant_id=request.args.get('plant_id')
	db=dbconnect()
	# plant_pid=find_and_filter('Plants',{"plant_id":plant_id},{"plant_id1":1,"_id":0})
	# mat = db.materialmaster.aggregate([
 #                {'$match' : {"plant_id":plant_pid[0]['plant_id1'],'$and':[{'$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
 #                {'$project' : {'material_desc':1,'material':1,'bun':1 ,"_id":0}}])
	# mat=list(mat)
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	mat = db.materialmaster.aggregate([
                {'$match' : { "plant_id":plant_pid[0]['plant_id1'],'$and':[{'$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
                {'$project' : {'material_desc':1,'material':1,'bun':1 ,"_id":0}}])
	mat=list(mat)
	return json.dumps({"results":mat})


@zen_pmpr.route('/quantitynew/', methods = ['GET', 'POST'])
def getquantitynew():
	user_details = base()	
	data={}
	form=request.form
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	sap_login=get_sap_user_credentials()
	if '_id' in sap_login:
		try:			
			conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
			result = conn.call('Z_MZMM01_MAT_QTY',I_MATNR=form['material'],I_WERKS=plant_pid[0]['plant_id1'])
			
			logging.debug("get quantity result:==%s",str(result))
			for x in range(0,len(result["IT_MARD"])):
				result["IT_MARD"][x]["LGORT"]=str(result["IT_MARD"][x]["LGORT"])
				result["IT_MARD"][x]["LABST"]=str(result["IT_MARD"][x]["LABST"])
				result["IT_MARD"][x]["TDLINE"]=str(result["IT_TLINE"][0]["TDLINE"])
				result["IT_MARD"][x]["EX_POVALUE"]=str(result['EX_POVALUE'])
				# result["IT_MARD"][x]["SOBKZ"]=str(result["IT_MARD"][x]["SOBKZ"])
			data['reports']=result['IT_MARD']
			conn.close()
			close_sap_user_credentials(sap_login['_id'])
		except Exception,e:
			close_sap_user_credentials(sap_login['_id'])
			logging.debug("get quantity error:==%s",str(e))
			data='getquantity_error'
	else:
		data='getquantity_error'
		logging.info('SAP Connection Error.No Free User Licenses @ '+str(datetime.datetime.now()))
	return jsonify(data)

@zen_pmpr.route('/material_consumption_report/', methods = ['GET', 'POST'])
@login_required
def material_consumption_report():
	try:
		user_details = base()
		quantity=0
		departments={}
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
		dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
		for i in dept:
			departments[i['dept_pid']]=i['dept']
		dept = sorted(dept, key=itemgetter('dept'))
		return render_template('material_consumption_report.html',user_details=user_details,dept=dept,plant_id=plant_pid[0]['plant_id1'])
	except:
		return redirect(url_for('zenpmpr.index'))
		
@zen_pmpr.route('/dept_material_report/', methods = ['GET', 'POST'])
def dept_material_report():
	if request.method=='POST':
		form=request.form
		work_centers={}
		dept_desc=find_one_in_collection("departmentmaster",{"plant_id":form['plant_id'],'dept_pid':form['dept']})
		work_center=find_in_collection("workcentermaster",{"Plant":form['plant_id']})
		for k in work_center:
			work_centers[k['work_center']]=k['work_center_desc']
		# dept_desc=dept[0]['dept']
		material_report=[]
		mat=[]
		db=dbconnect()
		form=request.form
		print form,'material list report'
		start_date=date_time_datetime(form['from_date'],'00:00:00')
		start_date=start_date.strftime('%Y-%m-%d %H:%M:%S')
		start_date=datetime.datetime.strptime(start_date,'%Y-%m-%d %H:%M:%S')
		end_date=date_time_datetime(form['to_date'],'00:00:00')
		end_date=end_date.strftime('%Y-%m-%d %H:%M:%S')
		end_date=datetime.datetime.strptime(end_date,'%Y-%m-%d %H:%M:%S')
		end_date=end_date+timedelta(days=1)
		if 'costcenter_report' not in form and 'detailed_report' not in form:
			flash('Please Select A Report Type To View','alert-danger')
			report=''
		else:
			if 'costcenter_report' in form:	
				if form['dept'] !='select':
					mat = db.mivcreation.aggregate([{'$unwind':'$voucher'},			
						{'$match':{'$and':[{'status':'approved'},{'PR_dept':form['dept']},{"pr_appliedon":{'$gte':start_date,'$lte':end_date}}]}},
						{'$limit':1000}
						])
					mat=list(mat)
					report='costcenter_report'
	
				for i in mat:
					# print "!!!!!!",i['voucher']['DMBTR']
					if 'equip_funcloc' in i:
						equip_funcloc=i['equip_funcloc']
						equipment=i['equipment'][5:]
						eqp_desc=i['eqp_desc']
					else:
						equip_funcloc=''
						equipment=''
						eqp_desc=''
					if 'cost_center_desc' in i:
						cost_center_desc=i['cost_center_desc']
					else:
						cost_center_desc=''
					if 'cost_activity_desc' in i:
						cost_activity_desc=i['cost_activity_desc']
					else:
						cost_activity_desc=''
					if 'DMBTR' in i['voucher']:
						DMBTR=i['voucher']['DMBTR']
					else:
						DMBTR=''
					if len(i['voucher']['MATNR'])!=14:
						material=i['voucher']['MATNR'][4:]
					else:
						material=i['voucher']['MATNR']
					material_report.append([dept_desc['dept'],i['pr_appliedon'].strftime('%d-%m-%Y'),material,i['voucher']['MAT_DESC'],i['voucher']['MENGE'],i['voucher']['MEINS'],i['costcenter'],cost_center_desc,work_centers[i['P_ARBPL']],cost_activity_desc,DMBTR])
			elif 'detailed_report' in form:
				if form['dept'] !='':
					if form['element_choice']=='fl_input':
						mat = db.mivcreation.aggregate([{'$unwind':'$voucher'},			
						{'$match':{'$and':[{'status':'approved'},{'PR_dept':form['dept']},{"pr_appliedon":{'$gte':start_date,'$lte':end_date}},{'functionlocation':form['function_location']}]}},
						{'$limit':10000}
						])
						mat=list(mat)
					elif form['element_choice']=='equip_input':
						mat = db.mivcreation.aggregate([{'$unwind':'$voucher'},			
						{'$match':{'$and':[{'status':'approved'},{'PR_dept':form['dept']},{"pr_appliedon":{'$gte':start_date,'$lte':end_date}},{'equipment':form['equipment']}]}},
						{'$limit':10000}
						])
						mat=list(mat)
					elif form['element_choice']=='wbs_input':
						mat = db.mivcreation.aggregate([{'$unwind':'$voucher'},			
							{'$match':{'$and':[{'status':'approved'},{'PR_dept':form['dept']},{"pr_appliedon":{'$gte':start_date,'$lte':end_date}},{'wbs_element':form['wbs_element']}]}},
							{'$limit':10000}
							])
						mat=list(mat)
					else:
						mat = db.mivcreation.aggregate([{'$unwind':'$voucher'},			
							{'$match':{'$and':[{'status':'approved'},{'PR_dept':form['dept']},{"pr_appliedon":{'$gte':start_date,'$lte':end_date}}]}},
							{'$limit':10000}
							])
						mat=list(mat)
					report='detailed_report'	
				if form['dept'] =='':
					flash('Please Select Department To View Report','alert-danger')
					report=''
				for i in mat:
					
					if 'equip_funcloc' in i:
						equip_funcloc=i['equip_funcloc']
						equipment=i['equipment'][5:]
						eqp_desc=i['eqp_desc']
						print "fun",equip_funcloc
					else:
						equip_funcloc=''
						equipment=''
						eqp_desc=''
					if 'cost_center_desc' in i:
						cost_center_desc=i['cost_center_desc']
					else:
						cost_center_desc=''
					if len(i['voucher']['MATNR'])!=14:
						material=i['voucher']['MATNR'][4:]
					else:
						material=i['voucher']['MATNR']

					if 'DMBTR' in i['voucher']:
						DMBTR=i['voucher']['DMBTR']
						# print "DMBTR",DMBTR
					else:
						DMBTR=''
					material_report.append([dept_desc['dept'],i['pr_appliedon'].strftime('%d-%m-%Y'),material,i['voucher']['MAT_DESC'],i['voucher']['MENGE'],i['voucher']['MEINS'],equip_funcloc,equipment,eqp_desc,i['costcenter'],cost_center_desc,i['wbs_element'],work_centers[i['P_ARBPL']],DMBTR])
		return jsonify({'result':material_report,"report":report})



################################### End New Developments ################################


# @zen_pmpr.route('/costcenter', methods = ['GET', 'POST'])
# def costcenter():
# 	q = request.args.get('term')
# 	db=dbconnect()
# 	user_details = base()
# 	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
# 	cm= db.costcentersmaster.aggregate([
# 	    {'$match' : { "plant":plant_pid[0]['plant_id1'],'$and': [{ '$or':[{"cost_center_desc" : {'$regex': str(q),'$options': 'i' }},{'cost_center' : {'$regex': str(q),'$options': 'i' }}] }]} }, 
# 	    {'$project' : {'cost_center':1,'cost_center_desc':1,"_id":0}}])
# 	cm=list(cm)
# 	return json.dumps({"results":cm})


# download all the  masters  from  sap to mongodb
@zen_pmpr.route('/equipmentmaster/', methods = ['GET', 'POST'])
def equipmentmaster():
	conn = Connection(user='portal_batch1', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')			
	result = conn.call('Z_MZMM01_EQUIP_F4')
	conn.close()
	drop_col('equipmentmaster')
	# close_sap_user_credentials(sap_login['_id'])
	for i in range(0,len(result['IT_EQUIP'])):
		e = result['IT_EQUIP'][i]
		array={"plant":e['WERKS'],"equipment_category":e['EQTYP'],"Desc":e['LTEXT'],"equipment_number":e['EQUNR'],"Desp_technical_object":e['SHTXT'],"cost_center":e['KOSTL'],"functional_loc":e['TPLNR'],"Desp_functional_loc":e['TXT_TPLNR'],"planner_group_id":e['INGRP'],"work_center":e['ARBPL'],"main_work_center":e['GEWRK']}
		save_collection('equipmentmaster',array)
	return "successfully"
	
@zen_pmpr.route('/materialmaster/')
def materialmaster():
	c=os.path.abspath("zenapp/static/Materialupload.csv")		
	with open(c) as csvfile:
		reader = csv.DictReader(csvfile)
		i=0
		for e in reader:
			array={u"material":str(e['Material']),u"material_desc":str(e['MaterialDescription']),u"old_material":str(e['Oldmaterialnumber']),u"mtype":str(e['MaterialType']),u"matl_group":str(e['MatlGroup']),u"prod_hierarchy":str(e['Producthierarchy']),u"prod_hierarchy_desc":str(e['ProducthierarchyDescription']),u"plant_id":str(e['Plant']),u"st_loc":str(e['Storagelocation']),u"stock":str(e['Stock']),u"bun":str(e['UnitofMeasure']),u"st_bin":str(e['StorageBin']),u"Mov_avg_price":str(e['MovingAveragePrice']),u"mat_po_txt":str(e['MaterialPOtext']),"mat_crtd_dt":""}
			i=i+1
			if i%1000==0:
				subprocess.call("sudo service mongod restart",shell=True)
			
			save_collection('materialmaster',array)
		return "successfully"


@zen_pmpr.route('/costcentersmaster/', methods = ['GET', 'POST'])
def costcentersmaster():
	conn = Connection(user='portal_batch1', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	result = conn.call('Z_MZMM01_COSTC_F4')
	conn.close()
	drop_col('costcentersmaster')
	# close_sap_user_credentials(sap_login['_id'])
	for i in range(0,len(result['IT_CSKS'])):
		e = result['IT_CSKS'][i]
		if e['VERAK'] == 'TCCEM':
			array={"controlling_area":e['KOKRS'],"cost_center_desc":e['LTEXT'],"cost_center":e['KOSTL'],"cost_center_category":e['KOSAR'],"person_responsible":e['VERAK'],"profit_center":e['PRCTR'],"business_area":e['GSBER'],"plant":"1001"}
		elif e['VERAK'] == 'GPCEM':
			array={"controlling_area":e['KOKRS'],"cost_center_desc":e['LTEXT'],"cost_center":e['KOSTL'],"cost_center_category":e['KOSAR'],"person_responsible":e['VERAK'],"profit_center":e['PRCTR'],"business_area":e['GSBER'],"plant":"1002"}
		elif e['VERAK'] == 'BPCEM':
			array={"controlling_area":e['KOKRS'],"cost_center_desc":e['LTEXT'],"cost_center":e['KOSTL'],"cost_center_category":e['KOSAR'],"person_responsible":e['VERAK'],"profit_center":e['PRCTR'],"business_area":e['GSBER'],"plant":"1003"}
		elif e['VERAK'] == 'TDCEM':
			array={"controlling_area":e['KOKRS'],"cost_center_desc":e['LTEXT'],"cost_center":e['KOSTL'],"cost_center_category":e['KOSAR'],"person_responsible":e['VERAK'],"profit_center":e['PRCTR'],"business_area":e['GSBER'],"plant":"1004"}
		elif e['VERAK'] == 'GPPOW':
			array={"controlling_area":e['KOKRS'],"cost_center_desc":e['LTEXT'],"cost_center":e['KOSTL'],"cost_center_category":e['KOSAR'],"person_responsible":e['VERAK'],"profit_center":e['PRCTR'],"business_area":e['GSBER'],"plant":"2001"}
		else:
			array={"controlling_area":e['KOKRS'],"cost_center_desc":e['LTEXT'],"cost_center":e['KOSTL'],"cost_center_category":e['KOSAR'],"person_responsible":e['VERAK'],"profit_center":e['PRCTR'],"business_area":e['GSBER'],"plant":""}
		save_collection('costcentersmaster',array)
	return "successfully"
	
@zen_pmpr.route('/storagelocmaster/', methods = ['GET', 'POST'])
def storagelocmaster():
	conn = Connection(user='portal_batch1', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	result = conn.call('Z_MZMM01_STORAGE_LOC_F4')
	conn.close()
	drop_col('storagelocmaster')
	for i in range(0,len(result['IT_T001L'])):
		e = result['IT_T001L'][i]
		array={"plant":e['WERKS'],"storage_loc":e['LGORT'],"desc_storage_loc":e['LGOBE']}
		save_collection('storagelocmaster',array)
	return "successfully"

@zen_pmpr.route('/material_master', methods = ['GET', 'POST'])
def material_master():
	sap_login=get_sap_user_credentials()
	todayDate=datetime.date.today()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
		result = conn.call('Z_MZMM01_PMPR_MATERIAL_INS',IM_ERSDA=todayDate)
		#logging.debug("lOAD material fROM SAP  result:==%s",str(result))
		logging.debug("Sucessfully Loaded material fROM SAP")
		conn.close()
		close_sap_user_credentials(sap_login['_id'])

	except Exception,e:
		logging.debug("lOAD material fROM SAP  error:==%s",str(e))
		close_sap_user_credentials(sap_login['_id'])
		# return "unsuccessfully"
	for i in range(0,len(result['IT_MARA_D'])):
		e = result['IT_MARA_D'][i]
		create= datetime.datetime.strptime(str(e['ERSDA']), '%Y-%m-%d')
		create_new = create.strftime('%d-%m-%Y')
		array={"matl_group" :e['MATKL'],"plant_id" : e['WERKS'],"material" : e['MATNR'],"material_desc" : e['MAKTX'],"prod_hierarchy" : e['PRODH'],"mat_crtd_dt" : create_new,"st_bin" : e['LGPBE'],"mtype" :e['MTART'],"prod_hierarchy_desc" : e['VTEXT'],"bun" :e['MEINS'],"mat_po_txt" :e['MPOTXT'],"st_loc" :e['LGORT'],"old_material" :e['BISMT'],"Mov_avg_price" : float(e['VERPR']),"stock" :float(e['LABST'])}

		findMaterialObj=find_one_in_collection("materialmaster",{"material":e['MATNR'],"plant_id":e['WERKS']})
		if findMaterialObj:
			del_doc('materialmaster',{"plant_id":e['WERKS'],"material" : e['MATNR']})
			save_collection('materialmaster',array)
			save_collection('addedMaterialMaster',array)
		
		else:
			save_collection('materialmaster',array)
			save_collection('addedMaterialMaster',array)
	return "Successfully"

@zen_pmpr.route('/functionlocmaster/', methods = ['GET', 'POST'])
def functionlocmasterr():
	
	drop_col('functionlocmaster')
	conn = Connection(user='portal_batch1', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	result = conn.call('Z_MZMM01_FUNLOC_F4')
	conn.close()
	for i in range(0,len(result['IT_FL'])):
		e = result['IT_FL'][i]
		array={"functionalloc_desc":str(e['PLTXT']),u"plant":str(e["IWERK"]),u"functional_loc":str(e["TPLNR"]),u"cost_center":str(e["KOSTL"])}
		save_collection('functionlocmaster',array)
	return "successfully"
	

@zen_pmpr.route('/wbselementmaster/', methods = ['GET', 'POST'])
def wbselementmaster():
	
	conn = Connection(user='portal_batch1', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
	result = conn.call('Z_MZMM01_WBS_F4')
	conn.close()
	for i in range(0,len(result['IT_PRPS'])):
		e = result['IT_PRPS'][i]
		array={"plant":e['WERKS'],"WBS_element":e['PSPNR'],"short_desc":e['POST1'],"object_number":e['OBJNR'],"current_number_appropriate_project":e['PSPHI'],"WBS_element_short_identification":e['POSKI'],"profit_center":e['PRCTR'],"project_type":e['PRART'],"level_project_hierarchy":e['STUFE'],"network_assignment":e['ZUORD'],"usage_condition_table":e['KVEWE'],"costing_sheet":e['KALSM'],"overhead_key":e['ZSCHL'],"results_analysis_key":e['ABGSL'],"object_class":e['SCOPE'],"loc":e['STORT'],"functional_area":e['FUNC_AREA'],}
		save_collection('wbselementmaster',array)
	return "successfully"
	

@zen_pmpr.route('/assetmaster/', methods = ['GET', 'POST'])
def assetmaster():
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_MZMM01_ASSET_F4')
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		drop_col('assetmaster')
		for i in range(0,len(result['IT_ASSET'])):
			e = result['IT_ASSET'][i]
			create= datetime.datetime.strptime(str(e['ERDAT']), '%Y-%m-%d')
			create_new = create.strftime('%d-%m-%Y')
			array={"main_asset_number":e['ANLN1'],"asset_subnumber":e['ANLN2'],"asset_class":e['ANLKL'],"technical_asset_number":e['GEGST'],"asset_types":e['ANLAR'],"object_creator":e['ERNAM'],"created_on":create_new,"asset_desc":e['TXT50'] }
			save_collection('assetmaster',array)
		return "successfully"
	except:
		close_sap_user_credentials(sap_login['_id'])
		return "Unable to connect Sap user Credential"



@zen_pmpr.route('/departmentmaster/')
def departmentmaster():
	c=os.path.abspath("zenapp/static/dept.csv")
		
	with open(c) as csvfile:
		reader = csv.DictReader(csvfile)
		i=0
		for e in reader:
			array={u"plant":str(e['plant']),u"dept":str(e['dept']),u"dept_pid":str(e['dept_pid']),u"pur_grp":str(e['pur_grp']),u"pur_grp_desc":str(e['pur_grp_desc']),u"dept_chief_pid":str(e['dept_chief_pid'])}
			i=i+1
			save_collection('departmentmaster',array)
		return "successfully"


# show  pdf/image file from mongodb(gridfs) to html page 
# when user creates an miv or pr he upload a file of pdf and by this function approves will able to see this document in miv/pr approval 

@zen_pmpr.route('/static/pdf/gridfs/<filename>')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    imgname= find_one_in_collection('fs.files',{"_id" :ObjectId(filename)})
    thing = fs.get_last_version(filename=imgname['filename'])
    return Response(thing, mimetype='application/pdf')

#when user upload any document while creating any miv or pr this function help this document to be saved in database

def file_save_gridfs(file_data,ctype,ctag):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username #current employee id
    file_ext = file_data.filename.rsplit('.', 1)[1]
    a=fs.put(file_data.read(), content_type=ctype, filename = str(uuid.uuid4())+'.'+file_ext, uid = uid,
        tag = ctag)
    return a 

#this is for creating pm order 
@zen_pmpr.route('/prcreation', methods=['GET', 'POST'])
@login_required
def purchasecreation():
	user_details = base()
	uid = current_user.username
	logging.info("prcreation start time::"+str(datetime.datetime.now().time()))
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	dept = sorted(dept, key=itemgetter('dept'))
	mtype=find_dist_in_collection("materialmaster","mtype")
	mtype.sort()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	
	workcenter=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
	work_center = sorted(workcenter, key=itemgetter('work_center_desc'))
	if request.method == 'POST':
		form=request.form
		
		top=[]
		top=form['topval'].split(',')
		
		ZRGS=[]
		ZRSV=[]
		ZRGS1=[]
		ZRSV1=[]
		prarray={}
		# this is for creating components  array 
		
		if top[1] == "ZRGS":
			description1=form.getlist("description1")
			material1=form.getlist("material1")
			quantity1=form.getlist('quantity1')
			uom1=form.getlist('uom1')
			# st_loc=form.getlist('st_loc')
			unit_value1=form.getlist('unit_value1')
			date1=form.getlist('date1')
			just1=form.getlist('just1')
			poheader1=form.getlist('poheader1')
			specify1=form.getlist('specify1')
			procured1=form.getlist('procured1')
			image=request.files.getlist("file1[]")
			
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","com_pmorder")
				else:
					a=""
				save_image.append(a)
			
			for i in range(0,len(material1)):
				array={}
				create_new=datetime.datetime.strptime(date1[i],"%d.%m.%Y")
				mat_grp=find_and_filter('materialmaster',{"material":material1[i]},{"matl_group":1,"material_desc":1,"_id":0})
				
				array={"PSTYP":"N","MATNR":material1[i],"MATKL":mat_grp[0]['matl_group'],"MENGE":decimal.Decimal(quantity1[i]),"MEINS":uom1[i],"VERPR":unit_value1[i],"EEIND":create_new,"JUSTI":just1[i],"POHEAD":poheader1[i],"SPEC":specify1[i],"PROD":procured1[i]}
				array1={"PSTYP":"N","MAT_DESC":mat_grp[0]['material_desc'],"MATKL":mat_grp[0]['matl_group'],"MATNR":material1[i],"MENGE":float(quantity1[i]),"MEINS":uom1[i],"LGORT":"","VERPR":unit_value1[i],"EEIND":create_new,"JUSTI":just1[i],"POHEAD":poheader1[i],"SPEC":specify1[i],"PROD":procured1[i]}
				try:
					if save_image[i]:
						array1['file']=save_image[i]
				except:
					pass			
				ZRGS.append(array)
				ZRGS1.append(array1)
				logging.info("material master end time::"+str(datetime.datetime.now().time()))
			
		# this is for creating services   array 
		else:
			description2=form.getlist("description2")
			quantity2=form.getlist('quantity2')
			uom2=form.getlist('uom2')
			unit_value2=form.getlist('unit_value2')
			date2=form.getlist('date2')
			procured2=form.getlist('procured2')
			image=request.files.getlist("file2[]")
			logging.info("pr services start time::"+str(datetime.datetime.now().time()))
			
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","service_pmorder")
				else:
					a=""
				save_image.append(a)

			for j in range(0,len(description2)):
				array={}
				create_new=datetime.datetime.strptime(date2[j],"%d.%m.%Y")
				array={"MATKL":"SERVICE","SER_TXT":description2[j],"MENGE":float(quantity2[j]),"MEINS":uom2[j],"VERPR":unit_value2[j],"EEIND":create_new,"PROD":procured2[j]}
				array1={"MATKL":"SERVICE","SER_TXT":description2[j],"MENGE":float(quantity2[j]),"MEINS":uom2[j],"VERPR":unit_value2[j],"EEIND":create_new,"PROD":procured2[j]}
				try:
					if save_image[i]:
						array1['file']=save_image[i]
				except:
					pass

				ZRSV.append(array)
				ZRSV1.append(array1)
			
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"pur_grp":1,"req_desc":1,"_id":0})
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
		pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
		logging.debug("pr_permission result:==%s",str(pr_permission))
		if pr_permission==None:
			data={}
			msg="Permission for your plant has not been setup."
			a=[{"check":"1","msg":msg}]
			data['report']=a
			return jsonify(data)
				
		levels=[]
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})

		if "1" in pr_permission["level"]:
			levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		if "2" in pr_permission["level"]:
			if "1" in pr_permission["level"]:
				levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
			else:
				 levels.append({"a_status":"current","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		if "3" in pr_permission["level"]:
			if "1" in pr_permission["level"] or "2" in pr_permission["level"]: 
				levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})
			else:
				levels.append({"a_status":"current","a_id":plant_pid1[0]['username'],"a_time":"None"})



		prname1=get_employee_name(current_user.username)
		prname=dept_chief_pid[0]['req_desc']
		data={}
		a=[]
		#this is used for sending  components  array plus header level arguments to sap and saving it to database 
		if len(ZRGS)!=0:
			sap_login=get_sap_user_credentials()
			try:
				conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
				result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT="F",P_ARBPL=top[6],P_TPLNR=top[3],P_EQUNR=top[4],P_KOSTL=top[5],P_AUART="PM04",P_EKORG="1000",P_EKGRP=dept_chief_pid[0]['pur_grp'],P_WERKS=plant_pid[0]['plant_id1'],P_PRNAME=prname,IT_PRCRT=ZRGS,P_STEUS="PM01",P_SIMULATION="X",P_DESCR=top[8],P_OBJID="00000000")
				
				conn.close()
				close_sap_user_credentials(sap_login['_id'])
				if result['IT_PMCRET'][0]["TYPE"]=='E':			
					# flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
					a=[{"check":"1","msg":result['IT_PMCRET'][0]["MESSAGE"]}]
					data['report']=a
					return jsonify(data)
				else:
					
					equip=find_and_filter('equipmentmaster',{"equipment_number":top[4]},{"functional_loc":1,"Desp_technical_object":1,"_id":0})
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[5]},{"cost_center_desc":1,"_id":0})
					cost_center_desc = costcenterdesc[0]['cost_center_desc']
					
					if len(equip)==0:
						Equip_Desp_technical_object=""
						Equip_functional_loc=""
					else:
						Equip_Desp_technical_object=equip[0]['Desp_technical_object']
						Equip_functional_loc=equip[0]['functional_loc']
					
					prarray={"status":"applied","Assignment_category":"F","functionlocation":top[3],"equipment":top[4],"costcenter":top[5],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels,"P_ARBPL":top[6],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"P_DESCR":top[8],
							"P_Datetime":top[9],"equip_funcloc":Equip_functional_loc,"equip_Desc":Equip_Desp_technical_object,"cost_center_desc":cost_center_desc}
					
					prarray['generalPr']='0'
					
					prarray['history']=[{"level1":{"component":quantity1,"service":[]},"level2":"","level3":"","level4":""}]
					save_collection("prcreation",prarray)
					
					msg="You have raised PM Order on "+top[9]+" and it is pending for approval at HOD of " +top[7]+" Department" 
					flash(msg,'alert-success')
					msg1=prname1+" has raised PM Order on "+top[9]+" and it is pending for your approval" 
					notification(uid,msg)
					notification(dept_chief_pid1[0]['username'],msg1)
					logging.info("prcreation end time::"+str(datetime.datetime.now().time()))
					a=[{"check":"0","msg":msg}]
					data['report']=a
					return jsonify(data)
				conn.close()
				return "successfully"
			except Exception,e:
				logging.debug("purchase creation approval error:==%s",str(e))
				flash("Unable to connect Sap user Credential","alert-danger")

			
		#this is used for sending  services  array plus header level arguments to sap and saving it to database 
		else:
			sap_login=get_sap_user_credentials()
			try:
				conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
				result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT="F",P_ARBPL=top[6],P_TPLNR=top[3],P_EQUNR=top[4],P_KOSTL=top[5],P_AUART="PM04",P_EKORG="1000",P_EKGRP=dept_chief_pid[0]['pur_grp'],P_WERKS=plant_pid[0]['plant_id1'],P_PRNAME=prname,IT_PRSER=ZRSV,P_STEUS="PM02",P_SIMULATION="X",P_DESCR=top[8],P_OBJID="00000000")
				
				conn.close()
				close_sap_user_credentials(sap_login['_id'])
				if result['IT_PMCRET'][0]["TYPE"]=='E':			
					# flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
					a=[{"check":"1","msg":result['IT_PMCRET'][0]["MESSAGE"]}]
					data['report']=a
					return jsonify(data)
				else:
					equip=find_and_filter('equipmentmaster',{"equipment_number":top[4]},{"functional_loc":1,"Desp_technical_object":1,"_id":0})
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[5]},{"cost_center_desc":1,"_id":0})
					cost_center_desc = costcenterdesc[0]['cost_center_desc']
					if len(equip)==0:
						Equip_Desp_technical_object=""
						Equip_functional_loc=""
					else:
						Equip_Desp_technical_object=equip[0]['Desp_technical_object']
						Equip_functional_loc=equip[0]['functional_loc']
					prarray={"status":"applied","Assignment_category":"F","functionlocation":top[3],"equipment":top[4],"costcenter":top[5],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":"" ,"service": ZRSV1,"approval_levels":levels,"P_ARBPL":top[6],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"P_DESCR":top[8],"P_Datetime":top[9],"equip_funcloc":Equip_functional_loc,"equip_Desc":Equip_Desp_technical_object,"cost_center_desc":cost_center_desc}
					prarray['generalPr']='0'
					prarray['history']=[{"level1":{"component":[],"service":quantity2},"level2":"","level3":"","level4":""}]
					save_collection("prcreation",prarray)

					msg= "You have raised PM Order on "+top[9]+" and it is pending for approval at HOD of "+top[7]+" Department"
					flash(msg,'alert-success')
					msg1=prname1+" has raised PM Order on "+top[9]+" and it is pending for your approval" 
					notification(uid,msg)
					notification(dept_chief_pid1[0]['username'],msg1)
					a=[{"check":"0","msg":msg}]
					data['report']=a
					logging.info("prservice end time::"+str(datetime.datetime.now().time()))
					return jsonify(data)
				conn.close()
				
			except Exception,e:
				logging.debug("purchase creation approval error:==%s",str(e))
				flash("Unable to connect Sap user Credential","alert-danger")

	return render_template('purchasecreation.html',user_details=user_details,dept=dept,mtype=mtype,work_center=work_center)

#this to create the costcenter  
@zen_pmpr.route('/prcostcenter', methods=['GET', 'POST'])
@login_required
def prcostcenter():
	user_details = base()
	uid = current_user.username
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	dept = sorted(dept, key=itemgetter('dept'))
	mtype=find_dist_in_collection("materialmaster","mtype")
	mtype.sort()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	workcenter=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
	work_center = sorted(workcenter, key=itemgetter('work_center_desc'))
	if request.method == 'POST':
		form=request.form
		top=[]
		top=form['topval'].split(',')
		ZRGS=[]
		ZRSV=[]
		ZRGS1=[]
		ZRSV1=[]
		# this is for creating components  array 
		if top[1] == "ZRGS":
			description3=form.getlist("description3")
			material3=form.getlist("material3")
			quantity3=form.getlist('quantity3')
			uom3=form.getlist('uom3')
			unit_value3=form.getlist('unit_value3')
			date3=form.getlist('date3')
			just3=form.getlist('just3')
			poheader3=form.getlist('poheader3')
			specify3=form.getlist('specify3')
			# st_loc=form.getlist('st_loc')
			procured3=form.getlist('procured3')

			image=request.files.getlist("file3[]")
			
			save_image=[]
			for img in image:
				if img.filename !="":
					
					a=file_save_gridfs(img,"application/pdf","com_costcenter")
				else:
					a=""
				save_image.append(a)
		
			for i in range(0,len(description3)):
				array={}
				create_new=datetime.datetime.strptime(date3[i],"%d.%m.%Y")
				mat_grp=find_and_filter('materialmaster',{"material":material3[i]},{"matl_group":1,"material_desc":1,"_id":0})

				array={"PSTYP":"N","MATNR":"0000"+material3[i],"MATKL":mat_grp[0]['matl_group'],"MENGE":decimal.Decimal(quantity3[i]),"MEINS":uom3[i],"VERPR":unit_value3[i],"EEIND":create_new,"JUSTI":just3[i],"POHEAD":poheader3[i],"SPEC":specify3[i],"PROD":procured3[i]}
				array1={"PSTYP":"N","MAT_DESC":mat_grp[0]['material_desc'],"MATNR":material3[i],"MATKL":mat_grp[0]['matl_group'],"MENGE":float(quantity3[i]),"MEINS":uom3[i],"VERPR":unit_value3[i],"EEIND":create_new,"JUSTI":just3[i],"POHEAD":poheader3[i],"SPEC":specify3[i],"LGORT":0,"PROD":procured3[i]}
				try:
					array1['file']=save_image[i]
				except:
					pass			
				ZRGS.append(array)
				ZRGS1.append(array1)
			
		# this is  for creating service  array 
		else:
			
			service_material3=form.getlist("service_material3")
			description4=form.getlist("description4")
			quantity4=form.getlist('quantity4')
			uom4=form.getlist('uom4')
			unit_value4=form.getlist('unit_value4')
			date4=form.getlist('date4')
			procured4=form.getlist('procured4')
			image=request.files.getlist("file4[]")
			# #print image
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","service_costcenter")
				else:
					a=""
				save_image.append(a)

			for j in range(0,len(description4)):
				array={}
				create_new=datetime.datetime.strptime(date4[j],"%d.%m.%Y")
				array={"MATKL":service_material3[j],"SER_TXT":description4[j],"MENGE":float(quantity4[j]),"MEINS":uom4[j],"VERPR":unit_value4[j],"EEIND":create_new,"PROD":procured4[j]}
				array1={"MATKL":service_material3[j],"SER_TXT":description4[j],"MENGE":float(quantity4[j]),"MEINS":uom4[j],"VERPR":unit_value4[j],"EEIND":create_new,"PROD":procured4[j]}
				try:
					array1['file']=save_image[j]
				except:
					pass

				ZRSV.append(array)
				ZRSV1.append(array1)
			
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"req_desc":1,"pur_grp":1,"_id":0})
		
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})

		#pr level permission check 
		pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
		logging.debug("pr_permission result:==%s",str(pr_permission))
		if pr_permission==None:
			data={}
			msg="Permission for your plant has not been setup."
			a=[{"check":"1","msg":msg}]
			data['report']=a
			return jsonify(data)
				
		levels=[]
		logging.debug("pr cost center  going to find approval level uids ");
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		logging.debug("dept_chief_pid :==%s",str(dept_chief_pid1[0]['username']))
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		logging.debug("plant_pid :==%s",str(plant_pid1[0]['username']))
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})

		if "1" in pr_permission["level"]:
			levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		if "2" in pr_permission["level"]:
			if "1" in pr_permission["level"]:
				levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
			else:
				 levels.append({"a_status":"current","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		if "3" in pr_permission["level"]:
			if "1" in pr_permission["level"] or "2" in pr_permission["level"]: 
				levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})
			else:
				levels.append({"a_status":"current","a_id":plant_pid1[0]['username'],"a_time":"None"})

		prname1=get_employee_name(current_user.username)
		prname=dept_chief_pid[0]['req_desc']
	
		data={}
		a=[]
		logging.debug("pr cost center  going to connect sap ");
		#this is used for sending  components  array plus header level arguments to sap and saving it to database 
		if len(ZRGS)!=0:
			sap_login=get_sap_user_credentials()
			try:
				conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
				#result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT="K",P_ARBPL=top[3],P_KOSTL=top[2],P_AUART="ZRGS",P_WERKS=plant_pid[0]['plant_id1'],P_PRNAME=prname,P_EKORG="1000",P_EKGRP=dept_chief_pid[0]['pur_grp'],IT_PRCRT=ZRGS,P_SIMULATION="X")
				result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT="K",IM_KOSTL=top[2],IM_AUART="ZRGS",IM_WERKS=plant_pid[0]['plant_id1'],IM_PRNAME=prname,IM_EKORG="1000",IM_EKGRP=dept_chief_pid[0]['pur_grp'],IT_PRCRT=ZRGS,IM_SIMULATION="X",IM_DESCR=top[6])
				print "result from Sap",result
				
				logging.debug("pr cost center only  result:==%s",str(result))
				conn.close()
				close_sap_user_credentials(sap_login['_id'])
				if result['IT_PMCRET'][0]["TYPE"]=='E':			
					# flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
					a=[{"check":"1","msg":result['IT_PMCRET'][0]["MESSAGE"]}]
					data['report']=a
					return jsonify(data)
				else:
					prarray={"status":"applied","Assignment_category":"K","functionlocation":"","equipment":"","costcenter":top[2],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels,"P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"P_Datetime":top[5],"IM_DESCR":top[6]}
					prarray['generalPr']='0'
					prarray['history']=[{"level1":{"component":quantity3,"service":[]},"level2":"","level3":"","level4":""}]
					save_collection("prcreation",prarray)
					msg="You have raised PR Cost Center on "+top[5]+" and it is pending for approval at HOD of " +top[4]+" Department"
					flash(msg,'alert-success')
					msg1=prname1+"has raised PR Cost Center on "+top[5]+" and it is pending for your approval" 
					notification(uid,msg)
					notification(dept_chief_pid1[0]['username'],msg1)
					a=[{"check":"0","msg":msg}]
					data['report']=a
					return jsonify(data)
			except Exception,e:
				logging.debug("costcenter approval error:==%s",str(e))
				flash("Unable to connect Sap user Credential","alert-danger")
			
		#this is used for sending  services   array plus header level arguments to sap and saving it to database 
		else:
			sap_login=get_sap_user_credentials()
			try:
				conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
				#result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT="K",P_ARBPL=top[3],P_KOSTL=top[2],P_AUART="ZRSV",P_WERKS=plant_pid[0]['plant_id1'],P_PRNAME=prname,P_EKORG="1000",P_EKGRP=dept_chief_pid[0]['pur_grp'],IT_PRSER=ZRSV,P_SIMULATION="X")
				result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT="K",IM_KOSTL=top[2],IM_AUART="ZRSV",IM_WERKS=plant_pid[0]['plant_id1'],IM_PRNAME=prname,IM_EKORG="1000",IM_EKGRP=dept_chief_pid[0]['pur_grp'],IT_PRSER=ZRSV,IM_SIMULATION="X",IM_DESCR=top[6])
				print "from sap",result
				logging.debug("pr Services only  result:==%s",str(result))
				
				conn.close()
				close_sap_user_credentials(sap_login['_id'])
				print"626" 
				if result['IT_PMCRET'][0]["TYPE"]=='E':		
					print("628")	
					# flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
					a=[{"check":"1","msg":result['IT_PMCRET'][0]["MESSAGE"]}]
					print "631"
					data['report']=a
					return jsonify(data)
				else:
					print "INSIDE ELSE 632"
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[2]},{"cost_center_desc":1,"_id":0})
					cost_center_desc = costcenterdesc[0]['cost_center_desc']
					prarray={"status":"applied","Assignment_category":"K","functionlocation":"","equipment":"","costcenter":top[2],"wbs_element":"","network":"","assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":"" ,"service": ZRSV1,"approval_levels":levels,"P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"P_Datetime":top[5],"cost_center_desc":cost_center_desc,"IM_DESCR":top[6]}
					print "636"
					prarray['generalPr']='0'
					print"638"
					prarray['history']=[{"level1":{"component":[],"service":quantity4},"level2":"","level3":"","level4":""}]
					print "640"
					save_collection("prcreation",prarray)
					msg="You have raised PR Cost Center on "+top[5]+" and it is pending for approval at HOD of " +top[4]+" Department"
					flash(msg,'alert-success')
					msg1=prname1+"has raised PR Cost Center on "+top[5]+" and it is pending for your approval" 
					notification(uid,msg)
					notification(dept_chief_pid1[0]['username'],msg1)
					a=[{"check":"0","msg":msg}]
					data['report']=a
					return jsonify(data)
			except Exception,e:
				logging.debug("costcenter approval error:==%s",str(e))
				flash("Unable to connect Sap user Credential","alert-danger")

		#return redirect(url_for('zenpmpr.prcostcenter'))
	return render_template('costcenter.html',user_details=user_details,dept=dept,mtype=mtype,work_center=work_center)

#this is to create  wbs and project 
@zen_pmpr.route('/prproject', methods=['GET', 'POST'])
@login_required
def prproject():
	user_details = base()
	uid = current_user.username
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	dept = sorted(dept, key=itemgetter('dept'))
	mtype=find_dist_in_collection("materialmaster","mtype")
	mtype.sort()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	
	workcenter=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
	work_center = sorted(workcenter, key=itemgetter('work_center_desc'))

	if request.method == 'POST':
		form=request.form
		
		top=[]
		top=form['topval'].split(',')
		ZRGS=[]
		ZRSV=[]
		ZRGS1=[]
		ZRSV1=[]
		# this is for creating components  array 
		if top[1] == "ZRGS":
			description5=form.getlist("description5")
			material5=form.getlist("material5")
			quantity5=form.getlist('quantity5')
			uom5=form.getlist('uom5')
			unit_value5=form.getlist('unit_value5')
			date5=form.getlist('date5')
			just5=form.getlist('just5')
			poheader5=form.getlist('poheader5')
			specify5=form.getlist('specify5')
			# st_loc=form.getlist('st_loc')
			procured5=form.getlist('procured5')
			image=request.files.getlist("file5[]")
			
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","com_p/n")
				else:
					a=""
				save_image.append(a)
		
			for i in range(0,len(description5)):
				array={}
				create_new=datetime.datetime.strptime(date5[i],"%d.%m.%Y")
				mat_grp=find_and_filter('materialmaster',{"material":material5[i]},{"matl_group":1,"material_desc":1,"_id":0})

				array={"PSTYP":"N","MATNR":material5[i],"MATKL":mat_grp[0]['matl_group'],"MENGE":decimal.Decimal(quantity5[i]),"MEINS":uom5[i],"VERPR":unit_value5[i],"EEIND":create_new,"JUSTI":just5[i],"POHEAD":poheader5[i],"SPEC":specify5[i],"PROD":procured5[i]}
				array1={"PSTYP":"N","MAT_DESC":mat_grp[0]['material_desc'],"MATNR":material5[i],"MATKL":mat_grp[0]['matl_group'],"MENGE":float(quantity5[i]),"MEINS":uom5[i],"VERPR":unit_value5[i],"EEIND":create_new,"JUSTI":just5[i],"POHEAD":poheader5[i],"SPEC":specify5[i],"LGORT":"","PROD":procured5[i],"file":save_image[i]}			
				ZRGS.append(array)
				ZRGS1.append(array1)
			
		# this is for creating components  array 
		else:
			description6=form.getlist("description6")
			quantity6=form.getlist('quantity6')
			uom6=form.getlist('uom6')
			unit_value6=form.getlist('unit_value6')
			date6=form.getlist('date6')
			procured6=form.getlist('procured6')
			image=request.files.getlist("file6[]")
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","service_n/p")
				else:
					a=""
				save_image.append(a)

			for j in range(0,len(description6)):
				array={}
				create_new=datetime.datetime.strptime(date6[j],"%d.%m.%Y")
				array={"MATKL":"SERVICE","SER_TXT":description6[j],"MENGE":quantity6[j],"MEINS":uom6[j],"VERPR":unit_value6[j],"EEIND":create_new,"PROD":procured6[j]}
				array1={"MATKL":"SERVICE","SER_TXT":description6[j],"MENGE":float(quantity6[j]),"MEINS":uom6[j],"VERPR":unit_value6[j],"EEIND":create_new,"PROD":procured6[j],"file":save_image[j]}
				ZRSV.append(array)
				ZRSV1.append(array1)
			
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"req_desc":1,"pur_grp":1,"_id":0})
	
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})

		#pr level permission check 
		pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
		logging.debug("pr_permission result:==%s",str(pr_permission))
		if pr_permission==None:
			data={}
			msg="Permission for your plant has not been setup."
			a=[{"check":"1","msg":msg}]
			data['report']=a
			return jsonify(data)
				
		levels=[]
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})

		if "1" in pr_permission["level"]:
			levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		if "2" in pr_permission["level"]:
			if "1" in pr_permission["level"]:
				levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
			else:
				 levels.append({"a_status":"current","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		if "3" in pr_permission["level"]:
			if "1" in pr_permission["level"] or "2" in pr_permission["level"]: 
				levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})
			else:
				levels.append({"a_status":"current","a_id":plant_pid1[0]['username'],"a_time":"None"})



		prname1=get_employee_name(current_user.username)
		prname=dept_chief_pid[0]['req_desc']
		
		if top[2]=="wbs":
			category="Q"
		else:
			category="N"
		
		data={}
		a=[]
		
		if len(ZRGS)!=0:
			sap_login=get_sap_user_credentials()
			try:
				conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
				result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=category,P_ARBPL=top[5],P_WBS=top[3],P_NTW=top[4],P_AUART="ZRGS",P_WERKS=plant_pid[0]['plant_id1'],P_EKORG="1000",P_EKGRP=dept_chief_pid[0]['pur_grp'],P_PRNAME=prname,IT_PRCRT=ZRGS,P_SIMULATION="X")
				
				conn.close()
				close_sap_user_credentials(sap_login['_id'])
				if result['IT_PMCRET'][0]["TYPE"]=='E':		
					# flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
					a=[{"check":"1","msg":result['IT_PMCRET'][0]["MESSAGE"]}]
					data['report']=a
					return jsonify(data)
				else:
					prarray={"status":"applied","Assignment_category":category,"functionlocation":"","equipment":"","costcenter":"","wbs_element":top[3],"network":top[4],"assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels,"P_ARBPL":top[5],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"P_Datetime":top[7]}
					prarray['generalPr']='0'
					save_collection("prcreation",prarray)
					msg="You have raised PR on "+top[7]+""+ top[2]+ " and it is pending for approval at HOD of " +top[6]+" Department"
					flash(msg,'alert-success')
					msg1=prname1+"has raised PR on "+top[7]+""+ top[2]+ " and it is pending for your approval" 
					notification(uid,msg)
					notification(dept_chief_pid1[0]['username'],msg1)
					a=[{"check":"0","msg":msg}]
					data['report']=a
					return jsonify(data)
			except:
				close_sap_user_credentials(sap_login['_id'])
				flash("Unable to connect Sap user Credential","alert-danger")
			
		#this is used for sending  services   array plus header level arguments to sap and saving it to database 
		else:
			sap_login=get_sap_user_credentials()
			try:
				conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
				result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=category,P_ARBPL=top[5],P_WBS=top[3],P_NTW=top[4],P_AUART="ZRSV",P_WERKS=plant_pid[0]['plant_id1'],P_EKORG="1000",P_EKGRP=dept_chief_pid[0]['pur_grp'],P_PRNAME=prname,IT_PRSER=ZRSV,P_SIMULATION="X")
				
				conn.close()
				close_sap_user_credentials(sap_login['_id'])
				if result['IT_PMCRET'][0]["TYPE"]=='E':			
					# flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
					a=[{"check":"1","msg":result['IT_PMCRET'][0]["MESSAGE"]}]
					data['report']=a
					return jsonify(data)
				else:
					prarray={"status":"applied","Assignment_category":category,"functionlocation":"","equipment":"","costcenter":"","wbs_element":top[3],"network":top[4],"assetcode":"","PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":"" ,"service": ZRSV1,"approval_levels":levels,"P_ARBPL":top[5],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname}
					prarray['generalPr']='0'
					save_collection("prcreation",prarray)
					msg="You have raised PR on "+top[7]+""+ top[2]+ " and it is pending for approval at HOD of " +top[6]+" Department"
					# flash(msg,'alert-success')
					msg1=prname1+"has raised PR on "+top[7]+""+top[2]+ "and it is pending for your approval" 
					notification(uid,msg)
					notification(dept_chief_pid1[0]['username'],msg1)
					a=[{"check":"0","msg":msg}]
					data['report']=a
					return jsonify(data)
			except:
				close_sap_user_credentials(sap_login['_id'])
				flash("Unable to connect Sap user Credential","alert-danger")

	
		#return redirect(url_for('zenpmpr.prproject'))
	return render_template('project.html',user_details=user_details,dept=dept,mtype=mtype,work_center=work_center)

@zen_pmpr.route('/prasset', methods=['GET', 'POST'])
@login_required
def prasset():
	user_details = base()
	uid = current_user.username
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	dept = sorted(dept, key=itemgetter('dept'))
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	
	workcenter=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
	work_center = sorted(workcenter, key=itemgetter('work_center_desc'))

	if request.method == 'POST':
		form=request.form
		top=[]
		top=form['topval'].split(',')
		
		ZRGS=[]
		ZRGS1=[]
		#description7=form.getlist('description7')
		quantity7=form.getlist('quantity7')
		uom7=form.getlist('uom7')
		short7=form.getlist('shorttext')
		unit_value7=form.getlist('unit_value7')
		date7=form.getlist('date7')
		# st_loc=form.getlist('st_loc')
		procured4=form.getlist('procured4')
		image=request.files.getlist("file7[]")
		
		save_image=[]
		for img in image:
			if img.filename !="":
				a=file_save_gridfs(img,"application/pdf","com_prasset")
			else:
				a=""
			save_image.append(a)
		
		for i in range(0,len(quantity7)):
			array={}
			create_new=datetime.datetime.strptime(date7[i],"%d.%m.%Y")
			array={"PSTYP":"N","MATKL":"96","MEINS":uom7[i],"SHORT_TEXT":short7[i],"MENGE":decimal.Decimal(quantity7[i]),"VERPR":unit_value7[i],"EEIND":create_new}
			array1={"PSTYP":"N","MATKL":"96","MENGE":float(quantity7[i]),"VERPR":unit_value7[i],"EEIND":create_new,"file":save_image[i],"SHORT_TEXT":short7[i],"MEINS":uom7[i]}
			ZRGS.append(array)
			ZRGS1.append(array1)
		
		dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[0]},{"dept_chief_pid":1,"req_desc":1,"pur_grp":1,"_id":0})
		
		dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
		
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
		
		#pr level permission check 
		pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
		logging.debug("pr_permission result:==%s",str(pr_permission))
		if pr_permission==None:
			data={}
			msg="Permission for your plant has not been setup."
			a=[{"check":"1","msg":msg}]
			data['report']=a
			return jsonify(data)
		levels=[]
		dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})

		if "1" in pr_permission["level"]:
			levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
		if "2" in pr_permission["level"]:
			if "1" in pr_permission["level"]:
				levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
			else:
				 levels.append({"a_status":"current","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
		if "3" in pr_permission["level"]:
			if "1" in pr_permission["level"] or "2" in pr_permission["level"]: 
				levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})
			else:
				levels.append({"a_status":"current","a_id":plant_pid1[0]['username'],"a_time":"None"})

		prname1=get_employee_name(current_user.username)
		prname=dept_chief_pid[0]['req_desc']

		data={}
		a=[]
		#this is used for sending  components  array plus header level arguments to sap and saving it to database 

		sap_login=get_sap_user_credentials()
		try:
			conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
			result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT="A",P_ARBPL=top[2],P_ASSET=top[1],P_AUART="ZRAP",P_WERKS=plant_pid[0]['plant_id1'],P_EKORG="1000",P_EKGRP=dept_chief_pid[0]['pur_grp'],P_PRNAME=prname,IT_PRCRT=ZRGS,P_SIMULATION="X")
			
			conn.close()
			close_sap_user_credentials(sap_login['_id'])
			if result['IT_PMCRET'][0]["TYPE"]=='E':			
				# flash(result['IT_PMCRET'][0]["MESSAGE"],'alert-danger')
				a=[{"check":"1","msg":result['IT_PMCRET'][0]["MESSAGE"]}]
				data['report']=a
				return jsonify(data)
			else:
				prarray={"status":"applied","Assignment_category":"A","functionlocation":"","equipment":"","costcenter":"","wbs_element":"","network":"","assetcode":top[1],"PR_dept":top[0],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service":"","approval_levels":levels,"P_ARBPL":top[2],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"P_Datetime":top[4]}
				prarray['generalPr']='0'
				prarray['history']=[{"level1":{"component":quantity7,"service":[]},"level2":"","level3":"","level4":""}]
				save_collection("prcreation",prarray)
				msg="You have raised PR Asset Code "+top[4]+" and it is pending for approval at HOD of " +top[3]+" Department"
				# flash(msg,'alert-success')
				msg1=prname1+"has raised PR Asset Code "+top[4]+" and it is pending for your approval" 
				notification(uid,msg)
				notification(dept_chief_pid1[0]['username'],msg1)
				a=[{"check":"0","msg":msg}]
				data['report']=a
				return jsonify(data)
		except:
			close_sap_user_credentials(sap_login['_id'])
			flash("Unable to connect Sap user Credential","alert-danger")

		#return redirect(url_for('zenpmpr.prasset'))
	return render_template('asset.html',user_details=user_details,dept=dept,work_center=work_center)

# delete line item from code 
@zen_pmpr.route('/dellineitem', methods=['GET', 'POST'])
@login_required
def  dellineitem():
	form=request.form
	c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(form['prid'])})
	key=form['key']
	i=0
	for x in xrange(0,len(c_pr1[key])):
		if 'disable' not in c_pr1[key][x]:
			i+=1
	if i==1:
		return "1"
	else:
		row=int(form['row'])
		c_pr1[key][row]['disable']="1"
		c_pr1[key][row]['reason']=form["reason"]
		update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{key:c_pr1[key]}})
		return "0"
#this method is usefull for Adding line item to the already raised pr which is waiting for Approval.
#only the Current Approver can add the line item to the PR.
@zen_pmpr.route('/add_line_item/', methods=['GET', 'POST'])
@login_required
def add_line_item():
	user_details = base()
	logging.info('welcome to add line item')
	uid = current_user.username
	mtype=find_dist_in_collection("materialmaster","mtype")
	mtype.sort()
	form=request.form
	r_id=form['rec_id']
	global record_id
	record_id=r_id
	c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(form['rec_id'])})
	

	if c_pr1['equipment']!="" and c_pr1['costcenter']!="":
		return render_template('NEW_FUNC_EQUP_ADD.html',user_details=user_details,mtype=mtype)

	elif c_pr1['functionlocation']!="" and c_pr1['costcenter']!="":
		return render_template('NEW_FUNC_EQUP_ADD.html',user_details=user_details,mtype=mtype)

	elif c_pr1['costcenter']!="":
		return render_template('NEW_COST_CENTER_ADD.html',user_details=user_details,mtype=mtype)

	else:
		return render_template('NEW_GENERAL_PR_NONE.html',user_details=user_details,mtype=mtype)


#This method is usefull to Add LineItem to the General PR (None,Func,Equip)
#This method is usefull to Add LineItem to the General PR (None,Func,Equip)
@zen_pmpr.route('/adding_line_item/', methods=['GET', 'POST'])
@login_required
def adding_line_item():
	c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(record_id)})
	if request.method == 'POST':
				form=request.form
				ZRGS=[]
				ZRSV=[]
				ZRGS1=[]
				ZRSV1=[]
				prarray={}
				type1=form.getlist("type1")
				description2=form.getlist("description2")
				material1=form.getlist("material1")
				service_material1=form.getlist("service_material1")
				quantity1=form.getlist('quantity1')
				uom1=form.getlist('uom1')
				unit_value1=form.getlist('unit_value1')
				date1=form.getlist('date1')
				just1=form.getlist('just1')
				poheader1=form.getlist('poheader1')
				specify1=form.getlist('specify1')
				procured1=form.getlist('procured1')
				image=request.files.getlist("file1[]")
				
				save_image=[]
				for img in image:
					if img.filename !="":
						a=file_save_gridfs(img,"application/pdf","com_generalpmorder")
					else:
						a=""
					save_image.append(a)


				for i in range(0,len(type1)):
					if type1[i]=="ZRGS":
						mat_grp=find_and_filter('materialmaster',{"material":material1[i]},{"matl_group":1,"material_desc":1,"_id":0}) 
						c_len=len(c_pr1['component'])
						for c_i in range(0,c_len):
							if c_pr1['component'][c_i]['MATKL']==mat_grp[0]['matl_group'] and c_pr1['component'][c_i]['MATNR']==material1[i]:
								message="Same Material"+" "+str(c_pr1['component'][c_i]['MATNR']) +" "+"Already Exists for following PR "+" "+c_pr1["P_Datetime"]+" "+"Please add New Material"
								flash(message,'alert-danger')
								return redirect(url_for('zenpmpr.prapproval'))	
					else:
						# s_len=len(c_pr1['service'])
						# for s_i in range(0,s_len):
						# 	if c_pr1['service'][s_i]['MATKL']==service_material1[i] :
						# 		message="Same Material "+" "+c_pr1['service'][s_i]['MATKL'] +" "+"Already Exists for following PR "+" "+c_pr1["P_Datetime"]+" "+"Please add New Material"
						# 		flash(message,'alert-danger')
						# 		return redirect(url_for('zenpmpr.prapproval'))
						pass


				for i in range(0,len(type1)):
					array={}
					array1={}
					if date1[i]=="":
						new_d=datetime.datetime.now()
						up_d=datetime.datetime.strftime(new_d,"%d.%m.%Y")
						create_new=datetime.datetime.strptime(up_d,"%d.%m.%Y")
					else:
						create_new=datetime.datetime.strptime(date1[i],"%d.%m.%Y")
					if type1[i]=="ZRGS":
						mat_grp=find_and_filter('materialmaster',{"material":material1[i]},{"matl_group":1,"material_desc":1,"_id":0})
						
						if quantity1[i]=="":
							quantity1[i]=float(1)

						if uom1[i]=="":
							uom1[i]="NOS"

						if unit_value1[i]=="":
							unit_value1[i]="10"

						if just1[i]=="":
							just1[i]="please fill"

						if poheader1[i]=="":
							poheader1[i]="please fill"

						if specify1[i]=="":
							specify1[i]="please fill"

						array1={"PSTYP":"N","MAT_DESC":mat_grp[0]['material_desc'],"MATKL":mat_grp[0]['matl_group'],
						"MATNR":material1[i],"MENGE":float(quantity1[i]),"MEINS":uom1[i],"LGORT":0,"VERPR":unit_value1[i],
						"EEIND":create_new,"JUSTI":just1[i],"POHEAD":poheader1[i],"SPEC":specify1[i],"PROD":procured1[i]}
						try:
							array1['file']=save_image[i]	
						except:
							pass
						ZRGS1.append(array1)
						
					else:
						if quantity1[i]=="":
							quantity1[i]=float(1)

						if uom1[i]=="":
							uom1[i]="NOS"

						if unit_value1[i]=="":
							unit_value1[i]="10"

						if description2[i]=="":
							description2[i]="please fill"

						array1={"MATKL":service_material1[i],"SER_TXT":description2[i],"MENGE":float(quantity1[i]),
						"MEINS":uom1[i],"VERPR":unit_value1[i],"EEIND":create_new,"PROD":procured1[i]}
						try:
							array1['file']=save_image[i]	
						except:
							pass
						ZRSV1.append(array1)

				if c_pr1['equipment']!="" and c_pr1['costcenter']!="":

					db=dbconnect()
					if ZRGS1 and ZRSV1:
						for i in range(0,len(ZRGS1)):
								db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "component": ZRGS1[i]}})
						for j in range(0,len(ZRSV1)):
							db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "service":ZRSV1[j]}})

					elif ZRGS1:
						for i in range(0,len(ZRGS1)):
							db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "component": ZRGS1[i]}})
					else:
						for j in range(0,len(ZRSV1)):
							db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{"service":ZRSV1[j]}})


					message="Sucessfully Added Line item to the following PR "+" "+c_pr1["P_Datetime"]
					flash(message,'alert-success')
					return redirect(url_for('zenpmpr.prapproval'))

				elif c_pr1['functionlocation']!="" and c_pr1['costcenter']!="":
					db=dbconnect()
					if ZRGS1 and ZRSV1:
						for i in range(0,len(ZRGS1)):
							db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "component": ZRGS1[i]}})
						for j in range(0,len(ZRSV1)):
							db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "service":ZRSV1[j]}})
					elif ZRGS1:
						for i in range(0,len(ZRGS1)):
							db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "component": ZRGS1[i]}})
					else:
						for j in range(0,len(ZRSV1)):
							db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{"service":ZRSV1[j]}})

					message="Sucessfully Added Line item to the following PR "+" "+c_pr1["P_Datetime"]
					flash(message,'alert-success')
					return redirect(url_for('zenpmpr.prapproval'))	
				
						
				else:

					db=dbconnect()
					for i in range(0,len(ZRGS1)):
						db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "component": ZRGS1[i]}})
					message="Sucessfully Added Line item to the following PR "+" "+c_pr1["P_Datetime"]
					flash(message,'alert-success')
					return redirect(url_for('zenpmpr.prapproval'))
					
				
#this method is usefull to add the line item to the Cost Center
@zen_pmpr.route('/adding_line_item1/', methods=['GET', 'POST'])
@login_required
def adding_line_item1():
	c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(record_id)})
	if request.method == 'POST':
		form=request.form
		ZRGS=[]
		ZRSV=[]
		ZRGS1=[]
		ZRSV1=[]
		# this is for creating components  array 
		if form["date3"]:
			description3=form.getlist("description3")
			material3=form.getlist("material3")
			quantity3=form.getlist('quantity3')
			uom3=form.getlist('uom3')
			unit_value3=form.getlist('unit_value3')
			date3=form.getlist('date3')
			just3=form.getlist('just3')
			poheader3=form.getlist('poheader3')
			specify3=form.getlist('specify3')
			# st_loc=form.getlist('st_loc')
			procured3=form.getlist('procured3')
			image=request.files.getlist("file3[]")
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","com_costcenter")
				else:
					a=""
				save_image.append(a)

			for i in range(0,len(description3)):
				mat_grp=find_and_filter('materialmaster',{"material":material3[i]},{"matl_group":1,"material_desc":1,"_id":0})
				c_len=len(c_pr1['component'])
				for c_i in range(0,c_len):
					if c_pr1['component'][c_i]['MATKL']==mat_grp[0]['matl_group'] and c_pr1['component'][c_i]['MATNR']==material3[i]:
						message="Same Material "+" "+c_pr1['component'][c_i]['MATNR'] +" "+ "Already Exists for following PR "+" "+c_pr1["P_Datetime"]+" "+"Please add New Material"
						flash(message,'alert-danger')
						return redirect(url_for('zenpmpr.prapproval'))
		
			for i in range(0,len(description3)):
				array={}
				if date3[i]=="":
					new_d=datetime.datetime.now()
					up_d=datetime.datetime.strftime(new_d,"%d.%m.%Y")
					create_new=datetime.datetime.strptime(up_d,"%d.%m.%Y")
				else:
					create_new=datetime.datetime.strptime(date3[i],"%d.%m.%Y")
				
				mat_grp=find_and_filter('materialmaster',{"material":material3[i]},{"matl_group":1,"material_desc":1,"_id":0})
				array={"PSTYP":"N","MATNR":"0000"+material3[i],"MATKL":mat_grp[0]['matl_group'],"MENGE":decimal.Decimal(quantity3[i]),"MEINS":uom3[i],"VERPR":unit_value3[i],"EEIND":create_new,"JUSTI":just3[i],"POHEAD":poheader3[i],"SPEC":specify3[i],"PROD":procured3[i]}
				
				if quantity3[i]=="":
					quantity3[i]=float(1)

				if uom3[i]=="":
					uom3[i]="NOS"

				if unit_value3[i]=="":
					unit_value3[i]="10"

				if just3[i]=="":
					just3[i]="please fill"

				if poheader3[i]=="":
					poheader3[i]="please fill"

				if specify3[i]=="":
					specify3[i]="please fill"

				array1={"PSTYP":"N","MAT_DESC":mat_grp[0]['material_desc'],"MATNR":material3[i],"MATKL":mat_grp[0]['matl_group'],
				"MENGE":float(quantity3[i]),"MEINS":uom3[i],"VERPR":unit_value3[i],"EEIND":create_new,"JUSTI":just3[i],
				"POHEAD":poheader3[i],"SPEC":specify3[i],"LGORT":0,"PROD":procured3[i]}
				try:
					array1['file']=save_image[i]	
				except:
					pass			
				ZRGS.append(array)
				ZRGS1.append(array1)
			
		else:
			
			service_material3=form.getlist("service_material3")
			description4=form.getlist("description4")
			quantity4=form.getlist('quantity4')
			uom4=form.getlist('uom4')
			unit_value4=form.getlist('unit_value4')
			date4=form.getlist('date4')
			procured4=form.getlist('procured4')
			image=request.files.getlist("file4[]")
			
			save_image=[]
			for img in image:
				if img.filename !="":
					a=file_save_gridfs(img,"application/pdf","service_costcenter")
				else:
					a=""
				save_image.append(a)

			for j in range(0,len(description4)):
				s_len=len(c_pr1['service'])
				for s_i in range(0,s_len):
					if c_pr1['service'][s_i]['MATKL']==service_material3[j] :
						message="Same Material "+" "+c_pr1['service'][s_i]['MATKL'] +" "+"Already Exists for following PR "+" "+c_pr1["P_Datetime"]+" "+"Please add New Material"
						flash(message,'alert-danger')
						return redirect(url_for('zenpmpr.prapproval'))

			for j in range(0,len(description4)):
				array={}
				if date4[j]=="":
					new_d=datetime.datetime.now()
					up_d=datetime.datetime.strftime(new_d,"%d.%m.%Y")
					create_new=datetime.datetime.strptime(up_d,"%d.%m.%Y")
				else:
					create_new=datetime.datetime.strptime(date4[j],"%d.%m.%Y")
					
				if description4[j]=="":
					description4[j]="please fill"


				if quantity4[j]=="":
					quantity4[j]=float(1)


				if uom4[j] == "":
					uom4[j]=="NOS"


				if unit_value4[j] =="":
					unit_value4[j]="10"



				array={"MATKL":service_material3[j],"SER_TXT":description4[j],"MENGE":float(quantity4[j]),"MEINS":uom4[j],"VERPR":unit_value4[j],"EEIND":create_new,"PROD":procured4[j]}
				array1={"MATKL":service_material3[j],"SER_TXT":description4[j],"MENGE":float(quantity4[j]),"MEINS":uom4[j],"VERPR":unit_value4[j],"EEIND":create_new,"PROD":procured4[j]}
				try:
					array1['file']=save_image[j] 	
				except:
					pass
				ZRSV.append(array)
				ZRSV1.append(array1)
			
		if len(ZRGS)!=0:
			if c_pr1['component']:
				db=dbconnect()
				for i in range(0,len(ZRGS1)):
					db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "component": ZRGS1[i]}})
				message="Sucessfully Added Line item to the following PR "+" "+c_pr1["P_Datetime"]
				flash(message,'alert-success')
				return redirect(url_for('zenpmpr.prapproval'))

			else:
				
				message="Only Service Allowed Because you are Adding Line item to Service"
				flash(message,'alert-danger')
				return redirect(url_for('zenpmpr.prapproval'))

		else:
			
			if c_pr1['service']:
				db=dbconnect()
				for j in range(0,len(ZRSV1)):
					db.prcreation.update({"_id":ObjectId(record_id)},{'$push':{ "service": ZRSV1[j]}})
				message="Sucessfully Added Line item to the following PR "+" "+c_pr1["P_Datetime"]
				flash(message,'alert-success')
				return redirect(url_for('zenpmpr.prapproval'))
			else:
				
				message="Only Component Allowed Because you are Adding Line item to Component"
				flash(message,'alert-danger')
				return redirect(url_for('zenpmpr.prapproval'))

#this is to give pr approvals 
@zen_pmpr.route('/bulk_prapproval/', methods=['GET', 'POST'])
@login_required
def bulk_prapproval():
	
	user_details=base()
	uid= current_user.username
	db=dbconnect()
	pr=[]
	search={'status':'applied'}
	date={}
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})

	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	dept = sorted(dept, key=itemgetter('dept'))
	new_department= str(request.args.get('pr_department'))
	new_func_loc = str(request.args.get('pr_fun_loc'))
	new_equipment=str(request.args.get('pr_equipment'))
	new_cost_center=str(request.args.get('pr_cost_center'))
	new_asset_code=str(request.args.get('pr_assetcode'))
	new_date1=str(request.args.get('pr_from_date'))
	new_date2=str(request.args.get('pr_to_date'))
	new_option = str(request.args.get('pr_option'))


	if new_department != 'None':
		search['PR_dept']=new_department

	if new_func_loc!= 'None':
		search['functionlocation']=new_func_loc
	
	if new_equipment!= 'None':
		search['equipment']=new_equipment

	if new_cost_center!= 'None':
		search['costcenter']=new_cost_center

	if new_asset_code!= 'None':
		search['assetcode']=new_asset_code

	if new_option =="comp":
		search['$and']=[{"component":{ '$not':{'$size': 0}}},{"component":{'$ne':"" }}]
		search['$or']=[{'service': {'$size': 0}}, {'service': ""}]
	
	if new_option =="ser":
		search['$and']=[{"service":{ '$not':{'$size': 0}}},{"service":{'$ne':"" }}]
		search['$or']=[{"component":{'$size': 0}},{"component":""}]
	if new_option =="both":
		search['$and']=[{"service":{ '$not':{'$size': 0}}},{"service":{'$ne':"" }},{"component":{ '$not':{'$size': 0}}},{"component":{'$ne':"" }}]
		
	if new_date1!='None':
		sdate=datetime.datetime.strptime(new_date1, "%d.%m.%Y")
		date['$gt']=sdate

	if new_date2!='None':
		edate=datetime.datetime.strptime(new_date2, "%d.%m.%Y")
		date['$lte']=edate+datetime.timedelta(days=1)
			
	if new_date1!= 'None' or new_date2!= 'None':
		search['pr_appliedon']=date	
	logging.debug("Search query :---%s",str(search))
	pr_data=db.prcreation.aggregate([{'$match' :search},
		{ '$unwind': '$approval_levels' },
         {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'},{'approval_levels.a_status':'waiting'}]}}])
	pr_data=list(pr_data)
	#pr_data=list(pr_data)
	#logging.debug("Search query :---%s",str(search))
	#pr_new=db.prcreation.aggregate([{'$match' : s1},
	#	{ '$unwind': '$approval_levels' },
	#	{'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'},{'approval_levels.a_status':'waiting'}]}}])
	if request.method == 'POST':
		form=request.form
		logging.debug("Posting User Form")
		
		if form['submit'] == "Approve":
			
			c_pr =find_and_filter('prcreation',{"_id":ObjectId(form['prid'])},{"_id":0,"approval_levels":1,"employee_id":1})
			c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(form['prid'])})
			pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
			count_a_levels = len(c_pr[0]['approval_levels'])
			for i in range(0,count_a_levels):
				if c_pr[0]['approval_levels'][i]['a_status'] == 'current':
					j=i
					employee_name =	get_employee_name(c_pr[0]['employee_id'])
			data={}
			a=[]
			newQuanityC=[]
			newQuanityS=[]
			j == count_a_levels-1
			#if approved by last approval levels
			#logging.debug("User Provided new quantity ===%s",newQuanity) 
			if 'generalPr' in c_pr1:
				if c_pr1['generalPr']=='1':
					if len(c_pr1['component'])!=0:
						newQuanityC=form['qC'].split(',')
						newspecC=form['specC'].split(',')
						newjustifyC=form['justifyC'].split(',')
						newdateC=form['dateC'].split(',')
						newunitC=form['unitC'].split(',')
						newproC=form['proC'].split(',')

						for x in range(len(c_pr1['component'])):
							c_pr1['component'][x]["MENGE"]=newQuanityC[x]
							c_pr1['component'][x]["JUSTI"]=newjustifyC[x]
							c_pr1['component'][x]["SPEC"]=newspecC[x]
							c_pr1['component'][x]["EEIND"]=datetime.datetime.strptime(newdateC[x],"%d.%m.%Y")
							c_pr1['component'][x]["VERPR"]=newunitC[x]
							c_pr1['component'][x]["PROD"]=newproC[x]
							logging.info("#printing component in general pr going to db %s",str(c_pr1['component']))
							update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"component":c_pr1['component']}})
					if len(c_pr1['service'])!=0:
						newQuanityS=form['qS'].split(',')
						newdesS=form['desS'].split(',')
						newunitS=form['unitS'].split(',')
						newdateS=form['dateS'].split(',')
						newproS=form['proS'].split(',')
						for x in range(len(c_pr1['service'])):
							c_pr1['service'][x]["MENGE"]=newQuanityS[x]
							c_pr1['service'][x]["SER_TXT"]=newdesS[x]
							c_pr1['service'][x]["VERPR"]=newunitS[x]
							c_pr1['service'][x]["EEIND"]=datetime.datetime.strptime(newdateS[x],"%d.%m.%Y")
							c_pr1['service'][x]["PROD"]=newproS[x]
						logging.info("#printing services in generalPr going to db %s",str(c_pr1['service']))
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"service":c_pr1['service']}})
				else:
					if len(c_pr1['component'])!=0:
						logging.debug("Form for pr approval %s",form)
						newQuanityC=form['qC'].split(',')
						newdateC=form['dateC'].split(',')
						newunitC=form['unitC'].split(',')
							
						if c_pr1["Assignment_category"]!="A":
							newspecC=form['specC'].split(',')
							newjustifyC=form['justifyC'].split(',')
							newproC=form['proC'].split(',')
						for x in range(len(c_pr1['component'])):
							c_pr1['component'][x]["MENGE"]=newQuanityC[x]
							c_pr1['component'][x]["EEIND"]=datetime.datetime.strptime(newdateC[x],"%d.%m.%Y")
							c_pr1['component'][x]["VERPR"]=newunitC[x]
								
							if c_pr1["Assignment_category"]!="A":
								c_pr1['component'][x]["JUSTI"]=newjustifyC[x]
								c_pr1['component'][x]["SPEC"]=newspecC[x]
								c_pr1['component'][x]["PROD"]=newproC[x]
						logging.info("#printing component in normalPr going to db %s",str(c_pr1['component']))
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"component":c_pr1['component']}})
					else:
						newQuanityS=form['qS'].split(',')
						newdesS=form['desS'].split(',')
						newunitS=form['unitS'].split(',')
						newdateS=form['dateS'].split(',')
						newproS=form['proS'].split(',')
						for x in range(len(c_pr1['service'])):
							c_pr1['service'][x]["MENGE"]=newQuanityS[x]
							c_pr1['service'][x]["SER_TXT"]=newdesS[x]
							c_pr1['service'][x]["VERPR"]=newunitS[x]
							c_pr1['service'][x]["EEIND"]=datetime.datetime.strptime(newdateS[x],"%d.%m.%Y")
							c_pr1['service'][x]["PROD"]=newproS[x]
						logging.info("#printing services in  normalPr  going to db %s",str(c_pr1['service']))
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"service":c_pr1['service']}})
	 		logging.info("Welcome to PR Approval")

			if j == count_a_levels-1:
				logging.info("Welcome to PR Final Approval login to sap")
				component=[]
				service=[]
				sap_login=get_sap_user_credentials()
				try:
					conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])				
					if c_pr1["Assignment_category"]=="F":
						if 'generalPr' in c_pr1:
							if c_pr1['generalPr']=='1':
								if len(c_pr1['component'])!=0:
									for x in range(len(c_pr1['component'])):
										if 'disable' not in c_pr1['component'][x]:
											try:
												del c_pr1['component'][x]["file"]
											except:
												pass
											del c_pr1['component'][x]["MAT_DESC"]
											c_pr1['component'][x]["LGORT"]=""
											component.append(c_pr1['component'][x])
								if len(c_pr1['service'])!=0:
									for y in range(len(c_pr1['service'])):
										if 'disable' not in c_pr1['service'][y]:
											try:
												del c_pr1['service'][y]["file"]
											except:
												pass
											service.append(c_pr1['service'][y])
								if len(c_pr1['component'])!=0 and len(c_pr1['service'])==0:
									IM_STEUSValue="PM01"
								else:
									IM_STEUSValue="PM02"
								logging.debug("component FUNC general pr going to sap approval---- %s",str(component))
								logging.debug("service FUNC general pr going to sap approval---- %s",str(service))
								result = conn.call('Z_MZMM01_PMPR_PMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_TPLNR=c_pr1["functionlocation"],IM_EQUNR=c_pr1["equipment"],IM_KOSTL=c_pr1["costcenter"],IM_AUART="PM04",IM_STEUS=IM_STEUSValue,IM_ARBPL=c_pr1["P_ARBPL"],IM_EKORG="1000",IM_EKGRP=c_pr1["P_EKGRP"],IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1["P_PRNAME"],IM_OBJID="00000000",IT_PRSER=service,IM_SIMULATION=" ",IM_DESCR=c_pr1["IM_DESCR"],IT_PRCRT=component)
								

							else:
								if len(c_pr1['component'])!=0:
									for x in range(len(c_pr1['component'])):
										if 'disable' not in c_pr1['component'][x]:
											try:
												del c_pr1['component'][x]["file"]
											except:
												pass
											del c_pr1['component'][x]["MAT_DESC"]
											c_pr1['component'][x]["LGORT"]=""
											component.append(c_pr1['component'][x])
									logging.debug("component going to sap FUNC pr approval---- %s",str(component))
									
									result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_TPLNR=c_pr1["functionlocation"],P_EQUNR=c_pr1["equipment"],P_KOSTL=c_pr1["costcenter"],P_AUART="PM04",P_EKORG="1000",P_EKGRP=c_pr1["P_EKGRP"],P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1["P_PRNAME"],IT_PRCRT=component,P_STEUS="PM01",P_SIMULATION="  ",P_DESCR=c_pr1["P_DESCR"],P_OBJID="00000000")
									
								else:
									for y in range(len(c_pr1['service'])):
										if 'disable' not in c_pr1['service'][y]:
											try:
												del c_pr1['service'][y]["file"]
											except:
												pass
											service.append(c_pr1['service'][y])
									logging.debug("service going to sap FUNC pr approval---- %s",str(service))
									
									result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_TPLNR=c_pr1["functionlocation"],P_EQUNR=c_pr1["equipment"],P_KOSTL=c_pr1["costcenter"],P_AUART="PM04",P_EKORG="1000",P_EKGRP=c_pr1["P_EKGRP"],P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1["P_PRNAME"],IT_PRSER=service,P_STEUS="PM02",P_SIMULATION=" ",P_DESCR=c_pr1["P_DESCR"],P_OBJID="00000000")
									
					elif c_pr1["Assignment_category"]=="K":
						if 'generalPr' in c_pr1:
							if c_pr1['generalPr']=='1':
								if len(c_pr1['component'])!=0:
									for x in range(len(c_pr1['component'])):
										if 'disable' not in c_pr1['component'][x]:
											try:
												del c_pr1['component'][x]["file"]
											except:
												pass
											del c_pr1['component'][x]["MAT_DESC"]
											c_pr1['component'][x]["LGORT"]=""
											component.append(c_pr1['component'][x])
								if len(c_pr1['service'])!=0:
									for y in range(len(c_pr1['service'])):
										if 'disable' not in c_pr1['service'][y]:
											try:
												del c_pr1['service'][y]["file"]
											except:
												pass
											service.append(c_pr1['service'][y])
								if len(c_pr1['component'])!=0 and len(c_pr1['service'])==0:
									IM_AUARTValue="ZRGS"
								else:
									IM_AUARTValue="ZRSV"
								logging.debug("component COST general pr going to sap approval---- %s",str(component))
								logging.debug("service COST general pr going to sap approval---- %s",str(service))
								result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_KOSTL=c_pr1["costcenter"],IM_AUART=IM_AUARTValue,IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IT_PRSER=service,IM_SIMULATION="",IT_PRCRT=component,IM_DESCR=c_pr1['IM_DESCR'])
		
							else:	
								if len(c_pr1['component'])!=0:
									for x in range(len(c_pr1['component'])):
										if 'disable' not in c_pr1['component'][x]:
											try:
												del c_pr1['component'][x]["file"]
											except:
												pass
											del c_pr1['component'][x]["MAT_DESC"]
											c_pr1['component'][x]["LGORT"]=""
											component.append(c_pr1['component'][x])
									logging.debug("component going to sap COST pr approval---- %s",str(component))
									result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_KOSTL=c_pr1["costcenter"],IM_AUART="ZRGS",IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IT_PRCRT=component,IM_SIMULATION="",IM_DESCR=c_pr1['IM_DESCR'])
									
								else:
									for y in range(len(c_pr1['service'])):
										if 'disable' not in c_pr1['service'][y]:
											try:
												del c_pr1['service'][y]["file"]
											except:
												pass
											service.append(c_pr1['service'][y])
									logging.debug("service going to sap COST pr approval---- %s",str(service))	
									
									result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_KOSTL=c_pr1["costcenter"],IM_AUART="ZRSV",IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IT_PRSER=service,IM_SIMULATION="",IM_DESCR=c_pr1['IM_DESCR'])
									
					elif c_pr1["Assignment_category"]=="Q" or c_pr1["Assignment_category"]=="N":
						if len(c_pr1['component'])!=0:
							for x in range(len(c_pr1['component'])):
								if 'disable' not in c_pr1['component'][x]:
									try:
										del c_pr1['component'][x]["file"]
									except:
										pass
									del c_pr1['component'][x]["MAT_DESC"]
									c_pr1['component'][x]["LGORT"]=""
									component.append(c_pr1['component'][x])
							
							result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_WBS=c_pr1['wbs_element'],P_NTW=c_pr1['network'],P_AUART="ZRGS",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRCRT=component,P_SIMULATION=" ")
						else:
							for y in range(len(c_pr1['service'])):
								if 'disable' not in c_pr1['service'][y]:
									try:
										del c_pr1['service'][y]["file"]
									except:
										pass
									service.append(c_pr1['service'][y])
							
							result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_WBS=c_pr1['wbs_element'],P_NTW=c_pr1['network'],P_AUART="ZRSV",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRSER=service,P_SIMULATION=" ")
					elif c_pr1["Assignment_category"]=="A":
						for x in range(len(c_pr1['component'])):
							if 'disable' not in c_pr1['component'][x]:
								try:
									del c_pr1['component'][x]["file"]
								except:
									pass
								component.append(c_pr1['component'][x])
						logging.debug("component going to sap ASSET pr approval---- %s",str(component))
						
						result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_ASSET=c_pr1["assetcode"],P_AUART="ZRAP",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRCRT=component,P_SIMULATION=" ")
					else:
						if len(c_pr1['component'])!=0:
							for x in range(len(c_pr1['component'])):
								if 'disable' not in c_pr1['component'][x]:
									try:
										del c_pr1['component'][x]["file"]
									except:
										pass
									del c_pr1['component'][x]["MAT_DESC"]
									c_pr1['component'][x]["LGORT"]=""
									component.append(c_pr1['component'][x])
						if len(c_pr1['service'])!=0:
							for x in range(len(c_pr1['service'])):
								if 'disable' not in c_pr1['service'][y]:
									try:
										del c_pr1['service'][y]["file"]
									except:
										pass
									service.append(c_pr1['service'][y])

						if len(c_pr1['component'])!=0 and len(c_pr1['service'])==0:
							IM_AUARTValue="ZRGS"
						else:
							IM_AUARTValue="ZRSV"	
						logging.debug("component EMPTY general pr going to sap approval---- %s",str(component))
						logging.debug("service EMPTY general pr going to sap approval---- %s",str(service))
						
						result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=" ",IM_KOSTL="",IM_WBS="",IM_NTW="",IM_ASSET="",IM_AUART=IM_AUARTValue,IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IT_PRSER=service,IM_SIMULATION=" ",IM_DESCR=c_pr1["IM_DESCR"],IT_PRCRT=component)
						

					logging.debug("PR Approval Final result:==%s",str(result))
					conn.close()
					close_sap_user_credentials(sap_login['_id'])
					msgerr=""
					msgsuc=""
					for x in range(0,len(result['IT_PMCRET'])):
						if result['IT_PMCRET'][x]["TYPE"]=='E':
							msgerr=msgerr+result['IT_PMCRET'][x]["MESSAGE"]
								
						else:
							msgsuc=msgsuc+result['IT_PMCRET'][x]["MESSAGE"]

					if msgerr !="":
						flash(msgerr,'alert-danger')
					else:
						arry3={"approval_levels."+str(j)+".a_status":"approved","status":"approved","sap":"1"}
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry3})
						if 'generalPr' in c_pr1:
							if c_pr1['generalPr']=='1':
								arry4={"P_AUFNR":result["EX_AUFNR"],"P_CBANFN":result["EX_CBANFN"],"P_OBANFN":result["EX_OBANFN"]}
								update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry4})
							else:	
								arry4={"P_AUFNR":result["EX_AUFNR"],"P_CBANFN":result["EX_CBANFN"],"P_OBANFN":result["EX_OBANFN"]}
								update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry4})
									
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level4":{"component":newQuanityC,"service":newQuanityS}}})
						
						msg="You have finally approved "+c_pr1["P_Datetime"]
						flash(msg,'alert-success')
						msg1=""
						msg2=""
						if count_a_levels==1:
							if "1" in pr_permission["level"]:
								msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by HOD "
								msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by HOD"
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
							elif "2" in pr_permission["level"]:
								msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Store Head "
								msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Store Head"
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
							else:
								msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Plant Head "
								msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
								
						elif count_a_levels==2:
							if pr_permission["level"][1]=="2":
								msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Store Head "
								msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Store Head"
							if pr_permission["level"][1]=="3":
								msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Plant Head "
								msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
							if msg2 !="":
								if "1" in pr_permission["level"]:
									notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
								if "2" in pr_permission["level"]:
									if pr_permission["level"][0]=="2":
										notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
									else:
										notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
								if "3" in pr_permission["level"]:
									notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
									
						else:
							msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Plant Head "
							msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
							notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
							notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
							notification(c_pr[0]['approval_levels'][2]['a_id'],msg2)

						notification(c_pr[0]['employee_id'],msg1)
				except Exception as e:
					logging.debug("PR Approval Exception:==%s",str(e))
					close_sap_user_credentials(sap_login['_id'])
					flash("Unable to connect Sap user Credential","alert-danger")
						
			else:
				#if approved by any other  approval levels employee
				arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
					"approval_levels."+str(j+1)+".a_time":"None"}
				update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry4})
				if j==0:
					if "1" in pr_permission["level"]:
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level2":{"component":newQuanityC,"service":newQuanityS}}})
						if "2" in pr_permission["level"]:
							msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Store Head"
							msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by HOD and waiting for Store head approval"
						else:
							msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Plant Head"
							msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by Store Head and waiting for Plant head approval"
						msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by HOD and waiting for your approval"
					elif "2" in pr_permission["level"]:
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level3":{"component":newQuanityC,"service":newQuanityS}}})
						msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Plant Head"
						msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by Store Head and waiting for Plant head approval"
						msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by Store Head and waiting for your approval"
				else:
					update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level3":{"component":newQuanityC,"service":newQuanityS}}})
					msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Plant Head"
					msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by Store Head and waiting for Plant head approval"
					msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by Store Head and waiting for your approval"

				flash(msg,'alert-success')
				notification(c_pr[0]['employee_id'],msg1)
				notification(c_pr[0]['approval_levels'][j+1]['a_id'],msg2)
				return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
				pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))			
			
		elif form['submit'] == 'Reject':
			c_pr =find_and_filter('prcreation',{"_id":ObjectId(form['prid'])},{"_id":0,"approval_levels":1,"employee_id":1})
			c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(form['prid'])})
			pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
			count_a_levels = len(c_pr[0]['approval_levels'])
			for i in range(0,count_a_levels):
				if c_pr[0]['approval_levels'][i]['a_status'] == 'current':
					j=i
					employee_name =	get_employee_name(c_pr[0]['employee_id'])
			data={}
			a=[]
			#if reject by approval employee
			arry2={"approval_levels."+str(j)+".a_status":"rejected","status":"rejected"}
			update_coll('prcreation',{'_id':ObjectId(form['prid'])},{'$set': arry2})
			msg="You have Rejected "+c_pr1["P_Datetime"]
			if j==0:
				if "1" in pr_permission["level"]:
					msg1="Your raised PR "+c_pr1["P_Datetime"]+" is rejected by HOD"
					msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by HOD"
				elif "2" in pr_permission["level"]:
					msg1="Your raised PR "+c_pr1["P_Datetime"]+" is rejected by Store Head"
					msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Store Head"
				else:
					msg1="Your raised PR "+c_pr1["P_Datetime"]+" is rejected by Plant Head"
					msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Plant Head"
				notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
			elif j==1:
				if "2" in pr_permission["level"] and pr_permission["level"][0]!="2":
					msg1="Your raised PR "+c_pr1["P_Datetime"]+" is rejected by Store Head"
					msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Store Head"
				else:
					msg1="Your raised PR "+c_pr1["P_Datetime"]+" is rejected by Plant Head"
					msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Plant Head"
				notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
				notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
			else:
				msg1="Your raised PR "+c_pr1["P_Datetime"]+" is rejected by Plant Head"
				msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Plant Head"
				notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
				notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
				notification(c_pr[0]['approval_levels'][2]['a_id'],msg2)
			flash(msg,'alert-success')
			notification(c_pr[0]['employee_id'],msg1)
			return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
			pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))


		elif form['submit'] == 'Save':
			c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(form['prid'])})
			
			a=[]
			newQuanityC=[]
			newQuanityS=[]
			
			if 'generalPr' in c_pr1:
				if c_pr1['generalPr']=='1':
					if len(c_pr1['component'])!=0:
						newQuanityC=form['qCC'].split(',')
						newspecC=form['specCC'].split(',')
						newjustifyC=form['justifyCC'].split(',')
						newdateC=form['dateComp'].split(',')
						newunitC=form['unitCC'].split(',')
						newproC=form['proCC'].split(',')

						for x in range(len(c_pr1['component'])):
							if newQuanityC[x]=="" or newjustifyC[x]=="" or newspecC[x]=="" or newunitC[x]=="" or newproC[x]=="" or newdateC[x]=="":
								message="Please fill all the Fields in"+" "+c_pr1['P_Datetime']
								flash(message,'alert-danger')
								return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
								pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))	
							else:
								c_pr1['component'][x]["MENGE"]=newQuanityC[x]
								c_pr1['component'][x]["JUSTI"]=newjustifyC[x]
								c_pr1['component'][x]["SPEC"]=newspecC[x]
								c_pr1['component'][x]["EEIND"]=datetime.datetime.strptime(newdateC[x],"%d.%m.%Y")
								c_pr1['component'][x]["VERPR"]=newunitC[x]
								c_pr1['component'][x]["PROD"]=newproC[x]
						logging.info("#printing component in general pr going to db %s",str(c_pr1['component']))
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"component":c_pr1['component']}})
					if len(c_pr1['service'])!=0:
						newQuanityS=form['qSS'].split(',')
						newdesS=form['desSS'].split(',')
						newunitS=form['unitSS'].split(',')
						newdateS=form['dateServ'].split(',')
						newproS=form['proSS'].split(',')
						for x in range(len(c_pr1['service'])):
							if newQuanityS[x]=="" or newdesS[x]=="" or newunitS[x]=="" or newproS[x]=="" or newdateS=="":
			
								message="Please fill all the Fields in"+" "+c_pr1['P_Datetime']
								flash(message,'alert-danger')
								return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
								pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))
							else:
								c_pr1['service'][x]["MENGE"]=newQuanityS[x]
								c_pr1['service'][x]["SER_TXT"]=newdesS[x]
								c_pr1['service'][x]["VERPR"]=newunitS[x]
								c_pr1['service'][x]["EEIND"]=datetime.datetime.strptime(newdateS[x],"%d.%m.%Y")
								c_pr1['service'][x]["PROD"]=newproS[x]
						logging.info("#printing services in generalPr going to db %s",str(c_pr1['service']))
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"service":c_pr1['service']}})
				else:
					if len(c_pr1['component'])!=0:
						logging.debug("Form for pr approval %s",form)
						newQuanityC=form['qCC'].split(',')
						newdateC=form['dateComp'].split(',')
						newunitC=form['unitCC'].split(',')
							
						if c_pr1["Assignment_category"]!="A":
							newspecC=form['specCC'].split(',')
							newjustifyC=form['justifyCC'].split(',')
							newproC=form['proCC'].split(',')
						for x in range(len(c_pr1['component'])):
							if newQuanityC[x]=="" or newjustifyC[x]=="" or newspecC[x]=="" or newunitC[x]=="" or newproC[x]=="" or newdateC=="":
								message="Please fill all the Fields in"+" "+c_pr1['P_Datetime']
								flash(message,'alert-danger')
								return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
								pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))	
							else:

								c_pr1['component'][x]["MENGE"]=newQuanityC[x]
								c_pr1['component'][x]["EEIND"]=datetime.datetime.strptime(newdateC[x],"%d.%m.%Y")
								c_pr1['component'][x]["VERPR"]=newunitC[x]
									
								if c_pr1["Assignment_category"]!="A":
									c_pr1['component'][x]["JUSTI"]=newjustifyC[x]
									c_pr1['component'][x]["SPEC"]=newspecC[x]
									c_pr1['component'][x]["PROD"]=newproC[x]
						logging.info("#printing component in normalPr going to db %s",str(c_pr1['component']))
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"component":c_pr1['component']}})
					else:
						newQuanityS=form['qSS'].split(',')
						newdesS=form['desSS'].split(',')
						newunitS=form['unitSS'].split(',')
						newdateS=form['dateServ'].split(',')
						newproS=form['proSS'].split(',')
						for x in range(len(c_pr1['service'])):
							if newQuanityS[x]=="" or newdesS[x]=="" or newunitS[x]=="" or newproS[x]=="" or newdateS=="":
								
								message="Please fill all the Fields in"+" "+c_pr1['P_Datetime']
								flash(message,'alert-danger')
								return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
								pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))
							else:
								c_pr1['service'][x]["MENGE"]=newQuanityS[x]
								c_pr1['service'][x]["SER_TXT"]=newdesS[x]
								c_pr1['service'][x]["VERPR"]=newunitS[x]
								c_pr1['service'][x]["EEIND"]=datetime.datetime.strptime(newdateS[x],"%d.%m.%Y")
								c_pr1['service'][x]["PROD"]=newproS[x]
					
						update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"service":c_pr1['service']}})
						
			msg="You have Successfully saved the changes for "+c_pr1["P_Datetime"]
			flash(msg,'alert-success')
			return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
			pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))			
	 		
			
			
		else:
			logging.info("Welcome to Bulk PR Approval")
			pr_ids = form.getlist('prid_s')
			for pid in range(0,len(pr_ids)):
				c_pr =find_and_filter('prcreation',{"_id":ObjectId(pr_ids[pid])},{"_id":0,"approval_levels":1,"employee_id":1})
				c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(pr_ids[pid])})
				pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
				count_a_levels = len(c_pr[0]['approval_levels'])
				for i in range(0,count_a_levels):
					if c_pr[0]['approval_levels'][i]['a_status'] == 'current':
						j=i
						employee_name =	get_employee_name(c_pr[0]['employee_id'])
				
				if j == count_a_levels-1:
					logging.info("Welcome to PR Final Approval login to sap")
					component=[]
					service=[]
					sap_login=get_sap_user_credentials()
					try:
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])				
						if c_pr1["Assignment_category"]=="F":
							if 'generalPr' in c_pr1:
								if c_pr1['generalPr']=='1':
									if len(c_pr1['component'])!=0:
										for x in range(len(c_pr1['component'])):
											if 'disable' not in c_pr1['component'][x]:
												try:
													del c_pr1['component'][x]["file"]
												except:
													pass
												del c_pr1['component'][x]["MAT_DESC"]
												c_pr1['component'][x]["LGORT"]=""
												component.append(c_pr1['component'][x])
									if len(c_pr1['service'])!=0:
										for y in range(len(c_pr1['service'])):
											if 'disable' not in c_pr1['service'][y]:
												try:
													del c_pr1['service'][y]["file"]
												except:
													pass
												service.append(c_pr1['service'][y])
									if len(c_pr1['component'])!=0 and len(c_pr1['service'])==0:
										IM_STEUSValue="PM01"
									else:
										IM_STEUSValue="PM02"
									logging.debug("component FUNC general pr going to sap approval---- %s",str(component))
									logging.debug("service FUNC general pr going to sap approval---- %s",str(service))
									result = conn.call('Z_MZMM01_PMPR_PMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_TPLNR=c_pr1["functionlocation"],IM_EQUNR=c_pr1["equipment"],IM_KOSTL=c_pr1["costcenter"],IM_AUART="PM04",IM_STEUS=IM_STEUSValue,IM_ARBPL=c_pr1["P_ARBPL"],IM_EKORG="1000",IM_EKGRP=c_pr1["P_EKGRP"],IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1["P_PRNAME"],IM_OBJID="00000000",IT_PRSER=service,IM_SIMULATION=" ",IM_DESCR=c_pr1["IM_DESCR"],IT_PRCRT=component)

								else:
									if len(c_pr1['component'])!=0:
										for x in range(len(c_pr1['component'])):
											if 'disable' not in c_pr1['component'][x]:
												try:
													del c_pr1['component'][x]["file"]
												except:
													pass
												del c_pr1['component'][x]["MAT_DESC"]
												c_pr1['component'][x]["LGORT"]=""
												component.append(c_pr1['component'][x])
										logging.debug("component going to sap FUNC pr approval---- %s",str(component))
										result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_TPLNR=c_pr1["functionlocation"],P_EQUNR=c_pr1["equipment"],P_KOSTL=c_pr1["costcenter"],P_AUART="PM04",P_EKORG="1000",P_EKGRP=c_pr1["P_EKGRP"],P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1["P_PRNAME"],IT_PRCRT=component,P_STEUS="PM01",P_SIMULATION="  ",P_DESCR=c_pr1["P_DESCR"],P_OBJID="00000000")
									else:
										for y in range(len(c_pr1['service'])):
											if 'disable' not in c_pr1['service'][y]:
												try:
													del c_pr1['service'][y]["file"]
												except:
													pass
												service.append(c_pr1['service'][y])
										logging.debug("service going to sap FUNC pr approval---- %s",str(service))
										result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_TPLNR=c_pr1["functionlocation"],P_EQUNR=c_pr1["equipment"],P_KOSTL=c_pr1["costcenter"],P_AUART="PM04",P_EKORG="1000",P_EKGRP=c_pr1["P_EKGRP"],P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1["P_PRNAME"],IT_PRSER=service,P_STEUS="PM02",P_SIMULATION=" ",P_DESCR=c_pr1["P_DESCR"],P_OBJID="00000000")
						elif c_pr1["Assignment_category"]=="K":
							if 'generalPr' in c_pr1:
								if c_pr1['generalPr']=='1':
									if len(c_pr1['component'])!=0:
										for x in range(len(c_pr1['component'])):
											if 'disable' not in c_pr1['component'][x]:
												try:
													del c_pr1['component'][x]["file"]
												except:
													pass
												del c_pr1['component'][x]["MAT_DESC"]
												c_pr1['component'][x]["LGORT"]=""
												component.append(c_pr1['component'][x])
									if len(c_pr1['service'])!=0:
										for y in range(len(c_pr1['service'])):
											if 'disable' not in c_pr1['service'][y]:
												try:
													del c_pr1['service'][y]["file"]
												except:
													pass
												service.append(c_pr1['service'][y])
									if len(c_pr1['component'])!=0 and len(c_pr1['service'])==0:
										IM_AUARTValue="ZRGS"
									else:
										IM_AUARTValue="ZRSV"
									logging.debug("component COST general pr going to sap approval---- %s",str(component))
									logging.debug("service COST general pr going to sap approval---- %s",str(service))
									result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_KOSTL=c_pr1["costcenter"],IM_AUART=IM_AUARTValue,IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IT_PRSER=service,IM_SIMULATION=" ",IT_PRCRT=component)
			
								else:	
									if len(c_pr1['component'])!=0:
										for x in range(len(c_pr1['component'])):
											if 'disable' not in c_pr1['component'][x]:
												try:
													del c_pr1['component'][x]["file"]
												except:
													pass
												del c_pr1['component'][x]["MAT_DESC"]
												c_pr1['component'][x]["LGORT"]=""
												component.append(c_pr1['component'][x])
										logging.debug("component going to sap COST pr approval---- %s",str(component))
										#result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_KOSTL=c_pr1["costcenter"],P_AUART="ZRGS",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRCRT=component,P_SIMULATION=" ")
										
										result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_KOSTL=c_pr1["costcenter"],IM_AUART="ZRGS",IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IT_PRCRT=component,IM_SIMULATION="",IM_DESCR=c_pr1['IM_DESCR'])
									else:
										for y in range(len(c_pr1['service'])):
											if 'disable' not in c_pr1['service'][y]:
												try:
													del c_pr1['service'][y]["file"]
												except:
													pass
												service.append(c_pr1['service'][y])
										logging.debug("service going to sap COST pr approval---- %s",str(service))	
										#result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_KOSTL=c_pr1["costcenter"],P_AUART="ZRSV",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRSER=service,P_SIMULATION=" ")
										
										result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=c_pr1["Assignment_category"],IM_KOSTL=c_pr1["costcenter"],IM_AUART="ZRSV",IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IT_PRSER=service,IM_SIMULATION="",IM_DESCR=c_pr1['IM_DESCR'])
										
						elif c_pr1["Assignment_category"]=="Q" or c_pr1["Assignment_category"]=="N":
							if len(c_pr1['component'])!=0:
								for x in range(len(c_pr1['component'])):
									if 'disable' not in c_pr1['component'][x]:
										try:
											del c_pr1['component'][x]["file"]
										except:
											pass
										del c_pr1['component'][x]["MAT_DESC"]
										c_pr1['component'][x]["LGORT"]=""
										component.append(c_pr1['component'][x])
								result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_WBS=c_pr1['wbs_element'],P_NTW=c_pr1['network'],P_AUART="ZRGS",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRCRT=component,P_SIMULATION=" ")
							else:
								for y in range(len(c_pr1['service'])):
									if 'disable' not in c_pr1['service'][y]:
										try:
											del c_pr1['service'][y]["file"]
										except:
											pass
										service.append(c_pr1['service'][y])
								result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_WBS=c_pr1['wbs_element'],P_NTW=c_pr1['network'],P_AUART="ZRSV",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRSER=service,P_SIMULATION=" ")
						elif c_pr1["Assignment_category"]=="A":
							for x in range(len(c_pr1['component'])):
								if 'disable' not in c_pr1['component'][x]:
									try:
										del c_pr1['component'][x]["file"]
									except:
										pass
									component.append(c_pr1['component'][x])
							logging.debug("component going to sap ASSET pr approval---- %s",str(component))
							result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1["Assignment_category"],P_ARBPL=c_pr1["P_ARBPL"],P_ASSET=c_pr1["assetcode"],P_AUART="ZRAP",P_WERKS=c_pr1["P_WERKS"],P_PRNAME=c_pr1['P_PRNAME'],P_EKORG="1000",P_EKGRP=c_pr1['P_EKGRP'],IT_PRCRT=component,P_SIMULATION=" ")
						else:
							if len(c_pr1['component'])!=0:
								for x in range(len(c_pr1['component'])):
									if 'disable' not in c_pr1['component'][x]:
										try:
											del c_pr1['component'][x]["file"]
										except:
											pass
										del c_pr1['component'][x]["MAT_DESC"]
										c_pr1['component'][x]["LGORT"]=""
										component.append(c_pr1['component'][x])
							if len(c_pr1['service'])!=0:
								for x in range(len(c_pr1['service'])):
									if 'disable' not in c_pr1['service'][y]:
										try:
											del c_pr1['service'][y]["file"]
										except:
											pass
										service.append(c_pr1['service'][y])

							if len(c_pr1['component'])!=0 and len(c_pr1['service'])==0:
								IM_AUARTValue="ZRGS"
							else:
								IM_AUARTValue="ZRSV"	
							logging.debug("component EMPTY general pr going to sap approval---- %s",str(component))
							logging.debug("service EMPTY general pr going to sap approval---- %s",str(service))
							result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=" ",IM_KOSTL="",IM_WBS="",IM_NTW="",IM_ASSET="",IM_AUART=IM_AUARTValue,IM_EKORG="1000",IM_EKGRP=c_pr1['P_EKGRP'],IM_WERKS=c_pr1["P_WERKS"],IM_PRNAME=c_pr1['P_PRNAME'],IT_PRSER=service,IM_SIMULATION=" ",IM_DESCR=c_pr1["IM_DESCR"],IT_PRCRT=component)

						

						logging.debug("PR Approval Final result:==%s",str(result))
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
						msgerr=""
						msgsuc=""
						for x in range(0,len(result['IT_PMCRET'])):
							if result['IT_PMCRET'][x]["TYPE"]=='E':
								msgerr=msgerr+result['IT_PMCRET'][x]["MESSAGE"]
									
							else:
								msgsuc=msgsuc+result['IT_PMCRET'][x]["MESSAGE"]

						if msgerr !="":
							flash(msgerr,'alert-danger')
						else:
							arry3={"approval_levels."+str(j)+".a_status":"approved","status":"approved","sap":"1"}
							update_coll('prcreation',{'_id':ObjectId(pr_ids[pid])},{'$set': arry3})
					
							if 'generalPr' in c_pr1:
								if c_pr1['generalPr']=='1':
									arry4={"P_AUFNR":result["EX_AUFNR"],"P_CBANFN":result["EX_CBANFN"],"P_OBANFN":result["EX_OBANFN"]}
									update_coll('prcreation',{'_id':ObjectId(pr_ids[pid])},{'$set': arry4})
								else:	
									arry4={"P_AUFNR":result["EX_AUFNR"],"P_CBANFN":result["EX_CBANFN"],"EX_OBANFN":result["EX_OBANFN"]}
									update_coll('prcreation',{'_id':ObjectId(pr_ids[pid])},{'$set': arry4})
										
							
							msg="You have finally approved "+c_pr1["P_Datetime"]
							flash(msg,'alert-success')
							msg1=""
							msg2=""
							if count_a_levels==1:
								if "1" in pr_permission["level"]:
									msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by HOD "
									msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by HOD"
									notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
								elif "2" in pr_permission["level"]:
									msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Store Head "
									msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Store Head"
									notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
								else:
									msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Plant Head "
									msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
									notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
									
							elif count_a_levels==2:
								if pr_permission["level"][1]=="2":
									msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Store Head "
									msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Store Head"
								if pr_permission["level"][1]=="3":
									msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Plant Head "
									msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
								if msg2 !="":
									if "1" in pr_permission["level"]:
										notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
									if "2" in pr_permission["level"]:
										if pr_permission["level"][0]=="2":
											notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
										else:
											notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
									if "3" in pr_permission["level"]:
										notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
										
							else:
								msg1="Your raised PR "+c_pr1["P_Datetime"]+"is finally approved by Plant Head "
								msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
								notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
								notification(c_pr[0]['approval_levels'][2]['a_id'],msg2)

							notification(c_pr[0]['employee_id'],msg1)
					except Exception as e:
						logging.debug("PR Approval Exception:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")
							
				else:
					#if approved by any other  approval levels employee
					arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
						"approval_levels."+str(j+1)+".a_time":"None"}
					update_coll('prcreation',{'_id':ObjectId(pr_ids[pid])},{'$set': arry4})
					if j==0:
						if "1" in pr_permission["level"]:
							#update_coll('prcreation',{'_id':ObjectId(pr_ids[pid])},{'$set':{"history."+str(0)+".level2":{"component":newQuanityC,"service":newQuanityS}}})
							if "2" in pr_permission["level"]:
								msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Store Head"
								msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by HOD and waiting for Store head approval"
							else:
								msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Plant Head"
								msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by Store Head and waiting for Plant head approval"
							msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by HOD and waiting for your approval"
						elif "2" in pr_permission["level"]:
							#update_coll('prcreation',{'_id':ObjectId()},{'$set':{"history."+str(0)+".level3":{"component":newQuanityC,"service":newQuanityS}}})
							msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Plant Head"
							msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by Store Head and waiting for Plant head approval"
							msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by Store Head and waiting for your approval"
					else:
						#update_coll('prcreation',{'_id':ObjectId(pr_ids[pid])},{'$set':{"history."+str(0)+".level3":{"component":newQuanityC,"service":newQuanityS}}})
						msg="You have approved "+c_pr1["P_Datetime"]+" and it is sent for approval to Plant Head"
						msg1="Your raised PR "+c_pr1["P_Datetime"]+" is approved by Store Head and waiting for Plant head approval"
						msg2="PR "+c_pr1["P_Datetime"]+" raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by Store Head and waiting for your approval"

					flash(msg,'alert-success')
					notification(c_pr[0]['employee_id'],msg1)
					notification(c_pr[0]['approval_levels'][j+1]['a_id'],msg2)

		return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=new_department,pr_fun_loc=new_func_loc,pr_cost_center=new_cost_center,pr_equipment=new_equipment,
			pr_assetcode=new_asset_code,pr_option=new_option,pr_from_date=new_date1,pr_to_date=new_date2))
	return render_template('NEW_PR_APPROVAL.html',user_details=user_details,dept=dept,pr=pr_data)
			
            


	


@zen_pmpr.route('/prapproval/', methods=['GET', 'POST'])
@login_required
def prapproval():
	# p =check_user_role_permissions('550fde51850d2d3861bd7adf2')
	# if p:
	user_details=base()
	uid= current_user.username
	db=dbconnect()
	pr=[]
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})

	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	dept = sorted(dept, key=itemgetter('dept'))
	pr_dept=None
	pr_fn_lc=None
	pr_eq= None
	pr_cc=None
	pr_ac=None
	pr_opt=None
	pr_date1=None
	pr_date2=None
	#employee_att = db.prcreation.aggregate([{'$match' : {'status': "applied"}},
	#{ '$unwind': '$approval_levels' },
    #        {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'},{'approval_levels.a_status':'waiting'}]}}])
	if request.method == 'POST':
		form=request.form

		if form['dept']!="select":
			pr_dept=form['dept']
					
		if form['function_location']!="":
			pr_fn_lc=form['function_location']

		if form['equipment']!="":
			pr_eq=form['equipment']

		if form['cost_center']!="":
			pr_cc=form['cost_center']

		if form['assetcode']!="":
			pr_ac=form['assetcode']

		if form['opt'] !="":
			pr_opt=form['opt']
		
		if form['date1']!="":
			pr_date1=form['date1']

		if form['date2']!="":
			
			pr_date2 = form['date2']
					
		
		return redirect(url_for('zenpmpr.bulk_prapproval',pr_department=pr_dept,pr_fun_loc=pr_fn_lc,pr_cost_center=pr_cc,pr_equipment=pr_eq,
			pr_assetcode=pr_ac,pr_option=pr_opt,pr_from_date=pr_date1,pr_to_date=pr_date2))
			
	return render_template('NEW_PR_APPROVAL_SEARCH.html',user_details=user_details,dept=dept)


#this is to create general pr creation
@zen_pmpr.route('/generalprcreations', methods=['GET', 'POST'])
@login_required
def generalprcreations():
	start_time=datetime.datetime.now()
	uid = current_user.username
	
	if current_user.username!="admin":
		user_details = base()
		if user_details["plant_id"]!="HY":
			logging.info('welcome to general pr creations')
			logging.info("Beggining time::"+str(datetime.datetime.now().time()))
			uid = current_user.username
			dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
			dept = sorted(dept, key=itemgetter('dept'))
			mtype=find_dist_in_collection("materialmaster","mtype")
			mtype.sort()
			plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
			
			workcenter=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
			work_center = sorted(workcenter, key=itemgetter('work_center_desc'))
			
			if request.method == 'POST':
				form=request.form
				logging.debug("General PR creation form provide:==%s",str(form))
				
				top=[]
				top=form['topval'].split(',')
				ZRGS=[]
				ZRGS1=[]
				ZRSV=[]
				ZRSV1=[]
				prarray={}
				type1=form.getlist("type1")
				# description1=form.getlist("description1")
				description2=form.getlist("description2")
				material1=form.getlist("material1")
				service_material1=form.getlist("service_material1")
				quantity1=form.getlist('quantity1')
				uom1=form.getlist('uom1')
				# st_loc=form.getlist('st_loc')
				unit_value1=form.getlist('unit_value1')
				date1=form.getlist('date1')
				just1=form.getlist('just1')
				poheader1=form.getlist('poheader1')
				specify1=form.getlist('specify1')
				procured1=form.getlist('procured1')
				
				logging.info("generalPr uom details-----%s",str(uom1))
				
				image=request.files.getlist("file1[]")
				
				save_image=[]
				for img in image:
					if img.filename !="":
						a=file_save_gridfs(img,"application/pdf","com_generalpmorder")
					else:
						a=""
					save_image.append(a)

				for i in range(0,len(type1)):
					array={}
					array1={}
					create_new=datetime.datetime.strptime(date1[i],"%d.%m.%Y")
					if type1[i]=="ZRGS":
						mat_grp=find_and_filter('materialmaster',{"material":material1[i]},{"matl_group":1,"material_desc":1,"_id":0})
						
						array={"PSTYP":"N","MATNR":material1[i],"MATKL":mat_grp[0]['matl_group'],"MENGE":decimal.Decimal(quantity1[i]),"MEINS":uom1[i],"VERPR":unit_value1[i],"EEIND":create_new,"JUSTI":just1[i],"POHEAD":poheader1[i],"SPEC":specify1[i],"PROD":procured1[i]}
						array1={"PSTYP":"N","MAT_DESC":mat_grp[0]['material_desc'],"MATKL":mat_grp[0]['matl_group'],"MATNR":material1[i],"MENGE":float(quantity1[i]),"MEINS":uom1[i],"LGORT":0,"VERPR":unit_value1[i],"EEIND":create_new,"JUSTI":just1[i],"POHEAD":poheader1[i],"SPEC":specify1[i],"PROD":procured1[i]}
						try:
							array1['file']=save_image[i]
						except:
							pass

						ZRGS.append(array)
						ZRGS1.append(array1)
					else:
						#Here Instead of "SERVICE" We are Sending Service_material Number
						array={"MATKL":service_material1[i],"SER_TXT":description2[i],"MENGE":float(quantity1[i]),"MEINS":uom1[i],"VERPR":unit_value1[i],"EEIND":create_new,"PROD":procured1[i]}
						array1={"MATKL":service_material1[i],"SER_TXT":description2[i],"MENGE":float(quantity1[i]),"MEINS":uom1[i],"VERPR":unit_value1[i],"EEIND":create_new,"PROD":procured1[i]}
						try:
							array1['file']=save_image[i]
						except:
							pass
						ZRSV.append(array)
						ZRSV1.append(array1)


				if len(ZRGS)!=0 and len(ZRSV)==0:
					IM_STEUSValue="PM01"
					IM_AUARTValue="ZRGS"
				else:
					IM_AUARTValue="ZRSV"
					IM_STEUSValue="PM02"

				logging.debug("top result top[4] is should be dept id :==%s",str(top[4]))

				dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[4]},{"dept_chief_pid":1,"req_desc":1,"pur_grp":1,"_id":0,"dept":1})
				
				dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
				
				plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
				

				#pr level permission check 
				pr_permission=find_one_in_collection('pr_permission',{"plant":plant_pid[0]['plant_id1']})
				logging.debug("pr_permission result:==%s",str(pr_permission))
				if pr_permission==None:
					data={}
					msg="Permission for your plant has not been setup."
					a=[{"check":"1","msg":msg}]
					data['report']=a
					return jsonify(data)
						
				levels=[]
				dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
				plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
				dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})

				if "1" in pr_permission["level"]:
					levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
				if "2" in pr_permission["level"]:
					if "1" in pr_permission["level"]:
						levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
					else:
						 levels.append({"a_status":"current","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
				if "3" in pr_permission["level"]:
					if "1" in pr_permission["level"] or "2" in pr_permission["level"]: 
						levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})
					else:
						levels.append({"a_status":"current","a_id":plant_pid1[0]['username'],"a_time":"None"})
				
				prname1=get_employee_name(dept_chief_pid1[0]['username'])
				prname=dept_chief_pid[0]['req_desc']

				if top[0]=="func":
					st_func=datetime.datetime.now()
				
					if len(ZRGS)!=0 and len(ZRSV)==0:
						data={}
						msg="Only Components  not allowed Against Functional.loc/Equipment"
						a=[{"check":"1","msg":msg}]
						data['report']=a
						return jsonify(data)
					sap_login=get_sap_user_credentials()
					try:			
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						result = conn.call('Z_MZMM01_PMPR_PMORDER',IM_ACAT="F",IM_TPLNR=top[1],IM_EQUNR="",IM_KOSTL=top[2],IM_WBS="",IM_NTW="",IM_ASSET="",IM_AUART="PM04",IM_STEUS=IM_STEUSValue,IM_ARBPL=top[3],IM_EKORG="1000",IM_EKGRP=dept_chief_pid[0]['pur_grp'],IM_WERKS=plant_pid[0]['plant_id1'],IM_PRNAME=prname,IM_OBJID="00000000",IT_PRSER=ZRSV,IM_SIMULATION="X",IM_DESCR=top[5],IT_PRCRT=ZRGS)
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
					except Exception as e:
						logging.debug("General PR creation Exception:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")
					
					logging.debug("General PR creation result:==%s",str(result))
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[2]},{"cost_center_desc":1,"_id":0})
					
					prarray={"status":"applied","Assignment_category":"F","functionlocation":top[1],"equipment":"","costcenter":top[2],"wbs_element":"","network":"","assetcode":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service": ZRSV1,"approval_levels":levels,"P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"IM_DESCR":top[5],"P_Datetime":top[6],"cost_center_desc":costcenterdesc[0]['cost_center_desc']}		
				elif top[0]=="equip":
					st_equip=datetime.datetime.now()
					if len(ZRGS)!=0 and len(ZRSV)==0:
						data={}
						msg="Only Components  not allowed aganist Functional.loc/Equiqment"
						a=[{"check":"1","msg":msg}]
						data['report']=a
						return jsonify(data)
					sap_login=get_sap_user_credentials()
					try:
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						result = conn.call('Z_MZMM01_PMPR_PMORDER',IM_ACAT="F",IM_TPLNR="",IM_EQUNR=top[1],IM_KOSTL=top[2],IM_WBS="",IM_NTW="",IM_ASSET="",IM_AUART="PM04",IM_STEUS=IM_STEUSValue,IM_ARBPL=top[3],IM_EKORG="1000",IM_EKGRP=dept_chief_pid[0]['pur_grp'],IM_WERKS=plant_pid[0]['plant_id1'],IM_PRNAME=prname,IM_OBJID="00000000",IT_PRSER=ZRSV,IM_SIMULATION="X",IM_DESCR=top[5],IT_PRCRT=ZRGS)
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
					except Exception as e:
						logging.debug("General PR creation Exception:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")


					logging.debug("General PR creation result:==%s",str(result))
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[2]},{"cost_center_desc":1,"_id":0})
					equip=find_and_filter('equipmentmaster',{"equipment_number":top[1]},{"functional_loc":1,"Desp_technical_object":1,"_id":0})
					
					prarray={"status":"applied","Assignment_category":"F","functionlocation":"","equip_funcloc":equip[0]['functional_loc'],"equipment":top[1],"costcenter":top[2],"wbs_element":"","network":"","assetcode":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service": ZRSV1,"approval_levels":levels,"P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"IM_DESCR":top[5],"P_Datetime":top[6],"cost_center_desc" :costcenterdesc[0]['cost_center_desc'],"equip_Desc":equip[0]['Desp_technical_object']}		
				elif top[0]=="cost":
					st_cost=datetime.datetime.now()
					sap_login=get_sap_user_credentials()
					try:
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT="K",IM_KOSTL=top[1],IM_WBS="",IM_NTW="",IM_ASSET="",IM_AUART=IM_AUARTValue,IM_EKORG="1000",IM_EKGRP=dept_chief_pid[0]['pur_grp'],IM_WERKS=plant_pid[0]['plant_id1'],IM_PRNAME=prname,IT_PRSER=ZRSV,IM_SIMULATION="X",IT_PRCRT=ZRGS)
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
					except Exception as e:
						logging.debug("General PR creation Exception:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")
					logging.debug("General PR creation result:==%s",str(result))
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[1]},{"cost_center_desc":1,"_id":0})
					
					prarray={"status":"applied","Assignment_category":"K","functionlocation":"","equipment":"","costcenter":top[1],"wbs_element":"","network":"","assetcode":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service": ZRSV1,"approval_levels":levels,"P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"IM_DESCR":top[5],"P_Datetime":top[6],"cost_center_desc":costcenterdesc[0]['cost_center_desc']}		

				
				else:
					st_none=datetime.datetime.now()
					sap_login=get_sap_user_credentials()
					try:
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						result = conn.call('Z_MZMM01_PMPR_MMORDER',IM_ACAT=" ",IM_KOSTL="",IM_WBS="",IM_NTW="",IM_ASSET="",IM_AUART=IM_AUARTValue,IM_EKORG="1000",IM_EKGRP=dept_chief_pid[0]['pur_grp'],IM_WERKS=plant_pid[0]['plant_id1'],IM_PRNAME=prname,IT_PRSER=ZRSV,IM_SIMULATION="X",IT_PRCRT=ZRGS)
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
					except Exception as e:
						logging.debug("General PR creation Exception:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")
					logging.debug("General PR creation result:==%s",str(result))
					
					prarray={"status":"applied","Assignment_category":"","functionlocation":"","equipment":"","costcenter":"","wbs_element":"","network":"","assetcode":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"component":ZRGS1 ,"service": ZRSV1,"approval_levels":levels,"P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"IM_DESCR":top[5],"P_Datetime":top[6]}		

			
				msgerr=""
				msgsuc=""
				for x in range(0,len(result['IT_PMCRET'])):
					if result['IT_PMCRET'][x]["TYPE"]=='E':
						msgerr=msgerr+result['IT_PMCRET'][x]["MESSAGE"]
					else:
						msgsuc=msgsuc+result['IT_PMCRET'][x]["MESSAGE"]

				if msgerr !="":
					data={}
					a=[{"check":"1","msg":msgerr}]
					data['report']=a
					return jsonify(data)
				else:
					
					prarray['generalPr']='1'
					save_collection("prcreation",prarray)
					
					msg= "You have raised General PR on "+top[6]+" and it is pending for approval at HOD of "+dept_chief_pid[0]['dept']+" Department"
					flash(msg,'alert-success')
					msg1=prname1+" has raised General PR on "+top[6]+" and it is pending for your approval" 
					notification(uid,msg)
					notification(dept_chief_pid1[0]['username'],msg1)
					
					logging.info("Ending time::"+str(datetime.datetime.now().time()))
					data={}
					a=[{"check":"0","msg":msg}]
					data['report']=a
					end_time=datetime.datetime.now()
					
					return jsonify(data)
				#return redirect(url_for('zenpmpr.generalprcreations'))
			return render_template('general_pmorder.html',user_details=user_details,dept=dept,mtype=mtype,work_center=work_center)
		else:
			return redirect(url_for('zenpmpr.index'))
	else:
		return redirect(url_for('zenpmpr.index'))
	
#this is to create miv creation 
# mivcreation enhancement for debuging profile
#this is to create miv creation 

@zen_pmpr.route('/miv_onbehalf_plant_details', methods=['GET', 'POST'])
def miv_onbehalf_plant_details():
	if request.method=='POST':
		form=request.form
		# print request.form,' ajax form displayed'
		dept=find_and_filter("departmentmaster",{"plant":form['data']},{'_id':0})
		plant_pid=find_and_filter('Plants',{"plant_id":form['data']},{"plant_id1":1,"_id":0})	
		workcenter=find_and_filter("workcentermaster",{"Plant":plant_pid[0]['plant_id1']},{'_id':0})
		plant_details=[dept,plant_pid,workcenter]
		return jsonify({'data':plant_details})

@zen_pmpr.route('/mivcreations', methods=['GET', 'POST'])
@login_required
def mivcreations():
	start_time=datetime.datetime.now()
	debug_flag=find_one_in_collection('miv_config',{'status':'debug'})
	plant_available=find_and_filter('Plants',{},{'_id':0,'plant_id1':1,'plant_id':1})
	if debug_flag:
		debug_flag=debug_flag['debug']['miv_creation']
	else:
		debug_flag="off"
	if current_user.username!="admin":
		user_details = base()
		if user_details["plant_id"]!="HY":
			user_plant=user_details["plant_id"]
			uid = current_user.username
			logging.info('Welcome to MIV Creation Plant_ID:'+" "+str(user_plant)+"  "+" User_ID:"+str(uid))
			dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
			dept = sorted(dept, key=itemgetter('dept'))
			mtype=find_dist_in_collection("materialmaster","mtype")
			mtype.sort()
			plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
			
			workcenter=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
			
			work_center = sorted(workcenter, key=itemgetter('work_center_desc'))
			if request.method == 'POST':
				form=request.form
				if debug_flag=='on':
					logging.info('MIV Creation Form '+ str(form)+' -- @ -- '+str(datetime.datetime.now()))
				top=[]
				top=form['topval'].split(',')
				miv=[]
				miv1=[]
				description=form.getlist('description1')
				material=form.getlist('material')
				st_loc=form.getlist('st_loc')
				uom=form.getlist('uom')
				quantity=form.getlist('quantity')
				specify=form.getlist('specify')
			
				for i in range(0,len(description)):
					array={}
					array1={}
					mat_grp=find_and_filter('materialmaster',{"material":material[i]},{"matl_group":1,"material_desc":1,"_id":0})
					create_new=datetime.datetime.now()
					array={"PSTYP":"L","MATNR":material[i],"MATKL":mat_grp[0]['matl_group'],"MEINS":uom[i],
					"MENGE":float(quantity[i]),"EEIND":create_new,"LGORT":st_loc[i],"SHORT_TEXT":specify[i]}
					array1={"PSTYP":"L","MAT_DESC":mat_grp[0]['material_desc'],"MATNR":material[i],"MATKL":mat_grp[0]['matl_group'],"MEINS":uom[i],"LGORT":st_loc[i],"MENGE":float(quantity[i]),"EEIND":create_new,"SHORT_TEXT":specify[i]}
					miv.append(array)
					miv1.append(array1)
				
				logging.debug("length of miv line items :==%s",str(len(miv)))
				#logging.debug("top result top[4] is should be dept id :==%s",str(top[4]))
				# print "top",top
				dept_chief_pid=find_and_filter('departmentmaster',{"dept_pid":top[4]},{"dept_chief_pid":1,"req_desc":1,"pur_grp":1,"_id":0,"dept":1})
				
				dept_store_pid=find_and_filter('departmentmaster',{"plant":user_details["plant_id"],"dept":"STORES"},{"dept_chief_pid":1,"_id":0})
				
				plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
				
				#miv level permission check 
				miv_permission=find_one_in_collection('miv_permission',{"plant":plant_pid[0]['plant_id1']})
				#logging.debug("miv_permission result:==%s",str(miv_permission))
				if miv_permission==None:
					data={}
					msg="Permission for your plant has not been setup."
					a=[{"check":"1","msg":msg}]
					data['report']=a
					return jsonify(data)

						
				levels=[]
				# print "Depet",dept_chief_pid
				dept_chief_pid1=find_and_filter('Userinfo',{"designation_id":dept_chief_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
				plant_pid1=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
				dept_store_pid1=find_and_filter('Userinfo',{"designation_id":dept_store_pid[0]['dept_chief_pid']},{"username":1,"_id":0})

				if "1" in miv_permission["level"]:
					levels.append({"a_status":"current","a_id":dept_chief_pid1[0]['username'],"a_time":"None"})
				if "2" in miv_permission["level"]:
					if "1" in miv_permission["level"]:
						levels.append({"a_status":"waiting","a_id":plant_pid1[0]['username'],"a_time":"None"})
					else:
						levels.append({"a_status":"current","a_id":plant_pid1[0]['username'],"a_time":"None"}) 
				if "3" in miv_permission["level"]:
					if "1" in miv_permission["level"] or "2" in miv_permission["level"]: 
						levels.append({"a_status":"waiting","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
					else:
						levels.append({"a_status":"current","a_id":dept_store_pid1[0]['username'],"a_time":"None"})
				
				prname1=get_employee_name(current_user.username)
				prname=dept_chief_pid[0]['req_desc']
				port_id = int(top[5])
				rand_num=random_with_N_digits(4)
				new_port_id=port_id + rand_num
				new_port_id=unicode(str(new_port_id), "utf-8")
				P_BUDAT=datetime.datetime.now()
				if top[0]=="func":
					stime_func=datetime.datetime.now()
					sap_login=get_sap_user_credentials()
					print sap_login
					IM_HEADER_FUNC = {"PORTALID":new_port_id,"ACAT":"F","TPLNR":top[1],"AUART":"PM04","STEUS":"PM01","ARBPL":top[3],"EKGRP":dept_chief_pid[0]['pur_grp'],"WERKS":plant_pid[0]['plant_id1'],"PRNAME":prname,"BUDAT":P_BUDAT,"REJECT":""}
					try:
						new_time=datetime.datetime.now()
						logging.debug("MIV For Functional Location Connecting to SAP")
						print "user fun",sap_login['user']
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						# conn = Connection(user='praveen', passwd='sappm2', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						print "Header",IM_HEADER_FUNC
						print "Items",miv
						result = conn.call('Z_MIV_CREATION',IM_HEADER=IM_HEADER_FUNC,IT_ITEMS=miv)
						conn.close()
						logging.info("Connection Closed and Time taken for Fetching Result from SAP :"+" "+str(datetime.datetime.now()-new_time))
						if debug_flag=='on':
							logging.info('Result From SAP Call Function Location '+ str(result)+' -- @ -- '+str(datetime.datetime.now()))
						close_sap_user_credentials(sap_login['_id'])
					except Exception,e:
						logging.debug("MIV creation error result:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")

					#top[2]
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[2]},{"cost_center_desc":1,"_id":0})
					cost_center_desc = costcenterdesc[0]['cost_center_desc']
					
					for x in range(len(result['IT_ITEMS'])):
						# miv1[x]['MBLNR']=result['IT_ITEMS'][x]['MBLNR']
						miv1[x]['MJAHR']=result['IT_ITEMS'][x]['MJAHR']
						miv1[x]['ZEILE']=result['IT_ITEMS'][x]['ZEILE']
						# miv1[x]['DMBTR']=result['IT_PRCRT'][x]['DMBTR']
					mivarray={"P_Datetime":new_port_id, "status":"submitted","functionlocation":top[1],"equipment":"","costcenter":top[2],"wbs_element":"","network":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"voucher":miv1,"approval_levels":levels,"P_ACAT":"F","P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"cost_center_desc":cost_center_desc,"cost_activity":"","cost_activity_desc":""}
					
				elif top[0]=="equip":
					stime_equip=datetime.datetime.now()
					#Equipment Header Inforamtion Sending through Dictionary
					IM_HEADER_EQUIP = {"PORTALID":new_port_id,"ACAT":"F","EQUNR":top[1],"AUART":"PM04","STEUS":"PM01","ARBPL":top[3],"EKGRP":dept_chief_pid[0]['pur_grp'],"WERKS":plant_pid[0]['plant_id1'],"PRNAME":prname,"BUDAT":P_BUDAT,"REJECT":""}
					
					sap_login=get_sap_user_credentials()
					try:
						stime_sap=datetime.datetime.now()
						logging.debug("MIV For Equipment Connecting to SAP")
						print "user equip",sap_login['user']
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						# conn = Connection(user='manasa', passwd='behappy', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						print "Header",IM_HEADER_EQUIP
						print "Items",miv
						result = conn.call('Z_MIV_CREATION',IM_HEADER=IM_HEADER_EQUIP,IT_ITEMS=miv)
						conn.close()
						logging.info("Connection Closed and Time taken for Fetching Result from SAP :"+" "+str(datetime.datetime.now()-stime_sap))
						close_sap_user_credentials(sap_login['_id'])
						if debug_flag=='on':
							logging.info('Result From SAP Call Equipment '+ str(result)+' -- @ -- '+str(datetime.datetime.now()))
					except Exception,e:
						logging.debug("MIV creation error result:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")

					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[2]},{"cost_center_desc":1,"_id":0})
					cost_center_desc = costcenterdesc[0]['cost_center_desc']
					
					equip=find_and_filter('equipmentmaster',{"equipment_number":top[1]},{"functional_loc":1,"Desp_technical_object":1,"_id":0})
					
					for x in range(len(result['IT_ITEMS'])):
						# miv1[x]['MBLNR']=result['IT_ITEMS'][x]['MBLNR']
						miv1[x]['MJAHR']=result['IT_ITEMS'][x]['MJAHR']
						miv1[x]['ZEILE']=result['IT_ITEMS'][x]['ZEILE']
						# miv1[x]['DMBTR']=result['IT_PRCRT'][x]['DMBTR']
					mivarray={"P_Datetime":new_port_id,"status":"submitted","functionlocation":"","equipment":top[1],"costcenter":top[2],"wbs_element":"","network":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"voucher":miv1,"approval_levels":levels,"P_ACAT":"F","P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"equip_funcloc":equip[0]["functional_loc"],"cost_center_desc":cost_center_desc,"eqp_desc":equip[0]["Desp_technical_object"],"cost_activity":"","cost_activity_desc":""}
					
				elif top[0]=="cost":
					stime_cost=datetime.datetime.now()
					print "cost",top[2]
					print top[6]
					#Cost Center Header Information Sending through Dictionary
					IM_HEADER_COST = {"PORTALID":new_port_id,"ACAT":"M","KOSTL":top[1],"LSTAR":top[2],"AUART":"PM04","STEUS":"PM01","ARBPL":top[3],"EKGRP":dept_chief_pid[0]['pur_grp'],"WERKS":plant_pid[0]['plant_id1'],"PRNAME":prname,"BUDAT":P_BUDAT,"REJECT":""}
					
					sap_login=get_sap_user_credentials()
					try:
						time_cost=datetime.datetime.now()
						logging.debug("MIV For CostCenter Connecting to SAP")
						print "user cost",sap_login['user']
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						# conn = Connection(user='manasa', passwd='behappy', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						print "Header",IM_HEADER_COST
						print "Items",miv
						result = conn.call('Z_MIV_CREATION',IM_HEADER=IM_HEADER_COST,IT_ITEMS=miv)
						conn.close()
						logging.info("Connection Closed and Time taken for Fetching Result from SAP :"+" "+str(datetime.datetime.now()-time_cost))
						close_sap_user_credentials(sap_login['_id'])
						if debug_flag=='on':
							logging.info('Result From SAP Call Costcenter '+ str(result)+' -- @ -- '+str(datetime.datetime.now()))

					except Exception,e:
						logging.debug("MIV creation error result:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")
					
					
					costcenterdesc=find_and_filter('costcentersmaster',{"cost_center":top[1]},{"cost_center_desc":1,"_id":0})
					cost_center_desc = costcenterdesc[0]['cost_center_desc']
					for x in range(len(result['IT_ITEMS'])):
						# miv1[x]['MBLNR']=result['IT_ITEMS'][x]['MBLNR']
						miv1[x]['MJAHR']=result['IT_ITEMS'][x]['MJAHR']
						miv1[x]['ZEILE']=result['IT_ITEMS'][x]['ZEILE']
						# miv1[x]['DMBTR']=result['IT_PRCRT'][x]['DMBTR']
					mivarray={"P_Datetime":new_port_id,"status":"submitted","functionlocation":"","equipment":"","costcenter":top[1],"wbs_element":"","network":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"voucher":miv1,"approval_levels":levels,"P_ACAT":"M","P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"cost_center_desc":cost_center_desc,"cost_activity":top[2],"cost_activity_desc":top[6]}
					logging.info("MIV Creation Sucessfull for costcenter")
					
				else:
					stime_none=datetime.datetime.now()
					#WBS Element Header Inforamtion Sending THrough Dictionary
					IM_HEADER = {"PORTALID":new_port_id,"ACAT":"M","WBS":top[1],"AUART":"PM04","STEUS":"PM01","ARBPL":top[3],"EKGRP":dept_chief_pid[0]['pur_grp'],"WERKS":plant_pid[0]['plant_id1'],"PRNAME":prname,"BUDAT":P_BUDAT,"REJECT":""}
					

					sap_login=get_sap_user_credentials()
					try:
						time_none=datetime.datetime.now()
						logging.debug("MIV For WBS Element Connecting to SAP")
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						# conn = Connection(user='manasa', passwd='behappy', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						result = conn.call('Z_MIV_CREATION',IM_HEADER=IM_HEADER,IT_ITEMS=miv)
						conn.close()
						logging.info("Connection Closed and Time taken for Fetching Result from SAP :"+" "+str(datetime.datetime.now()-time_none))
						close_sap_user_credentials(sap_login['_id'])
						if debug_flag=='on':
							logging.info('Result From SAP Call WBS Element '+ str(result)+' -- @ -- '+str(datetime.datetime.now()))					
						
					except Exception,e:
						logging.debug("MIV creation error result:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")
					
					#Insted of Getting Result from IT_PRCRT We are Getting it through IT_ITEMS
					for x in range(len(result['IT_ITEMS'])):
						# miv1[x]['MBLNR']=result['IT_ITEMS'][x]['MBLNR']
						miv1[x]['MJAHR']=result['IT_ITEMS'][x]['MJAHR']
						miv1[x]['ZEILE']=result['IT_ITEMS'][x]['ZEILE']
						# miv1[x]['DMBTR']=result['IT_PRCRT'][x]['DMBTR']
					mivarray={"P_Datetime":new_port_id,"status":"submitted","functionlocation":"","equipment":"","costcenter":"","wbs_element":top[1],"network":"","PR_dept":top[4],"employee_id":current_user.username,"employee_name":prname1,"pr_appliedon":datetime.datetime.now(),"voucher":miv1,"approval_levels":levels,"P_ACAT":"M","P_ARBPL":top[3],"P_EKGRP":dept_chief_pid[0]['pur_grp'],"P_WERKS":plant_pid[0]['plant_id1'],"P_PRNAME":prname,"short_desc":top[2],"cost_activity":"","cost_activity_desc":""}
					
				msgerr=""
				msgsuc=""				
				logging.info('Miv Creation Msg From SAP : ------- '+result["EX_MSG"])########checck for errors #2 msgs submitted succesfully ,or consumption already booked.				
				if msgerr !="":
					data={}
					a=[{"check":"1","msg":msgerr}]
					data['report']=a
					return jsonify(data)
				else:
					mivarray['history']=[{"level1":quantity,"level2":"","level3":"","level4":""}]
					mivarray['P_BUDAT']=P_BUDAT
					save_collection("mivcreation",mivarray)
					if debug_flag=='on':
						logging.info('Saving MIV To DataBase '+ str(mivarray)+' -- @ -- '+str(datetime.datetime.now()))	
					#if 3 Levels of Approval use 2740 msg1 else use 2741 msg1
					#msg="You have raised MIV and it is pending for approval at HOD of " +dept_chief_pid[0]['dept']+" Department. Document Reference Number is "+miv1[0]['MBLNR']
					msg="You have raised MIV and it is pending for approval at STORES of " +dept_chief_pid[0]['dept']+"portal id is : "+new_port_id				
					flash(msg,'alert-success')
					#if 3 Levels of Approval use 2743 msg1 else use 2744 msg1
					#msg1=prname1+"has raised MIV and it is pending for your approval" 
					msg1=prname1+"has raised MIV and it is pending for"+ " "+dept_store_pid1[0]['username']+" "+"Approval"
					notification(uid,msg)
					try:
						notification(dept_chief_pid1[0]['username'],msg1)
					except:
						logging.info('Department Chief PID of Plant :'+user_details["plant_id"]+' Is Not Available ,Please Consult The IT Team And Update For Created MIV To Be Approved.')
						flash('Department Chief PID of Plant :'+user_details["plant_id"]+' Is Not Available ,Please Consult The IT Team And Update For Created MIV To Be Approved.',"alert-danger")
					data={}
					a=[{"check":"0","msg":msg}]
					data['report']=a
					logging.info("Total Execution Time for MIV Creation IS :"+str(datetime.datetime.now()-start_time))
					return jsonify(data)
					
				
				#return redirect(url_for('zenpmpr.mivcreations'))
			logging.debug("Page loding time of MIV creation by user_id:"+" "+uid+" "+" is "+str(datetime.datetime.now()-start_time))
			return render_template('mivcreation.html',user_details=user_details,dept=dept,mtype=mtype,work_center=work_center,plant_available=plant_available)
		else:
			return redirect(url_for('zenpmpr.index'))
	else:
		return redirect(url_for('zenpmpr.index'))

# end of miv creation debuging profile


			

#this is for miv approval Search
@zen_pmpr.route('/mivapproval/', methods=['GET', 'POST'])
@login_required
def mivapproval():
	deptid=get_designation_id(current_user.username)
	p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
	q=find_one_in_collection("Plants",{"chief_pid":deptid})
	if p or q:
		user_details=base()
		uid = current_user.username
		dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
		dept = sorted(dept, key=itemgetter('dept'))
		miv_dept=None
		miv_date1=None
		miv_date2=None
		if request.method == 'POST':
			form=request.form
			f_dept=str(form['dept'])
			st_date=str(form['start_date'])
			ed_date=str(form['end_date'])

			if f_dept!="select":
				miv_dept=f_dept
						
			if st_date!="":
				miv_date1=st_date

			if ed_date!="":
				miv_date2 = ed_date

			return redirect(url_for('zenpmpr.new_mivapproval',miv_department=miv_dept,miv_from_date=miv_date1,miv_to_date=miv_date2))
			
	return render_template('MIV_APPROVAL.html',user_details=user_details,dept=dept)
	


# miv approval/rejection enhancement on 26_10_2016
@zen_pmpr.route('/new_mivapproval/', methods=['GET', 'POST'])
@login_required
def new_mivapproval():
	start_time=datetime.datetime.now()
	debug_flag=find_one_in_collection('miv_config',{'status':'debug'})
	if debug_flag:
		debug_flag=debug_flag['debug']['miv_approval']
	else:
		debug_flag="off"
	# p =check_user_role_permissions('550fde51850d2d3861bd7adf2')
	# if p:
	deptid=get_designation_id(current_user.username)
	p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
	q=find_one_in_collection("Plants",{"chief_pid":deptid})
	if p or q:
		user_details=base()
		uid = current_user.username
		user_plant=user_details["plant_id"]
		logging.info('Welcome to MIV Approval Plant_ID:'+" "+str(user_plant)+"  "+" User_ID:"+str(uid)+" ")
		search={'status':'applied'}
		date={}
		dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
		dept = sorted(dept, key=itemgetter('dept'))
		new_department= str(request.args.get('miv_department'))
		new_date1=str(request.args.get('miv_from_date'))
		new_date2=str(request.args.get('miv_to_date'))

		if new_department != 'None':
			search['PR_dept']=new_department


		if new_date1!='None':
			
			sdate=datetime.datetime.strptime(new_date1, "%d/%m/%Y")
			date['$gt']=sdate

		if new_date2!='None':
			
			edate=datetime.datetime.strptime(new_date2, "%d/%m/%Y")
			date['$lte']=edate+datetime.timedelta(days=1)
				
		if new_date1!= 'None' or new_date2!= 'None':
			search['pr_appliedon']=date

		logging.debug("Search query :---%s",str(search))
		db=dbconnect()
		employee_att=db.mivcreation.aggregate([{'$match' :search},
		{ '$unwind': '$approval_levels' },
         {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'},{'approval_levels.a_status':'waiting'}]}}])	
		employee_att=list(employee_att)
		
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
		miv_permission=find_one_in_collection('miv_permission',{"plant":plant_pid[0]['plant_id1']})
		if request.method == 'POST':
				form=request.form
				c_pr =find_and_filter('mivcreation',{"_id":ObjectId(form['prid'])},{"_id":0,"approval_levels":1,"employee_id":1})
				c_pr1=find_one_in_collection('mivcreation',{"_id":ObjectId(form['prid'])})
				count_a_levels = len(c_pr[0]['approval_levels'])
				if c_pr1['status']=='approved':
					if debug_flag=='on':
						logging.info('Order Is Already Approved '+'Employee Id : ('+uid+') -- @ -- '+str(datetime.datetime.now()))
					flash("The Order You Are Trying To Reject Has Already Been Approved ","alert-danger")
					return redirect(url_for('zenpmpr.new_mivapproval',miv_department=new_department,miv_from_date=new_date1,miv_to_date=new_date2))
				elif c_pr1['status']=='rejected':
					if debug_flag=='on':
						logging.info('Order Is Already Rejected '+'Employee Id : ('+uid+') -- @ -- '+str(datetime.datetime.now()))
					flash("The Order You Are Trying To Reject Has Already Been Rejected ","alert-danger")
					return redirect(url_for('zenpmpr.new_mivapproval',miv_department=new_department,miv_from_date=new_date1,miv_to_date=new_date2))
				elif c_pr1['status']=='inprocess':
					if debug_flag=='on':
						logging.info('Order Is Already Approved And In Inprocess '+'Employee Id : ('+uid+') -- @ -- '+str(datetime.datetime.now()))
					flash("Order Is Already Approved And In Inprocess ","alert-danger")
					return redirect(url_for('zenpmpr.new_mivapproval',miv_department=new_department,miv_from_date=new_date1,miv_to_date=new_date2))
				else:
					for i in range(0,count_a_levels):
						if c_pr[0]['approval_levels'][i]['a_status'] == 'current':
							j=i
							employee_name =	get_employee_name(c_pr[0]['employee_id'])
				P_BUDAT_DATE=datetime.datetime.strptime(str(form['dateC']).rstrip(),'%d.%m.%Y')
				P_BUDAT =datetime.datetime.combine(P_BUDAT_DATE, datetime.datetime.now().time())
				
				if form['submit']== 'Approve':				
					approving_time = datetime.datetime.now()
					logging.info('Approving MIV P_Datetime is:'+" "+str(c_pr1['P_Datetime']))
					#if approved by last approval level
					newQuanity=form['q'].split(',')
					# newdateC=form['dateC'].split(',')
					for x in range(len(c_pr1['voucher'])):
						c_pr1['voucher'][x]["MENGE"]=newQuanity[x]
						# c_pr1['voucher'][x]["P_BUDAT"]=newdateC[x]
					#logging.debug("P_BUDAT -->>%s",form['dateC'])
					P_BUDAT_DATE=datetime.datetime.strptime(str(form['dateC']).rstrip(),'%d.%m.%Y')
					P_BUDAT =datetime.datetime.combine(P_BUDAT_DATE, datetime.datetime.now().time())
					#logging.debug("P_BUDAT after converstion sending to sap %s",P_BUDAT)
					#logging.debug("datetime datetime ---> %s",datetime.datetime.now())
					update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{"$set":{"voucher":c_pr1['voucher'],"P_BUDAT":P_BUDAT}})
			
					if j == count_a_levels-1:
						for x in range(len(c_pr1['voucher'])):
							del c_pr1['voucher'][x]["MAT_DESC"]

							# del c_pr1['IT_PRCRT'][x]['DMBTR']
							
						sap_login=get_sap_user_credentials()
						try:
							app_start=datetime.datetime.now()
							if 'cost_activity' in c_pr1:
								cost_activity=c_pr1['cost_activity']
							else:
								cost_activity=''
							#Sending Header Inforamtion through IM_Header to SAP From MongoDB Database
							IM_HEADER={"PORTALID":c_pr1['P_Datetime'],"ACAT":c_pr1['P_ACAT'],"TPLNR":c_pr1['functionlocation'],"EQUNR":c_pr1['equipment'],"KOSTL":c_pr1['costcenter'],"LSTAR":cost_activity,'WBS':c_pr1['wbs_element'],'NTW':c_pr1['network'],'ARBPL':c_pr1['P_ARBPL'],'EKGRP':c_pr1['P_EKGRP'],'WERKS':c_pr1['P_WERKS'],'PRNAME':c_pr1['P_PRNAME'],"AUART":"PM04","STEUS":"PM01","BUDAT":P_BUDAT,"REJECT":""}
							conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
							#conn = Connection(user="manasa", passwd="be happy", ashost="192.168.18.16",sysnr="D01", gwserv="3301", client="150")
							#result = conn.call('Z_MZMM01_PMPR_MIV_CREATION',P_ACAT=c_pr1['P_ACAT'],P_TPLNR=c_pr1['functionlocation'],P_EQUNR=c_pr1['equipment'],P_KOSTL=c_pr1['costcenter'],P_WBS=c_pr1['wbs_element'],P_NTW=c_pr1['network'],P_ARBPL=c_pr1['P_ARBPL'],P_EKGRP=c_pr1['P_EKGRP'],P_WERKS=c_pr1['P_WERKS'],P_PRNAME=c_pr1['P_PRNAME'],P_SIMULATION=" ",IT_PRCRT=c_pr1['voucher'],P_AUART="PM04",P_STEUS="PM01",P_BUDAT=P_BUDAT)
							# New Function Module We are Using for Approval
							# conn = Connection(user='manasa', passwd='behappy', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
							result = conn.call('Z_MIV_CREATION',IM_HEADER=IM_HEADER,IT_ITEMS=c_pr1['voucher'])
							conn.close()
							gateway_user = sap_login['user']
							close_sap_user_credentials(sap_login['_id'])
							logging.info("Time taken for fetching result from sap after Approval is: "+str(datetime.datetime.now()-app_start))
							#logging.info("MIV final approval result Message from SAP:==%s",str(result["EX_MSG"]))
							

							#mblnr if already generated for 
							new_mblnr=result['EX_MBLNR_NEW'] #if already approved and not synced with batch job but sent for approval again returns a error/MBLNR
							msgsuc=result["EX_MSG"]
							if debug_flag=='on':
								logging.info('Response From SAP After Approval Request '+str(msgsuc)+' -- @ -- '+str(datetime.datetime.now()))

							if msgsuc == "":
								flash("Sorry Please try again",'alert-danger')
							else:
								arry3={"approval_levels."+str(j)+".a_status":"approved","status":"inprocess","sap":"1","gatewayuser":gateway_user,"approved_time":approving_time}
								update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{'$set': arry3})
								update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level4":newQuanity}})
								
								msg="You have approved. It will update doucment number in MIV History for approved MIV. Please chekck after 2min."
								logging.debug("Execution time for approving MIV Approval by "+uid+" is "+str(datetime.datetime.now()-start_time))
								flash(msg,'alert-success')
								msg1=""
								msg2=""
								if count_a_levels==1:
									if "1" in miv_permission["level"]:
										msg1="Your raised MIV is finally approved by HOD "
										msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by HOD"
										notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
									elif "2" in miv_permission["level"]:
										msg1="Your raised MIV is finally approved by Plant Head"
										msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
										notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
									else:
										msg1="Your raised MIV is finally approved by Store Head "
										msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Store Head"
										notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
									
								elif count_a_levels==2:
									if miv_permission["level"][1]=="2":
										msg1="Your raised MIV is finally approved by Plant Head"
										msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
									if miv_permission["level"][1]=="3":
										msg1="Your raised MIV is approved by Store Head. Please check in MIV History after 2min for document number. "
										msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Store Head"
									if msg2 !="":
										if "1" in miv_permission["level"]:
											notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
										if "2" in miv_permission["level"]:
											if miv_permission["level"][0]=="2":
												notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
											else:
												notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
										if "3" in miv_permission["level"]:
											notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
										
								else:
									msg1="Your raised MIV is approved by Store Head. Please check in MIV History after 2min for document number."
									msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by Store Head. Please check in MIV History after 2min for document number."
									notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
									notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
									notification(c_pr[0]['approval_levels'][2]['a_id'],msg2)

								notification(c_pr[0]['employee_id'],msg1)	
						except Exception,e:
							logging.debug("MIV final approval error:==%s",str(e))
							close_sap_user_credentials(sap_login['_id'])
							flash("Unable to connect Sap user Credential","alert-danger")
					else:
						#if approved by any other  approval levels employee
						arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
							"approval_levels."+str(j+1)+".a_time":"None"}
						update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{'$set': arry4})
						if debug_flag=='on':
							logging.info('Updating MIV In Database After Completion Of Approval Process -- @ -- '+str(datetime.datetime.now()))
						if j==0:
							if "1" in miv_permission["level"]:
								update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level2":newQuanity}})
								if "2" in miv_permission["level"]:
									msg="You have approved and it is sent for approval to Plant Head"
									msg1="Your raised MIV is approved by HOD and waiting for Plant head approval"
								else:
									msg="You have approved and it is sent for approval to Store Head"
									msg1="Your raised MIV is approved by Plant Head and waiting for Store head approval"
								msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by HOD and waiting for your approval"
							elif "2" in miv_permission["level"]:
								update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level3":newQuanity}})
								msg="You have approved and it is sent for approval to Store Head" 
								msg1="Your raised MIV is approved by Plant Head and waiting for Store head approval"
								msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by Plant Head and waiting for your approval"
						else:
							update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{'$set':{"history."+str(0)+".level3":newQuanity}})
							msg="You have approved and it is sent for approval to Store Head"
							msg1="Your raised MIV is approved by Plant Head and waiting for Store head approval"
							msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by Plant Head and waiting for your approval"

						flash(msg,'alert-success')
						notification(c_pr[0]['employee_id'],msg1)
						notification(c_pr[0]['approval_levels'][j+1]['a_id'],msg2)
						logging.debug("Execution Time for MIV Approval is:"+" "+str(datetime.datetime.now()-start_time))
						return redirect(url_for('zenpmpr.new_mivapproval',miv_department=new_department,miv_from_date=new_date1,miv_to_date=new_date2))
					
				elif form['submit'] == 'Reject':					
					logging.info('Rejecting MIV P_Datetime is:'+" "+str(c_pr1['P_Datetime']))
					start_time=datetime.datetime.now()
					for x in range(len(c_pr1['voucher'])):
						del c_pr1['voucher'][x]["MAT_DESC"]
						# del c_pr1['IT_PRCRT'][x]['DMBTR']
						c_pr1['voucher'][x]["MENGE"]=0
					
					sap_login=get_sap_user_credentials()
					try:
						rjct_start=datetime.datetime.now()
						if 'cost_activity' in c_pr1:
							cost_activity=c_pr1['cost_activity']
						else:
							cost_activity=''
						IM_HEADER={"PORTALID":c_pr1['P_Datetime'],"ACAT":c_pr1['P_ACAT'],"TPLNR":c_pr1['functionlocation'],"EQUNR":c_pr1['equipment'],"KOSTL":c_pr1['costcenter'],"LSTAR":cost_activity,'WBS':c_pr1['wbs_element'],'NTW':c_pr1['network'],'ARBPL':c_pr1['P_ARBPL'],'EKGRP':c_pr1['P_EKGRP'],'WERKS':c_pr1['P_WERKS'],'PRNAME':c_pr1['P_PRNAME'],"AUART":"PM04","STEUS":"PM01",'BUDAT':P_BUDAT,"REJECT":"X"}
						conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						# conn = Connection(user='manasa', passwd='behappy', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
						result = conn.call('Z_MIV_CREATION',IM_HEADER=IM_HEADER,IT_ITEMS=c_pr1['voucher'])
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
						logging.debug("Time taken for fetching rejection result is "+str(datetime.datetime.now()-rjct_start))
						
						msgsuc=result["EX_MSG"]
						if debug_flag=='on':
							logging.info('Response From SAP After Rejection Request '+str(msgsuc)+' -- @ -- '+str(datetime.datetime.now()))
						new_mblnr=result['EX_MBLNR_NEW']
						if msgsuc == "":
								flash("Sorry Please try again",'alert-danger')
						else:
							arry3={"approval_levels."+str(j)+".a_status":"rejected","status":"rejected","sap":"1"}
							update_coll('mivcreation',{'_id':ObjectId(form['prid'])},{'$set': arry3})
							if debug_flag=='on':
								logging.info('Updating MIV In Database After Completion Of Rejection Process -- @ -- '+str(datetime.datetime.now()))
							msg="You have Rejected it"
							if j==0:
								if "1" in miv_permission["level"]:
									msg1="Your raised MIV is rejected by HOD"
									msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by HOD"
								elif "2" in miv_permission["level"]:
									msg1="Your raised MIV is rejected by Plant Head"
									msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Plant Head"
								else:
									msg1="Your raised MIV is rejected by Store Head"
									msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Store Head"
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
							elif j==1:
								if "2" in miv_permission["level"] and miv_permission["level"][0]!="2":
									msg1="Your raised MIV is rejected by Plant Head"
									msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Plant Head"
								else:
									msg1="Your raised MIV is rejected by Store Head"
									msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Store Head"
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
								notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
							else:
								msg1="Your raised MIV is rejected by Store Head"
								msg2="MIV raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is rejected by Store Head"
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg2)
								notification(c_pr[0]['approval_levels'][1]['a_id'],msg2)
								notification(c_pr[0]['approval_levels'][2]['a_id'],msg2)
							flash(msg,'alert-success')
							notification(c_pr[0]['employee_id'],msg1)
							logging.debug("Rejecting MIV is Completed "+str(datetime.datetime.now()-start_time))
							return redirect(url_for('zenpmpr.new_mivapproval',miv_department=new_department,miv_from_date=new_date1,miv_to_date=new_date2))
					except Exception,e:
						logging.debug("MIV rejection error:==%s",str(e))
						close_sap_user_credentials(sap_login['_id'])
						flash("Unable to connect Sap user Credential","alert-danger")
				logging.debug("Execution time for MIV Approval/Rejection Function by User_ID:"+"  "+uid+" is "+"  "+str(datetime.datetime.now()-start_time))
				return redirect(url_for('zenpmpr.new_mivapproval',miv_department=new_department,miv_from_date=new_date1,miv_to_date=new_date2))
		st_flash=datetime.datetime.now()
		logging.debug("Page Loading time for MIV approval is "+str(datetime.datetime.now()-start_time))
		st_print=datetime.datetime.now()
		return render_template('NEW_MIV_APPROVAL.html',user_details=user_details,miv=employee_att,dept=dept)
	else:
		return redirect(url_for('zenpmpr.index'))

# end of miv approval/rejection enhancement on 26_10_2016

def SendSms(message,mobilenumber,uname):
	url = "http://www.smscountry.com/smscwebservice_bulk.aspx"
	values = {'user' : 'pcilhr',
	'passwd' : 'pcil@123',
	'message' : "Dear "+uname+','+message,
	'mobilenumber':mobilenumber,
	'mtype':'N',
	'DR':'Y'
	}

	data = urllib.urlencode(values)
	data = data.encode('utf-8')
	request = urllib2.Request(url,data)
	response = urllib2.urlopen(request)

@zen_pmpr.route('/miv_sync/')
def miv_syncr():
    print 'miv sync...........................'
    logging.debug("MIV synchronization Job is Started")
    portal_id= list()
    log_msg=''
    MSG=''
    debug_flag=find_one_in_collection('miv_config',{'status':'debug'})
    if debug_flag:
        debug_flag=debug_flag['debug']['miv_sync']
    else:
        debug_flag="off"
    # debug_flag="on"
    miv_under_process =find_in_collection('mivcreation',{"status" : "submitted",})
    miv_async_details_update=find_in_collection('mivcreation',{'$or':[{'status':'submitted'},{'status':'inprocess'}]})
    
    for docs in miv_async_details_update:
        if docs['status']=='submitted' and 'exclude' not in docs:
            portal_id.append({"PORTALID":docs['P_Datetime'],"STATUS":'C'})
            # logging.info({"PORTALID":docs['P_Datetime'],"STATUS":'C'})
        elif docs['status']=='inprocess' and 'exclude' not in docs:
            portal_id.append({"PORTALID":docs['P_Datetime'],"STATUS":'A'})
            if debug_flag=='on':
                # logging.info({"PORTALID":docs['P_Datetime'],"STATUS":'A'})
                logging.info('MIV_SYNC  -- @ -- '+str(datetime.datetime.now()))
        log_msg+=docs['P_Datetime']+" , "
    sap_login=get_sap_user_credentials()
   
    try:
        conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],
            sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
        # conn = Connection(user="nikhil.m", passwd="sap@123", ashost="192.168.18.16",sysnr="D01",gwserv="3301",client="150")
        result = conn.call('Z_MIV_SYNC',IT_SYNC=portal_id)
        conn.close()
        close_sap_user_credentials(sap_login['_id'])
    except Exception,e:
        logging.debug("MIV synchronization  error:==%s",str(e))
        close_sap_user_credentials(sap_login['_id'])
        flash("Unable to connect Sap user Credential","alert-danger")
    if debug_flag=='on':
        logging.debug("MIV synchronization Result:==%s",str(result['IT_SYNC']))
    # x=0
    for info in result['IT_SYNC']:
        if info['STATUS']=='C':
            if info['MBLNR_NEW']=="":
                if debug_flag=='on':
                    logging.debug("MBLNR for Portal_Id :"+info['PORTALID']+" is ---- Not Created.error : "+info['MSG'])
                update_collection('mivcreation',{"P_Datetime":info['PORTALID']},{'$set':{'MSG':info['MSG']}})
                continue
            else:
                c_pr1=find_one_in_collection('mivcreation',{"P_Datetime":info['PORTALID'],"status":"submitted"})
            if c_pr1:    
	            for x in range(0,len(c_pr1['voucher'])):
	                c_pr1['voucher'][x]['MBLNR']=info['MBLNR_NEW']
	                c_pr1['voucher'][x]['MJAHR']=info['MJAHR']
	            arry4={"voucher":c_pr1['voucher'],"status":"applied",'MSG':info['MSG']}
	            update_coll('mivcreation',{'P_Datetime':info['PORTALID'],"status" : "submitted"},{'$set': arry4})
	            if debug_flag=='on':
	                logging.debug("MBLNR for Portal_Id :"+info['PORTALID']+" is ---- "+info['MBLNR_NEW'])
        elif info['STATUS']=='A':
            if info['MBLNR_NEW']=="":
                if debug_flag=='on':
                    logging.debug("MBLNR_new for Portal_Id :"+info['PORTALID']+" is ---- Not Created.error : "+info['MSG'])
                update_collection('mivcreation',{"P_Datetime":info['PORTALID']},{'$set':{'MSG':info['MSG']}})
                continue
            else:
                c_pr2=find_one_in_collection('mivcreation',{"P_Datetime":info['PORTALID'],"status" : "inprocess"})
            if c_pr2:
                for x in range(0,len(c_pr2['voucher'])):
                    c_pr2['voucher'][x]['MBLNR_FINAL']=info['MBLNR_NEW']
                    c_pr2['voucher'][x]['MJAHR_FINAL']=info['MJAHR']              
                        
                arry5={"voucher":c_pr2['voucher'],"status":"approved"}
                update_coll('mivcreation',{'P_Datetime':info['PORTALID'],"status":"inprocess"},{'$set': arry5})
            if debug_flag=='on':
                logging.debug("MBLNR_new for Portal_Id :"+info['PORTALID']+" is ---- "+info['MBLNR_NEW'])
    	    # x+=1
    if debug_flag=='on':
        logging.debug("MIV synchronization Job is Completed.sent portal Id's : "+log_msg)
    else:
        logging.debug("MIV synchronization Job is Completed")
    return "successfully completed miv synchronization"


############# commented on 16 march 2017
# @zen_pmpr.route('/miv_sync/')
# def miv_syncr():
#     # print 'miv sync...........................'
#     logging.debug("MIV synchronization Job is Started")
#     portal_id= list()
#     log_msg=''
#     MSG=''
#     debug_flag=find_one_in_collection('miv_config',{'status':'debug'})
#     if debug_flag:
#         debug_flag=debug_flag['debug']['miv_sync']
#     else:
#         debug_flag="off"
#     # debug_flag="on"
#     miv_under_process =find_in_collection('mivcreation',{"status" : "submitted"})
#     miv_async_details_update=find_in_collection('mivcreation',{'$or':[{'status':'submitted'},{'status':'inprocess'}]})
    
#     for docs in miv_async_details_update:
#         if docs['status']=='submitted':
#             portal_id.append({"PORTALID":docs['P_Datetime'],"STATUS":'C'})
#             # logging.info({"PORTALID":docs['P_Datetime'],"STATUS":'C'})
#         elif docs['status']=='inprocess' and 'exclude' not in docs:
#             portal_id.append({"PORTALID":docs['P_Datetime'],"STATUS":'A'})
#             if debug_flag=='on':
#                 # logging.info({"PORTALID":docs['P_Datetime'],"STATUS":'A'})
#                 logging.info('MIV_SYNC  -- @ -- '+str(datetime.datetime.now()))
#         log_msg+=docs['P_Datetime']+" , "
#     sap_login=get_sap_user_credentials()
   
#     try:
#         conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],
#             sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
#         # conn = Connection(user="manasa", passwd="behappy@7890", ashost="192.168.18.16",sysnr="D01",gwserv="3301",client="150")
#         result = conn.call('Z_MIV_SYNC',IT_SYNC=portal_id)
#         conn.close()
#         close_sap_user_credentials(sap_login['_id'])
#     except Exception,e:
#         logging.debug("MIV synchronization  error:==%s",str(e))
#         close_sap_user_credentials(sap_login['_id'])
#         flash("Unable to connect Sap user Credential","alert-danger")
#     if debug_flag=='on':
#         logging.debug("MIV synchronization Result:==%s",str(result['IT_SYNC']))
#     # x=0
#     # print "reeee",result['IT_SYNC']
#     for info in result['IT_SYNC']:
#         if info['STATUS']=='C':
#             if info['MBLNR_NEW']=="":
#                 if debug_flag=='on':
#                     logging.debug("MBLNR_new for Portal_Id :"+info['PORTALID']+" is ---- Not Created.error : "+info['MSG'])
#                 update_collection('mivcreation',{"P_Datetime":info['PORTALID']},{'$set':{'MSG':info['MSG']}})
#                 continue
#             else:
#                 c_pr1=find_one_in_collection('mivcreation',{"P_Datetime":info['PORTALID'],"status":"submitted"})
#             if c_pr1:    
# 	            for x in range(0,len(c_pr1['voucher'])):
# 	                c_pr1['voucher'][x]['MBLNR']=info['MBLNR_NEW']
# 	                c_pr1['voucher'][x]['MJAHR']=info['MJAHR']
# 	            arry4={"voucher":c_pr1['voucher'],"status":"applied",'MSG':info['MSG']}
# 	            update_coll('mivcreation',{'P_Datetime':info['PORTALID'],"status" : "submitted"},{'$set': arry4})
# 	            if debug_flag=='on':
# 	                logging.debug("MBLNR for Portal_Id :"+info['PORTALID']+" is ---- "+info['MBLNR_NEW'])
#         elif info['STATUS']=='A':
#             if info['MBLNR_NEW']=="":
#                 if debug_flag=='on':
#                     logging.debug("MBLNR_new for Portal_Id :"+info['PORTALID']+" is ---- Not Created.error : "+info['MSG'])
#                 update_collection('mivcreation',{"P_Datetime":info['PORTALID']},{'$set':{'MSG':info['MSG']}})
#                 continue
#             else:
#                 c_pr2=find_one_in_collection('mivcreation',{"P_Datetime":info['PORTALID'],"status" : "inprocess"})
#             if c_pr2:
#                 for x in range(0,len(c_pr2['voucher'])):
#                     c_pr2['voucher'][x]['MBLNR_FINAL']=info['MBLNR_NEW']
#                     c_pr2['voucher'][x]['MJAHR_FINAL']=info['MJAHR']
#                     if info['MATNR']==c_pr2['voucher'][x]['MATNR'].strip('0000'):
#                         c_pr2['voucher'][x]['DMBTR']=float(info['DMBTR'])               
#                 dmbtr_count=0
#                 for jj in range(0,len(c_pr2['voucher'])):
#                     if 'DMBTR' in c_pr2['voucher'][jj]:
#                         dmbtr_count+=1                        
#                 if dmbtr_count==len(c_pr2['voucher']):
#                     arry5={"voucher":c_pr2['voucher'],"status":"approved"}
#                 else:
#                     arry5={"voucher":c_pr2['voucher'],"status":"inprocess"}
#                 update_coll('mivcreation',{'P_Datetime':info['PORTALID'],"status":"inprocess"},{'$set': arry5})
#             if debug_flag=='on':
#                 logging.debug("MBLNR_new for Portal_Id :"+info['PORTALID']+" is ---- "+info['MBLNR_NEW'])
#     	    # x+=1
#     if debug_flag=='on':
#         logging.debug("MIV synchronization Job is Completed.sent portal Id's : "+log_msg)
#     else:
#         logging.debug("MIV synchronization Job is Completed")
#     return "successfully completed miv synchronization"


#########old miv_sync
# #this method is usefull to synchronization of inprocess miv's
# @zen_pmpr.route('/miv_sync/')
# def miv_syncr():
# 	logging.debug("MIV synchronization Job is Started")
# 	portal_id= list()
# 	miv_under_process =find_in_collection('mivcreation',{"status" : "inprocess"})
# 	for docs in miv_under_process:
# 		portal_id.append({"PORTALID":docs['P_Datetime']})
# 	sap_login=get_sap_user_credentials()
# 	try:
# 		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
# 		#conn = Connection(user="venu", passwd="venu143", ashost="192.168.18.16",sysnr="D01",gwserv="3300",client="150")
# 		result = conn.call('Z_MIV_SYNC',IT_SYNC=portal_id)
# 		#print result
# 		conn.close()
# 		close_sap_user_credentials(sap_login['_id'])
# 	except Exception,e:
# 		logging.debug("MIV synchronization  error:==%s",str(e))
# 		close_sap_user_credentials(sap_login['_id'])
# 		flash("Unable to connect Sap user Credential","alert-danger")
# 	print result['IT_SYNC'],'sync result'
# 	#logging.debug("MIV synchronization  Result:==%s",str(result['IT_SYNC']))
# 	for info in result['IT_SYNC']:
		
# 		if info['MBLNR_NEW']=="":
# 			continue
# 		else:
# 			c_pr1=find_one_in_collection('mivcreation',{"P_Datetime":info['PORTALID'],"status" : "inprocess"})
# 		for x in range(0,len(c_pr1['voucher'])):
# 			c_pr1['voucher'][x]['MBLNR_FINAL']=info['MBLNR_NEW']
# 	 		c_pr1['voucher'][x]['MJAHR_FINAL']=info['MJAHR']
# 	 	arry4={"voucher":c_pr1['voucher'],"status":"approved"}
# 		update_coll('mivcreation',{'P_Datetime':info['PORTALID'],"status" : "inprocess"},{'$set': arry4})
		
# 	logging.debug("MIV synchronization Job is Completed")
# 	return "successfully completed miv synchronization"

#####old miv sync

#approve if still in Inprocess Approvertime > 5 Min to Current time
# @zen_pmpr.route('/mivapprove_inprocess/')
# def miv_inprocess_approve():
# 	miv_under_process =find_one_in_collection('mivcreation',{"status" : "inprocess"})
# 	if miv_under_process:
# 		logging.debug("MIV In Process Approval Started")
# 		logging.debug("In Process MIV Approval P_Datetime is::==%s",str(miv_under_process['P_Datetime']))
# 		for x in range(len(miv_under_process['voucher'])):
# 			del miv_under_process['voucher'][x]["MAT_DESC"]
# 		approving_time = datetime.datetime.now()
# 		diff = approving_time-miv_under_process['approved_time']
# 		diff_in_minutes= diff.seconds/60
# 		if diff_in_minutes>5:
# 			miv_under_process['P_BUDAT']=datetime.datetime.combine(miv_under_process['P_BUDAT'], datetime.datetime.now().time())
# 			update_coll('mivcreation',{'P_Datetime':str(miv_under_process['P_Datetime'])},{"$set":{'approved_time':approving_time,"P_BUDAT":miv_under_process['P_BUDAT']}})
# 			sap_login=get_sap_user_credentials()
# 			try:
# 				IM_HEADER={"PORTALID":miv_under_process['P_Datetime'],"ACAT":miv_under_process['P_ACAT'],
# 				"TPLNR":miv_under_process['functionlocation'],"EQUNR":miv_under_process['equipment'],
# 				"KOSTL":miv_under_process['costcenter'],'WBS':miv_under_process['wbs_element'],
# 				'NTW':miv_under_process['network'],'ARBPL':miv_under_process['P_ARBPL'],'EKGRP':miv_under_process['P_EKGRP'],
# 				'WERKS':miv_under_process['P_WERKS'],'PRNAME':miv_under_process['P_PRNAME'],"AUART":"PM04","STEUS":"PM01",
# 				"BUDAT":miv_under_process['P_BUDAT'],"REJECT":""}
# 				conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
# 				result = conn.call('Z_MIV_APPROVE',IM_HEADER=IM_HEADER,IT_ITEMS=miv_under_process['voucher'])
# 				conn.close()
# 				close_sap_user_credentials(sap_login['_id'])
# 				msgsuc=result["EX_MSG"]
# 				if msgsuc == "":
# 					logging.debug("MIV In Process Approval Message is Empty")
# 			except Exception,e:
# 				logging.debug("MIV In Process approval error:==%s",str(e))
# 				close_sap_user_credentials(sap_login['_id'])
# 			logging.debug("MIV In Process Approval Completed")
# 	else:
# 		pass
				
	
# 	return "Sucessfully Completed MIV In Process Approval"

@zen_pmpr.route('/mivapprove_inprocess/')
def miv_inprocess_approve():
    miv_under_process =find_in_collection('mivcreation',{"status" : "inprocess",'exclude':{'$exists':0}})
    exclude_count=find_in_collection('miv_config',{'status':'exclude_inprocess'})
    for miv_under_process in miv_under_process:
        approving_time = datetime.datetime.now()
        diff = approving_time-miv_under_process['approved_time']
        diff_in_minutes= diff.seconds/60
        if diff_in_minutes>5:
            if 'flag' in miv_under_process:
                if int(miv_under_process['flag']) < int(exclude_count[0]['exclude_count']):
                    logging.debug("MIV In Process Approval Started & Approval time is ::==%s",str(miv_under_process['P_Datetime']))
                    # logging.debug("In Process MIV Approval P_Datetime is::==%s",str(miv_under_process['P_Datetime']))
                    for x in range(len(miv_under_process['voucher'])):
                        del miv_under_process['voucher'][x]["MAT_DESC"]
                    miv_under_process['P_BUDAT']=datetime.datetime.combine(miv_under_process['P_BUDAT'], datetime.datetime.now().time())
                    update_coll('mivcreation',{'P_Datetime':str(miv_under_process['P_Datetime'])},{"$set":{'inprocess_approved_time':approving_time,"P_BUDAT":miv_under_process['P_BUDAT'],'flag':str(int(i['flag'])+1)}})
                    sap_login=get_sap_user_credentials()
                    try:
                        IM_HEADER={"PORTALID":miv_under_process['P_Datetime'],"ACAT":miv_under_process['P_ACAT'],
                        "TPLNR":miv_under_process['functionlocation'],"EQUNR":miv_under_process['equipment'],
                        "KOSTL":miv_under_process['costcenter'],'WBS':miv_under_process['wbs_element'],
                        'NTW':miv_under_process['network'],'ARBPL':miv_under_process['P_ARBPL'],'EKGRP':miv_under_process['P_EKGRP'],
                        'WERKS':miv_under_process['P_WERKS'],'PRNAME':miv_under_process['P_PRNAME'],"AUART":"PM04","STEUS":"PM01",
                        "BUDAT":miv_under_process['P_BUDAT'],"REJECT":""}
                        conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
                        # conn = Connection(user='ituser', passwd='JAISAI999', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
                        result = conn.call('Z_MIV_APPROVE',IM_HEADER=IM_HEADER,IT_ITEMS=miv_under_process['voucher'])
                        conn.close()
                        close_sap_user_credentials(sap_login['_id'])
                        msgsuc=result["EX_MSG"]
                        if msgsuc == "":
                            logging.debug("MIV In Process Approval Message is Empty")
                    except Exception,e:
                        logging.debug("MIV In Process approval error:==%s",str(e))
                        close_sap_user_credentials(sap_login['_id'])
                    logging.debug("MIV In Process Approval Completed")
                    update_collection('mivcreation',{'_id':ObjectId(miv_under_process['_id'])},{'$set':{'flag':str(int(miv_under_process['flag'])+1)}})
                else:
                    update_collection('mivcreation',{'_id':ObjectId(miv_under_process['_id'])},{'$set':{'exclude':True}})
            else:
                logging.debug("MIV In Process Approval Started & Approval time is ::==%s",str(miv_under_process['P_Datetime']))
                # logging.debug("In Process MIV Approval P_Datetime is::==%s",str(miv_under_process['P_Datetime']))
                for x in range(len(miv_under_process['voucher'])):
                    del miv_under_process['voucher'][x]["MAT_DESC"]
                miv_under_process['P_BUDAT']=datetime.datetime.combine(miv_under_process['P_BUDAT'], datetime.datetime.now().time())
                update_coll('mivcreation',{'P_Datetime':str(miv_under_process['P_Datetime'])},{"$set":{'inprocess_approved_time':approving_time,"P_BUDAT":miv_under_process['P_BUDAT']}})
                sap_login=get_sap_user_credentials()
                try:
                    IM_HEADER={"PORTALID":miv_under_process['P_Datetime'],"ACAT":miv_under_process['P_ACAT'],
                    "TPLNR":miv_under_process['functionlocation'],"EQUNR":miv_under_process['equipment'],
                    "KOSTL":miv_under_process['costcenter'],'WBS':miv_under_process['wbs_element'],
                    'NTW':miv_under_process['network'],'ARBPL':miv_under_process['P_ARBPL'],'EKGRP':miv_under_process['P_EKGRP'],
                    'WERKS':miv_under_process['P_WERKS'],'PRNAME':miv_under_process['P_PRNAME'],"AUART":"PM04","STEUS":"PM01",
                    "BUDAT":miv_under_process['P_BUDAT'],"REJECT":""}
                    conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
                    # conn = Connection(user='ituser', passwd='JAISAI999', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
                    result = conn.call('Z_MIV_APPROVE',IM_HEADER=IM_HEADER,IT_ITEMS=miv_under_process['voucher'])
                    conn.close()
                    close_sap_user_credentials(sap_login['_id'])
                    msgsuc=result["EX_MSG"]
                    if msgsuc == "":
                        logging.debug("MIV In Process Approval Message is Empty")
                except Exception,e:
                    logging.debug("MIV In Process approval error:==%s",str(e))
                    close_sap_user_credentials(sap_login['_id'])
                logging.debug("MIV In Process Approval Completed")
                update_collection('mivcreation',{'_id':ObjectId(miv_under_process['_id'])},{'$set':{'flag':"1"}})
        else:
            pass
    return "Sucessfully Completed MIV In Process Approval"
################## written by aswini ########################
# #approve if still in Inprocess Approvertime > 5 Min to Current time
# @zen_pmpr.route('/mivapprove_inprocess/')
# def miv_inprocess_approve():
# 	#miv_under_process =find_one_in_collection('mivcreation',{"status" : "inprocess"})
# 	miv_under_process =find_in_collection('mivcreation',{"status":"inprocess",'exclude':{'$exists':0}})
# 	exclude_count=find_in_collection('miv_config',{'status':'exclude_inprocess'})
# 	if miv_under_process:
# 		for docs in miv_under_process:
# 			logging.debug("MIV In Process Approval Started")
# 			approving_time = datetime.datetime.now()
# 			diff = approving_time-docs['approved_time']
# 			diff_in_minutes= diff.seconds/60
# 			# if diff_in_minutes>5:
# 			# time.sleep(60)
# 			logging.debug("In Process MIV Approval P_Datetime is::==%s",str(docs['P_Datetime']))
# 			docs['P_BUDAT']=datetime.datetime.combine(docs['P_BUDAT'], datetime.datetime.now().time())
# 			if 'flag' in docs:
# 				if int(docs['flag'])<int(exclude_count[0]['exclude_count']):
# 					update_coll('mivcreation',{'P_Datetime':str(docs['P_Datetime'])},{"$set":{'approved_time':approving_time,"P_BUDAT":docs['P_BUDAT'],'flag':str(int(docs['flag'])+1)}})
# 					# sap_login=get_sap_user_credentials()
# 					# try:
# 					# 	IM_HEADER={"PORTALID":docs['P_Datetime'],"ACAT":docs['P_ACAT'],
# 					# 	"TPLNR":docs['functionlocation'],"EQUNR":docs['equipment'],
# 					# 	"KOSTL":docs['costcenter'],'WBS':docs['wbs_element'],
# 					# 	'NTW':docs['network'],'ARBPL':docs['P_ARBPL'],'EKGRP':docs['P_EKGRP'],
# 					# 	'WERKS':docs['P_WERKS'],'PRNAME':docs['P_PRNAME'],"AUART":"PM04","STEUS":"PM01",
# 					# 	"BUDAT":docs['P_BUDAT'],"REJECT":""}
# 					# 	conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
# 					# 	result = conn.call('Z_MIV_APPROVE',IM_HEADER=IM_HEADER,IT_ITEMS=docs['voucher'])
# 					# 	conn.close()
# 					# 	close_sap_user_credentials(sap_login['_id'])
# 					# 	msgsuc=result["EX_MSG"]
# 					# 	if msgsuc == "":
# 					# 		logging.debug("MIV In Process Approval Message is Empty")
# 					# except Exception,e:
# 					# 	logging.debug("MIV In Process approval error:==%s",str(e))
# 					# 	close_sap_user_credentials(sap_login['_id'])
# 				else:
# 					try:
# 						save_collection('miv_inprocess_rejected',docs)
# 					except:
# 						pass
# 					for i in range(len(docs['approval_levels'])):
# 						update_collection('miv_notifications',{'employee_id':docs['approval_levels'][i]['a_id']},{'$set':{'rejected_inprocess':'1'}})
# 					update_collection('miv_notifications',{'employee_id':docs['employee_id']},{'$set':{'rejected_inprocess':'1'}})
# 					update_coll('mivcreation',{'_id':ObjectId(docs['_id'])},{'$set':{'status':'rejected','exclude':"1"}})
# 					# except:
						
# 			else:
# 				update_coll('mivcreation',{'P_Datetime':str(docs['P_Datetime'])},{"$set":{'approved_time':approving_time,"P_BUDAT":docs['P_BUDAT'],'flag':"1"}})
# 				# sap_login=get_sap_user_credentials()
# 				# try:
# 				# 	IM_HEADER={"PORTALID":docs['P_Datetime'],"ACAT":docs['P_ACAT'],
# 				# 	"TPLNR":docs['functionlocation'],"EQUNR":docs['equipment'],
# 				# 	"KOSTL":docs['costcenter'],'WBS':docs['wbs_element'],
# 				# 	'NTW':docs['network'],'ARBPL':docs['P_ARBPL'],'EKGRP':docs['P_EKGRP'],
# 				# 	'WERKS':docs['P_WERKS'],'PRNAME':docs['P_PRNAME'],"AUART":"PM04","STEUS":"PM01",
# 				# 	"BUDAT":docs['P_BUDAT'],"REJECT":""}
# 				# 	conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
# 				# 	result = conn.call('Z_MIV_APPROVE',IM_HEADER=IM_HEADER,IT_ITEMS=docs['voucher'])
# 				# 	conn.close()
# 				# 	close_sap_user_credentials(sap_login['_id'])
# 				# 	msgsuc=result["EX_MSG"]
# 				# 	if msgsuc == "":
# 				# 		logging.debug("MIV In Process Approval Message is Empty")
# 				# except Exception,e:
# 				# 	logging.debug("MIV In Process approval error:==%s",str(e))
# 				# 	close_sap_user_credentials(sap_login['_id'])
# 			logging.debug("MIV In Process Approval Completed")

# 			# else:
# 			# 	print "less time"
# 			# 	continue
# 	else:
# 		print 'not under process'
# 	print 'completed'
# 	return "Sucessfully Completed MIV In Process Approval"
###########################                  #########################
@zen_pmpr.route('/excluded_inprocess_report/', methods=['GET', 'POST'])
@login_required
def excluded_inprocess_report():
	if current_user.username!="admin":
		user_details = base()
		if user_details["plant_id"]!="HY":
			db = dbconnect()
			uid= current_user.username
			dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
			dept = sorted(dept, key=itemgetter('dept'))			
	return render_template('excluded_inprocess_report.html',user_details=user_details,dept=dept)

@zen_pmpr.route('/excluded_inprocess/', methods=['GET', 'POST'])
def excluded_inprocess():
	result=[]
	finalresult=[]
	uid= current_user.username
	if request.method == 'POST':
		form=request.form
		sdate=datetime.datetime.strptime(form['start_date'], "%d/%m/%Y")
		edate=datetime.datetime.strptime(form['end_date'], "%d/%m/%Y")
		if sdate==edate:
			EndDate = edate+timedelta(days=1)
		else:
			EndDate=edate
		new_dept=form['dept']
		if str(new_dept)=="select": #change the collection
			miv=find_in_collection('miv_inprocess_rejected',{'pr_appliedon':{ '$gte':sdate,'$lte':EndDate },'$or':[{"employee_id":uid},{'approval_levels.a_id' :uid}]})
		else:
			miv=find_in_collection('miv_inprocess_rejected',{'PR_dept':str(new_dept),'pr_appliedon':{ '$gte':sdate,'$lte':EndDate },'$or':[{"employee_id":uid},{'approval_levels.a_id' :uid}]})
		for i in miv:
			for j in i['voucher']:
				result=[j['MBLNR'],j['MATNR'],j['MAT_DESC'],j['MENGE'],i['P_PRNAME'],i['functionlocation'],i['equipment'],i['costcenter']]
				finalresult.append(result)
	return jsonify({'result':finalresult})

#this is for plant1  
@zen_pmpr.route('/plant1', methods=['GET', 'POST'])
@login_required
def plant1():
	user_details = base()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"chief_pid":1,"_id":0})
	dept=find_and_filter("departmentmaster",{"plant":user_details["plant_id"]},{"dept":1,"dept_pid":1,"dept_chief_pid":1,"_id":0})

	return render_template('plant1.html',user_details=user_details,dept=dept,plant=plant_pid)	

#this is for miv level
@zen_pmpr.route('/mivlevelConfig', methods=['GET', 'POST'])
@login_required
def mivlevelConfig():
	p =check_user_role_permissions('550fdf7a850d2d3861bd7ae5')
	if p:
		user_details = base()
		uid = current_user.username
		plant_pid=find_dist_in_collection('Plants','plant_id1')
		plant_pid.sort()
		if request.method == 'POST':
			form=request.form
			
			miv_permission=find_one_in_collection('miv_permission',{"plant":form['plant']})
			if miv_permission==None: 
				save_collection('miv_permission',{"plant":form['plant'],"level":form.getlist('level')})
			else:
				update_coll('miv_permission',{"plant":form['plant']},{"plant":form['plant'],"level":form.getlist('level')})


		return render_template('mivlevelConfig.html',user_details=user_details,plant=plant_pid)
	else:
		return redirect(url_for('zenpmpr.index'))


@zen_pmpr.route('/miv_permission', methods = ['GET', 'POST'])
def miv_permission():
	form=request.form
	data={}
	
	miv_permission=find_one_in_collection("miv_permission",{"plant":form['plant']})
	
	del miv_permission['_id']
	data['reports']=miv_permission
	return jsonify(data)

	
#this is for pr level
@zen_pmpr.route('/prlevelConfig', methods=['GET', 'POST'])
@login_required
def prlevelConfig():
	p =check_user_role_permissions('550fdf44850d2d3861bd7ae4')
	if p:
		user_details = base()
		uid = current_user.username
		plant_pid=find_dist_in_collection('Plants','plant_id1')
		plant_pid.sort()
		if request.method == 'POST':
			form=request.form
			miv_permission=find_one_in_collection('pr_permission',{"plant":form['plant']})
			if miv_permission==None: 
				save_collection('pr_permission',{"plant":form['plant'],"level":form.getlist('level')})
			else:
				update_coll('pr_permission',{"plant":form['plant']},{"plant":form['plant'],"level":form.getlist('level')})


		return render_template('prlevelConfig.html',user_details=user_details,plant=plant_pid)
	else:
		return redirect(url_for('zenpmpr.index'))


@zen_pmpr.route('/pr_permission', methods = ['GET', 'POST'])
def pr_permission():
	form=request.form
	data={}
	
	pr_permission=find_one_in_collection("pr_permission",{"plant":form['plant']})
	
	del pr_permission['_id']
	data['reports']=pr_permission
	return jsonify(data)



#this method is for MIV History Search
@zen_pmpr.route('/mivreports/', methods=['GET', 'POST'])
@login_required
def mivreports():
	
	if current_user.username!="admin":
		user_details = base()
		if user_details["plant_id"]!="HY":
			db = dbconnect()
			uid= current_user.username
			dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
			dept = sorted(dept, key=itemgetter('dept'))
			
			if request.method == 'POST':
				form=request.form

				sdate=datetime.datetime.strptime(form['start_date'], "%d/%m/%Y")
				edate=datetime.datetime.strptime(form['end_date'], "%d/%m/%Y")
				EndDate = edate+timedelta(days=1)

				new_dept=form['dept']
				if str(new_dept)=="select":

					miv=find_in_collection('mivcreation',{'pr_appliedon':{ '$gte':sdate,'$lte':EndDate },'$or':[{"employee_id":uid},{'approval_levels.a_id' :uid}]})
				else:
					miv=find_in_collection('mivcreation',{'PR_dept' :str(new_dept), 'pr_appliedon':{ '$gte':sdate,'$lte':EndDate },'$or':[{"employee_id":uid},{'approval_levels.a_id' :uid}]})
				for x in range(0,len(miv)):
				    if miv[x]['status']=="applied":
				        for y in range (0,len(miv[x]['approval_levels'])):
				            if miv[x]['approval_levels'][y]['a_status']=="current":
				                name=get_employee_name(miv[x]['approval_levels'][y]['a_id'])
				                miv[x]['current_holder']=name
				    elif miv[x]['status']=="rejected":
				    	miv[x]['current_holder']="REJECTED"
				    elif miv[x]['status']=="inprocess":
				    	if 'MSG' in miv[x].keys() and miv[x]['MSG'] !='':
				    		miv[x]['current_holder']="IN PROCESS ERROR"
				    		miv[x]['ERRMSG']=miv[x]['MSG']
				    	else:
				    		miv[x]['current_holder']="IN PROCESS"
				    elif miv[x]['status']=="submitted":
				    	miv[x]['current_holder']="SUBMITTED"
				    else:
				        miv[x]['current_holder']="APPROVED"
				return render_template('NEW_MIV_REPORT.html',user_details=user_details,dept=dept,miv=miv)
			
	return render_template('MIV_REPORTS.html',user_details=user_details,dept=dept)


@zen_pmpr.route('/hodmivreports/', methods=['GET', 'POST'])
@login_required
def hodmivreports():
	# p =check_user_role_permissions('550fde51850d2d3861bd7adf2')
	# if p:
	deptid=get_designation_id(current_user.username)
	p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
	if p:
		db = dbconnect()
		user_details = base()
		uid= current_user.username
		miv=find_in_collection('mivcreation',{"PR_dept":p['dept_pid']})
		for x in range(0,len(miv)):
		    if miv[x]['status']=="applied":
		        for y in range (0,len(miv[x]['approval_levels'])):
		            if miv[x]['approval_levels'][y]['a_status']=="current":
		                name=get_employee_name(miv[x]['approval_levels'][y]['a_id'])
		                miv[x]['current_holder']=name
		    elif miv[x]['status']=="rejected":
		    	miv[x]['current_holder']="REJECTED"
		    elif miv[x]['status']=="approved":
		        miv[x]['current_holder']="APPROVED"
		    else:
		    	miv[x]['current_holder']='SUBMITTED'
		#logging.debug("HOD-MIV Reports result:==%s",str(miv))
		return render_template('hodmivreports.html',user_details=user_details,miv=miv)
	else:
		return redirect(url_for('zenpmpr.index'))


@zen_pmpr.route('/prreports/', methods=['GET', 'POST'])
@login_required
def prreports():
	# p =check_user_role_permissions('550fde51850d2d3861bd7adf2')
	# if p:
	user_details=base()
	uid= current_user.username
	pr=[]
	pr=find_in_collection('prcreation',{"employee_id":uid})
	
	return render_template('prreports.html',user_details=user_details,pr=pr)




##report links 
@zen_pmpr.route('/comparision_report/', methods = ['GET', 'POST'])
@login_required
def comparision_report():
	user_details=base()
	return render_template('comparision_report.html', user_details=user_details)

@zen_pmpr.route('/consumption_report/', methods = ['GET', 'POST'])
@login_required
def consumption_report():
	user_details=base()
	
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	
	work_center=find_in_collection("workcentermaster",{"Plant":plant_pid[0]['plant_id1']})
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	return render_template('consumption_report.html', user_details=user_details,dept=dept,work_center=work_center)



@zen_pmpr.route('/prstatus_report/', methods = ['GET', 'POST'])
@login_required
def prstatus_report():
	user_details=base()
	uid= current_user.username
	db=dbconnect()
	pr=[]
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	
	dept=find_in_collection("departmentmaster",{"plant":user_details["plant_id"]})
	dept = sorted(dept, key=itemgetter('dept'))
	
	if request.method == 'POST':
		form=request.form
		logging.debug("getting from user in pr status %s",form)
		if form['submit']== 'Save':
			c_pr1=find_one_in_collection('prcreation',{"_id":ObjectId(form['prid'])})
			if len(c_pr1['component'])!=0:
				newQuanityC=form['qC'].split(',')
				
				newdateC=form['dateC'].split(',')
				
				newunitC=form['unitC'].split(',')
				
				if c_pr1["Assignment_category"]!="A":
					newspecC=form['specC'].split(',')
					newjustifyC=form['justifyC'].split(',')
					newproC=form['proC'].split(',')

				for x in range(len(c_pr1['component'])):
					c_pr1['component'][x]["MENGE"]=newQuanityC[x]
					c_pr1['component'][x]["EEIND"]=datetime.datetime.strptime(newdateC[x],"%d.%m.%Y")
					c_pr1['component'][x]["VERPR"]=newunitC[x]
					
					if c_pr1["Assignment_category"]!="A":
						c_pr1['component'][x]["JUSTI"]=newjustifyC[x]
						c_pr1['component'][x]["SPEC"]=newspecC[x]
						c_pr1['component'][x]["PROD"]=newproC[x]
				logging.debug("component going to DB pr status---- %s",str(c_pr1['component']))
									
				update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"component":c_pr1['component']}})
			if len(c_pr1['service'])!=0:
				newQuanityS=form['qS'].split(',')
				newdesS=form['desS'].split(',')
				newunitS=form['unitS'].split(',')
				newdateS=form['dateS'].split(',')
				logging.debug("date of pr status report service user given this -----%s",newdateS)
				newproS=form['proS'].split(',')
				for x in range(len(c_pr1['service'])):
					c_pr1['service'][x]["MENGE"]=newQuanityS[x]
					c_pr1['service'][x]["SER_TXT"]=newdesS[x]
					c_pr1['service'][x]["VERPR"]=newunitS[x]
					c_pr1['service'][x]["EEIND"]=datetime.datetime.strptime(newdateS[x],"%d.%m.%Y")
					c_pr1['service'][x]["PROD"]=newproS[x]
				logging.debug("service  going to DB pr status--- %s",str(c_pr1['service']))
				update_coll('prcreation',{'_id':ObjectId(form['prid'])},{"$set":{"service":c_pr1['service']}})
			msg="You have Successfully saved the changes for "+c_pr1["P_Datetime"]
			flash(msg,'alert-success')
		elif form['submit']== 'Search':
			search={}
			date={}
			if form['dept']!="select":
				search['PR_dept']=form['dept']
			
			if form['function_location']!="":
				search['functionlocation']=form['function_location']

			if form['equipment']!="":
				search['equipment']=form['equipment']

			if form['cost_center']!="":
				search['costcenter']=form['cost_center']

			if form['assetcode']!="":
				search['assetcode']=form['assetcode']

			if form['opt']=="comp":
				search['$and']=[{"component":{ '$not':{'$size': 0}}},{"component":{'$ne':"" }}]
				search['$or']=[{'service': {'$size': 0}}, {'service': ""}]
			if form['opt']=="ser":
				search['$and']=[{"service":{ '$not':{'$size': 0}}},{"service":{'$ne':"" }}]
				search['$or']=[{"component":{'$size': 0}},{"component":""}]
			if form['opt']=="both":
				search['$and']=[{"service":{ '$not':{'$size': 0}}},{"service":{'$ne':"" }},{"component":{ '$not':{'$size': 0}}},{"component":{'$ne':"" }}]
			
			if form['date1']!="":
				sdate=datetime.datetime.strptime(form['date1'], "%d.%m.%Y")
				date['$gt']=sdate

			if form['date2']!="":
				edate=datetime.datetime.strptime(form['date2'], "%d.%m.%Y")
				date['$lte']=edate+datetime.timedelta(days=1)
			
			if form['date1']!="" or form['date2']!="":
				search['pr_appliedon']=date
			search['P_WERKS']=plant_pid[0]['plant_id1']
			logging.debug("Search query :---%s",str(search))
			pr=find_in_collection('prcreation',search)
			for x in range(0,len(pr)):
			    if pr[x]['status']=="applied":
			        for y in range (0,len(pr[x]['approval_levels'])):
			            if pr[x]['approval_levels'][y]['a_status']=="current":
			                name=get_employee_name(pr[x]['approval_levels'][y]['a_id'])
			                pr[x]['current_holder']="pending with"+name
			    elif pr[x]['status']=="rejected":
			    	pr[x]['current_holder']="REJECTED"
			    else:
			        pr[x]['current_holder']="APPROVED"
			return render_template('prreports.html',user_details=user_details,pr=pr,dept=dept)

	return render_template('NEW_PR_REPORT.html',user_details=user_details,dept=dept)

# load masters from ui 

@zen_pmpr.route('/loadmasters/', methods = ['GET', 'POST'])
@login_required
def loadmasters():
	p =check_user_role_permissions('55f1cead850d2d6df7b113e0')
	if p:
		user_details=base()
		return render_template('uploadingmasters.html', user_details=user_details)
	else:
		return redirect(url_for('zenpmpr.index'))

#for fetching data from sap based on P_CBANFN for PR STATUS REPORT
@zen_pmpr.route('/fetchprreport', methods = ['GET', 'POST'])
@login_required
def fetchprreport():
	user_details=base()
	uid= current_user.username
	form=request.form
	
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])	
		result = conn.call('Z_MZMM01_PR_STATUS_REPORT',S_PRBANFN_LOW=form['row'])
		
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		data={}
		msg="successfully getting data"
		DictReader={}
		table=[]
		for x in range(0,len(result['IT_PRDISP'])):
			DictReader['POMENGE']=float(result['IT_PRDISP'][x]['POMENGE'])
			DictReader['PMENGE']=float(result['IT_PRDISP'][x]['PMENGE'])
			DictReader['EBELN']=str(result['IT_PRDISP'][x]['EBELN'])
			table.append(DictReader)

		a=[{"check":"0","msg":msg,"return":table}]
		data['report']=a
		return jsonify(data)
	except:
		close_sap_user_credentials(sap_login['_id'])
		flash("Unable to connect Sap user Credential","alert-danger")

	

#enhancement done on 26_10_2016
@zen_pmpr.route('/materiallist_report/', methods = ['GET', 'POST'])
@login_required
def materiallist_report():
	
	user_details=base()
	mtype=find_dist_in_collection("materialmaster","mtype")
	mtype.sort()
	mgrp=find_dist_in_collection("materialmaster","matl_group")
	mgrp.sort()
	plants_new=find_dist_in_collection("Plants","plant_id1")
	plants=[]
	for i in plants_new:
		if i!='0000':
			plants.append(i)
	plants.sort()
	return render_template('materiallist_report.html', user_details=user_details,mtype=mtype,mgrp=mgrp,plants=plants)
#end of enhancement
#enhacement done for material list report on 26 10 2016
@zen_pmpr.route('/getqty_material_list', methods = ['GET', 'POST'])
def getqty_material_list():
	user_details = base()	
	data={}
	sap_login=get_sap_user_credentials()
	try:			
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
		if 'wbs_ele'  in request.form:
			result = conn.call('Z_MZMM01_MAT_QTY',I_MATNR=request.form['material'],IM_WBS=request.form['wbs_ele'],I_WERKS=plant_pid[0]['plant_id1'])
		else:
			result = conn.call('Z_MZMM01_MAT_QTY',I_MATNR=request.form['material'],I_WERKS=request.form['plant_id'])
		logging.debug("get quantity result:==%s",str(result))
		for x in range(0,len(result["IT_MARD"])):
			result["IT_MARD"][x]["LGORT"]=str(result["IT_MARD"][x]["LGORT"])
			result["IT_MARD"][x]["LABST"]=str(result["IT_MARD"][x]["LABST"])
			result["IT_MARD"][x]["TDLINE"]=str(result["IT_TLINE"][0]["TDLINE"])
			result["IT_MARD"][x]["EX_POVALUE"]=str(result['EX_POVALUE'])
			# result["IT_MARD"][x]["SOBKZ"]=str(result["IT_MARD"][x]["SOBKZ"])
		data['reports']=result['IT_MARD']
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		return jsonify(data)
	except Exception,e:
		close_sap_user_credentials(sap_login['_id'])
		logging.debug("get quantity error:==%s",str(e))
#end of enhancement on material list report
#ajax functions 

@zen_pmpr.route('/functionlocation', methods = ['GET', 'POST'])
@login_required
def functionlocation():
	form=request.form
	plant_id=request.args.get('plant_id')
	q = request.args.get('term')
	print q,' functional location'
	db=dbconnect()
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":plant_id},{"plant_id1":1,"_id":0})
	
	funcloc = db.functionlocmaster.aggregate([
	    {'$match' : { "plant":plant_pid[0]['plant_id1'],'$and':[{'$or':[{'functional_loc' : {'$regex': str(q),'$options': 'i' }}] }]} }, 
	    {'$project' : {'functional_loc':1,"cost_center":1,"_id":0}}])
	funcloc=list(funcloc)
	return json.dumps({"results":funcloc})


@zen_pmpr.route('/profit_center', methods = ['GET', 'POST'])
def profit_center():
	q = request.args.get('term')
	db=dbconnect()
	profit = db.costcentersmaster.aggregate([
	    {'$match' : { '$or':[{'profit_center' : {'$regex': str(q),'$options': 'i' }}] }}, 
	    {'$project' : {'profit_center':1,"_id":0}}])
	profit=list(profit)
	return json.dumps({"results":profit})

# make the plant id dynamic
@zen_pmpr.route('/equipment', methods = ['GET', 'POST'])
def equipment():
	start_time=datetime.datetime.now()
	q = request.args.get('term')
	plant_id=request.args.get('plant_id')
	db=dbconnect()
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":plant_id},{"plant_id1":1,"_id":0})
	

	equip = db.equipmentmaster.aggregate([
                {'$match' : { "plant":plant_pid[0]['plant_id1'],'$and': [{'$or':[{"Desp_technical_object" : {'$regex': str(q),'$options': 'i' }},{'equipment_number' : {'$regex': str(q),'$options': 'i' }}] }]} }, 
                {'$project' : {'Desp_technical_object':1,'equipment_number':1,"cost_center":1,'functional_loc':1, "_id":0}}])
	equip=list(equip)
	return json.dumps({"results":equip})

# make the plant id dynamic
@zen_pmpr.route('/costcenter', methods = ['GET', 'POST'])
def costcenter():
	q = request.args.get('term')
	plant_id = request.args.get('plant_id')
	db=dbconnect()
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":plant_id},{"plant_id1":1,"_id":0})
	cm= db.costcentersmaster.aggregate([
	    {'$match' : { "plant":plant_pid[0]['plant_id1'],'$and': [{ '$or':[{"cost_center_desc" : {'$regex': str(q),'$options': 'i' }},{'cost_center' : {'$regex': str(q),'$options': 'i' }}] }]} }, 
	    {'$project' : {'cost_center':1,'cost_center_desc':1,"_id":0}}])
	cm=list(cm)
	return json.dumps({"results":cm})

# plant has been made dynamic to select
@zen_pmpr.route('/get_cost_activity/', methods = ['GET', 'POST'])
def get_cost_activity():
	form=request.form
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":form['plant_id']},{"plant_id1":1,"_id":0})
	data=find_and_filter('Activitymaster',{"plant_id":plant_pid[0]['plant_id1'],"cost_center":form['cost_center']},{"activities":1,"act_desc":1,"_id":0})
	print data
	if data ==[]:
		data_costcenter=[]
		data_cost_desc=[]
	else:
		data_costcenter=data[0]['activities']
		data_cost_desc=data[0]['act_desc']
	return jsonify({'result':{"activities":data_costcenter,'act_desc':data_cost_desc}})

@zen_pmpr.route('/wbs_element', methods = ['GET', 'POST'])
def wbs_element():
	q = request.args.get('term')
	plant_id = request.args.get('plant_id')
	db=dbconnect()
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":plant_id},{"plant_id1":1,"_id":0})
	wbselement = db.wbselementmaster.aggregate([
                {'$match' : { "plant":plant_pid[0]['plant_id1'],'$and': [{ '$or':[{"short_desc" : {'$regex': str(q),'$options': 'i' }},{'WBS_element' : {'$regex': str(q),'$options': 'i' }}] }]} }, 
                {'$project' : {'short_desc':1,'WBS_element':1, "_id":0}}])
	wbselement=list(wbselement)
	return json.dumps({"results":wbselement})


@zen_pmpr.route('/network', methods = ['GET', 'POST'])
def network():
	q = request.args.get('term')
	db=dbconnect()
	net = db.networkmaster.aggregate([
	    {'$match' : { '$or':[{'object_number' : {'$regex': str(q),'$options': 'i' }}] }}, 
	    {'$project' : {'object_number':1,"_id":0}}])
	net=list(net)
	return json.dumps({"results":net})

@zen_pmpr.route('/uom', methods = ['GET', 'POST'])
def uom():
	q = request.args.get('term')
	db=dbconnect()
	net = db.uommaster.aggregate([
	    {'$match' : { '$or':[{'mu' : {'$regex': str(q),'$options': 'i' }}] }}, 
	    {'$project' : {'mu':1,"_id":0}}])
	net=list(net)
	return json.dumps({"results":net})


@zen_pmpr.route('/asset', methods = ['GET', 'POST'])
def asset():
	q = request.args.get('term')
	db=dbconnect()
	asset = db.assetmaster.aggregate([
                {'$match' : { '$or':[{"asset_desc" : {'$regex': str(q),'$options': 'i' }},{'main_asset_number' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'asset_desc':1,'main_asset_number':1, "_id":0}}])
	asset=list(asset)
	return json.dumps({"results":asset})


@zen_pmpr.route('/department', methods = ['GET', 'POST'])
def department():
	q = request.args.get('term')
	db=dbconnect()
	dpt = db.departmentmaster.aggregate([
                {'$match' : { '$or':[{"department_desc" : {'$regex': str(q),'$options': 'i' }},{'department_id' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'department_desc':1,'department_id':1, "_id":0}}])
	dpt=list(dpt)
	return json.dumps({"results":dpt})


@zen_pmpr.route('/servicematerial', methods = ['GET', 'POST'])
def servicematerial():
	
	q = request.args.get('term')
	
	m=request.args.get('mtype')
	db=dbconnect()
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	
	cursor = db.servicegroupmaster.find({},{'_id':0})
	result = [item for item in cursor]
	
	return json.dumps({"results":result})
	
# made dynamic for selecting multiple plants
#material based on  material type also  in pr creations 
@zen_pmpr.route('/material', methods = ['GET', 'POST'])
def material():
	q = request.args.get('term')
	m=request.args.get('mtype')
	plant_id=request.args.get('plant_id')
	db=dbconnect()
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":plant_id},{"plant_id1":1,"_id":0})
	mat = db.materialmaster.aggregate([
                {'$match' : { "mtype":m,"plant_id":plant_pid[0]['plant_id1'],'$and':[{'$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
                {'$project' : {'material_desc':1,'material':1,'bun':1 ,"_id":0}}])
	mat=list(mat)			
	return json.dumps({"results":mat})

# made dynamic for selecting multiple plants
@zen_pmpr.route('/getquantity', methods = ['GET', 'POST'])
def getquantity():
	user_details = base()	
	data={}
	form=request.form
	print form["plant_id"],' testing form'
	plant_pid=find_and_filter('Plants',{"plant_id":form["plant_id"]},{"plant_id1":1,"_id":0})
	sap_login=get_sap_user_credentials()
	if '_id' in sap_login:
		try:			
			conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
			if 'wbs_ele'  in request.form:
				result = conn.call('Z_MZMM01_MAT_QTY',I_MATNR=request.form['material'],IM_WBS=request.form['wbs_ele'],I_WERKS=plant_pid[0]['plant_id1'])
			else:
				result = conn.call('Z_MZMM01_MAT_QTY',I_MATNR=request.form['material'],I_WERKS=plant_pid[0]['plant_id1'])
			
			logging.debug("get quantity result:==%s",str(result))
			for x in range(0,len(result["IT_MARD"])):
				result["IT_MARD"][x]["LGORT"]=str(result["IT_MARD"][x]["LGORT"])
				result["IT_MARD"][x]["LABST"]=str(result["IT_MARD"][x]["LABST"])
				result["IT_MARD"][x]["TDLINE"]=str(result["IT_TLINE"][0]["TDLINE"])
				result["IT_MARD"][x]["EX_POVALUE"]=str(result['EX_POVALUE'])
				# result["IT_MARD"][x]["SOBKZ"]=str(result["IT_MARD"][x]["SOBKZ"])
			data['reports']=result['IT_MARD']
			conn.close()
			close_sap_user_credentials(sap_login['_id'])			
			return jsonify(data)
		except Exception,e:
			close_sap_user_credentials(sap_login['_id'])
			logging.debug("get quantity error:==%s",str(e))
			data={'result':'error'}
			return jsonify(data)
	else:
		logging.info('Get Quantity Error ,Shortage Of Userlicense.')
		data={'result':'error'}
		return jsonify(data)		
			

#material based on search words only
@zen_pmpr.route('/material1', methods = ['GET', 'POST'])
def material1():
	q = request.args.get('term')
	db=dbconnect()
	mat1 = db.materialmaster.aggregate([
                {'$match' : { '$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'material_desc':1,'material':1, "_id":0}}])
	mat1=list(mat1)
	return json.dumps({"results":mat1})

@zen_pmpr.route('/material2', methods = ['GET', 'POST'])
def material2():
	q = request.args.get('term')
	mty=request.args.get('mty')
	mgrp=request.args.get('mgrp')
	db=dbconnect()
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	if (mty!=None or mty!="") and (mgrp==None or mgrp==""):
		mat1 = db.materialmaster.aggregate([
	                {'$match' : { "plant_id":plant_pid[0]['plant_id1'],'$and':[{'$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
	                {'$project' : {'material_desc':1,'material':1, "_id":0}}])
		mat1=list(mat1)
	elif (mty!=None or mty!="") and (mgrp==None or mgrp==""):
		mat1 = db.materialmaster.aggregate([
	                {'$match' : {"mtype":mty,"plant_id":plant_pid[0]['plant_id1'],'$and':[{ '$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
	                {'$project' : {'material_desc':1,'material':1, "_id":0}}])
		mat1=result(mat1)
	elif (mty==None or mty=="") and (mgrp!=None or mgrp!=""):
		mat1 = db.materialmaster.aggregate([
	                {'$match' : {"matl_group":mgrp,"plant_id":plant_pid[0]['plant_id1'],'$and':[{'$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
	                {'$project' : {'material_desc':1,'material':1, "_id":0}}])
		mat1=list(mat1)
	else:
		mat1 = db.materialmaster.aggregate([
	                {'$match' : { "matl_group":mgrp,"mtype":mty,"plant_id":plant_pid[0]['plant_id1'],'$and':[{'$or':[{"material_desc" : {'$regex': str(q),'$options': 'i' }},{'material' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
	                {'$project' : {'material_desc':1,'material':1, "_id":0}}])
		mat1=list(mat1)
	return json.dumps({"results":mat1})



@zen_pmpr.route('/storage', methods = ['GET', 'POST'])
def storage():
	user_details = base()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	#plant_pid[0]['plant_id1']
	q = request.args.get('term')
	db=dbconnect()
	store = db.storagelocmaster.aggregate([
                {'$match' : { "plant":plant_pid[0]['plant_id1'],'$and':[{'$or':[{"desc_storage_loc" : {'$regex': str(q),'$options': 'i' }},{'storage_loc' : {'$regex': str(q),'$options': 'i' }}]}] }}, 
                {'$project' : {'desc_storage_loc':1,'storage_loc':1,"_id":0}}])
	store=list(store)
	return json.dumps({"results":store})



## Report ajax function 


# enhancement done on 26 10 2016 evening for adding plant filter and viewing all plants stock
@zen_pmpr.route('/getmateriallistreport', methods = ['GET', 'POST'])
def getmateriallistreport():
	form=request.form
	logging.debug("user provide -->%s",str(form));
	user_details=base()
	
	user_details=base()
	plant_pid=find_and_filter('Plants',{},{"chief_pid":1,"plant_id1":1,"_id":0})
   	searchFrom={}
   	# searchTO={}
   	# searchFrom['plant_id']=plant_pid[0]['plant_id1']
   	# searchTO['plant_id']=plant_pid[0]['plant_id1']

   	if request.args.get('description')!=None:
   		searchFrom['mtype']=request.args.get('description')
   	if request.args.get('group')!=None:
   		searchFrom['matl_group']=request.args.get('group')
   	if request.args.get('material')!=None:
   		searchFrom['material']=request.args.get('material')
   	if request.args.get('plantid')!=None:
   		searchFrom['plant_id']=request.args.get('plantid')
   	
   	db=dbconnect()

   	fromCount=find_in_collection_count("materialmaster",searchFrom)
   
	s=int(form['start'])
	l=int(form['length'])
	e=int(s+l)
	
	data1=[]
	cursor=db.materialmaster.find(searchFrom).skip(s).limit(l)
	data = [item for item in cursor]
	for d in data:
		del d['_id']
		data1.append(d) 
	

	send={"recordsTotal":fromCount,"recordsFiltered":fromCount,"data":data1}
	
	return jsonify(send)

# end of enhancement 26 10 2016


	
@zen_pmpr.route('/getprstatusreport', methods = ['GET', 'POST'])
def getprstatusreport():
	form=request.form
	
	user_details=base()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
	
	if form['date1'] !="" and form['date2'] !="":
		create= datetime.datetime.strptime(form['date1'], '%d.%m.%Y')
		create1= datetime.datetime.strptime(form['date2'], '%d.%m.%Y')
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_PR_STATUS_REPORT',S_PRBANFN_LOW=form['prno1'],S_PRBANFN_HIGH=form['prno2'],S_PRDATE_LOW=create,S_PRDATE_HIGH=create1,S_PRMATNR_LOW=form['material'],S_PRMATNR_HIGH=form['material1'],P_PRMATKL=form['group'])
	elif form['date1'] !="":
		create= datetime.datetime.strptime(form['date1'], '%d.%m.%Y')
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_PR_STATUS_REPORT',S_PRBANFN_LOW=form['prno1'],S_PRBANFN_HIGH=form['prno2'],S_PRDATE_LOW=create,S_PRMATNR_LOW=form['material'],S_PRMATNR_HIGH=form['material1'],P_PRMATKL=form['group'])
	
	elif form['date2'] !="":
		create1= datetime.datetime.strptime(form['date2'], '%d.%m.%Y')
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_PR_STATUS_REPORT',S_PRBANFN_LOW=form['prno1'],S_PRBANFN_HIGH=form['prno2'],S_PRDATE_HIGH=create1,S_PRMATNR_LOW=form['material'],S_PRMATNR_HIGH=form['material1'],P_PRMATKL=form['group'])
	else:
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_PR_STATUS_REPORT',S_PRBANFN_LOW=form['prno1'],S_PRBANFN_HIGH=form['prno2'],S_PRMATNR_LOW=form['material'],S_PRMATNR_HIGH=form['material1'],P_PRMATKL=form['group'])
	data={}

	if len(result['IT_PRDISP'])!=0:
		data['error'] = 'no'
		no_of_reports=len(result['IT_PRDISP'])
		for i in range(0,no_of_reports):
			result['IT_PRDISP'][i]['MATNR']=str(result['IT_PRDISP'][i]['MATNR'])
			result['IT_PRDISP'][i]['MAKTX']=str(result['IT_PRDISP'][i]['MAKTX'])
			result['IT_PRDISP'][i]['MATKL']=str(result['IT_PRDISP'][i]['MATKL'])
			result['IT_PRDISP'][i]['BANFN']=str(result['IT_PRDISP'][i]['BANFN'])
			result['IT_PRDISP'][i]['DEPT']=str(result['IT_PRDISP'][i]['DEPT'])
			result['IT_PRDISP'][i]['KOSTL']=str(result['IT_PRDISP'][i]['KOSTL'])
			result['IT_PRDISP'][i]['DISUB_PSPNR']=str(result['IT_PRDISP'][i]['DISUB_PSPNR'])
			result['IT_PRDISP'][i]['PRMENGE']=str(result['IT_PRDISP'][i]['PRMENGE'])
			result['IT_PRDISP'][i]['MEINS']=str(result['IT_PRDISP'][i]['MEINS'])
			result['IT_PRDISP'][i]['PREIS']=str(result['IT_PRDISP'][i]['PREIS'])
			result['IT_PRDISP'][i]['STATU']=str(result['IT_PRDISP'][i]['STATU'])
			result['IT_PRDISP'][i]['JUSTI']=str(result['IT_PRDISP'][i]['JUSTI'])
			result['IT_PRDISP'][i]['BADAT']=result['IT_PRDISP'][i]['BADAT'].strftime('%d-%m-%Y')
			result['IT_PRDISP'][i]['EBELN']=str(result['IT_PRDISP'][i]['EBELN'])
			result['IT_PRDISP'][i]['PROCAT']=str(result['IT_PRDISP'][i]['PROCAT'])
			result['IT_PRDISP'][i]['POMENGE']=str(result['IT_PRDISP'][i]['POMENGE'])
			result['IT_PRDISP'][i]['PMENGE']=str(result['IT_PRDISP'][i]['PMENGE'])
			result['IT_PRDISP'][i]['LIFNR']=str(result['IT_PRDISP'][i]['LIFNR'])
			result['IT_PRDISP'][i]['NAME1']=str(result['IT_PRDISP'][i]['NAME1'])
			data['reports']=result['IT_PRDISP']
	else:
		data['error']= 'yes'
		data['msg']="Empty list"
	
	return jsonify(data)





@zen_pmpr.route('/getconsumptionreport', methods = ['GET', 'POST'])
def getconsumptionreport():
	form=request.form
	
	user_details=base()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"chief_pid":1,"plant_id1":1,"_id":0})
	
	if form['date1'] !="" and form['date2'] !="":
		create= datetime.datetime.strptime(form['date1'], '%d.%m.%Y')
		create1= datetime.datetime.strptime(form['date2'], '%d.%m.%Y')
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_RP_CONSUMPTION_REPORT',P_IDAT1_LOW=create,P_IDAT1_HIGH=create1,P_TPLNR_LOW=form['function_location1'],P_TPLNR_HIGH=form['function_location2'],P_EQUNR_LOW=form['equipment1'],P_EQUNR_HIGH=form['equipment2'],P_KOSTL_LOW=form['cost_center1'],P_KOSTL_HIGH=form['cost_center2'],P_ARBID_LOW=form['wrk_center1'],P_ARBID_HIGH=form['wrk_center2'])
	elif form['date1'] !="":
		create= datetime.datetime.strptime(form['date1'], '%d.%m.%Y')
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_RP_CONSUMPTION_REPORT',P_IDAT1_LOW=create,P_TPLNR_LOW=form['function_location1'],P_TPLNR_HIGH=form['function_location2'],P_EQUNR_LOW=form['equipment1'],P_EQUNR_HIGH=form['equipment2'],P_KOSTL_LOW=form['cost_center1'],P_KOSTL_HIGH=form['cost_center2'],P_ARBID_LOW=form['wrk_center1'],P_ARBID_HIGH=form['wrk_center2'])
	
	elif form['date2'] !="":
		create1= datetime.datetime.strptime(form['date2'], '%d.%m.%Y')
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_RP_CONSUMPTION_REPORT',P_IDAT1_HIGH=create1,P_TPLNR_LOW=form['function_location1'],P_TPLNR_HIGH=form['function_location2'],P_EQUNR_LOW=form['equipment1'],P_EQUNR_HIGH=form['equipment2'],P_KOSTL_LOW=form['cost_center1'],P_KOSTL_HIGH=form['cost_center2'],P_ARBID_LOW=form['wrk_center1'],P_ARBID_HIGH=form['wrk_center2'])
	else:
		conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', sysnr='D01', client='150')
		result = conn.call('Z_MZMM01_RP_CONSUMPTION_REPORT',P_TPLNR_LOW=form['function_location1'],P_TPLNR_HIGH=form['function_location2'],P_EQUNR_LOW=form['equipment1'],P_EQUNR_HIGH=form['equipment2'],P_KOSTL_LOW=form['cost_center1'],P_KOSTL_HIGH=form['cost_center2'],P_ARBID_LOW=form['wrk_center1'],P_ARBID_HIGH=form['wrk_center2'])
	
	data={}
	if len(result['IT_OUTPUT'])!=0:
		data['error'] = 'no'
		no_of_reports=len(result['IT_OUTPUT'])
		for i in range(0,no_of_reports):
			result['IT_OUTPUT'][i]['BUDAT']=result['IT_OUTPUT'][i]['BUDAT'].strftime('%d-%m-%Y')
			result['IT_OUTPUT'][i]['WERKS']=str(result['IT_OUTPUT'][i]['WERKS'])
			result['IT_OUTPUT'][i]['ARBID']=str(result['IT_OUTPUT'][i]['ARBID'])
			result['IT_OUTPUT'][i]['TPLNR']=str(result['IT_OUTPUT'][i]['TPLNR'])
			result['IT_OUTPUT'][i]['KOSTL']=str(result['IT_OUTPUT'][i]['KOSTL'])
			result['IT_OUTPUT'][i]['MATNR']=str(result['IT_OUTPUT'][i]['MATNR'])
			result['IT_OUTPUT'][i]['STEUS']=str(result['IT_OUTPUT'][i]['STEUS'])
			result['IT_OUTPUT'][i]['MAKTX']=str(result['IT_OUTPUT'][i]['MAKTX'])
			result['IT_OUTPUT'][i]['EQKTX']=str(result['IT_OUTPUT'][i]['EQKTX'])
			result['IT_OUTPUT'][i]['MEINS']=str(result['IT_OUTPUT'][i]['MEINS'])
			result['IT_OUTPUT'][i]['MENGE']=str(result['IT_OUTPUT'][i]['MENGE'])

			result['IT_OUTPUT'][i]['DMBTR']=str(result['IT_OUTPUT'][i]['DMBTR'])
			result['IT_OUTPUT'][i]['RMEINS']=str(result['IT_OUTPUT'][i]['RMEINS'])
			result['IT_OUTPUT'][i]['RMENGE']=str(result['IT_OUTPUT'][i]['RMENGE'])
			result['IT_OUTPUT'][i]['RDMBTR']=str(result['IT_OUTPUT'][i]['RDMBTR'])
			result['IT_OUTPUT'][i]['TOTQTY']=str(result['IT_OUTPUT'][i]['TOTQTY'])
			result['IT_OUTPUT'][i]['TOTAMT']=str(result['IT_OUTPUT'][i]['TOTAMT'])
			data['reports']=result['IT_OUTPUT']
	else:
		data['error']= 'yes'
		data['msg']="Empty list"
	
	return jsonify(data)


@zen_pmpr.route('/LoadMaterialFromSap', methods = ['GET', 'POST'])
def LoadMaterialFromSap():
	sap_login=get_sap_user_credentials()
	yesterdayDate=datetime.date.today()-datetime.timedelta(days=1)
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
		result = conn.call('Z_MZMM01_PMPR_MATERIAL_INS',IM_ERSDA=yesterdayDate)
		logging.debug("lOADING material fROM SAP  is Successfull")
		conn.close()
		close_sap_user_credentials(sap_login['_id'])

	except Exception,e:
		logging.debug("lOAD material fROM SAP  error:==%s",str(e))
		close_sap_user_credentials(sap_login['_id'])
		# return "unsuccessfully"
	for i in range(0,len(result['IT_MARA_D'])):
		e = result['IT_MARA_D'][i]
		create= datetime.datetime.strptime(str(e['ERSDA']), '%Y-%m-%d')
		
		create_new = create.strftime('%d-%m-%Y')
		
		array={"matl_group" :e['MATKL'],"plant_id" : e['WERKS'],"material" : e['MATNR'],"material_desc" : e['MAKTX'],"prod_hierarchy" : e['PRODH'],"mat_crtd_dt" : create_new,"st_bin" : e['LGPBE'],"mtype" :e['MTART'],"prod_hierarchy_desc" : e['VTEXT'],"bun" :e['MEINS'],"mat_po_txt" :e['MPOTXT'],"st_loc" :e['LGORT'],"old_material" :e['BISMT'],"Mov_avg_price" : float(e['VERPR']),"stock" :float(e['LABST'])}
		
		findMaterialObj=find_one_in_collection("materialmaster",{"material":e['MATNR'],"plant_id":e['WERKS']})
		if findMaterialObj:
			del_doc('materialmaster',{"plant_id":e['WERKS'],"material" : e['MATNR']})
			save_collection('materialmaster',array)
			save_collection('addedMaterialMaster',array)
		
		else:
			save_collection('materialmaster',array)
			save_collection('addedMaterialMaster',array)
	return "Successfully"


@zen_pmpr.route('/DeleteMaterialFromDB', methods = ['GET', 'POST'])
def DeleteMaterialFromDB():
	sap_login=get_sap_user_credentials()
	yesterdayDate=datetime.date.today()-datetime.timedelta(days=1)
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
		logging.debug("delete material last date fROM SAP :==%s",str(yesterdayDate))
		result = conn.call('Z_MZMM01_PMPR_MATERIAL_DEL',IM_LAEDA=yesterdayDate)
		logging.debug("delete material fROM SAP  result:==%s",str(result))
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
	except Exception,e:
		logging.debug("delete material fROM SAP error:==%s",str(e))
		close_sap_user_credentials(sap_login['_id'])
		# return "unsuccessfully"
	
	for i in range(0,len(result['IT_OUTPUT'])):
		e = result['IT_OUTPUT'][i]
		delMaterialObj=find_one_in_collection('materialmaster',{"plant_id":e['WERKS'],"material" : e['MATNR']})
		save_collection('deletedMaterialMaster',delMaterialObj)
		del_doc('materialmaster',{"plant_id":e['WERKS'],"material" : e['MATNR']})
		
	# return "successfully"


#Written By Sairam for Generating Random Number on 19_APR_2016

def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)
