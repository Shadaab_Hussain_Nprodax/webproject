from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
import csv
import os
import cgi
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from zenapp.modules.leave.lfunctions import *
import logging
#from pyrfc import *
import json
import random
import sys
from array import array

zen_proposal = Blueprint('zenproposal', __name__, url_prefix='/investment')

@zen_proposal.route('/investmentproposal', methods = ['GET', 'POST'])
@login_required
def investmentproposal():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
    plantid=plant_pid[0]['plant_id1']
    plantname=plant_pid[0]['plant_desc']
#     emp_exist=find_and_filter('Investementproposal',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
#     if emp_exist == [] :
#         emp='NF'
#     else:
#         emp='F'
#     print"emmmm:",emp
    create_new=datetime.datetime.today()
    # print "datetime",create_new.strftime("%d%m%Y")
    currentYear=datetime.datetime.today()
    yr=currentYear.year
    # print "year",str(yr)
    if request.method == 'POST':
            form=request.form
            emp_name=get_employee_name(uid)
            emp_exist=find_and_filter('Investementproposal',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
            # print emp_exist
#             print "employee_year",emp_exist[0]['year_of_proposal']
            if emp_exist == [] :
                print "hello"            
                a=[]
                b=[]
                c=[]
                d=[]
                e=[]
                f=[]
                g=[]
                if form['rad'] == "Yes":
                    # print "R1 Empty"
                    a.append({"accomidationByComp":form['rad'],"emp_to_comp_per_month":form['R1'],"emp_to_comp_No_of_month":form['R2'],"emp_to_comp_Tot_amount":form['R3'],"comp_to_emp_per_month":form['R4'],"comp_to_emp_No_of_month":form['R5'],"comp_to_emp_Tot_amount":form['R6']});
                else:
                    a.append({"accomidationByComp":form['rad'],"rent_paid_own_per_month":form['R7'],"ownner_pan":form['R8'],"owner_add":form['R9']});
                if form['rad1'] == 'Yes':
                    b.append({"selfoccupied":form['rad1'],"loan_on_self":form['A1'],"loan_on_outlet":form['A2'],"rental_income":form['A3'],"municipal_tax":form['A4'],"date_of_occupation":form['A5'],"house_add":form['A6']});
                else:
                    b.append({"selfoccupied":form['rad1']})   
                c.append({"medical_insurence":form['C1'],"medical_insu_for_parents":form['C2'],"section80DD":form['C3'],"section80DDB":form['C4'],"section80E":form['C5'],"section80U":form['C6']})
                d.append({"contribution_to_pension":form['CC1'],"life_insurance_premium":form['CC2'],"public_provident_fund":form['CC3'],"deposit_in_national_saving":form['CC4'],"interest_on_NSC":form['CC5'],"ulip":form['CC6'],"principal_loan":form['CC7'],"mutual_funds":form['CC8'],"elss":form['CC9'],"children_education_exp":form['CC10'],"fixed_deposits":form['CC11'],"notified_infrastructure_bonds":form['CC12'],"deferred_annuity":form['CC13'],"Others":form['CC14'],"total":form['CC15']})
                e.append({"gross_salary":form['P1'],"pre_PF":form['P2'],"pre_PT":form['P3'],"Pre_savings":form['P4']})
                f.append({"no_of_childerns":form['NC1'],"sch_going_child":form['NC2'],"stay_at_hostal":form['NC3'],"not_stay_in_hostal":form['NC4']})
                if form['rad2'] == 'Yes':
                    g.append({"form_no_12":form['rad2'],"accomidation":form['F1'],"cars_others":form['F2'],"sweeper":form['F3'],"Gas":form['F4'],"interest_free":form['F5'],"holiday_expenses":form['F6'],"free":form['F7'],"free_meals":form['F8'],"free_education":form['F9'],"gifts":form['F10'],"creditcard_exp":form['F11'],"club_exp":form['F12'],"use_of_assets":form['F13'],"transfer_of_assets":form['F14'],"values_of_others":form['F15'],"stock_options":form['F16'],"other_benefits":form['F17'],"house_loan":form['F18'],'vehicle_loan':form['F19'],"total_perquisites":form['F20'],"total_profits":form['F21']})
                else:
                    g.append({"form_no_12":form['rad2']})
                
                array={"employee_id":uid,"rent_details":a,"section24_details":b,"section80EE_details":form['E1'],"chapterVIA":c,"section80CC":d,"section80CCG":form['GSS1'],"pre_emp_sal_details":e,"children_details":f,"perquisites":g,"year_of_proposal":str(yr),"entry_date":create_new.strftime("%d/%m/%Y"),"flag":"1","employee_name":emp_name,"plant_name":plantname};
                save_collection('Investementproposal',array) 
                flash("Your Record saved successfully",'alert-success')
                return redirect(url_for('zenproposal.investmentproposal')) 
            else:
                print "employee_year",emp_exist[0]['year_of_proposal'] 
                yy=str(yr)
                if yy == emp_exist[0]['year_of_proposal'] :
                    flash("your record already entered",'alert-success')
                else:
                    a=[]
                    b=[]
                    c=[]
                    d=[]
                    e=[]
                    f=[]
                    g=[]
                    if form['rad'] == "Yes":
                        # print "R1 Empty"
                        a.append({"accomidationByComp":form['rad'],"emp_to_comp_per_month":form['R1'],"emp_to_comp_No_of_month":form['R2'],"emp_to_comp_Tot_amount":form['R3'],"comp_to_emp_per_month":form['R4'],"comp_to_emp_No_of_month":form['R5'],"comp_to_emp_Tot_amount":form['R6']});
                    else:
                        a.append({"accomidationByComp":form['rad'],"rent_paid_own_per_month":form['R7'],"ownner_pan":form['R8'],"owner_add":form['R9']});
                    if form['rad1'] == 'Yes':
                        b.append({"selfoccupied":form['rad1'],"loan_on_self":form['A1'],"loan_on_outlet":form['A2'],"rental_income":form['A3'],"municipal_tax":form['A4'],"date_of_occupation":form['A5'],"house_add":form['A6']});
                    else:
                        b.append({"selfoccupied":form['rad1']})   
                    c.append({"medical_insurence":form['C1'],"medical_insu_for_parents":form['C2'],"section80DD":form['C3'],"section80DDB":form['C4'],"section80E":form['C5'],"section80U":form['C6']})
                    d.append({"contribution_to_pension":form['CC1'],"life_insurance_premium":form['CC2'],"public_provident_fund":form['CC3'],"deposit_in_national_saving":form['CC4'],"interest_on_NSC":form['CC5'],"ulip":form['CC6'],"principal_loan":form['CC7'],"mutual_funds":form['CC8'],"elss":form['CC9'],"children_education_exp":form['CC10'],"fixed_deposits":form['CC11'],"notified_infrastructure_bonds":form['CC12'],"deferred_annuity":form['CC13'],"Others":form['CC14'],"total":form['CC15']})
                    e.append({"gross_salary":form['P1'],"pre_PF":form['P2'],"pre_PT":form['P3'],"Pre_savings":form['P4']})
                    f.append({"no_of_childerns":form['NC1'],"sch_going_child":form['NC2'],"stay_at_hostal":form['NC3'],"not_stay_in_hostal":form['NC4']})
                    if form['rad2'] == 'Yes':
                        g.append({"form_no_12":form['rad2'],"accomidation":form['F1'],"cars_others":form['F2'],"sweeper":form['F3'],"Gas":form['F4'],"interest_free":form['F5'],"holiday_expenses":form['F6'],"free":form['F7'],"free_meals":form['F8'],"free_education":form['F9'],"gifts":form['F10'],"creditcard_exp":form['F11'],"club_exp":form['F12'],"use_of_assets":form['F13'],"transfer_of_assets":form['F14'],"values_of_others":form['F15'],"stock_options":form['F16'],"other_benefits":form['F17'],"house_loan":form['F18'],'vehicle_loan':form['F19'],"total_perquisites":form['F20'],"total_profits":form['F21']})
                    else:
                        g.append({"form_no_12":form['rad2']})
                    
                    array={"employee_id":uid,"rent_details":a,"section24_details":b,"section80EE_details":form['E1'],"chapterVIA":c,"section80CC":d,"section80CCG":form['GSS1'],"pre_emp_sal_details":e,"children_details":f,"perquisites":g,"year_of_proposal":str(yr),"entry_date":create_new.strftime("%d/%m/%Y"),"flag":"1","employee_name":emp_name,"plant_name":plantname};
                    save_collection('Investementproposal',array) 
                    flash("Your Record saved successfully",'alert-success')
                    return redirect(url_for('zenproposal.investmentproposal')) 
    return render_template('INVESTMENTPROPOSAL_ENTRY_FORM.html', user_details=user_details,year=currentYear.year,date=currentYear)

@zen_proposal.route('/investmentproposallistview', methods = ['GET', 'POST'])
@login_required
def investmentproposallistview():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    list=find_all_in_collection('Investementproposal')
#     print "resutls",list
    if request.method == 'POST':
            form=request.form
            # print "Id",form['prid']
            logging.info(form['prid'])
            currentYear=datetime.datetime.today()
            yr=currentYear.year
            create_new=datetime.datetime.today()
            # print "datetime",create_new.strftime("%d%m%Y")  
            emp_data=db.Investementproposal.aggregate([{'$match':{'_id':ObjectId(form['prid'])}},{'$unwind':'$children_details'},{'$unwind':'$rent_details'},{'$unwind':'$section24_details'},{'$unwind':'$chapterVIA'},{'$unwind':'$section80CC'},{'$unwind':'$pre_emp_sal_details'},{'$unwind':'$perquisites'}])
            emp_data=list(emp_data)
            # print "data:",emp_data['result']
            logging.info(emp_data)
            return render_template('INVESTMENT_EMPLOYEE_DETAILS.html', user_details=user_details,emp_data=emp_data,year=currentYear.year,date=currentYear)
            
    return render_template('INVESTMENTPROPOSAL_LIST_VIEW.html', user_details=user_details,list=list)

@zen_proposal.route('/investmentproposalemployeeview', methods = ['GET', 'POST'])
@login_required
def investmentproposalemployeeview():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    list=find_and_filter('Investementproposal',{"employee_id":uid},{"flag":0})
            
    return render_template('INVESTMENTPROPOSAL_EMP_VIEW.html', user_details=user_details,list=list)


#used to create actual proposals
@zen_proposal.route('/actualinvestmentproposalemployeeview', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposalemployeeview():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    list=find_and_filter('InvestmentActuals',{"employee_id":uid},{"flag":0})
            
    return render_template('INVESTMENT_ACTUAL_PROPOSALS_EMP_VIEW.html', user_details=user_details,list=list)


@zen_proposal.route('/static/pdf/gridfs/<filename>')
#,year_of_proposal')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
        #year_of_proposal=str(year_of_proposal))
    
    return Response(thing, mimetype='application/pdf')

#saving pdf in databse
def file_save_gridfs(file_data,ctype,ctag,input_file_name,year_of_proposal):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username #current employee id
    file_ext = file_data.filename.rsplit('.', 1)[1]
    #today = datetime.datetime.now()
    #up_date =today.strftime('%d/%m/%Y')
    fs.put(file_data.read(), content_type=ctype, filename = str(input_file_name)+'.'+file_ext, uid = uid,
        tag = ctag,year_of_proposal= str(year_of_proposal))

@zen_proposal.route('/actualinvestmentproposal', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposal():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
    plantid=plant_pid[0]['plant_id1']
    plantname=plant_pid[0]['plant_desc']
#     emp_exist=find_and_filter('Investementproposal',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
#     if emp_exist == [] :
#         emp='NF'
#     else:
#         emp='F'
#     print"emmmm:",emp
    create_new=datetime.datetime.today()
    print "datetime",create_new.strftime("%d%m%Y")
    currentYear=datetime.datetime.today()
    yr=currentYear.year
    print "year",str(yr)
    if request.method == 'POST':

            form=request.form
            
            
                
            #print form
            emp_name=get_employee_name(uid)
            emp_exist=find_and_filter('InvestmentActuals',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
            print emp_exist
#             print "employee_year",emp_exist[0]['year_of_proposal']
            if emp_exist == [] :
               

                print "hello" 
                #print form           
                a=[]
                b=[]
                c=[]
                d=[]
                e=[]
                f=[]
                g=[]
                #print form['rad']
                #print form['rad1']
                #print form['R7']
                #print form['R8']
                #print form['R9']

                if form['rad'] == "Yes":
                    
                    a.append({"accomidationByComp":form['rad'],"emp_to_comp_per_month":form['R1'],
                        "emp_to_comp_No_of_month":form['R2'],"emp_to_comp_Tot_amount":form['R3'],
                        "comp_to_emp_per_month":form['R4'],"comp_to_emp_No_of_month":form['R5'],"comp_to_emp_Tot_amount":form['R6']});
                else:
                    
                    a.append({"accomidationByComp":form['rad'],"rent_paid_own_per_month":form['R7'],"ownner_pan":form['R8'],"owner_add":form['R9']});
                if form['rad1'] == 'Yes':
                    
                    b.append({"selfoccupied":form['rad1'],"loan_on_self":form['A1'],"loan_on_outlet":form['A2'],"rental_income":form['A3'],"municipal_tax":form['A4'],"date_of_occupation":form['A5'],"house_add":form['A6']});
                else:
                    
                    b.append({"selfoccupied":form['rad1']}) 
                    
                c.append({"medical_insurence":form['C1'],"medical_insu_for_parents":form['C2'],"section80DD":form['C3'],"section80DDB":form['C4'],"section80E":form['C5'],"section80U":form['C6']})
                
                d.append({"contribution_to_pension":form['CC1'],"life_insurance_premium":form['CC2'],"public_provident_fund":form['CC3'],"deposit_in_national_saving":form['CC4'],"interest_on_NSC":form['CC5'],"ulip":form['CC6'],"principal_loan":form['CC7'],"mutual_funds":form['CC8'],"elss":form['CC9'],"children_education_exp":form['CC10'],"fixed_deposits":form['CC11'],"notified_infrastructure_bonds":form['CC12'],"deferred_annuity":form['CC13'],"Others":form['CC14'],"total":form['CC15']})
                
                e.append({"gross_salary":form['P1'],"pre_PF":form['P2'],"pre_PT":form['P3'],"Pre_savings":form['P4']})
                
                f.append({"no_of_childerns":form['NC1'],"sch_going_child":form['NC2'],"stay_at_hostal":form['NC3'],"not_stay_in_hostal":form['NC4']})
                
                if form['rad2'] == 'Yes':
                    g.append({"form_no_12":form['rad2'],"accomidation":form['F1'],"cars_others":form['F2'],"sweeper":form['F3'],"Gas":form['F4'],"interest_free":form['F5'],"holiday_expenses":form['F6'],"free":form['F7'],"free_meals":form['F8'],"free_education":form['F9'],"gifts":form['F10'],"creditcard_exp":form['F11'],"club_exp":form['F12'],"use_of_assets":form['F13'],"transfer_of_assets":form['F14'],"values_of_others":form['F15'],"stock_options":form['F16'],"other_benefits":form['F17'],"house_loan":form['F18'],'vehicle_loan':form['F19'],"total_perquisites":form['F20'],"total_profits":form['F21']})
                else:
                    g.append({"form_no_12":form['rad2']})
                    


                array={"employee_id":uid,"rent_details":a,"section24_details":b,"section80EE_details":form['E1'],
                "chapterVIA":c,"section80CC":d,"section80CCG":form['GSS1'],"pre_emp_sal_details":e,"children_details":f,
                "perquisites":g,"year_of_proposal":str(yr),"entry_date":create_new.strftime("%d/%m/%Y"),"flag":"1",
                "employee_name":emp_name,"plant_name":plantname,'mobile_number':str(form['Mobile']),"email_id":str(form['Email'])};
                
                if request.files['rent_to_cmpny']:
                    input_file = request.files['rent_to_cmpny']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    


                if request.files['cmpny_to_emp']:
                    input_file = request.files['cmpny_to_emp']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['rent_period_year']:
                    input_file = request.files['rent_period_year']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['Interest_on_housing']:
                    input_file = request.files['Interest_on_housing']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['let_out_interest']:
                    input_file = request.files['let_out_interest']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['rental_income']:
                    input_file = request.files['rental_income']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['municipal_tax']:
                    input_file = request.files['municipal_tax']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['self_occupied']:
                    input_file = request.files['self_occupied']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['date_occupation']:
                    input_file = request.files['date_occupation']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['housing_address']:
                    input_file = request.files['housing_address']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['section_80_EE']:
                    input_file = request.files['section_80_EE']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['section_80_D_senr_ctzn']:
                    input_file = request.files['section_80_D_senr_ctzn']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)


                if request.files['med_insur_parents']:
                    input_file = request.files['med_insur_parents']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['section_80DD_disabled']:
                    input_file = request.files['section_80DD_disabled']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['section_80DB_medical']:
                    input_file = request.files['section_80DB_medical']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['edu_lone_intrst']:
                    input_file = request.files['edu_lone_intrst']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['sec_80U_self_hndcp']:
                    input_file = request.files['sec_80U_self_hndcp']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['cntrb_to_pnsn']:
                    input_file = request.files['cntrb_to_pnsn']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['lyf_insur_prem']:
                    input_file = request.files['lyf_insur_prem']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['PPF']:
                    input_file = request.files['PPF']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['NSC']:
                    input_file = request.files['NSC']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['intrst_on_NSC']:
                    input_file = request.files['intrst_on_NSC']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['ULIP']:
                    input_file = request.files['ULIP']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['Housing_Lone']:
                    input_file = request.files['Housing_Lone']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['mutual_funds']:
                    input_file = request.files['mutual_funds']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['ELSS']:
                    input_file = request.files['ELSS']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['children_tutn_fee']:
                    input_file = request.files['children_tutn_fee']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['fixed_deposits']:
                    input_file = request.files['fixed_deposits']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['tax_saving_bonds']:
                    input_file = request.files['tax_saving_bonds']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['Deferred_Annuity']:
                    input_file = request.files['Deferred_Annuity']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['others']:
                    input_file = request.files['others']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['80CCG']:
                    input_file = request.files['80CCG']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['gross_salary']:
                    input_file = request.files['gross_salary']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['previous_pf']:
                    input_file = request.files['previous_pf']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['previous_pt']:
                    input_file = request.files['previous_pt']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['previous_savings']:
                    input_file = request.files['previous_savings']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['no_of_childeren']:
                    input_file = request.files['no_of_childeren']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['school_going']:
                    input_file = request.files['school_going']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['hostel_staying']:
                    input_file = request.files['hostel_staying']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['home_staying']:
                    input_file = request.files['home_staying']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['Accomodation']:
                    input_file = request.files['Accomodation']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)


                if request.files['automotive']:
                    input_file = request.files['automotive']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    


                if request.files['Personal_attendant']:
                    input_file = request.files['Personal_attendant']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    


                if request.files['gas_elec_water']:
                    input_file = request.files['gas_elec_water']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['Concessional_loans']:
                    input_file = request.files['Concessional_loans']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['holiday_expenses']:
                    input_file = request.files['holiday_expenses']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    


                if request.files['Concessional_Travel']:
                    input_file = request.files['Concessional_Travel']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['free_meals']:
                    input_file = request.files['free_meals']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['free_education']:
                    input_file = request.files['free_education']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['gift_vouchers']:
                    input_file = request.files['gift_vouchers']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['credit_card_expnses']:
                    input_file = request.files['credit_card_expnses']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['club_expenses']:
                    input_file = request.files['club_expenses']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['movable_assets']:
                    input_file = request.files['movable_assets']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['trnsfr_of_assets']:
                    input_file = request.files['trnsfr_of_assets']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['any_other_benifits']:
                    input_file = request.files['any_other_benifits']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['stock_options']:
                    input_file = request.files['stock_options']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['other_benifits']:
                    input_file = request.files['other_benifits']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['house_lone']:
                    input_file = request.files['house_lone']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)


                if request.files['vehicle_lone']:
                    input_file = request.files['vehicle_lone']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                if request.files['value_of_perquisites']:
                    input_file = request.files['value_of_perquisites']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                    

                if request.files['value_of_profit']:
                    input_file = request.files['value_of_profit']
                    file_name= str(uid)+input_file.name
                    file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                save_collection('InvestmentActuals',array) 
                flash("Your Record saved successfully",'alert-success')
                return redirect(url_for('zenproposal.investmentproposal')) 
            else:
                print "employee_year",emp_exist[0]['year_of_proposal'] 
                yy=str(yr)
                if yy == emp_exist[0]['year_of_proposal'] :
                    flash("your record already entered",'alert-success')
                else:
                    a=[]
                    b=[]
                    c=[]
                    d=[]
                    e=[]
                    f=[]
                    g=[]
                    if form['rad'] == "Yes":
                        #print "R1 Empty"
                        a.append({"accomidationByComp":form['rad'],"emp_to_comp_per_month":form['R1'],
                            "emp_to_comp_No_of_month":form['R2'],
                            "emp_to_comp_Tot_amount":form['R3'],
                            "comp_to_emp_per_month":form['R4'],
                            "comp_to_emp_No_of_month":form['R5'],
                            "comp_to_emp_Tot_amount":form['R6']});
                    else:
                        a.append({"accomidationByComp":form['rad'],"rent_paid_own_per_month":form['R7'],"ownner_pan":form['R8'],"owner_add":form['R9']});
                    
                    if form['rad1'] == 'Yes':
                        b.append({"selfoccupied":form['rad1'],"loan_on_self":form['A1'],"loan_on_outlet":form['A2'],"rental_income":form['A3'],"municipal_tax":form['A4'],"date_of_occupation":form['A5'],"house_add":form['A6']});
                    else:
                        b.append({"selfoccupied":form['rad1']})   
                    c.append({"medical_insurence":form['C1'],"medical_insu_for_parents":form['C2'],"section80DD":form['C3'],"section80DDB":form['C4'],"section80E":form['C5'],"section80U":form['C6']})
                    d.append({"contribution_to_pension":form['CC1'],"life_insurance_premium":form['CC2'],"public_provident_fund":form['CC3'],"deposit_in_national_saving":form['CC4'],"interest_on_NSC":form['CC5'],"ulip":form['CC6'],"principal_loan":form['CC7'],"mutual_funds":form['CC8'],"elss":form['CC9'],"children_education_exp":form['CC10'],"fixed_deposits":form['CC11'],"notified_infrastructure_bonds":form['CC12'],"deferred_annuity":form['CC13'],"Others":form['CC14'],"total":form['CC15']})
                    e.append({"gross_salary":form['P1'],"pre_PF":form['P2'],"pre_PT":form['P3'],"Pre_savings":form['P4']})
                    f.append({"no_of_childerns":form['NC1'],"sch_going_child":form['NC2'],"stay_at_hostal":form['NC3'],"not_stay_in_hostal":form['NC4']})
                    if form['rad2'] == 'Yes':
                        g.append({"form_no_12":form['rad2'],"accomidation":form['F1'],"cars_others":form['F2'],"sweeper":form['F3'],"Gas":form['F4'],"interest_free":form['F5'],"holiday_expenses":form['F6'],"free":form['F7'],"free_meals":form['F8'],"free_education":form['F9'],"gifts":form['F10'],"creditcard_exp":form['F11'],"club_exp":form['F12'],"use_of_assets":form['F13'],"transfer_of_assets":form['F14'],"values_of_others":form['F15'],"stock_options":form['F16'],"other_benefits":form['F17'],"house_loan":form['F18'],'vehicle_loan':form['F19'],"total_perquisites":form['F20'],"total_profits":form['F21']})
                    else:
                        g.append({"form_no_12":form['rad2']})
                    
                   
                    array={"employee_id":uid,"rent_details":a,"section24_details":b,"section80EE_details":form['E1'],
                    "chapterVIA":c,"section80CC":d,"section80CCG":form['GSS1'],"pre_emp_sal_details":e,"children_details":f,
                    "perquisites":g,"year_of_proposal":str(yr),"entry_date":create_new.strftime("%d/%m/%Y"),"flag":"1",
                    "employee_name":emp_name,"plant_name":plantname,'mobile_number':str(form['Mobile']),"email_id":str(form['Email'])};

                    if request.files['rent_to_cmpny']:
                        input_file = request.files['rent_to_cmpny']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        


                    if request.files['cmpny_to_emp']:
                        input_file = request.files['cmpny_to_emp']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['rent_period_year']:
                        input_file = request.files['rent_period_year']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['Interest_on_housing']:
                        input_file = request.files['Interest_on_housing']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['let_out_interest']:
                        input_file = request.files['let_out_interest']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['rental_income']:
                        input_file = request.files['rental_income']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['municipal_tax']:
                        input_file = request.files['municipal_tax']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['self_occupied']:
                        input_file = request.files['self_occupied']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['date_occupation']:
                        input_file = request.files['date_occupation']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['housing_address']:
                        input_file = request.files['housing_address']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['section_80_EE']:
                        input_file = request.files['section_80_EE']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['section_80_D_senr_ctzn']:
                        input_file = request.files['section_80_D_senr_ctzn']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)


                    if request.files['med_insur_parents']:
                        input_file = request.files['med_insur_parents']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['section_80DD_disabled']:
                        input_file = request.files['section_80DD_disabled']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['section_80DB_medical']:
                        input_file = request.files['section_80DB_medical']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['edu_lone_intrst']:
                        input_file = request.files['edu_lone_intrst']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['sec_80U_self_hndcp']:
                        input_file = request.files['sec_80U_self_hndcp']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['cntrb_to_pnsn']:
                        input_file = request.files['cntrb_to_pnsn']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['lyf_insur_prem']:
                        input_file = request.files['lyf_insur_prem']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['PPF']:
                        input_file = request.files['PPF']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['NSC']:
                        input_file = request.files['NSC']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['intrst_on_NSC']:
                        input_file = request.files['intrst_on_NSC']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['ULIP']:
                        input_file = request.files['ULIP']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['Housing_Lone']:
                        input_file = request.files['Housing_Lone']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['mutual_funds']:
                        input_file = request.files['mutual_funds']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['ELSS']:
                        input_file = request.files['ELSS']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['children_tutn_fee']:
                        input_file = request.files['children_tutn_fee']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['fixed_deposits']:
                        input_file = request.files['fixed_deposits']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['tax_saving_bonds']:
                        input_file = request.files['tax_saving_bonds']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['Deferred_Annuity']:
                        input_file = request.files['Deferred_Annuity']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['others']:
                        input_file = request.files['others']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['80CCG']:
                        input_file = request.files['80CCG']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['gross_salary']:
                        input_file = request.files['gross_salary']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['previous_pf']:
                        input_file = request.files['previous_pf']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['previous_pt']:
                        input_file = request.files['previous_pt']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['previous_savings']:
                        input_file = request.files['previous_savings']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['no_of_childeren']:
                        input_file = request.files['no_of_childeren']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['school_going']:
                        input_file = request.files['school_going']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['hostel_staying']:
                        input_file = request.files['hostel_staying']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['home_staying']:
                        input_file = request.files['home_staying']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['Accomodation']:
                        input_file = request.files['Accomodation']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)


                    if request.files['automotive']:
                        input_file = request.files['automotive']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        


                    if request.files['Personal_attendant']:
                        input_file = request.files['Personal_attendant']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        


                    if request.files['gas_elec_water']:
                        input_file = request.files['gas_elec_water']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['Concessional_loans']:
                        input_file = request.files['Concessional_loans']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['holiday_expenses']:
                        input_file = request.files['holiday_expenses']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        


                    if request.files['Concessional_Travel']:
                        input_file = request.files['Concessional_Travel']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['free_meals']:
                        input_file = request.files['free_meals']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['free_education']:
                        input_file = request.files['free_education']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['gift_vouchers']:
                        input_file = request.files['gift_vouchers']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['credit_card_expnses']:
                        input_file = request.files['credit_card_expnses']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['club_expenses']:
                        input_file = request.files['club_expenses']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['movable_assets']:
                        input_file = request.files['movable_assets']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['trnsfr_of_assets']:
                        input_file = request.files['trnsfr_of_assets']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['any_other_benifits']:
                        input_file = request.files['any_other_benifits']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['stock_options']:
                        input_file = request.files['stock_options']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['other_benifits']:
                        input_file = request.files['other_benifits']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['house_lone']:
                        input_file = request.files['house_lone']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)


                    if request.files['vehicle_lone']:
                        input_file = request.files['vehicle_lone']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    if request.files['value_of_perquisites']:
                        input_file = request.files['value_of_perquisites']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)
                        

                    if request.files['value_of_profit']:
                        input_file = request.files['value_of_profit']
                        file_name= str(uid)+input_file.name
                        file_save_gridfs(input_file,"application/pdf","doc",file_name,yr)

                    save_collection('InvestmentActuals',array)
                    flash("Your Record saved successfully",'alert-success')
                    return redirect(url_for('zenproposal.actualinvestmentproposal')) 
    return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM.html', user_details=user_details,year=currentYear.year,
        date=currentYear)
                            



@zen_proposal.route('/actualinvestmentproposallistview', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposallistview():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    list=find_all_in_collection('InvestmentActuals')
#     print "resutls",list
    if request.method == 'POST':
            form=request.form

            print "Id",form['prid']
            currentYear=datetime.datetime.today()
            yr=currentYear.year
            create_new=datetime.datetime.today()
            print "datetime",create_new.strftime("%d%m%Y")  
            emp_data=db.InvestmentActuals.aggregate([{'$match':{'_id':ObjectId(form['prid'])}},
                {'$unwind':'$children_details'},{'$unwind':'$rent_details'},{'$unwind':'$section24_details'},
                {'$unwind':'$chapterVIA'},{'$unwind':'$section80CC'},{'$unwind':'$pre_emp_sal_details'},
                {'$unwind':'$perquisites'}])
            emp_data=list(emp_data)
            try:
                print "UID",form['user_id']
                print "Year" ,form['year_of_proposal']
                user_docs=find_in_collection('fs.files',{"uid" : str(form['user_id']),"tag" : "doc",
                    "year_of_proposal":str(form['year_of_proposal'])})
                
            except:
                print "none"

            return render_template('INVESTMENT_ACTUAL_EMPLOYEE_DETAILS.html', user_details=user_details,
                emp_data=emp_data,year=currentYear.year,date=currentYear,user_documents=user_docs,
                user_id=str(form['user_id']))
            
    return render_template('INVESTMENT_PROPOSAL_ACTUAL_LIST_VIEW.html', user_details=user_details,list=list)

