from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
import csv
import os
import cgi
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from zenapp.modules.leave.lfunctions import *
import logging
#from pyrfc import *
import json
import random
import sys
from array import array
import os
import smtplib
import email.utils
from email.mime.text import MIMEText

zen_proposal = Blueprint('zenproposal', __name__, url_prefix='/investment')

def notification(message,receiver,emp_name):
    USERNAME='myportal@pennacement.com'
    PASSWORD='Penna@123'
    msg = MIMEText('Dear '+emp_name+',\n\n'+message+'\n\n\n\nThanks,\nTeam-Finance.\n\nNote:For Any Furthur Assistance Please Contact DK Mallikarjun Ext-316 HO.')
    msg['Subject'] = 'Investment Proofs Notification'
    server = smtplib.SMTP("smtp.office365.com:587")
    server.starttls()
    server.login(USERNAME,PASSWORD)
    try:
        server.sendmail(USERNAME,receiver,str(msg))     
        server.quit()
    except Exception,R:
        print R

@zen_proposal.route('/actualinvestmentproposal_approval/', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposal_approval():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    create_new=datetime.datetime.today()
    currentYear=datetime.datetime.today()
    yr=currentYear.year
    if request.method == 'POST':
        form=request.form
        if 'submit' in form:
            if form['submit']=='View Details':
                if form['curr_user_plant_id']=='GC':
                    list_data=find_in_collection('Investment_Details',{'$or':[{'Payroll_area':'GP'},{'Payroll_area':'GC'}],"FY_YEAR":form['FY_YEAR'],"SUBMIT_STATUS":"SUBMITED","APPROVE_STATUS":{'$exists':0}})
                else:
                    list_data=find_in_collection('Investment_Details',{'Payroll_area':form['curr_user_plant_id'],"FY_YEAR":form['FY_YEAR'],"SUBMIT_STATUS":"SUBMITED","APPROVE_STATUS":{'$exists':0}})
                return render_template('INVESTMENT_ACTUAL_EMPLOYEES_DETAILS_LIST_VIEW.html', user_details=user_details,
                    emp_data=list_data)
            if form['submit']=='VIEW':
                requested_user=form['user_id']
            elif form['submit']=='Approve':
                requested_user=form['emp_id_update'][3:]
                if 'APPROVAL_Field' in form:
                    updt_data={}
                    # print form['APPROVAL_Field'],'-----',int(form['I584_S24_INTREST_AMT_APPROVED']),' -- ',form['I584_FINAL_LET_AMT_APPROVED'],' ----- ',form['I584_S24_OTHERS_AMT_APPROVED']
                    if form['APPROVAL_Field']=='I584_S24_INTREST_AMT_APPROVED':
                        for jj in ['I584_FINAL_LET_AMT_APPROVED','I584_S24_INTREST_AMT_APPROVED','I584_S24_REPAIR_AMT_APPROVED','I584_S24_OTHERS_AMT_APPROVED']:
                            # print int(form[jj]),'    ---'
                            if float(form[jj])!=0:
                                updt_data.update({jj:form[jj]})
                                update_collection('Investment_Details',{'EMPLOYEE_ID':form['emp_id_update'],'FY_YEAR':form['FY_YEAR']},{'$set':updt_data})
                            # if int(form['I584_FINAL_LET_AMT_APPROVED'])!=0:
                            #     updt_data.update({'I584_FINAL_LET_AMT_APPROVED':form['I584_FINAL_LET_AMT_APPROVED']})
                            
                            # if int(form['I584_S24_OTHERS_AMT_APPROVED'])!=0:
                            #     updt_data.update({'I584_S24_OTHERS_AMT_APPROVED':form['I584_S24_OTHERS_AMT_APPROVED']})
                            
                            # if int(form['I584_S24_REPAIR_AMT_APPROVED'])!=0:
                            #     updt_data.update({'I584_S24_REPAIR_AMT_APPROVED':form['I584_S24_REPAIR_AMT_APPROVED']})
                    else:
                        if form['APPROVAL_Field']=='I580_docs':
                            # print form,' testing I580'
                            updt_data={'I580_SALARY_AMT_APPROVED':form['I580_SALARY_AMT'],'I580_VALUE_PREREQ_AMT_APPROVED':form['I580_VALUE_PREREQ_AMT'],
                            'I580_PROFIT_AMT_APPROVED':form['I580_PROFIT_AMT'],'I580_EXMPT_AMT_APPROVED':form['I580_EXMPT_AMT'],
                            'I580_PROF_TAX_AMT_APPROVED':form['I580_PROF_TAX_AMT'],'I580_PROV_FUND_AMT_APPROVED':form['I580_PROV_FUND_AMT'],
                            'I580_INCOM_TAX_AMT_APPROVED':form['I580_INCOM_TAX_AMT'],'I580_SURCHRG_AMT_APPROVED':form['I580_SURCHRG_AMT'],
                            'I580_EDU_CESS_AMT_APPROVED':form['I580_EDU_CESS_AMT'],'I580_MED_EXMPT_AMT_APPROVED':form['I580_MED_EXMPT_AMT'],
                            'I580_LEAVE_ENCASH_EXMPT_AMT_APPROVED':form['I580_LEAVE_ENCASH_EXMPT_AMT'],'I580_GRAT_EXMPT_AMT_APPROVED':form['I580_GRAT_EXMPT_AMT'],
                            'I580_VRS_EXMPT_AMT_APPROVED':form['I580_VRS_EXMPT_AMT'],'I580_NUM_LTA_EXMPT_AMT_APPROVED':form['I580_NUM_LTA_EXMPT_AMT'],
                            'I580_LTA_EXMPT_X_APPROVED':form['I580_LTA_EXMPT_X'] }
                        else:
                            infotype_name=form['APPROVAL_Field']
                            infotype_val=form[infotype_name]                
                            # print infotype_val,'  updated approved value in investment proofs --- ',infotype_name
                            updt_data={infotype_name:infotype_val}
                    update_collection('Investment_Details',{'EMPLOYEE_ID':form['emp_id_update'],'FY_YEAR':form['FY_YEAR']},{'$set':updt_data})
                    flash('Investment Details Has Been Saved Sucessfully','alert-success')
            elif form['submit']=='Reject':
                requested_user=form['emp_id_update'][3:]
        
            details={}
            doj_flag=datetime.datetime.strptime(get_userdetails(requested_user)['doj'],'%d-%m-%Y')
            if doj_flag>datetime.datetime(2016,4,1):
                doj_flag='1'
            else:
                doj_flag='0'
            #viewing single employee investment actuals
            VIEW_TD=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':'000'+requested_user,'FY_YEAR':'2016-2017'})
            total_directories=os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+requested_user)
            # print len(total_directories),'   ',form['user_id']
            for ii in total_directories:
                details[ii]={}
                total_sub_directories=os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+requested_user+'/'+ii)
                subdetails=[]
                if len(total_sub_directories)!=0:
                    total_files=os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+requested_user+'/'+ii)
                    for jj in total_files:
                        subdetails.append(jj)
                details.update({ii:subdetails})
            return render_template('INVESTMENT_ACTUAL_EMPLOYEE_DETAILS_APPROVAL.html',testdetails=details,user_details=user_details,year=currentYear.year,date=currentYear,TD_RESULT=VIEW_TD,EMP_DETAILS=get_userdetails(requested_user),emp_doj_flag=doj_flag)
        if 'send_email_submit' in form:
            notification(form['send_email_msg'],form['receiver_email_id'],form['emp_name_update'])
            requested_user=form['emp_id_update'][3:]
            details={}
            doj_flag=datetime.datetime.strptime(get_userdetails(requested_user)['doj'],'%d-%m-%Y')
            if doj_flag>datetime.datetime(2016,4,1):
                doj_flag='1'
            else:
                doj_flag='0'
            #viewing single employee investment actuals
            VIEW_TD=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':'000'+requested_user,'FY_YEAR':'2016-2017'})
            total_directories=os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+requested_user)
            # print len(total_directories),'   ',form['user_id']
            for ii in total_directories:
                details[ii]={}
                total_sub_directories=os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+requested_user+'/'+ii)
                subdetails=[]
                if len(total_sub_directories)!=0:
                    total_files=os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+requested_user+'/'+ii)
                    for jj in total_files:
                        subdetails.append(jj)
                details.update({ii:subdetails})
            flash('Mail Notification Has Been Sent Sucessfully.','alert-success')
            return render_template('INVESTMENT_ACTUAL_EMPLOYEE_DETAILS_APPROVAL.html',testdetails=details,user_details=user_details,year=currentYear.year,date=currentYear,TD_RESULT=VIEW_TD,EMP_DETAILS=get_userdetails(requested_user),emp_doj_flag=doj_flag)
        
        if 'finalsubmit' in form:
            if form['finalsubmit']=='Confirm Approval':
                # print form['finalsubmit'],'test '
                flash('Employee Details Updated Sucessfully','alert-success')
                update_collection('Investment_Details',{'EMPLOYEE_ID':form['emp_id_update'],'FY_YEAR':form['FY_YEAR']},{'$set':{'APPROVE_STATUS':'APPROVED','APPROVED_TIME':datetime.datetime.now(),'approved_by':form['approved_by']}})
                return render_template('INVESTMENT_ACTUAL_EMPLOYEES_DETAILS_LIST_VIEW.html', user_details=user_details)
    return render_template('INVESTMENT_ACTUAL_EMPLOYEES_DETAILS_LIST_VIEW.html', user_details=user_details)


def get_userdetails(userid):
    print userid
    usr_details=find_one_in_collection('Userinfo',{'username':userid})
    return usr_details

####### report for checking how many have submitted the data
@zen_proposal.route('/investmentproposallistview', methods = ['GET', 'POST'])
@login_required
def investmentproposallistview():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    payroll_area=user_details['plant_id']    
    return render_template('INVESTMENTPROPOSAL_LIST_VIEW.html', user_details=user_details,payroll_area=payroll_area)

@zen_proposal.route('/employee_list_ajax/', methods = ['GET', 'POST'])
def employee_list_ajax():
    if request.method=='POST':
        form=request.form
        employee_list=find_in_collection('Investment_Details',{"FY_YEAR":form['FY_YEAR']})
        emp_details=[]
        for i in employee_list:
            temp_usr_details=get_userdetails(i['EMPLOYEE_ID'][3:])            
            if 'SAVE_STATUS' in i and 'SUBMIT_STATUS' not in i: #saved the form and not submitted
                status='Saved'
                status_time=str(i['LAST_SAVED'])
            elif 'SAVE_STATUS' in i and 'SUBMIT_STATUS' in i: #saved and submitted
                if "APPROVE_STATUS" in i:
                    status='Approved'
                    status_time=str(i['APPROVED_TIME'])
                else:
                    status='submitted'
                    status_time=str(i['SUBMIT_TIME'])
            elif 'SAVE_STATUS' not in i and 'SUBMIT_STATUS' in i: #directly submitted
                if "APPROVE_STATUS" in i:
                    status='Approved'
                    status_time=str(i['APPROVED_TIME'])
                else:
                    status='submitted'
                    status_time=str(i['SUBMIT_TIME'])
            else:
                status='Not Intiated'
                status_time='-'
            try:
                Name=i['Name']
                Payroll_area=i['Payroll_area']
            except:
                try:
                    Name=temp_usr_details['f_name']+' '+temp_usr_details['l_name']
                    Payroll_area=temp_usr_details['plant_id']
                except:
                    Name=''
                    Payroll_area=''
            if Payroll_area==form['curr_user_plant_id']:
                emp_details.append([Name,str(i['EMPLOYEE_ID'][3:]),Payroll_area,status,status_time])
    return jsonify({'result':emp_details})


# @zen_proposal.route('/UPDATE_TO_SAP_BATCH/', methods = ['GET', 'POST'])
# def UPDATE_TO_SAP_BATCH():
#     debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
#     if debug_flag and debug_flag['debug']=='on':
#         debug_flag=debug_flag['debug_status']['ip_approve']
#     else:
#         debug_flag="off" 
#     verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2016-2017','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED','sap_update':{'$exists':0}})
#     final_result_to_sap=[]
#     CH_TAX_DATA_TABLE=[]
#     CH_HRA_RENTS_TABLE=[]
#     for user in  verified_TD_Details:
#         if debug_flag=='on':
#             logging.info(str(form))
#         dates_range_year={'APR':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'MAY':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'JUN':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'JUL':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'AUG':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'SEP':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'OCT':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'NOV':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'DEC':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'JAN':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'FEB':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,28)},
#         'MAR':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)}}
#         RESULT=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':user['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'})
#         list_of_fields_I580=['I580_SALARY_AMT','I580_VALUE_PREREQ_AMT','I580_PROFIT_AMT','I580_EXMPT_AMT','I580_PROF_TAX_AMT','I580_PROV_FUND_AMT','I580_INCOM_TAX_AMT','I580_SURCHRG_AMT','I580_EDU_CESS_AMT','I580_MED_EXMPT_AMT','I580_LEAVE_ENCASH_EXMPT_AMT','I580_GRAT_EXMPT_AMT','I580_VRS_EXMPT_AMT','I580_NUM_LTA_EXMPT_AMT','I580_LTA_EXMPT_X']
#         list_of_fields_I582=['I582_SCEA_X','I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT','I582_MDA_X','I582_MDA_START_DATE','I582_MDA_EXMPT_AMT','I582_LTA_X','I582_LTA_START_DATE','I582_LTA_EXMPT_AMT','I582_LTA_OBJPS','I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE','I582_LTA_STRT_LOC','I582_LTA_END_LOC','I582_LTA_TRAVEL_MODE','I582_LTA_TRAVEL_CLASS','I582_LTA_TKT_NUMBERS','I582_LTA_SELF_TRAVLD']
#         list_of_fields_I584=['I584_LOAN_TYPE','I584_LOAN_SEQ_NUM','I584_ACC_MONTH_SRT','I584_ACC_MONTH_END','I584_S24_LIMIT_AMT','I584_PAYROLL_LIMIT_AMT','I584_FINAL_LET_AMT','I584_S24_REPAIR_AMT','I584_S24_INTREST_AMT','I584_S24_OTHERS_AMT','I584_TDS_OTHERS_AMT','I584_PRFT_BUS_PROF_AMT','I584_LT_CAP_GAIN_N_AMT','I584_LT_CAP_GAIN_S_AMT','I584_ST_CAP_GAIN_AMT','I584_DIVIDENDS_AMT','I584_INTERESTS_AMT','I584_OTH_SOURCES_AMT']
#         list_of_fields_I585=['I585_A1_PENSIONS_AMT','I585_A2_INS_N_SCTZ_AMT','I585_A3_INS_SCTZ_AMT','I585_A4_INS_P_NSCTZ_AMT','I585_A5_INS_P_SCTZ_AMT','I585_A6_HLT_CHK_SELF_AMT','I585_A7_HLT_CHK_PARNT_AMT','I585_A8_EXP_VSN_SELF_AMT','I585_A9_EXP_VSN_PARNT_AMT','I585_A10_DISABLED_AMT','I585_A11_SDISABLED_AMT','I585_A12_MED_TRMT_AMT','I585_A13_MED_TRMT_SCTZ_AMT','I585_A14_MED_TRMT_VSCTZ_AMT','I585_A15_INT_HIGH_EDU_AMT','I585_A16_CRY_AMT','I585_A17_PMCM_RELIEF_AMT','I585_A18_NAT_CHIL_AMT','I585_A19_SWAS_BHT_KSH_AMT','I585_A20_CLN_GANGA_AMT','I585_A21_DRUG_ABUS_AMT','I585_A22_RENT_PAID_AMT','I585_A23_FORN_SOURC_AMT','I585_A24_SRV_ABROAD_AMT','I585_A25_SELF_DISABLE_AMT','I585_A27_PENSN_CENGOV_AMT','I585_A28_INTR_SAV_ACC_AMT','I585_A29_RG_ESS_AMT','I585_A30_HOMELOAN_INTR_AMT']
#         list_of_fields_I586=['I586_A1_LIFE_INSU_AMT','I586_A2_SUPER_FUND_AMT','I586_A3_NSS_AMT','I586_A4_ULIP_AMT','I586_A5_EQUITY_AMT','I586_A6_INFRA_MF_AMT','I586_A7_PPF_AMT','I586_A8_RPF_AMT','I586_A9_POST_OFFICE_AMT','I586_A10_NSC_AMT','I586_A11_ULIP_LIC_AMT','I586_A12_PF_AMT','I586_A13_HOUSE_LOAN_AMT','I586_A14_NSC_VIII_AMT','I586_A15_ANNUITY_LIC_AMT','I586_A16_MF_AMT','I586_A17_PENSION_AMT','I586_A18_NHB_AMT','I586_A19_OTH_SCHEME_AMT','I586_A20_DEF_ANTY_SP_AMT','I586_A21_DEF_ANUITY_AMT','I586_A22_TUTE_FEE_1_AMT','I586_A23_TUTE_FEE_2_AMT','I586_A24_TERM_DEPOSIT_AMT','I586_A25_PO_5YEAR_AMT','I586_A26_SCSS_AMT','I586_A27_SSS_AMT']
#         updt_arr={'EMPLOYEE_ID':user['EMPLOYEE_ID']}
#         test_updt_arry={}
#         ## not sending accom text in 581 infotype
#         for month in ['APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC','JAN','FEB','MAR']:
#             if 'I581_HRA_EXMPT_X' in RESULT:
#                 I581_HRA_EXMPT_X=RESULT['I581_HRA_EXMPT_X']
#             else:
#                 I581_HRA_EXMPT_X=''
#             if RESULT['I581_ACCOM_TYPE']=="1":
#                 updt_data_hra={'EMPLOYEE_ID':user['EMPLOYEE_ID'],'I581_RENT_MONTH_NAME': month,'I581_HRA_EXMPT_X':I581_HRA_EXMPT_X,'I581_ACCOM_TYPE': RESULT['I581_ACCOM_TYPE'], 'I581_RENT': RESULT['I581_'+month+'_RENT'],'I581_OWNER_ADDRESS2':'','I581_OWNER_ADDRESS3': '', 'I581_TOT_FURN_VAL':'0', 'I581_HELPRS_AMT': '0',  'I581_RENT_START_DATE': dates_range_year[month]['st_dt'], 'I581_OWNER_ADDRESS1': '',  'I581_METRO': '', 'I581_HELPERS': '00000',  'I581_RENT_END_DATE':dates_range_year[month]['end_dt'], 'I581_HIRE_CRG_FURN': '0'}
#                 updt_arr.update({'I581_X':'X'})
#                 CH_HRA_RENTS_TABLE.append(dict(updt_data_hra))
#             else:
#                 logging.info('accomodation type selected as - '+RESULT['I581_ACCOM_TYPE'])
#         # print CH_HRA_RENTS_TABLE,' -------------- '
#         # print updt_arr_hra,' ..........................'
#         for field in list_of_fields_I580:            
#             if field+'_APPROVED' in RESULT:
#                 updt_arr.update({field:RESULT[field+'_APPROVED'],'I580_X':'X'})
#             else:
#                 updt_arr.update({field:'0'})
#             # if user['EMPLOYEE_ID']=='00002333':
#             #     print updt_arr,' I580 NEW JOINEE' 

#         #### check the field LTA start date what it hasto be filled and sent to SAP
#         # if float(RESULT['I582_MDA_EXMPT_AMT']) != 0 or RESULT['I582_MDA_EXMPT_AMT']!='':
#         #     # I582_MDA_X:[I582_MDA_START_DATE','I582_MDA_EXMPT_AMT]
#         #     updt_arr.update({'I582_MDA_X':'X','I582_MDA_START_DATE':datetime.date(2017,2,20),'I582_MDA_EXMPT_AMT':RESULT['I582_MDA_EXMPT_AMT'],'I582_X':'X'})
        
#         #### check the field LTA start date what it hasto be filled and sent to SAP
#         if float(RESULT['I582_SCEA_EXMPT_AMT']) != 0 or RESULT['I582_SCEA_EXMPT_AMT']!='':          
#             updt_arr.update({'I582_SCEA_X':'X'})
#             # I582_SCEA_X:[I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT]
#             updt_arr.update({'I582_SCEA_START_DATE':datetime.date(2017,2,20),'I582_SCEA_EXMPT_AMT':RESULT['I582_SCEA_EXMPT_AMT'],'I582_X':'X'})
        
#         # if float(RESULT['I582_LTA_EXMPT_AMT']) != 0 or RESULT['I582_LTA_EXMPT_AMT']!='':
#         #     # I582_LTA_X:[I582_LTA_START_DATE','I582_LTA_EXMPT_AMT','I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE','I582_LTA_STRT_LOC','I582_LTA_END_LOC','I582_LTA_TRAVEL_MODE','I582_LTA_TRAVEL_CLASS','I582_LTA_TKT_NUMBERS','I582_LTA_SELF_TRAVLD]
#         #     #### check the field LTA start date what it hasto be filled and sent to SAP
#         #     if RESULT['I582_LTA_JOURNY_START_DATE']!='None':
#            #      updt_arr.update({'I582_LTA_X':'X'})
#            #      for field in list_of_fields_I582:
#            #          if field in RESULT:
#            #              # print user['EMPLOYEE_ID'],'emppp'
#            #              if field in ['I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE']:
#            #                  # print {field:datetime.date(2017,2,20)},' I582 LTA '
#            #                  try:
#            #                      updt_arr.update({field:datetime.datetime.strptime(RESULT[field],'%Y/%m/%d')})
#            #                  except:
#            #                      date_conv=datetime.datetime.strptime(RESULT[field],'%d/%m/%Y').strftime('%Y/%m/%d')
#            #                      updt_arr.update({field:datetime.datetime.strptime(date_conv,'%Y/%m/%d')})
#            #                  # updt_arr.update({field:datetime.date(2017,2,20)})
#            #              elif field in ['I582_LTA_START_DATE']:
#            #                updt_arr.update({field:datetime.date(2017,2,20)})
#            #              elif field in ['I582_LTA_TRAVEL_MODE','I582_LTA_TKT_NUMBERS','I582_LTA_STRT_LOC','I582_LTA_STRT_LOC','I582_LTA_END_LOC']:
#            #                  if 'I582_LTA_END_LOC' in RESULT and RESULT['I582_LTA_END_LOC'] !='':
#            #                      updt_arr.update({field:RESULT[field]})
#            #                  else:
#            #                      updt_arr.update({field:''})   
#            #      if 'I582_LTA_EXMPT_AMT_APPROVED' in RESULT:
#            #          updt_arr.update({'I582_LTA_EXMPT_AMT':RESULT['I582_LTA_EXMPT_AMT_APPROVED'],'I582_X':'X'})   

#         #currently using fields in 584:
#         if RESULT['I584_SUBTYPE']=='0001':
#             if RESULT['I584_HOME_FUL_RNT_X']=='X':
#                 updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})
#             elif RESULT['I584_HOME_SELF_X']=='X':
#                 updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_HOME_SELF_X':'X','I584_X':'X'})
#             elif RESULT['I584_HOME_PART_RNT_X']=='X':
#                 updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_PART_RNT_X':'X','I584_X':'X'})

#         elif RESULT['I584_SUBTYPE']=='1':
#             updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_HOME_SELF_X':'X','I584_X':'X'})
#         elif RESULT['I584_SUBTYPE']=='2':
#             updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_PART_RNT_X':'X','I584_X':'X'})
#         elif RESULT['I584_SUBTYPE']=='3':
#             updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})

#         #INCOME FROM OTHER THAN 80C
#         for field in list_of_fields_I585:
#             # print field+'_APPROVED'
#             if field+'_APPROVED' in RESULT:
#                 # print RESULT[field+'_APPROVED']
#                 updt_arr.update({field:RESULT[field+'_APPROVED'],'I585_ACTUAL_X':'X','I585_X':'X'})
#             else:
#                 updt_arr.update({field:'0'})
#         # print '------------------'
#         #INCOME FROM OTHER SOURCES
#         for field1 in list_of_fields_I586:
#             if field1+'_APPROVED' in RESULT:
#                 # print RESULT[field1+'_APPROVED']
#                 updt_arr.update({field1:RESULT[field1+'_APPROVED'],'I586_ACTUAL_X':'X','I586_X':'X'})
#             else:
#                 updt_arr.update({field1:'0'})
#         # print updt_arr,'final update array'
#         # print '-------++++++++++++++++++-------------'
#         CH_TAX_DATA_TABLE.append(dict(updt_arr))
#     # print CH_TAX_DATA_TABLE
#     # print type(CH_HRA_RENTS_TABLE)
#     # testdata={'I585_A22_RENT_PAID_AMT': Decimal('22.00'), 'I580_INCOM_TAX_AMT': Decimal('7.00'), 'I586_P3_NSS_AMT': Decimal('3.00'), 'I585_A5_INS_P_SCTZ_AMT': Decimal('5.00'), 'I586_STATUS': '', 'I585_A2_INS_N_SCTZ_AMT': Decimal('2.00'), 'I585_A3_INS_SCTZ_AMT': Decimal('3.00'), 'I585_A8_EXP_VSN_SELF_AMT': Decimal('8.00'), 'I586_P27_SSS_AMT': Decimal('27.00'), 'I585_A12_MED_TRMT_AMT': Decimal('12.00'), 'I585_A19_SWAS_BHT_KSH_AMT': Decimal('19.00'), 'I586_A20_DEF_ANTY_SP_AMT': Decimal('20.00'), 'I584_PRFT_BUS_PROF_AMT': Decimal('0.00'), 'I581_STATUS': '', 'I580_MED_EXMPT_AMT': Decimal('10.00'), 'I582_STATUS': '', 'I586_A24_TERM_DEPOSIT_AMT': Decimal('24.00'), 'I584_ST_CAP_GAIN_AMT': Decimal('0.00'), 'I582_LTA_SELF_TRAVLD': 'Y', 'I585_X': '', 'I580_PROF_TAX_AMT': Decimal('5.00'), 'I585_A25_SELF_DISABLE_AMT': Decimal('25.00'), 'I585_P27_PENSN_CENGOV_AMT': Decimal('27.00'), 'I582_LTA_START_DATE': datetime.date(2017, 3, 20), 'I586_A23_TUTE_FEE_2_AMT': Decimal('23.00'), 'I585_PROVIS_X': '', 'I585_P3_INS_SCTZ_AMT': Decimal('3.00'), 'I586_A21_DEF_ANUITY_AMT': Decimal('21.00'), 'I586_P25_PO_5YEAR_AMT': Decimal('25.00'), 'I581_X': '', 'I585_A10_DISABLED_AMT': Decimal('10.00'), 'EMPLOYEE_ID': '00001962', 'I580_GRAT_EXMPT_AMT': Decimal('12.00'), 'I585_A24_SRV_ABROAD_AMT': Decimal('24.00'), 'I580_SURCHRG_AMT': Decimal('8.00'), 'I584_PAYROLL_LIMIT_AMT': Decimal('0.00'), 'I586_P20_DEF_ANTY_SP_AMT': Decimal('20.00'), 'I586_A5_EQUITY_AMT': Decimal('5.00'), 'I582_MDA_EXMPT_AMT': Decimal('20000.00'), 'I582_SCEA_EXMPT_AMT': Decimal('3000.00'),'I585_P7_HLT_CHK_PARNT_AMT': Decimal('7.00'), 'I585_A23_FORN_SOURC_AMT': Decimal('23.00'), 'I586_A18_NHB_AMT': Decimal('18.00'), 'I585_P17_PMCM_RELIEF_AMT': Decimal('17.00'), 'I584_DIVIDENDS_AMT': Decimal('0.00'), 'I584_ACC_MONTH_SRT': '00', 'I582_MDA_X': 'X', 'I584_HOME_SELF_X': '', 'I585_A27_PENSN_CENGOV_AMT': Decimal('27.00'), 'I586_A1_LIFE_INSU_AMT': Decimal('1.00'), 'I585_P14_MED_TRMT_VSCTZ_AMT': Decimal('14.00'), 'I580_LTA_EXMPT_X': '1', 'I582_X': '', 'I586_P18_NHB_AMT': Decimal('18.00'), 'I582_LTA_JOURNY_START_DATE': datetime.date(2017, 2, 15), 'I586_A9_POST_OFFICE_AMT': Decimal('9.00'), 'I586_A12_PF_AMT': Decimal('12.00'), 'I585_A30_HOMELOAN_INTR_AMT': Decimal('30.00'), 'I586_A27_SSS_AMT': Decimal('27.00'), 'I580_X': '', 'I584_TDS_OTHERS_AMT': Decimal('60.00'), 'I586_A13_HOUSE_LOAN_AMT': Decimal('13.00'), 'I582_LTA_TRAVEL_MODE': 'WATER', 'I585_P18_NAT_CHIL_AMT': Decimal('18.00'), 'I586_X': '', 'I585_P6_HLT_CHK_SELF_AMT': Decimal('6.00'), 'I585_P26_SELF_SDISABLE_AMT': Decimal('26.00'), 'I585_A14_MED_TRMT_VSCTZ_AMT': Decimal('14.00'), 'I585_P16_CRY_AMT': Decimal('16.00'), 'I584_X': '', 'I582_LTA_END_LOC': 'LANKA', 'I584_HOME_PART_RNT_X': 'X', 'I584_LOAN_SEQ_NUM': '', 'I586_A10_NSC_AMT': Decimal('10.00'), 'I580_VRS_EXMPT_AMT': Decimal('13.00'), 'I586_P22_TUTE_FEE_1_AMT': Decimal('22.00'), 'I580_SALARY_AMT': Decimal('1.00'), 'I584_STATUS': '', 'I586_P5_EQUITY_AMT': Decimal('5.00'), 'I584_LOAN_TYPE': '', 'I586_P17_PENSION_AMT': Decimal('17.00'), 'I580_NUM_LTA_EXMPT_AMT': '01', 'I585_A18_NAT_CHIL_AMT': Decimal('18.00'), 'I580_LEAVE_ENCASH_EXMPT_AMT': Decimal('11.00'), 'I580_EXMPT_AMT': Decimal('4.00'), 'I586_A26_SCSS_AMT': Decimal('26.00'), 'I586_A2_SUPER_FUND_AMT': Decimal('2.00'), 'I585_A29_RG_ESS_AMT': Decimal('29.00'), 'I586_ACTUAL_X': '', 'OBJPS': '', 'I586_P13_HOUSE_LOAN_AMT': Decimal('13.00'), 'I585_A16_CRY_AMT': Decimal('16.00'), 'I586_A7_PPF_AMT': Decimal('7.00'), 'I585_A4_INS_P_NSCTZ_AMT': Decimal('4.00'), 'I585_A15_INT_HIGH_EDU_AMT': Decimal('15.00'), 'I584_S24_REPAIR_AMT': Decimal('30.00'), 'I584_OTH_SOURCES_AMT': Decimal('0.00'), 'I585_A7_HLT_CHK_PARNT_AMT': Decimal('7.00'), 'I585_P22_RENT_PAID_AMT': Decimal('22.00'), 'I585_P13_MED_TRMT_SCTZ_AMT': Decimal('13.00'), 'I582_LTA_X': 'X', 'I580_VALUE_PREREQ_AMT': Decimal('2.00'), 'I585_P4_INS_P_NSCTZ_AMT': Decimal('4.00'), 'I584_S24_LIMIT_AMT': '', 'I586_A8_RPF_AMT': Decimal('8.00'), 'I586_P16_MF_AMT': Decimal('16.00'), 'I585_A28_INTR_SAV_ACC_AMT': Decimal('28.00'), 'I584_S24_INTREST_AMT': Decimal('40.00'), 'I586_P15_ANNUITY_LIC_AMT': Decimal('15.00'), 'I586_P2_SUPER_FUND_AMT': Decimal('2.00'), 'I586_A16_MF_AMT': Decimal('16.00'), 'I585_P29_RG_ESS_AMT': Decimal('29.00'), 'I586_P24_TERM_DEPOSIT_AMT': Decimal('24.00'), 'I586_A17_PENSION_AMT': Decimal('17.00'), 'I582_SCEA_X': 'X', 'I585_A17_PMCM_RELIEF_AMT': Decimal('17.00'), 'I586_A15_ANNUITY_LIC_AMT': Decimal('15.00'), 'I586_P8_RPF_AMT': Decimal('8.00'), 'I586_P7_PPF_AMT': Decimal('7.00'), 'I585_P11_SDISABLED_AMT': Decimal('11.00'), 'I585_P8_EXP_VSN_SELF_AMT': Decimal('8.00'), 'I586_P14_NSC_VIII_AMT': Decimal('14.00'), 'I582_LTA_TKT_NUMBERS': '2323,2324', 'I586_P11_ULIP_LIC_AMT': Decimal('11.00'), 'I585_ACTUAL_X': '', 'I586_P6_INFRA_MF_AMT': Decimal('6.00'), 'I586_P12_PF_AMT': Decimal('12.00'), 'I586_PROVIS_X': '', 'I580_STATUS': '', 'I584_SUBTYPE': '0001', 'I580_PROFIT_AMT': Decimal('3.00'), 'I586_A25_PO_5YEAR_AMT': Decimal('25.00'), 'I585_P30_HOMELOAN_INTR_AMT': Decimal('30.00'), 'I585_P20_CLN_GANGA_AMT': Decimal('20.00'), 'I585_A21_DRUG_ABUS_AMT': Decimal('21.00'), 'I586_P4_ULIP_AMT': Decimal('4.00'), 'I586_A3_NSS_AMT': Decimal('3.00'), 'I585_P25_SELF_DISABLE_AMT': Decimal('25.00'), 'I580_EDU_CESS_AMT': Decimal('9.00'), 'I584_FINAL_LET_AMT': Decimal('20.00'), 'I586_P26_SCSS_AMT': Decimal('26.00'), 'I586_P23_TUTE_FEE_2_AMT': Decimal('23.00'), 'I584_INTERESTS_AMT': Decimal('0.00'), 'I585_P24_SRV_ABROAD_AMT': Decimal('24.00'), 'I582_LTA_OBJPS': '01', 'I584_LT_CAP_GAIN_S_AMT': Decimal('0.00'), 'I586_A22_TUTE_FEE_1_AMT': Decimal('22.00'), 'I582_LTA_TRAVEL_CLASS': 'A', 'SUBTY': '', 'I585_P28_INTR_SAV_ACC_AMT': Decimal('28.00'), 'I585_P10_DISABLED_AMT': Decimal('10.00'), 'I586_P10_NSC_AMT': Decimal('10.00'), 'I580_PROV_FUND_AMT': Decimal('6.00'), 'I586_A19_OTH_SCHEME_AMT': Decimal('19.00'), 'I585_P1_PENSIONS_AMT': Decimal('1.00'), 'I585_A11_SDISABLED_AMT': Decimal('11.00'), 'I585_P21_DRUG_ABUS_AMT': Decimal('21.00'), 'I582_MDA_START_DATE': datetime.date(2017, 2, 6), 'I585_P12_MED_TRMT_AMT': Decimal('12.00'), 'I582_SUBTYPE': 'LTA', 'I585_A6_HLT_CHK_SELF_AMT': Decimal('6.00'), 'I586_P21_DEF_ANUITY_AMT': Decimal('21.00'), 'I585_A26_SELF_SDISABLE_AMT': Decimal('26.00'), 'I586_A14_NSC_VIII_AMT': Decimal('14.00'), 'I584_S24_OTHERS_AMT': Decimal('50.00'), 'I585_A20_CLN_GANGA_AMT': Decimal('20.00'), 'I586_A11_ULIP_LIC_AMT': Decimal('11.00'), 'I585_P23_FORN_SOURC_AMT': Decimal('23.00'), 'I582_SCEA_START_DATE': datetime.date(2017, 2, 1), 'I586_A6_INFRA_MF_AMT': Decimal('6.00'), 'I582_LTA_JOURNY_END_DATE': datetime.date(2017, 2, 19), 'I584_ACC_MONTH_END': '00', 'I585_A9_EXP_VSN_PARNT_AMT': Decimal('9.00'), 'I584_LT_CAP_GAIN_N_AMT': Decimal('0.00'), 'I586_P1_LIFE_INSU_AMT': Decimal('1.00'), 'I585_P9_EXP_VSN_PARNT_AMT': Decimal('9.00'), 'I582_LTA_STRT_LOC': 'HYD', 'I586_A4_ULIP_AMT': Decimal('4.00'), 'I585_STATUS': '', 'I585_A1_PENSIONS_AMT': Decimal('1.00'), 'I585_A13_MED_TRMT_SCTZ_AMT': Decimal('13.00'), 'I586_P9_POST_OFFICE_AMT': Decimal('9.00'), 'I584_HOME_FUL_RNT_X': '', 'I582_LTA_EXMPT_AMT': Decimal('20000.00'), 'I585_P5_INS_P_SCTZ_AMT': Decimal('5.00'), 'I585_P19_SWAS_BHT_KSH_AMT': Decimal('19.00'), 'I585_P2_INS_N_SCTZ_AMT': Decimal('2.00'), 'I585_P15_INT_HIGH_EDU_AMT': Decimal('15.00'), 'I586_P19_OTH_SCHEME_AMT': Decimal('19.00')}
#     # testdata={'I585_A22_RENT_PAID_AMT': '22.00', 'I580_INCOM_TAX_AMT': '7.00', 'I586_P3_NSS_AMT': '3.00', 'I585_A5_INS_P_SCTZ_AMT': '5.00', 'I586_STATUS': '', 'I585_A2_INS_N_SCTZ_AMT': '2.00', 'I585_A3_INS_SCTZ_AMT': '3.00', 'I585_A8_EXP_VSN_SELF_AMT': '8.00', 'I586_P27_SSS_AMT': '27.00', 'I585_A12_MED_TRMT_AMT': '12.00', 'I585_A19_SWAS_BHT_KSH_AMT': '19.00', 'I586_A20_DEF_ANTY_SP_AMT': '20.00', 'I584_PRFT_BUS_PROF_AMT': '0.00', 'I581_STATUS': '', 'I580_MED_EXMPT_AMT': '10.00', 'I582_STATUS': '', 'I586_A24_TERM_DEPOSIT_AMT': '24.00', 'I584_ST_CAP_GAIN_AMT': '0.00', 'I582_LTA_SELF_TRAVLD': 'Y', 'I585_X': '', 'I580_PROF_TAX_AMT': '5.00', 'I585_A25_SELF_DISABLE_AMT': '25.00', 'I585_P27_PENSN_CENGOV_AMT': '27.00', 'I582_LTA_START_DATE': datetime.date(2017, 3, 20), 'I586_A23_TUTE_FEE_2_AMT': '23.00', 'I585_PROVIS_X': '', 'I585_P3_INS_SCTZ_AMT': '3.00', 'I586_A21_DEF_ANUITY_AMT': '21.00', 'I586_P25_PO_5YEAR_AMT': '25.00', 'I581_X': '', 'I585_A10_DISABLED_AMT': '10.00', 'EMPLOYEE_ID': '00001962', 'I580_GRAT_EXMPT_AMT': '12.00', 'I585_A24_SRV_ABROAD_AMT': '24.00', 'I580_SURCHRG_AMT': '8.00', 'I584_PAYROLL_LIMIT_AMT': '0.00', 'I586_P20_DEF_ANTY_SP_AMT': '20.00', 'I586_A5_EQUITY_AMT': '5.00', 'I582_MDA_EXMPT_AMT': '20000.00', 'I582_SCEA_EXMPT_AMT': '3000.00','I585_P7_HLT_CHK_PARNT_AMT': '7.00', 'I585_A23_FORN_SOURC_AMT': '23.00', 'I586_A18_NHB_AMT': '18.00', 'I585_P17_PMCM_RELIEF_AMT': '17.00', 'I584_DIVIDENDS_AMT': '0.00', 'I584_ACC_MONTH_SRT': '00', 'I582_MDA_X': 'X', 'I584_HOME_SELF_X': '', 'I585_A27_PENSN_CENGOV_AMT': '27.00', 'I586_A1_LIFE_INSU_AMT': '1.00', 'I585_P14_MED_TRMT_VSCTZ_AMT': '14.00', 'I580_LTA_EXMPT_X': '1', 'I582_X': '', 'I586_P18_NHB_AMT': '18.00', 'I582_LTA_JOURNY_START_DATE': datetime.date(2017, 2, 15), 'I586_A9_POST_OFFICE_AMT': '9.00', 'I586_A12_PF_AMT': '12.00', 'I585_A30_HOMELOAN_INTR_AMT': '30.00', 'I586_A27_SSS_AMT': '27.00', 'I580_X': '', 'I584_TDS_OTHERS_AMT': '60.00', 'I586_A13_HOUSE_LOAN_AMT': '13.00', 'I582_LTA_TRAVEL_MODE': 'WATER', 'I585_P18_NAT_CHIL_AMT': '18.00', 'I586_X': '', 'I585_P6_HLT_CHK_SELF_AMT': '6.00', 'I585_P26_SELF_SDISABLE_AMT': '26.00', 'I585_A14_MED_TRMT_VSCTZ_AMT': '14.00', 'I585_P16_CRY_AMT': '16.00', 'I584_X': '', 'I582_LTA_END_LOC': 'LANKA', 'I584_HOME_PART_RNT_X': 'X', 'I584_LOAN_SEQ_NUM': '', 'I586_A10_NSC_AMT': '10.00', 'I580_VRS_EXMPT_AMT': '13.00', 'I586_P22_TUTE_FEE_1_AMT': '22.00', 'I580_SALARY_AMT': '1.00', 'I584_STATUS': '', 'I586_P5_EQUITY_AMT': '5.00', 'I584_LOAN_TYPE': '', 'I586_P17_PENSION_AMT': '17.00', 'I580_NUM_LTA_EXMPT_AMT': '01', 'I585_A18_NAT_CHIL_AMT': '18.00', 'I580_LEAVE_ENCASH_EXMPT_AMT': '11.00', 'I580_EXMPT_AMT': '4.00', 'I586_A26_SCSS_AMT': '26.00', 'I586_A2_SUPER_FUND_AMT': '2.00', 'I585_A29_RG_ESS_AMT': '29.00', 'I586_ACTUAL_X': '', 'OBJPS': '', 'I586_P13_HOUSE_LOAN_AMT': '13.00', 'I585_A16_CRY_AMT': '16.00', 'I586_A7_PPF_AMT': '7.00', 'I585_A4_INS_P_NSCTZ_AMT': '4.00', 'I585_A15_INT_HIGH_EDU_AMT': '15.00', 'I584_S24_REPAIR_AMT': '30.00', 'I584_OTH_SOURCES_AMT': '0.00', 'I585_A7_HLT_CHK_PARNT_AMT': '7.00', 'I585_P22_RENT_PAID_AMT': '22.00', 'I585_P13_MED_TRMT_SCTZ_AMT': '13.00', 'I582_LTA_X': 'X', 'I580_VALUE_PREREQ_AMT': '2.00', 'I585_P4_INS_P_NSCTZ_AMT': '4.00', 'I584_S24_LIMIT_AMT': '', 'I586_A8_RPF_AMT': '8.00', 'I586_P16_MF_AMT': '16.00', 'I585_A28_INTR_SAV_ACC_AMT': '28.00', 'I584_S24_INTREST_AMT': '40.00', 'I586_P15_ANNUITY_LIC_AMT': '15.00', 'I586_P2_SUPER_FUND_AMT': '2.00', 'I586_A16_MF_AMT': '16.00', 'I585_P29_RG_ESS_AMT': '29.00', 'I586_P24_TERM_DEPOSIT_AMT': '24.00', 'I586_A17_PENSION_AMT': '17.00', 'I582_SCEA_X': 'X', 'I585_A17_PMCM_RELIEF_AMT': '17.00', 'I586_A15_ANNUITY_LIC_AMT': '15.00', 'I586_P8_RPF_AMT': '8.00', 'I586_P7_PPF_AMT': '7.00', 'I585_P11_SDISABLED_AMT': '11.00', 'I585_P8_EXP_VSN_SELF_AMT': '8.00', 'I586_P14_NSC_VIII_AMT': '14.00', 'I582_LTA_TKT_NUMBERS': '2323,2324', 'I586_P11_ULIP_LIC_AMT': '11.00', 'I585_ACTUAL_X': '', 'I586_P6_INFRA_MF_AMT': '6.00', 'I586_P12_PF_AMT': '12.00', 'I586_PROVIS_X': '', 'I580_STATUS': '', 'I584_SUBTYPE': '0001', 'I580_PROFIT_AMT': '3.00', 'I586_A25_PO_5YEAR_AMT': '25.00', 'I585_P30_HOMELOAN_INTR_AMT': '30.00', 'I585_P20_CLN_GANGA_AMT': '20.00', 'I585_A21_DRUG_ABUS_AMT': '21.00', 'I586_P4_ULIP_AMT': '4.00', 'I586_A3_NSS_AMT': '3.00', 'I585_P25_SELF_DISABLE_AMT': '25.00', 'I580_EDU_CESS_AMT': '9.00', 'I584_FINAL_LET_AMT': '20.00', 'I586_P26_SCSS_AMT': '26.00', 'I586_P23_TUTE_FEE_2_AMT': '23.00', 'I584_INTERESTS_AMT': '0.00', 'I585_P24_SRV_ABROAD_AMT': '24.00', 'I582_LTA_OBJPS': '01', 'I584_LT_CAP_GAIN_S_AMT': '0.00', 'I586_A22_TUTE_FEE_1_AMT': '22.00', 'I582_LTA_TRAVEL_CLASS': 'A', 'SUBTY': '', 'I585_P28_INTR_SAV_ACC_AMT': '28.00', 'I585_P10_DISABLED_AMT': '10.00', 'I586_P10_NSC_AMT': '10.00', 'I580_PROV_FUND_AMT': '6.00', 'I586_A19_OTH_SCHEME_AMT': '19.00', 'I585_P1_PENSIONS_AMT': '1.00', 'I585_A11_SDISABLED_AMT': '11.00', 'I585_P21_DRUG_ABUS_AMT': '21.00', 'I582_MDA_START_DATE': datetime.date(2017, 2, 6), 'I585_P12_MED_TRMT_AMT': '12.00', 'I582_SUBTYPE': 'LTA', 'I585_A6_HLT_CHK_SELF_AMT': '6.00', 'I586_P21_DEF_ANUITY_AMT': '21.00', 'I585_A26_SELF_SDISABLE_AMT': '26.00', 'I586_A14_NSC_VIII_AMT': '14.00', 'I584_S24_OTHERS_AMT': '50.00', 'I585_A20_CLN_GANGA_AMT': '20.00', 'I586_A11_ULIP_LIC_AMT': '11.00', 'I585_P23_FORN_SOURC_AMT': '23.00', 'I582_SCEA_START_DATE': datetime.date(2017, 2, 1), 'I586_A6_INFRA_MF_AMT': '6.00', 'I582_LTA_JOURNY_END_DATE': datetime.date(2017, 2, 19), 'I584_ACC_MONTH_END': '00', 'I585_A9_EXP_VSN_PARNT_AMT': '9.00', 'I584_LT_CAP_GAIN_N_AMT': '0.00', 'I586_P1_LIFE_INSU_AMT': '1.00', 'I585_P9_EXP_VSN_PARNT_AMT': '9.00', 'I582_LTA_STRT_LOC': 'HYD', 'I586_A4_ULIP_AMT': '4.00', 'I585_STATUS': '', 'I585_A1_PENSIONS_AMT': '1.00', 'I585_A13_MED_TRMT_SCTZ_AMT': '13.00', 'I586_P9_POST_OFFICE_AMT': '9.00', 'I584_HOME_FUL_RNT_X': '', 'I582_LTA_EXMPT_AMT': '20000.00', 'I585_P5_INS_P_SCTZ_AMT': '5.00', 'I585_P19_SWAS_BHT_KSH_AMT': '19.00', 'I585_P2_INS_N_SCTZ_AMT': '2.00', 'I585_P15_INT_HIGH_EDU_AMT': '15.00', 'I586_P19_OTH_SCHEME_AMT': '19.00'}
#     # conn = Connection(user='veerahcm', passwd='Veera@143', ashost='192.168.18.16',sysnr='D01', gwserv='3301', client='150')
#     if len(verified_TD_Details)!=0:
#         sap_login=get_sap_user_credentials()
#         conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])

#         result = conn.call('Z_HR_UPDATE_TAX_DECLARATION',CH_TAX_DATA_TABLE=CH_TAX_DATA_TABLE,CH_HRA_RENTS_TABLE=CH_HRA_RENTS_TABLE)
#         conn.close()
#         close_sap_user_credentials(sap_login['_id'])
#         sent_emp_data=[]
#         for jjj in result['CH_TAX_DATA_TABLE']:
#             if jjj['I580_STATUS'] =="" and jjj['I581_STATUS']=="" and jjj['I582_STATUS'] =="" and jjj['I584_STATUS']=="" and jjj['I585_STATUS'] =="" and jjj['I586_STATUS']=="":
#                 update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS']}})
#             else:
#                 update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS']}})
#             sent_emp_data.append(jjj['EMPLOYEE_ID'])
#         logging.info('Details Sent TO Sap - '+str(sent_emp_data))
#         return 'success'
#     else:
#         return 'No Data To Send To SAP'

################ sending data one by one ###################
@zen_proposal.route('/UPDATE_TO_SAP_BATCH_PROPOSALS/', methods = ['GET', 'POST'])
def UPDATE_TO_SAP_BATCH_PROPOSALS():
    debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
    if debug_flag and debug_flag['debug']=='on':
        debug_flag=debug_flag['debug_status']['ip_approve']
    else:
        debug_flag="off"
    if  debug_flag=='on':
        # verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2017-2018','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED','debug':'1','sap_update':{'$exists':0}})
        verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2017-2018','debug':'1','sap_update':{'$exists':0}})
    else:
        verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2017-2018','sap_update':{'$exists':0}})
        # verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2017-2018','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED','sap_update':{'$exists':0}})
    final_result_to_sap=[]
    for user in  verified_TD_Details:
        print user['EMPLOYEE_ID']
        logging.info(user['EMPLOYEE_ID'])
        CH_TAX_DATA_TABLE=[]
        CH_HRA_RENTS_TABLE=[]
        if debug_flag=='on':
            logging.info(user)
            # print 'debug on'
        # print user['EMPLOYEE_ID']
        dates_range_year={'APR':{'st_dt':datetime.date(2018,4,1),'end_dt':datetime.date(2018,4,30)},
        'MAY':{'st_dt':datetime.date(2018,5,1),'end_dt':datetime.date(2018,5,31)},
        'JUN':{'st_dt':datetime.date(2018,6,1),'end_dt':datetime.date(2018,6,30)},
        'JUL':{'st_dt':datetime.date(2018,7,1),'end_dt':datetime.date(2018,7,31)},
        'AUG':{'st_dt':datetime.date(2018,8,1),'end_dt':datetime.date(2018,8,31)},
        'SEP':{'st_dt':datetime.date(2018,9,1),'end_dt':datetime.date(2018,9,30)},
        'OCT':{'st_dt':datetime.date(2018,10,1),'end_dt':datetime.date(2018,10,31)},
        'NOV':{'st_dt':datetime.date(2018,11,1),'end_dt':datetime.date(2018,11,30)},
        'DEC':{'st_dt':datetime.date(2018,12,1),'end_dt':datetime.date(2018,12,31)},
        'JAN':{'st_dt':datetime.date(2018,1,1),'end_dt':datetime.date(2018,1,31)},
        'FEB':{'st_dt':datetime.date(2018,2,1),'end_dt':datetime.date(2018,2,28)},
        'MAR':{'st_dt':datetime.date(2018,3,1),'end_dt':datetime.date(2018,3,31)}}
        RESULT=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':user['EMPLOYEE_ID'],'FY_YEAR':'2017-2018'})
        list_of_fields_I580=['I580_SALARY_AMT','I580_VALUE_PREREQ_AMT','I580_PROFIT_AMT','I580_EXMPT_AMT','I580_PROF_TAX_AMT','I580_PROV_FUND_AMT','I580_INCOM_TAX_AMT','I580_SURCHRG_AMT','I580_EDU_CESS_AMT','I580_MED_EXMPT_AMT','I580_LEAVE_ENCASH_EXMPT_AMT','I580_GRAT_EXMPT_AMT','I580_VRS_EXMPT_AMT','I580_NUM_LTA_EXMPT_AMT','I580_LTA_EXMPT_X']
        list_of_fields_I582=['I582_SCEA_X','I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT','I582_MDA_X','I582_MDA_START_DATE','I582_MDA_EXMPT_AMT','I582_LTA_X','I582_LTA_START_DATE','I582_LTA_EXMPT_AMT','I582_LTA_OBJPS','I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE','I582_LTA_STRT_LOC','I582_LTA_END_LOC','I582_LTA_TRAVEL_MODE','I582_LTA_TRAVEL_CLASS','I582_LTA_TKT_NUMBERS','I582_LTA_SELF_TRAVLD']
        list_of_fields_I584=['I584_LOAN_TYPE','I584_LOAN_SEQ_NUM','I584_ACC_MONTH_SRT','I584_ACC_MONTH_END','I584_S24_LIMIT_AMT','I584_PAYROLL_LIMIT_AMT','I584_FINAL_LET_AMT','I584_S24_REPAIR_AMT','I584_S24_INTREST_AMT','I584_S24_OTHERS_AMT','I584_TDS_OTHERS_AMT','I584_PRFT_BUS_PROF_AMT','I584_LT_CAP_GAIN_N_AMT','I584_LT_CAP_GAIN_S_AMT','I584_ST_CAP_GAIN_AMT','I584_DIVIDENDS_AMT','I584_INTERESTS_AMT','I584_OTH_SOURCES_AMT']
        list_of_fields_I585=['I585_A1_PENSIONS_AMT','I585_A2_INS_N_SCTZ_AMT','I585_A3_INS_SCTZ_AMT','I585_A4_INS_P_NSCTZ_AMT','I585_A5_INS_P_SCTZ_AMT','I585_A6_HLT_CHK_SELF_AMT','I585_A7_HLT_CHK_PARNT_AMT','I585_A8_EXP_VSN_SELF_AMT','I585_A9_EXP_VSN_PARNT_AMT','I585_A10_DISABLED_AMT','I585_A11_SDISABLED_AMT','I585_A12_MED_TRMT_AMT','I585_A13_MED_TRMT_SCTZ_AMT','I585_A14_MED_TRMT_VSCTZ_AMT','I585_A15_INT_HIGH_EDU_AMT','I585_A16_CRY_AMT','I585_A17_PMCM_RELIEF_AMT','I585_A18_NAT_CHIL_AMT','I585_A19_SWAS_BHT_KSH_AMT','I585_A20_CLN_GANGA_AMT','I585_A21_DRUG_ABUS_AMT','I585_A22_RENT_PAID_AMT','I585_A23_FORN_SOURC_AMT','I585_A24_SRV_ABROAD_AMT','I585_A25_SELF_DISABLE_AMT','I585_A27_PENSN_CENGOV_AMT','I585_A28_INTR_SAV_ACC_AMT','I585_A29_RG_ESS_AMT','I585_A30_HOMELOAN_INTR_AMT']
        list_of_fields_I586=['I586_A1_LIFE_INSU_AMT','I586_A2_SUPER_FUND_AMT','I586_A3_NSS_AMT','I586_A4_ULIP_AMT','I586_A5_EQUITY_AMT','I586_A6_INFRA_MF_AMT','I586_A7_PPF_AMT','I586_A8_RPF_AMT','I586_A9_POST_OFFICE_AMT','I586_A10_NSC_AMT','I586_A11_ULIP_LIC_AMT','I586_A12_PF_AMT','I586_A13_HOUSE_LOAN_AMT','I586_A14_NSC_VIII_AMT','I586_A15_ANNUITY_LIC_AMT','I586_A16_MF_AMT','I586_A17_PENSION_AMT','I586_A18_NHB_AMT','I586_A19_OTH_SCHEME_AMT','I586_A20_DEF_ANTY_SP_AMT','I586_A21_DEF_ANUITY_AMT','I586_A22_TUTE_FEE_1_AMT','I586_A23_TUTE_FEE_2_AMT','I586_A24_TERM_DEPOSIT_AMT','I586_A25_PO_5YEAR_AMT','I586_A26_SCSS_AMT','I586_A27_SSS_AMT']
        list_of_fields_I585_P=['I585_P1_PENSIONS_AMT','I585_P2_INS_N_SCTZ_AMT','I585_P3_INS_SCTZ_AMT','I585_P4_INS_P_NSCTZ_AMT','I585_P5_INS_P_SCTZ_AMT','I585_P6_HLT_CHK_SELF_AMT','I585_P7_HLT_CHK_PARNT_AMT','I585_P8_EXP_VSN_SELF_AMT','I585_P9_EXP_VSN_PARNT_AMT','I585_P10_DISABLED_AMT','I585_P11_SDISABLED_AMT','I585_P12_MED_TRMT_AMT','I585_P13_MED_TRMT_SCTZ_AMT','I585_P14_MED_TRMT_VSCTZ_AMT','I585_P15_INT_HIGH_EDU_AMT','I585_P16_CRY_AMT','I585_P17_PMCM_RELIEF_AMT','I585_P18_NAT_CHIL_AMT','I585_P19_SWAS_BHT_KSH_AMT','I585_P20_CLN_GANGA_AMT','I585_P21_DRUG_ABUS_AMT','I585_P22_RENT_PAID_AMT','I585_P23_FORN_SOURC_AMT','I585_P24_SRV_ABROAD_AMT','I585_P25_SELF_DISABLE_AMT','I585_P27_PENSN_CENGOV_AMT','I585_P28_INTR_SAV_ACC_AMT','I585_P29_RG_ESS_AMT','I585_P30_HOMELOAN_INTR_AMT']
        list_of_fields_I586_P=['I586_P1_LIFE_INSU_AMT','I586_P2_SUPER_FUND_AMT','I586_P3_NSS_AMT','I586_P4_ULIP_AMT','I586_P5_EQUITY_AMT','I586_P6_INFRA_MF_AMT','I586_P7_PPF_AMT','I586_P8_RPF_AMT','I586_P9_POST_OFFICE_AMT','I586_P10_NSC_AMT','I586_P11_ULIP_LIC_AMT','I586_P12_PF_AMT','I586_P13_HOUSE_LOAN_AMT','I586_P14_NSC_VIII_AMT','I586_P15_ANNUITY_LIC_AMT','I586_P16_MF_AMT','I586_P17_PENSION_AMT','I586_P18_NHB_AMT','I586_P19_OTH_SCHEME_AMT','I586_P20_DEF_ANTY_SP_AMT','I586_P21_DEF_ANUITY_AMT','I586_P22_TUTE_FEE_1_AMT','I586_P23_TUTE_FEE_2_AMT','I586_P24_TERM_DEPOSIT_AMT','I586_P25_PO_5YEAR_AMT','I586_P26_SCSS_AMT','I586_P27_SSS_AMT']

        updt_arr={'EMPLOYEE_ID':user['EMPLOYEE_ID']}
        test_updt_arry={}

        ## not sending accom text in 581 infotype
        for month in ['APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC','JAN','FEB','MAR']:
            if 'I581_HRA_EXMPT_X' in RESULT:
                I581_HRA_EXMPT_X=RESULT['I581_HRA_EXMPT_X']
            else:
                I581_HRA_EXMPT_X=''
            if RESULT['I581_ACCOM_TYPE']=="1":
                updt_data_hra={'EMPLOYEE_ID':user['EMPLOYEE_ID'],'I581_RENT_MONTH_NAME': month,'I581_HRA_EXMPT_X':I581_HRA_EXMPT_X,'I581_ACCOM_TYPE': RESULT['I581_ACCOM_TYPE'], 'I581_RENT': RESULT['I581_'+month+'_RENT'],'I581_OWNER_ADDRESS2':'','I581_OWNER_ADDRESS3': '', 'I581_TOT_FURN_VAL':'0', 'I581_HELPRS_AMT': '0',  'I581_RENT_START_DATE': dates_range_year[month]['st_dt'], 'I581_OWNER_ADDRESS1': '',  'I581_METRO': '', 'I581_HELPERS': '00000',  'I581_RENT_END_DATE':dates_range_year[month]['end_dt'], 'I581_HIRE_CRG_FURN': '0'}
                updt_arr.update({'I581_X':'X'})
                CH_HRA_RENTS_TABLE.append(dict(updt_data_hra))
            else:
                logging.info('accomodation type selected as - '+RESULT['I581_ACCOM_TYPE'])

        for field in list_of_fields_I580:            
            if field in RESULT:
                updt_arr.update({field:RESULT[field],'I580_X':'X'})
            else:
                updt_arr.update({field:'0'})

        #### check the field LTA start date what it hasto be filled and sent to SAP
        # if RESULT['I582_SCEA_EXMPT_AMT'] !='0.00':
        #     if RESULT['I582_SCEA_EXMPT_AMT'] =='':
        #         pass
        #     else:
        #         print RESULT['I582_SCEA_EXMPT_AMT']           
        #         updt_arr.update({'I582_X':'X','I582_SCEA_X':'X'})
        #         # update_collection('Investment_Details',{'EMPLOYEE_ID':user['EMPLOYEE_ID'],'FY_YEAR':'2016-2017','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED'},{'$set':{'I582_X':'X'}})
        #         # I582_SCEA_X:[I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT]
        #         updt_arr.update({'I582_SCEA_START_DATE':datetime.date(2017,2,20),'I582_SCEA_EXMPT_AMT':RESULT['I582_SCEA_EXMPT_AMT']})

        if RESULT['I584_SUBTYPE']=='0001':
            print 'in subtype 0001'
            if RESULT['I584_HOME_FUL_RNT_X']=='X':
                if 'I584_S24_INTREST_AMT' in RESULT:
                    I584_S24_INTREST_AMT=RESULT['I584_S24_INTREST_AMT']
                else:
                    I584_S24_INTREST_AMT='0'
                if 'I584_FINAL_LET_AMT' in RESULT:
                    I584_FINAL_LET_AMT=RESULT['I584_FINAL_LET_AMT']
                else:
                    I584_FINAL_LET_AMT='0'
                if 'I584_S24_OTHERS_AMT' in RESULT:
                    I584_S24_OTHERS_AMT=RESULT['I584_S24_OTHERS_AMT']
                else:
                    I584_S24_OTHERS_AMT='0'
                if 'I584_S24_REPAIR_AMT' in RESULT:
                    I584_S24_REPAIR_AMT=RESULT['I584_S24_REPAIR_AMT']
                else:
                    I584_S24_REPAIR_AMT='0'
                updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT,'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})
            elif RESULT['I584_HOME_SELF_X']=='X':
                if 'I584_S24_INTREST_AMT' in RESULT:
                    I584_S24_INTREST_AMT=RESULT['I584_S24_INTREST_AMT']
                else:
                    I584_S24_INTREST_AMT='0'
                updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT,'I584_HOME_SELF_X':'X','I584_X':'X'})
            elif RESULT['I584_HOME_PART_RNT_X']=='X':
                if 'I584_S24_INTREST_AMT' in RESULT:
                    I584_S24_INTREST_AMT=RESULT['I584_S24_INTREST_AMT']
                else:
                    I584_S24_INTREST_AMT='0'
                if 'I584_FINAL_LET_AMT' in RESULT:
                    I584_FINAL_LET_AMT=RESULT['I584_FINAL_LET_AMT']
                else:
                    I584_FINAL_LET_AMT='0'
                if 'I584_S24_OTHERS_AMT' in RESULT:
                    I584_S24_OTHERS_AMT=RESULT['I584_S24_OTHERS_AMT']
                else:
                    I584_S24_OTHERS_AMT='0'
                if 'I584_S24_REPAIR_AMT' in RESULT:
                    I584_S24_REPAIR_AMT=RESULT['I584_S24_REPAIR_AMT']
                else:
                    I584_S24_REPAIR_AMT='0'
                updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT,'I584_HOME_PART_RNT_X':'X','I584_X':'X'})

        elif RESULT['I584_SUBTYPE']=='1':
            if 'I584_S24_INTREST_AMT' in RESULT:
                I584_S24_INTREST_AMT=RESULT['I584_S24_INTREST_AMT']
            else:
                I584_S24_INTREST_AMT='0'
            updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT,'I584_HOME_SELF_X':'X','I584_X':'X'})
        elif RESULT['I584_SUBTYPE']=='2':
            if 'I584_S24_INTREST_AMT' in RESULT:
                I584_S24_INTREST_AMT=RESULT['I584_S24_INTREST_AMT']
            else:
                I584_S24_INTREST_AMT='0'
            if 'I584_FINAL_LET_AMT' in RESULT:
                I584_FINAL_LET_AMT=RESULT['I584_FINAL_LET_AMT']
            else:
                I584_FINAL_LET_AMT='0'
            if 'I584_S24_OTHERS_AMT' in RESULT:
                I584_S24_OTHERS_AMT=RESULT['I584_S24_OTHERS_AMT']
            else:
                I584_S24_OTHERS_AMT='0'
            if 'I584_S24_REPAIR_AMT' in RESULT:
                I584_S24_REPAIR_AMT=RESULT['I584_S24_REPAIR_AMT']
            else:
                I584_S24_REPAIR_AMT='0'
            updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT,'I584_HOME_PART_RNT_X':'X','I584_X':'X'})
        elif RESULT['I584_SUBTYPE']=='3':
            if 'I584_S24_INTREST_AMT' in RESULT:
                I584_S24_INTREST_AMT=RESULT['I584_S24_INTREST_AMT']
            else:
                I584_S24_INTREST_AMT='0'
            if 'I584_FINAL_LET_AMT' in RESULT:
                I584_FINAL_LET_AMT=RESULT['I584_FINAL_LET_AMT']
            else:
                I584_FINAL_LET_AMT='0'
            if 'I584_S24_OTHERS_AMT' in RESULT:
                I584_S24_OTHERS_AMT=RESULT['I584_S24_OTHERS_AMT']
            else:
                I584_S24_OTHERS_AMT='0'
            if 'I584_S24_REPAIR_AMT' in RESULT:
                I584_S24_REPAIR_AMT=RESULT['I584_S24_REPAIR_AMT']
            else:
                I584_S24_REPAIR_AMT='0'
            updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT,'I584_HOME_FUL_RNT_X':'X','I584_X':'X'}) 
        #INCOME FROM OTHER THAN 80C
        for field in list_of_fields_I585_P:
            # print field+'_APPROVED'
            if field in RESULT:
                # print RESULT[field+'_APPROVED']
                updt_arr.update({field:RESULT[field],'I585_PROVIS_X':'X','I585_X':'X'})
            else:
                try:
                    if RESULT[field] !='0.00':
                        updt_arr.update({field:RESULT[field],'I585_PROVIS_X':'X','I585_X':'X'})
                    else:
                        updt_arr.update({field:'0'})
                except:
                    pass
        # print '------------------'
        #INCOME FROM OTHER SOURCES
        for field1 in list_of_fields_I586_P:
            if field1 in RESULT:
                # print RESULT[field1+'_APPROVED']
                updt_arr.update({field1:RESULT[field1],'I586_PROVIS_X':'X','I586_X':'X'})
            else:
                updt_arr.update({field1:'0'})
        CH_TAX_DATA_TABLE.append(dict(updt_arr))
        logging.info(CH_TAX_DATA_TABLE)
        # logging.info(CH_TAX_DATA_TABLE)
        if debug_flag=='on':
            logging.info(CH_TAX_DATA_TABLE)
        # print CH_TAX_DATA_TABLE,'whole data '
        # conn = Connection(user='veerahcm', passwd='Veera@143', ashost='192.168.18.16',sysnr='D01', gwserv='3301', client='150')
        sap_login=get_sap_user_credentials()
        conn = Connection(user='portal_batch1', passwd='Penna@123', ashost='192.168.18.52', gwserv='3301', sysnr='P01', client='900')
        # conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])

        result = conn.call('Z_HR_UPDATE_TAX_DECLARATION',CH_TAX_DATA_TABLE=CH_TAX_DATA_TABLE,CH_HRA_RENTS_TABLE=CH_HRA_RENTS_TABLE)
        conn.close()
        # close_sap_user_credentials(sap_login['_id'])
        # conn = Connection(user='veerahcm', passwd='Veera&143', ashost='192.168.18.15',sysnr='Q01', gwserv='3300', client='900')
        # result = conn.call('Z_HR_UPDATE_TAX_DECLARATION',CH_TAX_DATA_TABLE=CH_TAX_DATA_TABLE,CH_HRA_RENTS_TABLE=CH_HRA_RENTS_TABLE)
        sent_emp_data=[]
        # print result['CH_TAX_DATA_TABLE'],'table details'
        for jjj in result['CH_TAX_DATA_TABLE']:
            # if jjj['I580_STATUS'] =="" and jjj['I581_STATUS']=="" and jjj['I582_STATUS'] =="" and jjj['I584_STATUS']=="" and jjj['I585_STATUS'] =="" and jjj['I586_STATUS']=="":
            #     update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS'],'sap_update':'1'}})
            # else:
            #     update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS'],'sap_update':'2'}})
            update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2017-2018'},{'$set':{'sap_update':'1','sap_update_time':datetime.datetime.now()}})
            sent_emp_data.append(jjj['EMPLOYEE_ID'])
        logging.info('Details Sent TO Sap - '+str(sent_emp_data))
    return 'success'
#################### end of updating proposals ###############



################ sending data one by one ###################
@zen_proposal.route('/UPDATE_TO_SAP_BATCH/', methods = ['GET', 'POST'])
def UPDATE_TO_SAP_BATCH():
    debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
    if debug_flag and debug_flag['debug']=='on':
        debug_flag=debug_flag['debug_status']['ip_approve']
    else:
        debug_flag="off"
    if  debug_flag=='on':
        verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2016-2017','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED','debug':'1','sap_update':{'$exists':0}})
    else:
        verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2016-2017','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED','sap_update':{'$exists':0}})
    final_result_to_sap=[]
    for user in  verified_TD_Details:
        CH_TAX_DATA_TABLE=[]
        CH_HRA_RENTS_TABLE=[]
        if debug_flag=='on':
            logging.info(user)
            print 'debug on'
        print user['EMPLOYEE_ID']
        logging.info(user['EMPLOYEE_ID'])
        dates_range_year={'APR':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
        'MAY':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
        'JUN':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
        'JUL':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
        'AUG':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
        'SEP':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
        'OCT':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
        'NOV':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
        'DEC':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
        'JAN':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
        'FEB':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,28)},
        'MAR':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)}}
        RESULT=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':user['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'})
        list_of_fields_I580=['I580_SALARY_AMT','I580_VALUE_PREREQ_AMT','I580_PROFIT_AMT','I580_EXMPT_AMT','I580_PROF_TAX_AMT','I580_PROV_FUND_AMT','I580_INCOM_TAX_AMT','I580_SURCHRG_AMT','I580_EDU_CESS_AMT','I580_MED_EXMPT_AMT','I580_LEAVE_ENCASH_EXMPT_AMT','I580_GRAT_EXMPT_AMT','I580_VRS_EXMPT_AMT','I580_NUM_LTA_EXMPT_AMT','I580_LTA_EXMPT_X']
        list_of_fields_I582=['I582_SCEA_X','I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT','I582_MDA_X','I582_MDA_START_DATE','I582_MDA_EXMPT_AMT','I582_LTA_X','I582_LTA_START_DATE','I582_LTA_EXMPT_AMT','I582_LTA_OBJPS','I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE','I582_LTA_STRT_LOC','I582_LTA_END_LOC','I582_LTA_TRAVEL_MODE','I582_LTA_TRAVEL_CLASS','I582_LTA_TKT_NUMBERS','I582_LTA_SELF_TRAVLD']
        list_of_fields_I584=['I584_LOAN_TYPE','I584_LOAN_SEQ_NUM','I584_ACC_MONTH_SRT','I584_ACC_MONTH_END','I584_S24_LIMIT_AMT','I584_PAYROLL_LIMIT_AMT','I584_FINAL_LET_AMT','I584_S24_REPAIR_AMT','I584_S24_INTREST_AMT','I584_S24_OTHERS_AMT','I584_TDS_OTHERS_AMT','I584_PRFT_BUS_PROF_AMT','I584_LT_CAP_GAIN_N_AMT','I584_LT_CAP_GAIN_S_AMT','I584_ST_CAP_GAIN_AMT','I584_DIVIDENDS_AMT','I584_INTERESTS_AMT','I584_OTH_SOURCES_AMT']
        list_of_fields_I585=['I585_A1_PENSIONS_AMT','I585_A2_INS_N_SCTZ_AMT','I585_A3_INS_SCTZ_AMT','I585_A4_INS_P_NSCTZ_AMT','I585_A5_INS_P_SCTZ_AMT','I585_A6_HLT_CHK_SELF_AMT','I585_A7_HLT_CHK_PARNT_AMT','I585_A8_EXP_VSN_SELF_AMT','I585_A9_EXP_VSN_PARNT_AMT','I585_A10_DISABLED_AMT','I585_A11_SDISABLED_AMT','I585_A12_MED_TRMT_AMT','I585_A13_MED_TRMT_SCTZ_AMT','I585_A14_MED_TRMT_VSCTZ_AMT','I585_A15_INT_HIGH_EDU_AMT','I585_A16_CRY_AMT','I585_A17_PMCM_RELIEF_AMT','I585_A18_NAT_CHIL_AMT','I585_A19_SWAS_BHT_KSH_AMT','I585_A20_CLN_GANGA_AMT','I585_A21_DRUG_ABUS_AMT','I585_A22_RENT_PAID_AMT','I585_A23_FORN_SOURC_AMT','I585_A24_SRV_ABROAD_AMT','I585_A25_SELF_DISABLE_AMT','I585_A27_PENSN_CENGOV_AMT','I585_A28_INTR_SAV_ACC_AMT','I585_A29_RG_ESS_AMT','I585_A30_HOMELOAN_INTR_AMT']
        list_of_fields_I586=['I586_A1_LIFE_INSU_AMT','I586_A2_SUPER_FUND_AMT','I586_A3_NSS_AMT','I586_A4_ULIP_AMT','I586_A5_EQUITY_AMT','I586_A6_INFRA_MF_AMT','I586_A7_PPF_AMT','I586_A8_RPF_AMT','I586_A9_POST_OFFICE_AMT','I586_A10_NSC_AMT','I586_A11_ULIP_LIC_AMT','I586_A12_PF_AMT','I586_A13_HOUSE_LOAN_AMT','I586_A14_NSC_VIII_AMT','I586_A15_ANNUITY_LIC_AMT','I586_A16_MF_AMT','I586_A17_PENSION_AMT','I586_A18_NHB_AMT','I586_A19_OTH_SCHEME_AMT','I586_A20_DEF_ANTY_SP_AMT','I586_A21_DEF_ANUITY_AMT','I586_A22_TUTE_FEE_1_AMT','I586_A23_TUTE_FEE_2_AMT','I586_A24_TERM_DEPOSIT_AMT','I586_A25_PO_5YEAR_AMT','I586_A26_SCSS_AMT','I586_A27_SSS_AMT']
        updt_arr={'EMPLOYEE_ID':user['EMPLOYEE_ID']}
        test_updt_arry={}

        ## not sending accom text in 581 infotype
        for month in ['APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC','JAN','FEB','MAR']:
            if 'I581_HRA_EXMPT_X' in RESULT:
                I581_HRA_EXMPT_X=RESULT['I581_HRA_EXMPT_X']
            else:
                I581_HRA_EXMPT_X=''
            if RESULT['I581_ACCOM_TYPE']=="1":
                updt_data_hra={'EMPLOYEE_ID':user['EMPLOYEE_ID'],'I581_RENT_MONTH_NAME': month,'I581_HRA_EXMPT_X':I581_HRA_EXMPT_X,'I581_ACCOM_TYPE': RESULT['I581_ACCOM_TYPE'], 'I581_RENT': RESULT['I581_'+month+'_RENT'],'I581_OWNER_ADDRESS2':'','I581_OWNER_ADDRESS3': '', 'I581_TOT_FURN_VAL':'0', 'I581_HELPRS_AMT': '0',  'I581_RENT_START_DATE': dates_range_year[month]['st_dt'], 'I581_OWNER_ADDRESS1': '',  'I581_METRO': '', 'I581_HELPERS': '00000',  'I581_RENT_END_DATE':dates_range_year[month]['end_dt'], 'I581_HIRE_CRG_FURN': '0'}
                updt_arr.update({'I581_X':'X'})
                CH_HRA_RENTS_TABLE.append(dict(updt_data_hra))
            else:
                logging.info('accomodation type selected as - '+RESULT['I581_ACCOM_TYPE'])

        for field in list_of_fields_I580:            
            if field+'_APPROVED' in RESULT:
                updt_arr.update({field:RESULT[field+'_APPROVED'],'I580_X':'X'})
            else:
                updt_arr.update({field:'0'})

        #### check the field LTA start date what it hasto be filled and sent to SAP
        if RESULT['I582_SCEA_EXMPT_AMT'] !='0.00':
            if RESULT['I582_SCEA_EXMPT_AMT'] =='':
                pass
            else:
                print RESULT['I582_SCEA_EXMPT_AMT']           
                updt_arr.update({'I582_X':'X','I582_SCEA_X':'X'})
                # update_collection('Investment_Details',{'EMPLOYEE_ID':user['EMPLOYEE_ID'],'FY_YEAR':'2016-2017','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED'},{'$set':{'I582_X':'X'}})
                # I582_SCEA_X:[I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT]
                updt_arr.update({'I582_SCEA_START_DATE':datetime.date(2017,2,20),'I582_SCEA_EXMPT_AMT':RESULT['I582_SCEA_EXMPT_AMT']})

        if RESULT['I584_SUBTYPE']=='0001':
            if RESULT['I584_HOME_FUL_RNT_X']=='X':
                if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
                    I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
                else:
                    I584_S24_INTREST_AMT_APPROVED='0'
                if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
                    I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
                else:
                    I584_FINAL_LET_AMT_APPROVED='0'
                if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
                    I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
                else:
                    I584_S24_OTHERS_AMT_APPROVED='0'
                if 'I584_S24_REPAIR_AMT_APPROVED' in RESULT:
                    I584_S24_REPAIR_AMT_APPROVED=RESULT['I584_S24_REPAIR_AMT_APPROVED']
                else:
                    I584_S24_REPAIR_AMT_APPROVED='0'
                updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})
            elif RESULT['I584_HOME_SELF_X']=='X':
                if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
                    I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
                else:
                    I584_S24_INTREST_AMT_APPROVED='0'
                updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_HOME_SELF_X':'X','I584_X':'X'})
            elif RESULT['I584_HOME_PART_RNT_X']=='X':
                if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
                    I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
                else:
                    I584_S24_INTREST_AMT_APPROVED='0'
                if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
                    I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
                else:
                    I584_FINAL_LET_AMT_APPROVED='0'
                if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
                    I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
                else:
                    I584_S24_OTHERS_AMT_APPROVED='0'
                if 'I584_S24_REPAIR_AMT_APPROVED' in RESULT:
                    I584_S24_REPAIR_AMT_APPROVED=RESULT['I584_S24_REPAIR_AMT_APPROVED']
                else:
                    I584_S24_REPAIR_AMT_APPROVED='0'
                updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_PART_RNT_X':'X','I584_X':'X'})

        elif RESULT['I584_SUBTYPE']=='1':
            if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
                I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
            else:
                I584_S24_INTREST_AMT_APPROVED='0'
            updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_HOME_SELF_X':'X','I584_X':'X'})
        elif RESULT['I584_SUBTYPE']=='2':
            if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
                I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
            else:
                I584_S24_INTREST_AMT_APPROVED='0'
            if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
                I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
            else:
                I584_FINAL_LET_AMT_APPROVED='0'
            if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
                I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
            else:
                I584_S24_OTHERS_AMT_APPROVED='0'
            if 'I584_S24_REPAIR_AMT_APPROVED' in RESULT:
                I584_S24_REPAIR_AMT_APPROVED=RESULT['I584_S24_REPAIR_AMT_APPROVED']
            else:
                I584_S24_REPAIR_AMT_APPROVED='0'
            updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_PART_RNT_X':'X','I584_X':'X'})
        elif RESULT['I584_SUBTYPE']=='3':
            if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
                I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
            else:
                I584_S24_INTREST_AMT_APPROVED='0'
            if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
                I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
            else:
                I584_FINAL_LET_AMT_APPROVED='0'
            if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
                I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
            else:
                I584_S24_OTHERS_AMT_APPROVED='0'
            if 'I584_S24_REPAIR_AMT_APPROVED' in RESULT:
                I584_S24_REPAIR_AMT_APPROVED=RESULT['I584_S24_REPAIR_AMT_APPROVED']
            else:
                I584_S24_REPAIR_AMT_APPROVED='0'
            updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})
        #INCOME FROM OTHER THAN 80C
        for field in list_of_fields_I585:
            # print field+'_APPROVED'
            if field+'_APPROVED' in RESULT:
                # print RESULT[field+'_APPROVED']
                updt_arr.update({field:RESULT[field+'_APPROVED'],'I585_ACTUAL_X':'X','I585_X':'X'})
            else:
                if RESULT[field] !='0.00':
                    updt_arr.update({field:RESULT[field],'I585_ACTUAL_X':'X','I585_X':'X'})
                else:
                    updt_arr.update({field:'0'})
        # print '------------------'
        #INCOME FROM OTHER SOURCES
        for field1 in list_of_fields_I586:
            if field1+'_APPROVED' in RESULT:
                # print RESULT[field1+'_APPROVED']
                updt_arr.update({field1:RESULT[field1+'_APPROVED'],'I586_ACTUAL_X':'X','I586_X':'X'})
            else:
                updt_arr.update({field1:'0'})
        CH_TAX_DATA_TABLE.append(dict(updt_arr))
        # logging.info(CH_TAX_DATA_TABLE)
        if debug_flag=='on':
            logging.info(CH_TAX_DATA_TABLE)
        # conn = Connection(user='veerahcm', passwd='Veera@143', ashost='192.168.18.16',sysnr='D01', gwserv='3301', client='150')
        sap_login=get_sap_user_credentials()
        conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])

        result = conn.call('Z_HR_UPDATE_TAX_DECLARATION',CH_TAX_DATA_TABLE=CH_TAX_DATA_TABLE,CH_HRA_RENTS_TABLE=CH_HRA_RENTS_TABLE)
        conn.close()
        close_sap_user_credentials(sap_login['_id'])
        # conn = Connection(user='veerahcm', passwd='Veera&143', ashost='192.168.18.15',sysnr='Q01', gwserv='3300', client='900')
        # result = conn.call('Z_HR_UPDATE_TAX_DECLARATION',CH_TAX_DATA_TABLE=CH_TAX_DATA_TABLE,CH_HRA_RENTS_TABLE=CH_HRA_RENTS_TABLE)
        sent_emp_data=[]
        for jjj in result['CH_TAX_DATA_TABLE']:
            if jjj['I580_STATUS'] =="" and jjj['I581_STATUS']=="" and jjj['I582_STATUS'] =="" and jjj['I584_STATUS']=="" and jjj['I585_STATUS'] =="" and jjj['I586_STATUS']=="":
                update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS'],'sap_update':'1'}})
            else:
                update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS'],'sap_update':'2'}})
            sent_emp_data.append(jjj['EMPLOYEE_ID'])
        logging.info('Details Sent TO Sap - '+str(sent_emp_data))
    return 'success'


# commentedon 25th mornign for changing the logic in sending data to SAP investments proofs.
# @zen_proposal.route('/UPDATE_TO_SAP_BATCH/', methods = ['GET', 'POST'])
# def UPDATE_TO_SAP_BATCH():
#     debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
#     if debug_flag and debug_flag['debug']=='on':
#         debug_flag=debug_flag['debug_status']['ip_approve']
#     else:
#         debug_flag="off"
#     if  debug_flag=='on':
#         verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2016-2017','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED','debug':'1','sap_update':{'$exists':0}})
#     else:
#         verified_TD_Details=find_in_collection('Investment_Details',{'FY_YEAR':'2016-2017','SUBMIT_STATUS' : 'SUBMITED','APPROVE_STATUS':'APPROVED','sap_update':{'$exists':0}})
#     final_result_to_sap=[]
#     CH_TAX_DATA_TABLE=[]
#     CH_HRA_RENTS_TABLE=[]
#     for user in  verified_TD_Details:
#         if debug_flag=='on':
#             logging.info(user)
#         dates_range_year={'APR':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'MAY':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'JUN':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'JUL':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'AUG':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'SEP':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'OCT':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'NOV':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'DEC':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,30)},
#         'JAN':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)},
#         'FEB':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,28)},
#         'MAR':{'st_dt':datetime.date(2017,3,1),'end_dt':datetime.date(2017,3,31)}}
#         RESULT=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':user['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'})
#         list_of_fields_I580=['I580_SALARY_AMT','I580_VALUE_PREREQ_AMT','I580_PROFIT_AMT','I580_EXMPT_AMT','I580_PROF_TAX_AMT','I580_PROV_FUND_AMT','I580_INCOM_TAX_AMT','I580_SURCHRG_AMT','I580_EDU_CESS_AMT','I580_MED_EXMPT_AMT','I580_LEAVE_ENCASH_EXMPT_AMT','I580_GRAT_EXMPT_AMT','I580_VRS_EXMPT_AMT','I580_NUM_LTA_EXMPT_AMT','I580_LTA_EXMPT_X']
#         list_of_fields_I582=['I582_SCEA_X','I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT','I582_MDA_X','I582_MDA_START_DATE','I582_MDA_EXMPT_AMT','I582_LTA_X','I582_LTA_START_DATE','I582_LTA_EXMPT_AMT','I582_LTA_OBJPS','I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE','I582_LTA_STRT_LOC','I582_LTA_END_LOC','I582_LTA_TRAVEL_MODE','I582_LTA_TRAVEL_CLASS','I582_LTA_TKT_NUMBERS','I582_LTA_SELF_TRAVLD']
#         list_of_fields_I584=['I584_LOAN_TYPE','I584_LOAN_SEQ_NUM','I584_ACC_MONTH_SRT','I584_ACC_MONTH_END','I584_S24_LIMIT_AMT','I584_PAYROLL_LIMIT_AMT','I584_FINAL_LET_AMT','I584_S24_REPAIR_AMT','I584_S24_INTREST_AMT','I584_S24_OTHERS_AMT','I584_TDS_OTHERS_AMT','I584_PRFT_BUS_PROF_AMT','I584_LT_CAP_GAIN_N_AMT','I584_LT_CAP_GAIN_S_AMT','I584_ST_CAP_GAIN_AMT','I584_DIVIDENDS_AMT','I584_INTERESTS_AMT','I584_OTH_SOURCES_AMT']
#         list_of_fields_I585=['I585_A1_PENSIONS_AMT','I585_A2_INS_N_SCTZ_AMT','I585_A3_INS_SCTZ_AMT','I585_A4_INS_P_NSCTZ_AMT','I585_A5_INS_P_SCTZ_AMT','I585_A6_HLT_CHK_SELF_AMT','I585_A7_HLT_CHK_PARNT_AMT','I585_A8_EXP_VSN_SELF_AMT','I585_A9_EXP_VSN_PARNT_AMT','I585_A10_DISABLED_AMT','I585_A11_SDISABLED_AMT','I585_A12_MED_TRMT_AMT','I585_A13_MED_TRMT_SCTZ_AMT','I585_A14_MED_TRMT_VSCTZ_AMT','I585_A15_INT_HIGH_EDU_AMT','I585_A16_CRY_AMT','I585_A17_PMCM_RELIEF_AMT','I585_A18_NAT_CHIL_AMT','I585_A19_SWAS_BHT_KSH_AMT','I585_A20_CLN_GANGA_AMT','I585_A21_DRUG_ABUS_AMT','I585_A22_RENT_PAID_AMT','I585_A23_FORN_SOURC_AMT','I585_A24_SRV_ABROAD_AMT','I585_A25_SELF_DISABLE_AMT','I585_A27_PENSN_CENGOV_AMT','I585_A28_INTR_SAV_ACC_AMT','I585_A29_RG_ESS_AMT','I585_A30_HOMELOAN_INTR_AMT']
#         list_of_fields_I586=['I586_A1_LIFE_INSU_AMT','I586_A2_SUPER_FUND_AMT','I586_A3_NSS_AMT','I586_A4_ULIP_AMT','I586_A5_EQUITY_AMT','I586_A6_INFRA_MF_AMT','I586_A7_PPF_AMT','I586_A8_RPF_AMT','I586_A9_POST_OFFICE_AMT','I586_A10_NSC_AMT','I586_A11_ULIP_LIC_AMT','I586_A12_PF_AMT','I586_A13_HOUSE_LOAN_AMT','I586_A14_NSC_VIII_AMT','I586_A15_ANNUITY_LIC_AMT','I586_A16_MF_AMT','I586_A17_PENSION_AMT','I586_A18_NHB_AMT','I586_A19_OTH_SCHEME_AMT','I586_A20_DEF_ANTY_SP_AMT','I586_A21_DEF_ANUITY_AMT','I586_A22_TUTE_FEE_1_AMT','I586_A23_TUTE_FEE_2_AMT','I586_A24_TERM_DEPOSIT_AMT','I586_A25_PO_5YEAR_AMT','I586_A26_SCSS_AMT','I586_A27_SSS_AMT']
#         updt_arr={'EMPLOYEE_ID':user['EMPLOYEE_ID']}
#         test_updt_arry={}
#         ## not sending accom text in 581 infotype
#         for month in ['APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC','JAN','FEB','MAR']:
#             if 'I581_HRA_EXMPT_X' in RESULT:
#                 I581_HRA_EXMPT_X=RESULT['I581_HRA_EXMPT_X']
#             else:
#                 I581_HRA_EXMPT_X=''
#             if RESULT['I581_ACCOM_TYPE']=="1":
#                 updt_data_hra={'EMPLOYEE_ID':user['EMPLOYEE_ID'],'I581_RENT_MONTH_NAME': month,'I581_HRA_EXMPT_X':I581_HRA_EXMPT_X,'I581_ACCOM_TYPE': RESULT['I581_ACCOM_TYPE'], 'I581_RENT': RESULT['I581_'+month+'_RENT'],'I581_OWNER_ADDRESS2':'','I581_OWNER_ADDRESS3': '', 'I581_TOT_FURN_VAL':'0', 'I581_HELPRS_AMT': '0',  'I581_RENT_START_DATE': dates_range_year[month]['st_dt'], 'I581_OWNER_ADDRESS1': '',  'I581_METRO': '', 'I581_HELPERS': '00000',  'I581_RENT_END_DATE':dates_range_year[month]['end_dt'], 'I581_HIRE_CRG_FURN': '0'}
#                 updt_arr.update({'I581_X':'X'})
#                 CH_HRA_RENTS_TABLE.append(dict(updt_data_hra))
#             else:
#                 logging.info('accomodation type selected as - '+RESULT['I581_ACCOM_TYPE'])
#         # print CH_HRA_RENTS_TABLE,' -------------- '
#         # print updt_arr_hra,' ..........................'
#         for field in list_of_fields_I580:            
#             if field+'_APPROVED' in RESULT:
#                 updt_arr.update({field:RESULT[field+'_APPROVED'],'I580_X':'X'})
#             else:
#                 updt_arr.update({field:'0'})
#             # if user['EMPLOYEE_ID']=='00002333':
#             #     print updt_arr,' I580 NEW JOINEE' 

#         #### check the field LTA start date what it hasto be filled and sent to SAP
#         # if float(RESULT['I582_MDA_EXMPT_AMT']) != 0 or RESULT['I582_MDA_EXMPT_AMT']!='':
#         #     # I582_MDA_X:[I582_MDA_START_DATE','I582_MDA_EXMPT_AMT]
#         #     updt_arr.update({'I582_MDA_X':'X','I582_MDA_START_DATE':datetime.date(2017,2,20),'I582_MDA_EXMPT_AMT':RESULT['I582_MDA_EXMPT_AMT'],'I582_X':'X'})
        
#         #### check the field LTA start date what it hasto be filled and sent to SAP
#         if RESULT['I582_SCEA_EXMPT_AMT'] !='0.00':
#             if RESULT['I582_SCEA_EXMPT_AMT'] =='':
#                 pass
#             else:
#                 print RESULT['I582_SCEA_EXMPT_AMT']           
#                 updt_arr.update({'I582_X':'X','I582_SCEA_X':'X'})
#                 # I582_SCEA_X:[I582_SCEA_START_DATE','I582_SCEA_EXMPT_AMT]
#                 updt_arr.update({'I582_SCEA_START_DATE':datetime.date(2017,2,20),'I582_SCEA_EXMPT_AMT':RESULT['I582_SCEA_EXMPT_AMT']})
        
#         # if float(RESULT['I582_LTA_EXMPT_AMT']) != 0 or RESULT['I582_LTA_EXMPT_AMT']!='':
#         #     # I582_LTA_X:[I582_LTA_START_DATE','I582_LTA_EXMPT_AMT','I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE','I582_LTA_STRT_LOC','I582_LTA_END_LOC','I582_LTA_TRAVEL_MODE','I582_LTA_TRAVEL_CLASS','I582_LTA_TKT_NUMBERS','I582_LTA_SELF_TRAVLD]
#         #     #### check the field LTA start date what it hasto be filled and sent to SAP
#         #     if RESULT['I582_LTA_JOURNY_START_DATE']!='None':
#            #      updt_arr.update({'I582_LTA_X':'X'})
#            #      for field in list_of_fields_I582:
#            #          if field in RESULT:
#            #              # print user['EMPLOYEE_ID'],'emppp'
#            #              if field in ['I582_LTA_JOURNY_START_DATE','I582_LTA_JOURNY_END_DATE']:
#            #                  # print {field:datetime.date(2017,2,20)},' I582 LTA '
#            #                  try:
#            #                      updt_arr.update({field:datetime.datetime.strptime(RESULT[field],'%Y/%m/%d')})
#            #                  except:
#            #                      date_conv=datetime.datetime.strptime(RESULT[field],'%d/%m/%Y').strftime('%Y/%m/%d')
#            #                      updt_arr.update({field:datetime.datetime.strptime(date_conv,'%Y/%m/%d')})
#            #                  # updt_arr.update({field:datetime.date(2017,2,20)})
#            #              elif field in ['I582_LTA_START_DATE']:
#            #                updt_arr.update({field:datetime.date(2017,2,20)})
#            #              elif field in ['I582_LTA_TRAVEL_MODE','I582_LTA_TKT_NUMBERS','I582_LTA_STRT_LOC','I582_LTA_STRT_LOC','I582_LTA_END_LOC']:
#            #                  if 'I582_LTA_END_LOC' in RESULT and RESULT['I582_LTA_END_LOC'] !='':
#            #                      updt_arr.update({field:RESULT[field]})
#            #                  else:
#            #                      updt_arr.update({field:''})   
#            #      if 'I582_LTA_EXMPT_AMT_APPROVED' in RESULT:
#            #          updt_arr.update({'I582_LTA_EXMPT_AMT':RESULT['I582_LTA_EXMPT_AMT_APPROVED'],'I582_X':'X'})   

#         #currently using fields in 584:
        
#         # if RESULT['I584_SUBTYPE']=='0001':
#         #     if RESULT['I584_HOME_FUL_RNT_X']=='X':
#         #         updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})
#         #     elif RESULT['I584_HOME_SELF_X']=='X':
#         #         updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_HOME_SELF_X':'X','I584_X':'X'})
#         #     elif RESULT['I584_HOME_PART_RNT_X']=='X':
#         #         updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_PART_RNT_X':'X','I584_X':'X'})

#         # elif RESULT['I584_SUBTYPE']=='1':
#         #     updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_HOME_SELF_X':'X','I584_X':'X'})
#         # elif RESULT['I584_SUBTYPE']=='2':
#         #     updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_PART_RNT_X':'X','I584_X':'X'})
#         # elif RESULT['I584_SUBTYPE']=='3':
#         #     updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':RESULT['I584_S24_INTREST_AMT_APPROVED'],'I584_FINAL_LET_AMT':RESULT['I584_FINAL_LET_AMT_APPROVED'],'I584_S24_REPAIR_AMT':RESULT['I584_S24_REPAIR_AMT_APPROVED'],'I584_S24_OTHERS_AMT':RESULT['I584_S24_OTHERS_AMT_APPROVED'],'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})

#         if RESULT['I584_SUBTYPE']=='0001':
#             if RESULT['I584_HOME_FUL_RNT_X']=='X':
#                 if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
#                     I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
#                 else:
#                     I584_S24_INTREST_AMT_APPROVED='0'
#                 if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
#                     I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
#                 else:
#                     I584_FINAL_LET_AMT_APPROVED='0'
#                 if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
#                     I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
#                 else:
#                     I584_S24_OTHERS_AMT_APPROVED='0'
#                 if 'I584_S24_REPAIR_AMT' in RESULT:
#                     I584_S24_REPAIR_AMT=RESULT['I584_S24_REPAIR_AMT']
#                 else:
#                     I584_S24_REPAIR_AMT='0'
#                 updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})
#             elif RESULT['I584_HOME_SELF_X']=='X':
#                 if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
#                     I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
#                 else:
#                     I584_S24_INTREST_AMT_APPROVED='0'
#                 updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_HOME_SELF_X':'X','I584_X':'X'})
#             elif RESULT['I584_HOME_PART_RNT_X']=='X':
#                 if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
#                     I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
#                 else:
#                     I584_S24_INTREST_AMT_APPROVED='0'
#                 if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
#                     I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
#                 else:
#                     I584_FINAL_LET_AMT_APPROVED='0'
#                 if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
#                     I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
#                 else:
#                     I584_S24_OTHERS_AMT_APPROVED='0'
#                 if 'I584_S24_REPAIR_AMT' in RESULT:
#                     I584_S24_REPAIR_AMT=RESULT['I584_S24_REPAIR_AMT']
#                 else:
#                     I584_S24_REPAIR_AMT='0'
#                 updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_PART_RNT_X':'X','I584_X':'X'})

#         elif RESULT['I584_SUBTYPE']=='1':
#             if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
#                 I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
#             else:
#                 I584_S24_INTREST_AMT_APPROVED='0'
#             updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_HOME_SELF_X':'X','I584_X':'X'})
#         elif RESULT['I584_SUBTYPE']=='2':
#             if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
#                 I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
#             else:
#                 I584_S24_INTREST_AMT_APPROVED='0'
#             if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
#                 I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
#             else:
#                 I584_FINAL_LET_AMT_APPROVED='0'
#             if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
#                 I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
#             else:
#                 I584_S24_OTHERS_AMT_APPROVED='0'
#             if 'I584_S24_REPAIR_AMT' in RESULT:
#                 I584_S24_REPAIR_AMT=RESULT['I584_S24_REPAIR_AMT']
#             else:
#                 I584_S24_REPAIR_AMT='0'
#             updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_PART_RNT_X':'X','I584_X':'X'})
#         elif RESULT['I584_SUBTYPE']=='3':
#             if 'I584_S24_INTREST_AMT_APPROVED' in RESULT:
#                 I584_S24_INTREST_AMT_APPROVED=RESULT['I584_S24_INTREST_AMT_APPROVED']
#             else:
#                 I584_S24_INTREST_AMT_APPROVED='0'
#             if 'I584_FINAL_LET_AMT_APPROVED' in RESULT:
#                 I584_FINAL_LET_AMT_APPROVED=RESULT['I584_FINAL_LET_AMT_APPROVED']
#             else:
#                 I584_FINAL_LET_AMT_APPROVED='0'
#             if 'I584_S24_OTHERS_AMT_APPROVED' in RESULT:
#                 I584_S24_OTHERS_AMT_APPROVED=RESULT['I584_S24_OTHERS_AMT_APPROVED']
#             else:
#                 I584_S24_OTHERS_AMT_APPROVED='0'
#             if 'I584_S24_REPAIR_AMT_APPROVED' in RESULT:
#                 I584_S24_REPAIR_AMT_APPROVED=RESULT['I584_S24_REPAIR_AMT_APPROVED']
#             else:
#                 I584_S24_REPAIR_AMT_APPROVED='0'
#             updt_arr.update({'I584_SUBTYPE':'0001','I584_S24_INTREST_AMT':I584_S24_INTREST_AMT_APPROVED,'I584_FINAL_LET_AMT':I584_FINAL_LET_AMT_APPROVED,'I584_S24_REPAIR_AMT':I584_S24_REPAIR_AMT_APPROVED,'I584_S24_OTHERS_AMT':I584_S24_OTHERS_AMT_APPROVED,'I584_HOME_FUL_RNT_X':'X','I584_X':'X'})
#         #INCOME FROM OTHER THAN 80C
#         for field in list_of_fields_I585:
#             # print field+'_APPROVED'
#             if field+'_APPROVED' in RESULT:
#                 # print RESULT[field+'_APPROVED']
#                 updt_arr.update({field:RESULT[field+'_APPROVED'],'I585_ACTUAL_X':'X','I585_X':'X'})
#             else:
#                 if RESULT[field] !='0.00':
#                     updt_arr.update({field:RESULT[field],'I585_ACTUAL_X':'X','I585_X':'X'})
#                 else:
#                     updt_arr.update({field:'0'})
#         # print '------------------'
#         #INCOME FROM OTHER SOURCES
#         for field1 in list_of_fields_I586:
#             if field1+'_APPROVED' in RESULT:
#                 # print RESULT[field1+'_APPROVED']
#                 updt_arr.update({field1:RESULT[field1+'_APPROVED'],'I586_ACTUAL_X':'X','I586_X':'X'})
#             else:
#                 updt_arr.update({field1:'0'})
#         # print updt_arr,'final update array'
#         # print '-------++++++++++++++++++-------------'
#         CH_TAX_DATA_TABLE.append(dict(updt_arr))
#     if debug_flag=='on':
#         logging.info(CH_TAX_DATA_TABLE)
#     # print CH_TAX_DATA_TABLE
#     # print type(CH_HRA_RENTS_TABLE)
#     # testdata={'I585_A22_RENT_PAID_AMT': Decimal('22.00'), 'I580_INCOM_TAX_AMT': Decimal('7.00'), 'I586_P3_NSS_AMT': Decimal('3.00'), 'I585_A5_INS_P_SCTZ_AMT': Decimal('5.00'), 'I586_STATUS': '', 'I585_A2_INS_N_SCTZ_AMT': Decimal('2.00'), 'I585_A3_INS_SCTZ_AMT': Decimal('3.00'), 'I585_A8_EXP_VSN_SELF_AMT': Decimal('8.00'), 'I586_P27_SSS_AMT': Decimal('27.00'), 'I585_A12_MED_TRMT_AMT': Decimal('12.00'), 'I585_A19_SWAS_BHT_KSH_AMT': Decimal('19.00'), 'I586_A20_DEF_ANTY_SP_AMT': Decimal('20.00'), 'I584_PRFT_BUS_PROF_AMT': Decimal('0.00'), 'I581_STATUS': '', 'I580_MED_EXMPT_AMT': Decimal('10.00'), 'I582_STATUS': '', 'I586_A24_TERM_DEPOSIT_AMT': Decimal('24.00'), 'I584_ST_CAP_GAIN_AMT': Decimal('0.00'), 'I582_LTA_SELF_TRAVLD': 'Y', 'I585_X': '', 'I580_PROF_TAX_AMT': Decimal('5.00'), 'I585_A25_SELF_DISABLE_AMT': Decimal('25.00'), 'I585_P27_PENSN_CENGOV_AMT': Decimal('27.00'), 'I582_LTA_START_DATE': datetime.date(2017, 3, 20), 'I586_A23_TUTE_FEE_2_AMT': Decimal('23.00'), 'I585_PROVIS_X': '', 'I585_P3_INS_SCTZ_AMT': Decimal('3.00'), 'I586_A21_DEF_ANUITY_AMT': Decimal('21.00'), 'I586_P25_PO_5YEAR_AMT': Decimal('25.00'), 'I581_X': '', 'I585_A10_DISABLED_AMT': Decimal('10.00'), 'EMPLOYEE_ID': '00001962', 'I580_GRAT_EXMPT_AMT': Decimal('12.00'), 'I585_A24_SRV_ABROAD_AMT': Decimal('24.00'), 'I580_SURCHRG_AMT': Decimal('8.00'), 'I584_PAYROLL_LIMIT_AMT': Decimal('0.00'), 'I586_P20_DEF_ANTY_SP_AMT': Decimal('20.00'), 'I586_A5_EQUITY_AMT': Decimal('5.00'), 'I582_MDA_EXMPT_AMT': Decimal('20000.00'), 'I582_SCEA_EXMPT_AMT': Decimal('3000.00'),'I585_P7_HLT_CHK_PARNT_AMT': Decimal('7.00'), 'I585_A23_FORN_SOURC_AMT': Decimal('23.00'), 'I586_A18_NHB_AMT': Decimal('18.00'), 'I585_P17_PMCM_RELIEF_AMT': Decimal('17.00'), 'I584_DIVIDENDS_AMT': Decimal('0.00'), 'I584_ACC_MONTH_SRT': '00', 'I582_MDA_X': 'X', 'I584_HOME_SELF_X': '', 'I585_A27_PENSN_CENGOV_AMT': Decimal('27.00'), 'I586_A1_LIFE_INSU_AMT': Decimal('1.00'), 'I585_P14_MED_TRMT_VSCTZ_AMT': Decimal('14.00'), 'I580_LTA_EXMPT_X': '1', 'I582_X': '', 'I586_P18_NHB_AMT': Decimal('18.00'), 'I582_LTA_JOURNY_START_DATE': datetime.date(2017, 2, 15), 'I586_A9_POST_OFFICE_AMT': Decimal('9.00'), 'I586_A12_PF_AMT': Decimal('12.00'), 'I585_A30_HOMELOAN_INTR_AMT': Decimal('30.00'), 'I586_A27_SSS_AMT': Decimal('27.00'), 'I580_X': '', 'I584_TDS_OTHERS_AMT': Decimal('60.00'), 'I586_A13_HOUSE_LOAN_AMT': Decimal('13.00'), 'I582_LTA_TRAVEL_MODE': 'WATER', 'I585_P18_NAT_CHIL_AMT': Decimal('18.00'), 'I586_X': '', 'I585_P6_HLT_CHK_SELF_AMT': Decimal('6.00'), 'I585_P26_SELF_SDISABLE_AMT': Decimal('26.00'), 'I585_A14_MED_TRMT_VSCTZ_AMT': Decimal('14.00'), 'I585_P16_CRY_AMT': Decimal('16.00'), 'I584_X': '', 'I582_LTA_END_LOC': 'LANKA', 'I584_HOME_PART_RNT_X': 'X', 'I584_LOAN_SEQ_NUM': '', 'I586_A10_NSC_AMT': Decimal('10.00'), 'I580_VRS_EXMPT_AMT': Decimal('13.00'), 'I586_P22_TUTE_FEE_1_AMT': Decimal('22.00'), 'I580_SALARY_AMT': Decimal('1.00'), 'I584_STATUS': '', 'I586_P5_EQUITY_AMT': Decimal('5.00'), 'I584_LOAN_TYPE': '', 'I586_P17_PENSION_AMT': Decimal('17.00'), 'I580_NUM_LTA_EXMPT_AMT': '01', 'I585_A18_NAT_CHIL_AMT': Decimal('18.00'), 'I580_LEAVE_ENCASH_EXMPT_AMT': Decimal('11.00'), 'I580_EXMPT_AMT': Decimal('4.00'), 'I586_A26_SCSS_AMT': Decimal('26.00'), 'I586_A2_SUPER_FUND_AMT': Decimal('2.00'), 'I585_A29_RG_ESS_AMT': Decimal('29.00'), 'I586_ACTUAL_X': '', 'OBJPS': '', 'I586_P13_HOUSE_LOAN_AMT': Decimal('13.00'), 'I585_A16_CRY_AMT': Decimal('16.00'), 'I586_A7_PPF_AMT': Decimal('7.00'), 'I585_A4_INS_P_NSCTZ_AMT': Decimal('4.00'), 'I585_A15_INT_HIGH_EDU_AMT': Decimal('15.00'), 'I584_S24_REPAIR_AMT': Decimal('30.00'), 'I584_OTH_SOURCES_AMT': Decimal('0.00'), 'I585_A7_HLT_CHK_PARNT_AMT': Decimal('7.00'), 'I585_P22_RENT_PAID_AMT': Decimal('22.00'), 'I585_P13_MED_TRMT_SCTZ_AMT': Decimal('13.00'), 'I582_LTA_X': 'X', 'I580_VALUE_PREREQ_AMT': Decimal('2.00'), 'I585_P4_INS_P_NSCTZ_AMT': Decimal('4.00'), 'I584_S24_LIMIT_AMT': '', 'I586_A8_RPF_AMT': Decimal('8.00'), 'I586_P16_MF_AMT': Decimal('16.00'), 'I585_A28_INTR_SAV_ACC_AMT': Decimal('28.00'), 'I584_S24_INTREST_AMT': Decimal('40.00'), 'I586_P15_ANNUITY_LIC_AMT': Decimal('15.00'), 'I586_P2_SUPER_FUND_AMT': Decimal('2.00'), 'I586_A16_MF_AMT': Decimal('16.00'), 'I585_P29_RG_ESS_AMT': Decimal('29.00'), 'I586_P24_TERM_DEPOSIT_AMT': Decimal('24.00'), 'I586_A17_PENSION_AMT': Decimal('17.00'), 'I582_SCEA_X': 'X', 'I585_A17_PMCM_RELIEF_AMT': Decimal('17.00'), 'I586_A15_ANNUITY_LIC_AMT': Decimal('15.00'), 'I586_P8_RPF_AMT': Decimal('8.00'), 'I586_P7_PPF_AMT': Decimal('7.00'), 'I585_P11_SDISABLED_AMT': Decimal('11.00'), 'I585_P8_EXP_VSN_SELF_AMT': Decimal('8.00'), 'I586_P14_NSC_VIII_AMT': Decimal('14.00'), 'I582_LTA_TKT_NUMBERS': '2323,2324', 'I586_P11_ULIP_LIC_AMT': Decimal('11.00'), 'I585_ACTUAL_X': '', 'I586_P6_INFRA_MF_AMT': Decimal('6.00'), 'I586_P12_PF_AMT': Decimal('12.00'), 'I586_PROVIS_X': '', 'I580_STATUS': '', 'I584_SUBTYPE': '0001', 'I580_PROFIT_AMT': Decimal('3.00'), 'I586_A25_PO_5YEAR_AMT': Decimal('25.00'), 'I585_P30_HOMELOAN_INTR_AMT': Decimal('30.00'), 'I585_P20_CLN_GANGA_AMT': Decimal('20.00'), 'I585_A21_DRUG_ABUS_AMT': Decimal('21.00'), 'I586_P4_ULIP_AMT': Decimal('4.00'), 'I586_A3_NSS_AMT': Decimal('3.00'), 'I585_P25_SELF_DISABLE_AMT': Decimal('25.00'), 'I580_EDU_CESS_AMT': Decimal('9.00'), 'I584_FINAL_LET_AMT': Decimal('20.00'), 'I586_P26_SCSS_AMT': Decimal('26.00'), 'I586_P23_TUTE_FEE_2_AMT': Decimal('23.00'), 'I584_INTERESTS_AMT': Decimal('0.00'), 'I585_P24_SRV_ABROAD_AMT': Decimal('24.00'), 'I582_LTA_OBJPS': '01', 'I584_LT_CAP_GAIN_S_AMT': Decimal('0.00'), 'I586_A22_TUTE_FEE_1_AMT': Decimal('22.00'), 'I582_LTA_TRAVEL_CLASS': 'A', 'SUBTY': '', 'I585_P28_INTR_SAV_ACC_AMT': Decimal('28.00'), 'I585_P10_DISABLED_AMT': Decimal('10.00'), 'I586_P10_NSC_AMT': Decimal('10.00'), 'I580_PROV_FUND_AMT': Decimal('6.00'), 'I586_A19_OTH_SCHEME_AMT': Decimal('19.00'), 'I585_P1_PENSIONS_AMT': Decimal('1.00'), 'I585_A11_SDISABLED_AMT': Decimal('11.00'), 'I585_P21_DRUG_ABUS_AMT': Decimal('21.00'), 'I582_MDA_START_DATE': datetime.date(2017, 2, 6), 'I585_P12_MED_TRMT_AMT': Decimal('12.00'), 'I582_SUBTYPE': 'LTA', 'I585_A6_HLT_CHK_SELF_AMT': Decimal('6.00'), 'I586_P21_DEF_ANUITY_AMT': Decimal('21.00'), 'I585_A26_SELF_SDISABLE_AMT': Decimal('26.00'), 'I586_A14_NSC_VIII_AMT': Decimal('14.00'), 'I584_S24_OTHERS_AMT': Decimal('50.00'), 'I585_A20_CLN_GANGA_AMT': Decimal('20.00'), 'I586_A11_ULIP_LIC_AMT': Decimal('11.00'), 'I585_P23_FORN_SOURC_AMT': Decimal('23.00'), 'I582_SCEA_START_DATE': datetime.date(2017, 2, 1), 'I586_A6_INFRA_MF_AMT': Decimal('6.00'), 'I582_LTA_JOURNY_END_DATE': datetime.date(2017, 2, 19), 'I584_ACC_MONTH_END': '00', 'I585_A9_EXP_VSN_PARNT_AMT': Decimal('9.00'), 'I584_LT_CAP_GAIN_N_AMT': Decimal('0.00'), 'I586_P1_LIFE_INSU_AMT': Decimal('1.00'), 'I585_P9_EXP_VSN_PARNT_AMT': Decimal('9.00'), 'I582_LTA_STRT_LOC': 'HYD', 'I586_A4_ULIP_AMT': Decimal('4.00'), 'I585_STATUS': '', 'I585_A1_PENSIONS_AMT': Decimal('1.00'), 'I585_A13_MED_TRMT_SCTZ_AMT': Decimal('13.00'), 'I586_P9_POST_OFFICE_AMT': Decimal('9.00'), 'I584_HOME_FUL_RNT_X': '', 'I582_LTA_EXMPT_AMT': Decimal('20000.00'), 'I585_P5_INS_P_SCTZ_AMT': Decimal('5.00'), 'I585_P19_SWAS_BHT_KSH_AMT': Decimal('19.00'), 'I585_P2_INS_N_SCTZ_AMT': Decimal('2.00'), 'I585_P15_INT_HIGH_EDU_AMT': Decimal('15.00'), 'I586_P19_OTH_SCHEME_AMT': Decimal('19.00')}
#     # testdata={'I585_A22_RENT_PAID_AMT': '22.00', 'I580_INCOM_TAX_AMT': '7.00', 'I586_P3_NSS_AMT': '3.00', 'I585_A5_INS_P_SCTZ_AMT': '5.00', 'I586_STATUS': '', 'I585_A2_INS_N_SCTZ_AMT': '2.00', 'I585_A3_INS_SCTZ_AMT': '3.00', 'I585_A8_EXP_VSN_SELF_AMT': '8.00', 'I586_P27_SSS_AMT': '27.00', 'I585_A12_MED_TRMT_AMT': '12.00', 'I585_A19_SWAS_BHT_KSH_AMT': '19.00', 'I586_A20_DEF_ANTY_SP_AMT': '20.00', 'I584_PRFT_BUS_PROF_AMT': '0.00', 'I581_STATUS': '', 'I580_MED_EXMPT_AMT': '10.00', 'I582_STATUS': '', 'I586_A24_TERM_DEPOSIT_AMT': '24.00', 'I584_ST_CAP_GAIN_AMT': '0.00', 'I582_LTA_SELF_TRAVLD': 'Y', 'I585_X': '', 'I580_PROF_TAX_AMT': '5.00', 'I585_A25_SELF_DISABLE_AMT': '25.00', 'I585_P27_PENSN_CENGOV_AMT': '27.00', 'I582_LTA_START_DATE': datetime.date(2017, 3, 20), 'I586_A23_TUTE_FEE_2_AMT': '23.00', 'I585_PROVIS_X': '', 'I585_P3_INS_SCTZ_AMT': '3.00', 'I586_A21_DEF_ANUITY_AMT': '21.00', 'I586_P25_PO_5YEAR_AMT': '25.00', 'I581_X': '', 'I585_A10_DISABLED_AMT': '10.00', 'EMPLOYEE_ID': '00001962', 'I580_GRAT_EXMPT_AMT': '12.00', 'I585_A24_SRV_ABROAD_AMT': '24.00', 'I580_SURCHRG_AMT': '8.00', 'I584_PAYROLL_LIMIT_AMT': '0.00', 'I586_P20_DEF_ANTY_SP_AMT': '20.00', 'I586_A5_EQUITY_AMT': '5.00', 'I582_MDA_EXMPT_AMT': '20000.00', 'I582_SCEA_EXMPT_AMT': '3000.00','I585_P7_HLT_CHK_PARNT_AMT': '7.00', 'I585_A23_FORN_SOURC_AMT': '23.00', 'I586_A18_NHB_AMT': '18.00', 'I585_P17_PMCM_RELIEF_AMT': '17.00', 'I584_DIVIDENDS_AMT': '0.00', 'I584_ACC_MONTH_SRT': '00', 'I582_MDA_X': 'X', 'I584_HOME_SELF_X': '', 'I585_A27_PENSN_CENGOV_AMT': '27.00', 'I586_A1_LIFE_INSU_AMT': '1.00', 'I585_P14_MED_TRMT_VSCTZ_AMT': '14.00', 'I580_LTA_EXMPT_X': '1', 'I582_X': '', 'I586_P18_NHB_AMT': '18.00', 'I582_LTA_JOURNY_START_DATE': datetime.date(2017, 2, 15), 'I586_A9_POST_OFFICE_AMT': '9.00', 'I586_A12_PF_AMT': '12.00', 'I585_A30_HOMELOAN_INTR_AMT': '30.00', 'I586_A27_SSS_AMT': '27.00', 'I580_X': '', 'I584_TDS_OTHERS_AMT': '60.00', 'I586_A13_HOUSE_LOAN_AMT': '13.00', 'I582_LTA_TRAVEL_MODE': 'WATER', 'I585_P18_NAT_CHIL_AMT': '18.00', 'I586_X': '', 'I585_P6_HLT_CHK_SELF_AMT': '6.00', 'I585_P26_SELF_SDISABLE_AMT': '26.00', 'I585_A14_MED_TRMT_VSCTZ_AMT': '14.00', 'I585_P16_CRY_AMT': '16.00', 'I584_X': '', 'I582_LTA_END_LOC': 'LANKA', 'I584_HOME_PART_RNT_X': 'X', 'I584_LOAN_SEQ_NUM': '', 'I586_A10_NSC_AMT': '10.00', 'I580_VRS_EXMPT_AMT': '13.00', 'I586_P22_TUTE_FEE_1_AMT': '22.00', 'I580_SALARY_AMT': '1.00', 'I584_STATUS': '', 'I586_P5_EQUITY_AMT': '5.00', 'I584_LOAN_TYPE': '', 'I586_P17_PENSION_AMT': '17.00', 'I580_NUM_LTA_EXMPT_AMT': '01', 'I585_A18_NAT_CHIL_AMT': '18.00', 'I580_LEAVE_ENCASH_EXMPT_AMT': '11.00', 'I580_EXMPT_AMT': '4.00', 'I586_A26_SCSS_AMT': '26.00', 'I586_A2_SUPER_FUND_AMT': '2.00', 'I585_A29_RG_ESS_AMT': '29.00', 'I586_ACTUAL_X': '', 'OBJPS': '', 'I586_P13_HOUSE_LOAN_AMT': '13.00', 'I585_A16_CRY_AMT': '16.00', 'I586_A7_PPF_AMT': '7.00', 'I585_A4_INS_P_NSCTZ_AMT': '4.00', 'I585_A15_INT_HIGH_EDU_AMT': '15.00', 'I584_S24_REPAIR_AMT': '30.00', 'I584_OTH_SOURCES_AMT': '0.00', 'I585_A7_HLT_CHK_PARNT_AMT': '7.00', 'I585_P22_RENT_PAID_AMT': '22.00', 'I585_P13_MED_TRMT_SCTZ_AMT': '13.00', 'I582_LTA_X': 'X', 'I580_VALUE_PREREQ_AMT': '2.00', 'I585_P4_INS_P_NSCTZ_AMT': '4.00', 'I584_S24_LIMIT_AMT': '', 'I586_A8_RPF_AMT': '8.00', 'I586_P16_MF_AMT': '16.00', 'I585_A28_INTR_SAV_ACC_AMT': '28.00', 'I584_S24_INTREST_AMT': '40.00', 'I586_P15_ANNUITY_LIC_AMT': '15.00', 'I586_P2_SUPER_FUND_AMT': '2.00', 'I586_A16_MF_AMT': '16.00', 'I585_P29_RG_ESS_AMT': '29.00', 'I586_P24_TERM_DEPOSIT_AMT': '24.00', 'I586_A17_PENSION_AMT': '17.00', 'I582_SCEA_X': 'X', 'I585_A17_PMCM_RELIEF_AMT': '17.00', 'I586_A15_ANNUITY_LIC_AMT': '15.00', 'I586_P8_RPF_AMT': '8.00', 'I586_P7_PPF_AMT': '7.00', 'I585_P11_SDISABLED_AMT': '11.00', 'I585_P8_EXP_VSN_SELF_AMT': '8.00', 'I586_P14_NSC_VIII_AMT': '14.00', 'I582_LTA_TKT_NUMBERS': '2323,2324', 'I586_P11_ULIP_LIC_AMT': '11.00', 'I585_ACTUAL_X': '', 'I586_P6_INFRA_MF_AMT': '6.00', 'I586_P12_PF_AMT': '12.00', 'I586_PROVIS_X': '', 'I580_STATUS': '', 'I584_SUBTYPE': '0001', 'I580_PROFIT_AMT': '3.00', 'I586_A25_PO_5YEAR_AMT': '25.00', 'I585_P30_HOMELOAN_INTR_AMT': '30.00', 'I585_P20_CLN_GANGA_AMT': '20.00', 'I585_A21_DRUG_ABUS_AMT': '21.00', 'I586_P4_ULIP_AMT': '4.00', 'I586_A3_NSS_AMT': '3.00', 'I585_P25_SELF_DISABLE_AMT': '25.00', 'I580_EDU_CESS_AMT': '9.00', 'I584_FINAL_LET_AMT': '20.00', 'I586_P26_SCSS_AMT': '26.00', 'I586_P23_TUTE_FEE_2_AMT': '23.00', 'I584_INTERESTS_AMT': '0.00', 'I585_P24_SRV_ABROAD_AMT': '24.00', 'I582_LTA_OBJPS': '01', 'I584_LT_CAP_GAIN_S_AMT': '0.00', 'I586_A22_TUTE_FEE_1_AMT': '22.00', 'I582_LTA_TRAVEL_CLASS': 'A', 'SUBTY': '', 'I585_P28_INTR_SAV_ACC_AMT': '28.00', 'I585_P10_DISABLED_AMT': '10.00', 'I586_P10_NSC_AMT': '10.00', 'I580_PROV_FUND_AMT': '6.00', 'I586_A19_OTH_SCHEME_AMT': '19.00', 'I585_P1_PENSIONS_AMT': '1.00', 'I585_A11_SDISABLED_AMT': '11.00', 'I585_P21_DRUG_ABUS_AMT': '21.00', 'I582_MDA_START_DATE': datetime.date(2017, 2, 6), 'I585_P12_MED_TRMT_AMT': '12.00', 'I582_SUBTYPE': 'LTA', 'I585_A6_HLT_CHK_SELF_AMT': '6.00', 'I586_P21_DEF_ANUITY_AMT': '21.00', 'I585_A26_SELF_SDISABLE_AMT': '26.00', 'I586_A14_NSC_VIII_AMT': '14.00', 'I584_S24_OTHERS_AMT': '50.00', 'I585_A20_CLN_GANGA_AMT': '20.00', 'I586_A11_ULIP_LIC_AMT': '11.00', 'I585_P23_FORN_SOURC_AMT': '23.00', 'I582_SCEA_START_DATE': datetime.date(2017, 2, 1), 'I586_A6_INFRA_MF_AMT': '6.00', 'I582_LTA_JOURNY_END_DATE': datetime.date(2017, 2, 19), 'I584_ACC_MONTH_END': '00', 'I585_A9_EXP_VSN_PARNT_AMT': '9.00', 'I584_LT_CAP_GAIN_N_AMT': '0.00', 'I586_P1_LIFE_INSU_AMT': '1.00', 'I585_P9_EXP_VSN_PARNT_AMT': '9.00', 'I582_LTA_STRT_LOC': 'HYD', 'I586_A4_ULIP_AMT': '4.00', 'I585_STATUS': '', 'I585_A1_PENSIONS_AMT': '1.00', 'I585_A13_MED_TRMT_SCTZ_AMT': '13.00', 'I586_P9_POST_OFFICE_AMT': '9.00', 'I584_HOME_FUL_RNT_X': '', 'I582_LTA_EXMPT_AMT': '20000.00', 'I585_P5_INS_P_SCTZ_AMT': '5.00', 'I585_P19_SWAS_BHT_KSH_AMT': '19.00', 'I585_P2_INS_N_SCTZ_AMT': '2.00', 'I585_P15_INT_HIGH_EDU_AMT': '15.00', 'I586_P19_OTH_SCHEME_AMT': '19.00'}
#     # conn = Connection(user='veerahcm', passwd='Veera@143', ashost='192.168.18.16',sysnr='D01', gwserv='3301', client='150')
#     if len(verified_TD_Details)!=0:
#         sap_login=get_sap_user_credentials()
#         conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])

#         result = conn.call('Z_HR_UPDATE_TAX_DECLARATION',CH_TAX_DATA_TABLE=CH_TAX_DATA_TABLE,CH_HRA_RENTS_TABLE=CH_HRA_RENTS_TABLE)
#         conn.close()
#         close_sap_user_credentials(sap_login['_id'])
#         # conn = Connection(user='veerahcm', passwd='Veera&143', ashost='192.168.18.15',sysnr='Q01', gwserv='3300', client='900')
#         # result = conn.call('Z_HR_UPDATE_TAX_DECLARATION',CH_TAX_DATA_TABLE=CH_TAX_DATA_TABLE,CH_HRA_RENTS_TABLE=CH_HRA_RENTS_TABLE)
#         sent_emp_data=[]
#         for jjj in result['CH_TAX_DATA_TABLE']:
#             if jjj['I580_STATUS'] =="" and jjj['I581_STATUS']=="" and jjj['I582_STATUS'] =="" and jjj['I584_STATUS']=="" and jjj['I585_STATUS'] =="" and jjj['I586_STATUS']=="":
#                 update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS']}})
#             else:
#                 update_collection('Investment_Details',{'EMPLOYEE_ID':jjj['EMPLOYEE_ID'],'FY_YEAR':'2016-2017'},{'$set':{'I580_STATUS':jjj['I580_STATUS'],'I581_STATUS':jjj['I581_STATUS'],'I582_STATUS':jjj['I582_STATUS'],'I584_STATUS':jjj['I584_STATUS'],'I585_STATUS':jjj['I585_STATUS'],'I586_STATUS':jjj['I586_STATUS']}})
#             sent_emp_data.append(jjj['EMPLOYEE_ID'])
#         logging.info('Details Sent TO Sap - '+str(sent_emp_data))
#         return 'success'
#     else:
#         return 'No Data To Send To SAP'

############# Enhancements for Investment Proofs ###################
@zen_proposal.route('/investmentproof/', methods = ['GET', 'POST'])
@login_required
def investmentproof():
    user_details = base()
    # sap_login=get_sap_user_credentials()
    stime_sap=datetime.datetime.now()
    conn = Connection(user='nikhil.m', passwd='sap@123', ashost='192.168.18.16',sysnr='D01', gwserv='3301', client='150')
    print conn
    data1={'SIGN':'I','OPTION':'EQ','LOW':'00001937'}
    result = conn.call('Z_HR_READ_TAX_DECLARATION',IT_EMPLOYEES=[data1])
    conn.close()
    logging.info(result)
    return render_template('investmentproofs.html', user_details=user_details)



@zen_proposal.route('/investmentproposal', methods = ['GET', 'POST'])
@login_required
def investmentproposal():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
    plantid=plant_pid[0]['plant_id1']
    plantname=plant_pid[0]['plant_desc']
#     emp_exist=find_and_filter('Investementproposal',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
#     if emp_exist == [] :
#         emp='NF'
#     else:
#         emp='F'
#     print"emmmm:",emp
    create_new=datetime.datetime.today()
    # print "datetime",create_new.strftime("%d%m%Y")
    currentYear=datetime.datetime.today()
    yr=currentYear.year
    # print "year",str(yr)
    if request.method == 'POST':
            form=request.form
            emp_name=get_employee_name(uid)
            emp_exist=find_and_filter('Investementproposal',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
            # print emp_exist
#             print "employee_year",emp_exist[0]['year_of_proposal']
            if emp_exist == [] :
                print "hello"            
                a=[]
                b=[]
                c=[]
                d=[]
                e=[]
                f=[]
                g=[]
                if form['rad'] == "Yes":
                    # print "R1 Empty"
                    a.append({"accomidationByComp":form['rad'],"emp_to_comp_per_month":form['R1'],"emp_to_comp_No_of_month":form['R2'],"emp_to_comp_Tot_amount":form['R3'],"comp_to_emp_per_month":form['R4'],"comp_to_emp_No_of_month":form['R5'],"comp_to_emp_Tot_amount":form['R6']});
                else:
                    a.append({"accomidationByComp":form['rad'],"rent_paid_own_per_month":form['R7'],"ownner_pan":form['R8'],"owner_add":form['R9']});
                if form['rad1'] == 'Yes':
                    b.append({"selfoccupied":form['rad1'],"loan_on_self":form['A1'],"loan_on_outlet":form['A2'],"rental_income":form['A3'],"municipal_tax":form['A4'],"date_of_occupation":form['A5'],"house_add":form['A6']});
                else:
                    b.append({"selfoccupied":form['rad1']})   
                c.append({"medical_insurence":form['C1'],"medical_insu_for_parents":form['C2'],"section80DD":form['C3'],"section80DDB":form['C4'],"section80E":form['C5'],"section80U":form['C6']})
                d.append({"contribution_to_pension":form['CC1'],"life_insurance_premium":form['CC2'],"public_provident_fund":form['CC3'],"deposit_in_national_saving":form['CC4'],"interest_on_NSC":form['CC5'],"ulip":form['CC6'],"principal_loan":form['CC7'],"mutual_funds":form['CC8'],"elss":form['CC9'],"children_education_exp":form['CC10'],"fixed_deposits":form['CC11'],"notified_infrastructure_bonds":form['CC12'],"deferred_annuity":form['CC13'],"Others":form['CC14'],"total":form['CC15']})
                e.append({"gross_salary":form['P1'],"pre_PF":form['P2'],"pre_PT":form['P3'],"Pre_savings":form['P4']})
                f.append({"no_of_childerns":form['NC1'],"sch_going_child":form['NC2'],"stay_at_hostal":form['NC3'],"not_stay_in_hostal":form['NC4']})
                if form['rad2'] == 'Yes':
                    g.append({"form_no_12":form['rad2'],"accomidation":form['F1'],"cars_others":form['F2'],"sweeper":form['F3'],"Gas":form['F4'],"interest_free":form['F5'],"holiday_expenses":form['F6'],"free":form['F7'],"free_meals":form['F8'],"free_education":form['F9'],"gifts":form['F10'],"creditcard_exp":form['F11'],"club_exp":form['F12'],"use_of_assets":form['F13'],"transfer_of_assets":form['F14'],"values_of_others":form['F15'],"stock_options":form['F16'],"other_benefits":form['F17'],"house_loan":form['F18'],'vehicle_loan':form['F19'],"total_perquisites":form['F20'],"total_profits":form['F21']})
                else:
                    g.append({"form_no_12":form['rad2']})
                
                array={"employee_id":uid,"rent_details":a,"section24_details":b,"section80EE_details":form['E1'],"chapterVIA":c,"section80CC":d,"section80CCG":form['GSS1'],"pre_emp_sal_details":e,"children_details":f,"perquisites":g,"year_of_proposal":str(yr),"entry_date":create_new.strftime("%d/%m/%Y"),"flag":"1","employee_name":emp_name,"plant_name":plantname};
                save_collection('Investementproposal',array) 
                flash("Your Record saved successfully",'alert-success')
                return redirect(url_for('zenproposal.investmentproposal')) 
            else:
                print "employee_year",emp_exist[0]['year_of_proposal'] 
                yy=str(yr)
                if yy == emp_exist[0]['year_of_proposal'] :
                    flash("your record already entered",'alert-success')
                else:
                    a=[]
                    b=[]
                    c=[]
                    d=[]
                    e=[]
                    f=[]
                    g=[]
                    if form['rad'] == "Yes":
                        # print "R1 Empty"
                        a.append({"accomidationByComp":form['rad'],"emp_to_comp_per_month":form['R1'],"emp_to_comp_No_of_month":form['R2'],"emp_to_comp_Tot_amount":form['R3'],"comp_to_emp_per_month":form['R4'],"comp_to_emp_No_of_month":form['R5'],"comp_to_emp_Tot_amount":form['R6']});
                    else:
                        a.append({"accomidationByComp":form['rad'],"rent_paid_own_per_month":form['R7'],"ownner_pan":form['R8'],"owner_add":form['R9']});
                    if form['rad1'] == 'Yes':
                        b.append({"selfoccupied":form['rad1'],"loan_on_self":form['A1'],"loan_on_outlet":form['A2'],"rental_income":form['A3'],"municipal_tax":form['A4'],"date_of_occupation":form['A5'],"house_add":form['A6']});
                    else:
                        b.append({"selfoccupied":form['rad1']})   
                    c.append({"medical_insurence":form['C1'],"medical_insu_for_parents":form['C2'],"section80DD":form['C3'],"section80DDB":form['C4'],"section80E":form['C5'],"section80U":form['C6']})
                    d.append({"contribution_to_pension":form['CC1'],"life_insurance_premium":form['CC2'],"public_provident_fund":form['CC3'],"deposit_in_national_saving":form['CC4'],"interest_on_NSC":form['CC5'],"ulip":form['CC6'],"principal_loan":form['CC7'],"mutual_funds":form['CC8'],"elss":form['CC9'],"children_education_exp":form['CC10'],"fixed_deposits":form['CC11'],"notified_infrastructure_bonds":form['CC12'],"deferred_annuity":form['CC13'],"Others":form['CC14'],"total":form['CC15']})
                    e.append({"gross_salary":form['P1'],"pre_PF":form['P2'],"pre_PT":form['P3'],"Pre_savings":form['P4']})
                    f.append({"no_of_childerns":form['NC1'],"sch_going_child":form['NC2'],"stay_at_hostal":form['NC3'],"not_stay_in_hostal":form['NC4']})
                    if form['rad2'] == 'Yes':
                        g.append({"form_no_12":form['rad2'],"accomidation":form['F1'],"cars_others":form['F2'],"sweeper":form['F3'],"Gas":form['F4'],"interest_free":form['F5'],"holiday_expenses":form['F6'],"free":form['F7'],"free_meals":form['F8'],"free_education":form['F9'],"gifts":form['F10'],"creditcard_exp":form['F11'],"club_exp":form['F12'],"use_of_assets":form['F13'],"transfer_of_assets":form['F14'],"values_of_others":form['F15'],"stock_options":form['F16'],"other_benefits":form['F17'],"house_loan":form['F18'],'vehicle_loan':form['F19'],"total_perquisites":form['F20'],"total_profits":form['F21']})
                    else:
                        g.append({"form_no_12":form['rad2']})
                    
                    array={"employee_id":uid,"rent_details":a,"section24_details":b,"section80EE_details":form['E1'],"chapterVIA":c,"section80CC":d,"section80CCG":form['GSS1'],"pre_emp_sal_details":e,"children_details":f,"perquisites":g,"year_of_proposal":str(yr),"entry_date":create_new.strftime("%d/%m/%Y"),"flag":"1","employee_name":emp_name,"plant_name":plantname};
                    save_collection('Investementproposal',array) 
                    flash("Your Record saved successfully",'alert-success')
                    return redirect(url_for('zenproposal.investmentproposal')) 
    return render_template('INVESTMENTPROPOSAL_ENTRY_FORM.html', user_details=user_details,year=currentYear.year,date=currentYear)

# @zen_proposal.route('/investmentproposallistview', methods = ['GET', 'POST'])
# @login_required
# def investmentproposallistview():
#     user_details = base()
#     db=dbconnect()
#     uid = current_user.username
#     list_data=find_all_in_collection('Investementproposal')
# #     print "resutls",list
#     if request.method == 'POST':
#             form=request.form
#             # print "Id",form['prid']
#             logging.info(form['prid'])
#             currentYear=datetime.datetime.today()
#             yr=currentYear.year
#             create_new=datetime.datetime.today()
#             # print "datetime",create_new.strftime("%d%m%Y")  
#             emp_data=db.Investementproposal.aggregate([{'$match':{'_id':ObjectId(form['prid'])}},{'$unwind':'$children_details'},{'$unwind':'$rent_details'},{'$unwind':'$section24_details'},{'$unwind':'$chapterVIA'},{'$unwind':'$section80CC'},{'$unwind':'$pre_emp_sal_details'},{'$unwind':'$perquisites'}])
#             emp_data=list(emp_data)
#             # print "data:",emp_data['result']
#             # logging.info(emp_data)
#             return render_template('INVESTMENT_EMPLOYEE_DETAILS.html', user_details=user_details,emp_data=emp_data,year=currentYear.year,date=currentYear)
            
#     return render_template('INVESTMENTPROPOSAL_LIST_VIEW.html', user_details=user_details,list=list_data)

@zen_proposal.route('/investmentproposalemployeeview', methods = ['GET', 'POST'])
@login_required
def investmentproposalemployeeview():
    user_details = base()
    db=dbconnect()

    uid = current_user.username
    list_data=find_and_filter('Investementproposal',{"employee_id":uid},{"flag":0})
            
    return render_template('INVESTMENTPROPOSAL_EMP_VIEW.html', user_details=user_details,list=list_data)


#used to create actual proposals
@zen_proposal.route('/actualinvestmentproposalemployeeview', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposalemployeeview():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    list_data=find_and_filter('InvestmentActuals',{"employee_id":uid},{"flag":0})
            
    return render_template('INVESTMENT_ACTUAL_PROPOSALS_EMP_VIEW.html', user_details=user_details,list=list_data)


@zen_proposal.route('/static/pdf/gridfs/<filename>')
#,year_of_proposal')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
        #year_of_proposal=str(year_of_proposal))
    
    return Response(thing, mimetype='application/pdf')

#saving pdf in databse
def file_save_gridfs(file_data,ctype,ctag,input_file_name,year_of_proposal):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username #current employee id
    file_ext = file_data.filename.rsplit('.', 1)[1]
    #today = datetime.datetime.now()
    #up_date =today.strftime('%d/%m/%Y')
    fs.put(file_data.read(), content_type=ctype, filename = str(input_file_name)+'.'+file_ext, uid = uid,
        tag = ctag,year_of_proposal= str(year_of_proposal))
from werkzeug import secure_filename

@zen_proposal.route('/investmentproofs_approval/', methods = ['GET', 'POST'])
@login_required
def investmentproofs_approval():
    user_details = base()
    uid = current_user.username
    db=dbconnect()
    plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
    plantid=plant_pid[0]['plant_id1']
    plantname=plant_pid[0]['plant_desc']
    create_new=datetime.datetime.today()
    currentYear=datetime.datetime.today()
    yr=currentYear.year    
    return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM_APPROVAL_VIEW.html', user_details=user_details,year=currentYear.year,date=currentYear)



@zen_proposal.route('/actualinvestmentproposal_data', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposal_data():    
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    # plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
    # plantid=plant_pid[0]['plant_id1']
    # plantname=plant_pid[0]['plant_desc']
    sap_login=get_sap_user_credentials()
    conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
    # conn = Connection(user='NWGW060', passwd='sap123', ashost='192.168.18.15',sysnr='Q01', gwserv='3300', client='900')

    data1={'SIGN':'I','OPTION':'BT','LOW':'01001','HIGH':'02900'}
    result = conn.call('Z_HR_READ_TAX_DECLARATION',IT_EMPLOYEES=[data1])
    conn.close()
    close_sap_user_credentials(sap_login['_id'])
    TD_HRA_RESULT=result['CH_HRA_RENTS_TABLE']
    TD_RESULT=result['CH_TAX_DATA_TABLE'][0]    
    for i in range(len(result['CH_TAX_DATA_TABLE'])):
        TD_FINAL={}
        # print result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID']
        for result_temp in result['CH_TAX_DATA_TABLE'][i].keys():
            TD_FINAL.update({result_temp:str(result['CH_TAX_DATA_TABLE'][i][result_temp])})
        result['CH_TAX_DATA_TABLE'][0].update({'HRA_DETAILS':TD_HRA_RESULT})
        for line_temp in TD_HRA_RESULT:
            # TD_FINAL.update({'FY_YEAR':'2016-2017','I581_HRA_EXMPT_X':line_temp['I581_HRA_EXMPT_X'],'I581_ACCOM_TYPE':line_temp['I581_ACCOM_TYPE'],'I581_METRO':line_temp['I581_METRO']})
            if line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='APR':
                TD_FINAL.update({'FY_YEAR':'2016-2017','I581_HRA_EXMPT_X':line_temp['I581_HRA_EXMPT_X'],'I581_ACCOM_TYPE':line_temp['I581_ACCOM_TYPE'],'I581_METRO':line_temp['I581_METRO']})
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='MAY':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='JUN':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='JUL':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='AUG':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='SEP':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='OCT':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='NOV':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='DEC':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='JAN':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='FEB':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
            elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='MAR':
                TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
        
        save_collection('Investment_Details',TD_FINAL)
        # return 'sucessfully updated data from SAP to MyPortal'


#### commented on 17th April 2017
# @zen_proposal.route('/actualinvestmentproposal', methods = ['GET', 'POST'])
# @login_required
# def actualinvestmentproposal():
    
#     user_details = base()
#     db=dbconnect()
#     uid = current_user.username
#     plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
#     plantid=plant_pid[0]['plant_id1']
#     plantname=plant_pid[0]['plant_desc']
#     # conn = Connection(user='NWGW060', passwd='sap123', ashost='192.168.18.15',sysnr='Q01', gwserv='3300', client='900')

#     # data1={'SIGN':'I','OPTION':'BT','LOW':'00001','HIGH':'01893'}
#     # result = conn.call('Z_HR_READ_TAX_DECLARATION',IT_EMPLOYEES=[data1])
#     # TD_HRA_RESULT=result['CH_HRA_RENTS_TABLE']
#     # print '--------------------------------'
#     # TD_RESULT=result['CH_TAX_DATA_TABLE'][0]    
#     # for i in range(len(result['CH_TAX_DATA_TABLE'])):
#     #     TD_FINAL={}
#     #     # print result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID']
#     #     for result_temp in result['CH_TAX_DATA_TABLE'][i].keys():
#     #         TD_FINAL.update({result_temp:str(result['CH_TAX_DATA_TABLE'][i][result_temp])})
#     #     # result['CH_TAX_DATA_TABLE'][0].update({'HRA_DETAILS':TD_HRA_RESULT})
#     #     for line_temp in TD_HRA_RESULT:
#     #         # print line_temp
#     #         TD_FINAL.update({'FY_YEAR':'2016-2017','I581_HRA_EXMPT_X':line_temp['I581_HRA_EXMPT_X'],'I581_ACCOM_TYPE':line_temp['I581_ACCOM_TYPE'],'I581_METRO':line_temp['I581_METRO']})
#     #         if line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='APR':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='MAY':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='JUN':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='JUL':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='AUG':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='SEP':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='OCT':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='NOV':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='DEC':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='JAN':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='FEB':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})
#     #         elif line_temp['EMPLOYEE_ID']==result['CH_TAX_DATA_TABLE'][i]['EMPLOYEE_ID'] and line_temp['I581_RENT_MONTH_NAME']=='MAR':
#     #             TD_FINAL.update({'I581_'+line_temp['I581_RENT_MONTH_NAME']+'_RENT':str(line_temp['I581_RENT'])})



                
#         # save_collection('Investment_Details',TD_FINAL)
# #     emp_exist=find_and_filter('Investementproposal',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
# #     if emp_exist == [] :
# #         emp='NF'
# #     else:
# #         emp='F'
# #     print"emmmm:",emp
#     NEW_HRA_RESULT={}
#     TD_RESULT=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':'000'+user_details['username'],'FY_YEAR':'2016-2017'})
#     create_new=datetime.datetime.today()
#     currentYear=datetime.datetime.today()
#     doj_flag=datetime.datetime.strptime(user_details['doj'],'%d-%m-%Y')


#     if doj_flag>datetime.datetime(2016,4,1):
#     	doj_flag='1'
#     else:
#     	doj_flag='0'
    
#     yr=currentYear.year
#     if request.method == 'POST':
#     	os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details')
#     	directories=os.listdir(os.getcwd())
#     	if user_details['username'] not in directories:
#     		os.mkdir(user_details['username'])
#     		base_path='/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']
#     		os.chdir(base_path)
#     	else:
#     		base_path='/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']
#     		os.chdir(base_path)
#     		pass
    	
#         form=request.form        
#         files=request.files
#         for f in files:
#             if f in os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']):
#                 os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']+'/'+f)
#                 temp_file=request.files.getlist(f)
#                 for shrt_file in range(len(temp_file)):
#                     if temp_file[shrt_file]:
#                         filename = secure_filename(temp_file[shrt_file].filename)
#                         temp_file[shrt_file].save(filename)
#                         os.rename(filename,f+'_'+str(shrt_file)+'.pdf')
#                 else:
#                     pass
#             elif f not in os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']):
#                 os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username'])
#                 os.mkdir(f)
#                 os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']+'/'+f)
#                 temp_file=request.files.getlist(f)
#                 for shrt_file in range(len(temp_file)):
#                     if temp_file[shrt_file]:
#                         filename = secure_filename(temp_file[shrt_file].filename)
#                         temp_file[shrt_file].save(filename)
#                         os.rename(filename,f+'_'+str(shrt_file)+'.pdf')

#         filed_names=form.keys()
#         TD_DETAILS={'Name':user_details['f_name']+' '+user_details['l_name'],'FY_YEAR':'2016-2017','Payroll_area':user_details['plant_id']}
#         for field in filed_names:
#             TD_DETAILS.update({field:form[field]})
#         if 'save_details' in form:
#             update_status='Saved'
#             TD_DETAILS.update({'SAVE_STATUS':'SAVED','LAST_SAVED':datetime.datetime.now()})            
#         else:
#             update_status='Submited'
#             TD_DETAILS.update({'SUBMIT_STATUS':'SUBMITED','SUBMIT_TIME':datetime.datetime.now()}) 
#         update_collection('Investment_Details',{'EMPLOYEE_ID':'000'+user_details['username'],'FY_YEAR':'2016-2017'},{'$set':TD_DETAILS})
#         flash("Your Investment Proofs Have Been "+update_status+" successfully.",'alert-success')
#         return redirect(url_for('zenproposal.actualinvestmentproposal')) 
#             # files=request.files['I586_TUTE_FEE_2_AMT']
#             # file_save_gridfs(files,"application/pdf","doc",'aswinikumar','year')
# #             # for i in files:
# #             # 	logging.info(i)
# #             # 	logging.info(type(i))
# #             # 	i.save(filename+str(i)+'- test')
# #             # for i in files:
# #             #     filename = secure_filename(i.filename)
# #             #     i.save(filename+str(i)+'- test')
# #             emp_name=get_employee_name(uid)
# #             emp_exist=find_and_filter('InvestmentActuals',{"employee_id":uid},{"year_of_proposal":1,"employee_id":1,"_id":0})
            
#     return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM.html', doj_flag=doj_flag,user_details=user_details,year=currentYear.year,date=currentYear,TD_RESULT=TD_RESULT)
                            

@zen_proposal.route('/actualinvestmentproposal', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposal():
    investment_details_flag=find_one_in_collection('portal_settings',{'config':'investment_proposal_edit'})
    debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
    if debug_flag and debug_flag['debug']=='on':
        debug_flag=debug_flag['debug_status']['apply_leave']
    else:
        debug_flag="off"

    date_till_editable=investment_details_flag['duration']
    actuals_submit_start_date=investment_details_flag['actuals_submit_start_date']
    actuals_submit_end_date=investment_details_flag['actuals_submit_end_date']
    approval_start_date=investment_details_flag['approval_start_date']
    approval_end_date=investment_details_flag['approval_end_date']

    user_details = base()
    db=dbconnect()
    uid = current_user.username
    print user_details
    plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
    plantid=plant_pid[0]['plant_id1']
    plantname=plant_pid[0]['plant_desc']
    NEW_HRA_RESULT={}
    TD_RESULT=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':'000'+user_details['username'],'FY_YEAR':'2017-2018'})
    TD_DETAILS={'Name':user_details['f_name']+' '+user_details['l_name'],'FY_YEAR':'2017-2018'}
    if not TD_RESULT:
        TD_RESULT=find_one_in_collection('Investment_Details',{'EMPLOYEE_ID':'000'+user_details['username'],'FY_YEAR':'2016-2017'})
        TD_DETAILS.update({'Assigned_time':datetime.datetime.now()})
        if not TD_RESULT:
            TD_RESULT={}
    if 'SUBMIT_STATUS' in TD_RESULT:
        submit_status='submited'
    else:
        submit_status=''

    # Assigned date manually such that the scenarios will be tested correctly.
    curr_def_date=datetime.datetime.now()
    yr=curr_def_date.year
    doj_flag=datetime.datetime.strptime(user_details['doj'],'%d-%m-%Y')

    if doj_flag>datetime.datetime((yr-1),4,1):
        doj_flag='1'
    else:
        doj_flag='0'
    # total_directories=os.listdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+uid)
            # print len(total_directories),'   ',form['user_id']
    doc_details={}
    # for ii in total_directories:
    #     doc_details[ii]={}
    #     total_sub_directories=os.listdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+uid+'/'+ii)
    #     subdetails=[]
    #     if len(total_sub_directories)!=0:
    #         total_files=os.listdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+uid+'/'+ii)
    #         for jj in total_files:
    #             subdetails.append(jj)
    #     doc_details.update({ii:subdetails})
    curr_yr=curr_def_date.year
    print curr_yr,'testttt'
    curr_mnth=str(curr_def_date.month)
    curr_day=str(curr_def_date.day)
    # print approval_end_date
    # print curr_def_date.date()<datetime.datetime.strptime(approval_end_date,'%Y-%m-%d').date()
    # print curr_def_date.date()>=datetime.datetime.strptime(approval_start_date,'%Y-%m-%d').date()
    if 'SUBMIT_STATUS_'+str(curr_mnth) in TD_RESULT:
        month_submit_status='submitted'
    else:
        month_submit_status=''
    if curr_def_date.date()<datetime.datetime.strptime(approval_end_date,'%Y-%m-%d').date() and curr_def_date.date()>=datetime.datetime.strptime(approval_start_date,'%Y-%m-%d').date():
        approvals_status='enabled'
    else:
        approvals_status='disabled'

    if curr_def_date.date()>=datetime.datetime.strptime(actuals_submit_start_date,'%Y-%m-%d').date() and curr_def_date.date()<=datetime.datetime.strptime(actuals_submit_end_date,'%Y-%m-%d').date():
        actuals_status='enabled' # will not be able to submit after that date.
    else:
        actuals_status='disabled'
    if curr_mnth in ['4','5','6','7','8','9','10','11'] and int(curr_day)<=int(date_till_editable) and actuals_status=='disabled' and approvals_status=='disabled':
        proposals_view='proposal_enabled'
    elif curr_mnth in ['4','5','6','7','8','9'] and int(curr_day)>int(date_till_editable) and actuals_status=='disabled' and approvals_status=='disabled': 
        proposals_view='proposal_disabled'
    elif curr_mnth not in ['4','5','6','7','8','9'] and int(curr_day)<=int(date_till_editable): 
        proposals_view='actuals_enabled'
    elif curr_mnth not in ['4','5','6','7','8','9'] and int(curr_day)>int(date_till_editable): 
        proposals_view='actuals_disabled'
    if curr_mnth in ['4','5','6','7','8','9','10','11','12']:
        fin_year_start=curr_yr
        fin_year_end=curr_yr+1
    elif curr_mnth in ['1','2','3']:
        fin_year_start=curr_yr-1
        fin_year_end=curr_yr
    if request.method == 'POST':
        os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details')
        directories=os.listdir(os.getcwd())
        if user_details['username'] not in directories:
            os.mkdir(user_details['username'])
            base_path='/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']
            os.chdir(base_path)
        else:
            base_path='/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']
            os.chdir(base_path)
            pass
        
        form=request.form        
        files=request.files
        # for f in files:
        #     if f in os.listdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+user_details['username']):
        #         os.chdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+user_details['username']+'/'+f)
        #         temp_file=request.files.getlist(f)
        #         for shrt_file in range(len(temp_file)):
        #             if temp_file[shrt_file]:
        #                 filename = secure_filename(temp_file[shrt_file].filename)
        #                 temp_file[shrt_file].save(filename)
        #                 os.rename(filename,f+'_'+str(shrt_file)+'.pdf')
        #         else:
        #             pass
        #     elif f not in os.listdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+user_details['username']):
        #         os.chdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+user_details['username'])
        #         os.mkdir(f)
        #         os.chdir('/home/administrator/Aswinikumar/PCIL_myPortal/zenapp/static/Investment_details/'+user_details['username']+'/'+f)
        #         temp_file=request.files.getlist(f)
        #         for shrt_file in range(len(temp_file)):
        #             if temp_file[shrt_file]:
        #                 filename = secure_filename(temp_file[shrt_file].filename)
        #                 temp_file[shrt_file].save(filename)
        #                 os.rename(filename,f+'_'+str(shrt_file)+'.pdf')
        for f in files:
            if f in os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']):
                os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']+'/'+f)
                temp_file=request.files.getlist(f)
                for shrt_file in range(len(temp_file)):
                    if temp_file[shrt_file]:
                        filename = secure_filename(temp_file[shrt_file].filename)
                        temp_file[shrt_file].save(filename)
                        os.rename(filename,f+'_'+str(shrt_file)+'.pdf')
                else:
                    pass
            elif f not in os.listdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']):
                os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username'])
                os.mkdir(f)
                os.chdir('/home/pavan/Penna-Npx/zenapp/static/Investment_details/'+user_details['username']+'/'+f)
                temp_file=request.files.getlist(f)
                for shrt_file in range(len(temp_file)):
                    if temp_file[shrt_file]:
                        filename = secure_filename(temp_file[shrt_file].filename)
                        temp_file[shrt_file].save(filename)
                        os.rename(filename,f+'_'+str(shrt_file)+'.pdf')
        filed_names=form.keys()
        
        for field in filed_names:
            TD_DETAILS.update({field:form[field]})
        # print form,' form'
        if 'details_assigned' in form:
            update_status='assigned'
            TD_DETAILS.update({'Assigned_time':datetime.datetime.now()})
        if 'save_details' in form:
            update_status='Saved'
            TD_DETAILS.update({'SAVE_STATUS_'+str(curr_mnth):'SAVED','LAST_SAVED':datetime.datetime.now()})
            print 'hello in saved'            
        else:
            update_status='Submited'
            
            TD_DETAILS.update({'SUBMIT_STATUS_'+str(curr_mnth):'SUBMITED','SUBMIT_TIME':datetime.datetime.now()}) 
        update_collection('Investment_Details',{'EMPLOYEE_ID':'000'+user_details['username'],'FY_YEAR':'2017-2018','Payroll_area':user_details['plant_id']},{'$set':TD_DETAILS})
        flash("Your Investment Proofs Have Been "+update_status+" successfully.",'alert-success')        
        return redirect(url_for('zenproposal.actualinvestmentproposal'))
    if 'Assigned_time' not in TD_RESULT:
        print 'no assigned values'
        return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM_4.html',fin_year_start=fin_year_start,fin_year_end=fin_year_end,doj_flag=doj_flag,submit_status=submit_status,proposals_view=proposals_view,approvals_status=approvals_status,actuals_status=actuals_status,doc_details=doc_details,user_details=user_details,year=curr_yr,curr_date=curr_day,curr_month=curr_mnth,date_till_editable=date_till_editable,date=curr_def_date,TD_RESULT=TD_RESULT)
    elif proposals_view=='proposal_enabled':
    	print "Prposal Edit enable Mode"
        return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM_1.html',fin_year_start=fin_year_start,fin_year_end=fin_year_end,month_submit_status=month_submit_status,doj_flag=doj_flag,submit_status=submit_status,proposals_view=proposals_view,approvals_status=approvals_status,actuals_status=actuals_status,doc_details=doc_details,user_details=user_details,year=curr_yr,curr_date=curr_day,curr_month=curr_mnth,date_till_editable=date_till_editable,date=curr_def_date,TD_RESULT=TD_RESULT)
    elif proposals_view=='proposal_disabled':
    	print "Prposal Edit disable Mode"
        return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM_2.html',fin_year_start=fin_year_start,fin_year_end=fin_year_end,month_submit_status=month_submit_status,doj_flag=doj_flag,submit_status=submit_status,proposals_view=proposals_view,approvals_status=approvals_status,actuals_status=actuals_status,doc_details=doc_details,user_details=user_details,year=curr_yr,curr_date=curr_day,curr_month=curr_mnth,date_till_editable=date_till_editable,date=curr_def_date,TD_RESULT=TD_RESULT)        
    elif proposals_view=='actuals_enabled':
        return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM_3.html',fin_year_start=fin_year_start,fin_year_end=fin_year_end,month_submit_status=month_submit_status,doj_flag=doj_flag,submit_status=submit_status,proposals_view=proposals_view,approvals_status=approvals_status,actuals_status=actuals_status,doc_details=doc_details,user_details=user_details,year=curr_yr,curr_date=curr_day,curr_month=curr_mnth,date_till_editable=date_till_editable,date=curr_def_date,TD_RESULT=TD_RESULT)
    elif proposals_view=='actuals_disabled':
        return render_template('INVESTMENTPROPOSAL_ACTUAL_ENTERY_FORM_3.html',fin_year_start=fin_year_start,fin_year_end=fin_year_end,month_submit_status=month_submit_status,doj_flag=doj_flag,submit_status=submit_status,proposals_view=proposals_view,approvals_status=approvals_status,actuals_status=actuals_status,doc_details=doc_details,user_details=user_details,year=curr_yr,curr_date=curr_day,curr_month=curr_mnth,date_till_editable=date_till_editable,date=curr_def_date,TD_RESULT=TD_RESULT)




@zen_proposal.route('/actualinvestmentproposallistview', methods = ['GET', 'POST'])
@login_required
def actualinvestmentproposallistview():
    user_details = base()
    db=dbconnect()
    uid = current_user.username
    list_data=find_all_in_collection('InvestmentActuals')
#     print "resutls",list
    if request.method == 'POST':
            form=request.form

            print "Id",form['prid']
            currentYear=datetime.datetime.today()
            yr=currentYear.year
            create_new=datetime.datetime.today()
            print "datetime",create_new.strftime("%d%m%Y")  
            emp_data=db.InvestmentActuals.aggregate([{'$match':{'_id':ObjectId(form['prid'])}},
                {'$unwind':'$children_details'},{'$unwind':'$rent_details'},{'$unwind':'$section24_details'},
                {'$unwind':'$chapterVIA'},{'$unwind':'$section80CC'},{'$unwind':'$pre_emp_sal_details'},
                {'$unwind':'$perquisites'}])
            emp_data=list(emp_data)
            try:
                print "UID",form['user_id']
                print "Year" ,form['year_of_proposal']
                user_docs=find_in_collection('fs.files',{"uid" : str(form['user_id']),"tag" : "doc",
                    "year_of_proposal":str(form['year_of_proposal'])})
                
            except:
                print "none"

            return render_template('INVESTMENT_ACTUAL_EMPLOYEE_DETAILS.html', user_details=user_details,
                emp_data=emp_data,year=currentYear.year,date=currentYear,user_documents=user_docs,
                user_id=str(form['user_id']))
            
    return render_template('INVESTMENT_PROPOSAL_ACTUAL_LIST_VIEW.html', user_details=user_details,list=list_data)

