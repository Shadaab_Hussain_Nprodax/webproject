from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
import csv
import os
import cgi
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from zenapp.modules.leave.lfunctions import *
import logging
#from pyrfc import *
from operator import itemgetter, attrgetter
import json
import random
import sys
from array import array
from zenapp.modules.resumebank import *

zen_resumebank = Blueprint('zenresumebank', __name__, url_prefix='/resumebank')

@zen_resumebank.route('/uploadprofile/', methods = ['GET', 'POST'])
@login_required
def resume_upload():
	
	user_details = base()
	dept=find_in_collection("departmentmaster",{"plant":"HY"})
	dept = sorted(dept, key=itemgetter('dept'))
	new_skill_set=list()
	if request.method=="POST":
		form=request.form
		if request.files:
			applicant_resume=request.files['resume']
			file_name=applicant_resume.filename
			file_found=find_in_collection('fs.files',{"file_name":file_name})
			if file_found:
				flash("Resume with File Name Already Exists",'alert-danger')
				return render_template('RESUME_UPLOAD.html', user_details=user_details,dept=dept)
		applicant_name=form['name']
		refer_by =form['referedby']
		qualification=form['qualification']
		experience=form['experience']
		department=form['dept']
		dept_desc=find_one_in_collection("departmentmaster",{"plant":"HY",'dept_pid':department})
		location=form['location']
		district = form['district']
		village = form['village']
		created_date=datetime.datetime.now()
		count=find_in_collection_count('ResumeBank',{})
		count=count+1
		Application_ID=str(created_date.year)+str(count)
		array={"village":village.lower(),"district":district.lower() ,"application_ID":Application_ID,"applicant_name":applicant_name.lower(),
		"experience":experience,"department_id":department,"created_date":created_date,
		"created_by":current_user.username,'location':location.lower(),'department_desc':dept_desc['req_desc'],'reference_by':refer_by.lower(),"qualification":qualification.lower()}
		save_collection('ResumeBank',array)
		if request.files:
			applicant_image=request.files['image_to_upload']
			applicant_resume=request.files['resume']
			file_name=applicant_resume.filename
			tag_name="applicant_resume"
			upload_date= datetime.datetime.now()
			user_id = current_user.username #current employee id
			if applicant_resume:
				file_save_gridfs(applicant_resume,"application/pdf/doc/docx",tag_name,upload_date,user_id,Application_ID,department,file_name)
			image_tag="applicant_image"
			image_name=applicant_image.filename
			if applicant_image:
				file_save_gridfs(applicant_image,"image/jpeg/png",image_tag,upload_date,user_id,Application_ID,department,image_name)
			
			flash("Sucessfully Uploaded the User Profile His Reference_ID IS:"+ "  "+Application_ID,'alert-success')
		
	
	return render_template('RESUME_UPLOAD.html', user_details=user_details,dept=dept)

@zen_resumebank.route('/search/', methods = ['GET', 'POST'])
@login_required
def resume_search():
	user_details = base()
	dept=find_in_collection("departmentmaster",{"plant":"HY"})
	dept = sorted(dept, key=itemgetter('dept'))
	array={}
	array1={}
	date_search={}
	app_name={}
	new_skill_set=list()
	if request.method=="POST":
		form=request.form
		if form['submit']=="View":
			department = form['dept']		
			name=form['name']
			experience=form['experience']
			start_date=form['start_date']
			end_date=form['end_date']
			qualification=form['qualification']
			village=form['village']
			district=form['district']
			referby =form['referedby']
			location=form['location']
			if village:
				array['village']=village.lower()
			if district:
				array['district']=district.lower()
			if qualification:
				array['qualification']=qualification.lower()
			if referby:
				array['reference_by']=referby.lower()
			if location:
				array['location']=location.lower()
			if department:
				array['department_id']=str(department)
			if name:
				apclnt_name=str(name).lower()
				app_name['$regex']=apclnt_name
				array['applicant_name']=app_name
			
			if start_date:
				from_date = datetime.datetime.strptime(start_date,"%d/%m/%Y")
				date_search['$gte']=from_date
			if end_date:
				to_date = datetime.datetime.strptime(end_date,"%d/%m/%Y")
				date_search['$lt']=to_date+datetime.timedelta(days=1)

			if start_date or end_date :
				array['created_date']=date_search
			if experience:
				array['experience']=str(experience)
			
			if len(array)==0:
				flash("Please Select at Least One search Criteria",'alert-danger')
				return render_template('RESUME_SEARCH.html', user_details=user_details,dept=dept)
			else:
				found_docs=find_in_collection('ResumeBank',array)
				if len(found_docs)==0:
					flash("No Data Availabe for the Current Selection",'alert-danger')
					return render_template('RESUME_SEARCH.html', user_details=user_details,dept=dept)

				
			a=[]
			if found_docs:
				for e in found_docs:
					e['created_date']= datetime.datetime.strftime(e['created_date'],"%d/%m/%Y")
					a=a+[e]

			user_profile=find_in_collection('fs.files',{"tag_name":"applicant_resume"})
			user_image = find_in_collection('fs.files',{"tag_name":"applicant_image"})
			return render_template('RESUME_SEARCH.html', user_details=user_details,dept=dept,
			documents=a,user_profile=user_profile,user_image=user_image)
		
	return render_template('RESUME_SEARCH.html', user_details=user_details,dept=dept)

def file_save_gridfs(file_data,ctype,tag_name,upload_date,user_id,Application_ID,department,file_name):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    fs.put(file_data.read(),content_type=ctype,tag_name=tag_name,upload_date=upload_date,uploded_by=user_id,
    	application_ID=Application_ID,department_id=str(department),file_name=file_name)


@zen_resumebank.route('/static/pdf/gridfs/<filename>')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(file_name=filename)
    return Response(thing, mimetype='application/pdf/doc/docx')

@zen_resumebank.route('/static/img/gridfs/<filename>')
@login_required
def gridfs_img(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(file_name=filename)
    return Response(thing, mimetype='image/jpeg')

@zen_resumebank.route('/shortlist/', methods = ['GET', 'POST'])
@login_required
def resume_shortlist():
	user_details = base()
	uid = current_user.username
	userinfo=find_one_in_collection("Userinfo",{"username":uid})
	dept_id=userinfo['department_id']
	all_records= find_in_collection("ShortListed",{'department_id':dept_id})
	
	if len(all_records)==0:
		all_records= find_in_collection("ShortListed",{'hr_dept_id':dept_id})
		
	
	user_profile=find_in_collection('fs.files',{"tag_name":"applicant_resume"})
	if request.method=="POST":
		form = request.form
		update_coll('ShortListed',{'application_ID':form['application_id']},{"$set":{'approved_status':form['submit']}})
	return render_template('RESUME_SHORT_LIST.html',user_details=user_details,documents=all_records,user_profile=user_profile)