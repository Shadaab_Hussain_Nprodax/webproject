from wtforms import Form, TextField, validators, SelectField, StringField, PasswordField, DateField, FileField, SelectMultipleField,widgets
from zenapp.dbfunctions import *

class back_up(Form):
	host = StringField('Host:')
	username = StringField('Username:')
	foldername = StringField('Foldername:')

class restoredb(Form):
	host_r = StringField('Host:')
	username_r = StringField('Username:')
	foldername_r = StringField('Foldername:')