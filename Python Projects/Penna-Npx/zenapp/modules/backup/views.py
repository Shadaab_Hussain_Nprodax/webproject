from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.backup.form import *
from zenapp.dbfunctions import *
import uuid
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import os
from calendar import monthrange
from datetime import datetime as dt
from zenapp.modules.leave.lfunctions import *
import pysftp
import paramiko
import os,sys
import datetime

import os
import pysftp
import paramiko
import os,sys
import datetime
import smtplib
import email.utils
from email.mime.text import MIMEText
from calendar import monthrange
from datetime import datetime as dt


zen_backup = Blueprint('zenbackup', __name__, url_prefix='/backup')


@zen_backup.route('/backup/', methods=['GET', 'POST'])
@login_required
def backup():
	p =check_user_role_permissions('55c9d3f512ebc4ff41fa157d')
	user_details = base()
	form = back_up(request.values)
	# form_restore = restoredb(request.values)
	
	if p:
		if request.method == 'POST':
			form1 = request.form
			if form1['database'] == "backup":
				host_name = str(form.host.data)
				u_name = str(form.username.data)
				folder_name = str(form.foldername.data)
				print "host backup:"+host_name
				backup = backup_form(host_name,u_name,"penna@1234$",folder_name)
			elif form1['database'] == "restore":
				host_name_r = str(form.host.data)
				print "host_name_r :"+host_name_r
				u_name_r = str(form.username.data)
				folder_name_r = str(form.foldername.data)
				restore = restore_db(host_name_r,u_name_r,folder_name_r,"penna@1234$")
			return redirect(url_for('zenbackup.backup'))
		return render_template('backup.html', form=form,user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))

def backup_form(host,uname,passw,data_backup):
	time_start =datetime.datetime.now()
	try:
		os.mkdir(data_backup)
	except:
		pass
	fol_name = str(datetime.datetime.now().ctime())
	time_now = datetime.datetime.now()
	client = paramiko.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	try:
		client.connect(host, username=uname, password=passw)
		print "paramiko connected successfully"
		sftp = pysftp.Connection(host, username=uname, password=passw)
		print "sftp connected successfully"
		stdin, stdout, stderr = client.exec_command('mongodump')
		print "dumped sucessfully......."
		sftp.rename('dump',fol_name)
		sftp.get_r(fol_name,data_backup)
		time_end =datetime.datetime.now()
		try:
			time_span = time_end-time_start
			time_span =divmod(time_span.days * 86400 + time_span.seconds, 60)
			message = "successfully backed up data in '"+data_backup+"'as :"+ fol_name +" in "+str(time_span[0])+" minutes : "+str(time_span[1])+" seconds"
			flash(message,'alert-success')
		except:
			message = "successfully backed up data in '"+data_backup+"'as :"+ fol_name
			flash(message,'alert-success')
	except :
		message = "connection error! check server credentials."
		flash(message,'alert-success')	


def restore_db(host_r,uname_r,fol_name_local,pass_r):
	fol_name =str(fol_name_local)
	try:
		client = paramiko.SSHClient()
		client.load_system_host_keys()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		client.connect(host_r,username=uname_r,password=pass_r)
		print "paramiko connection successful"
		sftp = pysftp.Connection(host_r, username=uname_r, password=pass_r)
		print "pysftp connection successful"
		try:
			sftp.mkdir(fol_name)
			print "directory is created"
		except:
			print "directory is already available"
		time_now = datetime.datetime.now()
		try:
			sftp.put_r(fol_name,fol_name)
			print "folder transfer successfull"
		except:
			print "folder transfer error"

		try:
			stdin,stdout, stderr = client.exec_command('mongorestore '+fol_name)
			print "sucessfully executed restore "
		except:
			print "unable to run the command mongorestore"

		message1 = "sucessfully restored the database"
		flash(message1,'alert-success')
	except:
		message1 = "connection error! wrong credentials give"
		flash(message1,'alert-success')


	
#target backup folder must be created defaultly
#password must be hardcoded
#in backup process: folder where the backup is taken should unique




def notification(message,receiver):
	USERNAME='myportal@pennacement.com'
	PASSWORD='Penna@123'
	msg = MIMEText('Dear '+'Admin'+',\n\n'+message+'\n\n\n\nThanks,\nTeam-Admin.')
	msg['Subject'] = 'Cronjob Status Notification'
	server = smtplib.SMTP("smtp.office365.com:587")
	server.starttls()
	server.login(USERNAME,PASSWORD)
	try:
		server.sendmail(USERNAME,receiver,str(msg))		
		server.quit()
	except Exception,R:
		print R


# @zen_backup.route('/db_backup/', methods=['GET', 'POST'])
# @login_required
# def db_backup():
# 	try:
# 		p_path=str(datetime.datetime.now().date())
# 		c_path=str(datetime.datetime.now().strftime('%d%b%Y_%H%M'))
# 		if str(p_path) in os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS'):
# 			os.chdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path))
# 			os.system('mongodump --port 14041')
# 			os.system('mv dump '+str(c_path))
# 		else:
# 			# os.system('mkdir /home/administrator/DB_BACKUPS/'+str(folder_name))
# 			os.mkdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path))
# 			os.mkdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path)+'/'+str(c_path))
# 			os.system('mongodump -o /home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path)+'/'+str(c_path)+' --port 14041')
# 		message='Backup function is executed sucessfully @ '+str(datetime.datetime.now().strftime('%d%b%Y %H:%M:%S'))
# 		notification(message,'portalsupport1@pennacement.com')
# 		notification(message,'ananthkumar.p@pennacement.com')
# 		flash(message,'alert-success')
# 	except:
# 		message='Encountered a error while performing Backup function @ '+str(datetime.datetime.now().strftime('%d%b%Y %H:%M:%S'))
# 		notification(message,'portalsupport1@pennacement.com')
# 		notification(message,'ananthkumar.p@pennacement.com')
# 		flash(message,'alert-danger')
# 	return redirect(url_for('zenuser.index'))




@zen_backup.route('/db_backup/', methods=['GET', 'POST'])
@login_required
def db_backup():
	try:
		p_path=str(datetime.datetime.now().date())
		c_path=str(datetime.datetime.now().strftime('%d%b%Y_%H%M'))
		if str(p_path) in os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS'):
			os.chdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path))
			os.system('mongodump --port 14041')
			os.system('mv dump '+str(c_path))
			path, dirs, files = os.walk('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path)+'/'+str(c_path)+'/pcil_npx_dev').next()
			message_files=len(files)/2
		else:
			# os.system('mkdir /home/administrator/DB_BACKUPS/'+str(folder_name))
			os.mkdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path))
			os.mkdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path)+'/'+str(c_path))
			os.system('mongodump -o /home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path)+'/'+str(c_path)+' --port 14041')
			path, dirs, files = os.walk('/home/administrator/PCIL_MYPORTAL_PRODUCTION/DB_BACKUPS/'+str(p_path)+'/'+str(c_path)+'/pcil_npx_dev').next()
			message_files=len(files)/2
		message='Backup function is executed sucessfully and a backup of '+str(message_files)+' collections  @ '+str(datetime.datetime.now().strftime('%d%b%Y %H:%M:%S'))
		notification(message,'portalsupport1@pennacement.com')
		notification(message,'ananthkumar.p@pennacement.com')
		flash(message,'alert-success')
	except:
		message='Encountered a error while performing Backup function @ '+str(datetime.datetime.now().strftime('%d%b%Y %H:%M:%S'))
		notification(message,'portalsupport1@pennacement.com')
		notification(message,'ananthkumar.p@pennacement.com')
		flash(message,'alert-danger')
	return redirect(url_for('zenuser.index'))