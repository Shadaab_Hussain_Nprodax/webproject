from zenapp.dbfunctions import *
from bson.objectid import ObjectId
from httplib import HTTPException
from flask_login import current_user
import smtplib
import datetime
import urllib,urllib2,urlparse
import email.utils
from email.mime.text import MIMEText
from zenapp.modules.leave.lfunctions import *
import logging
#class to create session when someone login 
class User():
	def __init__(self, userid=None, username=None ,date=None):
		self.userid = userid
		self.username = username
		self.date = date

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.userid)

	def __repr__(self):
		return '<User %r>' % self.username

def find_by_username(username):
	try:
		data = find_one_in_collection('Userinfo',{'$or':[{'username': username},{'official_email': {'$regex':username,'$options':'i'}}]})
		if data is None:
			return None
		else:
			user = User(unicode(data['_id']), data['username'] , data['dob'])
			return user
	except HTTPException:
		return None

def find_by_id(userid):
	try:
		data = find_one_in_collection('Userinfo',{'_id': ObjectId(userid)})
		if data is None:
			return None
		else:
			user = User(unicode(data['_id']), data['username'], data['dob'])
			return user

	except HTTPException:
		return None

#function to find employee's profile image
def profile_pic():
	uid = current_user.username
	userpic = find_one_in_collection('fs.files',{"uid" :uid,"tag":"profileimg"})
	if userpic:
		return '/user/static/img/gridfs/'+userpic['filename']
	else:
		return ''

#function to find employee's permanent address
def peraddress_pdf():
	uid = current_user.get_id()
	userpic = find_one_in_collection('fs.files',{"uid" :current_user.username,"tag":"peraddress_proof"})
	if userpic:
		return '/user/static/pdf/gridfs/'+userpic['filename']
	else:
		return '/user/static/pdf/gridfs/9e9b3083-b980-499d-8e73-b186a916309d.png'

#function to find employee's present address
def preaddress_pdf():
	uid = current_user.get_id()
	userpic = find_one_in_collection('fs.files',{"uid" :current_user.username,"tag":"preaddress_proof"})
	if userpic:
		return '/user/static/pdf/gridfs/'+userpic['filename']
	else:
		return '/user/static/pdf/gridfs/9e9b3083-b980-499d-8e73-b186a916309d.png'

#function to find whether an employee is having a particular permission or not 
def check_user_role_permissions(permissionid):
	logging.info(current_user.get_id())
	uid = current_user.get_id()
	pid=find_and_filter('Userinfo',{'_id': ObjectId(uid)},{"designation_id":1,"_id":0})
	db = dbconnect()
	u_roles = db.Positions.aggregate([
                {'$match' : {"position_id": pid[0]['designation_id']}}, 
                {'$project' : {"roles":1,"_id":0}}])
	u_roles=list(u_roles)
	user_roles = u_roles[0]['roles']
	count = len(user_roles)
	Role_Permissions =[]
	for k in range(0,count,1):
		P_in_Roles = db.Roles.aggregate([
                    {'$match' : {'_id' : ObjectId(user_roles[k])}},
                    {'$project' : {'permissions' : 1, '_id' : 0}}])
		P_in_Roles=list(P_in_Roles)
		print "Premissss",P_in_Roles
		Role_Permissions = Role_Permissions + P_in_Roles[0]["permissions"]
	if permissionid in Role_Permissions:
		return True
	else:
		return False 

# new enhancement for performanance improvement	@14_nov_2016
def base():
	# stime=datetime.datetime.now()
	# uid = current_user.get_id()
	# usr_details = find_one_in_collection('Userinfo',{'_id': ObjectId(uid)})
	# usr_details['profpic']=profile_pic()
	# arr={"e_id":current_user.username}
	# notify=find_in_collection('Notifications',arr)
	# usr_details['notification']=notify
	# userpermissions=find_one_in_collection('userpermissions',{"username":current_user.username})
	# logging.info('user logged in : '+str(current_user.username))
	# # print userpermissions
	# for key in userpermissions:
	# 	usr_details[key]=key
	# # logging.info('permission_assign elapsed time of user : '+current_user.username+' '+str(datetime.datetime.now()-stime))
	# deptid=get_designation_id(current_user.username)
	# p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
	# q=find_one_in_collection("Plants",{"chief_pid":deptid})
	# if p or q:
	# 	usr_details['pmprmivapproval']='ok'

	# if current_user.username!="admin":
	# 	if usr_details["plant_id"]!="HY":
	# 		usr_details['pmprmivcreation']='ok'

	# deptid=get_designation_id(current_user.username)
	# p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
	# if p:
	# 	usr_details['hodmivreport']='ok'

	# tim=find_one_in_collection("Notificationtimestamp",{"username":current_user.username})	
	# if tim:
	# 	notify_num=find_in_collection_count("Notifications",{"created_time": {"$gt":tim['created_time']},"e_id":current_user.username })
	# 	usr_details['notify_num']=notify_num
	# else:
	# 	usr_details['notify_num']=0
	# etime=datetime.datetime.now()-stime
	# # logging.info(str(etime)+' base function elapsed time acessed by user : '+current_user.username+' .')
	# # logging.info(str(etime)+'time for base function to load-------------------------------------------')
	# return usr_details


	stime=datetime.datetime.now()
	uid = current_user.get_id()
	usr_details = find_one_in_collection('Userinfo',{'_id': ObjectId(uid)})
	usr_details['profpic']=profile_pic()
	arr={"e_id":current_user.username}
	notify=find_in_collection_sort_limit('Notifications',arr,[("_id",-1)],2)
	usr_details['notification']=notify
	notify_len=find_in_collection_count('Notifications',arr)
	usr_details['notify_len']=notify_len
	userpermissions=find_one_in_collection('userpermissions',{"username":current_user.username})
	# print "userpermiss",userpermissions
	for key in userpermissions:
		if key not in ['username','designation_id']:
			usr_details[key]=key

	# permissions for DMR added on 26th june 2017
	deptid=get_designation_id(current_user.username)
	DMR_P=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid,'dept':'MINES'})
	DMR_q=find_one_in_collection("Plants",{"chief_pid":deptid})
	if DMR_q:
		usr_details['DMR_EMAILID']=DMR_q['chief_email']
		usr_details['kaizen_approval']='View' # permission for kaizen approval
	if DMR_P or DMR_q:
		usr_details['DMR_approval']='ok'

	kaizen_approver=find_one_in_collection('portal_settings',{'config':'kaizen_approvals'})
	if 'plant_id' in usr_details:
		if usr_details["plant_id"]=="HY" and current_user.username in kaizen_approver['users']:
			usr_details['kaizen_approval']='View' # permission for kaizen approval
		
	#permission for creation of DMR added on 26th june 2017
	DMR_user=find_one_in_collection('portal_settings',{"config":"DMR_users"})
	if current_user.username in DMR_user['employee_ids']:
		usr_details['DMR_creation']='Ok'

	# has to moved to production for permission of L1 and L2 CP_Kra form. 
	cp_kra_l1=find_one_in_collection('portal_settings',{"config":"cp_kra_form"})
	if current_user.username in cp_kra_l1['l1_employees']:
		usr_details['cp_kra_l1']='Ok'
	else:
		usr_details['cp_kra_l2']='Ok'
	# End Of Assigning permission of L1 and L2 CP_Kra form.
	deptid=get_designation_id(current_user.username)
	p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
	q=find_one_in_collection("Plants",{"chief_pid":deptid})
	if p or q:
		usr_details['pmprmivapproval']='ok'
	elif q:
		usr_details['plant_chief']=[{'desig_id':deptid,'plant_id':q[plant_id]}]
	if current_user.username!="admin":
		if usr_details["plant_id"]!="HY":
			usr_details['pmprmivcreation']='ok'

	deptid=get_designation_id(current_user.username)
	p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
	if p:
		usr_details['hod']=[{'hod_pid':deptid,'dept_id':p['dept_pid']}]
		usr_details['hodmivreport']='ok'

	tim=find_one_in_collection("Notificationtimestamp",{"username":current_user.username})	
	if tim:
		notify_num=find_in_collection_count("Notifications",{"created_time": {"$gt":tim['created_time']},"e_id":current_user.username })
		usr_details['notify_num']=notify_num
	else:
		usr_details['notify_num']=0
	etime=datetime.datetime.now()-stime
	return usr_details


# def base():
# 	stime=datetime.datetime.now()
# 	uid = current_user.get_id()
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"get uid"
# 	usr_details = find_one_in_collection('Userinfo',{'username': current_user.username})
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"find one userinfo"
# 	usr_details['profpic']=profile_pic()
# 	arr={"e_id":current_user.username}
# 	notify=find_in_collection('Notifications',arr)
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"notification"
# 	usr_details['notification']=notify
# 	p =check_user_role_permissions('546f2097850d2d1b00023b26')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"la"
# 	if p:
# 		usr_details['la']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'la':'546f2097850d2d1b00023b26'}})
# 	p =check_user_role_permissions('546f228c850d2d1b00023b2c')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ae"
# 	if p:
# 		usr_details['ae']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ae':'546f228c850d2d1b00023b2c'}})
# 	p =check_user_role_permissions('546f22de850d2d1b00023b32')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"pa"
# 	if p:
# 		usr_details['pa']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'pa':'546f22de850d2d1b00023b32'}})
# 	p =check_user_role_permissions('546f204b850d2d1b00023b22')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"al"
# 	if p:
# 		usr_details['al']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'al':'546f204b850d2d1b00023b22'}})
# 	p =check_user_role_permissions('547c7e0157fbb30ed690336e')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"hl"
# 	if p:
# 		usr_details['hl']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'hl':'547c7e0157fbb30ed690336e'}})
# 	p =check_user_role_permissions('5487ed0757fbb30f702709c0')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"rep"
# 	if p:
# 		usr_details['rep']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'rep':'5487ed0757fbb30f702709c0'}})
# 	p=check_user_role_permissions('546f22a8850d2d1b00023b2e')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"cr"
# 	if p:
# 		usr_details['cr']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'cr':'546f22a8850d2d1b00023b2e'}})
# 	p=check_user_role_permissions('546f2245850d2d1b00023b27')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ps"
# 	if p:
# 		usr_details['ps']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ps':'546f2245850d2d1b00023b27'}})
# 	p=check_user_role_permissions('546f2257850d2d1b00023b28')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"f16"
# 	if p:
# 		usr_details['f16']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'f16':'546f2257850d2d1b00023b28'}})
# 	p=check_user_role_permissions('546f2038850d2d1b00023b21')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ale"
# 	if p:
# 		usr_details['ale']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ale':'546f2038850d2d1b00023b21'}})
# 	p=check_user_role_permissions('546f22b9850d2d1b00023b2f')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ar"
# 	if p:
# 		usr_details['ar']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ar':'546f22b9850d2d1b00023b2f'}})
# 	p=check_user_role_permissions('54ab5b94507c00c54ba22297')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ams"
# 	if p:
# 		usr_details['ams']='ok'	
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ams':'54ab5b94507c00c54ba22297'}})	
# 	p=check_user_role_permissions('54ae3abd507c00c54ba22299')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"lt"
# 	if p:
# 		usr_details['lt']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'lt':'54ae3abd507c00c54ba22299'}})
# 	p=check_user_role_permissions('54b0fe8a507c00c54ba2229a')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"er"
# 	if p:
# 		usr_details['er']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'er':'54b0fe8a507c00c54ba2229a'}})

# 	p=check_user_role_permissions('54b378d4507c00c54ba2229b')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"eh"
# 	if p:
# 		usr_details['eh']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'eh':'54b378d4507c00c54ba2229b'}})
# 	p=check_user_role_permissions('54ab60d0507c00c54ba22298')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"sh"
# 	if p:
# 		usr_details['sh']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'sh':'54ab60d0507c00c54ba22298'}})
# 	p=check_user_role_permissions('54b9001c507c00c54ba2229d')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"trade"
# 	if p:
# 		usr_details['trade']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'trade':'54b9001c507c00c54ba2229d'}})
# 	p=check_user_role_permissions('54b9002e507c00c54ba2229e')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"nontrade"
# 	if p:
# 		usr_details['nontrade']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'nontrade':'54b9002e507c00c54ba2229e'}})
# 	p=check_user_role_permissions('54b90061507c00c54ba2229f')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ag"
# 	if p:
# 		usr_details['ag']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ag':'54b90061507c00c54ba2229f'}})
# 	p=check_user_role_permissions('54b900a2507c00c54ba222a0')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"sales"
# 	if p:
# 		usr_details['sales']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'sales':'54b900a2507c00c54ba222a0'}})
# 	p=check_user_role_permissions('54b900db507c00c54ba222a1')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"coll"
# 	if p:
# 		usr_details['coll']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'coll':'54b900db507c00c54ba222a1'}})
# 	p=check_user_role_permissions('54b900f7507c00c54ba222a2')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"po"
# 	if p:
# 		usr_details['po']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'po':'54b900f7507c00c54ba222a2'}})
# 	p=check_user_role_permissions('55c9d3c112ebc4ff41fa157c')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"us"
# 	if p:
# 		usr_details['us']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'us':'55c9d3c112ebc4ff41fa157c'}})
# 	p=check_user_role_permissions('55c9d3f512ebc4ff41fa157d')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"bk"
# 	if p:
# 		usr_details['bk']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'bk':'55c9d3f512ebc4ff41fa157d'}})
# 	p=check_user_role_permissions('54b90128507c00c54ba222a3')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ob"
# 	if p:
# 		usr_details['ob']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ob':'54b90128507c00c54ba222a3'}})
# 	p=check_user_role_permissions('550fdf44850d2d3861bd7ae4')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"prlevelConfig"
# 	if p:
# 		usr_details['prlevelConfig']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'prlevelConfig':'550fdf44850d2d3861bd7ae4'}})
# 	p=check_user_role_permissions('550fdf7a850d2d3861bd7ae5')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"mivlevelConfig"
# 	if p:
# 		usr_details['mivlevelConfig']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'mivlevelConfig':'550fdf7a850d2d3861bd7ae5'}})	



# 	p=check_user_role_permissions('55f1cead850d2d6df7b113e0')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"cronjobs_history"
# 	if p:
# 		usr_details['loadmaster']='ok'	
# 		usr_details['mng_cronjobs']='ok'
# 		usr_details['cronjobs_history']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'loadmaster':'55f1cead850d2d6df7b113e0','mng_cronjobs':'55f1cead850d2d6df7b113e0','cronjobs_history':'55f1cead850d2d6df7b113e0'}})


# 	deptid=get_designation_id(current_user.username)
# 	p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"pmprmivapproval"
# 	q=find_one_in_collection("Plants",{"chief_pid":deptid})
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"pmprmivapproval"
# 	if p or q:
# 		usr_details['pmprmivapproval']='ok'
# 		# update_collection('userpermissions',{'username':current_user.username},{'$set':{'la':'546f2097850d2d1b00023b26'}})

# 	if current_user.username!="admin":
# 		if usr_details["plant_id"]!="HY":
# 			usr_details['pmprmivcreation']='ok'
# 			# update_collection('userpermissions',{'username':current_user.username},{'$set':{'pmprmivcreation':'546f2097850d2d1b00023b26'}})

# 	deptid=get_designation_id(current_user.username)
# 	p=find_one_in_collection("departmentmaster",{"dept_chief_pid":deptid})
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"hodmivreport"
# 	if p:
# 		usr_details['hodmivreport']='ok'
# 		# update_collection('userpermissions',{'username':current_user.username},{'$set':{'hodmivreport':'546f2097850d2d1b00023b26'}})

# 	# 	P and QC permissions

# 	p=check_user_role_permissions('562760c619893c58e7867967')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"pqctcentry"
# 	if p:
# 		usr_details['pqctcentry']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'pqctcentry':'562760c619893c58e7867967'}})
		
# 	p=check_user_role_permissions('562761e519893c58e7867969')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"pqctcview"
# 	if p:
# 		usr_details['pqctcapproval']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'pqctcapproval':'562761e519893c58e7867969'}})

# 	p=check_user_role_permissions('5627612919893c58e7867968')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"endtime"
# 	if p:
# 		usr_details['pqctcview']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'pqctcview':'5627612919893c58e7867968'}})



# 	#Investment proposal permissions
# 	p=check_user_role_permissions('565305fbe56ed73aa892b4c1')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"investadmin"
# 	if p:
# 		usr_details['investadmin']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'investadmin':'565305fbe56ed73aa892b4c1'}})

# 	#Kiazen Admin role and permission
# 	p=check_user_role_permissions('56d16ac52d6a7883ef2e81e9')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"Kiazenadmin"
# 	if p:
# 		usr_details['Kiazenadmin']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'Kiazenadmin':'56d16ac52d6a7883ef2e81e9'}})

# 	#IMS Admin role and permission
# 	p=check_user_role_permissions('5715c333a82b5bb3a11b3ec8')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"IMSadmin"
# 	if p:
# 		usr_details['IMSadmin']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'IMSadmin':'5715c333a82b5bb3a11b3ec8'}})

# 	#ERecuritment Admin role and permission
# 	p=check_user_role_permissions('5739aa20a82b5bb3a11b4219')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"ERecuriadmin"
# 	if p:
# 		usr_details['ERecuriadmin']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'ERecuriadmin':'5739aa20a82b5bb3a11b4219'}})

# 	p=check_user_role_permissions('573d5a183e85a7622d018684')
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"super_level_config"
# 	if p:
# 		usr_details['super_level_config']='ok'
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'super_level_config':'573d5a183e85a7622d018684'}})


# 	tim=find_one_in_collection("Notificationtimestamp",{"username":current_user.username})
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"Notificationtimestamp"	
# 	if tim:
# 		notify_num=find_in_collection_count("Notifications",{"created_time": {"$gt":tim['created_time']},"e_id":current_user.username})
# 		usr_details['notify_num']=notify_num
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'notify_num':notify_num}})
# 	else:
# 		usr_details['notify_num']=0
# 		update_collection('userpermissions',{'username':current_user.username},{'$set':{'notify_num':0}})
# 	endtime=datetime.datetime.now()-stime
# 	print endtime,"base functions access time"
# 	return usr_details


#function to send email
def email(subject,receivers,message,uname):
	sender = 'myportal@pennacement.com'
	msg = MIMEText('Dear '+uname+',\n\n'+message+'\n\n\n\nThanks,\nTeam-HR.')
	msg['To'] = receivers
	msg['From'] ='myportal@pennacement.com'
	msg['Subject'] = subject
	# smtpObj = smtplib.SMTP('202.65.143.58',25)
	smtpObj = smtplib.SMTP('202.65.143.62',25)
	smtpObj.set_debuglevel(True) # show communication with the server

	try:
		smtpObj.sendmail(sender, receivers,msg.as_string())         
   		print "Successfully sent email"
	except Exception,R:
            print R
 
 #function to send SMS
def SendSms(message,mobilenumber,uname):
	url = "http://www.smscountry.com/smscwebservice_bulk.aspx"
	values = {'user' : 'pcilhr',
	'passwd' : 'pcil@123',
	'message' : "Dear "+uname+','+message,
	'mobilenumber':mobilenumber,
	'mtype':'N',
	'DR':'Y'
	}

	data = urllib.urlencode(values)
	data = data.encode('utf-8')
	request = urllib2.Request(url,data)
	response = urllib2.urlopen(request)

 #function to create notification in myportal for an employee
def notification(employee_id,message):
	save_collection('Notifications',{"e_id":employee_id,"msg":message,"created_time":datetime.datetime.now()})

