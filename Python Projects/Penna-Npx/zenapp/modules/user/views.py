from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
from zenapp.sessions import *
from datetime import datetime
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from zenapp.modules.leave.lfunctions import *
from pyrfc import *
import logging
import json
import random
import urllib2
import sys
import smtplib
zen_user = Blueprint('zenuser', __name__, url_prefix='/user')


#moved on 10_june_2016
@zen_user.route('/', methods = ['GET', 'POST'])
@login_required
def index():
	p =check_user_role_permissions('546f2271850d2d1b00023b2a')
	if p:
		user_details=base()
		uid = current_user.username
		lh=find_in_collection('Leaves',{"employee_id":uid, "status":"applied"})
		a=[]
		if lh:
			# Datetime in DB is datetime.datetime format so no need to convert again
			for e in lh:
				e['start_date']=datetime.datetime.strftime(e['start_date'],"%d/%m/%Y")
				e['end_date']=datetime.datetime.strftime(e['end_date'],"%d/%m/%Y")
				e['reason']=e['leave_reason']
				if e['attribute'] == 'attendance':
					e['leave']=get_attendance_type(e['leave_type'])
				elif e['attribute'] == 'permission':
					e['leave']='Permission'
				else:
					e['leave']=get_leave_type(e['leave_type'])
				for k in e['approval_levels']:
					if k['a_status'] == 'current':
						e['current_discuss'] = k['a_discuss']
						e['current_al_name'] = get_employee_name(k['a_id'])
				try:
					e['hr_name']= get_employee_name(get_hr_id(e['plant_id']))
				except:
					logging.info('HR Of Plant Is Not Available For Employee '+str(uid)+' Of Plant '+user_details['plant_id'])		
				a=a+[e]
		if uid != 'admin':
			ams=find_in_collection_sort('Announcements',{"plant_id":get_plant_id(uid),"view":1},[("created_time",-1)])
		if request.method == 'POST':
			form=request.form
			update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"status":"rejected"}})
			revoked_info=find_one_in_collection('Leaves',{'_id':ObjectId(form['leaveid'])})
			if revoked_info['attribute'] == 'attendance':
				leave_type=get_attendance_type(revoked_info['leave_type'])
			else:
				leave_type=get_leave_type(revoked_info['leave_type'])
			revoked_start_date = datetime.datetime.strftime(revoked_info['start_date'], "%d/%m/%Y")
			revoked_end_date = datetime.datetime.strftime(revoked_info['end_date'], "%d/%m/%Y")
			#message1='You have revoked '+leave_type+' from '+revoked_info['start_date']+' to '+revoked_info['end_date']
			message1='You have revoked '+leave_type+' from '+revoked_start_date+' to '+revoked_end_date
			notification(uid,message1)
			get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
			#message2=get_employee_name(uid)+' revoked '+leave_type+' from '+revoked_info['start_date']+' to '+revoked_info['end_date']	
			message2=get_employee_name(uid)+' revoked '+leave_type+' from '+revoked_start_date+' to '+revoked_end_date	
			notification(get_hr_id(revoked_info['plant_id']),message2)
			#sms2=get_employee_name(uid)+' revoked '+leave_type+' from '+revoked_info['start_date']+' to '+revoked_info['end_date']
			sms2=get_employee_name(uid)+' revoked '+leave_type+' from '+revoked_start_date+' to '+revoked_end_date
			get_employee_email_mobile(get_hr_id(revoked_info['plant_id']),'My Portal Leave Notification',message2,sms2)
			if revoked_info['approval_levels']:
				for i in range(0,len(revoked_info['approval_levels'])):
					notification(revoked_info['approval_levels'][i]['a_id'],message2)
					get_employee_email_mobile(revoked_info['approval_levels'][i]['a_id'],'My Portal Leave Notification',message2,sms2)
			flash(message1,'alert-success')
			return redirect(url_for('zenuser.index'))
		if uid != 'admin':
			if user_details['dob'][:5]==datetime.datetime.now().strftime("%d-%m"):
				if user_details['birthday_flag']==0:
					users = {"username": current_user.username}
					users1= {"$set":{"birthday_flag":1}}
					update_coll('Userinfo',users,users1)
					ran=random.randrange(1,5)
					return render_template('index.html', user_details=user_details,lh=a,ams=ams,birthday=ran)
				else:
					return render_template('index.html', user_details=user_details,lh=a,ams=ams)
			else:
				if user_details['birthday_flag']==1:
					users = {"username": current_user.username}
					users1= {"$set":{"birthday_flag":0}}
					update_coll('Userinfo',users,users1)
					return render_template('index.html', user_details=user_details,lh=a,ams=ams)
		try:
			return render_template('index.html', user_details=user_details,lh=a,ams=ams)
		except:
			return render_template('index.html', user_details=user_details,lh=a)
	else:
		return redirect(url_for('zenuser.index'))

@zen_user.route('/signin/', methods = ['GET', 'POST'])
def signin():
	if current_user is not None and current_user.is_authenticated:
        	return redirect(url_for('zenuser.index'))
	form = SigninForm(request.form)
	if request.method == 'POST':
		if request.form['submit'] == 'login':		
			pw_hash = hashlib.sha1(form.password.data).hexdigest()
			user = find_by_username(form.username.data)
			first_mail = request.form['username']
			print(first_mail)
			pwd=find_one_in_collection('Userinfo',{'$or':[{'username':form.username.data},{'official_email':{'$regex':str(first_mail),'$options':'i'}}]})      		
			if user is not None:
				if pwd['employee_status']=='inactive':
					flash('User is blocked','alert-danger')
					return redirect(url_for('zenuser.signin'))
				if pwd['password'] == "":
					if user.date == form.date.data:
						users = {"dob": form.date.data, "username": form.username.data}
						users1= {"$set":{"password": hashlib.sha1(str(form.changepwd.data)).hexdigest()}}
	 					update_collection('Userinfo',users,users1)
	 					flash('New password is updated','alert-success')
	 					return redirect(url_for('zenuser.signin')) 											
					else:
						flash('Password incorrect','alert-danger')
				elif pwd['password'] == pw_hash:
					if login_user(user):
						forget_pwd=find_one_in_collection('Userinfo',{'chngpwd':'1','username':form.username.data})
						if forget_pwd:
							return redirect(url_for('zenuser.changepassword'))
						else:
							try:
								unfinished_sess = find_in_collection_sort_limit('logurl',{"username":form.username.data},[('logintime',-1)],1)
			 					if unfinished_sess[0]['lastlogin'] == "":
									unexp_logout = find_in_collection_sort_limit('sessions',{'data.user_id':current_user.userid},[("expiration",-1)],1 )
					 				dt_now=datetime.datetime.now()
					 				if unexp_logout[0]['expiration'] >dt_now :
					 					update_collection('logurl',{'_id':unfinished_sess[0]['_id']},{'$set':{'lastlogin':dt_now}})
					 				else:
					 					update_collection('logurl',{'_id':unfinished_sess[0]['_id']},{'$set':{'lastlogin':unexp_logout[0]['expiration']}})
							except:
								pass
							try:
								user=find_one_in_collection('Userinfo',{'username':form.username.data})
								users = {"username": form.username.data}
								module = []
								moduleinfo ={"username":form.username.data,"l_name":user['l_name'],"logintime":datetime.datetime.now(),"lastlogin":'',"module":module}
								users1= {"$set":{"current_login":datetime.datetime.now(),"last_login":user['current_login']}}
			 					update_coll('Userinfo',users,users1)
			 					save_collection('logurl',moduleinfo)
							except:
								user=find_one_in_collection('Userinfo',{'username':form.username.data})
								users = {"username": form.username.data}
								users1= {"$set":{"current_login":datetime.datetime.now(),"last_login":"Welcome to My Portal"}}
			 					update_coll('Userinfo',users,users1)
							return redirect(request.args.get('next') or url_for('zenuser.index'))
					else:
						flash('Error','alert-danger')
				else:
					flash('Password incorrect','alert-danger')
					return redirect(url_for('zenuser.signin'))
			else:
				flash('Password incorrect','alert-danger')	
				return redirect(url_for('zenuser.signin'))
		elif request.form['submit'] == 'getpassword':
			try:
				user = find_one_in_collection('Userinfo',{'username':request.form['f_username']})
				if user["official_email"]!='' or user['official_contact']!='' or user['personal_contact1']!='':
					new_pwd=str(uuid.uuid4().get_hex().upper()[0:6])
					users = {"username": request.form['f_username']}
					users1= {"$set":{"password": hashlib.sha1(new_pwd).hexdigest(),"chngpwd":"1","status":"1"}}
					update_collection('Userinfo',users,users1)
					smsg= "Your password to login into MyPortal is "+new_pwd
					# get_employee_email_mobile(request.form['f_username']," My Portal Profile Notification",smsg,smsg)
					fname=get_employee_name(request.form['f_username'])
					if user["official_contact"] != '':
						SendSms(smsg,user["official_contact"],fname)
					elif user["personal_contact1"] != '':
						SendSms(smsg,user["personal_contact1"],fname)
					else:
						notification(uid,'Please update your official email or official mobile number or personal mobile number')
					

					# if user["official_email"]!='':
					# 	flash("Your password has been sent to your official email",'alert-success')
					if user["official_contact"]!='':
						flash("Your password has been sent to your  official contact number",'alert-success')
					elif user["personal_contact1"]!='':
						flash("Your password has been sent to your  personal contact number",'alert-success')
				else:
					flash("Please Contact HR to reset your password",'alert-info')
			except:
				flash("Employee Not Found. Try Again",'alert-info')
			return redirect(url_for('zenuser.signin'))
	return render_template('signin.html', form = form)


@zen_user.route('/signout')
@login_required
def signout():
	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	update_coll('logurl',{'logintime':time},{'$set':{"lastlogin":datetime.datetime.now()}})
	logout_user()
	flash('You have been logged out.','alert-info')
	return redirect(url_for('zenuser.signin'))
        
# generating dynamic urls for images
@zen_user.route('/static/img/gridfs/<filename>')
@login_required
def gridfs_img(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
    return Response(thing, mimetype='image/jpeg')

@zen_user.route('/static/pdf/gridfs/<filename>')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
    return Response(thing, mimetype='application/pdf/doc/docx//xlsm/xlsx')


@zen_user.route('/profile/', methods = ['GET', 'POST'])
@login_required
def profile():
	p =check_user_role_permissions('546f22c5850d2d1b00023b30')
	if p:
		user_details = base()
		user_details['dob']=current_user.date
		prof_status = find_one_in_collection('Gendata',{"_id":ObjectId("5465de13850d2d61a4a4e25d")})
		dependant=find_one_in_collection('Gendata',{"_id":ObjectId("54a92f9e850d2d5ce738308e")})
		position_details=find_one_in_collection('Positions',{"position_id":user_details['designation_id']})
		rms=[]
		try:
			for i in range(0,len(position_details['levels'])):
				if user_details['m'+str(i+1)+'_pid'] != '' and user_details['m'+str(i+1)+'_pid'] in position_details['levels']:
					rms+=[user_details['m'+str(i+1)+'_name']]
		except:
			logging.info('Profile error')
		return render_template('profile.html', user_details=user_details ,prof_status=prof_status,dependant=dependant,rms=rms)

	else:
		return redirect(url_for('zenuser.index'))

#function to check while login  user is present in our database or not 

@zen_user.route('/checkuser', methods=['GET', 'POST'])
def checkuser():
	user =  request.form['username']
	users = {"username": user,"password" : ""}
	a=find_one_in_collection('Userinfo',users) 
	if a:
		return '1'
	else:
		users = {"username": user}
		b=find_one_in_collection('Userinfo',users) 
		if b:
			return '2'	
		else:
			return '0'

#function to check while login  user is active or not 
@zen_user.route('/checkemp', methods=['GET', 'POST'])
def checkemp():
	user =  request.form['employee_id']
	users = {"username": user}
	a=find_one_in_collection('Userinfo',users) 
	if a:
		if a['employee_status']=="active":
			return '1'
		else:
			return '2'
	else:
		return '0'

#needed to be moved for forgot password
@zen_user.route('/checkempstatus', methods=['GET', 'POST'])
def checkempstatus():
	user =  request.form['employee_inactive']
	users = {"employee_id": user}
	a=find_one_in_collection('Leaves',{"employee_id": user})
	b=find_in_collection('Userinfo',{"username":user,"employee_status":"active"})
	if a:
		if 'employee_status' in a:
			return '1' #employee is inactive in leaves already ,then show 
		else:
			return "2" #employee is inactive in userinfo and active in leaves
	elif b:
		return '3' #active in userinfo and inactive in leave reports
	else:
		return '0' #employee data not found 


#function to check while creating password  the given date of birth with respect to username is correct or not 
@zen_user.route('/checkdate', methods=['GET', 'POST'])
def checkdate():
	user =  request.form['username']
	date =  request.form['date']

	users = {"username": user,"dob" : date}
	a=find_one_in_collection('Userinfo',users) 
	if a:
		return '1'
	else:
		return '0'			

# function to  add external file to our database
def file_save_gridfs(file_data,ctype,ctag):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username
    userpic = find_one_in_collection('fs.files',{"uid" :uid ,"tag" :ctag})
    if userpic:
    	del_doc('fs.chunks',{"files_id":userpic['_id']})
    	del_doc('fs.files',{"_id":userpic['_id']})
    file_ext = file_data.filename.rsplit('.', 1)[1]
    fs.put(file_data.read(), content_type=ctype, filename = str(uuid.uuid4())+'.'+file_ext, uid = uid,
        tag = ctag)

#function to upload profile pic 
@zen_user.route('/imgupload', methods=['GET', 'POST'])
@login_required
def imgupload():
	input_file = request.files['image']
	file_save_gridfs(input_file,"image/jpeg","profileimg") 
	return '0'


@zen_user.route('/updateofficaluser', methods=['GET', 'POST'])
@login_required
def updateofficaluser():
	user_details = base()
	e=0
	a={}
	if(user_details['official_email']!=request.form['official_email']):
		a["official_email"]=request.form['official_email']
		e=e+1
	if(user_details['official_contact']!=request.form['official_contact']):
		a["official_contact"]=request.form['official_contact']
		e=e+1
	if(e > 0):
		 check= find_one_in_collection('Temp_userinfo',{'employee_id':current_user.username})
		 if check:
		 	users={'employee_id':current_user.username}
		 	users1={"$set":a}
		 	update_collection('Temp_userinfo',users,users1)
		 	return '0'
		 else:	
			a["employee_id"]=current_user.username
			a["plant_id"]=user_details["plant_id"]
			save_collection('Temp_userinfo',a)
			return '0'
	else:
		return '1'

@zen_user.route('/updateuser', methods=['GET', 'POST'])
@login_required
def updateuser():
	user_details = base()
	uid = current_user.get_id()
	usr_details = find_one_in_collection('Userinfo',{'_id': ObjectId(uid)})
	a={}
	e=0
	h=0

	if request.files['preaddress']:
		preaddress=request.files["preaddress"]
		file_save_gridfs(preaddress,"application/pdf","preaddress_proof") 
		b={}
		b["hno_street"]=request.form['prehno_street']
		b["address_line2"]=request.form['preaddress_line2']
		b["district"]=request.form['predistrict']
		b["pincode"]=request.form['prepincode']
		b["city"]=request.form['precity']
		b["state"]=request.form['prestate']
		b["subtype"]="2"
		b["country"]="IN"
		b["url"]=preaddress_pdf()
		a={"present_address":b}
		a["a_status"]="1"
		e=e+1
		

	if request.files['peraddress']:
		peraddress=request.files["peraddress"]
		file_save_gridfs(peraddress,"application/pdf","peraddress_proof")
		c={}
		c["hno_street"]=request.form['perhno_street']
		c["address_line2"]=request.form['peraddress_line2']
		c["district"]=request.form['perdistrict']
		c["pincode"]=request.form['perpincode']
		c["city"]=request.form['percity']
		c["state"]=request.form['perstate']
		c["subtype"]="1"
		c["country"]="IN"
		c["url"]=peraddress_pdf()
		a["permanent_address"]=c
		a["a_status"]="1"
		e=e+1
		

	if(request.form['m_status']!='select'):
		if(user_details['m_status']!=request.form['m_status']):
			a["m_status"]=request.form['m_status']
			a["p_status"]="1"
			e=e+1
	if(usr_details['personal_contact1']!=request.form['contact_no']):
		a["personal_contact1"]=request.form['contact_no']
		a["p_status"]="1"
		e=e+1
	if(usr_details['personal_contact2']!=request.form['contact_no2']):
		a["personal_contact2"]=request.form['contact_no2']
		a["p_status"]="1"
		e=e+1

	de=request.form["dependents"].split(',')


	list1=[]
	if usr_details['dependents']:
		for dependent in usr_details['dependents']:
			list1.append(dependent['relation'])
			list1.append(dependent['fname'])
			list1.append(dependent['lname'])
			list1.append(dependent['dob'])
		
		if list1 != de:
			dependents=[]
			for z in range(0,len(de),4):
				dependents+=[{"relation":de[z],"fname":de[z+1],"lname":de[z+2],"dob":de[z+3]}]
			a["dependents"]=dependents
			a["d_status"]="1"
			e=e+1
	else :
		dependents=[]
		for z in range(0,len(de),4):
			dependents+=[{"relation":de[z],"fname":de[z+1],"lname":de[z+2],"dob":de[z+3]}]
		a["dependents"]=dependents
		a["d_status"]="1"
		e=e+1

	if(user_details['personal_email']!=request.form['personal_email']):
		users = {'_id': ObjectId(uid)}
		users1= {"$set":{"personal_email":request.form['personal_email'],"p_status":"1"}}
 		update_collection('Userinfo',users,users1)
 		h=h+1




	if(e > 0):
		 check= find_one_in_collection('Temp_userinfo',{'employee_id':current_user.username})
		 if check:
		 	users={'employee_id':current_user.username}
		 	users1={"$set":a}
		 	update_collection('Temp_userinfo',users,users1)
		 	return '0'
		 else:	
			a["employee_id"]=current_user.username
			a["plant_id"]=user_details["plant_id"]
			save_collection('Temp_userinfo',a)
			return '0'
	elif(h>0):
		return '0'
	else:
		return '1'

# application/pdf


@zen_user.route('/createrole/', methods=['GET', 'POST'])
@login_required
def createrole():
	p =check_user_role_permissions('546f22b9850d2d1b00023b2f')
	if p:
		user_details = base()
		form = CreateRoleForm(request.form)
		list_of_permissions = find_all_and_filter('Rolepermissions',{"name" : 1})
		form.permissions.choices = [(str(x['_id']), x['name']) for x in list_of_permissions]
		if request.method == 'POST':
			array={"role_name":form.role_name.data, "permissions":form.permissions.data, "created_time": datetime.datetime.now()}
			save_collection('Roles',array)
			flash('role created successfully','alert-success')
			return redirect(url_for('zenuser.createrole'))
		return render_template('role.html', form=form, user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))
 

@zen_user.route('/createpermission/', methods=['GET', 'POST'])
@login_required
def createpermission():
	user_details = base()
	form = CreatePermissionForm(request.form)
	if request.method == 'POST' and form.validate():
		array={"name":form.name.data, "description":form.description.data, "created_time": datetime.datetime.now()}
		save_collection('Rolepermissions',array)
		flash('permission created successfully','alert-success')
		return redirect(url_for('zenuser.createpermission'))
	return render_template('permission.html', form=form, user_details=user_details)

# def assigningbasevalues(permissionid):
#     print "acessing permissions"
#     basesetvalues={'la':'546f2097850d2d1b00023b26','ae':'546f228c850d2d1b00023b2c','pa':'546f22de850d2d1b00023b32','al':'546f204b850d2d1b00023b22',
#                     'hl':'547c7e0157fbb30ed690336e','rep':'5487ed0757fbb30f702709c0','cr':'546f22a8850d2d1b00023b2e','ps':'546f2245850d2d1b00023b27',
#                     'f16':'546f2257850d2d1b00023b28','ale':'546f2038850d2d1b00023b21','ar':'546f22b9850d2d1b00023b2f','ams':'54ab5b94507c00c54ba22297',
#                     'lt':'54ae3abd507c00c54ba22299','er':'54b0fe8a507c00c54ba2229a','eh':'54b378d4507c00c54ba2229b','sh':'54ab60d0507c00c54ba22298',
#                     'trade':'54b9001c507c00c54ba2229d','nontrade':'54b9002e507c00c54ba2229e','ag':'54b90061507c00c54ba2229f','sales':'54b900a2507c00c54ba222a0',
#                     'coll':'54b900db507c00c54ba222a1','po':'54b900f7507c00c54ba222a2','us':'55c9d3c112ebc4ff41fa157c','bk':'55c9d3f512ebc4ff41fa157d',
#                     'ob':'54b90128507c00c54ba222a3','prlevelConfig':'550fdf44850d2d3861bd7ae4',
#                     'loadmaster':'55f1cead850d2d6df7b113e0','mng_cronjobs':'55f1cead850d2d6df7b113e0','cronjobs_history':'55f1cead850d2d6df7b113e0',
#                     'pqctcentry':'562760c619893c58e7867967','pqctcapproval':'562761e519893c58e7867969','pqctcview':'5627612919893c58e7867968',
#                     'investadmin':'565305fbe56ed73aa892b4c1','Kiazenadmin':'56d16ac52d6a7883ef2e81e9','IMSadmin':'5715c333a82b5bb3a11b3ec8',
#                     'ERecuriadmin':'5739aa20a82b5bb3a11b4219','super_level_config':'573d5a183e85a7622d018684'
#                     }
#     if permissionid in basesetvalues.values():
#         fieldname=basesetvalues.keys()[basesetvalues.values().index(permissionid)]
#         setbase=update_collection_multi('userpermissions',{'permissions':{'$in':[permissionid]}},{'$set':{fieldname:permissionid}})
#         print {fieldname:permissionid}
#         if setbase:
#             print "success"
#         else:
#             print "error"

def getusername(desid):
    username=find_one_in_collection('Userinfo',{'designation_id':desid})
    username=username['username']
    return username

def assigningbasevalues(permissionid,designation_id,permissions):
    # print "acessing permissions"
    basesetvalues=find_one_in_collection('userpermissions_config',{'config':'user_permissions'})
    basesetvalues=basesetvalues['permissions']
    if permissionid in basesetvalues.values():
        fieldname=basesetvalues.keys()[basesetvalues.values().index(permissionid)]

        chkpermission=find_in_collection('userpermissions',{'designation_id':designation_id,'permissions':{'$in':[permissionid]}})
        if chkpermission:
            setbase=update_coll('userpermissions',{'designation_id':designation_id},{'$set':{fieldname:permissionid}})
        else:
            setbase=update_coll('userpermissions',{'designation_id':designation_id},{'$unset':{fieldname:permissionid}})

@zen_user.route('/assignrole/', methods=['GET', 'POST'])
@login_required
def assignrole():
	print "asssssss"
	p =check_user_role_permissions('546f22b9850d2d1b00023b2f')
	print "ttttt",p
	if p:
		user_details = base()
		form = AssignRoles(request.form)
		list_of_positions = find_all_and_filter('Positions',{"_id":0,"position_id":1,"position_name":1})
		list_of_roles = find_all_and_filter('Roles',{"role_name" : 1})
		form.select_role.choices = [(str(x['_id']), x['role_name']) for x in list_of_roles]	
		print list_of_positions
		if request.method == 'POST':
			db = dbconnect()
			array={"position_id":form.designations.data}
			update_collection('Positions',array,{'$set' : {"roles" : form.select_role.data}})
			# print form.select_role.data,"new_role"
			user_roles = form.select_role.data
			count = len(user_roles)
			Role_Permissions =[]
			for k in range(0,count,1):
				P_in_Roles = db.Roles.aggregate([
							{'$match' : {'_id' : ObjectId(user_roles[k])}},
							{'$project' : {'permissions' : 1, '_id' : 0}}])
				P_in_Roles=list(P_in_Roles)
				Role_Permissions = Role_Permissions + P_in_Roles[0]["permissions"]
			update_collection('userpermissions',{'designation_id':form.designations.data},{'designation_id':form.designations.data,"username":getusername(form.designations.data),'permissions':Role_Permissions})
			for i in Role_Permissions:
				assigningbasevalues(i,form.designations.data,Role_Permissions)
			flash('Role assigned successfully','alert-success')
			return redirect(url_for('zenuser.assignrole'))
		return render_template('assign_role.html', form=form, user_details=user_details,list_of_positions=list_of_positions)
	else:
		return redirect(url_for('zenuser.index'))

@zen_user.route('/editrole/', methods=['GET', 'POST'])
@login_required
def editrole():
	p =check_user_role_permissions('54b0fe8a507c00c54ba2229a')
	if p:
		users_role_update=[]
		user_details = base()
		form = EditRole(request.form)
		list_of_roles = find_all_and_filter('Roles',{"role_name":1,"permissions":1})
		form.roles.choices = [(str(x['_id']), x['role_name']) for x in list_of_roles]
		form.roles.choices.insert(0, ("", "Select Role"))
		list_of_permissions = find_all_and_filter('Rolepermissions',{"name" : 1})
		form.select_permissions.choices = [(str(x['_id']), x['name']) for x in list_of_permissions]	
		if request.method == 'POST':
			array={"_id":ObjectId(form.roles.data)}
			update_collection('Roles',array,{'$set' : {"permissions" : form.select_permissions.data}})
			new_permissions=form.select_permissions.data
			usrs_with_permission=find_and_filter('userpermissions',{'permissions':{'$in':new_permissions}},{"_id":0,"username":1,"designation_id":1})
			for i in usrs_with_permission:
				users_role_update.append(i['designation_id'])
			users="users with position id "
			for i in users_role_update:
				users=users+","+str(i)
			msg= users+" Roles has tobe updated once"
			if len(usrs_with_permission)== 0:
				flash('Role Updated successfully','alert-success')
			else:
				flash(msg,'alert-danger')
			return redirect(url_for('zenuser.editrole'))
		return render_template('edit_role.html', form=form, user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))
		
@zen_user.route('/getpositionroles', methods=['GET', 'POST'])
def getpositionroles():
	deg = request.form.get('deg')
	p_details=find_one_in_collection('Positions',{"position_id":deg})
	if p_details:
		roles=p_details['roles']
	else:
		roles=[]
	return jsonify({"roles":roles})

@zen_user.route('/getrolepermissions', methods=['GET', 'POST'])
def getrolepermissions():
	deg = request.form.get('deg')
	p_details=find_one_in_collection('Roles',{"_id":ObjectId(deg)})
	if p_details:
		permissions=p_details['permissions']
	else:
		permissions=[]
	return jsonify({"permissions":permissions})

@zen_user.route('/changepassword', methods=['GET', 'POST'])
@login_required
def changepassword():
	user_details=base()
	form = ChangePasswordForm(request.form)	
	if request.method == 'POST' and form.validate():
		pw_hash = hashlib.sha1(form.old_pass.data).hexdigest()
		if pw_hash == user_details['password'] :
			pw_hash_new = hashlib.sha1(form.new_pass.data).hexdigest()

			update_coll('Userinfo',{"_id":ObjectId(current_user.get_id())},{'$set':{"password": pw_hash_new,"chngpwd":'0'}})
			flash('password updated successfully','alert-success')
			return redirect(url_for('zenuser.index'))
		else:
			flash('invalid password','alert-danger')
			return redirect(url_for('zenuser.changepassword'))
	return render_template('changepassword.html', user_details=user_details, form=form)

@zen_user.route('/profileapprovals/', methods = ['GET', 'POST'])
@login_required
def profileapprovals():
	p =check_user_role_permissions('546f22de850d2d1b00023b32')
	if p:
		user_details=base()
		
		if request.method == 'POST':
			hr1 = find_one_in_collection('Temp_userinfo',{'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']})
			if request.form['submit'] == 'agree':
				del hr1['_id']
				del hr1['employee_id']
				try:
					hr1['permanent_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"peraddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']})
					del hr1['permanent_address']["url"]
				except:
					print "none"


				try:
					hr1['present_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"preaddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']})
					del hr1['present_address']["url"]
				except:
					print "none"

				users={'plant_id':get_plant_id(current_user.username),'username':request.form['id']}
				users1={"$set":hr1}
				update_collection('Userinfo',users,users1)
				use={'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']}
				del_doc('Temp_userinfo',use)
				name=get_employee_name(current_user.username)
				smsg="Your Profile changes has been accepted by "+name
				notification(request.form['id'],smsg)
				get_employee_email_mobile(request.form['id']," My Portal Profile Notification",smsg,smsg)
			elif request.form['submit'] == 'disagree':
				hr1 = find_one_in_collection('Temp_userinfo',{'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']})
				try:
					hr1['permanent_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"peraddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']})
				except:
					print "none"

				try:
					hr1['present_address']
					userpic = find_one_in_collection('fs.files',{"uid" :request.form['id'],"tag" :"preaddress_proof"})
					del_doc('fs.chunks',{"files_id":userpic['_id']})
					del_doc('fs.files',{"_id":userpic['_id']}) 
				except:
					print "none"

				users={'plant_id':get_plant_id(current_user.username),'employee_id':request.form['id']}
				del_doc('Temp_userinfo',users)
				name=get_employee_name(current_user.username)
				smsg="Your Profile changes has been rejected by "+name
				notification(request.form['id'],smsg)
				get_employee_email_mobile(request.form['id']," My Portal Profile Notification",smsg,smsg)


				
		prof_status = find_one_in_collection('Gendata',{"_id":ObjectId("5465de13850d2d61a4a4e25d")})
		dependant=find_one_in_collection('Gendata',{"_id":ObjectId("54a92f9e850d2d5ce738308e")}) 		
		hr = find_in_collection('Temp_userinfo',{'plant_id':get_plant_id(current_user.username)})
		return render_template('profile_approvals.html', user_details=user_details,hr=hr,prof_status=prof_status,dependant=dependant)
	else:
		return redirect(url_for('zenuser.index'))


@zen_user.route('/notificationstamp', methods=['GET', 'POST'])
@login_required
def notificationstamp():
	chk=find_one_in_collection('Notificationtimestamp',{"username":current_user.username})
	if chk:
		update_collection('Notificationtimestamp',{"username":current_user.username},{"$set":{"created_time":datetime.datetime.now()}})
		return '0'
	else:
		save_collection('Notificationtimestamp',{"username":current_user.username,"created_time":datetime.datetime.now()})
		return '0'

@zen_user.route('/newemployee/', methods=['GET', 'POST'])
@login_required
def addnewemployee():
	p =check_user_role_permissions('546f228c850d2d1b00023b2c')
	if p:
		user_details=base()
		if request.method == 'POST':
			form=request.form
			if form['submit'] == 'Add':
				check_employee=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if check_employee:
					flash('Employee is already exist','alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
				sap_login=get_sap_user_credentials()
				try:
					conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
					# conn = Connection(user='greenbyte', passwd='guna1234', ashost='192.168.18.16', gwserv='3301', sysnr='D01', client='150')
					
					result = conn.call('Z_LMS_PERSONAL_DATA',PERNR=form['employee_id'])
					if result['IT_0002']:
						result1 = conn.call('Z_LMS_ADDRESS_DATA',PERNR=form['employee_id'])
						e = result['IT_0002'][0]
						dob = datetime.datetime.strptime(str(e['GBDAT']), '%Y-%m-%d')
						dob_new = dob.strftime('%d-%m-%Y')
						if e['EMPDOJ'] is None :
							doj_new='Not Available'
						else:
							doj = datetime.datetime.strptime(str(e['EMPDOJ']), '%Y-%m-%d')
							doj_new = doj.strftime('%d-%m-%Y')
						if result1['IT_0006T'] :
							f = result1['IT_0006T'][0]
							array3={"hno_street":f['STRAS'],"address_line2":f['LOCAT'],"city":f['ORT01'],
						"district":f['ORT02'],"state":f['STATE'],"country":f['LAND1'],"pincode":f['PSTLZ'],
						"subtype":f['SUBTY']}
						else:
							array3={}
						if result1['IT_0006P'] :
							p = result1['IT_0006P'][0]
							array4={"hno_street":p['STRAS'],"address_line2":p['LOCAT'],"city":p['ORT01'],
						"district":p['ORT02'],"state":p['STATE'],"country":p['LAND1'],"pincode":p['PSTLZ'],
						"subtype":p['SUBTY']}
						else:
							array4={}
						result2 = conn.call('Z_LMS_EMP_DEPENDENT_DATA',PERNR=form['employee_id'])
						current_year=datetime.datetime.now().year
						end_date=str(current_year)+'1231'
						start_date=str(current_year)+'0101'
						result3 = conn.call('Z_LMS_WORK_SCHEDULE',BEGDA=start_date,ENDDA=end_date,PERNR=form['employee_id'])
						result4 = conn.call('Z_LMS_LEAVE_QUOTA',P_PERNR=form['employee_id'])
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
						b=[]
						if result2['IT_0021']:
							for j in range(0,len(result2['IT_0021'])):
								if result2['IT_0021'][j]['DOB'] is not None:
									new_dob=result2['IT_0021'][j]['DOB'].strftime("%d-%m-%Y")
								else:
									new_dob=''
								b+=[{"fname":result2['IT_0021'][j]['FIRSTNAME'],"lname":result2['IT_0021'][j]['LASTNAME'],"relation":result2['IT_0021'][j]['SUBTY'],"dob":new_dob}]
						
						array2={"username":e['PERNR'][3:],"password":"","dob":dob_new,"f_name":e['VORNA'],"l_name": e['NACHN'],
								"m_status": e['FAMST'],"personal_contact1": e['CONTACT'],"personal_contact2": e['PCONTACT'],"official_contact": e['OCONTACT'],
								"personal_email": e['PEMAIL'],"official_email" :e['OEMAIL'],"department_id":e['ORGEH'],"department_name":e['ORGTX'],
								"gender":e['GESCH'],"designation_id":e['PLANS'],"designation":e['PLSTX'],"country":e['NATIO'],
								"created_time":datetime.datetime.now(),
								"present_address":array3,"permanent_address":array4,"pancard":e['ICNUM'],"calendar_code":e['MOFID'],
								"employee_status":"active","m1_pid":e['MANAGER1'],"m2_pid":e['MANAGER2'],"m3_pid":e['MANAGER3'],
								"m4_pid":e['MANAGER4'],"m5_pid":e['MANAGER5'],"m6_pid":e['MANAGER6'],"m7_pid":e['MANAGER7'],"m1_name":e['MAN_NAME1'],
								"m2_name":e['MAN_NAME2'],"m3_name":e['MAN_NAME3'],"m4_name":e['MAN_NAME4'],"m5_name":e['MAN_NAME5'],
								"m6_name":e['MAN_NAME6'],"m7_name":e['MAN_NAME7'],"plant_id":e['ABKRS'],"dependents":b,"doj":doj_new,"ee_group":e['PERSG'],"birthday_flag":0}
						save_collection('Userinfo',array2)
						save_collection('Notificationtimestamp',{"username":e['PERNR'][3:],"created_time":datetime.datetime.now()})						
						total_count=len(result3['IT_WORK_SCHDL'])
						d=[]	
						for j in range(0,total_count):
							new_date=result3['IT_WORK_SCHDL'][j]['DATE'].strftime("%d/%m/%Y")
							n_date=datetime.datetime.strptime(new_date,"%d/%m/%Y")
							d+=[{"date":n_date,"worked_hours":str(result3['IT_WORK_SCHDL'][j]['WORKED_HOURS'])}]
						save_collection('WorkSchedule',{"employee_id":form['employee_id'],"schedule":d,"year":current_year})
						if result4['IT_P006']:
							a={}
							if len(result4['IT_P006']) == 3:
								save_collection('LeaveQuota',{"employee_id":form['employee_id'],"available_cls":int(float(str(result4['IT_P006'][1]['ANZHL']))),"leaves_taken":int(float(str(result4['IT_P006'][1]['KVERB']))),"available_sls":int(float(str(result4['IT_P006'][2]['ANZHL']))),"sls_taken":int(float(str(result4['IT_P006'][2]['KVERB']))),"available_els":int(float(str(result4['IT_P006'][0]['ANZHL']))),"els_taken":int(float(str(result4['IT_P006'][0]['KVERB']))),"lwps_taken":0,"tours_taken":0,"onduty_taken":0})
							elif len(result4['IT_P006']) == 2:
								for p in range(0,2):
									if result4['IT_P006'][p]['SUBTY']=='10':
										a["available_els"]=float(result4['IT_P006'][p]['ANZHL'])
										a["els_taken"]=float(result4['IT_P006'][p]['KVERB'])
									elif result4['IT_P006'][p]['SUBTY']=='20':
										a["available_cls"]=float(result4['IT_P006'][p]['ANZHL'])
										a["leaves_taken"]=float(result4['IT_P006'][p]['KVERB'])
									elif result4['IT_P006'][p]['SUBTY']=='50':
										a["available_sls"]=float(result4['IT_P006'][p]['ANZHL'])
										a["sls_taken"]=float(result4['IT_P006'][p]['KVERB'])	
								try:
									print a["available_els"]
								except:
									a["available_els"]=0
									a["els_taken"]=0
								try:
									print a["available_cls"]
								except:
									a["available_cls"]=0
									a["leaves_taken"]=0
								try:
									print a["available_sls"]
								except:
									a["available_sls"]=0
									a["sls_taken"]=0
								a["employee_id"]=form['employee_id']
								a["lwps_taken"]=0
								a["tours_taken"]=0
								a["onduty_taken"]=0
								save_collection('LeaveQuota',a)	
							elif len(result4['IT_P006']) == 1:
								if result4['IT_P006'][0]['SUBTY']=='10':
									a["available_els"]=float(result4['IT_P006'][0]['ANZHL'])
									a["els_taken"]=float(result4['IT_P006'][0]['KVERB'])
								elif result4['IT_P006'][0]['SUBTY']=='20':
									a["available_cls"]=float(result4['IT_P006'][0]['ANZHL'])
									a["leaves_taken"]=float(result4['IT_P006'][0]['KVERB'])
								elif result4['IT_P006'][0]['SUBTY']=='50':
									a["available_sls"]=float(result4['IT_P006'][0]['ANZHL'])
									a["sls_taken"]=float(result4['IT_P006'][0]['KVERB'])
								a["employee_id"]=form['employee_id']
								a["lwps_taken"]=0
								a["tours_taken"]=0
								a["onduty_taken"]=0
								try:
									print a["available_els"]
								except:
									a["available_els"]=0
									a["els_taken"]=0
								try:
									print a["available_cls"]
								except:
									a["available_cls"]=0
									a["leaves_taken"]=0
								try:
									print a["available_sls"]
								except:
									a["available_sls"]=0
									a["sls_taken"]=0
								save_collection('LeaveQuota',a)	
						chart=[]
						for i in range(1,13):
							chart+=[{"month":i,"CL":0,"SL":0,"LWP":0,"EL":0}]
						current_year=datetime.datetime.now().year
						save_collection('ChartData',{"employee_id":form['employee_id'],"year":current_year,"chartdata":chart})
						list_of_holidays=find_one_in_collection('Calendar',{"calendar_id":e['MOFID']})		
						for h in list_of_holidays['holidays']:
							d=int(h['date'].strftime('%j'))-1
							arry5={"schedule."+str(d)+".worked_hours":"0.00"}
							update_coll('WorkSchedule',{"employee_id":form['employee_id']},{'$set': arry5})
						check_position=find_one_in_collection('Positions',{"position_id":e['PLANS']})
						if check_position is None:
							levels=[]
							if e['MANAGER1'] != '':
								levels+=[e['MANAGER1']]
								if e['MANAGER2'] != '':
									levels+=[e['MANAGER2']]
									if e['MANAGER3'] != '':
										levels+=[e['MANAGER3']]
										if e['MANAGER4'] != '':
											levels+=[e['MANAGER4']]
											if e['MANAGER5'] != '':
												levels+=[e['MANAGER5']]
												if e['MANAGER6'] != '':
													levels+=[e['MANAGER6']]
													if e['MANAGER7'] != '':
														levels+=[e['MANAGER7']]
							array6 = {"position_id":e['PLANS'], "position_name":e['PLSTX'],"roles":["5487f2a957fbb310be5f9ab1"],"levels":levels}
							save_collection('Positions',array6)
						# User premission will add automatically
						check_user_premission=find_one_in_collection('userpermissions',{"username":form['employee_id']})
						if check_user_premission is None:
							print "userpermission check"
							array7={"username":form['employee_id'],"designation_id":e['PLANS'],"permissions":["546f2038850d2d1b00023b21","546f2071850d2d1b00023b24","546f2245850d2d1b00023b27","546f2257850d2d1b00023b28","546f2271850d2d1b00023b2a","546f22c5850d2d1b00023b30","5487ec6757fbb30f702709be","565305a2e56ed73aa892b4c0"],
									"ale":"546f2038850d2d1b00023b21","ps":"546f2245850d2d1b00023b27","f16":"546f2257850d2d1b00023b28","operationreportsmenu":"55d57d552df26e99beb1ec78"}
							save_collection('userpermissions',array7)
						flash('employee added successfully','alert-success')
						return redirect(url_for('zenuser.addnewemployee'))
					else:
						conn.close()
						close_sap_user_credentials(sap_login['_id'])
						flash('Employee is not available in SAP','alert-danger')
						return redirect(url_for('zenuser.addnewemployee'))
				except:
					close_sap_user_credentials(sap_login['_id'])
					flash('Unable to connect Sap user Credential','alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			elif form['submit'] == 'Block':
				chk=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if chk:
					update_collection('Userinfo',{"username":form['employee_id']},{"$set":{"employee_status":"inactive"}})
					msg=get_employee_name(form['employee_id'])+' blocked successfully'
					flash(msg,'alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					msg='No user exist with employee ID: '+form['employee_id']
					flash(msg,'alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			elif form['submit'] == 'Unblock':
				chk=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if chk:
					update_collection('Userinfo',{"username":form['employee_id']},{"$set":{"employee_status":"active"}})
					msg=get_employee_name(form['employee_id'])+' Unblocked successfully'
					flash(msg,'alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					msg='No user exist with employee ID: '+form['employee_id']
					flash(msg,'alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			elif form['submit'] == 'Reset Password':
				chk=find_one_in_collection('Userinfo',{"username":form['employee_id']})
				if chk:
					new_pwd=str(uuid.uuid4().get_hex().upper()[0:6])
					update_collection('Userinfo',{"username":form['employee_id']},{"$set":{"password":hashlib.sha1(new_pwd).hexdigest(),
						"chngpwd":"1"}})
					msg='Password generated successfully. New password: '+new_pwd
					flash(msg,'alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					msg='No user exist with employee ID: '+form['employee_id']
					flash(msg,'alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			elif form['submit'] == 'Include_In_Reports':
				chk=find_and_filter('Userinfo',{"username":form['employee_inactive']},{"_id":0,"username":1})
				if chk:
					for i in chk:
						leave_result=update_collection_multi('Leaves',{"employee_id":i['username']},{'$unset':{"employee_status":1}})
					flash('Employee Status has been updated sucessfully.Employee history Can be viewed now','alert-success')
					return redirect(url_for('zenuser.addnewemployee'))
				else:
					msg='User with employee ID : '+form['employee_inactive'] + " "+"Active"
					flash(msg,'alert-danger')
					return redirect(url_for('zenuser.addnewemployee'))
			elif form['submit'] == 'Remove_From_Reports':#making employeeinactive in leaves collection
				chk=find_and_filter('Userinfo',{"username":form['employee_inactive']},{"_id":0,"username":1})
				if chk:
					for i in chk:
						leave_result=update_collection_multi('Leaves',{"employee_id":i['username']},{'$set':{"employee_status":1}})
        			flash('Employee Status has been updated sucessfully for Leaves','alert-success')
        			return redirect(url_for('zenuser.addnewemployee'))
        		else:
        			msg='No user exist with employee ID: '+form['employee_inactive']
        			flash(msg,'alert-danger')
        			return redirect(url_for('zenuser.addnewemployee'))

		return render_template('addnewuser.html', user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))


@zen_user.route('/material', methods=['GET', 'POST'])
@login_required
def material():
	user_details=base()
	return render_template('material.html', user_details=user_details)

@zen_user.route('/getmaterialdetails', methods = ['GET', 'POST'])
@login_required
def getmaterialdetails():
	mat = request.form.get('mat')
	db = dbconnect()
	u_mat = db.Materials.aggregate([{'$match' : {"_id": ObjectId('547ef573850d2d44def5bb06')}},
		{ '$unwind': '$materials' },
                {'$match' : {'materials.MATNR' : mat}}, 
                {'$project' : {'materials.MATNR':1,"_id":0}}])
	u_mat = list(u_mat)
	if u_mat :
		sap_login=get_sap_user_credentials()
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_MATNR_DISP_STOCK', MATNR=mat)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		count = len(result['IT_FINAL'])
		mat_details=[]
		for i in range(0,count):
			array={"NAME1":str(result['IT_FINAL'][i]['NAME1']),"SPEME":str(result['IT_FINAL'][i]['SPEME']),
			"UMLME":str(result['IT_FINAL'][i]['UMLME']),"EINME":str(result['IT_FINAL'][i]['EINME']),
			"LABST":str(result['IT_FINAL'][i]['LABST']),"WINSM":str(result['IT_FINAL'][i]['WINSM']),
			"WSPEM":str(result['IT_FINAL'][i]['WSPEM']),"WLABS":str(result['IT_FINAL'][i]['WLABS']),
			"WEINM":str(result['IT_FINAL'][i]['WEINM']),"WRETM":str(result['IT_FINAL'][i]['WRETM']),
			"WUMLM":str(result['IT_FINAL'][i]['WUMLM']),"INSME":str(result['IT_FINAL'][i]['INSME']),
			"RETME":str(result['IT_FINAL'][i]['RETME']),"MATNR":str(result['IT_FINAL'][i]['MATNR'])}
			mat_details = mat_details+[array]
		array={"materials":mat_details,"flag":result['FLAG']}	
	else:
		array={"flag":'Y'}
	return jsonify(array)	

@zen_user.route('/getmateriallist', methods = ['GET', 'POST'])
@login_required
def getmateriallist():
	q = request.args.get('term')
	db=dbconnect()
	u_mat = db.Materials.aggregate([{'$match' : {"_id": ObjectId('547ef573850d2d44def5bb06')}},
		{ '$unwind': '$materials' },
                {'$match' : {'materials.MAKTX' : {'$regex': str(q),'$options': 'i' }}}, 
                {'$project' : {'materials.MATNR':1,'materials.MAKTX':1, "_id":0}}])
	u_mat = list(u_mat)
	if u_mat:
		m=[]
		for mat in u_mat:
			m=m+[mat['materials']]
	return json.dumps({"results":m})		

@zen_user.route('/trade/', methods = ['GET', 'POST'])
@login_required
def trade():
	user_details = base()
	if request.method == 'POST':
		form=request.form
		if form['order'] == 'PTQT':
			im_auart='ZTRD'
		elif form['order'] == 'PDTQ':
			im_auart='ZDTD'
		sap_login=get_sap_user_credentials()
		try:
			conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
			result = conn.call('Z_BAPI_TRADE_ORDER_CREATE',IM_QUOT_NO=form['quot_no'],IM_AUART=im_auart,IM_BSTKD=form['po_no'],IM_PAYCON=form['payment_terms'])
			conn.close()
			close_sap_user_credentials(sap_login['_id'])
			if result['IT_RETURN'][0]['TYPE'] == 'S':
				array={"order_type":form['order'],"sold_to_id":form['sold_to_id'],"sold_to_add":form['sold_to_add'],
			"ship_to_id":form['ship_to_id'],"ship_to_add":form['ship_to_add'],"po_no":form['po_no'],
			"inco_terms":form['inco_terms'],"material_code":form['material_code'],"quantity":form['quantity'],
			"plant":form['plant'],"route":form['route'],"payment_terms":form['payment_terms'],"sales_rep":form['sales_rep'],
			"destination":form['destination'],"quot_no":form['quot_no'],"zinv":form['zinv'],"zfrt":form['zfrt'],
			"zfr3":form['zfr3'],"jivp":form['jivp'],"jivc":form['jivc'],"jext":form['jext'],"ject":form['ject'],
			"jset":form['jset'],"zbas":form['zbas'],"netwr":form['netwr'],"order_no":result['EX_VBELN']}
				save_collection('TradeData',array)
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-success')
			elif result['IT_RETURN'][0]['TYPE'] == 'E':
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-danger')
		except:
			close_sap_user_credentials(sap_login['_id'])
			flash('Unable to connect Sap user Credential','alert-danger')
		return redirect(url_for('zenuser.trade'))
	return render_template('trade.html', user_details=user_details)

@zen_user.route('/nontrade/', methods = ['GET', 'POST'])
@login_required
def nontrade():
	user_details = base()
	if request.method == 'POST':
		form=request.form
		if form['order'] == 'PNQT':
			im_auart='ZNTD'
		elif form['order'] == 'PDNQ':
			im_auart='ZDNT'
		sap_login=get_sap_user_credentials()
		try:
			conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
			result = conn.call('Z_BAPI_NONTRADE_ORDER_CREATE',IM_QUOT_NO=form['quot_no'],IM_AUART=im_auart,IM_BSTKD=form['po_no'],IM_PAYCON=form['payment_terms'])
			conn.close()
			close_sap_user_credentials(sap_login['_id'])
			if result['IT_RETURN'][0]['TYPE'] == 'S':
				array={"order_type":form['order'],"sold_to_id":form['sold_to_id'],"sold_to_add":form['sold_to_add'],
			"ship_to_id":form['ship_to_id'],"ship_to_add":form['ship_to_add'],"po_no":form['po_no'],
			"inco_terms":form['inco_terms'],"material_code":form['material_code'],"quantity":form['quantity'],
			"plant":form['plant'],"route":form['route'],"payment_terms":form['payment_terms'],"sales_rep":form['sales_rep'],
			"destination":form['destination'],"quot_no":form['quot_no'],"zmnp":form['zmnp'],"zfrt":form['zfrt'],
			"zfr3":form['zfr3'],"jivp":form['jivp'],"jivc":form['jivc'],"jext":form['jext'],"ject":form['ject'],
			"jset":form['jset'],"zbas":form['zbas'],"zcom":form['zcom'],"ztri":form['ztri'],"zpdn":form['zpdn'],"netwr":form['netwr'],"order_no":result['EX_VBELN']}
				save_collection('NonTradeData',array)
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-success')
			elif result['IT_RETURN'][0]['TYPE'] == 'E':
				flash(result['IT_RETURN'][0]['MESSAGE'],'alert-danger')
		except:
			close_sap_user_credentials(sap_login['_id'])
			flash('Unable to connect Sap user Credential','alert-danger')
		return redirect(url_for('zenuser.nontrade'))
	return render_template('nontrade.html', user_details=user_details)
	
@zen_user.route('/getcustomerlist', methods = ['GET', 'POST'])
def getcustomerlist():
	q = request.args.get('term')
	db=dbconnect()
	u_cust = db.Customers.aggregate([
                {'$match' : { '$or':[{'NAME1' : {'$regex': str(q),'$options': 'i' }},{'KUNNR' : {'$regex': str(q),'$options': 'i' }}] }},
                {'$group':{"_id":{"NAME1":'$NAME1',"KUNNR":'$KUNNR'}}} 
                ])
	u_cust = list(u_cust)
	return json.dumps({"results":u_cust})

@zen_user.route('/getshiptopartylist', methods = ['GET', 'POST'])
def getshiptopartylist():
	q = str(request.form.get('deg'))
	ship_to_party=find_and_filter('Customers',{"KUNNR":q},{"_id":0,"KUNN2":1,'NAME1_WE':1,"LAND1":1,"LANDX":1,
                "STRAS":1,"TELF1":1,"PSTLZ":1,"PARVW":1,"REGIO":1,"ORT01":1,
                "BEZEI":1,"KUNN3":1})
	return jsonify({"results":ship_to_party})

@zen_user.route('/getshiptopartyadd', methods = ['GET', 'POST'])
def getshiptopartyadd():
	q = str(request.form.get('deg'))
	ship_to_party=find_and_filter('Customers',{"KUNN2":q},{"_id":0,"LAND1_WE":1,"LANDX_WE":1,
                "STRAS_WE":1,"TELF1_WE":1,"PSTLZ_WE":1,"PARVW_WE":1,"REGIO_WE":1,"ORT01_WE":1,
                "BEZEI_WE":1,"DESTINT":1})
	return jsonify(ship_to_party[0])

@zen_user.route('/getprice', methods = ['GET', 'POST'])
def getprice():
	form=request.form
	im_auart=form['order']
	im_kunnr=str(int(form['sold_to_id']))
	im_bstkd=form['po_no']
	im_inco1=form['inco_terms']
	im_matnr=form['material_code']
	im_kwmeng=str(form['quantity'])
	im_werks=form['plant']
	im_route=form['route']
	im_paycon=form['payment_terms']
	im_srep=str(int(form['sales_rep']))
	im_destint=form['destination']
	im_kunnr1=str(int(form['ship_to_id']))	 
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'],sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_BAPI_TRADE_ORDER_PRICING',IM_AUART=im_auart,IM_KUNNR=im_kunnr,IM_KUNNR1=im_kunnr1,IM_SREP=im_srep,IM_BSTKD=im_bstkd,IM_INCO1=im_inco1,IM_MATNR=im_matnr,IM_KWMENG=im_kwmeng,IM_WERKS=im_werks,IM_ROUTE=im_route,IM_PAYCON=im_paycon,IM_DESTINT=im_destint)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		if result['IT_RETURN'][0]['TYPE'] == 'S':
			a={"EX_QUOT_NO":str(result['EX_QUOT_NO']),"EX_ZINV":str(result['EX_ZINV']),"EX_ZFRT":str(result['EX_ZFRT']),
		"EX_ZFR3":str(result['EX_ZFR3']),"EX_JIVP":str(result['EX_JIVP']),"EX_JIVC":str(result['EX_JIVC']),
		"EX_JEXT":str(result['EX_JEXT']),"EX_JECT":str(result['EX_JECT']),"EX_JSET":str(result['EX_JSET']),
		"EX_ZBAS":str(result['EX_ZBAS']),"EX_NETWR":str(result['EX_NETWR']),"error":"no"}
		elif result['IT_RETURN'][0]['TYPE'] == 'E':
			a={"error":"yes","message":result['IT_RETURN'][0]['MESSAGE']}
	except:
		close_sap_user_credentials(sap_login['_id'])
		a={}
		a={"error":"yes","message":'Unable to connect Sap user Credential'}
	return jsonify(a)

@zen_user.route('/nontradegetprice', methods = ['GET', 'POST'])
def nontradegetprice():
	form=request.form
	im_auart=form['order']
	im_kunnr=str(int(form['sold_to_id']))
	im_bstkd=form['po_no']
	im_inco1=form['inco_terms']
	im_matnr=form['material_code']
	im_kwmeng=str(form['quantity'])
	im_werks=form['plant']
	im_route=form['route']
	im_paycon=form['payment_terms']
	im_srep=str(int(form['sales_rep']))
	im_destint=form['destination']
	im_kunnr1=str(int(form['ship_to_id']))
	im_partner_z2=form['comission_agent']
	im_zmnp=form['manual_price'] 
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_BAPI_NONTRADE_ORDER_PRICING',IM_AUART=im_auart,IM_KUNNR=im_kunnr,IM_KUNNR1=im_kunnr1,IM_PARTNER_Z1=im_srep,IM_BSTKD=im_bstkd,IM_INCO1=im_inco1,IM_MATNR=im_matnr,IM_KWMENG=im_kwmeng,IM_WERKS=im_werks,IM_ROUTE=im_route,IM_PAYCON=im_paycon,IM_DESTINT=im_destint,IM_PARTNER_Z2=im_partner_z2,IM_ZMNP=im_zmnp)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		if result['IT_RETURN'][0]['TYPE'] == 'S':
			a={"EX_QUOT_NO":str(result['EX_QUOT_NO']),"EX_ZFRT":str(result['EX_ZFRT']),
		"EX_ZFR3":str(result['EX_ZFR3']),"EX_JIVP":str(result['EX_JIVP']),"EX_JIVC":str(result['EX_JIVC']),
		"EX_JEXT":str(result['EX_JEXT']),"EX_JECT":str(result['EX_JECT']),"EX_JSET":str(result['EX_JSET']),
		"EX_ZBAS":str(result['EX_ZBAS']),"EX_NETWR":str(result['EX_NETWR']),"error":"no","EX_ZMNP":str(result['EX_ZMNP']),
		"EX_ZCOM":str(result['EX_ZCOM']),"EX_ZTRI":str(result['EX_ZTRI']),"EX_ZPDN":str(result['EX_ZPDN'])}
		elif result['IT_RETURN'][0]['TYPE'] == 'E':
			a={"error":"yes","message":result['IT_RETURN'][0]['MESSAGE']}
	except:
		close_sap_user_credentials(sap_login['_id'])
		a={}
		a={"error":"yes","message":'Unable to connect Sap user Credential'}
	return jsonify(a)
	
@zen_user.route('/getsdmaterialslist', methods = ['GET', 'POST'])
def getsdmaterialslist():
	q = request.args.get('term')
	db=dbconnect()
	u_mat = db.Sdmaterials.aggregate([
                {'$match' : { '$or':[{'MATNR' : {'$regex': str(q),'$options': 'i' }},{'MAKTX' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'MAKTX':1,'MATNR':1, "_id":0}}])
	u_mat = list(u_mat)
	return json.dumps({"results":u_mat})

@zen_user.route('/getsdplantslist', methods = ['GET', 'POST'])
def getsdplantslist():
	q = request.args.get('term')
	db=dbconnect()
	u_plant = db.Sdplants.aggregate([
                {'$match' : { '$or':[{'NAME1' : {'$regex': str(q),'$options': 'i' }},{'WERKS' : {'$regex': str(q),'$options': 'i' }}] }}, 
                {'$project' : {'NAME1':1,'WERKS':1, "_id":0}}])
	u_plant = list(u_plant)
	return json.dumps({"results":u_plant})


# Moved on 10_june_2016 for Notication Scrolling Functionality in DashBoard
@zen_user.route('/announcement/', methods = ['GET', 'POST'])
@login_required
def announcement():
	p =check_user_role_permissions('54ab5b94507c00c54ba22297')
	if p:
		user_details = base()
		if request.method == 'POST':
			form=request.form
			if form['submit']=="submit":
				uid = current_user.username
				array={"announcement":form['announcement'],"plant_id":get_plant_id(uid),"created_time": datetime.datetime.now(),"view":1}
				save_collection('Announcements',array)
				flash('Announcement created successfully','alert-success')
				return redirect(url_for('zenuser.announcement'))
			if form['submit']=="delete":
				hide_msg=update_collection('Announcements',{"_id":ObjectId(form['announcementid'])},{'$set':{"view":0}})
				flash('Announcement Deleted successfully','alert-success')
				return redirect(url_for('zenuser.announcement'))
			if form['submit']=="view":
				hide_msg=update_collection('Announcements',{"_id":ObjectId(form['announcementid'])},{'$set':{"view":1}})
				flash('Announcement Added successfully','alert-success')
				return redirect(url_for('zenuser.announcement'))
		all_announce=find_in_collection('Announcements',{"plant_id":get_plant_id(current_user.username)})
		return render_template('announcements.html', user_details=user_details,announcements=all_announce)
	else:
		return redirect(url_for('zenuser.index'))

@zen_user.route('/help/', methods = ['GET', 'POST'])
@login_required
def help():
	user_details = base()
	return render_template('help.html', user_details=user_details)
		

@zen_user.route('/preports/', methods = ['GET', 'POST'])
@login_required
def preports():
	user_details = base()
	return render_template('pending_order_reports.html', user_details=user_details)

@zen_user.route('/getpendingreports', methods = ['GET', 'POST'])
def getpendingreports():
	form=request.form
	employees=get_employees_list()
	today=datetime.datetime.now()
	tday=today.strftime('%d.%m.%Y')
	yesterday=today-datetime.timedelta(days=1)
	yday=yesterday.strftime('%d.%m.%Y')
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		if form['select_date'] == '1':
			result = conn.call('Z_BAPI_PENDING_ORD_REPORT',IM_YDAY=tday,IM_TDAY=tday,IT_PERNR=employees)
		elif form['select_date'] == '2':
			result = conn.call('Z_BAPI_PENDING_ORD_REPORT',IM_YDAY=yday,IM_TDAY=yday,IT_PERNR=employees)
		else:
			result = conn.call('Z_BAPI_PENDING_ORD_REPORT',IM_YDAY=yday,IM_TDAY=tday,IT_PERNR=employees)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['KBETR']=str(result['IT_FINAL'][i]['KBETR'])
				result['IT_FINAL'][i]['BKWMENG']=str(result['IT_FINAL'][i]['BKWMENG'])
				result['IT_FINAL'][i]['OKWMENG']=str(result['IT_FINAL'][i]['OKWMENG'])
				result['IT_FINAL'][i]['KBETR1']=str(result['IT_FINAL'][i]['KBETR1'])
				result['IT_FINAL'][i]['DKWMENG']=str(result['IT_FINAL'][i]['DKWMENG'])
				result['IT_FINAL'][i]['ERDAT']=result['IT_FINAL'][i]['ERDAT'].strftime('%d-%m-%Y')
			data['reports']=result['IT_FINAL']
	except:
		close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/creports/', methods = ['GET', 'POST'])
@login_required
def creports():
	user_details = base()
	return render_template('collecting_reports.html', user_details=user_details)

@zen_user.route('/getcollectingreports', methods = ['GET', 'POST'])
def getcollectingreports():
	form=request.form
	employees=get_employees_list()
	today=datetime.datetime.now()
	tday=today.strftime('%d.%m.%Y')
	yesterday=today-datetime.timedelta(days=1)
	yday=yesterday.strftime('%d.%m.%Y')
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		if form['select_date'] == '1':
			result = conn.call('Z_BAPI_COLLECTION_REPORT',IM_YDAY='25.02.2015',IM_TDAY='25.02.2015',IT_PERNR=employees)
		else:
			result = conn.call('Z_BAPI_COLLECTION_REPORT',IM_YDAY=yday,IM_TDAY=yday,IT_PERNR=employees)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['DMBTR']=str(result['IT_FINAL'][i]['DMBTR'])
				result['IT_FINAL'][i]['CPUDT']=result['IT_FINAL'][i]['CPUDT'].strftime('%d-%m-%Y')
				result['IT_FINAL'][i]['INSDATE']=result['IT_FINAL'][i]['INSDATE'].strftime('%d-%m-%Y')
				result['IT_FINAL'][i]['BUDAT']=result['IT_FINAL'][i]['BUDAT'].strftime('%d-%m-%Y')
			data['data']=result['IT_FINAL']
	except:
		close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/sreports/', methods = ['GET', 'POST'])
@login_required
def sreports():
	user_details = base()
	return render_template('sales_reports.html', user_details=user_details)

@zen_user.route('/getsalesreports', methods = ['GET', 'POST'])
def getsalesreports():
	form=request.form
	employees=get_employees_list()
	today=datetime.datetime.now()
	tday=today.strftime('%d.%m.%Y')
	yesterday=today-datetime.timedelta(days=1)
	yday=yesterday.strftime('%d.%m.%Y')
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		if form['select_date'] == '1':
			result = conn.call('Z_BAPI_DISPATCH_REPORT',IM_YDAY=tday,IM_TDAY=tday,IM_RTYPE=form['select_type'],IT_PERNR=employees)
		elif form['select_date'] == '2':
			result = conn.call('Z_BAPI_DISPATCH_REPORT',IM_YDAY=yday,IM_TDAY=yday,IM_RTYPE=form['select_type'],IT_PERNR=employees)
		else:
			result = conn.call('Z_BAPI_DISPATCH_REPORT',IM_YDAY=yday,IM_TDAY=tday,IM_RTYPE=form['select_type'],IT_PERNR=employees)
		conn.close()
		# close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['ZINV']=str(result['IT_FINAL'][i]['ZINV'])
				result['IT_FINAL'][i]['FKIMG']=str(result['IT_FINAL'][i]['FKIMG'])
				result['IT_FINAL'][i]['ERDAT']=result['IT_FINAL'][i]['ERDAT'].strftime('%d-%m-%Y')
				result['IT_FINAL'][i]['FKDAT']=result['IT_FINAL'][i]['FKDAT'].strftime('%d-%m-%Y')
			data['reports']=result['IT_FINAL']
	except:
		# close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/areports/', methods = ['GET', 'POST'])
@login_required
def areports():
	user_details = base()
	return render_template('aging_reports.html', user_details=user_details)

@zen_user.route('/getagingreports', methods = ['GET', 'POST'])
def getagingreports():
	form=request.form
	employees=get_employees_list()
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'], gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		result = conn.call('Z_BAPI_AGEING_REPORT',IM_BUDAT=form['end_date'],IT_PERNR=employees)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		data={}
		if result['IT_RETURN']:
			data['error']= 'yes'
			data['msg']=result['IT_RETURN'][0]['MESSAGE']
		else:
			data['error'] = 'no'
			no_of_reports=len(result['IT_FINAL'])
			for i in range(0,no_of_reports):
				result['IT_FINAL'][i]['ZCLOSE']=str(result['IT_FINAL'][i]['ZCLOSE'])
				result['IT_FINAL'][i]['SLABAMT8']=str(result['IT_FINAL'][i]['SLABAMT8'])
				result['IT_FINAL'][i]['SLABAMT9']=str(result['IT_FINAL'][i]['SLABAMT9'])
				result['IT_FINAL'][i]['SLABAMT2']=str(result['IT_FINAL'][i]['SLABAMT2'])
				result['IT_FINAL'][i]['SLABAMT3']=str(result['IT_FINAL'][i]['SLABAMT3'])
				result['IT_FINAL'][i]['SLABAMT1']=str(result['IT_FINAL'][i]['SLABAMT1'])
				result['IT_FINAL'][i]['SLABAMT6']=str(result['IT_FINAL'][i]['SLABAMT6'])
				result['IT_FINAL'][i]['SLABAMT7']=str(result['IT_FINAL'][i]['SLABAMT7'])
				result['IT_FINAL'][i]['SLABAMT4']=str(result['IT_FINAL'][i]['SLABAMT4'])
				result['IT_FINAL'][i]['SLABAMT5']=str(result['IT_FINAL'][i]['SLABAMT5'])
				result['IT_FINAL'][i]['ADDLDEPOSIT']=str(result['IT_FINAL'][i]['ADDLDEPOSIT'])
				result['IT_FINAL'][i]['KLIMK']=str(result['IT_FINAL'][i]['KLIMK'])
				result['IT_FINAL'][i]['DEPOSIT']=str(result['IT_FINAL'][i]['DEPOSIT'])
			data['reports']=result['IT_FINAL']
	except:
		close_sap_user_credentials(sap_login['_id'])
		data={}
		data['error']= 'yes'
		data['msg']='Unable to connect Sap user Credential'
	return jsonify(data)

@zen_user.route('/selecthr/', methods = ['GET', 'POST'])
@login_required
def selecthr():
	user_details = base()
	if request.method == 'POST':
		form=request.form
		update_collection('Plants',{"plant_id":form['plant']},{"$set":{"plant_current_hr":form['hr']}})
		flash('HR updated successfully','alert-success')
		return redirect(url_for('zenuser.selecthr'))
	return render_template('select_hr.html', user_details=user_details)

@zen_user.route('/choosehr', methods = ['GET', 'POST'])
@login_required
def choosehr():
	deg = str(request.form.get('deg'))
	plant=find_one_in_collection('Plants',{"plant_id":deg})
	e_name=get_employee_name_given_pid(plant['plant_current_hr'])
	if deg == 'HY':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,'$or':[{"department_id":"50005157"},{"department_id":"50095158"}]},{"designation_id":1,"_id":0})
	elif deg == 'TC':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095181"},{"designation_id":1,"_id":0})
	elif deg == 'BP':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095192"},{"designation_id":1,"_id":0})
	elif deg == 'GC':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095183"},{"designation_id":1,"_id":0})
	elif deg == 'TA':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095182"},{"designation_id":1,"_id":0})
	elif deg == 'GP':
		hr_pids=find_and_filter('Userinfo',{"plant_id":deg,"department_id":"50095184"},{"designation_id":1,"_id":0})
	return jsonify({"current_hr":e_name,"hr_pids":hr_pids})

#dump html files for next project

@zen_user.route('/materiallist/', methods=['GET', 'POST'])
@login_required
def materiallist():
	user_details = base()
	return render_template('material_list.html', user_details=user_details)

@zen_user.route('/prstatus/', methods=['GET', 'POST'])
@login_required
def prstatus():
	user_details = base()
	return render_template('pr_status.html', user_details=user_details)

@zen_user.route('/consumption/', methods=['GET', 'POST'])
@login_required
def consumption():
	user_details = base()
	return render_template('consumption.html', user_details=user_details)

@zen_user.route('/consumption_RM/', methods=['GET', 'POST'])
@login_required
def consumption_RM():
	user_details = base()
	return render_template('consumption_RM.html', user_details=user_details)

@zen_user.route('/consumption_emp/', methods=['GET', 'POST'])
@login_required
def consumption_emp():
	user_details = base()
	return render_template('consumption_emp.html', user_details=user_details)

@zen_user.route('/comparision/', methods=['GET', 'POST'])
@login_required
def comparision():
	user_details = base()
	return render_template('comparision.html', user_details=user_details)

@zen_user.route('/materialissue/', methods=['GET', 'POST'])
@login_required
def materialissue():
	user_details = base()
	return render_template('material_issue.html', user_details=user_details)

@zen_user.route('/material_miv_report/', methods=['GET', 'POST'])
@login_required
def material_miv_report():
	user_details = base()
	return render_template('material_miv_report.html', user_details=user_details)

@zen_user.route('/material_miv_rpt/', methods=['GET', 'POST'])
@login_required
def material_miv_rpt():
	user_details = base()
	return render_template('material_miv_rpt.html', user_details=user_details)

@zen_user.route('/purchasecreation/', methods=['GET', 'POST'])
@login_required
def purchasecreation():
	user_details = base()
	return render_template('purchasecreation.html', user_details=user_details)

@zen_user.route('/purchase_pm_order/', methods=['GET', 'POST'])
@login_required
def purchase_pm_order():
	user_details = base()
	return render_template('purchase_pm_order.html', user_details=user_details)
						
@zen_user.route('/purchase_cost_order/', methods=['GET', 'POST'])
@login_required
def purchase_cost_order():
	user_details = base()
	return render_template('purchase_cost_order.html', user_details=user_details)

@zen_user.route('/purchase_project_network/', methods=['GET', 'POST'])
@login_required
def purchase_project_network():
	user_details = base()
	return render_template('purchase_project_network.html', user_details=user_details)
						
@zen_user.route('/purchase_asset/', methods=['GET', 'POST'])
@login_required
def purchase_asset():
	user_details = base()
	return render_template('purchase_asset.html', user_details=user_details)

@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
        #year_of_proposal=str(year_of_proposal))
    
    return Response(thing, mimetype='application/pdf')

# Below Lines of Code is Written by Sairam For Kaizen and IMS Modules

#This Method is Usefull to Upload Kaizen Documents to My Portal Database
@zen_user.route('/file_upload/', methods = ['GET', 'POST'])
@login_required
def file_upload():
	user_details = base()
	found=None
	#print user_details['username']
	#print user_details['plant_id']
	if request.method == 'POST':
		form=request.form
		uid = current_user.username #current employee id
		if form['plant_id'] != "":
			plant_id=form['plant_id']
		else:
			plant_id=user_details['plant_id']
		upload_date= datetime.datetime.strptime(form['upload_date'],"%d/%m/%Y")
		
		val=len(request.files)
		
		
		for i in range(1,val+1):
			if request.files['file'+str(i)]:
				yr= upload_date.year
				month= upload_date.month
				input_file=request.files['file'+str(i)]
				# plant_id=user_details['plant_id']
				new_file_name= form['text'+str(i)]
				f_name=str(new_file_name)+'.'+"pdf"
				kaizen_docs=find_in_collection('fs.files',{"filename":str(f_name),"month_of_proposal": str(month) ,"year_of_proposal" : str(yr),"tag" : "kaizen_documents","plant_id" : str(plant_id),"uid":uid})
				if kaizen_docs:
					found=kaizen_docs
					flash("File name"+""+str(f_name)+"already exists Please change file name and upload again",'alert-danger')
				
			
		if found:
			return render_template('Kaizen_Upload.html', user_details=user_details)

		else:

			for i in range(1,val+1):
				if request.files['file'+str(i)]:
					yr= upload_date.year
					month= upload_date.month
					input_file=request.files['file'+str(i)]
					# plant_id=user_details['plant_id']
					new_file_name= form['text'+str(i)]
					f_name=str(new_file_name)+'.'+"pdf"
					save_file_gridfs(input_file,"application/pdf","kaizen_documents",
						new_file_name,yr,month,plant_id,uid)

				else:
					flash('Sorry Unable to Uplaod the Documents try again','alert-danger')
					return render_template('Kaizen_Upload.html', user_details=user_details)

					
				
			flash("Sucessfully Uploaded the Kaizen Documents",'alert-success')
					
		
	return render_template('Kaizen_Upload.html', user_details=user_details)
	

#saving pdf in databse
def save_file_gridfs(file_data,ctype,ctag,input_file_name,year_of_proposal,month,plant,uid):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username #current employee id
    file_ext = file_data.filename.rsplit('.', 1)[1]
    
    fs.put(file_data.read(), content_type=ctype, filename = str(input_file_name)+'.'+file_ext, plant_id = plant,
        tag = ctag,month_of_proposal=str(month),year_of_proposal= str(year_of_proposal),uid=uid)

#This Method is Usefull to View Kaizen Documents from MyPortal Database
@zen_user.route('/file_view/', methods = ['GET', 'POST'])
@login_required
def file_view():
	user_details = base()
	if request.method == 'POST':
		form=request.form
		if str(form['month']) == str(1):
			new_month="January"

		elif str(form['month']) == str(2):
			new_month="February"

		elif str(form['month']) == str(3):
			new_month="March"

		elif str(form['month']) == str(4):
			new_month="April"

		elif str(form['month']) == str(5):
			new_month="May"

		elif str(form['month']) == str(6):
			new_month="June"

		elif str(form['month']) == str(7):
			new_month="July"

		elif str(form['month']) == str(8):
			new_month="August"

		elif str(form['month']) == str(9):
			new_month="September"

		elif str(form['month']) == str(10):
			new_month="October"

		elif str(form['month']) == str(11):
			new_month="November"

		elif str(form['month']) == str(12):
			new_month="December"
		
		else:
			new_month=""

		if new_month=="":
			message = "Below are the Documents of Year"+" "+" "+str(form['year'])
			kaizen_docs=find_in_collection('fs.files',{"year_of_proposal" : str(form['year']),"tag" : "kaizen_documents",
				"plant_id" : str(form['plant_id'])})
		else:
			message = "Below are the Documents of Month"+" "+" "+new_month+" " +"of Year"+" "+" "+str(form['year'])
			kaizen_docs=find_in_collection('fs.files',{"year_of_proposal" : str(form['year']),
				"month_of_proposal" : str(form['month']),"tag" : "kaizen_documents",
				"plant_id" : str(form['plant_id'])})
		return render_template('Kaizen_Documents_View.html', user_details=user_details,
			kaizen_documents=kaizen_docs,Message=message)

	return render_template('Kaizen_Documents_Search.html', user_details=user_details) 

 
#this Code is Usefull to Uplaod View and Delete the Standard IMS Documents
@zen_user.route('/standards_imsupload/', methods = ['GET', 'POST'])
@login_required
def standard_upload():
	user_details = base()
	found =None
	upload_documents= find_in_collection('fs.files',{'delete_flag':0,"$or": [ { "tag":"ISO_9001"}, { "tag": "ISO_14001" },{ "tag": "OHSAS_18001"} ] } )
	if request.method == 'POST':
		form=request.form
		if form['submit']=="Upload":
			employee_id=user_details['username']
			plant_id=user_details['plant_id']
			if request.files['file1']:
				ims_docs=find_in_collection('fs.files',{"tag" : request.form['tag_name'],"delete_flag":0})
				if ims_docs:
					found=ims_docs
					
			if found:
				upload_documents= find_in_collection('fs.files',{'delete_flag':0,"$or": [ { "tag":"ISO_9001"}, { "tag": "ISO_14001" },{ "tag": "OHSAS_18001"} ] } )
				flash("Document"+"  "+str(request.form['tag_name'])+ " "+"already exists Please Delete it to Upload Again",'alert-danger')
				return render_template('IMS_STANDARD_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
			else:
				if request.files['file1']:
					tag_name=request.form['tag_name']
					input_file = request.files['file1']
					file_name=input_file.filename
					upload_date= datetime.datetime.now()
					plant_id=user_details['plant_id']
					user_id = current_user.username #current employee id
					file_save_gridfss(input_file,"application/pdf",tag_name,file_name,upload_date,plant_id,user_id)
					upload_documents= find_in_collection('fs.files',{'delete_flag':0,"$or": [ { "tag":"ISO_9001"}, { "tag": "ISO_14001" },{ "tag": "OHSAS_18001"} ] } )
			 		flash("Sucessfully Uploaded the"+ "  "+tag_name +" "+"Document",'alert-success')
			 		return render_template('IMS_STANDARD_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
							
		else:
			ims_docs={"tag" : form['tag_name'],'delete_flag':0}
			ims_docs1= {"$set":{"deleted_date":datetime.datetime.now(),"delete_flag":1,"deleted_by":current_user.username}}
	 		update_collection('fs.files',ims_docs,ims_docs1)
	 		upload_documents= find_in_collection('fs.files',{'delete_flag':0,"$or": [ { "tag":"ISO_9001"}, { "tag": "ISO_14001" },{ "tag": "OHSAS_18001"} ] } )
	 		flash("Sucessfully Deletd the"+ "  "+form['tag_name']+" "+"Document",'alert-success')
	 		return render_template('IMS_STANDARD_UPLOAD.html', user_details=user_details,all_docs=upload_documents) 
	
	return render_template('IMS_STANDARD_UPLOAD.html', user_details=user_details,all_docs=upload_documents) 

@zen_user.route('/standards_imsview/', methods = ['GET', 'POST'])
@login_required
def standard_View():
	user_details = base()
	upload_documents= find_in_collection('fs.files',{'delete_flag':0,"$or": [ { "tag":"ISO_9001"}, { "tag": "ISO_14001" },{ "tag": "OHSAS_18001"} ] } )

	return render_template('IMS_STANDARD_VIEW.html', user_details=user_details,all_docs=upload_documents) 

#this Code is Usefull to Uplaod View and Delete the Ploicy IMS Documents
@zen_user.route('/policy_imsupload/', methods = ['GET', 'POST'])
@login_required
def policy_upload():
	user_details = base()
	found =None
	upload_documents=find_in_collection('fs.files',{"tag" : "IMS_Policy",'delete_flag':0})
	if request.method == 'POST':
		form=request.form
		if form['submit']=="Upload":
			employee_id=user_details['username']
			plant_id=user_details['plant_id']
			tag_name="IMS_Policy"
			if request.files['file1']:
				ims_docs=find_in_collection('fs.files',{"tag" : "IMS_Policy","delete_flag":0})
				if ims_docs:
					found=ims_docs
					
			if found:
				upload_documents=find_in_collection('fs.files',{"tag" : "IMS_Policy",'delete_flag':0})
				flash("Document"+"  "+tag_name+ " "+"already exists Please Delete it to Upload Again",'alert-danger')
				return render_template('IMS_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
			else:
			
				if request.files['file1']:
					tag_name="IMS_Policy"
					input_file = request.files['file1']
					file_name=input_file.filename
					upload_date= datetime.datetime.now()
					plant_id=user_details['plant_id']
					user_id = current_user.username #current employee id
					file_save_gridfss(input_file,"application/pdf",tag_name,file_name,upload_date,plant_id,user_id)
					upload_documents=find_in_collection('fs.files',{"tag" : "IMS_Policy",'delete_flag':0})
			 		flash("Sucessfully Uploaded the"+ "  "+tag_name +" "+"Document",'alert-success')
			 		return render_template('IMS_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
		else:
			ims_docs={"tag" : form['tag_name'],'delete_flag':0}
			ims_docs1= {"$set":{"deleted_date":datetime.datetime.now(),"delete_flag":1,"deleted_by":current_user.username}}
	 		update_collection('fs.files',ims_docs,ims_docs1)
	 		upload_documents=find_in_collection('fs.files',{"tag" : "IMS_Policy",'delete_flag':0})
			flash("Sucessfully Deleted the"+ "  "+form['tag_name']+" "+"Document",'alert-success')
			return render_template('IMS_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)


	return render_template('IMS_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)

@zen_user.route('/policy_imsview/', methods = ['GET', 'POST'])
@login_required
def policy_view():
	user_details = base()
	upload_documents=find_in_collection('fs.files',{"tag" : "IMS_Policy",'delete_flag':0})
	return render_template('IMS_POLICY_VIEW.html', user_details=user_details,all_docs=upload_documents)
#this Code is Usefull to Uplaod View and Delete the L1 System Manual IMS Documents
@zen_user.route('/l1_imsupload/', methods = ['GET', 'POST'])
@login_required
def l1_upload():
	user_details = base()
	found =None
	upload_documents=find_in_collection('fs.files',{"tag" : "L1_System_Manual",'delete_flag':0})
	if request.method == 'POST':
		form=request.form
		if form['submit']=="Upload":
			employee_id=user_details['username']
			plant_id=user_details['plant_id']
			tag_name="L1_System_Manual"
			if request.files['file1']:
				ims_docs=find_in_collection('fs.files',{"tag" : "L1_System_Manual","delete_flag":0})
				if ims_docs:
					found=ims_docs	
			if found:
				upload_documents=find_in_collection('fs.files',{"tag" : "L1_System_Manual",'delete_flag':0})
				flash("Document"+"  "+tag_name+ " "+"already exists Please Delete it to Upload Again",'alert-danger')
				return render_template('IMS_L1_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
			else:

				if request.files['file1']:
					tag_name="L1_System_Manual"
					input_file = request.files['file1']
					file_name=input_file.filename
					upload_date= datetime.datetime.now()
					plant_id=user_details['plant_id']
					user_id = current_user.username #current employee id
					file_save_gridfss(input_file,"application/pdf",tag_name,file_name,upload_date,plant_id,user_id)
					upload_documents=find_in_collection('fs.files',{"tag" : "L1_System_Manual",'delete_flag':0})
			 		flash("Sucessfully Uploaded the"+ "  "+tag_name +" "+"Document",'alert-success')
			 		return render_template('IMS_L1_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
		else:
			ims_docs={"tag" : form['tag_name'],'delete_flag':0}
			ims_docs1= {"$set":{"deleted_date":datetime.datetime.now(),"delete_flag":1,"deleted_by":current_user.username}}
	 		update_collection('fs.files',ims_docs,ims_docs1)
	 		upload_documents=find_in_collection('fs.files',{"tag" : "L1_System_Manual",'delete_flag':0})
			flash("Sucessfully Deleted the"+ "  "+form['tag_name']+" "+"Document",'alert-success')
			return render_template('IMS_L1_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
				
	return render_template('IMS_L1_UPLOAD.html', user_details=user_details,all_docs=upload_documents)

@zen_user.route('/l1_imsview/', methods = ['GET', 'POST'])
@login_required
def l1_view():
	user_details = base()
	upload_documents=find_in_collection('fs.files',{"tag" : "L1_System_Manual",'delete_flag':0})

	return render_template('IMS_L1_VIEW.html', user_details=user_details,all_docs=upload_documents)

#this Code is Usefull to Uplaod View and Delete the L2 Procedure Manual IMS Documents
@zen_user.route('/l2_imsupload/', methods = ['GET', 'POST'])
@login_required
def l2_upload():
	user_details = base()
	found =None
	upload_documents=find_in_collection('fs.files',{"tag" : "L2_Procedure_Manual","delete_flag":0})
	if request.method == 'POST':
		form=request.form
		if form['submit']=="Upload":
			employee_id=user_details['username']
			plant_id=user_details['plant_id']
			tag_name="L2_Procedure_Manual"
			val=len(request.files)
			for i in range(1,val+1):
				if request.files['file'+str(i)]:
					input_file=request.files['file'+str(i)]
					file_name=input_file.filename
					ims_docs=find_in_collection('fs.files',{"tag" : "L2_Procedure_Manual",'delete_flag':0,
						"plant_id":str(request.form['plant_id']),'filename':str(file_name)})
					if ims_docs:
						found=ims_docs
						flash("Document with "+"  "+file_name+ " "+"already exists Please Delete it to Upload Again",'alert-danger')
					
			if found:
				return render_template('IMS_L2_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
			else:
				for i in range(1,val+1):
					if request.files['file'+str(i)]:
						tag_name="L2_Procedure_Manual"
						input_file=request.files['file'+str(i)]
						file_name=input_file.filename
						upload_date= datetime.datetime.now()
						plant_id=request.form['plant_id']
						user_id = current_user.username #current employee id
						file_save_gridfss(input_file,"application/pdf",tag_name,file_name,upload_date,plant_id,user_id)
				
				upload_documents=find_in_collection('fs.files',{"tag" : "L2_Procedure_Manual","delete_flag":0})
				flash("Sucessfully Uploaded the"+ "  "+tag_name +" "+"Documents",'alert-success')
				return render_template('IMS_L2_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
		else:
			ims_docs={"tag" : form['tag_name'],'delete_flag':0,'plant_id':form['plant_id'],'filename':form['filename']}
			ims_docs1= {"$set":{"deleted_date":datetime.datetime.now(),"delete_flag":1,"deleted_by":current_user.username}}
	 		update_collection('fs.files',ims_docs,ims_docs1)
			upload_documents=find_in_collection('fs.files',{"tag" : "L2_Procedure_Manual","delete_flag":0})
			flash("Sucessfully Deleted the"+ "  "+form['filename']+" "+"Document",'alert-success')
			return render_template('IMS_L2_UPLOAD.html', user_details=user_details,all_docs=upload_documents)


	return render_template('IMS_L2_UPLOAD.html', user_details=user_details,all_docs=upload_documents)

@zen_user.route('/l2_imsview/', methods = ['GET', 'POST'])
@login_required
def l2_view():
	user_details = base()
	all_plants = find_and_filter('Plants',{},{'plant_id':1,'plant_desc':1,'_id':0})
	if request.method == 'POST':
		form=request.form
		plant_id=form['plant_id']
		upload_documents=find_in_collection('fs.files',{"tag" : "L2_Procedure_Manual","delete_flag":0,'plant_id':plant_id})
		return render_template('IMS_L2_VIEW.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)
	return render_template('IMS_L2_VIEW.html', user_details=user_details,all_plants=all_plants)




def file_save_gridfss(file_data,ctype,tag_name,file_name,upload_date,plant_id,user_id):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    fs.put(file_data.read(), content_type=ctype,tag=tag_name,filename = str(file_name),upload_date=upload_date,plant_id = plant_id,user_id=user_id,delete_flag=0)


    #this Code is Usefull to Uplaod View and Delete the L2 Procedure Manual IMS Documents
@zen_user.route('/l3_imsupload/', methods = ['GET', 'POST'])
@login_required
def l3_upload():
	user_details = base()
	all_plants = find_and_filter('Plants',{},{'plant_id':1,'plant_desc':1,'_id':0})
	found =None
	upload_documents=find_in_collection('fs.files',{"tag" : "L3_Work_Instructions","delete_flag":0})
	if request.method == 'POST':
		form=request.form
		if form['submit']=="Upload":
			dept=find_one_in_collection("departmentmaster",{'dept_pid':form['dept_id']})
			employee_id=user_details['username']
			plant_id=form['plant_id']
			tag_name="L3_Work_Instructions"
			dept_id=dept['dept_pid']
			dept_name=dept['dept']
			plant_name=dept['plant_desc']
			val=len(request.files)
			for i in range(1,val+1):
				if request.files['file'+str(i)]:
					input_file=request.files['file'+str(i)]
					file_name=input_file.filename
					ims_docs=find_in_collection('fs.files',{"tag" : "L3_Work_Instructions",'delete_flag':0,
						"plant_id":str(plant_id),'filename':str(file_name),'plant_id':plant_id,'dept_id':dept['dept_pid'],'dept_name':dept['dept']})
					if ims_docs:
						found=ims_docs
						flash("Document with "+"  "+file_name+ " "+"already exists Please Delete it to Upload Again",'alert-danger')
					
			if found:
				return render_template('IMS_L3_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)
			else:
				for i in range(1,val+1):
					if request.files['file'+str(i)]:
						tag_name="L3_Work_Instructions"
						input_file=request.files['file'+str(i)]
						file_name=input_file.filename
						upload_date= datetime.datetime.now()
						user_id = current_user.username #current employee id
						file_save_gridfs_l3(input_file,"application/pdf/doc/docx/xlsm/xlsx",tag_name,file_name,upload_date,plant_id,user_id,
							dept_id,dept_name,plant_name)
				
				upload_documents=find_in_collection('fs.files',{"tag" : "L3_Work_Instructions","delete_flag":0})
				flash("Sucessfully Uploaded the"+ "  "+tag_name +" "+"Documents",'alert-success')
				return render_template('IMS_L3_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)
		else:

			ims_docs={"tag" : form['tag_name'],'delete_flag':0,'plant_id':form['plant_id'],'filename':form['filename']}
			ims_docs1= {"$set":{"deleted_date":datetime.datetime.now(),"delete_flag":1,"deleted_by":current_user.username}}
	 		update_collection('fs.files',ims_docs,ims_docs1)
			upload_documents=find_in_collection('fs.files',{"tag" : "L3_Work_Instructions","delete_flag":0})
			flash("Sucessfully Deleted the"+ "  "+form['filename']+" "+"Document",'alert-success')
			return render_template('IMS_L3_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)


	return render_template('IMS_L3_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)


@zen_user.route('/l4_imsupload/', methods = ['GET', 'POST'])
@login_required
def l4_upload():
	user_details = base()
	all_plants = find_and_filter('Plants',{},{'plant_id':1,'plant_desc':1,'_id':0})
	found =None
	upload_documents=find_in_collection('fs.files',{"tag" : "L4_Documents","delete_flag":0})
	if request.method == 'POST':
		form=request.form
		if form['submit']=="Upload":
			dept=find_one_in_collection("departmentmaster",{'dept_pid':form['dept_id']})
			employee_id=user_details['username']
			plant_id=form['plant_id']
			tag_name="L4_Documents"
			dept_id=dept['dept_pid']
			dept_name=dept['dept']
			plant_name=dept['plant_desc']
			val=len(request.files)
			for i in range(1,val+1):
				if request.files['file'+str(i)]:
					input_file=request.files['file'+str(i)]
					file_name=input_file.filename
					ims_docs=find_in_collection('fs.files',{"tag" : "L4_Documents",'delete_flag':0,
						"plant_id":str(plant_id),'filename':str(file_name),'plant_id':plant_id,'dept_id':dept['dept_pid'],'dept_name':dept['dept']})
					if ims_docs:
						found=ims_docs
						flash("Document with "+"  "+file_name+ " "+"already exists Please Delete it to Upload Again",'alert-danger')
					
			if found:
				return render_template('IMS_L4_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)
			else:
				for i in range(1,val+1):
					if request.files['file'+str(i)]:
						tag_name="L4_Documents"
						input_file=request.files['file'+str(i)]
						file_name=input_file.filename
						upload_date= datetime.datetime.now()
						user_id = current_user.username #current employee id
						file_save_gridfs_l3(input_file,"application/pdf/doc/docx/xlsm/xlsx",tag_name,file_name,upload_date,plant_id,
							user_id,dept_id,dept_name,plant_name)
				
				upload_documents=find_in_collection('fs.files',{"tag" : "L4_Documents","delete_flag":0})
				flash("Sucessfully Uploaded the"+ "  "+tag_name +" "+"Documents",'alert-success')
				return render_template('IMS_L4_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)
		else:

			ims_docs={"tag" : form['tag_name'],'delete_flag':0,'plant_id':form['plant_id'],'filename':form['filename']}
			ims_docs1= {"$set":{"deleted_date":datetime.datetime.now(),"delete_flag":1,"deleted_by":current_user.username}}
	 		update_collection('fs.files',ims_docs,ims_docs1)
			upload_documents=find_in_collection('fs.files',{"tag" : "L4_Documents","delete_flag":0})
			flash("Sucessfully Deleted the"+ "  "+form['filename']+" "+"Document",'alert-success')
			return render_template('IMS_L4_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)


	return render_template('IMS_L4_UPLOAD.html', user_details=user_details,all_docs=upload_documents,all_plants=all_plants)


@zen_user.route('/l3_imsview/', methods = ['GET', 'POST'])
@login_required
def l3_view():
	user_details = base()
	all_plants = find_and_filter('Plants',{},{'plant_id':1,'plant_desc':1,'_id':0})
	if request.method == 'POST':
		form=request.form
		plant_id=form['plant_id']
		dept_id=form['dept_id']
		if dept_id:
			upload_documents=find_in_collection('fs.files',{"tag" : "L3_Work_Instructions","delete_flag":0,'plant_id':plant_id,'dept_id':dept_id})
		else:
			upload_documents=find_in_collection('fs.files',{"tag" : "L3_Work_Instructions","delete_flag":0,'plant_id':plant_id})
		return render_template('IMS_L3_VIEW.html', user_details=user_details,all_plants=all_plants,all_docs=upload_documents)
	return render_template('IMS_L3_VIEW.html', user_details=user_details,all_plants=all_plants)

@zen_user.route('/l4_imsview/', methods = ['GET', 'POST'])
@login_required
def l4_view():
	user_details = base()
	all_plants = find_and_filter('Plants',{},{'plant_id':1,'plant_desc':1,'_id':0})
	if request.method == 'POST':
		form=request.form
		plant_id=form['plant_id']
		dept_id=form['dept_id']
		if dept_id:
			upload_documents=find_in_collection('fs.files',{"tag" : "L4_Documents","delete_flag":0,'plant_id':plant_id,'dept_id':dept_id})
		else:
			upload_documents=find_in_collection('fs.files',{"tag" : "L4_Documents","delete_flag":0,'plant_id':plant_id})
		return render_template('IMS_L4_VIEW.html', user_details=user_details,all_plants=all_plants,all_docs=upload_documents)
	return render_template('IMS_L4_VIEW.html', user_details=user_details,all_plants=all_plants)


@zen_user.route('/departmentlist', methods = ['GET', 'POST'])
@login_required
def functionlocation():
	q = request.args.get('mtype')
	db=dbconnect()
	user_details = base()
	departmentlist=find_and_filter('departmentmaster',{"plant":q},{"dept":1,"_id":0,'dept_pid':1})
	return json.dumps({"results":departmentlist})

def file_save_gridfs_l3(file_data,ctype,tag_name,file_name,upload_date,plant_id,user_id,dept_id,dept_name,plant_name):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    file_ext = file_data.filename.rsplit('.', 1)[1]
    fs.put(file_data.read(), content_type=ctype,tag=tag_name,filename = str(file_name),upload_date=upload_date,plant_id = plant_id,
    	user_id=user_id,delete_flag=0,dept_id=dept_id,dept_name=dept_name,file_ext=file_ext,plant_name=plant_name)

#this Code is Usefull to Uplaod View and Delete the Ploicy HR Documents
@zen_user.route('/policy_hrupload/', methods = ['GET', 'POST'])
@login_required
def policy_hrupload():
	user_details = base()
	found =None
	upload_documents=find_in_collection('fs.files',{"tag" : "HR_Policy","delete_flag":0})
	if request.method == 'POST':
		form=request.form
		if form['submit']=="Upload":
			employee_id=user_details['username']
			tag_name="HR_Policy"
			val=len(request.files)
			for i in range(1,val+1):
				if request.files['file'+str(i)]:
					input_file=request.files['file'+str(i)]
					file_name=input_file.filename
					plant_id=user_details['plant_id']
					new_file_name= form['text'+str(i)]
					f_name=str(new_file_name)+'.'+"pdf"
					ims_docs=find_in_collection('fs.files',{"filename":str(f_name),"tag" : "HR_Policy",'delete_flag':0,
						'filename':str(f_name)})
					if ims_docs:
						found=ims_docs
						flash("Document with "+"  "+f_name+ " "+"already exists Please Delete it to Upload Again",'alert-danger')
					
			if found:
				return render_template('HR_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
			else:
				for i in range(1,val+1):
					if request.files['file'+str(i)]:
						tag_name="HR_Policy"
						input_file=request.files['file'+str(i)]
						file_name=input_file.filename
						new_file_name= form['text'+str(i)]
						f_name=str(new_file_name)+'.'+"pdf"
						upload_date= datetime.datetime.now()
						user_id = current_user.username #current employee id
						file_save_gridfss(input_file,"application/pdf",tag_name,f_name,upload_date,
							plant_id,user_id)
				upload_documents=find_in_collection('fs.files',{"tag" : "HR_Policy","delete_flag":0})
				flash("Sucessfully Uploaded the"+ "  "+tag_name +" "+"Documents",'alert-success')
				return render_template('HR_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)
		else:

			ims_docs={"tag" : form['tag_name'],'delete_flag':0,'filename':form['filename']}
			ims_docs1= {"$set":{"deleted_date":datetime.datetime.now(),"delete_flag":1,"deleted_by":current_user.username}}
	 		update_collection('fs.files',ims_docs,ims_docs1)
			upload_documents=find_in_collection('fs.files',{"tag" : "HR_Policy","delete_flag":0})
			flash("Sucessfully Deleted the"+ "  "+form['filename']+" "+"Document",'alert-success')
			return render_template('HR_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)

	return render_template('HR_POLICY_UPLOAD.html', user_details=user_details,all_docs=upload_documents)

@zen_user.route('/policy_hrview/', methods = ['GET', 'POST'])
@login_required
def policy_hrview():
	user_details = base()
	upload_documents=find_in_collection('fs.files',{"tag" : "HR_Policy",'delete_flag':0})
	return render_template('HR_POLICY_VIEW.html', user_details=user_details,all_docs=upload_documents)



