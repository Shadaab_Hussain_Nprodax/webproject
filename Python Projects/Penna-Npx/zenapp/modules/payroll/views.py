from flask import Flask, render_template, redirect, request, flash, url_for,Response
from zenapp import * 
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import os
import zipfile
import StringIO
#from pyrfc import *
from zenapp.modules.leave.lfunctions import *
zen_payroll = Blueprint('zenpayroll', __name__, url_prefix='/payroll')

@zen_payroll.route('/payslips/')
@login_required
def payslips():
	p =check_user_role_permissions('546f2245850d2d1b00023b27')
	if p:
		user_details=base()
		user_details['username']= current_user.username
		return render_template('payslips.html',user_details=user_details)
	else:
		return redirect(url_for('zenuser.index'))


#zip the last 6 months payslips pdfs 
@zen_payroll.route('/zipfiles', methods=['GET', 'POST'])
@login_required
def zipfiles():
	uid = current_user.get_id()
	user_details=base()
	form=request.form
	print form
	print "Name",
	emp_id=current_user.username
	

	filenames=[]
	yr=request.args.get('yr')
	yr1=request.args.get('yr1')
	month1=request.args.get('month1')
	month2=request.args.get('month2')
	payslip_type=request.args.get('cat')

	if payslip_type=='general':

		if(yr!=yr1):

			r=int(month1);
			a=""
			o=int(month2)+1;
			user_details=base()

			for x in range(r, 13):
				c=os.path.abspath("zenapp/static/forms/Payslips/"+yr+"/"+str(x)+"/000"+emp_id+".pdf")
				filenames.append(c);
			for y in range(1, o):
				c=os.path.abspath("zenapp/static/forms/Payslips/"+yr1+"/"+str(y)+"/000"+emp_id+".pdf")
				filenames.append(c);
		
		else:

			r=int(month1);
			a=""
			o=int(month2)+1;
			user_details=base()

			for x in range(r, o):
				c=os.path.abspath("zenapp/static/forms/Payslips/"+yr+"/"+str(x)+"/000"+emp_id+".pdf")
				filenames.append(c);
	else:
		if(yr!=yr1):

			r=int(month1);
			a=""
			o=int(month2)+1;
			user_details=base()

			for x in range(r, 13):
				c=os.path.abspath("zenapp/static/forms/Payslips/"+yr+"/"+str(x)+"/000"+emp_id+"-IT.pdf")
				filenames.append(c);
			for y in range(1, o):
				c=os.path.abspath("zenapp/static/forms/Payslips/"+yr1+"/"+str(y)+"/000"+emp_id+"-IT.pdf")
				filenames.append(c);
		
		else:

			r=int(month1);
			a=""
			o=int(month2)+1;
			user_details=base()

			for x in range(r, o):
				c=os.path.abspath("zenapp/static/forms/Payslips/"+yr+"/"+str(x)+"/000"+emp_id+"-IT.pdf")
				filenames.append(c);
		

	zip_subdir = "Payslips"
	zip_filename = "%s.zip" % zip_subdir

	# Open StringIO to grab in-memory ZIP contents
	s = StringIO.StringIO()
	# The zip compressor
	zf = zipfile.ZipFile(s, "a")

	for fpath in filenames:
	    # Calculate path for file in zip
		fdir, fname = os.path.split(fpath)
		fdir1, fname1 = os.path.split(fdir)
		fdir2, fname2 = os.path.split(fdir1)
		zip_path = os.path.join(zip_subdir, fname2+'_'+fname1+'_'+fname)
		zf.write(fpath, zip_path)


 ## Add file, at correct path
	

 #    # Must close zip for all contents to be written
	zf.close()

 #    # Grab ZIP file from in-memory, make response with correct MIME-type
 	resp =Response(s.getvalue(), mimetype = "application/x-zip-compressed")
    # ..and correct content-disposition
	resp.headers['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
	return resp
# @zen_payroll.route('/zipfiles', methods=['GET', 'POST'])
# @login_required
# def zipfiles():
# 	uid = current_user.get_id()
# 	filenames=[]
# 	yr=request.args.get('yr')
# 	yr1=request.args.get('yr1')
# 	month1=request.args.get('month1')
# 	month2=request.args.get('month2')

# 	if(yr!=yr1):

# 		r=int(month1);
# 		a=""
# 		o=int(month2)+1;
# 		user_details=base()

# 		for x in range(r, 13):
# 			c=os.path.abspath("zenapp/static/forms/Payslips/"+yr+"/"+str(x)+"/000"+user_details['username']+".pdf")
# 			filenames.append(c);
# 		for y in range(1, o):
# 			c=os.path.abspath("zenapp/static/forms/Payslips/"+yr1+"/"+str(y)+"/000"+user_details['username']+".pdf")
# 			filenames.append(c);
	
# 	else:

# 		r=int(month1);
# 		a=""
# 		o=int(month2)+1;
# 		user_details=base()

# 		for x in range(r, o):
# 			c=os.path.abspath("zenapp/static/forms/Payslips/"+yr+"/"+str(x)+"/000"+user_details['username']+".pdf")
# 			filenames.append(c);
		

		

# 	zip_subdir = "Payslips"
# 	zip_filename = "%s.zip" % zip_subdir

# 	# Open StringIO to grab in-memory ZIP contents
# 	s = StringIO.StringIO()
# 	# The zip compressor
# 	zf = zipfile.ZipFile(s, "a")

# 	for fpath in filenames:
# 	    # Calculate path for file in zip
# 		fdir, fname = os.path.split(fpath)
# 		fdir1, fname1 = os.path.split(fdir)
# 		fdir2, fname2 = os.path.split(fdir1)
# 		zip_path = os.path.join(zip_subdir, fname2+'_'+fname1+'_'+fname)
# 		zf.write(fpath, zip_path)


#  #        # Add file, at correct path
	

#  #    # Must close zip for all contents to be written
# 	zf.close()

#  #    # Grab ZIP file from in-memory, make response with correct MIME-type
#  	resp =Response(s.getvalue(), mimetype = "application/x-zip-compressed")
#     # ..and correct content-disposition
# 	resp.headers['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
# 	return resp
	

 	
   # ..and correct content-disposition
	 #resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
	# return url_for(filename=%s' % zip_filename)


@zen_payroll.route('/form16/')
@login_required
def form16():
	uid = current_user.username
	p =check_user_role_permissions('546f2257850d2d1b00023b28')
	if p:
		user_details=base()
		return render_template('form16.html',user_details=user_details,uid=uid)
	else:
		return redirect(url_for('zenuser.index'))




        
