from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
import csv
import os
import cgi
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from zenapp.modules.leave.lfunctions import *
import logging
#from pyrfc import *
import json
import random
import sys
from array import array
from zenapp.modules.pennovation import *
import time;

zen_pennovation = Blueprint('zenpennovation', __name__, url_prefix='/pennovation')

@zen_pennovation.route('/pennovationideas/', methods = ['GET', 'POST'])
@login_required
def pennovationideas_submit():
	user_details = base()
	uid = current_user.username

	IMList =  find_and_filter('PennovationSettings',{"Active":"Yes","InnogateManager":"Yes"},{'_id':0,'InnogateManagerId':1})

	print IMList
	IMCheck = False

	for i in range(0,len(IMList)):
		if uid == IMList[i]['InnogateManagerId']:
			IMCheck = True			

	if IMCheck:
		return render_template('pennovationNoAccess.html', user_details=user_details)

	BoardMemberList = find_and_filter('PennovationSettings',{"Active":"Yes","BoardMember":"Yes"},{'_id':0,'BoardMemberId':1})

	BMCheck = False

	for i in range(0,len(BoardMemberList)):
		if uid == BoardMemberList[i]['BoardMemberId']:
			BMCheck = True	

	if BMCheck:
		return render_template('pennovationNoAccess.html', user_details=user_details)


	if request.method == 'POST':
		form=request.form
		IdeaTitle = form['IdeaTitle']
		IdeaDescription = form['IdeaDescription']
		IdeaShortDescription = IdeaDescription[0:40]
		IdeaCount=find_in_collection_count("PennovationIdeas",{"UserId":uid,"Valid":"Yes"}) + 1
		IdeaId = "P-"+uid+"-"+str(IdeaCount)
		TotalIdeaCount=find_in_collection_count("PennovationIdeas",{"Valid":"Yes"}) + 1
		InternalIdeaId = "I-00"+"-"+str(TotalIdeaCount)
		DisplayStatus = "Submitted"
		save_collection('PennovationIdeas',{"UserId":uid,"IdeaId":IdeaId,"InternalIdeaId":InternalIdeaId,"IdeaTitle":IdeaTitle,"IdeaShortDescription":IdeaShortDescription,"IdeaDescription":IdeaDescription,"DisplayStatus":DisplayStatus,"Status":"Submitted","Valid":"Yes","SubmittedOn":datetime.datetime.now()})
		flash("Thankyou for submitting your idea to Pennovation. Your Pennovation Idea Submission Id : "+IdeaId,'alert-success')
	return render_template('pennovationideas.html', user_details=user_details)


@zen_pennovation.route('/pennovationsubmissions/', methods = ['GET', 'POST'])
@login_required
def pennovationsubmissions_view():
	user_details = base()
	uid = current_user.username

	IMList =  find_and_filter('PennovationSettings',{"Active":"Yes","InnogateManager":"Yes"},{'_id':0,'InnogateManagerId':1})

	IMCheck = False

	for i in range(0,len(IMList)):
		if uid == IMList[i]['InnogateManagerId']:
			IMCheck = True			

	if IMCheck:
		return render_template('pennovationNoAccess.html', user_details=user_details)

	BoardMemberList = find_and_filter('PennovationSettings',{"Active":"Yes","BoardMember":"Yes"},{'_id':0,'BoardMemberId':1})

	BMCheck = False

	for i in range(0,len(BoardMemberList)):
		if uid == BoardMemberList[i]['BoardMemberId']:
			BMCheck = True	

	if BMCheck:
		return render_template('pennovationNoAccess.html', user_details=user_details)
		
	PennovationIdeas = find_in_collection_sort('PennovationIdeas',{"UserId":uid,"Valid":"Yes"},[("SubmittedOn",1)])
	#print PennovationIdeas
	return render_template('pennovationsubmissions.html', user_details=user_details,PennovationIdeas=PennovationIdeas)

@zen_pennovation.route('/pennovationupdates/', methods = ['GET', 'POST'])
#@login_required
def pennovationupdatesapi():
	# form=request.form
	# print form['Name']
	if request.method == 'POST':
		jsonData = request.get_json()
		if jsonData['ChangeType'] == "MoveToBoard":
			currentIdeaId = jsonData['IdeaId']
			BoardMemberList = find_and_filter('PennovationSettings',{"Active":"Yes","BoardMember":"Yes"},{'_id':0,'BoardMemberId':1})

			update_collection('PennovationIdeas',{"IdeaId":currentIdeaId},
					{'$set' :
					{
					"DisplayStatus":"Sent To Board",
					"Status":"Approved-SentToBoard",
					"SentToBoardOn":datetime.datetime.now(),
					"ScoresSubmittedByBoardMember-"+BoardMemberList[0]['BoardMemberId']:"No",
					"ScoresSubmittedByBoardMember-"+BoardMemberList[1]['BoardMemberId']:"No",
					"ScoresSubmittedByBoardMember-"+BoardMemberList[2]['BoardMemberId']:"No",
					"BoardReviewCompleted":"No"
					}
					})
			return "Sent To Board"
		elif jsonData['ChangeType'] == "RejectedByIM":
			currentIdeaId = jsonData['IdeaId']
			update_collection('PennovationIdeas',{"IdeaId":currentIdeaId},
					{'$set' :
					{
					"DisplayStatus":"Rejected",
					"Status":"Rejected-ByIM",
					"RejectedByIMOn":datetime.datetime.now()
					}
					})
			return "Rejected By IM"
	#	return jsonData['ChangeType']+"-"+jsonData['IdeaId']
	#return render_template('pennovationsubmissions.html', user_details=user_details,PennovationIdeas=PennovationIdeas)

@zen_pennovation.route('/pennovationIMView/', methods = ['GET', 'POST'])
@login_required
def pennovationInnogateManager_view():

	user_details = base()
	uid = current_user.username
	IMList =  find_and_filter('PennovationSettings',{"Active":"Yes","InnogateManager":"Yes"},{'_id':0,'InnogateManagerId':1})

	IMCheck = False

	for i in range(0,len(IMList)):
		if uid == IMList[i]['InnogateManagerId']:
			IMCheck = True			

	if not IMCheck:
		return render_template('pennovationNoAccess.html', user_details=user_details)

	BoardMemberList = find_and_filter('PennovationSettings',{"Active":"Yes","BoardMember":"Yes"},{'_id':0,'BoardMemberId':1})
	WaitingForInnogateManagerPreliminaryReview = find_in_collection_sort('PennovationIdeas',{"Valid":"Yes","Status":"Submitted"},[("SubmittedOn",1)])
	RejectedByInnogateManagerPreliminaryReview = find_in_collection_sort('PennovationIdeas',{"Valid":"Yes","Status":"Rejected-ByIM"},[("SubmittedOn",1)])
	WaitingForBoardMembersAction = find_in_collection_sort('PennovationIdeas',{"Valid":"Yes","BoardReviewCompleted":"No"},[("SubmittedOn",1)])
	BoardReviewCompleted = find_in_collection_sort('PennovationIdeas',{"Valid":"Yes","BoardReviewCompleted":"Yes","FinalEligible":"Yes"},[("SubmittedOn",1)])
	BoardReviewCompletedNotEligible = find_in_collection_sort('PennovationIdeas',{"Valid":"Yes","BoardReviewCompleted":"Yes","FinalEligible":"No"},[("FinalAverageScorePercentage",1)])
	#print len(WaitingForBoardMembersAction)
	#print WaitingForBoardMembersAction

	return render_template('pennovationIMView.html', user_details=user_details,WaitingForInnogateManagerPreliminaryReview=WaitingForInnogateManagerPreliminaryReview,RejectedByInnogateManagerPreliminaryReview=RejectedByInnogateManagerPreliminaryReview,WaitingForBoardMembersAction=WaitingForBoardMembersAction,BoardMemberList=BoardMemberList,BoardReviewCompleted=BoardReviewCompleted,BoardReviewCompletedNotEligible=BoardReviewCompletedNotEligible)	

@zen_pennovation.route('/pennovationBoardMemberView/', methods = ['GET', 'POST'])
@login_required
def pennovationBoardMember_view():
	user_details = base()
	uid = current_user.username

	BoardMemberList = find_and_filter('PennovationSettings',{"Active":"Yes","BoardMember":"Yes"},{'_id':0,'BoardMemberId':1})

	BMCheck = False

	for i in range(0,len(BoardMemberList)):
		if uid == BoardMemberList[i]['BoardMemberId']:
			BMCheck = True	

	if not BMCheck:
		return render_template('pennovationNoAccess.html', user_details=user_details)


	WaitingForBoardMemberReview = find_in_collection_sort('PennovationIdeas',{"Valid":"Yes","Status":"Approved-SentToBoard","ScoresSubmittedByBoardMember-"+uid:{'$nin':[ "Yes" ]}},[("SubmittedOn",1)])

	if request.method == 'POST':
		print "POST SUBMIT"
		form=request.form
		print form['NoveltyScore']
		currentInternalIdeaId = form['ModalInternalIdeaId']
		print "IIIIID",currentInternalIdeaId
		BoardMemberScores = []
		array={}
		array={
				# "DisplayStatus":"Pending With Innogate Manager For Final Review",
				# "Status":"ReviwedByBoardMember-PendingWithInnogateManager",
				"ScoresSubmittedByBoardMember":uid,
				"NoveltyScore":form['NoveltyScore'],
				"OriginalityScore":form['OriginalityScore'],
				"RelevanceScore":form['RelevanceScore'],
				"ImplementableScore":form['ImplementableScore'],
				"CommercialScore":form['CommercialScore']					
			  }
		TotalScores = float(form['NoveltyScore'])+float(form['OriginalityScore'])+float(form['RelevanceScore'])+float(form['ImplementableScore'])+float(form['CommercialScore'])

		BoardMemberScores.append(array) 
		update_collection('PennovationIdeas',{"InternalIdeaId":currentInternalIdeaId},
				{'$set' :
				{
				# "DisplayStatus":"Pending With Innogate Manager For Final Review",
				# "Status":"ReviwedByBoardMember-PendingWithInnogateManager",
				"ScoresSubmittedByBoardMember-"+uid:"Yes",
				"TotalScoresSubmittedByBoardMember-"+uid:TotalScores,
				"BoardMemberScores-"+uid:BoardMemberScores,
				"BoardReviewCompleted":"No"
				}
				})
		print "DDDDDDDDDD",TotalScores
		currentInternalData = find_in_collection_sort('PennovationIdeas',{"InternalIdeaId":currentInternalIdeaId,"Valid":"Yes"},[("SubmittedOn",1)])

		BoardMemberList = find_and_filter('PennovationSettings',{"Active":"Yes","BoardMember":"Yes"},{'_id':0,'BoardMemberId':1})

		if currentInternalData[0]['ScoresSubmittedByBoardMember-'+BoardMemberList[0]['BoardMemberId']]== "Yes" and currentInternalData[0]['ScoresSubmittedByBoardMember-'+BoardMemberList[1]['BoardMemberId']]== "Yes" and currentInternalData[0]['ScoresSubmittedByBoardMember-'+BoardMemberList[2]['BoardMemberId']]== "Yes":

			FinalTotalScore = (currentInternalData[0]['TotalScoresSubmittedByBoardMember-'+BoardMemberList[0]['BoardMemberId']] + currentInternalData[0]['TotalScoresSubmittedByBoardMember-'+BoardMemberList[1]['BoardMemberId']] + currentInternalData[0]['TotalScoresSubmittedByBoardMember-'+BoardMemberList[2]['BoardMemberId']])
						
			FinalAverageScorePercentage = round((100*FinalTotalScore)/float(150),2)

			if FinalAverageScorePercentage < 80:
				FinalEligible = "No"
			else :
				FinalEligible = "Yes"
			
			update_collection('PennovationIdeas',{"InternalIdeaId":currentInternalIdeaId},
				{'$set' :
				{
					"BoardReviewCompleted":"Yes",
					"Status":"BoardReview-Completed",
					"FinalTotalScore":FinalTotalScore,
					"FinalAverageScorePercentage":FinalAverageScorePercentage,
					"FinalEligible":FinalEligible
				}
				})


		WaitingForBoardMemberReview = find_in_collection_sort('PennovationIdeas',{"Valid":"Yes","Status":"Approved-SentToBoard","ScoresSubmittedByBoardMember-"+uid:{'$nin':[ "Yes" ]}},[("SubmittedOn",1)])

	return render_template('pennovationBoardMemberView.html', user_details=user_details,WaitingForBoardMemberReview=WaitingForBoardMemberReview)	