from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.sessions.form import *
from zenapp.dbfunctions import *
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import os
import csv
import zipfile
import StringIO
from calendar import monthrange
from datetime import datetime as dt
from pyrfc import *
from zenapp.modules.leave.lfunctions import *
import threading
import time
zen_sessions = Blueprint('zensessions', __name__, url_prefix='/sessions')


@zen_sessions.route('/usersessions/', methods=['GET', 'POST'])
@login_required
def usersessions():
	p =check_user_role_permissions('55c9d3c112ebc4ff41fa157c')
	user_details = base()		
	now =datetime.datetime.now()
	now_str =str(datetime.datetime.now())
	pre_day = str(datetime.datetime.now().date())

	check = ""
	chart_data=find_in_collection('logurl',{})
	if check == "":
		form = user_sessions(request.values)
		fromd = form.user_date1.data
		tod = form.user_date2.data
	else:
		check = "true"
	if check == "true":
		form = user_sessions1(request.values)
		fromd = request.form.get('user_date1')
		tod = request.form.get('user_date2')

	pre_dt = yy_mm_dd(now)  #present date taking yy,mm,dd
	pre_date_milli=dt_milli_regular(now_str) #present date in milliseconds

	# pre_day ="2015-08-19"   #for finding the count manually taking date
	# pre_dt = pre_day.strip().split('-')
	# pre_yy=int(pre_dt[0])
	# pre_mm=int(pre_dt[1])
	# pre_dd=int(pre_dt[2])
	# pre_date_milli=int(datetime.datetime(2015,8,19).strftime("%s"))*1000
	# today_sessions = find_in_collection('logurl',{'$and':[{"lastlogin":{'$gt':dt(pre_yy,pre_mm,pre_dd)}},{"lastlogin":{'$lt':dt(pre_yy,pre_mm,pre_dd,23,59,59)}}]})

	unique_logins = find_and_filter('logurl',{"lastlogin":""},{'_id':0,'username':1,'l_name':1,'logintime':1})
	unique_login_count = len(unique_logins)

	sessions_tilldate=find_in_collection('logurl',{"logintime":{'$exists':1}})
	total_sessions_count = len(sessions_tilldate)

	today_sessions = find_in_collection('logurl',{'$and':[{"lastlogin":{'$gt':dt(pre_dt[0],pre_dt[1],pre_dt[2])}},{"lastlogin":{'$lt':dt(pre_dt[0],pre_dt[1],pre_dt[2],23,59,59)}}]})
	sessions_count=len(today_sessions)
	# pr_save_dt=save_collection('datachart',{"pre_day":pre_day,"date_millisec":pre_date_milli,"sessions_count":sessions_count})
	pr_save_dt=update_collection('datachart',{"pre_day":pre_day},{"pre_day":pre_day,"date_millisec":pre_date_milli,"sessions_count":sessions_count})
	data_sessions_count = find_all_in_collection('datachart')

	
	now_hr = hr_normal(now_str) #present hour taken using function,input is datetime.datetime.now()
	now_hr_milli = hr_milli(now_str) #present date in milli using function,input is datetime.datetime.now().
	detail_complete = find_in_collection('logurl',{'$and':[{"lastlogin":{"$ne":""}},{"lastlogin":{'$gt':dt(pre_dt[0],pre_dt[1],pre_dt[2])}},{"lastlogin":{'$lt':dt(pre_dt[0],pre_dt[1],pre_dt[2])}}]})

	data_ss = find_all_in_collection_sort('unique_login_chart',[("now_hr_millisec",1)])
	unique_users_sessions = find_dist_in_collection('logurl',"username")
	unique_users_sess_count = len(unique_users_sessions)
	


	session1=find_in_collection('Userinfo',{"username":current_user.username})
	time = session1[0]['current_login']
	arry = {"logmod_time":datetime.datetime.now(),"url":request.path}
	update_coll('logurl',{'logintime':time},{'$addToSet':{"module":arry}})
	total_user_session = find_all_in_collection('logurl')
	date = datetime.datetime.now().date() 
	week = datetime.datetime.now().isocalendar()[1]
	year = datetime.datetime.now().year
	list_name =[]
	list_test=[]
	counttotal = find_all_in_collection('logurl')
	counttotal = len(counttotal)

	verify_max =find_and_filter('unique_login_chart',{'$and':[{"now_hour":now_hr,"Date":pre_day}]},{"_id":0,"sessions_count":1})#taking max count from the collection
	unique_login_count = find_in_collection_count('logurl',{"lastlogin":""})
	if len(verify_max) == 0:
		# print "verify max is zero"
		save_collection('unique_login_chart',{"now_hour":now_hr,"now_hour":now_hr,"now_hr_millisec":now_hr_milli,"sessions_count":unique_login_count,"Date":pre_day})
	else:
		# unique_login_count = find_in_collection_count('logurl',{"lastlogin":""}) #present count
		if unique_login_count < verify_max[0]['sessions_count']:
			# print "updating with present unique login"
			pr_update_dt=update_collection('unique_login_chart',{"now_hour":now_hr,"Date":pre_day},{'$set':{"sessions_count":verify_max[0]['sessions_count']}})
		else:
			# print "rejecting the present unique login"
			pr_update_dt=update_collection('unique_login_chart',{"now_hour":now_hr,"Date":pre_day},{"now_hour":now_hr,"now_hr_millisec":now_hr_milli,"sessions_count":unique_login_count,"Date":pre_day})

	del_doc('logurl',{"logintime":{'$exists':0}})
# needed to be moved to development
	empty_logouts = find_and_filter('logurl',{'$and':[{"lastlogin":""},{"username":{'$ne':current_user.username}}]},{'_id':0,'username':1})
	for i in empty_logouts:
		lost_user_id = find_and_filter('Userinfo',{"username":i['username']},{'_id':1})
		fatal_time = find_in_collection_sort_limit('sessions',{"data.user_id":str(lost_user_id[0]['_id'])},[("expiration",-1)],1 )
		dt_now =datetime.datetime.now()
		try:
			if dt_now > fatal_time[0]['expiration']:
				update_collection('logurl',{"username":i['username'],"lastlogin":""},{'$set':{"lastlogin":fatal_time[0]['expiration']}})
		except:
			pass
# needed to be moved to development

	for i in range(1,len(total_user_session)):
		test = total_user_session[i]['username']
		l_name = find_in_collection('Userinfo',{"username":test})
		list_name.append(l_name[0]['l_name'])
	for i in range(0,len(list_name)):
		list_test.append(list_name[i].encode("utf-8"))
	
	if p:
		return render_template('session.html',form=form,data=data_sessions_count,data2=data_ss,name=list_test,counttotal=unique_users_sess_count, user_details=user_details,emp_det=unique_logins,today_sessions=today_sessions,emp_det_complete=detail_complete,log_count1=unique_login_count,sessions_tilldate=sessions_tilldate,log_count=total_sessions_count,date=date,year=year, week=week)
	else:
		return redirect(url_for('zenuser.index'))


@zen_sessions.route('/getsessions', methods=['GET', 'POST'])
@login_required
def getsessions():
	form=request.form;
	que_frm_dt = str(form['user_date1'])
	que_to_dt = str(form['user_date2'])
	dt_frm=dt_rng(que_frm_dt) # taking dates from ajax 
	dt_to=dt_rng(que_to_dt)
	from_dt_milli = dt_rng_milli(que_frm_dt)
	to_dt_milli = dt_rng_milli(que_to_dt)
	range_dt_milli = find_and_filter('datachart',{'$and':[{"date_millisec":{'$gt':from_dt_milli}},{"date_millisec":{'$lt':to_dt_milli}}]},{"_id":0})
	range_dt = find_and_filter('logurl',{'$and':[{"logintime":{'$gt':dt(dt_frm[0],dt_frm[1],dt_frm[2])}},{"lastlogin":{'$ne':""}},{"logintime":{'$lt':dt(dt_to[0],dt_to[1],dt_to[2])}}]},{"_id":0,"logintime":1,"username":1,"lastlogin":1,"l_name":1})
	get_list_details=[]
	get_list_milli = []
	for i in range_dt:
		get_list_details+=[[i['username'],i['l_name'],str(i['logintime']),str(i['lastlogin'])]]
	for i in range_dt_milli:
		get_list_milli += [[int(i['date_millisec']),i['sessions_count']]]
	return jsonify({"result":get_list_details,"line_data":get_list_milli})



def yy_mm_dd(req_dt):   #accessing year,month,day,hour from datetime.datetime.now()
 	req_fields=req_dt.timetuple()
	return req_fields


def dt_rng(req_dt):   #date string  to yy,mm,dd
	req_date = req_dt.strip().split('/') #when date format is iso date '/' for custom time
	req_y = int(req_date[0])
	req_m = int(req_date[1])
	req_d = int(req_date[2])
	req_dt = [req_y,req_m,req_d]
	return req_dt

def dt_rng_milli(req_dt):   #date string to milli second directly
	req_date = req_dt.strip().split('/') #when date format is iso date '/' for custom time
	req_y=int(req_date[0])
	req_m=int(req_date[1])
	req_d=int(req_date[2])
	req_dt_milli=int(datetime.datetime(req_y,req_m,req_d).strftime("%s"))*1000
	return req_dt_milli

def dt_milli_regular(req_dt):   #date string to milli second directly
	req_date = req_dt.strip().split(' ') #when date format is iso date '/' for custom time
	req_date = req_date[0]
	req_date = req_date.strip().split('-')
	req_y=int(req_date[0])
	req_m=int(req_date[1])
	req_d=int(req_date[2])
	req_dt_milli=int(datetime.datetime(req_y,req_m,req_d).strftime("%s"))*1000
	# print req_dt_milli
	return req_dt_milli

def hr_normal(dt_hr): #pass datetime.datetime.now().time() result
	req_dt = dt_hr.strip().split(' ')
	req_hr = req_dt[1]
	req_hr_arr = str(req_hr).strip().split(':')
	req_hr=int(req_hr_arr[0])
	return req_hr

#moved to development
def hr_min_normal(dt_hr): #pass datetime.datetime.now().time() result
	req_dt = dt_hr.strip().split(' ')
	req_hr = req_dt[1]
	req_hr_arr = str(req_hr).strip().split(':')
	req_hr=[int(req_hr_arr[0]),int(req_hr_arr[1])]
	return req_hr
#moved to development

def hr_milli(dt_hr): #pass datetime.datetime.now().time() result
	req_dt = dt_hr.strip().split(' ')
	req_date = req_dt[0]
	req_hr = req_dt[1]
	req_date = req_date.strip().split('-')
	req_y=int(req_date[0])
	req_m=int(req_date[1])
	req_d=int(req_date[2])
	req_hr_arr = str(req_hr).strip().split(':')
	req_hr=int(req_hr_arr[0])
	req_hr_milli=int(datetime.datetime(req_y,req_m,req_d,req_hr).strftime("%s"))*1000
	return req_hr_milli


# moved to development
def recurr():
	now_str =str(datetime.datetime.now())
	pre_day = str(datetime.datetime.now().date())
	now_hr = hr_normal(now_str) #present hour taken using function,input is datetime.datetime.now()
	now_hr_milli = hr_milli(now_str) #present date in milli using function,input is datetime.datetime.now().
	verify_max =find_and_filter('unique_login_chart',{'$and':[{"now_hour":now_hr,"Date":pre_day}]},{"_id":0,"sessions_count":1})#taking max count from the collection
	unique_login_count = find_in_collection_count('logurl',{"lastlogin":""})
	if len(verify_max) == 0:
		# print "verify max is zero"
		save_collection('unique_login_chart',{"now_hour":now_hr,"now_hour":now_hr,"now_hr_millisec":now_hr_milli,"sessions_count":unique_login_count,"Date":pre_day})
	else:
		# unique_login_count = find_in_collection_count('logurl',{"lastlogin":""}) #present count
		if unique_login_count < verify_max[0]['sessions_count']:
			# print "updating with present unique login"
			pr_update_dt=update_collection('unique_login_chart',{"now_hour":now_hr,"Date":pre_day},{'$set':{"sessions_count":verify_max[0]['sessions_count']}})
		else:
			# print "rejecting the present unique login"
			pr_update_dt=update_collection('unique_login_chart',{"now_hour":now_hr,"Date":pre_day},{"now_hour":now_hr,"now_hr_millisec":now_hr_milli,"sessions_count":unique_login_count,"Date":pre_day})
		# print datetime.datetime.now()
		empty_logouts = find_and_filter('logurl',{"lastlogin":""},{'_id':0,'username':1})
		for i in empty_logouts:
			lost_user_id = find_and_filter('Userinfo',{"username":i['username']},{'_id':1})
			fatal_time = find_in_collection_sort_limit('sessions',{"data.user_id":str(lost_user_id[0]['_id'])},[("expiration",-1)],1 )
			dt_now =datetime.datetime.now()
			# print fatal_time[0]['expiration']
			try:
				if dt_now > fatal_time[0]['expiration']:
					# print "setting logout"
					update_collection('logurl',{"username":i['username'],"lastlogin":""},{'$set':{"lastlogin":fatal_time[0]['expiration']}})
			except:
				pass


	#threading.Timer(10, recurr).start()

def today_session_count():
	now = datetime.datetime.now()
	pre_day = str(datetime.datetime.now().date())
	now_str =str(datetime.datetime.now())
	pre_dt = yy_mm_dd(now)  #present date taking yy,mm,dd
	pre_date_milli=dt_milli_regular(now_str) #present date in milliseconds
	sessions_count = find_in_collection_count('logurl',{'$and':[{"lastlogin":{'$gt':dt(pre_dt[0],pre_dt[1],pre_dt[2])}},{"lastlogin":{'$lt':dt(pre_dt[0],pre_dt[1],pre_dt[2],23,59,59)}}]})
	# print sessions_count
	pr_save_dt=update_collection('datachart',{"pre_day":pre_day},{"pre_day":pre_day,"date_millisec":pre_date_milli,"sessions_count":sessions_count})
	# print "updating todays sessions"
	#threading.Timer(10, today_session_count).start()

#recurr()
#today_session_count()
# moved to development