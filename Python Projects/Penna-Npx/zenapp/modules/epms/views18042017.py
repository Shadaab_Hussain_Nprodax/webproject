from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
import csv
import os
import cgi
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from zenapp.modules.leave.lfunctions import *
import logging
#from pyrfc import *
import json
import random
import sys
from array import array
from zenapp.modules.epms import *
import time;
from Crypto.Cipher import AES
import base64

zen_epms = Blueprint('zenepms', __name__, url_prefix='/epms')


def fieldhash(string):                                                                      
        BLOCK_SIZE = 32                                                                               
        PADDING = '`'                                                                            
        pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING                               
        secretKey = "g$yhdfh68h$gdjr6ue456dv$^%gdncTh"                                                   
        cipher = AES.new(secretKey)                                                                 
        EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))
        encoded = EncodeAES(cipher, string)
        return encoded

def filedreversehash(string):
        BLOCK_SIZE = 32
        PADDING = '`'
        pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
        secretKey = "g$yhdfh68h$gdjr6ue456dv$^%gdncTh"
        cipher = AES.new(secretKey)
        DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
        decoded = DecodeAES(cipher, string)
        return decoded

        
#updating new fields in userinfo
@zen_epms.route('/empupdate1', methods = ['GET', 'POST'])
@login_required
def empupdate1():
    # print "#### Updating Employee Status from CSV file ####"
    users=[]
    repeated=[]
    multi_profiles=[]
    all_user_data=[]
    all_results=find_and_filter('newinfo_epms',{},{"_id":0,"username":1,"emp_subgroup":1,"last_promotion_dt":1,"last_promotion_group":1})
    all_users=find_and_filter('Userinfo',{},{"_id":0,"username":1})
    for j in all_results:
        if 'last_promotion_dt' in j:
            lastpromotiondate=j['last_promotion_dt']
        elif 'last_promotion_group' in j:
            lastpromotiondate=j['last_promotion_group']

        if str(j['username']).zfill(5) not in users: #checking for user is already in users
            update_collection('Userinfo',{'username':str(j['username']).zfill(5)},{'$set':{"emp_subgroup":j['emp_subgroup'],"last_promotion_dt":lastpromotiondate}})
            users.append(str(j['username']).zfill(5)) #in not a user already appending to users
        else:
            repeated.append(str(j['username']).zfill(5)) #if user already appending to repeated user list
    for i in all_users:
        all_user_data.append(i['username'])

    test=set(all_user_data)
    for i in test:
        usrcount=all_user_data.count(i)
        multi_profiles.append({i:usrcount})

    results={"multi profiles":multi_profiles}
    return jsonify(results)

@zen_epms.route('/epmsmain/', methods = ['GET', 'POST'])
@login_required
def epms_upload():
	
	user_details = base()
	uid = current_user.username
	uid_id= current_user.get_id()
	user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid_id)},{"designation_id":1,"_id":0,"plant_id":1})
	db=dbconnect()
	dateOfJoining = user_details['doj']
	year = datetime.date.today().year
	dateafterJoining = "31-12-"+str(year-1)	
	# flash(dateOfJoining)
	dateOfJoiningCalc = time.strptime(dateOfJoining, "%d-%m-%Y")
	maxDateOfJoiningCalc = time.strptime(dateafterJoining, "%d-%m-%Y")
	
	m = datetime.date.today()
	mon='{:02d}'.format(m.month)
	finance_year=0
	emps_year=0
	val=4
	val='{:02d}'.format(val)
	if mon>=val:
		finance_year=(str(year-1))+"-"+(str(year))
		emps_year=(str(year))+"-"+(str(year+1))
	else:
		finance_year=(str(year-1))+"-"+(str(year))
		emps_year=(str(year-1))+"-"+(str(year))

	if dateOfJoiningCalc > maxDateOfJoiningCalc :
#		flash ("Render EPMS Not Eligible")
		return render_template('epmsnoteligible.html',user_details=user_details)

	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	#RoleFromDB = current_user.Role
	RoleFromDB = user_details['emp_subgroup']
	#print RoleFromDB
	if RoleFromDB in ['S1', 'S2', 'S3', 'S4', 'S5']:
		Role = "AGMAbove"
	else:
	    Role = "BelowAGM"
#	flash(Role)

	QualificationData = find_one_in_collection('Userinfo',{'username':uid})
	Qualification = QualificationData['Qualification1']
	if QualificationData['Qualification2'] != "":
	   Qualification = Qualification+"/"+QualificationData['Qualification2']
	approval_levels=[]
	user_rm=find_one_in_collection('Userinfo',{'username':uid})  #Current User Who is Logged IN
	rms_list=["m1_pid","m2_pid","m3_pid","m4_pid","m5_pid","m6_pid","m7_pid"]
	approval_list=[]
	# for rm in rms_list:
	# 	rm1_username=find_one_in_collection('Userinfo',{'designation_id':user_rm[rm]})
		
	# 	if rm1_username!= None:
	# 		approval_list.append(rm1_username['username'])

	view_levels=[]
	view_list=[]
	# print "desig",user_data[0]['designation_id']
	q=find_one_in_collection('Positions',{"position_id":user_data[0]['designation_id']})
	for rm in rms_list:
		# print"desss",user_rm[rm]
		rm1_username=find_one_in_collection('Userinfo',{'designation_id':user_rm[rm]})
		try:
			print "try lo",rm1_username['designation_id']
			if rm1_username['designation_id'] in q['levels']:
				# print "JJJJ",rm1_username['designation_id']
				approval_list.append(rm1_username['username'])
			else:
				view_list.append(rm1_username['username'])
		except:
			print "no user"
		# if rm1_username!= None:
		# 	approval_list.append(rm1_username['username'])

	for i in range(len(view_list)):
		appraiserInfo = find_one_in_collection('Userinfo',{'username':view_list[i]})
		a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
		view_levels = view_levels+[{"a_id":view_list[i],"a_status":"view",
				"a_name":a_name}]
	
	for i in range(0,len(approval_list)):
		if i==0:
			appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
			a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
			a_designation = appraiserInfo['designation']
			approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"current",
				'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
		elif i==1:
			appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
			a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
			a_designation = appraiserInfo['designation']
			approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"pending",
				'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
		else:
			logging.debug(uid+" "+"User Dosent have any Approval Levels")

	lengthOfApprovalLevels = len(approval_levels)
	
	if(lengthOfApprovalLevels==1):
	   appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[0]})
	   a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
	   a_designation = appraiserInfo['designation']
	   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
				'submit_status':"pending","a_name":"-","a_designation":"-"}]
	# print"lennn",(len(approval_levels))
	# print "Date",emps_year
	if(len(approval_list)==0):
	   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
				'submit_status':"pending","a_name":"-","a_designation":"-"}]+[{"a_id":"-","a_status":"current",
				'submit_status':"pending","a_name":"-","a_designation":"-"}]
	
	if (year == datetime.date.today().year) & (mon >= 10):
		# print "current mon",uid
		if(not find_one_in_collection('EPMSKRAData',{"UserId":uid,'Finance_year' : finance_year})):
			print "your not eligible"
			return render_template('epmsnoteligibleforPMS.html',user_details=user_details)
		else:
			employee_att = db.EPMSKRAData.aggregate([{'$match' : {'UserId': uid}},
			{ '$unwind': '$AppraisalAndKRA' },
			{'$match' : {'Finance_year' : finance_year}}])
			
			EMPSJob = find_and_filter('EPMSKRAData',{"UserId" : uid},{"JobResponsibilities":1})
			AppraisalAndKRA=[]
			for e in list(employee_att):
				AppraisalAndKRA=AppraisalAndKRA+[{"AppraisalKRAObjective":e['AppraisalAndKRA']['AppraisalKRAObjective'],"Weightage":e['AppraisalAndKRA']['Weightage'],"TargetPerUnit":e['AppraisalAndKRA']['TargetPerUnit']}]
		
		if(not find_one_in_collection('EPMSData',{"UserId":uid,'Fin_year' : emps_year})):
			
			save_collection('EPMSData',{"UserId":uid,"Role":Role,"Qualification":Qualification,
			"approval_levels":approval_levels,"AppraisalAndKRA":AppraisalAndKRA,"Fin_year":"2016-2017","JobResponsibilities":EMPSJob[0]['JobResponsibilities'],"view_levels":view_levels})
		else:
			update_collection('EPMSData',{"UserId":uid},{'$set' :{"UserId":uid,"Role":Role,"Qualification":Qualification,
			"approval_levels":approval_levels}})
	# print "Appraisal",AppraisalAndKRA
	# if(not find_one_in_collection('EPMSData',{"UserId":uid})):
	# 	update_collection('EPMSData',{"UserId":uid},{'$set' :{"UserId":uid,"Role":Role,"Qualification":Qualification,
	# 		"approval_levels":approval_levels}})

	len(approval_list)	

	if request.method == 'POST':
			form=request.form

			# if user_rm['m1_pid']!="":
			# 	rm1_username=find_one_in_collection('Userinfo',{'designation_id':user_rm['m1_pid']})
			# 	if rm1_username!= None:
			# 		approval_levels = approval_levels+[{"a_id":rm1_username['username'],"a_status":"current",
			# 		'submit_status':"pending"}]
			# if user_rm['m2_pid']!="":
			# 	rm2_username=find_one_in_collection('Userinfo',{'designation_id':user_rm['m2_pid']})
			# 	if rm2_username!=None:
			# 		approval_levels = approval_levels+[{"a_id":rm2_username['username'],"a_status":"pending",
			# 		'submit_status':"pending"}]
       


			AppraisalKRAObjective=form.getlist('AppraisalKRAObjective')
			Weightage=form.getlist('Weightage')
			TargetPerUnit=form.getlist('TargetPerUnit')
			AppraiseeAchievement=form.getlist('AppraiseeAchievement')
			AppraiserAchievement=form.getlist('AppraiserAchievement')
			KRACommentsAppraiser = form.getlist('KRACommentsAppraiser')
			KRACommentsAppraisee = form.getlist('KRACommentsAppraisee')
			CalculatedWeightage = form.getlist('CalculatedWeightage')

			AppraisalAndKRA=[]

			for i in range(0,len(AppraisalKRAObjective)):
				if AppraisalKRAObjective[i] != "":
					array={}
					array={
					"AppraisalKRAObjective":AppraisalKRAObjective[i],
					"Weightage":Weightage[i],
					"TargetPerUnit":TargetPerUnit[i],
					"AppraiseeAchievement":AppraiseeAchievement[i],
					"AppraiserAchievement":AppraiserAchievement[i],
					"KRACommentsAppraiser":KRACommentsAppraiser[i],
					"KRACommentsAppraisee":KRACommentsAppraisee[i],
					"CalculatedWeightage":CalculatedWeightage[i]
					}
					AppraisalAndKRA.append(array)			

			TrainingsAttendedTitle=form.getlist('TrainingsAttendedTitle')
			TrainingsAttendedObjectives=form.getlist('TrainingsAttendedObjectives')
			TrainingsAttendedDuration=form.getlist('TrainingsAttendedDuration')
			TrainingsAttendedUtilization=form.getlist('TrainingsAttendedUtilization')
			
			TrainingsAttended=[]

			for i in range(0,len(TrainingsAttendedTitle)):
				if TrainingsAttendedTitle[i] != "":
					array={}
					array={
					"TrainingsAttendedTitle":TrainingsAttendedTitle[i],
					"TrainingsAttendedObjectives":TrainingsAttendedObjectives[i],
					"TrainingsAttendedDuration":TrainingsAttendedDuration[i],
					"TrainingsAttendedUtilization":TrainingsAttendedUtilization[i]
					}
					TrainingsAttended.append(array)	

			TechnicalFunctional=form.getlist('TechnicalFunctional')			

			TrainingDevelopmentalNeedsTechnicalFunctional=[]

			for i in range(0,len(TechnicalFunctional)):
				if TechnicalFunctional[i] != "":
					array={}
					array={
					"TechnicalFunctional":TechnicalFunctional[i]
					}
					TrainingDevelopmentalNeedsTechnicalFunctional.append(array) 

			BehavioralAttitudinal=form.getlist('BehavioralAttitudinal')			

			TrainingDevelopmentalNeedsBehavioralAttitudinal=[]

			for i in range(0,len(BehavioralAttitudinal)):
				if BehavioralAttitudinal[i] != "":
					array={}
					array={
					"BehavioralAttitudinal":BehavioralAttitudinal[i]
					}
					TrainingDevelopmentalNeedsBehavioralAttitudinal.append(array) 

			lenTrainingDevelopmentalNeedsTechnicalFunctional = len(TrainingDevelopmentalNeedsTechnicalFunctional)
			lenTrainingDevelopmentalNeedsBehavioralAttitudinal =  len(TrainingDevelopmentalNeedsBehavioralAttitudinal)

			diff = lenTrainingDevelopmentalNeedsTechnicalFunctional - lenTrainingDevelopmentalNeedsBehavioralAttitudinal
#			print diff

			if diff > 0:
				for i in range(0,diff):
						array={}
						array={
						"BehavioralAttitudinal":""
						}
						TrainingDevelopmentalNeedsBehavioralAttitudinal.append(array) 
			if diff < 0:
				for i in range(0,diff*-1):
				 		array={}
						array={
						"TechnicalFunctional":""
						}
						TrainingDevelopmentalNeedsTechnicalFunctional.append(array)				


			Action=form.getlist('Action')
			ActionOwner=form.getlist('ActionOwner')				
			TimeFrame=form.getlist('TimeFrame')

			ActionPlan=[]

			for i in range(0,len(Action)):
				if Action[i] != "":
					array={}
					array={
					"Action":Action[i],
					"ActionOwner":ActionOwner[i],
					"TimeFrame":TimeFrame[i]					
					}
					ActionPlan.append(array) 

			if 'noTrainingsAttendedConfirmation' in form:
#				flash("Check - ON")
				noTrainingsAttendedConfirmation = "Yes"
			else:
#				flash("Check - Off",'alert-success')
				noTrainingsAttendedConfirmation = "No"

			if form['submit'] == 'Save': #SAVE

				if(find_one_in_collection('EPMSData',{"UserId":uid,"SubmitIndicator":"Yes","Fin_year":emps_year})):
					print("Submitted already")
					flash("Your E-PMS has been submitted already and cannot be edited,saved or resubmitted",'alert-success')
					EPMSData = find_one_in_collection('EPMSData',{"UserId" : uid,"Fin_year":"2016-2017"})
					Yes = "Yes"
					EPMSTrainingProgramsTechnicalConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Technical,"Conducted":Yes},[("SkillName",1)])
					EPMSTrainingProgramsSoftConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Soft,"Conducted":Yes},[("SkillName",1)])
					EPMSTrainingProgramsBehaviouralConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Behavioural,"Conducted":Yes},[("SkillName",1)])
					EPMSTrainingProgramsQualityConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Quality,"Conducted":Yes},[("SkillName",1)])
					EPMSTrainingProgramsITConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":IT,"Conducted":Yes},[("SkillName",1)])	

					EPMSTrainingProgramsTechnicalRequired = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Technical,"Required":Yes},[("DeptName",1)])
					EPMSTrainingProgramsSoftRequired = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Soft,"Required":Yes},[("SkillName",1)])

					EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})

					return render_template('epmsmain.html', user_details=user_details,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)



				flash("Your E-PMS details are saved successfully",'alert-success')
				update_collection('EPMSData',{"UserId":uid,"Fin_year":emps_year},
					{'$set' :
					{
					"approval_levels":approval_levels,
					"view_levels":view_levels,
					"UserId":uid,
					"UserEmail":user_details['official_email'],
					"JobResponsibilities":form['JobResponsibilities'] , 
					"noTrainingsAttendedConfirmation":noTrainingsAttendedConfirmation,
					"AppraisalAndKRA":AppraisalAndKRA,
					"AchievementOrientation-AppraiseeRating":form['AchievementOrientation-AppraiseeRating'],
					"AchievementOrientation-AppraiseeRemarks":form['AchievementOrientation-AppraiseeRemarks'],
					"AchievementOrientation-AppriserRating":form['AchievementOrientation-AppriserRating'],
					"AchievementOrientation-AppraiserRemarks":form['AchievementOrientation-AppraiserRemarks'],
					"CommitmentToExcellence-AppraiseeRating":form['CommitmentToExcellence-AppraiseeRating'],
					"CommitmentToExcellence-AppraiseeRemarks":form['CommitmentToExcellence-AppraiseeRemarks'],
					"CommitmentToExcellence-AppriserRating":form['CommitmentToExcellence-AppriserRating'],
					"CommitmentToExcellence-AppraiserRemarks":form['CommitmentToExcellence-AppraiserRemarks'],
					"CommunicationSkills-AppraiseeRating":form['CommunicationSkills-AppraiseeRating'],
					"CommunicationSkills-AppraiseeRemarks":form['CommunicationSkills-AppraiseeRemarks'],
					"CommunicationSkills-AppriserRating":form['CommunicationSkills-AppriserRating'],
					"CommunicationSkills-AppraiserRemarks":form['CommunicationSkills-AppraiserRemarks'],
					"TeamBuilding-AppraiseeRating":form['TeamBuilding-AppraiseeRating'],
					"TeamBuilding-AppraiseeRemarks":form['TeamBuilding-AppraiseeRemarks'],
					"TeamBuilding-AppriserRating":form['TeamBuilding-AppriserRating'],
					"TeamBuilding-AppraiserRemarks":form['TeamBuilding-AppraiserRemarks'],
					"StrategicThinking-AppraiseeRating":form['StrategicThinking-AppraiseeRating'],
					"StrategicThinking-AppraiseeRemarks":form['StrategicThinking-AppraiseeRemarks'],
					"StrategicThinking-AppriserRating":form['StrategicThinking-AppriserRating'],
					"StrategicThinking-AppraiserRemarks":form['StrategicThinking-AppraiserRemarks'],
					"PeopleDevelopment-AppraiseeRating":form['PeopleDevelopment-AppraiseeRating'],
					"PeopleDevelopment-AppraiseeRemarks":form['PeopleDevelopment-AppraiseeRemarks'],
					"PeopleDevelopment-AppriserRating":form['PeopleDevelopment-AppriserRating'],
					"PeopleDevelopment-AppraiserRemarks":form['PeopleDevelopment-AppraiserRemarks'],
					"LeadershipSkills-AppraiseeRating":form['LeadershipSkills-AppraiseeRating'],
					"LeadershipSkills-AppraiseeRemarks":form['LeadershipSkills-AppraiseeRemarks'],
					"LeadershipSkills-AppriserRating":form['LeadershipSkills-AppriserRating'],
					"LeadershipSkills-AppraiserRemarks":form['LeadershipSkills-AppraiserRemarks'],
					"ToDepartmentFunction-AppraiseeComments":form['ToDepartmentFunction-AppraiseeComments'],
					"ToDepartmentFunction-AppraiserComments":form['ToDepartmentFunction-AppraiserComments'],
					"TrainingsAttended":TrainingsAttended,
					"TrainingDevelopmentalNeedsTechnicalFunctional":TrainingDevelopmentalNeedsTechnicalFunctional,
					"TrainingDevelopmentalNeedsBehavioralAttitudinal":TrainingDevelopmentalNeedsBehavioralAttitudinal,
					"ActionPlan":ActionPlan,
					"PerformanceWeightage":form['PerformanceWeightage'],
					"PerformanceAchievement":form['PerformanceAchievement'],
					"PerformanceRemarks":form['PerformanceRemarks'],
					"CompetenciesWeightage":form['CompetenciesWeightage'],
					"CompetenciesAchievement":form['CompetenciesAchievement'],
					"CompetenciesRemarks":form['CompetenciesRemarks'],
					"OverallAchievementWeightage":form['OverallAchievementWeightage'],
					"OverallAchievementAchievement":form['OverallAchievementAchievement'],
					"OverallAchievementRemarks":form['OverallAchievementRemarks'],
					"OverallRatingWeightage":form['OverallRatingWeightage'],
					"OverallRatingAchievement":form['OverallRatingAchievement'],
					"OverallRatingRemarks":form['OverallRatingRemarks'],
					"Final-AppraiseeComments":form['Final-AppraiseeComments'],
					"Final-AppraiserComments":form['Final-AppraiserComments'],
					"Final-ReviewerComments":form['Final-ReviewerComments'],
					"otherTrainingNeeds":form['otherTrainingNeeds'],
					"SavedIndicator":"Yes",
					"SubmitIndicator_Reviewer":"No",
					"SubmitIndicator_Appraiser":"No",
					"SubmitIndicator" : "No",
					"SavedOn":datetime.datetime.now(),
					"Role":Role,
					"Fin_year":emps_year
					}
				  }
				)
			
			else: #SUBMIT
				flash("Your E-PMS details are submitted successfully",'alert-success')
				update_collection('EPMSData',{"UserId":uid,"Fin_year":"2016-2017"},
					{'$set' :
					{
					"approval_levels":approval_levels,
					"view_levels":view_levels,
					"UserId":uid,
					"UserEmail":user_details['official_email'],
					"JobResponsibilities":form['JobResponsibilities'] , 
					"AppraisalAndKRA":AppraisalAndKRA,
					"noTrainingsAttendedConfirmation":noTrainingsAttendedConfirmation,
					"AchievementOrientation-AppraiseeRating":form['AchievementOrientation-AppraiseeRating'],
					"AchievementOrientation-AppraiseeRemarks":form['AchievementOrientation-AppraiseeRemarks'],
					"AchievementOrientation-AppriserRating":form['AchievementOrientation-AppriserRating'],
					"AchievementOrientation-AppraiserRemarks":form['AchievementOrientation-AppraiserRemarks'],
					"CommitmentToExcellence-AppraiseeRating":form['CommitmentToExcellence-AppraiseeRating'],
					"CommitmentToExcellence-AppraiseeRemarks":form['CommitmentToExcellence-AppraiseeRemarks'],
					"CommitmentToExcellence-AppriserRating":form['CommitmentToExcellence-AppriserRating'],
					"CommitmentToExcellence-AppraiserRemarks":form['CommitmentToExcellence-AppraiserRemarks'],
					"CommunicationSkills-AppraiseeRating":form['CommunicationSkills-AppraiseeRating'],
					"CommunicationSkills-AppraiseeRemarks":form['CommunicationSkills-AppraiseeRemarks'],
					"CommunicationSkills-AppriserRating":form['CommunicationSkills-AppriserRating'],
					"CommunicationSkills-AppraiserRemarks":form['CommunicationSkills-AppraiserRemarks'],
					"TeamBuilding-AppraiseeRating":form['TeamBuilding-AppraiseeRating'],
					"TeamBuilding-AppraiseeRemarks":form['TeamBuilding-AppraiseeRemarks'],
					"TeamBuilding-AppriserRating":form['TeamBuilding-AppriserRating'],
					"TeamBuilding-AppraiserRemarks":form['TeamBuilding-AppraiserRemarks'],
					"StrategicThinking-AppraiseeRating":form['StrategicThinking-AppraiseeRating'],
					"StrategicThinking-AppraiseeRemarks":form['StrategicThinking-AppraiseeRemarks'],
					"StrategicThinking-AppriserRating":form['StrategicThinking-AppriserRating'],
					"StrategicThinking-AppraiserRemarks":form['StrategicThinking-AppraiserRemarks'],
					"PeopleDevelopment-AppraiseeRating":form['PeopleDevelopment-AppraiseeRating'],
					"PeopleDevelopment-AppraiseeRemarks":form['PeopleDevelopment-AppraiseeRemarks'],
					"PeopleDevelopment-AppriserRating":form['PeopleDevelopment-AppriserRating'],
					"PeopleDevelopment-AppraiserRemarks":form['PeopleDevelopment-AppraiserRemarks'],
					"LeadershipSkills-AppraiseeRating":form['LeadershipSkills-AppraiseeRating'],
					"LeadershipSkills-AppraiseeRemarks":form['LeadershipSkills-AppraiseeRemarks'],
					"LeadershipSkills-AppriserRating":form['LeadershipSkills-AppriserRating'],
					"LeadershipSkills-AppraiserRemarks":form['LeadershipSkills-AppraiserRemarks'],
					"ToDepartmentFunction-AppraiseeComments":form['ToDepartmentFunction-AppraiseeComments'],
					"ToDepartmentFunction-AppraiserComments":form['ToDepartmentFunction-AppraiserComments'],
					"TrainingsAttended":TrainingsAttended,
					"TrainingDevelopmentalNeedsTechnicalFunctional":TrainingDevelopmentalNeedsTechnicalFunctional,
					"TrainingDevelopmentalNeedsBehavioralAttitudinal":TrainingDevelopmentalNeedsBehavioralAttitudinal,
					"ActionPlan":ActionPlan,
					"PerformanceWeightage":form['PerformanceWeightage'],
					"PerformanceAchievement":form['PerformanceAchievement'],
					"PerformanceRemarks":form['PerformanceRemarks'],
					"CompetenciesWeightage":form['CompetenciesWeightage'],
					"CompetenciesAchievement":form['CompetenciesAchievement'],
					"CompetenciesRemarks":form['CompetenciesRemarks'],
					"OverallAchievementWeightage":form['OverallAchievementWeightage'],
					"OverallAchievementAchievement":form['OverallAchievementAchievement'],
					"OverallAchievementRemarks":form['OverallAchievementRemarks'],
					"OverallRatingWeightage":form['OverallRatingWeightage'],
					"OverallRatingAchievement":form['OverallRatingAchievement'],
					"OverallRatingRemarks":form['OverallRatingRemarks'],
					"Final-AppraiseeComments":form['Final-AppraiseeComments'],
					"Final-AppraiserComments":form['Final-AppraiserComments'],
					"Final-ReviewerComments":form['Final-ReviewerComments'],
					"otherTrainingNeeds":form['otherTrainingNeeds'],
					"SavedIndicator":"Yes",
					"SubmitIndicator_Appraiser":"No",
					"SubmitIndicator" : "Yes",
					"SubmitIndicator_Reviewer":"No",
					"SavedOn":datetime.datetime.now(),
					"SubmittedOn":datetime.datetime.now(),
					"Role":Role,
					"Fin_year":emps_year
					}
					}
				)			   
				
	EPMSData = find_one_in_collection('EPMSData',{"UserId" : uid,"Fin_year":emps_year})
	Yes = "Yes"
	# print "RM",EPMSData
	EPMSTrainingProgramsTechnicalConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Technical,"Conducted":Yes},[("SkillName",1)])
	EPMSTrainingProgramsSoftConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Soft,"Conducted":Yes},[("SkillName",1)])
	EPMSTrainingProgramsBehaviouralConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Behavioural,"Conducted":Yes},[("SkillName",1)])
	EPMSTrainingProgramsQualityConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Quality,"Conducted":Yes},[("SkillName",1)])
	EPMSTrainingProgramsITConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":IT,"Conducted":Yes},[("SkillName",1)])	

	EPMSTrainingProgramsTechnicalRequired = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Technical,"Required":Yes},[("DeptName",1)])
	EPMSTrainingProgramsSoftRequired = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Soft,"Required":Yes},[("SkillName",1)])

	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})

	return render_template('epmsmain.html', user_details=user_details,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)

@zen_epms.route('/appraiser_view/', methods = ['GET', 'POST'])
@login_required
def epms_appraiser():
	user_details = base()
	finance_year=request.args['finyear']
	uid = request.args['uid']
	
	db=dbconnect()
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		employee_att = find_in_collection("EPMSData",{'SubmitIndicator':  "Yes","Fin_year":finance_year,'SubmitIndicator_Reviewer' : "Yes"});
		print employee_att
	else:
		employee_att = db.EPMSData.aggregate([{'$match' : {'SubmitIndicator':  "Yes","Fin_year":finance_year}},
			{ '$unwind': '$approval_levels' },
	                {'$match' : {'approval_levels.a_id' : uid}}])
		employee_att = list(employee_att)

	a=[]
	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	for e in employee_att:
		e['fullname']=get_employee_name(e['UserId'])
		print "name:",e['Fin_year']
		e['SavedOn'] = e['SavedOn'].strftime("%d/%m/%Y")
		e['UserId']=e['UserId']
		e['SubmitIndicator'] = e['SubmitIndicator']
		e['Fin_year']=e['Fin_year']
		a=a+[e]

	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})

	return render_template('epms_pms_view.html', user_details=user_details,list=a)

#PMS 7 levels view
@zen_epms.route('/epms_pms_teamview/', methods = ['GET', 'POST'])
@login_required
def epms_pms_teamview():
	user_details = base()
	finance_year=request.args['finyear']
	uid = request.args['uid']
	db=dbconnect()
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		# print "Hello"
		employee_att = find_in_collection("EPMSData",{'SubmitIndicator':  "Yes","Fin_year":finance_year,'SubmitIndicator_Reviewer' : "Yes"});
		# print employee_att
	else:
		employee_att = db.EPMSData.aggregate([{'$match' : {'SubmitIndicator':  "Yes","Fin_year":finance_year}},
			{ '$unwind': '$view_levels' },
	                {'$match' : {'view_levels.a_id' : uid}}])
		employee_att = list(employee_att)
	a=[]
	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	for e in employee_att:
		e['fullname']=get_employee_name(e['UserId'])
		# print "name:",e['Fin_year']
		e['SavedOn'] = e['SavedOn'].strftime("%d/%m/%Y")
		e['UserId']=e['UserId']
		e['SubmitIndicator'] = e['SubmitIndicator']
		e['Fin_year']=e['Fin_year']
		a=a+[e]

	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})

	return render_template('emps_pms_team_list_view.html', user_details=user_details,list=a)


@zen_epms.route('/appraiser_info_view', methods = ['GET', 'POST'])
@login_required
def appraiser_detailed_view():

	user_details = base()
	uid = current_user.username
	uid_id= current_user.get_id()
	user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid_id)},{"designation_id":1,"_id":0,"plant_id":1})

	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	result_appraiser=[]
	result_reviewer=[]
	db=dbconnect()
	employee_att = db.EPMSData.aggregate([{'$match' : {'SubmitIndicator': "Yes"}},
	{ '$unwind': '$approval_levels' },
	{'$match' : {'approval_levels.a_id' : uid}}])

	employee_att0 = db.EPMSData.aggregate([{'$match' : {'approval_levels.0.a_id' : uid}}])
	for emp0 in list(employee_att0):
		result_appraiser.append(emp0['UserId'])


	employee_att1 = db.EPMSData.aggregate([{'$match' : {'approval_levels.1.a_id' : uid}}])
	for emp1 in list(employee_att1):
		result_reviewer.append(emp1['UserId'])

	status=[result_appraiser,result_reviewer]

	a=[]

	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	
	for e in list(employee_att):
		e['fullname']=get_employee_name(e['UserId'])
		e['SavedOn'] = e['SavedOn'].strftime("%d/%m/%Y")
		e['UserId']=e['UserId']
		e['SubmitIndicator'] = e['SubmitIndicator']
		a=a+[e]
#	flash("Manager Save",'alert-success')
	
	if request.method == 'POST':
			form=request.form
			AppraisalKRAObjective=form.getlist('AppraisalKRAObjective')
			Weightage=form.getlist('Weightage')
			TargetPerUnit=form.getlist('TargetPerUnit')
			AppraiseeAchievement=form.getlist('AppraiseeAchievement')
			AppraiserAchievement=form.getlist('AppraiserAchievement')
			KRACommentsAppraiser = form.getlist('KRACommentsAppraiser')
			KRACommentsAppraisee = form.getlist('KRACommentsAppraisee')
			CalculatedWeightage = form.getlist('CalculatedWeightage')

			

			AppraisalAndKRA=[]
			
			for i in range(0,len(AppraisalKRAObjective)):
				if AppraisalKRAObjective[i] != "":
					array={}
					array={
					"AppraisalKRAObjective":AppraisalKRAObjective[i],
					"Weightage":Weightage[i],
					"TargetPerUnit":TargetPerUnit[i],
					"AppraiseeAchievement":AppraiseeAchievement[i],
					"AppraiserAchievement":AppraiserAchievement[i],
					"KRACommentsAppraiser":KRACommentsAppraiser[i],
					"KRACommentsAppraisee":KRACommentsAppraisee[i],
					"CalculatedWeightage":CalculatedWeightage[i]
					}
					AppraisalAndKRA.append(array)			

			TrainingsAttendedTitle=form.getlist('TrainingsAttendedTitle')
			TrainingsAttendedObjectives=form.getlist('TrainingsAttendedObjectives')
			TrainingsAttendedDuration=form.getlist('TrainingsAttendedDuration')
			TrainingsAttendedUtilization=form.getlist('TrainingsAttendedUtilization')
#			print TrainingsAttendedTitle

			TrainingsAttended=[]

			for i in range(0,len(TrainingsAttendedTitle)):
				if TrainingsAttendedTitle[i] != "":					
					array={}
					array={
					"TrainingsAttendedTitle":TrainingsAttendedTitle[i],
					"TrainingsAttendedObjectives":TrainingsAttendedObjectives[i],
					"TrainingsAttendedDuration":TrainingsAttendedDuration[i],
					"TrainingsAttendedUtilization":TrainingsAttendedUtilization[i]
					}
					TrainingsAttended.append(array)	
#			print TrainingsAttended
			TechnicalFunctional=form.getlist('TechnicalFunctional')			
#			print TechnicalFunctional
			TrainingDevelopmentalNeedsTechnicalFunctional=[]

			for i in range(0,len(TechnicalFunctional)):
				if TechnicalFunctional[i] != "":
					array={}
					array={
					"TechnicalFunctional":TechnicalFunctional[i]
					}
					TrainingDevelopmentalNeedsTechnicalFunctional.append(array) 

			BehavioralAttitudinal=form.getlist('BehavioralAttitudinal')			

			TrainingDevelopmentalNeedsBehavioralAttitudinal=[]

			for i in range(0,len(BehavioralAttitudinal)):
				if BehavioralAttitudinal[i] != "":
					array={}
					array={
					"BehavioralAttitudinal":BehavioralAttitudinal[i]
					}
					TrainingDevelopmentalNeedsBehavioralAttitudinal.append(array) 

			lenTrainingDevelopmentalNeedsTechnicalFunctional = len(TrainingDevelopmentalNeedsTechnicalFunctional)
			lenTrainingDevelopmentalNeedsBehavioralAttitudinal =  len(TrainingDevelopmentalNeedsBehavioralAttitudinal)

			diff = lenTrainingDevelopmentalNeedsTechnicalFunctional - lenTrainingDevelopmentalNeedsBehavioralAttitudinal
#			print diff

			if diff > 0:
				for i in range(0,diff):
						array={}
						array={
						"BehavioralAttitudinal":""
						}
						TrainingDevelopmentalNeedsBehavioralAttitudinal.append(array) 
			if diff < 0:
				for i in range(0,diff*-1):
				 		array={}
						array={
						"TechnicalFunctional":""
						}
						TrainingDevelopmentalNeedsTechnicalFunctional.append(array)	

#			print TrainingDevelopmentalNeedsTechnicalFunctional
			Action=form.getlist('Action')
			ActionOwner=form.getlist('ActionOwner')				
			TimeFrame=form.getlist('TimeFrame')

			ActionPlan=[]

			for i in range(0,len(Action)):
				if Action[i] != "":
					array={}
					array={
					"Action":Action[i],
					"ActionOwner":ActionOwner[i],
					"TimeFrame":TimeFrame[i]					
					}
					ActionPlan.append(array) 
			
			EPMSData=find_one_in_collection('EPMSData',{'_id':ObjectId(form['prid'])})
			
			user_details1=find_one_in_collection('Userinfo',{'username':form['userid']})
			
			EPMSTrainingProgramsTechnicalConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Technical,"Conducted":Yes},[("SkillName",1)])
			EPMSTrainingProgramsSoftConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Soft,"Conducted":Yes},[("SkillName",1)])
			EPMSTrainingProgramsBehaviouralConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Behavioural,"Conducted":Yes},[("SkillName",1)])
			EPMSTrainingProgramsQualityConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Quality,"Conducted":Yes},[("SkillName",1)])
			EPMSTrainingProgramsITConducted = find_in_collection_sort('EPMSTrainingPrograms',{"Type":IT,"Conducted":Yes},[("SkillName",1)])			
			
			EPMSTrainingProgramsTechnicalRequired = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Technical,"Required":Yes},[("DeptName",1)])
			EPMSTrainingProgramsSoftRequired = find_in_collection_sort('EPMSTrainingPrograms',{"Type":Soft,"Required":Yes},[("SkillName",1)])
			
			if EPMSData["SubmitIndicator_Appraiser"]=="Yes":
			   varSubmitIndicator_Appraiser = "Yes"
			else:
			   varSubmitIndicator_Appraiser = "No"


			if form['submit'] == "VIEW":
				# print form['userid'],'user to be viewed'
				hashfields_view=['PerformanceWeightage','CompetenciesWeightage','OverallAchievementWeightage','OverallRatingWeightage','Final-ReviewerComments','Final-AppraiserComments']
				for i in hashfields_view:
					if EPMSData[i] != "":
						EPMSData[i]=filedreversehash(EPMSData[i])
				count_a_levels=len(EPMSData['approval_levels'])
#				print EPMSData["approval_levels"][0]["a_id"]
#				print uid
				if count_a_levels==2:
					if EPMSData["SubmitIndicator_Appraiser"]=="No" or EPMSData["approval_levels"][0]["a_id"] == uid:
						EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
						return render_template('epms_appraiser_detailed_view.html',user_details=user_details, status=status,user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)
					else:
						EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
						return render_template('epms_reviewer_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)
				else:
					EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
					return render_template('epms_appraiser_detailed_view.html',user_details=user_details,status=status, user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)
			elif form['submit'] == 'TEAMVIEW':
				hashfields_view=['PerformanceWeightage','CompetenciesWeightage','OverallAchievementWeightage','OverallRatingWeightage','Final-ReviewerComments','Final-AppraiserComments']
				for i in hashfields_view:
					if EPMSData[i] != "":
						EPMSData[i]=filedreversehash(EPMSData[i])
				EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
				return render_template('epms_pms_detailed_teamview.html',user_details=user_details,status=status, user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)


			elif form['submit'] == 'Save':

					data={}
					hashfields_list=['PerformanceWeightage','CompetenciesWeightage','OverallAchievementWeightage','OverallRatingWeightage','Final-ReviewerComments','Final-AppraiserComments']
					for i in hashfields_list:
						if form[i] != "":
							data[i]=fieldhash(form[i])
						else:
							data[i]=""

					uid = form['prid']
					update_collection('EPMSData',{"_id":ObjectId(uid)},
						{'$set' :
							{
							"Test" : "Sample",
							"JobResponsibilities":form['JobResponsibilities'] , 
							"AppraisalAndKRA":AppraisalAndKRA,
							"AchievementOrientation-AppraiseeRating":form['AchievementOrientation-AppraiseeRating'],
							"AchievementOrientation-AppraiseeRemarks":form['AchievementOrientation-AppraiseeRemarks'],
							"AchievementOrientation-AppriserRating":form['AchievementOrientation-AppriserRating'],
							"AchievementOrientation-AppraiserRemarks":form['AchievementOrientation-AppraiserRemarks'],
							"CommitmentToExcellence-AppraiseeRating":form['CommitmentToExcellence-AppraiseeRating'],
							"CommitmentToExcellence-AppraiseeRemarks":form['CommitmentToExcellence-AppraiseeRemarks'],
							"CommitmentToExcellence-AppriserRating":form['CommitmentToExcellence-AppriserRating'],
							"CommitmentToExcellence-AppraiserRemarks":form['CommitmentToExcellence-AppraiserRemarks'],
							"CommunicationSkills-AppraiseeRating":form['CommunicationSkills-AppraiseeRating'],
							"CommunicationSkills-AppraiseeRemarks":form['CommunicationSkills-AppraiseeRemarks'],
							"CommunicationSkills-AppriserRating":form['CommunicationSkills-AppriserRating'],
							"CommunicationSkills-AppraiserRemarks":form['CommunicationSkills-AppraiserRemarks'],
							"TeamBuilding-AppraiseeRating":form['TeamBuilding-AppraiseeRating'],
							"TeamBuilding-AppraiseeRemarks":form['TeamBuilding-AppraiseeRemarks'],
							"TeamBuilding-AppriserRating":form['TeamBuilding-AppriserRating'],
							"TeamBuilding-AppraiserRemarks":form['TeamBuilding-AppraiserRemarks'],
							"StrategicThinking-AppraiseeRating":form['StrategicThinking-AppraiseeRating'],
							"StrategicThinking-AppraiseeRemarks":form['StrategicThinking-AppraiseeRemarks'],
							"StrategicThinking-AppriserRating":form['StrategicThinking-AppriserRating'],
							"StrategicThinking-AppraiserRemarks":form['StrategicThinking-AppraiserRemarks'],
							"PeopleDevelopment-AppraiseeRating":form['PeopleDevelopment-AppraiseeRating'],
							"PeopleDevelopment-AppraiseeRemarks":form['PeopleDevelopment-AppraiseeRemarks'],
							"PeopleDevelopment-AppriserRating":form['PeopleDevelopment-AppriserRating'],
							"PeopleDevelopment-AppraiserRemarks":form['PeopleDevelopment-AppraiserRemarks'],
							"LeadershipSkills-AppraiseeRating":form['LeadershipSkills-AppraiseeRating'],
							"LeadershipSkills-AppraiseeRemarks":form['LeadershipSkills-AppraiseeRemarks'],
							"LeadershipSkills-AppriserRating":form['LeadershipSkills-AppriserRating'],
							"LeadershipSkills-AppraiserRemarks":form['LeadershipSkills-AppraiserRemarks'],
							"ToDepartmentFunction-AppraiseeComments":form['ToDepartmentFunction-AppraiseeComments'],
							"ToDepartmentFunction-AppraiserComments":form['ToDepartmentFunction-AppraiserComments'],
							"TrainingsAttended":TrainingsAttended,
							"TrainingDevelopmentalNeedsTechnicalFunctional":TrainingDevelopmentalNeedsTechnicalFunctional,
							"TrainingDevelopmentalNeedsBehavioralAttitudinal":TrainingDevelopmentalNeedsBehavioralAttitudinal,
							"ActionPlan":ActionPlan,
							"PerformanceWeightage":data['PerformanceWeightage'],
							"PerformanceAchievement":form['PerformanceAchievement'],
							"PerformanceRemarks":form['PerformanceRemarks'],
							"CompetenciesWeightage":data['CompetenciesWeightage'],
							"CompetenciesAchievement":form['CompetenciesAchievement'],
							"CompetenciesRemarks":form['CompetenciesRemarks'],
							"OverallAchievementWeightage":data['OverallAchievementWeightage'],
							"OverallAchievementAchievement":form['OverallAchievementAchievement'],
							"OverallAchievementRemarks":form['OverallAchievementRemarks'],
							"OverallRatingWeightage":data['OverallRatingWeightage'],
							"OverallRatingAchievement":form['OverallRatingAchievement'],
							"OverallRatingRemarks":form['OverallRatingRemarks'],
							"Final-AppraiseeComments":form['Final-AppraiseeComments'],
							"Final-AppraiserComments":data['Final-AppraiserComments'],
							"Final-ReviewerComments":data['Final-ReviewerComments'],
							"otherTrainingNeeds":form['otherTrainingNeeds'],
							"SavedIndicator":"Yes",
							"SubmitIndicator" : "Yes",
							"SubmitIndicator_Appraiser":varSubmitIndicator_Appraiser,
							"SubmitIndicator_Reviewer":"No",
							"SavedOn":datetime.datetime.now(),
							}
					   }
					)
					flash("Your E-PMS details are saved successfully - Manager",'alert-success')

					EPMSData=find_one_in_collection('EPMSData',{'_id':ObjectId(form['prid'])})
					EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
					count_a_levels=len(EPMSData['approval_levels'])

					hashfields_view=['PerformanceWeightage','CompetenciesWeightage','OverallAchievementWeightage','OverallRatingWeightage','Final-ReviewerComments','Final-AppraiserComments']
					for i in hashfields_view:
						if EPMSData[i] != "":
							EPMSData[i]=filedreversehash(EPMSData[i])

	#				print EPMSData["approval_levels"][0]["a_id"]
	#				print uid
					if count_a_levels==2:
						if EPMSData["SubmitIndicator_Appraiser"]=="No" or EPMSData["approval_levels"][0]["a_id"] == uid:
							return render_template('epms_appraiser_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)
						else:
							return render_template('epms_reviewer_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)
					else:
						return render_template('epms_appraiser_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)


						return render_template('epms_appraiser_detailed_view.html',user_details=user_details,status=status, user_details1=user_details1,EPMSData=EPMSData,EPMSTrainingProgramsTechnicalConducted=EPMSTrainingProgramsTechnicalConducted,EPMSTrainingProgramsSoftConducted=EPMSTrainingProgramsSoftConducted,EPMSTrainingProgramsBehaviouralConducted=EPMSTrainingProgramsBehaviouralConducted,EPMSTrainingProgramsQualityConducted=EPMSTrainingProgramsQualityConducted,EPMSTrainingProgramsITConducted=EPMSTrainingProgramsITConducted,EPMSTrainingProgramsTechnicalRequired=EPMSTrainingProgramsTechnicalRequired,EPMSTrainingProgramsSoftRequired=EPMSTrainingProgramsSoftRequired,EPMSGlobalSettings=EPMSGlobalSettings)

			else: # Submit
					uid = form['prid']
					print"PMS Submit"
					data={}
					hashfields_list=['PerformanceWeightage','CompetenciesWeightage','OverallAchievementWeightage','OverallRatingWeightage','Final-ReviewerComments','Final-AppraiserComments']
					for i in hashfields_list:
						if form[i] != "":
							data[i]=fieldhash(form[i])
						else:
							data[i]=""
					update_collection('EPMSData',{"_id":ObjectId(uid)},
						{'$set' :
							{
							"Test" : "Sample",
							"JobResponsibilities":form['JobResponsibilities'] , 
							"AppraisalAndKRA":AppraisalAndKRA,
							"AchievementOrientation-AppraiseeRating":form['AchievementOrientation-AppraiseeRating'],
							"AchievementOrientation-AppraiseeRemarks":form['AchievementOrientation-AppraiseeRemarks'],
							"AchievementOrientation-AppriserRating":form['AchievementOrientation-AppriserRating'],
							"AchievementOrientation-AppraiserRemarks":form['AchievementOrientation-AppraiserRemarks'],
							"CommitmentToExcellence-AppraiseeRating":form['CommitmentToExcellence-AppraiseeRating'],
							"CommitmentToExcellence-AppraiseeRemarks":form['CommitmentToExcellence-AppraiseeRemarks'],
							"CommitmentToExcellence-AppriserRating":form['CommitmentToExcellence-AppriserRating'],
							"CommitmentToExcellence-AppraiserRemarks":form['CommitmentToExcellence-AppraiserRemarks'],
							"CommunicationSkills-AppraiseeRating":form['CommunicationSkills-AppraiseeRating'],
							"CommunicationSkills-AppraiseeRemarks":form['CommunicationSkills-AppraiseeRemarks'],
							"CommunicationSkills-AppriserRating":form['CommunicationSkills-AppriserRating'],
							"CommunicationSkills-AppraiserRemarks":form['CommunicationSkills-AppraiserRemarks'],
							"TeamBuilding-AppraiseeRating":form['TeamBuilding-AppraiseeRating'],
							"TeamBuilding-AppraiseeRemarks":form['TeamBuilding-AppraiseeRemarks'],
							"TeamBuilding-AppriserRating":form['TeamBuilding-AppriserRating'],
							"TeamBuilding-AppraiserRemarks":form['TeamBuilding-AppraiserRemarks'],
							"StrategicThinking-AppraiseeRating":form['StrategicThinking-AppraiseeRating'],
							"StrategicThinking-AppraiseeRemarks":form['StrategicThinking-AppraiseeRemarks'],
							"StrategicThinking-AppriserRating":form['StrategicThinking-AppriserRating'],
							"StrategicThinking-AppraiserRemarks":form['StrategicThinking-AppraiserRemarks'],
							"PeopleDevelopment-AppraiseeRating":form['PeopleDevelopment-AppraiseeRating'],
							"PeopleDevelopment-AppraiseeRemarks":form['PeopleDevelopment-AppraiseeRemarks'],
							"PeopleDevelopment-AppriserRating":form['PeopleDevelopment-AppriserRating'],
							"PeopleDevelopment-AppraiserRemarks":form['PeopleDevelopment-AppraiserRemarks'],
							"LeadershipSkills-AppraiseeRating":form['LeadershipSkills-AppraiseeRating'],
							"LeadershipSkills-AppraiseeRemarks":form['LeadershipSkills-AppraiseeRemarks'],
							"LeadershipSkills-AppriserRating":form['LeadershipSkills-AppriserRating'],
							"LeadershipSkills-AppraiserRemarks":form['LeadershipSkills-AppraiserRemarks'],
							"ToDepartmentFunction-AppraiseeComments":form['ToDepartmentFunction-AppraiseeComments'],
							"ToDepartmentFunction-AppraiserComments":form['ToDepartmentFunction-AppraiserComments'],
							"TrainingsAttended":TrainingsAttended,
							"TrainingDevelopmentalNeedsTechnicalFunctional":TrainingDevelopmentalNeedsTechnicalFunctional,
							"TrainingDevelopmentalNeedsBehavioralAttitudinal":TrainingDevelopmentalNeedsBehavioralAttitudinal,
							"ActionPlan":ActionPlan,
							"PerformanceWeightage":data['PerformanceWeightage'],
							"PerformanceAchievement":form['PerformanceAchievement'],
							"PerformanceRemarks":form['PerformanceRemarks'],
							"CompetenciesWeightage":data['CompetenciesWeightage'],
							"CompetenciesAchievement":form['CompetenciesAchievement'],
							"CompetenciesRemarks":form['CompetenciesRemarks'],
							"OverallAchievementWeightage":data['OverallAchievementWeightage'],
							"OverallAchievementAchievement":form['OverallAchievementAchievement'],
							"OverallAchievementRemarks":form['OverallAchievementRemarks'],
							"OverallRatingWeightage":data['OverallRatingWeightage'],
							"OverallRatingAchievement":form['OverallRatingAchievement'],
							"OverallRatingRemarks":form['OverallRatingRemarks'],
							"Final-AppraiseeComments":form['Final-AppraiseeComments'],
							"Final-AppraiserComments":data['Final-AppraiserComments'],
							"Final-ReviewerComments":data['Final-ReviewerComments'],
							"otherTrainingNeeds":form['otherTrainingNeeds'],
							"SavedIndicator":"Yes",
							"SubmitIndicator" : "Yes",
							"SubmitIndicator_Appraiser":"No",
							"SubmitIndicator_Reviewer":"No",
							"SavedOn":datetime.datetime.now(),
							}
					   }
					)
					flash("Your E-PMS details are submitted successfully - Manager",'alert-success')
					hashfields_view=['PerformanceWeightage','CompetenciesWeightage','OverallAchievementWeightage','OverallRatingWeightage','Final-ReviewerComments','Final-AppraiserComments']
					for i in hashfields_view:
						if EPMSData[i] != "":
							EPMSData[i]=filedreversehash(EPMSData[i])
					a_levels = find_one_in_collection('EPMSData',{"_id":ObjectId(form['prid'])})
					count_a_levels=len(a_levels['approval_levels'])
					for i in range(0,count_a_levels):
						if a_levels['approval_levels'][i]['a_status']=="current":
							j=i
							if j==count_a_levels-1:
								array3={"SubmitIndicator" : "Yes","SubmitIndicator_Appraiser":"Yes","SubmitIndicator_Reviewer":"Yes","approval_levels."+str(i)+".a_status":"approved","approval_levels."+str(i)+".submit_status":"submitted"}
								
							else:
								array3={"SubmitIndicator" : "Yes","SubmitIndicator_Appraiser":"Yes","approval_levels."+str(i)+".a_status":"approved","approval_levels."+str(i)+".submit_status":"submitted","approval_levels."+str(i+1)+".a_status":"current"}
								
					update_collection('EPMSData',{"_id":ObjectId(form['prid'])},{'$set' :array3})
						
					return redirect(url_for('zenepms.emps_kra_view'))
	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
	return render_template('epms_appraiser_view.html', user_details=user_details,list=a)

def getuserfullname(userid):
	uname=find_one_in_collection('Userinfo',{"username":userid})
	username=uname['f_name']+" "+uname['l_name']
	return username

# hod report:
def check_dept_chief(designation_id):
	chk_dep_cheif_pid=find_and_filter('departmentmaster',{'dept_chief_pid':designation_id},{'_id':0,"dept_pid":1,"plant_id":1,"plant":1})
	# print chk_dep_cheif_pid
	if chk_dep_cheif_pid:
		result=chk_dep_cheif_pid
		usr_under_hod=find_and_filter('Userinfo',{'department_id':chk_dep_cheif_pid[0]['dept_pid']},{'_id':0,"username":1,"f_name":1,"l_name":1,"doj":1,"designation":1})
		# print usr_under_hod
		if usr_under_hod:
			result=usr_under_hod
			#print result
	else:
		result = False
	return result



@zen_epms.route('/epmsHODreports/', methods = ['GET', 'POST'])
@login_required
def epms_HOD_reports():
	
	user_details = base()
	uid = current_user.username
	uid_id= current_user.get_id()
	chief_pid_results=check_dept_chief(user_details['designation_id'])


	currentDate = datetime.datetime.now()
	EPMSCutOff = "31"+"-"+"12"+"-"+str(currentDate.year-1)
	EPMSCutOff = datetime.datetime.strptime(EPMSCutOff, '%d-%m-%Y').date()

	EPMSDetailedListEmployeesNotEligibleForAppraisal = []
	EPMSDetailedListEmployeesNotInitiatedEPMS = []
	EPMSDetailedListUserSavedNotSubmitted = []
	EPMSDetailedListUserSubmitted = []
	EPMSDetailedListUserSubmittedAppraiserSubmitted = []
	EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted = []

	TotalUserInfoEmployees = find_in_collection('Userinfo',{"employee_status":"active"})

	TotalUserInfoEmployees = chief_pid_results

	if TotalUserInfoEmployees == False :
		#print "Not HOD"
		return render_template('epmsreportsNoAccess.html',user_details=user_details)
	#print TotalUserInfoEmployees

	for i in range(0,len(TotalUserInfoEmployees)):
		currentDOJ = datetime.datetime.strptime(TotalUserInfoEmployees[i]['doj'], '%d-%m-%Y').date()
		currentuserid = TotalUserInfoEmployees[i]['username']
	   	if currentDOJ > EPMSCutOff:
	   		EPMSDetailedListEmployeesNotEligibleForAppraisal.append(currentuserid)
	   	elif (not find_one_in_collection('EPMSData',{"UserId":currentuserid})):
	   		EPMSDetailedListEmployeesNotInitiatedEPMS.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SavedIndicator":"Yes","SubmitIndicator":"No"}):
	   		EPMSDetailedListUserSavedNotSubmitted.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SubmitIndicator":"Yes","approval_levels.0.submit_status":"pending","approval_levels.1.submit_status":"pending"}):
	   		EPMSDetailedListUserSubmitted.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SubmitIndicator":"Yes","approval_levels.0.submit_status":"submitted","approval_levels.1.submit_status":"pending"}):
	   		EPMSDetailedListUserSubmittedAppraiserSubmitted.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SubmitIndicator":"Yes","$or":[{"approval_levels.0.submit_status":"submitted","approval_levels.1.submit_status":"submitted"},{"approval_levels.0.submit_status":"submitted","approval_levels.1.a_id":"-"},{"approval_levels.1.submit_status":"submitted","approval_levels.1.a_id":"-"}]}):
	   		EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted.append(currentuserid)

	CountData = {}

	CountData['EmployeesNotEligibleForAppraisal'] = len(EPMSDetailedListEmployeesNotEligibleForAppraisal)
	CountData['EmployeesNotInitiatedEPMS'] = len(EPMSDetailedListEmployeesNotInitiatedEPMS)
	CountData['UserSavedNotSubmitted'] = len(EPMSDetailedListUserSavedNotSubmitted)
	CountData['UserSubmitted'] = len(EPMSDetailedListUserSubmitted)
	CountData['UserSubmitted-AppraiserSubmitted'] = len(EPMSDetailedListUserSubmittedAppraiserSubmitted)
	CountData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted'] = len(EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted)
	CountData['TotalActiveEmployees'] = len(TotalUserInfoEmployees)

	# EPMSReportData['EmployeesNotEligibleForAppraisal'] = 90
	# EPMSReportData['UserSavedNotSubmitted'] = 40
	# EPMSReportData['UserSubmitted'] = 10
	# EPMSReportData['UserSubmitted-AppraiserSubmitted'] = 30
	# EPMSReportData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted'] = 20

	EPMSReportData = {}

	EPMSReportData['UserSavedNotSubmitted'] = round((CountData['UserSavedNotSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['UserSubmitted'] = round((CountData['UserSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['UserSubmitted-AppraiserSubmitted'] = round((CountData['UserSubmitted-AppraiserSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted'] = round((CountData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['EmployeesNotEligibleForAppraisal'] = round((CountData['EmployeesNotEligibleForAppraisal']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['EmployeesNotInitiatedEPMS'] = round((CountData['EmployeesNotInitiatedEPMS']/float(CountData['TotalActiveEmployees']))*100,2)

	totalPercentage = EPMSReportData['UserSavedNotSubmitted'] + EPMSReportData['UserSubmitted'] + EPMSReportData['UserSubmitted-AppraiserSubmitted']+ EPMSReportData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted']+EPMSReportData['EmployeesNotEligibleForAppraisal']

	#print EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted


	totalList = [EPMSDetailedListEmployeesNotEligibleForAppraisal,EPMSDetailedListEmployeesNotInitiatedEPMS,EPMSDetailedListUserSavedNotSubmitted,EPMSDetailedListUserSubmitted,EPMSDetailedListUserSubmittedAppraiserSubmitted,EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted]

	EPMSUserInfo = []

	for i in range(0,len(TotalUserInfoEmployees)):

		if TotalUserInfoEmployees[i]['username'] in EPMSDetailedListEmployeesNotEligibleForAppraisal:
		  currentStatus = "Not Eligible For EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListEmployeesNotInitiatedEPMS:
		  currentStatus = "Not Initiated EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSavedNotSubmitted:
		  currentStatus = "Saved EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSubmitted:
		  currentStatus = "Submitted EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSubmittedAppraiserSubmitted:
		  currentStatus = "Appraiser Approved"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted:
		  currentStatus = "Appraiser & Reviewer Approved"

		arrayTemp = {}
		arrayTemp ={
				"UserId":TotalUserInfoEmployees[i]['username'],
				"UserName":getuserfullname(TotalUserInfoEmployees[i]['username']),
				"Designation" : TotalUserInfoEmployees[i]['designation'],
				"Status":currentStatus					
				}

		EPMSUserInfo.append(arrayTemp)


	return render_template('epmsHODreports.html',user_details=user_details,EPMSReportData=EPMSReportData,CountData=CountData,EPMSUserInfo=EPMSUserInfo)


@zen_epms.route('/epmsHRreports/', methods = ['GET', 'POST'])
@login_required
def epms_HR_reports():
	
	user_details = base()
	uid = current_user.username
	uid_id= current_user.get_id()
	chief_pid_results=check_dept_chief(user_details['designation_id'])


	currentDate = datetime.datetime.now()
	EPMSCutOff = "31"+"-"+"12"+"-"+str(currentDate.year-1)
	EPMSCutOff = datetime.datetime.strptime(EPMSCutOff, '%d-%m-%Y').date()

	EPMSDetailedListEmployeesNotEligibleForAppraisal = []
	EPMSDetailedListEmployeesNotInitiatedEPMS = []
	EPMSDetailedListUserSavedNotSubmitted = []
	EPMSDetailedListUserSubmitted = []
	EPMSDetailedListUserSubmittedAppraiserSubmitted = []
	EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted = []

	TotalUserInfoEmployees = find_in_collection('Userinfo',{"employee_status":"active"})

	#TotalUserInfoEmployees = chief_pid_results
	#print TotalUserInfoEmployees

	for i in range(0,len(TotalUserInfoEmployees)):
		# print TotalUserInfoEmployees[i]['username']
		currentDOJ = datetime.datetime.strptime(TotalUserInfoEmployees[i]['doj'], '%d-%m-%Y').date()
		currentuserid = TotalUserInfoEmployees[i]['username']
	   	if currentDOJ > EPMSCutOff:
	   		EPMSDetailedListEmployeesNotEligibleForAppraisal.append(currentuserid)
	   	elif (not find_one_in_collection('EPMSData',{"UserId":currentuserid})):
	   		EPMSDetailedListEmployeesNotInitiatedEPMS.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SavedIndicator":"Yes","SubmitIndicator":"No"}):
	   		EPMSDetailedListUserSavedNotSubmitted.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SubmitIndicator":"Yes","approval_levels.0.submit_status":"pending","approval_levels.1.submit_status":"pending"}):
	   		EPMSDetailedListUserSubmitted.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SubmitIndicator":"Yes","approval_levels.0.submit_status":"submitted","approval_levels.1.submit_status":"pending"}):
	   		EPMSDetailedListUserSubmittedAppraiserSubmitted.append(currentuserid)
	   	elif find_one_in_collection('EPMSData',{"UserId":currentuserid,"SubmitIndicator":"Yes","$or":[{"approval_levels.0.submit_status":"submitted","approval_levels.1.submit_status":"submitted"},{"approval_levels.0.submit_status":"submitted","approval_levels.1.a_id":"-"},{"approval_levels.1.submit_status":"submitted","approval_levels.1.a_id":"-"}]}):
	   		EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted.append(currentuserid)

	CountData = {}

	CountData['EmployeesNotEligibleForAppraisal'] = len(EPMSDetailedListEmployeesNotEligibleForAppraisal)
	CountData['EmployeesNotInitiatedEPMS'] = len(EPMSDetailedListEmployeesNotInitiatedEPMS)
	CountData['UserSavedNotSubmitted'] = len(EPMSDetailedListUserSavedNotSubmitted)
	CountData['UserSubmitted'] = len(EPMSDetailedListUserSubmitted)
	CountData['UserSubmitted-AppraiserSubmitted'] = len(EPMSDetailedListUserSubmittedAppraiserSubmitted)
	CountData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted'] = len(EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted)
	CountData['TotalActiveEmployees'] = len(TotalUserInfoEmployees)

	# EPMSReportData['EmployeesNotEligibleForAppraisal'] = 90
	# EPMSReportData['UserSavedNotSubmitted'] = 40
	# EPMSReportData['UserSubmitted'] = 10
	# EPMSReportData['UserSubmitted-AppraiserSubmitted'] = 30
	# EPMSReportData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted'] = 20

	EPMSReportData = {}

	EPMSReportData['UserSavedNotSubmitted'] = round((CountData['UserSavedNotSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['UserSubmitted'] = round((CountData['UserSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['UserSubmitted-AppraiserSubmitted'] = round((CountData['UserSubmitted-AppraiserSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted'] = round((CountData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['EmployeesNotEligibleForAppraisal'] = round((CountData['EmployeesNotEligibleForAppraisal']/float(CountData['TotalActiveEmployees']))*100,2)
	EPMSReportData['EmployeesNotInitiatedEPMS'] = round((CountData['EmployeesNotInitiatedEPMS']/float(CountData['TotalActiveEmployees']))*100,2)

	totalPercentage = EPMSReportData['UserSavedNotSubmitted'] + EPMSReportData['UserSubmitted'] + EPMSReportData['UserSubmitted-AppraiserSubmitted']+ EPMSReportData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted']+EPMSReportData['EmployeesNotEligibleForAppraisal']

	#print EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted


	totalList = [EPMSDetailedListEmployeesNotEligibleForAppraisal,EPMSDetailedListEmployeesNotInitiatedEPMS,EPMSDetailedListUserSavedNotSubmitted,EPMSDetailedListUserSubmitted,EPMSDetailedListUserSubmittedAppraiserSubmitted,EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted]

	EPMSUserInfo = []

	for i in range(0,len(TotalUserInfoEmployees)):

		if TotalUserInfoEmployees[i]['username'] in EPMSDetailedListEmployeesNotEligibleForAppraisal:
		  currentStatus = "Not Eligible For EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListEmployeesNotInitiatedEPMS:
		  currentStatus = "Not Initiated EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSavedNotSubmitted:
		  currentStatus = "Saved EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSubmitted:
		  currentStatus = "Submitted EPMS"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSubmittedAppraiserSubmitted:
		  currentStatus = "Appraiser Approved"
		elif TotalUserInfoEmployees[i]['username'] in EPMSDetailedListUserSubmittedAppraiserSubmittedReviewerSubmitted:
		  currentStatus = "Appraiser & Reviewer Approved"

		if 'location' in TotalUserInfoEmployees[i]:
			currentLocation = TotalUserInfoEmployees[i]['location']
		else:
			currentLocation=TotalUserInfoEmployees[i]['plant_id']

		if 'department_name' in TotalUserInfoEmployees[i]: 
			if not TotalUserInfoEmployees[i]['department_name']:
				currentDepartment = "-"
			else:
				currentDepartment = TotalUserInfoEmployees[i]['department_name']			
		else:
			currentDepartment = "Not available"

		arrayTemp = {}
		arrayTemp ={
				"UserId":TotalUserInfoEmployees[i]['username'],
				"UserName":getuserfullname(TotalUserInfoEmployees[i]['username']),
				"Designation":TotalUserInfoEmployees[i]['designation'], 
				"Department":currentDepartment,
				"Location":currentLocation,
				"Status":currentStatus					
				}

		EPMSUserInfo.append(arrayTemp)


	return render_template('epmsHRreports.html',user_details=user_details,EPMSReportData=EPMSReportData,CountData=CountData,EPMSUserInfo=EPMSUserInfo)


#EPMS employee final report
# @zen_epms.route('/epmsfinalreport/', methods = ['GET', 'POST'])
# @login_required
# def epmsfinalreport():
# 	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
# 	if p:
# 		user_details = base()
# 		report=[]
# 		count=0
# 		submitted=[]
# 		not_submitted=[]
# 		reviewer_submitted_count=0
# 		reviewer_not_submitted_count=0
# 		count_submit_appraiser=0
# 		count_not_submit_appraiser=0
# 		count_submit_appraise=0
# 		count_not_submit_appraise=0
# 		count_not_filled=0
# 		epmsdetails=find_in_collection('EPMSData',{})
# 		for i in epmsdetails:
# 			count+=1
# 			try:
# 				user_detail=find_one_in_collection('Userinfo',{'username':i['UserId']})
# 				name=user_detail['f_name']+' '+user_detail['l_name']
# 				payrollarea=user_detail['plant_id']
# 				if 'location' in user_detail:
# 					location=user_detail['location']
# 					if location == 'GANESHPAHAD' and user_detail['plant_id']=='GP':
# 						location = "GANESHPAHAD POWER"
# 					elif location == 'GANESHPAHAD' and user_detail['plant_id']=='GC':
# 						location = "GANESHPAHAD CEMENT"
# 				else:
# 					location=user_detail['plant_id']
# 			except:
# 				# print 'user '+i['UserId']+' data is not available'
# 				logging.info('user '+i['UserId']+' data is not available')

# 	# data is only shown if the reviewer or appraiser has subbmitted
# 			if 'SubmitIndicator' in i:
# 				if i['SubmitIndicator']=="Yes":
# 					if i['SubmitIndicator_Appraiser'] == "Yes":
# 						overallweightage=filedreversehash(i['OverallAchievementWeightage'])
# 						overallweightage=round(float(overallweightage),2)
# 						grade=filedreversehash(i['OverallRatingWeightage'])
# 						appraiser_comments=filedreversehash(i['Final-AppraiserComments'])
# 						if i['SubmitIndicator_Reviewer'] =="Yes":
# 							reviewer_comments=filedreversehash(i['Final-ReviewerComments'])
# 							# print i['UserId'],'Reviewer submited'
# 							reviewer_submitted_count+=1
# 							submitted.append(i['UserId'])
# 						elif i['SubmitIndicator_Reviewer'] =="No":
# 							reviewer_comments="Reviewer Not Yet Submitted"
# 							# print i['UserId'],'Reviewer not submited'
# 							reviewer_not_submitted_count+=1
# 							not_submitted.append(i['UserId']),'reviewer not submitted'
# 						else:
# 							# print "check"
# 						# print i['UserId'],'Appraiser submited'
# 						count_submit_appraiser+=1
# 					elif i['SubmitIndicator_Appraiser'] == "No":
# 						overallweightage="Appraiser Not Yet Submitted"
# 						grade="Appraiser Not Yet Submitted"
# 						appraiser_comments="Appraiser Not Yet Submitted"
# 						reviewer_comments="Appraiser Not Yet Submitted"
# 						count_not_submit_appraiser+=1
# 					else:
# 					# 	print "check2"
# 					# print i['UserId'],'Appraiser not submited'
# 					count_submit_appraise+=1

# 				elif i['SubmitIndicator']=="No":
# 					overallweightage="Apraisee Not Submitted"
# 					grade="Apraisee Not Submitted"
# 					appraiser_comments="Apraisee Not Submitted"
# 					reviewer_comments="Apraisee Not Submitted"
# 					appraiser_comments="Apraisee Not Submitted"
# 					# print i['UserId'],'not submited'
# 					count_not_submit_appraise+=1
# 				else:
# 					print "check 3"

# 			if i.has_key('SubmitIndicator') == False:
# 				overallweightage="Apraisee Not Yet Filled"
# 				grade="Apraisee Not Yet Filled"
# 				appraiser_comments="Apraisee Not Yet Filled"
# 				reviewer_comments="Apraisee Not Yet Filled"
# 				appraiser_comments="Apraisee Not Yet Filled"
# 				print i['UserId'],'not filled'
# 				count_not_filled+=1
			

# 			try:
# 				report.append({'username':i['UserId'],'name':name,'designation':user_detail['designation'],'department':user_detail['department_name'],
# 								'location':location,'payrollarea':payrollarea,'overallweightage':overallweightage,
# 								'grade':grade,'appraiser_comments':appraiser_comments,
# 								'reviewer_comments':reviewer_comments})
# 			except:
# 				# print 'user '+i['UserId']+' data is not available'
# 				logging.info('user '+i['UserId']+' data is not available in userinfo')

# 		print submitted,'data'
# 		print not_submitted,'reviewer not submitted' 
# 		print len(report),'report count ,',count,' total loopcount','count_not_filled : ',count_not_filled
# 		print 'reviewer_not_submitted_count :',reviewer_not_submitted_count,'reviewer_submitted_count: ',reviewer_submitted_count
# 		print 'count_submit_appraiser: ',count_submit_appraiser,'count_not_submit_appraiser :',count_not_submit_appraiser
# 		print 'count_submit_appraise: ',count_submit_appraise,'count_not_submit_appraise :',count_not_submit_appraise
# 		return render_template('epmsfinal_report.html',report=report,user_details=user_details)
# 	else:
# 		return redirect(url_for('zenuser.index'))	

# Employee KRA Data Report
@zen_epms.route('/epmskrareport/', methods = ['GET', 'POST'])
@login_required
def epmskrareport():
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		user_details = base()
		report=[]
		count=0
		finyear=request.args['finyear']
		epmsdetails=find_in_collection('EPMSData',{'SubmitIndicator':'Yes','Fin_year':finyear})
		for i in epmsdetails:
			count+=1
			try:
				user_detail=find_one_in_collection('Userinfo',{'username':i['UserId']})
				name=user_detail['f_name']+' '+user_detail['l_name']
				payrollarea=user_detail['plant_id']
				if 'location' in user_detail:
					location=user_detail['location']
					if location == 'GANESHPAHAD' and user_detail['plant_id']=='GP':
						location = "GANESHPAHAD POWER"
					elif location == 'GANESHPAHAD' and user_detail['plant_id']=='GC':
						location = "GANESHPAHAD CEMENT"
				else:
					location=user_detail['plant_id']
			except:
				logging.info('user '+i['UserId']+' data is not available')

			if 'AppraisalAndKRA' in i:
				AppraisalAndKRA=[]
				KRAWeightage=[]
				for j in range(len(i['AppraisalAndKRA'])):
					report.append({'username':i['UserId'],'name':name,'AppraisalAndKRA':i['AppraisalAndKRA'][j]['AppraisalKRAObjective'],'KRAWeightage':i['AppraisalAndKRA'][j]['Weightage'],'designation':user_detail['designation'],'department':user_detail['department_name'],
								'location':location,'payrollarea':payrollarea})
			else:	
				print "No TrainingsAttendedTitle"			
		return render_template('epmsKRAs_report.html',report=report,user_details=user_details)

	else:
		return redirect(url_for('zenuser.index'))

# Employee Training Attended Report
@zen_epms.route('/epmstrainingreport/', methods = ['GET', 'POST'])
@login_required
def epmstrainingreport():
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		user_details = base()
		report=[]
		count=0
		finyear=request.args['finyear']
		epmsdetails=find_in_collection('EPMSData',{'SubmitIndicator':'Yes','Fin_year':finyear})
		for i in epmsdetails:
			count+=1
			try:
				user_detail=find_one_in_collection('Userinfo',{'username':i['UserId']})
				name=user_detail['f_name']+' '+user_detail['l_name']
				payrollarea=user_detail['plant_id']
				if 'location' in user_detail:
					location=user_detail['location']
					if location == 'GANESHPAHAD' and user_detail['plant_id']=='GP':
						location = "GANESHPAHAD POWER"
					elif location == 'GANESHPAHAD' and user_detail['plant_id']=='GC':
						location = "GANESHPAHAD CEMENT"
				else:
					location=user_detail['plant_id']
			except:
				# print 'user '+i['UserId']+' data is not available'
				logging.info('user '+i['UserId']+' data is not available')

			if 'TrainingsAttended' in i:
				TrainingsAttended=[]
				for j in range(len(i['TrainingsAttended'])):
					report.append({'username':i['UserId'],'name':name,'TrainingsAttended':i['TrainingsAttended'][j]['TrainingsAttendedTitle'],'designation':user_detail['designation'],'department':user_detail['department_name'],
								'location':location,'payrollarea':payrollarea})
			else:
				print "No TrainingsAttendedTitle"
		return render_template('epmstraining_report.html',report=report,user_details=user_details)

	else:
		return redirect(url_for('zenuser.index'))

#Employee Training Need Report
@zen_epms.route('/epmstrainingneedreport/', methods = ['GET', 'POST'])
@login_required
def epmstrainingneedreport():
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		user_details = base()
		report=[]
		count=0
		finyear=request.args['finyear']
		epmsdetails=find_in_collection('EPMSData',{'SubmitIndicator':'Yes','Fin_year':finyear})
		for i in epmsdetails:
			count+=1
			try:
				user_detail=find_one_in_collection('Userinfo',{'username':i['UserId']})
				name=user_detail['f_name']+' '+user_detail['l_name']
				payrollarea=user_detail['plant_id']
				if 'location' in user_detail:
					location=user_detail['location']
					if location == 'GANESHPAHAD' and user_detail['plant_id']=='GP':
						location = "GANESHPAHAD POWER"
					elif location == 'GANESHPAHAD' and user_detail['plant_id']=='GC':
						location = "GANESHPAHAD CEMENT"
				else:
					location=user_detail['plant_id']
			except:
				logging.info('user '+i['UserId']+' data is not available')

			if 'TrainingDevelopmentalNeedsTechnicalFunctional' in i:
				TrainingDevelopmentalNeedsTechnicalFunctional=[]
				a=len(i['TrainingDevelopmentalNeedsTechnicalFunctional'])
				b=len(i['TrainingDevelopmentalNeedsBehavioralAttitudinal'])
				c=max(a,b)
				for j in range(c):
					
					try:
						report.append({'username':i['UserId'],'name':name,'TrainingDevelopmentalNeedsTechnicalFunctional':i['TrainingDevelopmentalNeedsTechnicalFunctional'][j]['TechnicalFunctional'],'designation':user_detail['designation'],'department':user_detail['department_name'],
								'location':location,'payrollarea':payrollarea,'TrainingDevelopmentalNeedsBehavioralAttitudinal':i['TrainingDevelopmentalNeedsBehavioralAttitudinal'][j]['BehavioralAttitudinal'],'otherTrainingNeeds':i['otherTrainingNeeds']})
					except:
						logging.info('user '+i['UserId']+' data is not available')
			else:
				print "No TrainingsAttended"
			
		return render_template('epmstraining_dev_need_report.html',report=report,user_details=user_details)

	else:
		return redirect(url_for('zenuser.index'))


#Employee Action of Plan Report
@zen_epms.route('/epmsactionofplanreport/', methods = ['GET', 'POST'])
@login_required
def epmsactionofplanreport():
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		user_details = base()
		report=[]
		count=0
		finyear=request.args['finyear']
		epmsdetails=find_in_collection('EPMSData',{'SubmitIndicator':'Yes','Fin_year':finyear})
		for i in epmsdetails:
			count+=1
			try:
				user_detail=find_one_in_collection('Userinfo',{'username':i['UserId']})
				name=user_detail['f_name']+' '+user_detail['l_name']
				payrollarea=user_detail['plant_id']
				if 'location' in user_detail:
					location=user_detail['location']
					if location == 'GANESHPAHAD' and user_detail['plant_id']=='GP':
						location = "GANESHPAHAD POWER"
					elif location == 'GANESHPAHAD' and user_detail['plant_id']=='GC':
						location = "GANESHPAHAD CEMENT"
				else:
					location=user_detail['plant_id']
			except:
				logging.info('user '+i['UserId']+' data is not available')

			if 'ActionPlan' in i:
				Action=[]
				ActionOwner=[]
				TimeFrame=[]
				a=len(i['ActionPlan'])
				for j in range(a):
					
					try:
						report.append({'username':i['UserId'],'name':name,'Action':i['ActionPlan'][j]['Action'],'designation':user_detail['designation'],'department':user_detail['department_name'],
								'location':location,'payrollarea':payrollarea,'ActionOwner':i['ActionPlan'][j]['ActionOwner'],'TimeFrame':i['ActionPlan'][j]['TimeFrame']})
					except:
						logging.info('user '+i['UserId']+' data is not available')
			else:
				print "No TrainingsAttended"
			
		return render_template('epms_action_plan_report.html',report=report,user_details=user_details)

	else:
		return redirect(url_for('zenuser.index'))

#Employee Job Description Report
@zen_epms.route('/epmsjobdescreport/', methods = ['GET', 'POST'])
@login_required
def epmsjobdescreport():
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		user_details = base()
		report=[]
		count=0
		finyear=request.args['finyear']
		epmsdetails=find_in_collection('EPMSData',{'SubmitIndicator':'Yes','Fin_year':finyear})
		for i in epmsdetails:
			count+=1
			try:
				user_detail=find_one_in_collection('Userinfo',{'username':i['UserId']})
				name=user_detail['f_name']+' '+user_detail['l_name']
				payrollarea=user_detail['plant_id']
				if 'location' in user_detail:
					location=user_detail['location']
					if location == 'GANESHPAHAD' and user_detail['plant_id']=='GP':
						location = "GANESHPAHAD POWER"
					elif location == 'GANESHPAHAD' and user_detail['plant_id']=='GC':
						location = "GANESHPAHAD CEMENT"
				else:
					location=user_detail['plant_id']
			except:
				logging.info('user '+i['UserId']+' data is not available')

			if 'JobResponsibilities' in i:
				JobResponsibilities=[]					
				try:
					report.append({'username':i['UserId'],'name':name,'Jobresp':i['JobResponsibilities'],'designation':user_detail['designation'],'department':user_detail['department_name'],
							'location':location,'payrollarea':payrollarea})
				except:
					logging.info('user '+i['UserId']+' data is not available')
			else:
				print "No TrainingsAttended"
			
		return render_template('epms_jobresp_report.html',report=report,user_details=user_details)

	else:
		return redirect(url_for('zenuser.index'))


#Reports on epms
@zen_epms.route('/epmsreports/', methods = ['GET', 'POST'])
@login_required
def epmsreports():
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		user_details = base()
		report=[]
		count=0
		if request.method == 'POST':
			form=request.form
			print "Data",form['type']
			print "FIn",form['fin_date']

			if form['type'] == 'KR' :
				return redirect(url_for('zenepms.epmskrareport',finyear=form['fin_date']))
			elif form['type'] == 'TAR' :
				return redirect(url_for('zenepms.epmstrainingreport',finyear=form['fin_date']))
			elif form['type'] == 'TNR' :
				return redirect(url_for('zenepms.epmstrainingneedreport',finyear=form['fin_date']))
			elif form['type'] == 'JDR' :
				return redirect(url_for('zenepms.epmsjobdescreport',finyear=form['fin_date']))
			elif form['type'] == 'APR' :
				return redirect(url_for('zenepms.epmsactionofplanreport',finyear=form['fin_date']))
			elif form['type'] == 'KRAS' :
				return redirect(url_for('zenepms.kra_status_report',finyear=form['fin_date']))
			else:
				return redirect(url_for('zenuser.index'))
	
		return render_template('emps_reports.html',user_details=user_details)

	else:
		return redirect(url_for('zenuser.index'))

# EPMS Phase-2 /Employee KRAs Entry Form
@zen_epms.route('/epmskrasentryform/', methods = ['GET', 'POST'])
@login_required
def epmskrasentryform():
	
	user_details = base()
	uid = current_user.username
	uid_id= current_user.get_id()
	db=dbconnect()
	user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid_id)},{"designation_id":1,"_id":0,"plant_id":1,"calendar_code":1})
	year = datetime.date.today().year
	m = datetime.date.today()
	mon='{:02d}'.format(m.month)
	dateOfJoining = user_details['doj']
	ff_year=0
	eff_year=0
	val=4
	val='{:02d}'.format(val)
	if mon>=val:
		ff_year=(str(year))+"-"+(str(year+1))
	else:
		ff_year=(str(year-1))+"-"+(str(year))

	if mon>=val:
		eff_year=(str(year-1))+"-"+(str(year))
	else:
		eff_year=(str(year-2))+"-"+(str(year-1))
		
	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	
	RoleFromDB = user_details['emp_subgroup']
	
	if RoleFromDB in ['S1', 'S2', 'S3', 'S4', 'S5']:
		Role = "AGMAbove"
	else:
	    Role = "BelowAGM"

	QualificationData = find_one_in_collection('Userinfo',{'username':uid})
	Qualification = QualificationData['Qualification1']
	if QualificationData['Qualification2'] != "":
	   Qualification = Qualification+"/"+QualificationData['Qualification2']
	approval_levels=[]
	user_rm=find_one_in_collection('Userinfo',{'username':uid})  #Current User Who is Logged IN
	rms_list=["m1_pid","m2_pid","m3_pid","m4_pid","m5_pid","m6_pid","m7_pid"]
	approval_list=[]
	view_levels=[]
	view_list=[]
	
	q=find_one_in_collection('Positions',{"position_id":user_data[0]['designation_id']})
	for rm in rms_list:
		rm1_username=find_one_in_collection('Userinfo',{'designation_id':user_rm[rm]})
		try:
			if rm1_username['designation_id'] in q['levels']:
				
				approval_list.append(rm1_username['username'])
			else:
				view_list.append(rm1_username['username'])
		except:
			logging.info("no user")
		

	for i in range(len(view_list)):
		appraiserInfo = find_one_in_collection('Userinfo',{'username':view_list[i]})
		a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
		view_levels = view_levels+[{"a_id":view_list[i],"a_status":"view",
				"a_name":a_name}]
	
	for i in range(0,len(approval_list)):
		if i==0:
			
			appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
			a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
			a_designation = appraiserInfo['designation']
			approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"current",
				'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
		elif i ==1:
			
			appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
			a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
			a_designation = appraiserInfo['designation']
			approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"pending",
				'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
		else:
			logging.debug(uid+" "+"User Dosent have any Approval Levels")

	lengthOfApprovalLevels = len(approval_levels)
	
	if(lengthOfApprovalLevels==1):
	   appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[0]})
	   a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
	   a_designation = appraiserInfo['designation']
	   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
				'submit_status':"pending","a_name":"-","a_designation":"-"}]

	if(len(approval_list)==0):
	   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
				'submit_status':"pending","a_name":"-","a_designation":"-"}]+[{"a_id":"-","a_status":"current",
				'submit_status':"pending","a_name":"-","a_designation":"-"}]

	if(not find_one_in_collection('EPMSData',{"UserId":uid})):
		if(not find_one_in_collection('EPMSKRAData',{"UserId":uid,"Finance_year":ff_year})):
			update_collection('EPMSKRAData',{"UserId":uid},{'$set' :{"UserId":uid,"Role":Role,"Qualification":Qualification,
				"approval_levels":approval_levels,"view_levels":view_levels,"Finance_year":ff_year,"SubmitIndicator" : "No"}})
		
	else:
		# This finance year for fetching of pervious pms data
		finance_year=0
		if mon>=val:
			finance_year=(str(year-1))+"-"+(str(year))
		else:
			finance_year=(str(year-2))+"-"+(str(year-1))
		
		employee_att = db.EPMSData.aggregate([{'$match' : {'UserId': uid}},
		{ '$unwind': '$AppraisalAndKRA' },
		{'$match' : {'Fin_year' : finance_year}}])
		
		EMPSJob = find_and_filter('EPMSData',{"UserId" : uid},{"JobResponsibilities":1})
		AppraisalAndKRA=[]
		for e in list(employee_att):
			AppraisalAndKRA=AppraisalAndKRA+[{"AppraisalKRAObjective":e['AppraisalAndKRA']['AppraisalKRAObjective'],"Weightage":e['AppraisalAndKRA']['Weightage'],"TargetPerUnit":e['AppraisalAndKRA']['TargetPerUnit']}]
		
		if(not find_one_in_collection('EPMSKRAData',{"UserId":uid})):
			update_collection('EPMSKRAData',{"UserId":uid},{'$set' :{"UserId":uid,"Role":Role,"Qualification":Qualification,
				"approval_levels":approval_levels,"AppraisalAndKRA":AppraisalAndKRA,"JobResponsibilities":EMPSJob[0]['JobResponsibilities'],"view_levels":view_levels,"Finance_year":ff_year,"SubmitIndicator" : "No"}})

	len(approval_list)

	if request.method == 'POST':
			form=request.form
			
			
			AppraisalKRAObjective=form.getlist('AppraisalKRAObjective')
			Weightage=form.getlist('Weightage')
			TargetPerUnit=form.getlist('TargetPerUnit')
			
			TotalWeightageSum = form['TotalWeightageSum']

			AppraisalAndKRA=[]

			for i in range(0,len(AppraisalKRAObjective)):
				if AppraisalKRAObjective[i] != "":
					array={}
					array={
					"AppraisalKRAObjective":AppraisalKRAObjective[i],
					"Weightage":Weightage[i],
					"TargetPerUnit":TargetPerUnit[i],
					
					}
					AppraisalAndKRA.append(array)			

			if form['submit'] == 'Save': #SAVE
				finance_year=0
				if mon>=val:
					finance_year=(str(year))+"-"+(str(year+1))
				else:
					finance_year=(str(year-1))+"-"+(str(year))

				# print "save"
				
				flash("KRAs for year "+ finance_year +" has been saved successfully",'alert-success')
				update_collection('EPMSKRAData',{"UserId":uid},
					{'$set' :
					{
					"approval_levels":approval_levels,
					"UserId":uid,
					"UserEmail":user_details['official_email'],
					"JobResponsibilities":form['JobResponsibilities'] , 
					"AppraisalAndKRA":AppraisalAndKRA,
					"SavedIndicator":"Yes",
					"SubmitIndicator_Reviewer":"No",
					"SubmitIndicator_Appraiser":"No",
					"SubmitIndicator" : "No",
					"SavedOn":datetime.datetime.now(),
					"Role":Role,
					"Finance_year":finance_year,
					"calendar_code":user_details['calendar_code'],
					"TotalWeightageSum":TotalWeightageSum
					}
				  }
				)
				
			else: #SUBMIT
				finance_year=0
				if mon>=val:
					finance_year=(str(year))+"-"+(str(year+1))
				else:
					finance_year=(str(year-1))+"-"+(str(year))

				flash("KRAs for year "+finance_year+" has been submitted successfully",'alert-success')
				update_collection('EPMSKRAData',{"UserId":uid},
					{'$set' :
					{
					"approval_levels":approval_levels,
					"UserId":uid,
					"UserEmail":user_details['official_email'],
					"JobResponsibilities":form['JobResponsibilities'] , 
					"AppraisalAndKRA":AppraisalAndKRA,
					"SavedIndicator":"Yes",
					"SubmitIndicator_Appraiser":"No",
					"SubmitIndicator" : "Yes",
					"SubmitIndicator_Reviewer":"No",
					"SavedOn":datetime.datetime.now(),
					"SubmittedOn":datetime.datetime.now(),
					"Role":Role,
					"calendar_code":user_details['calendar_code'],
					"Finance_year":finance_year,
					"TotalWeightageSum":TotalWeightageSum
					}
					}
				)
				
	EPMSKRAData = find_one_in_collection('EPMSKRAData',{"UserId" : uid})
	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
	
	return render_template('epms_kras_entry_form.html', user_details=user_details,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings,Finance_year=ff_year)

@zen_epms.route('/appraiser_kras_view/', methods = ['GET', 'POST'])
@login_required
def appraiser_kras_view():
	user_details = base()
	finance_year=request.args['finyear']
	print "view",finance_year
	uid = request.args['uid']
	db=dbconnect()
	
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		employee_att = find_in_collection("EPMSKRAData",{'SubmitIndicator':  "Yes","Finance_year":finance_year,"SubmitIndicator_Reviewer" : "Yes"});
	else:
		employee_att = db.EPMSKRAData.aggregate([{'$match' : {'SubmitIndicator':  "Yes","Finance_year":finance_year}},
			{ '$unwind': '$approval_levels' },
	                {'$match' : {'approval_levels.a_id' : uid}}])
		employee_att = list(employee_att)

	a=[]

	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	for e in employee_att:
		e['fullname']=get_employee_name(e['UserId'])
		e['SavedOn'] = e['SavedOn'].strftime("%d/%m/%Y")
		e['UserId']=e['UserId']
		e['SubmitIndicator'] = e['SubmitIndicator']
		a=a+[e]

	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})

	return render_template('epms_kra_view.html', user_details=user_details,list=a)

#7 LEVELS KRAS TEAM VIEW
@zen_epms.route('/appraiser_kras_teamview/', methods = ['GET', 'POST'])
@login_required
def appraiser_kras_teamview():
	user_details = base()
	finance_year=request.args['finyear']
	uid = request.args['uid']
	db=dbconnect()
	print "Team View"
	
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		employee_att = find_in_collection("EPMSKRAData",{'SubmitIndicator':  "Yes","Finance_year":finance_year,"SubmitIndicator_Reviewer" : "Yes"});
	else:
		employee_att = db.EPMSKRAData.aggregate([{'$match' : {'SubmitIndicator':  "Yes","Finance_year":finance_year}},
			{ '$unwind': '$view_levels' },
	                {'$match' : {'view_levels.a_id' : uid}}])
		employee_att = list(employee_att)


	a=[]

	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	for e in employee_att:
		e['fullname']=get_employee_name(e['UserId'])
		e['SavedOn'] = e['SavedOn'].strftime("%d/%m/%Y")
		e['UserId']=e['UserId']
		e['SubmitIndicator'] = e['SubmitIndicator']
		a=a+[e]

	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})

	return render_template('epms_kra_teamview.html', user_details=user_details,list=a)

@zen_epms.route('/appraiser_KRA_info_view', methods = ['GET', 'POST'])
@login_required
def appraiser_KRA_detailed_view():

	user_details = base()
	uid = current_user.username
	uid_id= current_user.get_id()
	user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid_id)},{"designation_id":1,"_id":0,"plant_id":1})

	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	result_appraiser=[]
	result_reviewer=[]
	db=dbconnect()
	employee_att = db.EPMSKRAData.aggregate([{'$match' : {'SubmitIndicator': "Yes"}},
	{ '$unwind': '$approval_levels' },
	{'$match' : {'approval_levels.a_id' : uid}}])

	employee_att0 = db.EPMSKRAData.aggregate([{'$match' : {'approval_levels.0.a_id' : uid}}])
	for emp0 in list(employee_att0):
		result_appraiser.append(emp0['UserId'])


	employee_att1 = db.EPMSKRAData.aggregate([{'$match' : {'approval_levels.1.a_id' : uid}}])
	for emp1 in list(employee_att1):
		result_reviewer.append(emp1['UserId'])

	status=[result_appraiser,result_reviewer]

	a=[]

	Technical = "Technical"
	Soft = "Soft Skills"
	Yes = "Yes"
	Behavioural = "Behavioural"
	Quality = "Quality"
	IT = "IT"
	
	for e in list(employee_att):
		e['fullname']=get_employee_name(e['UserId'])
		e['SavedOn'] = e['SavedOn'].strftime("%d/%m/%Y")
		e['UserId']=e['UserId']
		e['SubmitIndicator'] = e['SubmitIndicator']
		a=a+[e]
	
	if request.method == 'POST':
			form=request.form
			AppraisalKRAObjective=form.getlist('AppraisalKRAObjective')
			Weightage=form.getlist('Weightage')
			TargetPerUnit=form.getlist('TargetPerUnit')
		
			AppraisalAndKRA=[]
			
			for i in range(0,len(AppraisalKRAObjective)):
				if AppraisalKRAObjective[i] != "":
					array={}
					array={
					"AppraisalKRAObjective":AppraisalKRAObjective[i],
					"Weightage":Weightage[i],
					"TargetPerUnit":TargetPerUnit[i]
					
					}
					AppraisalAndKRA.append(array)			
		
			EPMSKRAData=find_one_in_collection('EPMSKRAData',{'_id':ObjectId(form['prid'])})
			user_details1=find_one_in_collection('Userinfo',{'username':form['userid']})
			
			if EPMSKRAData["SubmitIndicator_Appraiser"]=="Yes":
			   varSubmitIndicator_Appraiser = "Yes"
			else:
			   varSubmitIndicator_Appraiser = "No"


			if form['submit'] == "VIEW":
				print form['userid'],'user to be viewed'
				count_a_levels=len(EPMSKRAData['approval_levels'])
				print "levels",count_a_levels
				if count_a_levels==2:
					if EPMSKRAData["SubmitIndicator_Appraiser"]=="No" or EPMSKRAData["approval_levels"][0]["a_id"] == uid:
						EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
						return render_template('epms_appraiser_KRA_detailed_view.html',user_details=user_details, status=status,user_details1=user_details1,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings)
					else:
						EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
						return render_template('epms_reviewer_KRA_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings)
				else:
					EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
					return render_template('epms_appraiser_KRA_detailed_view.html',user_details=user_details,status=status, user_details1=user_details1,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings)
			elif form['submit'] == 'TEAMVIEW' :
				print "Submit teamview"
				EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
				return render_template('epms_kra_detailed_teamview.html',user_details=user_details,status=status, user_details1=user_details1,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings,year=EPMSKRAData["Finance_year"],totalsum=EPMSKRAData["TotalWeightageSum"])

			elif form['submit'] == 'Save':

					

					uid = form['prid']
					print uid
					update_collection('EPMSKRAData',{"_id":ObjectId(uid)},
						{'$set' :
							{
							"Test" : "Sample",
							"JobResponsibilities":form['JobResponsibilities'] , 
							"AppraisalAndKRA":AppraisalAndKRA,
							"SavedIndicator":"Yes",
							"SubmitIndicator" : "Yes",
							"SubmitIndicator_Appraiser":varSubmitIndicator_Appraiser,
							"SubmitIndicator_Reviewer":"No",
							"SavedOn":datetime.datetime.now(),
							}
					   }
					)
					flash("KRAs for year "+ EPMSKRAData['Finance_year'] +" has been saved successfully - Manager",'alert-success')

					EPMSKRAData=find_one_in_collection('EPMSKRAData',{'_id':ObjectId(form['prid'])})
					EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
					count_a_levels=len(EPMSKRAData['approval_levels'])

					
					if count_a_levels==2:
						if EPMSKRAData["SubmitIndicator_Appraiser"]=="No" or EPMSKRAData["approval_levels"][0]["a_id"] == uid:
							return render_template('epms_appraiser_KRA_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings)
						else:
							return render_template('epms_reviewer_KRA_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings)
					else:
						return render_template('epms_appraiser_KRA_detailed_view.html',user_details=user_details,status=status,user_details1=user_details1,EPMSData=EPMSKRAData,EPMSGlobalSettings=EPMSGlobalSettings)

			else: # Submit
					uid = form['prid']
					
					update_collection('EPMSKRAData',{"_id":ObjectId(uid)},
						{'$set' :
							{
							"JobResponsibilities":form['JobResponsibilities'], 
							"AppraisalAndKRA":AppraisalAndKRA,
							"SavedIndicator":"Yes",
							"SubmitIndicator" : "Yes",
							"SubmitIndicator_Appraiser":"No",
							"SubmitIndicator_Reviewer":"No",
							"SavedOn":datetime.datetime.now(),
							}
					   }
					)
					flash("KRAs for year "+EPMSKRAData['Finance_year']+" has been submitted successfully - Manager",'alert-success')
					
					a_levels = find_one_in_collection('EPMSKRAData',{"_id":ObjectId(form['prid'])})
					count_a_levels=len(a_levels['approval_levels'])
					print "submit levels",count_a_levels
					for i in range(0,count_a_levels):
						if a_levels['approval_levels'][i]['a_status']=="current":
							j=i
							if j==count_a_levels-1:
								print "Approval--2",j
								array3={"SubmitIndicator" : "Yes","SubmitIndicator_Appraiser":"Yes","SubmitIndicator_Reviewer":"Yes","approval_levels."+str(i)+".a_status":"approved","approval_levels."+str(i)+".submit_status":"submitted"}
								
							else:
								print "Approval-1",j
								array3={"SubmitIndicator" : "Yes","SubmitIndicator_Appraiser":"Yes","approval_levels."+str(i)+".a_status":"approved","approval_levels."+str(i)+".submit_status":"submitted","approval_levels."+str(i+1)+".a_status":"current"}
							update_collection('EPMSKRAData',{"_id":ObjectId(form['prid'])},{'$set' :array3})
						
					return redirect(url_for('zenepms.appraiser_KRA_detailed_view'))
	EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
	return render_template('epms_appraiser_kra_view.html', user_details=user_details,list=a)


@zen_epms.route('/emps_kra_view/', methods = ['GET', 'POST'])
@login_required
def emps_kra_view():
	user_details = base()
	uid = current_user.username
	if request.method == 'POST':
		form=request.form
		
		if form['reporttype'] == "KRA":
			return redirect(url_for('zenepms.appraiser_kras_view',finyear=form['fin_date'],uid=uid))
		elif form['reporttype'] == "PMS":
			return redirect(url_for('zenepms.epms_appraiser',finyear=form['fin_date'],uid=uid))
		else:
			return redirect(url_for('zenuser.index'))
	return render_template('epms_kra_view.html', user_details=user_details)

# 7 LEVELS TEAM KRAS VIEW
@zen_epms.route('/emps_kra_teamview/', methods = ['GET', 'POST'])
@login_required
def emps_kra_teamview():
	user_details = base()
	uid = current_user.username
	if request.method == 'POST':
		form=request.form
		
		if form['reporttype'] == "KRA":
			return redirect(url_for('zenepms.appraiser_kras_teamview',finyear=form['fin_date'],uid=uid))
		elif form['reporttype'] == "PMS":
			return redirect(url_for('zenepms.epms_pms_teamview',finyear=form['fin_date'],uid=uid))
		else:
			return redirect(url_for('zenuser.index'))
	return render_template('epms_kra_teamview.html', user_details=user_details)

# KRAs Status Rpeort for HR
@zen_epms.route('/kra_status_report/', methods = ['GET', 'POST'])
@login_required
def kra_status_report():
	p=check_user_role_permissions('5789083aa82b5bb3a11b450a')
	if p:
		user_details = base()
		uid = current_user.username
		uid_id= current_user.get_id()
		
		fin_year=request.args['finyear']
		KRADetailedListEmployeesNotInitiated = []
		KRADetailedListEmployeesInitiated = []
		KRADetailedListUserSavedNotSubmitted = []
		KRADetailedListUserSubmitted = []
		KRADetailedListUserSubmittedAndAppraiserSubmitted = []
		KRADetailedListUserSubmittedAndAppraiserSubmittedAndReviewerSubmitted = []

		TotalUserInfoEmployees = find_in_collection('Userinfo',{"employee_status":"active"})
		print "totalusers",len(TotalUserInfoEmployees)
		
		for i in range(0,len(TotalUserInfoEmployees)):
			currentuserid = TotalUserInfoEmployees[i]['username']
			if (not find_one_in_collection('EPMSKRAData',{"UserId":currentuserid,'Finance_year':fin_year})):
				KRADetailedListEmployeesNotInitiated.append(currentuserid)
			elif find_one_in_collection('EPMSKRAData',{"UserId":currentuserid,"SubmitIndicator":"No",'Finance_year':fin_year}):
	   			KRADetailedListEmployeesInitiated.append(currentuserid)
			elif find_one_in_collection('EPMSKRAData',{"UserId":currentuserid,"SavedIndicator":"Yes","SubmitIndicator":"No",'Finance_year':fin_year}):
	   			KRADetailedListUserSavedNotSubmitted.append(currentuserid)
	   		elif find_one_in_collection('EPMSKRAData',{"UserId":currentuserid,"SubmitIndicator":"Yes",'Finance_year':fin_year,"approval_levels.0.submit_status":"pending","approval_levels.1.submit_status":"pending"}):
	   			KRADetailedListUserSubmitted.append(currentuserid)
	   		elif find_one_in_collection('EPMSKRAData',{"UserId":currentuserid,"SubmitIndicator":"Yes",'Finance_year':fin_year,"approval_levels.0.submit_status":"submitted","approval_levels.1.submit_status":"pending"}):
	   			KRADetailedListUserSubmittedAndAppraiserSubmitted.append(currentuserid)
	   		elif find_one_in_collection('EPMSKRAData',{"UserId":currentuserid,"SubmitIndicator":"Yes",'Finance_year':fin_year,"$or":[{"approval_levels.0.submit_status":"submitted","approval_levels.1.submit_status":"submitted"},{"approval_levels.0.submit_status":"submitted","approval_levels.1.a_id":"-"},{"approval_levels.1.submit_status":"submitted","approval_levels.1.a_id":"-"}]}):
	   			KRADetailedListUserSubmittedAndAppraiserSubmittedAndReviewerSubmitted.append(currentuserid)

		report=[]
		CountData = {}

		CountData['EmployeesNotInitiatedKRA'] = len(KRADetailedListEmployeesNotInitiated)
		CountData['EmployeesInitiatedKRA'] = len(KRADetailedListEmployeesInitiated)
		CountData['UserSavedNotSubmittedKRA'] = len(KRADetailedListUserSavedNotSubmitted)
		CountData['UserSubmittedKRA'] = len(KRADetailedListUserSubmitted)
		CountData['UserSubmitted-AppraiserSubmitted'] = len(KRADetailedListUserSubmittedAndAppraiserSubmitted)
		CountData['UserSubmitted-AppraiserSubmitted-ReviewerSubmitted'] = len(KRADetailedListUserSubmittedAndAppraiserSubmittedAndReviewerSubmitted)
		CountData['TotalActiveEmployees'] = len(TotalUserInfoEmployees)

		EPMSUserInfo = []

		for i in range(0,len(TotalUserInfoEmployees)):
			if TotalUserInfoEmployees[i]['username'] in KRADetailedListEmployeesNotInitiated:
		  		currentStatus = "Not Initiated KRAs"
		  	elif TotalUserInfoEmployees[i]['username'] in KRADetailedListEmployeesInitiated:
		 		currentStatus = "Initiated KRAs"
		 	elif TotalUserInfoEmployees[i]['username'] in KRADetailedListUserSavedNotSubmitted:
		  		currentStatus = "Saved KRAs"
		  	elif TotalUserInfoEmployees[i]['username'] in KRADetailedListUserSubmitted:
		  		currentStatus = "Submitted KRAs"
		  	elif TotalUserInfoEmployees[i]['username'] in KRADetailedListUserSubmittedAndAppraiserSubmitted:
		  		currentStatus = "Appraiser Approved"
		  	elif TotalUserInfoEmployees[i]['username'] in KRADetailedListUserSubmittedAndAppraiserSubmittedAndReviewerSubmitted:
		  		currentStatus = "Appraiser And Reviewer Approved"

		  	if 'department_name' in TotalUserInfoEmployees[i]: 
				if not TotalUserInfoEmployees[i]['department_name']:
					currentDepartment = "-"
				else:
					currentDepartment = TotalUserInfoEmployees[i]['department_name']			
			else:
				currentDepartment = "Not available"
			if 'plant_id' in TotalUserInfoEmployees[i]:
				if not TotalUserInfoEmployees[i]['plant_id']:
					currentplantId = "-"
				else:
					currentplantId = TotalUserInfoEmployees[i]['plant_id']
			else:
				currentplantId = "Not available"

			arrayTemp = {}
			arrayTemp ={
					"UserId":TotalUserInfoEmployees[i]['username'],
					"UserName":getuserfullname(TotalUserInfoEmployees[i]['username']),
					"Designation":TotalUserInfoEmployees[i]['designation'], 
					"Department":currentDepartment,
					"PayrollArea":currentplantId,
					"Status":currentStatus					
					}

			EPMSUserInfo.append(arrayTemp)

		# logging.info("---------------------------------------------")
		# logging.info(EPMSUserInfo)
		
		return render_template('epms_KRAs_Status_report.html',user_details=user_details,CountData=CountData,EPMSUserInfo=EPMSUserInfo,Fin_year=fin_year)
	else:
		return redirect(url_for('zenuser.index'))


# @zen_epms.route('/teamview/')
# @login_required
# def employee_history():
#     p =check_user_role_permissions('54b378d4507c00c54ba2229b')
#     if p:
#         user_details=base()
#         uid = current_user.username
#         print uid
#         p_id=get_designation_id(uid)
#         print p_id
#         employee_name=get_employee_name_given_pid(p_id)
#         print employee_name
#         emp1=[]
#         emp=[]
#         d=[]
#         employees_list=find_and_filter('Userinfo',{'employee_status':"active","m1_pid":p_id},{"_id":0,"username":1,"l_name":1})
#        	print "lenght",len(employees_list)
#         for i in employees_list:
#         	emp1.append(i['l_name'])
#         	emp.append(i['l_name'])
#         	print "list",emp
#         	s=emp1.index(i['l_name'])
#         	print "index",s
#         	p_id=get_designation_id(i['username'])
#         	employees_list1=find_and_filter('Userinfo',{'employee_status':"active","m1_pid":p_id},{"_id":0,"username":1,"l_name":1})
#         	for j in employees_list1:
#         		d.append(j['l_name'])
#         	emp1[s]={i['l_name']:d}	

#         	emp1.append(d)
#         	d=[]
       
#         return render_template('teamview.html', user_details=user_details,emp=emp,el=emp1,employee_name=employee_name)
#     else:
#         return redirect(url_for('zenuser.index'))




############ dev started on 17th march 2017
from operator import itemgetter, attrgetter
@zen_epms.route('/cp_goals_report/', methods = ['GET', 'POST'])
@login_required
def Goals_report():
	user_details = base()
	uid = current_user.username
	payroll_area=user_details['plant_id']
	plant_ids=find_all_in_collection("Plants")
	plant_ids = sorted(plant_ids, key=itemgetter('plant_id'))
	return render_template('cp_goals_report.html', user_details=user_details,payroll_area=payroll_area,plant_ids=plant_ids)

@zen_epms.route('/getdepartment/', methods = ['GET', 'POST'])
def getdepartment():
	form=request.form
	dept=find_and_filter("departmentmaster",{"plant":form['plant_id']},{'dept':1,'plant':1,'dept_pid':1,'_id':0})
	dept = sorted(dept, key=itemgetter('dept'))
	return jsonify({"results":dept})


def dept_name(dept_id):
	try:
		dept=find_and_filter("departmentmaster",{"dept_pid":dept_id},{'dept':1,'_id':0})
		dept=dept[0]['dept']
	except:
		dept=dept_id
	return dept

def emp_name(user_id_name):
	user_name=find_one_in_collection("Userinfo",{"username":user_id_name})
	return user_name['f_name']+' '+user_name['l_name']

@zen_epms.route('/default_emp_data_save/', methods = ['GET', 'POST'])
def default_emp_data_save():
	default_save=find_in_collection('Userinfo',{'employee_status':'active','username':{'$ne':'admin'},'designation_id':{'$ne':'999999999'},'plant_id':{'$exists':1}})
	cp_kra_status=find_one_in_collection('portal_settings',{"config":"cp_kra_form"})
	for ii in default_save:
		if ii['username'] in cp_kra_status['l1_employees']:
			emp_status='L1'
		else:
			emp_status='L2'
		check_details=find_one_in_collection('EmployeeGoals',{'finance_year':2016,'UserId':ii['username']})
		if check_details:
			pass
		else:
			update_collection('EmployeeGoals',{'UserId':ii['username']},{'$set':{'UserId':ii['username'],'finance_year':2016,'emp_status':emp_status,'plant_id':ii['plant_id'],'department':ii['department_id'],"savedindicater" : "No",'status':'Not Intiated'}})
			print 'updating........'
	return 'success'

@zen_epms.route('/employee_list_ajax/', methods = ['GET', 'POST'])
def employee_list_ajax():
	default_save=find_in_collection('Userinfo',{'employee_status':'active','username':{'$ne':'admin'},'designation_id':{'$ne':'999999999'},'plant_id':{'$exists':1}})
	cp_kra_status=find_one_in_collection('portal_settings',{"config":"cp_kra_form"})
	# for ii in default_save:
	# 	if ii['username'] in cp_kra_status['l1_employees']:
	# 		emp_status='L1'
	# 	else:
	# 		emp_status='L2'
	# 	check_details=find_one_in_collection('EmployeeGoals',{'finance_year':2016,'UserId':ii['username']})
	# 	if check_details:
	# 		pass
	# 	else:
	# 		update_collection('EmployeeGoals',{'UserId':ii['username']},{'$set':{'UserId':ii['username'],'finance_year':2016,'emp_status':emp_status,'plant_id':ii['plant_id'],'department':ii['department_id'],"savedindicater" : "No",'status':'Not Intiated'}})
	# 		print 'updating........'
	# print 'manual assigning done'
	form=request.form
	finance_year=int(form['FY_YEAR'])
	curr_user_plant_id=form['curr_user_plant_id']
	plant_id=form['plant']
	emp_level=form['emp_level']
	department=form['dept']
	submit_status=form['submit_status']
	submit_values={'GNI':'Not Intiated','GSAVE':'Saved','GSUB':'Submitted','GA':'Approved'}
	if submit_status=='':
		if plant_id !='':
			if emp_level !='' and department !='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'plant_id':plant_id,'emp_status':emp_level,'department':department})
			elif emp_level !='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'plant_id':plant_id,'emp_status':emp_level})
			elif emp_level =='' and department!='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'plant_id':plant_id,'department':department})
			elif emp_level =='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'plant_id':plant_id})	
		if plant_id =='':
			if emp_level !='' and department !='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'emp_status':emp_level,'department':department})
			elif emp_level !='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'emp_status':emp_level})
			elif emp_level =='' and department!='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'department':department})
			elif emp_level =='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year})
	else:
		if plant_id !='':
			if emp_level !='' and department !='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status],'plant_id':plant_id,'emp_status':emp_level,'department':department})
			elif emp_level !='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status],'plant_id':plant_id,'emp_status':emp_level})
			elif emp_level =='' and department!='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status],'plant_id':plant_id,'department':department})
			elif emp_level =='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status],'plant_id':plant_id})	
		if plant_id =='':
			if emp_level !='' and department !='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status],'emp_status':emp_level,'department':department})
			elif emp_level !='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status],'emp_status':emp_level})
			elif emp_level =='' and department!='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status],'department':department})
			elif emp_level =='' and department=='':
				employee_details=find_in_collection('EmployeeGoals',{'finance_year':finance_year,'status':submit_values[submit_status]})
	empdetails_arr=[]
	for i in employee_details:
		empdetails_arr.append([i['UserId'],emp_name(i['UserId']),dept_name(i['department']),i['emp_status'],i['plant_id'],i['status']])
	return jsonify({'result':empdetails_arr})


@zen_epms.route('/manager_goals_report/', methods = ['GET', 'POST'])
@login_required
def manager_goals_report():
	p =check_user_role_permissions('54b378d4507c00c54ba2229b')
	if p:
		user_details = base()
		uid = current_user.username
		employee_details=get_employees_under_manager()
		cp_kra_status=find_one_in_collection('portal_settings',{"config":"cp_kra_form"})
		employee_att=[]
		for i in employee_details:
			if i['username'] in cp_kra_status['l1_employees']:
				emp_status='L1'
			else:
				emp_status='L2'
			temp_details=find_one_in_collection('EmployeeGoals',{'UserId':i['username']})
			if temp_details:
				employee_att.append({'Userid':i['username'],'Name':i['f_name']+' '+i['l_name'],'emp_status':emp_status,'status':temp_details['status'].upper()})
			else:
				employee_att.append({'Userid':i['username'],'Name':i['f_name']+' '+i['l_name'],'emp_status':emp_status,'status':'Not Intiated'.upper()})
		return render_template('cp_manager_goals_report.html', user_details=user_details,employee_att=employee_att)
	else:
		return redirect(url_for('zenuser.index'))


def save_data_to_db(file,uid,goals_year):
	start_time=datetime.datetime.now()
	data = get_data(file)
	field_data=data
	fields=field_data['L1'][0]
	per_det={"savedindicater" : "Yes",'upload_status':'manual','status':'Saved'}
	dyn_array_fc=[]
	dyn_array_e=[]
	dyn_array_c=[]
	dyn_array_p=[]
	for i in range(0,len(field_data['L1'])):
		if str(field_data['L1'][i][0]).zfill(5)==uid:
			employee=uid
			if field_data['L1'][i][1]=='Financial/ Costing':
				per_det.update({'financialobjective':field_data['L1'][i][2],'UserId':employee})
				dyn_array_fc.append({"finitiative":[{"intiatives":"","finitidates":""}],'fmeasure':field_data['L1'][i][3],'fuom':field_data['L1'][i][4],'fmethodofcal':field_data['L1'][i][5],'fweightage':field_data['L1'][i][6]})
			elif field_data['L1'][i][1]=='Customer':
				per_det.update({'customerobjective':field_data['L1'][i][2],'UserId':employee})
				dyn_array_c.append({"cinitiative":[{"intiatives":"","cinitidates":""}],'cmeasure':field_data['L1'][i][3],'cuom':field_data['L1'][i][4],'cmethodofcal':field_data['L1'][i][5],'cweightage':field_data['L1'][i][6]})
			elif field_data['L1'][i][1]=='Process':
				per_det.update({'process':field_data['L1'][i][2],'UserId':employee})
				dyn_array_p.append({"pinitiative":[{"intiatives":"","pinitidates":""}],'pmeasure':field_data['L1'][i][3],'puom':field_data['L1'][i][4],'pmethodofcal':field_data['L1'][i][5],'pweightage':field_data['L1'][i][6]})
			elif field_data['L1'][i][1]=='Employee':
				per_det.update({'employee':field_data['L1'][i][2],'UserId':employee})
				dyn_array_e.append({"einitiative":[{"intiatives":"","einitidates":""}],'emeasure':field_data['L1'][i][3],'euom':field_data['L1'][i][4],'emethodofcal':field_data['L1'][i][5],'eweightage':field_data['L1'][i][6]})
			# dyn_array.append({fields[3]:field_data['L1'][i][3],fields[4]:field_data['L1'][i][4],fields[5]:field_data['L1'][i][5],fields[6]:field_data['L1'][i][6]})
	per_det.update({'financialobjectivedetails':dyn_array_fc,'customerobjectivedetails':dyn_array_c,'processobjectivedetials':dyn_array_p,'employeeobjectivedetails':dyn_array_e})
	update_collection('EmployeeGoals',{'UserId':uid,'finance_year':goals_year},{'$set':per_det})
	per_det={}
	dyn_array_fc=[]
	dyn_array_e=[]
	dyn_array_c=[]
	dyn_array_p=[]
	return 'Saved Sucessfully'



####### changed submit indicator_l2 and saveindicator_l2 to saveindicator and submit indicator
@zen_epms.route('/cp_KRA', methods = ['GET', 'POST'])
@login_required
def cp_KRA():
	user_details = base()
	# print err_msg
	if 'cp_kra_l1' in user_details:
		uid = current_user.username
		uid_id= current_user.get_id()
		db=dbconnect()
		cp_kra_config=find_one_in_collection('portal_settings',{'config':'cp_kra_form'})
		user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid_id)},{"designation_id":1,"_id":0,"plant_id":1,"calendar_code":1})
		year = datetime.date.today().year

		m = datetime.date.today()
		mon='{:02d}'.format(m.month)
		finance_year=0
		emps_year=0
		val=4
		val='{:02d}'.format(val)
		if mon>=val:
			goals_year=year
		else:
			goals_year=year-1
		ff_year=0
		eff_year=0
		ff_year=(str(year))+"-"+(str(year+1))
		eff_year=(str(year-1))+"-"+(str(year))
		
		if(not find_one_in_collection('EmployeeGoals',{"UserId":uid,'finance_year':goals_year})):
			array1={"UserId":uid,"finance_year":goals_year,"savedindicater":"No",'status':'Not Intiated','department':user_details['department_id'],'plant_id':user_details['plant_id'],'emp_status':'L1'}
			save_collection("EmployeeGoals",array1)
			employee_att=find_one_in_collection("EmployeeGoals",{"UserId":uid,'finance_year' : goals_year})
			########### the below code should be uncomment when uploading Employee Goals Manually
			if cp_kra_config['upload']=='Enabled':
				err_msg=save_data_to_db(cp_kra_config['upload_path'],uid,goals_year)
				logging.info(err_msg+' ---- '+uid)
			# return render_template('cp_kra_form.html',user_details=user_details,ff_year=ff_year,eff_year=eff_year)
		else:
			# employee_att = db.EmployeeGoals.aggregate([{'$match' : {'UserId': uid}},
			# { '$unwind': '$financialobjectivedetails'},{ '$unwind': '$customerobjectivedetails'},{ '$unwind': '$processobjectivedetials'},{ '$unwind': '$employeeobjectivedetails'},
			# {'$match' : {'finance_year' : goals_year}}])
			employee_att=find_one_in_collection("EmployeeGoals",{"UserId":uid,'finance_year' : goals_year})
			# employee_att=list(employee_att)
			# print employee_att
		# print "result",list(employee_att)
			# print "Financal",len(employee_att['financialobjectivedetails'])
		if request.method == 'POST':
			form=request.form
			# print form
			#financial / costing strategic objectives
			fmeasure=form.getlist('fmeasure')
			fuom=form.getlist('fuom')
			fmethodofcalculation=form.getlist('fmethodofcalculation')
			fweightage=form.getlist('fweightage')
			fbasevalue=form.getlist('fbase')
			ftarget=form.getlist('ftarget')
			finitiative=form.getlist('finitiative')
			fcompeltiondate=form.getlist('finitidate')
			# print"FInitative",finitiative
			# print str(fmeasure)+":"+str(fuom)+":"+str(fmethodofcalculation)+":"+str(fweightage)+":"+str(fbasevalue)+":"+str(ftarget)+":"+str(finitiative)+":"+str(fcompeltiondate)
			#customer startegic objectives
			cmeasure=form.getlist('cmeasure')
			cuom=form.getlist('cuom')
			cmethodofcalculation=form.getlist('cmethodofcalculation')
			cweightage=form.getlist('cweightage')
			cbasevalue=form.getlist('cbase')
			ctarget=form.getlist('ctarget')
			cinitiative=form.getlist('cinitiative')
			ccompeltiondate=form.getlist('cinitidate')
			# print len(cmeasure)
			#process objectives
			pmeasure=form.getlist('pmeasure')
			puom=form.getlist('puom')
			pmethodofcalculation=form.getlist('pmethodofcalculation')
			pweightage=form.getlist('pweightage')
			pbasevalue=form.getlist('pbase')
			ptarget=form.getlist('ptarget')
			pinitiative=form.getlist('pinitiative')
			pcompeltiondate=form.getlist('pinitidate')
			# print len(pmeasure)
			#employee objectives
			emeasure=form.getlist('emeasure')
			euom=form.getlist('euom')
			emethodofcalculation=form.getlist('emethodofcalculation')
			eweightage=form.getlist('eweightage')
			ebasevalue=form.getlist('ebase')
			etarget=form.getlist('etarget')
			einitiative=form.getlist('einitiative')
			ecompeltiondate=form.getlist('einitidate')
			# print len(emeasure)

			financialobjective=form['financialobjective']
			customerobjective=form['customerobjective']
			process=form['process']
			employee=form['employee']
			TotalWeightageSum=form['TotalWeightageSum']
			approval_levels=[]
			user_rm=find_one_in_collection('Userinfo',{'username':uid})  #Current User Who is Logged IN
			rms_list=["m1_pid","m2_pid","m3_pid","m4_pid","m5_pid","m6_pid","m7_pid"]
			approval_list=[]
			view_levels=[]
			view_list=[]

			q=find_one_in_collection('Positions',{"position_id":user_data[0]['designation_id']})
			for rm in rms_list:
				rm1_username=find_one_in_collection('Userinfo',{'designation_id':user_rm[rm]})
				try:
					if rm1_username['designation_id'] in q['levels']:
						
						approval_list.append(rm1_username['username'])
					else:
						view_list.append(rm1_username['username'])
				except:
					logging.info("no user")
				

			for i in range(len(view_list)):
				appraiserInfo = find_one_in_collection('Userinfo',{'username':view_list[i]})
				a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
				view_levels = view_levels+[{"a_id":view_list[i],"a_status":"view",
						"a_name":a_name}]
			
			for i in range(0,len(approval_list)):
				if i==0:
					
					appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
					a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
					a_designation = appraiserInfo['designation']
					approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"current",
						'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
				elif i ==1:
					
					appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
					a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
					a_designation = appraiserInfo['designation']
					approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"pending",
						'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
				else:
					logging.debug(uid+" "+"User Dosent have any Approval Levels")

			lengthOfApprovalLevels = len(approval_levels)
			
			if(lengthOfApprovalLevels==1):
			   appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[0]})
			   a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
			   a_designation = appraiserInfo['designation']
			   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
						'submit_status':"pending","a_name":"-","a_designation":"-"}]

			if(len(approval_list)==0):
			   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
						'submit_status':"pending","a_name":"-","a_designation":"-"}]+[{"a_id":"-","a_status":"current",
						'submit_status':"pending","a_name":"-","a_designation":"-"}]
			financialobjectivedetails=[]
			customerobjectivedetails=[]
			processobjectivedetials=[]
			employeeobjectivedetails=[]
			if form['submit'] == 'Save':
				# print "Save block"			
				for i in range(0,len(fmeasure)):
					# print "f",form.getlist('finitiative'+str(i))
					finitiatives=[]
					for ii in range(len(form.getlist('finitiative'+str(i)))):
						finitiatives.append({'intiatives':form.getlist('finitiative'+str(i))[ii],'finitidates':form.getlist('finitidate'+str(i))[ii]})
						# print "finit",{'intiatives':form.getlist('finitiative'+str(ii))[ii],'finitidates':form.getlist('finitidate'+str(ii))[ii]},' -------- loop',ii
					financialobjectivedetails.append({"fmeasure":fmeasure[i],"fuom":fuom[i],"fmethodofcal":fmethodofcalculation[i],"fweightage":fweightage[i],"fbase":fbasevalue[i],"ftraget":ftarget[i],"finitiative":finitiatives})
					# print "append"			
				for j in range(0,len(cmeasure)):
					# print "c",form.getlist('cinitiative'+str(j))
					cinitiatives=[]
					for jj in range(len(form.getlist('cinitiative'+str(j)))):
						cinitiatives.append({'intiatives':form.getlist('cinitiative'+str(j))[jj],'cinitidates':form.getlist('cinitidate'+str(j))[jj]})

					customerobjectivedetails.append({"cmeasure":cmeasure[j],"cuom":cuom[j],"cmethodofcal":cmethodofcalculation[j],"cweightage":cweightage[j],"cbase":cbasevalue[j],"ctraget":ctarget[j],"cinitiative":cinitiatives})
				for k in range(0,len(pmeasure)):
					# print "p",form.getlist('pinitiative'+str(k))
					pinitiatives=[]
					for kk in range(len(form.getlist('pinitiative'+str(k)))):
						pinitiatives.append({'intiatives':form.getlist('pinitiative'+str(k))[kk],'pinitidates':form.getlist('pinitidate'+str(k))[kk]})
					processobjectivedetials.append({"pmeasure":pmeasure[k],"puom":puom[k],"pmethodofcal":pmethodofcalculation[k],"pweightage":pweightage[k],"pbase":pbasevalue[k],"ptraget":ptarget[k],"pinitiative":pinitiatives})
				
				for l in range(0,len(emeasure)):
					einitiatives=[]
					for ll in range(len(form.getlist('einitiative'+str(l)))):
						einitiatives.append({'intiatives':form.getlist('einitiative'+str(l))[ll],'einitidates':form.getlist('einitidate'+str(l))[ll]})
					employeeobjectivedetails.append({"emeasure":emeasure[l],"euom":euom[l],"emethodofcal":emethodofcalculation[l],"eweightage":eweightage[l],"ebase":ebasevalue[l],"etraget":etarget[l],"einitiative":einitiatives})

				array={"financialobjectivedetails":financialobjectivedetails,"customerobjectivedetails":customerobjectivedetails,"processobjectivedetials":processobjectivedetials,"employeeobjectivedetails":employeeobjectivedetails,"financialobjective":financialobjective,"customerobjective":customerobjective,"process":process,"employee":employee,"approval_levels":approval_levels,"view_levels":view_levels,"submitindicater":"No","savedindicater":"Yes",'status':'Saved',"savedon":datetime.datetime.now(),"TotalWeightageSum":TotalWeightageSum}
				# save_collection("EmployeeGoals",array)
				update_collection('EmployeeGoals',{"UserId":uid,'finance_year' : goals_year},{'$set' :array})
				flash('Sucessfully Saved Details.','alert-success')
				return redirect(url_for('zenepms.cp_KRA'))
			else: #Submit block
				# print "Submit block"
				for i in range(0,len(fmeasure)):
					# print "f",form.getlist('finitiative'+str(i))
					finitiatives=[]
					for ii in range(len(form.getlist('finitiative'+str(i)))):
						finitiatives.append({'intiatives':form.getlist('finitiative'+str(i))[ii],'finitidates':form.getlist('finitidate'+str(i))[ii]})
						# print "finit",{'intiatives':form.getlist('finitiative'+str(ii))[ii],'finitidates':form.getlist('finitidate'+str(ii))[ii]},' -------- loop',ii
					financialobjectivedetails.append({"fmeasure":fmeasure[i],"fuom":fuom[i],"fmethodofcal":fmethodofcalculation[i],"fweightage":fweightage[i],"fbase":fbasevalue[i],"ftraget":ftarget[i],"finitiative":finitiatives})
					# print "append"			
				for j in range(0,len(cmeasure)):
					# print "c",form.getlist('cinitiative'+str(j))
					cinitiatives=[]
					for jj in range(len(form.getlist('cinitiative'+str(j)))):
						cinitiatives.append({'intiatives':form.getlist('cinitiative'+str(j))[jj],'cinitidates':form.getlist('cinitidate'+str(j))[jj]})

					customerobjectivedetails.append({"cmeasure":cmeasure[j],"cuom":cuom[j],"cmethodofcal":cmethodofcalculation[j],"cweightage":cweightage[j],"cbase":cbasevalue[j],"ctraget":ctarget[j],"cinitiative":cinitiatives})
				for k in range(0,len(pmeasure)):
					# print "p",form.getlist('pinitiative'+str(k))
					pinitiatives=[]
					for kk in range(len(form.getlist('pinitiative'+str(k)))):
						pinitiatives.append({'intiatives':form.getlist('pinitiative'+str(k))[kk],'pinitidates':form.getlist('pinitidate'+str(k))[kk]})
					processobjectivedetials.append({"pmeasure":pmeasure[k],"puom":puom[k],"pmethodofcal":pmethodofcalculation[k],"pweightage":pweightage[k],"pbase":pbasevalue[k],"ptraget":ptarget[k],"pinitiative":pinitiatives})
				
				for l in range(0,len(emeasure)):
					einitiatives=[]
					for ll in range(len(form.getlist('einitiative'+str(l)))):
						einitiatives.append({'intiatives':form.getlist('einitiative'+str(l))[ll],'einitidates':form.getlist('einitidate'+str(l))[ll]})
					employeeobjectivedetails.append({"emeasure":emeasure[l],"euom":euom[l],"emethodofcal":emethodofcalculation[l],"eweightage":eweightage[l],"ebase":ebasevalue[l],"etraget":etarget[l],"einitiative":einitiatives})

				array={"financialobjectivedetails":financialobjectivedetails,"customerobjectivedetails":customerobjectivedetails,"processobjectivedetials":processobjectivedetials,"employeeobjectivedetails":employeeobjectivedetails,"financialobjective":financialobjective,"customerobjective":customerobjective,"process":process,"employee":employee,"approval_levels":approval_levels,"view_levels":view_levels,"submitindicater":"Yes","savedindicater":"Yes",'status':'Submitted',"submittedon":datetime.datetime.now(),"TotalWeightageSum":TotalWeightageSum}
				# save_collection("EmployeeGoals",array)
				update_collection('EmployeeGoals',{"UserId":uid,'finance_year' : goals_year},{'$set' :array})
				flash('Sucessfully submitted.','alert-success')
				return redirect(url_for('zenepms.cp_KRA'))
		employee_att=find_one_in_collection("EmployeeGoals",{"UserId":uid,'finance_year' : goals_year})
		EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
		return render_template('cp_kra_form.html',user_details=user_details,ff_year=ff_year,eff_year=eff_year,employee_att=employee_att,EPMSGlobalSettings=EPMSGlobalSettings)
	else:
		flash('The Page You Are Looking Is Not Available At The Moment !!','alert-danger')
		return redirect(url_for('zenuser.index'))

def upload_kra_l2(file,uid,goals_year):
	data = get_data(file)
	field_data=data
	fields_l2=field_data['L2'][0]
	l2_update={'savedindicater':'Yes','upload_status':'manual','status':'Saved'}
	dyn_arr_l2=[]
	fields_l2=field_data['L2'][0]
	for jj in range(0,len(field_data['L2'])):
		if str(field_data['L2'][jj][0]).zfill(5)==uid:
			employee_l2=uid
			# print employee_l2
			l2_update.update({'UserId':employee_l2})
			dyn_arr_l2.append({'measures':field_data['L2'][jj][1],'uom':field_data['L2'][jj][2],'methodofcal':field_data['L2'][jj][3],'weightage':str(field_data['L2'][jj][4]),'base':'','target':''})
		l2_update.update({'kradetials':dyn_arr_l2})
	update_collection('EmployeeGoals',{'UserId':uid,'finance_year':goals_year},{'$set':l2_update})
	l2_update={}
	dyn_arr_l2=[]
	return 'saved sucessfully'



@zen_epms.route('/cp_KRA1', methods = ['GET', 'POST'])
@login_required
def cp_KRA1():
	user_details = base()
	# print user_details
	if 'cp_kra_l2' in user_details:
		uid = current_user.username
		uid_id= current_user.get_id()
		db=dbconnect()
		cp_kra_config=find_one_in_collection('portal_settings',{'config':'cp_kra_form'})
		user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid_id)},{"designation_id":1,"_id":0,"plant_id":1,"calendar_code":1})
		year = datetime.date.today().year

		m = datetime.date.today()
		mon='{:02d}'.format(m.month)
		finance_year=0
		emps_year=0
		val=4
		val='{:02d}'.format(val)
		if mon>=val:
			goals_year=year
		else:
			goals_year=year-1
		ff_year=0
		eff_year=0
		ff_year=(str(year))+"-"+(str(year+1))
		eff_year=(str(year-1))+"-"+(str(year))
		
		if(not find_one_in_collection('EmployeeGoals',{"UserId":uid,'finance_year':goals_year})):
			# print "No record in collection"
			array1={"UserId":uid,"finance_year":goals_year,"savedindicater" : "No",'status':'Not Intiated','department':user_details['department_id'],'plant_id':user_details['plant_id'],'emp_status':'L2'}
			save_collection("EmployeeGoals",array1)
			##### THe below code should be uncommented when uploading the excel file for saving the Goals Data of employees.
			if cp_kra_config['upload']=='Enabled':
				err_msg=upload_kra_l2(cp_kra_config['upload_path'],uid,goals_year)
				logging.info(err_msg+' ---- '+uid)
			userstatus='new'
			employee_att=find_one_in_collection("EmployeeGoals",{"UserId":uid,'finance_year' : goals_year})
		else:
			employee_att=find_one_in_collection("EmployeeGoals",{"UserId":uid,'finance_year' : goals_year})
			userstatus='existing'

		if request.method == 'POST':
			form=request.form
			
			fmeasure=form.getlist('measure')
			fuom=form.getlist('uom')
			fmethodofcalculation=form.getlist('methodofcal')
			fweightage=form.getlist('weightage')
			fbasevalue=form.getlist('base')
			ftarget=form.getlist('target')
			finitiative=form.getlist('initiatives')
			TotalWeightageSum=form['TotalWeightageSum']
			print "data",fmeasure
			
			approval_levels=[]
			user_rm=find_one_in_collection('Userinfo',{'username':uid})  #Current User Who is Logged IN
			rms_list=["m1_pid","m2_pid","m3_pid","m4_pid","m5_pid","m6_pid","m7_pid"]
			approval_list=[]
			view_levels=[]
			view_list=[]

			q=find_one_in_collection('Positions',{"position_id":user_data[0]['designation_id']})
			for rm in rms_list:
				rm1_username=find_one_in_collection('Userinfo',{'designation_id':user_rm[rm]})
				try:
					if rm1_username['designation_id'] in q['levels']:
						
						approval_list.append(rm1_username['username'])
					else:
						view_list.append(rm1_username['username'])
				except:
					logging.info("no user")
				

			for i in range(len(view_list)):
				appraiserInfo = find_one_in_collection('Userinfo',{'username':view_list[i]})
				a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
				view_levels = view_levels+[{"a_id":view_list[i],"a_status":"view",
						"a_name":a_name}]
			
			for i in range(0,len(approval_list)):
				if i==0:
					
					appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
					a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
					a_designation = appraiserInfo['designation']
					approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"current",
						'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
				elif i ==1:
					
					appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[i]})
					a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
					a_designation = appraiserInfo['designation']
					approval_levels = approval_levels+[{"a_id":approval_list[i],"a_status":"pending",
						'submit_status':"pending","a_name":a_name,"a_designation":a_designation}]
				else:
					logging.debug(uid+" "+"User Dosent have any Approval Levels")

			lengthOfApprovalLevels = len(approval_levels)
			
			if(lengthOfApprovalLevels==1):
			   appraiserInfo = find_one_in_collection('Userinfo',{'username':approval_list[0]})
			   a_name = appraiserInfo['f_name'] + " " + appraiserInfo['l_name']
			   a_designation = appraiserInfo['designation']
			   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
						'submit_status':"pending","a_name":"-","a_designation":"-"}]

			if(len(approval_list)==0):
			   approval_levels = approval_levels+[{"a_id":"-","a_status":"current",
						'submit_status':"pending","a_name":"-","a_designation":"-"}]+[{"a_id":"-","a_status":"current",
						'submit_status':"pending","a_name":"-","a_designation":"-"}]
			kradetial=[]
			if form['submit'] == 'Save':	
				for i in range(0,len(fmeasure)):
					kradetial.append({"measures":fmeasure[i],"uom":fuom[i],"methodofcal":fmethodofcalculation[i],"weightage":fweightage[i],"base":fbasevalue[i],"target":ftarget[i],"initiatives":finitiative[i]})
				
				array={"approval_levels":approval_levels,"view_levels":view_levels,"submitindicater":"No","savedindicater":"Yes",'status':'Saved',"savedon":datetime.datetime.now(),"TotalWeightageSum":TotalWeightageSum,"kradetials":kradetial}
				logging.info
				update_collection('EmployeeGoals',{"UserId":uid,'finance_year' : goals_year},{'$set' :array})
				flash('Sucessfully Saved Details.','alert-success')
				return redirect(url_for('zenepms.cp_KRA1'))
			else: # submit block
				for i in range(0,len(fmeasure)):
					kradetial.append({"measures":fmeasure[i],"uom":fuom[i],"methodofcal":fmethodofcalculation[i],"weightage":fweightage[i],"base":fbasevalue[i],"target":ftarget[i],"initiatives":finitiative[i]})
				
				array={"approval_levels":approval_levels,"view_levels":view_levels,"submitindicater":"Yes",'status':'Submitted',"savedindicater":"Yes","Submittedon":datetime.datetime.now(),"savedon":datetime.datetime.now(),"TotalWeightageSum":TotalWeightageSum,"kradetials":kradetial}
				update_collection('EmployeeGoals',{"UserId":uid,'finance_year' : goals_year},{'$set' :array})
				flash('Submitted Sucessfully.','alert-success')
				return redirect(url_for('zenepms.cp_KRA1'))
		EPMSGlobalSettings = find_one_in_collection('EPMSGlobalSettings',{"Active":"Yes"})
		print "golabal",EPMSGlobalSettings
		return render_template('cp_kra_form_l2.html',user_details=user_details,employee_att=employee_att,userstatus=userstatus,EPMSGlobalSettings=EPMSGlobalSettings)
	else:
		flash('The Page You Are Looking Is Not Available At The Moment !!','alert-danger')
		return redirect(url_for('zenuser.index'))



@zen_epms.route('/cp_kra_approval/', methods = ['GET', 'POST'])
@login_required
def cp_kra_approval():
	db=dbconnect()
	# p =check_user_role_permissions('54b378d4507c00c54ba2229b')
 #    if p:
	user_details = base()
	uid = current_user.username
	if request.method == 'POST':
		form=request.form
		print "final date",
		goals_year=form['final_date']
		employee_att = db.EmployeeGoals.aggregate([{'$match' : {"submitindicater":"Yes","finance_year":int(goals_year)}},
			{ '$unwind': '$approval_levels' },
					{'$match' : {'approval_levels.a_id' : uid}}])
		employee_att = list(employee_att)
		# print len(list(employee_att))
		# employee_att=find_one_in_collection("EmployeeGoals",{"approval_levels.a_id":uid,'finance_year' : int(goals_year)})
		a=[]
		for e in employee_att:
			e['fullname']=get_employee_name(e['UserId'])
			e['savedon'] = e['savedon'].strftime("%d/%m/%Y")
			e['UserId']=e['UserId']
			e['submitindicater'] = e['submitindicater']
			a=a+[e]
		return render_template('cp_kra_approval_list.html', user_details=user_details,list=a)
	return render_template('cp_kra_approval_search.html', user_details=user_details)
	# else:
	#     return redirect(url_for('zenuser.index'))

@zen_epms.route('/cp_appraiser_goals_info_view', methods = ['GET', 'POST'])
@login_required
def cp_appraiser_goals_info_view():
	db=dbconnect()
	user_details = base()
	uid = current_user.username
	year = datetime.date.today().year
	ff_year=0
	eff_year=0
	ff_year=(str(year))+"-"+(str(year+1))
	eff_year=(str(year-1))+"-"+(str(year))
	if request.method == 'POST':
		form=request.form
		print "PDIII",form['prid']
		employee_att=find_one_in_collection("EmployeeGoals",{"_id":ObjectId(form['prid'])})
		fullname=get_employee_name(employee_att['UserId'])
		if form['submit'] == 'View':
			if employee_att['emp_status'] == 'L1':
				
				return render_template('cpr_kra_approval_L1_form.html', user_details=user_details,ff_year=ff_year,eff_year=eff_year,employee_att=employee_att,empname=fullname)
			else:
				return render_template('cpr_kra_approval_L2_form.html', user_details=user_details,ff_year=ff_year,eff_year=eff_year,employee_att=employee_att,empname=fullname)
		elif form['submit'] == 'TeamView':
			if employee_att['emp_status'] == 'L1':
				
				return render_template('cpr_kra_team_view_L1_form.html', user_details=user_details,ff_year=ff_year,eff_year=eff_year,employee_att=employee_att,empname=fullname)
			else:
				return render_template('cpr_kra_team_view_L2_form.html', user_details=user_details,ff_year=ff_year,eff_year=eff_year,employee_att=employee_att,empname=fullname)

		elif form['submit'] == 'Approve':
			print "prid",form['prid']
			uid = form['prid']
		
			flash("KRAs for year "+str(employee_att['finance_year'])+"-"+str(employee_att['finance_year']+1)+" has been approved successfully - Manager",'alert-success')
			
			a_levels = find_one_in_collection('EmployeeGoals',{"_id":ObjectId(uid)})
			count_a_levels=len(a_levels['approval_levels'])
			print "submit levels",count_a_levels
			for i in range(0,count_a_levels):
				if a_levels['approval_levels'][i]['a_status']=="current":
					j=i
					if j==count_a_levels-1:
						print "Approval--2",j
						array3={"SubmitIndicator_Appraiser":"Yes","SubmitIndicator_Reviewer":"Yes","approval_levels."+str(i)+".a_status":"approved","approval_levels."+str(i)+".submit_status":"submitted","status":"Approved"}
						
					else:
						print "Approval-1",j
						array3={"SubmitIndicator_Appraiser":"Yes","approval_levels."+str(i)+".a_status":"approved","approval_levels."+str(i)+".submit_status":"submitted","approval_levels."+str(i+1)+".a_status":"current"}
					update_collection('EmployeeGoals',{"_id":ObjectId(form['prid'])},{'$set' :array3})
			
			employee_att=find_one_in_collection("EmployeeGoals",{"_id":ObjectId(form['prid'])})	
			if employee_att['emp_status'] == 'L1':
				return render_template('cpr_kra_approval_L1_form.html', user_details=user_details,ff_year=ff_year,eff_year=eff_year,employee_att=employee_att,empname=fullname)
			else:
				return render_template('cpr_kra_approval_L2_form.html', user_details=user_details,ff_year=ff_year,eff_year=eff_year,employee_att=employee_att,empname=fullname)
		elif form['submit'] == 'Reject':
			uid = form['prid']
			a_levels = find_one_in_collection('EmployeeGoals',{"_id":ObjectId(uid)})
			count_a_levels=len(a_levels['approval_levels'])
			print "submit levels",count_a_levels
			for i in range(0,count_a_levels):
				if a_levels['approval_levels'][i]['a_status']=="current":
					j=i
					if j==count_a_levels-1:
						print "Approval--2",j
						array3={"approval_levels."+str(i)+".a_status":"rejected","approval_levels."+str(i)+".submit_status":"submitted","status":"Rejected","savedindicater":"Yes","submitindicater":"No","reject_falg":"Y"}
						
					else:
						print "Approval-1",j
						array3={"approval_levels."+str(i)+".a_status":"rejected","approval_levels."+str(i)+".submit_status":"submitted","approval_levels."+str(i+1)+".a_status":"pending","status":"Rejected","savedindicater":"Yes","submitindicater":"No","reject_falg":"Y"}
					update_collection('EmployeeGoals',{"_id":ObjectId(form['prid'])},{'$set' :array3})
			
			flash("KRAs for year "+str(employee_att['finance_year'])+"-"+str(employee_att['finance_year']+1)+" has been rejected - Manager",'alert-danger')
		return redirect(url_for('zenepms.cp_kra_approval'))
	return render_template('cp_kra_approval_search.html', user_details=user_details)

@zen_epms.route('/cp_kra_team_view/', methods = ['GET', 'POST'])
@login_required
def cp_kra_team_view():
	db=dbconnect()
	# p =check_user_role_permissions('54b378d4507c00c54ba2229b')
 #    if p:
	user_details = base()
	uid = current_user.username
	if request.method == 'POST':
		form=request.form
		print "final date",
		goals_year=form['final_date']
		employee_att = db.EmployeeGoals.aggregate([{'$match' : {"submitindicater":"Yes","finance_year":int(goals_year)}},
			{ '$unwind': '$view_levels' },
					{'$match' : {'view_levels.a_id' : uid}}])
		employee_att = list(employee_att)
		# print len(list(employee_att))
		# employee_att=find_one_in_collection("EmployeeGoals",{"approval_levels.a_id":uid,'finance_year' : int(goals_year)})
		a=[]
		for e in employee_att:
			e['fullname']=get_employee_name(e['UserId'])
			e['savedon'] = e['savedon'].strftime("%d/%m/%Y")
			e['UserId']=e['UserId']
			e['submitindicater'] = e['submitindicater']
			a=a+[e]
		return render_template('cp_kra_team_list.html', user_details=user_details,list=a)
	return render_template('cp_kra_team_view_search.html', user_details=user_details)



@zen_epms.route('/usermanual', methods = ['GET', 'POST'])
@login_required
def usermanual():
	user_details = base()
	return render_template('user_manuals.html', user_details=user_details)