import pymongo
from pymongo import MongoClient
import smtplib
import email.utils
from email.mime.text import MIMEText
import logging
import datetime
from pyrfc import *

def dbconnect() :
	client = MongoClient('192.168.18.10')
	db = client['pcil_npx_dev']
	return db

def getlast_index(coll,field):
	last_index=find_all_in_collection_sort(coll,[(field,-1)])
	index=last_index[0][field]
	return index

def save_collection(collection,arr):
	db = dbconnect()
	coll = db[collection]
	data = coll.insert(arr)
	return data

def find_all_in_collection_sort(collection,sort_arr):
	db = dbconnect()
	coll = db[collection]
	cursor = coll.find().sort(sort_arr)
	result = [item for item in cursor]
	return result

def sync_attendance_data():
	count=0
	server_connections=find_in_collection('attendance_config',{'config':'sap_dev_license'})
	try:
		last_index=getlast_index('Auth_Log','INDEXKEY')
		conn = Connection(user=server_connections[0]['user'], passwd=server_connections[0]['passwd'], ashost=server_connections[0]['ashost'],gwserv=server_connections[0]['gwserv'], sysnr=server_connections[0]['sysnr'], client=server_connections[0]['client'])
		result = conn.call('Z_HR_ACCESS_CTRL_AUTHLOG',IM_INDEXKEY=last_index)
		print len(result['IT_USER_AUTHLOG'])		
		# try:
		for i in result['IT_USER_AUTHLOG']:
			print i
			SERVERRECORDTIME=datetime_str_to_datetime(i['SERVERRECORDTIME'])
			array={'INDEXKEY':i['INDEXKEY'],'EMP_ID':i['EMP_ID'],'USERNAME':i['USERNAME'],'BUKRS':i['BUKRS'],'EMP_TYP':i['EMP_TYP'],'ETYP_CODE':i['ETYP_CODE'],'PLNT_CODE':i['PLNT_CODE'],
			'TERMINALID':i['TERMINALID'],'TERMINALNAME':i['TERMINALNAME'],'SERVERRECORDTIME':SERVERRECORDTIME,'ERDAT':str(i['ERDAT']),
			'USERID':i['USERID']}
			
			save_collection('Auth_Log',array)
			count+=1
		msg='Successfully Updated '+str(count)+' Authentications'
		last_index_after=getlast_index('Auth_Log','INDEXKEY')
		logging.info(msg+' From indexes '+str(last_index)+' to '+str(last_index_after)+' on '+datetime_desc())
		# logging.info(msg+' From indexes '+str(last_index)+' to '+str(last_index_after)+' on '+datetime_desc())
		# except:
		# 	msg="Data Is Up-To-Date"
		# 	print msg
	except:
		msg="SAP Connection Error !!!"
	logging.info('function executed on '+str(datetime.datetime.now()))


sync_attendance_data()