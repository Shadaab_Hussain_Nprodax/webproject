from zenapp.dbfunctions import *
from bson.objectid import ObjectId
from zenapp.modules.user.userfunctions import *
from flask_login import current_user
import datetime
import logging
import calendar
from pyrfc import *
import urllib,urllib2,urlparse


def SendSms(message,mobilenumber,uname):
	url = "http://www.smscountry.com/smscwebservice_bulk.aspx"
	values = {'user' : 'pcilhr',
	'passwd' : 'pcil@123',
	'message' : "Dear "+uname+','+message,
	'mobilenumber':mobilenumber,
	'mtype':'N',
	'DR':'Y'
	}

	data = urllib.urlencode(values)
	data = data.encode('utf-8')
	request = urllib2.Request(url,data)
	response = urllib2.urlopen(request)

 #function to create notification in myportal for an employee
def notification(employee_id,message):
	save_collection('Notifications',{"e_id":employee_id,"msg":message,"created_time":datetime.datetime.now()})



def get_employee_name(e_id):
	data=find_and_filter('Userinfo',{"username":e_id},{"_id":0,"f_name":1,"l_name":1})
	fullname=data[0]['f_name']+' '+data[0]['l_name']
	return fullname

def get_attendance_type(a_id):
	data=find_one_in_collection('AttendanceTypes',{"att_id":a_id})
	return data['att_type']

def get_leave_type(l_id):
	data=find_one_in_collection('LeaveTypes',{"leave_id":l_id})
	return data['leave_type']

def get_leave_reason(r_id):
	data=find_one_in_collection('LeaveReasons',{"reason_id":r_id})
	return data['reason']

def get_employee_id(e_pid):
	data=find_and_filter('Userinfo',{"designation_id":e_pid},{"_id":0,"username":1})
	return data[0]['username']
	
def get_designation_id(e_pid):
	data=find_and_filter('Userinfo',{"username":e_pid},{"_id":0,"designation_id":1})
	return data[0]['designation_id']

def get_hr_id(plant_id):
	data=find_and_filter('Plants',{"plant_id":plant_id},{"_id":0,"plant_current_hr":1})
	data1=find_and_filter('Userinfo',{"designation_id":data[0]['plant_current_hr']},{"_id":0,"username":1})
	return data1[0]['username']

def get_plant_id(uid):
	data=find_and_filter('Userinfo',{"username":uid},{"_id":0,"plant_id":1})
	return data[0]['plant_id']

def get_employee_name_given_pid(p_id):
	data=find_and_filter('Userinfo',{"designation_id":p_id},{"_id":0,"f_name":1,"l_name":1})
	fullname=data[0]['f_name']+' '+data[0]['l_name']
	return fullname
#send email or sms or notification to employee
def get_employee_email_mobile(uid,emailsubject,emailmsg,smsg):
	data=find_and_filter('Userinfo',{"username":uid},{"_id":0,"official_email":1,"official_contact":1,"personal_contact1":1})
	fname=get_employee_name(uid)
	if data[0]['official_email'] != '':
		save_collection('Email',{"emailsubject":emailsubject,"receiver":data[0]['official_email'],"msg":emailmsg,"fname":fname,"flag":"1"})
	elif data[0]['official_contact'] != '':
		SendSms(smsg,data[0]['official_contact'],fname)
	elif data[0]['personal_contact1'] != '':
		SendSms(smsg,data[0]['personal_contact1'],fname)
	else:
		notification(uid,'Please update your official email or official mobile number or personal mobile number')

def get_leave_time(plant_id):
	data=find_and_filter('Plants',{"plant_id":plant_id},{"_id":0,"leave_time":1})
	return data[0]['leave_time']

def get_employee_designation_id(e_id):
	data=find_and_filter('Userinfo',{"username":e_id},{"_id":0,"designation_id":1})
	return data[0]['designation_id']

def get_employees_list():
	uid = current_user.username
	employees_list=[{'PERNR':uid}]
	d_id=get_employee_designation_id(uid)
	p_id=get_plant_id(uid)
	data=find_and_filter('Userinfo', {'$or': [{"m1_pid":d_id},{"m2_pid":d_id},{"m3_pid":d_id},{"m4_pid":d_id},{"m5_pid":d_id},{"m6_pid":d_id},{"m7_pid":d_id}]},{"_id":0,"username":1})
	if data:
		for i in range(0,len(data)):
			employees_list+=[{'PERNR':data[i]['username']}]
	return employees_list

def get_employees_under_manager():
	uid = current_user.username
	employees_list=[{'PERNR':uid}]  #username off RM
	d_id=get_employee_designation_id(uid) #designation id of user logdein(RM)
	p_id=get_plant_id(uid)
	data=find_and_filter('Userinfo', {'employee_status':"active",'$or': [{"m1_pid":d_id},{"m2_pid":d_id},{"m3_pid":d_id},{"m4_pid":d_id},{"m5_pid":d_id},{"m6_pid":d_id},{"m7_pid":d_id}],"employee_status":"active"},{"_id":0,"username":1,"f_name":1,"l_name":1})
	return data

def get_calendar_data(e_id):
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":e_id}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00"}},
		{'$project':{'_id':0,'date':'$schedule.date'}}])
	k=[]
	data=list(data)
	if data:		
		for i in range(0,len(data)):
			k+=[{"date":str(data[i]['date'].month)+'-'+str(data[i]['date'].day)+'-'+str(data[i]['date'].year)}]
	return k
# employee cannot apply a leave if same type of previous leave is under process
def business_rule_1(uname,leave_type):
	leave_ex=find_one_in_collection('Leaves',{"employee_id":uname,"attribute":"leave","leave_type":leave_type,"status":"applied"})
	if leave_ex:
		m='Your previous '+leave_type+' is in process. please apply '+leave_type+' once its approved.'
	else:
		m='0'
	return m

### new enhancement moved on 23 Dec 2016
# sap validations
def business_rule_2(uname,start_date,end_date,list1,leave_type,fromtime,totime):
	sd=datetime.datetime.strptime(start_date,"%d/%m/%Y")
	newsd=sd.strftime("%Y%m%d")
	ed=datetime.datetime.strptime(end_date,"%d/%m/%Y")
	newed=ed.strftime("%Y%m%d")
	sap_login=get_sap_user_credentials()
	try:
		conn = Connection(user=sap_login['user'], passwd=sap_login['passwd'], ashost=sap_login['ashost'],gwserv=sap_login['gwserv'], sysnr=sap_login['sysnr'], client=sap_login['client'])
		# conn = Connection(user='veerahcm', passwd='Veera@143', ashost=sap_login['ashost'],sysnr=sap_login['sysnr'], gwserv=sap_login['gwserv'], client=sap_login['client'])
		if leave_type not in ['2','Permission']:
			if len(list1) == 0 :
				result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY=leave_type,IM_BEGDA=newsd,IM_ENDDA=newed,IM_SIMULATION='X')
			elif len(list1) == 1 :
				if list1[0] == "1":
					result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY=leave_type,IM_BEGDA=newsd,IM_ENDDA=newed,IM_BFLAG='X',IM_SIMULATION='X')
				else:
					result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY=leave_type,IM_BEGDA=newsd,IM_ENDDA=newed,IM_BFLAG='X',IM_SIMULATION='X')
			else:
				result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY=leave_type,IM_BEGDA=newsd,IM_ENDDA=newed,IM_BFLAG='X',IM_EFLAG='X',IM_SIMULATION='X')
		else:
			if leave_type == '2':
				if len(list1) == 0 :
					result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY='OD',IM_BEGDA=newsd,IM_ENDDA=newed,IM_SIMULATION='X',IM_BEGUZ=fromtime ,IM_ENDUZ=totime)
				elif len(list1) == 1 :
					if list1[0] == "1":
						result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY='OD',IM_BEGDA=newsd,IM_ENDDA=newed,IM_BFLAG='X',IM_SIMULATION='X',IM_BEGUZ=fromtime ,IM_ENDUZ=totime)
					else:
						result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY='OD',IM_BEGDA=newsd,IM_ENDDA=newed,IM_BFLAG='X',IM_SIMULATION='X',IM_BEGUZ=fromtime ,IM_ENDUZ=fromtime)
				else:
					result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY='OD',IM_BEGDA=newsd,IM_ENDDA=newed,IM_BFLAG='X',IM_EFLAG='X',IM_SIMULATION='X',IM_BEGUZ=fromtime ,IM_ENDUZ=fromtime)
				print result,' simulation testing'
			elif leave_type == 'Permission':
				if len(list1) == 0 :
					result = conn.call('Z_LMS_LEAVE_APPLICATION',IM_PERNR=uname,IM_SUBTY='PERM',IM_BEGDA=newsd,IM_ENDDA=newed,IM_SIMULATION='X',IM_BEGUZ=fromtime ,IM_ENDUZ=totime)
		conn.close()
		close_sap_user_credentials(sap_login['_id'])
		if result['CH_RETURN']['TYPE'] == 'E':
			m=result['CH_RETURN']['MESSAGE']
		else:
			m='0'
	except:
		m='Unable to connect Sap user Credential'
	close_sap_user_credentials(sap_login['_id'])
	return m


# total no of leaves in a applied casual leave excluding holidays and weekly halfs
def total_no_of_days_leave(uname,start_date,end_date,list1):
        #if start_date and end_date is datetime.datetime no need to convert else convert string date to datetime.datetime
        if type(start_date)== datetime.datetime and type(end_date) == datetime.datetime :
                sdate=start_date
                edate=end_date
        else:
                sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
                edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	total_days=(edate-sdate).days+1
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':{'$gt':sdate,'$lt':edate}}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	data=list(data)
	total_days-=len(data)
	if len(list1) == 1 :
		total_days-=0.5
	elif len(list1) == 2:
		total_days-=1
	return total_days

# total no of leaves in a applied earned/sick leave excluding holidays and weekly halfs
def total_no_of_days_el(uname,start_date,end_date,list1):
        #if start_date and end_date is datetime.datetime no need to convert else convert string date to datetime.datetime
        if type(start_date)== datetime.datetime and type(end_date) == datetime.datetime :
                sdate=start_date
                edate=end_date
        else:
                sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
                edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	total_days=(edate-sdate).days+1
	if len(list1) == 1 :
		total_days-=0.5
	elif len(list1) == 2:
		total_days-=1
	return total_days

# list of holidays in between start date and end date
# total no of leaves in a applied leave excluding holidays and weekly halfs
# start date is holiday or not
# end date is holiday or not 
def list_of_holidays_total_days(uname,start_date,end_date,list1,leave_type):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	#print "Leave Date",sdate
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	total_days=(edate-sdate).days+1
	if len(list1) == 1 :
		total_days-=0.5
	elif len(list1) == 2:
		total_days-=1	
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':{'$gt':sdate,'$lt':edate}}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	data=list(data)
	if leave_type == 'CL' or leave_type == 'LWP' or leave_type == 'EL':
		total_days-=len(data)
	data1=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':sdate}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	data1=list(data1)
	if len(data1) == 0:
		st_date='No'
	else:
		st_date='Yes'
	data2=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':edate}},
		{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours','date':'$schedule.date'}}])
	data2=list(data2)
	if len(data2) == 0:
		en_date='No'
	else:
		en_date='Yes'
	return {"total_days":total_days,"holidays":data,"st_date":st_date,"en_date":en_date}
# no of cls available to apply now
def business_rule_3(uname,employee_type):
	leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":uname})
	current_month=datetime.datetime.now().month
	if employee_type == 'T':
		if (current_month < 11):
			leaves_available=current_month*0.5+1-leave_quota['leaves_taken']
		else:
			leaves_available=leave_quota['available_cls']-leave_quota['leaves_taken']
	else:
		if (current_month < 11):
			leaves_available=current_month+2-leave_quota['leaves_taken']
		else:
			leaves_available=leave_quota['available_cls']-leave_quota['leaves_taken']
	return leaves_available
# how many times els applied
def business_rule_4(uname):
	current_time=datetime.datetime.now()
	yr_first=datetime.datetime(current_time.year,1,1)
	next_year=datetime.datetime(current_time.year+1,1,1)
	el_applied=find_in_collection('Leaves',{"employee_id":uname,"leave_type":"EL","status":"approved","applied_time":{'$gte':yr_first,'$lt':next_year}})
	return len(el_applied)
#update approve leaves/attendance taken data
def update_approved_leaves_in_leave_quota(uid,leave_type,n_leaves):
	quota_details=find_one_in_collection('LeaveQuota',{"employee_id":uid})
	if leave_type == 'CL':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"leaves_taken":quota_details['leaves_taken']+n_leaves}})
	elif leave_type == 'SL':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"sls_taken":quota_details['sls_taken']+n_leaves}})
	elif leave_type == 'EL':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"els_taken":quota_details['els_taken']+n_leaves}})
	elif leave_type == 'LWP':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"lwps_taken":quota_details['lwps_taken']+n_leaves}})
	elif leave_type == '1':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"tours_taken":quota_details['tours_taken']+n_leaves}})
	elif leave_type == '2':
		update_coll('LeaveQuota',{"employee_id":uid},{'$set': {"onduty_taken":quota_details['onduty_taken']+n_leaves}})
	return 'success'

def no_of_holidays(uname,start_date,end_date):
	work_schedule=find_one_in_collection('WorkSchedule',{"employee_id":uname})
	db=dbconnect()
	data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
		{'$unwind':'$schedule'},
		{'$match':{'schedule.worked_hours' : "0.00",'schedule.date':{'$gte':start_date,'$lte':end_date}}},
		{'$project':{'_id':0,'date':'$schedule.date'}}])
	data=list(data)
	return len(data)

def get_leave_chart_data(uid,s_date,e_date,halfdays,leave_type):
    #no need to convert start and end dates to datetime.datetime because they are already in datetime.datetime
	#start_date=datetime.datetime.strptime(s_date,"%d/%m/%Y")
	#end_date=datetime.datetime.strptime(e_date,"%d/%m/%Y")
	start_date=s_date
	end_date=e_date
	current_year=datetime.datetime.now().year
	if start_date.year == end_date.year and start_date.year==current_year:
		data=find_one_in_collection('ChartData',{"employee_id":uid,"year":start_date.year})
		if data:
			if start_date.month == end_date.month:
				holidays=no_of_holidays(uid,start_date,end_date)
				new_month= start_date.month-1
				tnd=1+end_date.day-start_date.day-0.5*len(halfdays)-holidays+data['chartdata'][new_month][leave_type]
				array={}
				update_coll('ChartData',{"employee_id":uid,"year":start_date.year},{'$set': {"chartdata."+str(start_date.month-1)+"."+leave_type:tnd}})			
			else:
				c=calendar.monthrange(start_date.year, start_date.month)[1]
				new_sd_month= start_date.month-1
				tnd=1+c-start_date.day+data['chartdata'][new_sd_month][leave_type]
				month_end_date=datetime.datetime(start_date.year, start_date.month,c)
				holidays1=no_of_holidays(uid,start_date,month_end_date)
				tnd-+holidays1
				new_ed_month = end_date.month-1
				tnd1=end_date.day+data['chartdata'][new_ed_month][leave_type]
				month_start_date=datetime.datetime(end_date.year, end_date.month,1)
				holidays2=no_of_holidays(uid,month_start_date,month_end_date)
				tnd1-+holidays2
				if len(halfdays) == 2 :
					tnd-=0.5
					tnd1-=0.5
				elif len(halfdays) == 1 :
					if halfdays[1]=="1":
						tnd-=0.5
					else:
						tnd1-=0.5
				update_coll('ChartData',{"employee_id":uid,"year":start_date.year},{'$set': {"chartdata."+str(start_date.month-1)+"."+leave_type:tnd,"chartdata."+str(end_date.month-1)+"."+leave_type:tnd1}})
		else:
			pass
	return 'success'
#if a CL is applied there should not be any SL or EL before or after
def business_rule_5(uname,start_date,end_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	new_sdate=sdate-datetime.timedelta(days=1)
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	new_edate=edate+datetime.timedelta(days=1)
	a='yes'
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_sdate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		data=list(data)
		if data[0]['worked_hours'] == "0.00" :
			a = 'yes'
			new_sdate-=datetime.timedelta(days=1)
		else:
			a = 'no'
	b='yes'
	while(b == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_edate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		data=list(data)
		if data[0]['worked_hours'] == "0.00" :
			b = 'yes'
			new_edate+=datetime.timedelta(days=1)
		else:
			b = 'no'
	#print new_sdate
	#print new_edate
	leaves=find_and_filter('Leaves',{"employee_id":uname,'$and':[{'$or':[{"leave_type":"SL"},{"leave_type":"EL"}]},{'$or':[{"status":"approved"},{"status":"applied"}]}]},{"_id":0,"end_date":1,"start_date":1})
	#print leaves
	allow_applied_leave = 0
	for l in leaves:
                #leves_copy date is datetime format so no need to convert
		#if new_sdate == datetime.datetime.strptime(l['end_date'], "%d/%m/%Y") :
                if new_sdate == l['end_date'] :
			allow_applied_leave+=1
		#elif new_edate == datetime.datetime.strptime(l['start_date'], "%d/%m/%Y") :
                elif new_edate == l['start_date']:
			allow_applied_leave+=1
	return allow_applied_leave 
		
#if EL or SL is applied there should not be any CL before or after
def business_rule_6(uname,start_date,end_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	new_sdate=sdate-datetime.timedelta(days=1)
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	new_edate=edate+datetime.timedelta(days=1)
	a='yes'
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_sdate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		data=list(data)
		if data[0]['worked_hours'] == "0.00" :
			a = 'yes'
			new_sdate-=datetime.timedelta(days=1)
		else:
			a = 'no'
	b='yes'
	while(b == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_edate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		data=list(data)
		if data[0]['worked_hours'] == "0.00" :
			b = 'yes'
			new_edate+=datetime.timedelta(days=1)
		else:
			b = 'no'
	leaves=find_and_filter('Leaves',{"employee_id":uname,"leave_type":"CL",'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1})
	allow_applied_leave = 0
	#print leaves
	if leaves:
		for l in leaves:
                        #in leaves dateformat is datetime.datetime so no need to convert
			#if new_sdate == datetime.datetime.strptime(l['end_date'], "%d/%m/%Y") :
                        if new_sdate == l['end_date']:
				allow_applied_leave+=1
			#elif new_edate == datetime.datetime.strptime(l['start_date'], "%d/%m/%Y") :
			elif new_edate == l['start_date']:
				allow_applied_leave+=1
	#print allow_applied_leave
	return allow_applied_leave 
#leave cannot apply in already applied leave dates
def business_rule_7(uname,start_date,end_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y") #datetime.datetime
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")	# datetime.datetime
	leaves=find_and_filter('Leaves',{"employee_id":uname,'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1})
	previous_leaves = 0
	for l in leaves:
                n_sdate=l['start_date'] #if n_sdate and n_edate are of string format convert it to datetiem.datetime
		n_edate=l['end_date']	
		if n_sdate <= sdate <= n_edate :
			previous_leaves+=1
		elif n_sdate <= edate <= n_edate :
			previous_leaves+=1
		elif sdate <= n_sdate <= edate :
			previous_leaves+=1
		elif sdate <= n_edate <= edate :
			previous_leaves+=1
	
	return previous_leaves 

def business_rule_7_1(uname,start_date,end_date,from_time,to_time):
	if from_time != "":#attendance on Duty
		time_to12={"00:00":"0:00 AM","00:15":"0:15 AM","00:30":"0:30 AM","00:45":"0:45 AM","01:00":"1:00 AM","01:15":"1:15 AM","01:30":"1:30 AM","01:45":"1:45 AM"
		,"02:00":"2:00 AM","02:15":"2:15 AM","02:30":"2:30 AM","02:45":"2:45 AM","03:00":"3:00 AM","03:15":"3:15 AM","03:30":"3:30 AM","03:45":"3:45 AM"
		,"04:00":"4:00 AM","04:15":"4:15 AM","04:30":"4:30 AM","04:45":"4:45 AM","05:00":"5:00 AM","05:15":"5:15 AM","05:30":"5:30 AM","05:45":"5:45 AM"
		,"06:00":"6:00 AM","06:15":"6:15 AM","06:30":"6:30 AM","06:45":"6:45 AM","07:00":"7:00 AM","07:15":"7:15 AM","07:30":"7:30 AM","07:45":"7:45 AM"
		,"08:00":"8:00 AM","08:15":"8:15 AM","08:30":"8:30 AM","08:45":"8:45 AM","09:00":"9:00 AM","09:15":"9:15 AM","09:30":"9:30 AM","09:45":"9:45 AM"
		,"10:00":"10:00 AM","10:15":"10:15 AM","10:30":"10:30 AM","10:45":"10:45 AM","11:00":"11:00 AM","11:15":"11:15 AM","11:30":"11:30 AM","11:45":"11:45 AM"
		,"12:00":"12:00 PM","12:15":"12:15 PM","12:30":"12:30 PM","12:45":"12:45 PM","13:00":"1:00 PM","13:15":"1:15 PM","13:30":"1:30 PM","13:45":"1:45 PM"
		,"14:00":"2:00 PM","14:15":"2:15 PM","14:30":"2:30 PM","14:45":"2:45 PM","15:00":"3:00 PM","15:15":"3:15 PM","15:30":"3:30 PM","15:45":"3:45 PM"
		,"16:00":"4:00 PM","16:15":"4:15 PM","16:30":"4:30 PM","16:45":"4:45 PM","17:00":"5:00 PM","17:15":"5:15 PM","17:30":"5:30 PM","17:45":"5:45 PM"
		,"18:00":"6:00 PM","18:15":"6:15 PM","18:30":"6:30 PM","18:45":"6:45 PM","19:00":"7:00 PM","19:15":"7:15 PM","19:30":"7:30 PM","19:45":"7:45 PM"
		,"20:00":"8:00 PM","20:15":"8:15 PM","20:30":"8:30 PM","20:45":"8:45 PM","21:00":"9:00 PM","21:15":"9:15 PM","21:30":"9:30 PM","21:45":"9:45 PM"
		,"22:00":"10:00 PM","22:15":"10:15 PM","22:30":"10:30 PM","22:45":"10:45 PM","23:00":"11:00 PM","23:15":"11:15 PM","23:30":"11:30 PM","23:45":"11:45 PM"}
		from_time=str(from_time).strip().split(' ')
		to_time=str(to_time).strip().split(' ')
		from_time=from_time[0].strip().split(':')
		to_time=to_time[0].strip().split(':')

		start_date=start_date+"/"+from_time[0]+"/"+from_time[1]
		end_date=end_date+"/"+to_time[0]+"/"+to_time[1]

		sdate=datetime.datetime.strptime(start_date,"%d/%m/%Y/%H/%M") #datetime.datetime
		edate=datetime.datetime.strptime(end_date,"%d/%m/%Y/%H/%M")	# datetime.datetime
		leaves=find_and_filter('Leaves',{"employee_id":uname,'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1,"to_time":1,"from_time":1})
		leaves1=find_in_collection_sort('Leaves',{"employee_id":uname,'from_time':{'$exists':1},'$or':[{"status":"approved"},{"status":"applied"}]},[("_id",1)])
		# leaves2=find_and_filter('Leaves',{"employee_id":uname,"form_time":{'$exists':1},'$or':[{"attribute":"attendance"},{"attribute":"leave"}]},{"_id":0,"end_date":1,"start_date":1,"to_time":1,"from_time":1})
		leaves_var=[]
		a=[]
		att_ftime=""
		att_ttime=""
		for i in leaves1:
			a=i
		# print a['employee_id']
		previous_attendance = 0
		for l in leaves:
			if 'from_time' in l:
				try:
					from_time_12H_24=list(time_to12.keys())[list(time_to12.values()).index(l['from_time'])]
					from_time_12H=from_time_12H_24.strip().split(':')
					to_time_12H_24=list(time_to12.keys())[list(time_to12.values()).index(l['to_time'])]
					to_time_12H=to_time_12H_24.strip().split(':')
					l['start_date']=l['start_date']+datetime.timedelta(hours=int(from_time_12H[0]),minutes=int(from_time_12H[1]))
					l['end_date']=l['end_date']+datetime.timedelta(hours=int(to_time_12H[0]),minutes=int(to_time_12H[1]))
				except:					
					att_ftime=l['from_time']
					att_ttime=l['to_time']
					l['from_time']=l['from_time'].strip().split(' ')
					l['from_time']=l['from_time'][0].strip().split(':')
					l['to_time']=l['to_time'].strip().split(' ')
					l['to_time']=l['to_time'][0].strip().split(':')
					# print l['from_time']
					l['start_date']=l['start_date']+datetime.timedelta(hours=int(l['from_time'][0]),minutes=int(l['from_time'][1]))
					l['end_date']=l['end_date']+datetime.timedelta(hours=int(l['to_time'][0]),minutes=int(l['to_time'][1]))
					# print l['start_date']
			else:
				l['start_date']=l['start_date']
				l['end_date']=l['end_date']
			n_sdate=l['start_date']
			n_edate=l['end_date']	
			if n_sdate <= sdate <= n_edate :
				previous_attendance+=1
				att_ftime=att_ftime
				att_ttime=att_ttime
			elif n_sdate <= edate <= n_edate :
				previous_attendance+=1
				att_ftime=att_ftime
				att_ttime=att_ttime
			elif sdate <= n_sdate <= edate :
				previous_attendance+=1
				att_ftime=att_ftime
				att_ttime=att_ttime
			elif sdate <= n_edate <= edate :
				previous_attendance+=1
				att_ftime=att_ftime
				att_ttime=att_ttime
			
			elif n_sdate <= sdate <= n_edate :
				previous_attendance+=1
				att_ftime=att_ftime
				att_ttime=att_ttime
			else:
				pass
			n_sdate=n_sdate.strftime("%d/%m/%y")
			n_edate=n_edate.strftime("%d/%m/%y")

		if att_ftime:
			try:
				results=[previous_attendance,a,time_to12[str(att_ftime)],time_to12[str(att_ttime)],n_sdate,n_edate]
			except:
				results=[previous_attendance,a,time_to12[str(from_time_12H_24)],time_to12[str(to_time_12H_24)],n_sdate,n_edate]
		else:
			results=[previous_attendance,a]
	elif from_time == "":#attendance on Tour
		sdate=datetime.datetime.strptime(start_date,"%d/%m/%Y") #datetime.datetime
		edate=datetime.datetime.strptime(end_date,"%d/%m/%Y")	# datetime.datetime
		leaves=find_and_filter('Leaves',{"employee_id":uname,'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1,"to_time":1,"from_time":1})
		# print a['employee_id']
		previous_attendance = 0
		# print "start time " +str(sdate)
		# print "end_date "+str(edate)
		if leaves:
			for l in leaves:
				n_sdate=l['start_date']
				n_edate=l['end_date']
				if n_sdate <= sdate <= n_edate :
					previous_attendance+=1
					n_sdate=n_sdate
					n_edate=n_edate
				elif n_sdate <= edate <= n_edate :
					previous_attendance+=1
					n_sdate=n_sdate
					n_edate=n_edate
				elif sdate <= n_sdate <= edate :
					previous_attendance+=1
					n_sdate=n_sdate
					n_edate=n_edate
				elif sdate <= n_edate <= edate :
					previous_attendance+=1
					n_sdate=n_sdate
					n_edate=n_edate
				elif n_sdate <= sdate <= n_edate :
					previous_attendance+=1
					n_sdate=n_sdate
					n_edate=n_edate
				else:
					pass
			n_sdate=n_sdate.strftime("%d/%m/%y")
			n_edate=n_edate.strftime("%d/%m/%y")
			results=[previous_attendance,n_sdate,n_edate]
		else:
			results=[previous_attendance]
	return results 


def business_rule_9(uname,p_date):
	leaves=find_and_filter('Leaves',{"employee_id":uname,"leave_type":"CL",'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1})
	previous_leaves = 0
	for l in leaves:
		#date is in datetime.datetime no need to convert
		#n_sdate=datetime.datetime.strptime(l['start_date'], "%d/%m/%Y")
		n_sdate=l['start_date']
		n_edate=l['end_date']
		#n_edate=datetime.datetime.strptime(l['end_date'], "%d/%m/%Y")
		if n_sdate <= p_date <= n_edate :
			previous_leaves+=1	
	return previous_leaves 

def business_rule_8(uname,start_date):
	sdate=datetime.datetime.strptime(start_date, "%d/%m/%Y")
	new_sdate=sdate-datetime.timedelta(days=1)
	a='yes'
	b=0
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_sdate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		data=list(data)
		if data[0]['worked_hours'] != "0.00" :
			p_leaves=business_rule_9(uname,new_sdate)
			if p_leaves > 0 :
				b+=1
				a = 'yes'
				new_sdate-=datetime.timedelta(days=1)
			else:
				a = 'no'
		else:
			a = 'yes'
			new_sdate-=datetime.timedelta(days=1)
	return b 


def business_rule_10(uname,end_date):
	edate=datetime.datetime.strptime(end_date, "%d/%m/%Y")
	new_edate=edate+datetime.timedelta(days=1)
	a='yes'
	b=0
	while(a == 'yes'):		
		db=dbconnect()
		data=db.WorkSchedule.aggregate([{'$match':{"employee_id":uname}},
			{'$unwind':'$schedule'},
			{'$match':{'schedule.date':new_edate}},
			{'$project':{'_id':0,'worked_hours':'$schedule.worked_hours'}}])
		data=list(data)
		if data[0]['worked_hours'] != "0.00" :
			p_leaves=business_rule_9(uname,new_edate)
			if p_leaves > 0 :
				b+=1
				a = 'yes'
				new_edate+=datetime.timedelta(days=1)
			else:
				a = 'no'
		else:
			a = 'yes'
			new_edate+=datetime.timedelta(days=1)
	return b 


# Below Function are usefull for Checking LTA Rules
def remove_months(sourcedate,months):
    month = sourcedate.month - 1 - months
    year = int(sourcedate.year + month / 12 )
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.datetime(year,month,day)

#LTA Should Apply Only Once In a Financial Year
def business_rule_11(uname):
	current_time=datetime.datetime.now()
	yr_first=datetime.datetime(current_time.year,1,4)
	next_year=datetime.datetime(current_time.year+1,1,3)
	el_applied=find_in_collection('Leaves',{"employee_id":uname,"lta":"1","leave_type":"EL","status":"approved","applied_time":{'$gte':yr_first,'$lt':next_year}})
	return len(el_applied)

#Minimum 10 Month Gap Should Maintained between 2 consecutive LTA
def business_rule_12(uname):
	current_time=datetime.datetime.now()
	yr_first=datetime.datetime(current_time.year,current_time.month,current_time.day)
	last_year= remove_months(yr_first,10)
	el_applied=find_in_collection('Leaves',{"employee_id":uname,"lta":"1","leave_type":"EL","status":"approved","applied_time":{'$gte':last_year,'$lt':yr_first}})
	return len(el_applied)


##### new enhancements for adding permission moved on 23 Dec 2016
def formatDate(dtDateTime,strFormat="%d/%m/%Y"):
    # format a datetime object as YYYY-MM-DD string and return
    return dtDateTime.strftime(strFormat)

def mk_DateTime(dateString,strFormat="%d/%m/%Y"):# Expects "YYYY-MM-DD" string
    eSeconds = time.mktime(time.strptime(dateString,strFormat))
    return datetime.datetime.fromtimestamp(eSeconds)

def mkFirstOfMonth(dtDateTime):
    return mk_DateTime(formatDate(dtDateTime,"01/%m/%Y"))

def mkLastOfMonth(dtDateTime):
    dYear = dtDateTime.strftime("%Y")        #get the year
    dYear=str(int(dYear)+1)
    dMonth = str(int(dtDateTime.strftime("%m"))%12+1)#get next month, watch rollover
    dDay = "1"                               #first day of next month
    nextMonth = mk_DateTime("%s/%s/%s"%(dDay,dMonth,dYear))#make a datetime obj for 1st of next month
    delta = datetime.timedelta(seconds=1)    #create a delta of 1 second
    return nextMonth - delta

def date_time_datetime(strdate,strtime):
    new_dt=strdate+' '+strtime
    result_datetime=datetime.datetime.strptime(new_dt,'%d/%m/%Y %H:%M:%S')
    return result_datetime

def business_rule_13(empid,per_date,from_time,to_time,plant_id):#per_date - 'DD-MM-YYYY'
	sdate=datetime.datetime.strptime(per_date, "%d/%m/%Y") #datetime.datetime
	edate=sdate
	leaves=find_and_filter('Leaves',{"employee_id":empid,'$or':[{"status":"approved"},{"status":"applied"}]},{"_id":0,"end_date":1,"start_date":1})
	prev_leaves = 0
	result=0
	for l in leaves:
		n_sdate=l['start_date'] #if n_sdate and n_edate are of string format convert it to datetiem.datetime
		n_edate=l['end_date']
		if n_sdate <= sdate <= n_edate :
			prev_leaves+=1
		elif n_sdate <= edate <= n_edate :
			prev_leaves+=1
		elif sdate <= n_sdate <= edate :
			prev_leaves+=1
		elif sdate <= n_edate <= edate :
			prev_leaves+=1
	if prev_leaves==0:
		permissions=find_and_filter('Leaves',{"employee_id":empid,'attribute':'permission','$or':[{"status":"approved"},{"status":"applied"}],'start_date':{'$gt':mkFirstOfMonth(mk_DateTime(per_date)),'$lte':mkLastOfMonth(mk_DateTime(per_date))}},{"_id":0,"end_date":1,"start_date":1})
		permission_limit=find_and_filter('Plants',{"plant_id":plant_id},{"_id":0,"leave_time":1,'permissions_limit':1})
		from_time=from_time+':00'
		to_time=to_time+':00'
		difference=date_time_datetime(per_date,to_time)-date_time_datetime(per_date,from_time)
		difference=str(difference).split(':')
		if len(permissions)<permission_limit[0]['permissions_limit']:
			if from_time!=to_time:
				if int(difference[0])<=2 and int(difference[1])<=0:
					msg='accepted'
					result=1
				elif int(difference[0])>=0 and int(difference[1])>0:
					msg='accepted'
					result=1
				else:
					msg='Permission Can Be Applied For Only 2 hr\'s.'
			else:
				msg='start time and end time cannot be same.'
		else:
			msg='You Have Reached Permission Limit.You Are Not Eligible To Apply Anymore.'
	else:
		msg='you have already appled leave/attendance/permission on '+per_date+' .'
	return [result,msg,prev_leaves]

def count_permissions(plant_id,emp_id):
	per_date=datetime.datetime.now().date().strftime('%d/%m/%Y')	
	permissions=find_and_filter('Leaves',{"employee_id":emp_id,'attribute':'permission','$or':[{'status':'applied'},{'status':'approved'}],'start_date':{'$gt':mkFirstOfMonth(mk_DateTime(per_date)),'$lte':mkLastOfMonth(mk_DateTime(per_date))}},{"_id":0,"end_date":1,"start_date":1})
	permission_limit=find_and_filter('Plants',{"plant_id":plant_id},{"_id":0,"leave_time":1,'permissions_limit':1,'permission_duration':1})
	logging.info('permissions taken by employee : '+ emp_id+' , '+str(len(permissions)))
	return [permission_limit[0]['permissions_limit'],permission_limit[0]['permissions_limit']-len(permissions),len(permissions),permission_limit[0]['permission_duration']]


