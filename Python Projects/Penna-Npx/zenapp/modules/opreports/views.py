from flask import Flask, render_template, redirect, request, flash, url_for,Response, jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import calendar
import hashlib
import gridfs
import uuid
import csv
import os
import xlsxwriter
import cgi
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import StringIO
from datetime import date
from datetime import timedelta
from zenapp.modules.leave.lfunctions import *
#from pyrfc import *
import xlsxwriter
import json
import random
import sys
reload(sys)
sys.setdefaultencoding('utf8')
from array import array
from fpdf import FPDF
import smtplib
import email.utils
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import email.mime.application
import email.mime.multipart
import logging
# from aifc import data
# from reportlab.pdfgen import canvas
# from django.http import HttpResponse
zen_reports = Blueprint('zenreports', __name__, url_prefix='/opreports')


######################### moved to production on 30th June 2017 ######################### 

import random
from werkzeug import secure_filename
# --------------------- Kaizen Dev 
@zen_reports.route('/Kaizen_Entry/', methods = ['GET', 'POST'])
@login_required
def Kaizen_Entry():
	user_details = base()	
	if request.method=='POST':
		form=request.form
		if form['submit']=='Save' or form['submit']=='Submit':
			print '-------------------------------------------------------------------'
			kaizen_docs_inprogress=find_one_in_collection('Kaizen_details',{'username':user_details['username'],'kaizen_date':form['kaizen_date'],'$or':[{'status':'Approved'},{'status':'Submitted'},{'status':'Saved'}]})			
			if not kaizen_docs_inprogress:
				user_palnt=form['plant']
				kaizen_date=form['kaizen_date']
				prob_identified=form['prob_identified']
				countermeasure=form['Countermeasure']
				try:
					implementation_cost=int(form['imp_cost'])
				except:
					implementation_cost=0
				result=form['result']
				try:
					tangible_cost=int(form['tangible_amnt'])
				except:
					tangible_cost=0
				if 'InTangible_option' in form and form['InTangible_option']=='checked':
					Intangible_sel_option=form['InTangible_sel_option']
					Intangible_desc=form['InTangible_desc']
					Intangible='checked'
					InTangible_details={'Intangible_sel_option':Intangible_sel_option,'Intangible_desc':Intangible_desc}
				else:
					Intangible_sel_option=""
					Intangible_desc=""
					Intangible='unchecked'
					InTangible_details={}
				if form['imp_type']=='ind_imp':
					emp_ids=form.getlist('emp_id')
					emp_desigs=form.getlist('desig')
					implementation_type='individual'
					print emp_ids,'@kaizenDetails-Employee_ids'
					imp_details=[{'employee_id':emp_ids[0],'employee_name':get_fullname(emp_ids[0]),'emp_designation':emp_desigs[0]},{'employee_id':emp_ids[1],'employee_name':get_fullname(emp_ids[1]),'emp_designation':emp_desigs[1]},{'employee_id':emp_ids[2],'employee_name':get_fullname(emp_ids[2]),'emp_designation':emp_desigs[2]},{'employee_id':emp_ids[3],'employee_name':get_fullname(emp_ids[3]),'emp_designation':emp_desigs[3]}]
					# pass
				elif form['imp_type']=='team_imp':
					team_imp_desc=form['team_imp_desc']
					implementation_type='team'
					imp_details=[{'Implementation_desc':team_imp_desc},{},{},{}]
					# pass
				final_details={'plant_id':user_palnt,'kaizen_date':kaizen_date,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details}
				dept_name=find_one_in_collection('departmentmaster',{'dept_pid':user_details['department_id']})
				dept_details=find_one_in_collection('departmentmaster',{'dept_pid':user_details['department_id']})
				saved_details=find_one_in_collection('Kaizen_details',{'username':user_details['username'],'kaizen_date':kaizen_date})
				if saved_details:
					update_coll('Kaizen_details',{'username':user_details['username'],'status':'Saved'},{'$set':{'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'status':'Saved','saved_time':datetime.datetime.now()}})
					flash('Kaizen Details Are Saved Sucessfully.','alert-success')
					kaizen_dir_modified=saved_details['kaizen_id']
					kaizen_dir=''				
					for i in range(len(kaizen_dir_modified.split('/'))):
						if i== len(kaizen_dir_modified.split('/'))-1:
							kaizen_dir+=kaizen_dir_modified.split('/')[i]
						else:
							kaizen_dir+=kaizen_dir_modified.split('/')[i]+'-'
					print kaizen_dir
				else:
					dept_code=dept_name['dep_code']
					dept_name=dept_name['req_desc']


					sl_no=find_in_collection_sort('Kaizen_details',{'plant_id':user_details['plant_id'],'department':dept_name.upper(),'year':str(datetime.datetime.now().year)},[("_id",-1)])
					# print sl_no,' serial details'
					if sl_no:
						sl_no=sl_no[0]['serial_no']+1
						kaizen_id='PCIL/'+user_details['plant_id']+'/'+dept_code.upper()+'/'+str(datetime.datetime.now().year)+'/'+str(sl_no)
						kaizen_dir='PCIL-'+user_details['plant_id']+'-'+dept_code.upper()+'-'+str(datetime.datetime.now().year)+'-'+str(sl_no)
					else:
						sl_no=1
						kaizen_id='PCIL/'+user_details['plant_id']+'/'+dept_code.upper()+'/'+str(datetime.datetime.now().year)+'/'+str(sl_no)
						kaizen_dir='PCIL-'+user_details['plant_id']+'-'+dept_code.upper()+'-'+str(datetime.datetime.now().year)+'-'+str(sl_no)
					if form['submit']=='Save':
						save_collection('Kaizen_details',{'kaizen_path':kaizen_dir,'department':dept_name.upper(),'department_id':user_details['department_id'],'kaizen_id':kaizen_id,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'year':str(datetime.datetime.now().year),'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'saved_time':datetime.datetime.now(),'status':'Saved','serial_no':sl_no})
						flash('Kaizen Saved Sucessfully.','alert-success')
					elif form['submit']=='Submit':
						approval_levels={'a_id':get_plant_head(user_details['plant_id']),'a_status':'current'}
						save_collection('Kaizen_details',{'kaizen_path':kaizen_dir,'department':dept_name.upper(),'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'department_id':user_details['department_id'],'dept_head':dept_details['dept_chief_pid'],'dept_head_name':get_fullname(dept_details['dept_chief_pid']),'kaizen_id':kaizen_id,'year':str(datetime.datetime.now().year),'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'submit_time':datetime.datetime.now(),'approval_levels':approval_levels,'status':'Submitted','serial_no':sl_no})
						flash('Kaizen Saved Sucessfully.','alert-success')
				directories=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/')
				if kaizen_dir not in directories:
					os.mkdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
				files=request.files
				imp_users_pic=request.files.getlist('implementedby_img')
				try:
					for img in range(len(imp_users_pic)):
						filename = secure_filename(imp_users_pic[img].filename)
						if form['imp_type']=='ind_imp':
							print filename,' filename'
							imp_users_pic[img].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+filename)
							print 'filesaved'
							if filename !='':
								filename_path='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
								os.rename(filename_path+'/'+filename,filename_path+'/'+emp_ids[img]+'.'+filename.split('.')[1])
						pass
				except:
					pass
				try:
					filename_path='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
					imp_pics=request.files.getlist('imp_pics')
					before_pic=secure_filename(imp_pics[0].filename)
					before_imp=imp_pics[0].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+before_pic)
					if before_imp!='':
						os.rename(filename_path+'/'+before_pic,filename_path+'/'+'before_imp'+'.'+before_pic.split('.')[1])
				except:
					pass
				try:
					after_pic = secure_filename(imp_pics[1].filename)
					after_imp=imp_pics[1].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+after_pic)
					os.rename(filename_path+'/'+after_pic,filename_path+'/'+'after_imp'+'.'+after_pic.split('.')[1])
				except:
					pass

				# flash('Kaizen Details Are Saved Sucessfully.','alert-success')
				return render_template('Kaizen_Entryform.html',user_details=user_details)
			else:
				flash('The User Has Already Submitted A Kaizen On The Selected Date.Please Select Another Date And Proceed.','alert-danger')
				return render_template('Kaizen_Entryform.html',user_details=user_details)
		else:
			pass
	return render_template('Kaizen_Entryform.html',user_details=user_details)

# def get_plant_head(plant_id):
# 	p_head_details=find_one_in_collection('Plants',{'plant_id':plant_id})
# 	p_head_details=find_one_in_collection('Userinfo',{'designation_id':p_head_details['chief_pid']})
# 	result=p_head_details['username']
# 	return result

# moved to prod from dev on 12th october 2017
def get_plant_head(plant_id,dept_pid):
	if plant_id!='HY':
		p_head_details=find_one_in_collection('Plants',{'plant_id':plant_id})
		p_head_details=find_one_in_collection('Userinfo',{'designation_id':p_head_details['chief_pid']})
		result=p_head_details['username']
	elif plant_id=='HY':
		p_head_details=find_one_in_collection('departmentmaster',{'plant':'HY','dept_pid':dept_pid})
		print p_head_details,'head details------------'
		p_head_details=find_one_in_collection('Userinfo',{'designation_id':p_head_details['dept_chief_pid']})
		result=p_head_details['username']
	return result

def get_fullname(p_id):
	fullname=find_one_in_collection('Userinfo',{'$or':[{'designation_id':p_id},{'username':p_id}]})
	try:
		result=fullname['f_name']+' '+fullname['l_name']
		result=result[:30]
	except:
		result=""
	return result

# @zen_reports.route('/Kaizen_History/', methods = ['GET', 'POST'])
# @login_required
# def Kaizen_History():
# 	user_details = base()
# 	# my_kaizens=find_in_collection('Kaizen_details',{'username':user_details['username']})
# 	my_kaizens=find_in_collection_sort('Kaizen_details',{'username':user_details['username']},[("kaizen_isodate",-1)])
# 	if request.method=='POST':
# 		form=request.form
# 		print form,'kaizen historyyyyyyyyyyyyyyyyyyyyy'
# 		if form['submit']=='Edit':
# 			print 'into viewwwwwwwwwwwwwwwwwwwwwwww'
# 			kaizen_details=find_one_in_collection('Kaizen_details',{'_id':ObjectId(form['prid'])})
# 			kaizen_dir_modified=kaizen_details['kaizen_id']
# 			kaizen_dir_path=''				
# 			for i in range(len(kaizen_dir_modified.split('/'))):
# 				if i== len(kaizen_dir_modified.split('/'))-1:
# 					kaizen_dir_path+=kaizen_dir_modified.split('/')[i]
# 				else:
# 					kaizen_dir_path+=kaizen_dir_modified.split('/')[i]+'-'
# 			# print kaizen_dir_path,' ---- @Kaizen_History-kaizen_dir_path 1267'
# 			files_saved=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir_path)
# 			image_files=[]
# 			emp_images=[]
# 			before_imp=''
# 			after_imp=''
# 			kaizen_files={}
# 			for iii in files_saved:
# 				image_files.append('/static/Kaizens/'+kaizen_dir_path+'/'+iii)
# 				emp_images.append(iii.split('.')[0])
# 				kaizen_files.update({iii.split('.')[0]:iii.split('.')[1]})
# 			ran_num=random.randint(1,10000000000000000000000000000001)
# 			# print kaizen_files,' ---- @Kaizen_History-kaizenfiles @1280'
# 			return render_template('Kaizen_Entryform_edit.html',user_details=user_details,kaizen_details=kaizen_details,kaizen_dir_path=kaizen_dir_path,kaizen_files=kaizen_files,image_files=image_files,emp_images=emp_images,ran_num=ran_num)
# 		if form['submit']=='Save' or form['submit']=='Submit':
# 			print form,'----form 1233'
# 			user_palnt=form['plant']
# 			kaizen_date=form['kaizen_date']
# 			print kaizen_date,' kaiden date'
# 			prob_identified=form['prob_identified']
# 			countermeasure=form['Countermeasure']
# 			try:
# 				implementation_cost=int(form['imp_cost'])
# 			except:
# 				implementation_cost=0
# 			result=form['result']
# 			try:
# 				tangible_cost=int(form['tangible_amnt'])
# 			except:
# 				tangible_cost=0
# 			# implementation_cost=int(form['imp_cost'])
# 			# result=form['result']
# 			# tangible_cost=int(form['tangible_amnt'])
# 			kaizen_dir_modified=form['kaizen_id']
# 			before_after_status=form.getlist('before_after_status')
# 			kaizen_dir=''				
# 			for i in range(len(kaizen_dir_modified.split('/'))):
# 				if i== len(kaizen_dir_modified.split('/'))-1:
# 					kaizen_dir+=kaizen_dir_modified.split('/')[i]
# 				else:
# 					kaizen_dir+=kaizen_dir_modified.split('/')[i]+'-'
# 			print kaizen_dir,' ---- 1 @Kaizen_History-save/submit-kaizen_dir 1314'
# 			if 'InTangible_option' in form and form['InTangible_option']=='checked':
# 				Intangible_sel_option=form['InTangible_sel_option']
# 				Intangible_desc=form['InTangible_desc']
# 				Intangible='checked'
# 				InTangible_details={'Intangible_sel_option':Intangible_sel_option,'Intangible_desc':Intangible_desc}
# 			else:
# 				Intangible_sel_option=""
# 				Intangible_desc=""
# 				Intangible='unchecked'
# 				InTangible_details={}
# 			if form['imp_type']=='ind_imp':
# 				new_emp_ids=form.getlist('emp_id_new')
# 				new_emp_name=form.getlist('emp_name_new')
# 				emp_changed=form.getlist('emp_id_changed')
# 				emp_ids=form.getlist('emp_id')
# 				emp_desigs=form.getlist('desig')
# 				implementation_type='individual'
# 				imp_details=[]
# 				emp_pic_rename=[]
# 				existing_file_details={}
# 				print new_emp_ids,' ---- 2 @kaizenDetails-New-Employee-ids'
# 				print emp_ids,' ---- 3 @kaizenDetails-old-Employee_ids'
# 				# retriving all the files already available in the kaizen dir
# 				files_existing=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
# 				for ij in files_existing:
# 					existing_file_details.update({ij.split('.')[0]:ij.split('.')[1]})
# 				for i in range(len(emp_ids)):
# 					if emp_changed[i]=="1":
# 						try:
# 							# print '/home/administrator/Aswinikumar/PCIL_myPortal_QA/zenapp/static/Kaizens/'+kaizen_dir+'/'+new_emp_ids[i]+'.'+existing_file_details[new_emp_ids[i]]
# 							os.remove('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+new_emp_ids[i]+'.'+existing_file_details[new_emp_ids[i]])
# 							# print '@Kaizen_History-'+new_emp_ids[i]+'.'+existing_file_details[new_emp_ids[i]]+'File Has Been Overwritten'
# 						except:
# 							# print '@Kaizen_History-no previous file to Overwrite.'
# 							imp_details.append({'employee_id':emp_ids[i],'employee_name':get_fullname(emp_ids[i]),'emp_designation':emp_desigs[i]})
# 							emp_pic_rename.append(emp_ids[i])
# 					else:
# 						imp_details.append({'employee_id':new_emp_ids[i],'employee_name':new_emp_name[i],'emp_designation':emp_desigs[i]})
# 						emp_pic_rename.append(new_emp_ids[i])
# 				# imp_details=[,{'employee_id':emp_ids[1],'employee_name':get_fullname(emp_ids[1]),'emp_designation':emp_desigs[1]},{'employee_id':emp_ids[2],'employee_name':get_fullname(emp_ids[2]),'emp_designation':emp_desigs[2]},{'employee_id':emp_ids[3],'employee_name':get_fullname(emp_ids[3]),'emp_designation':emp_desigs[3]}]
# 			elif form['imp_type']=='team_imp':
# 				team_imp_desc=form['team_imp_desc']
# 				implementation_type='team'
# 				imp_details=[{'Implementation_desc':team_imp_desc},{},{},{}]
# 			# final_details={'plant_id':user_palnt,'kaizen_isodate':datetime.datetime.now().date(),'kaizen_date':kaizen_date,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details}
# 			dept_name=find_one_in_collection('departmentmaster',{'dept_pid':user_details['department_id']})
# 			saved_details=find_one_in_collection('Kaizen_details',{'username':user_details['username'],'kaizen_date':kaizen_date,'$or':[{'status':'Saved'},{'status':'Re-Submit'}]})
			
# 			if saved_details:
# 				kaizen_dir_modified=saved_details['kaizen_id']
# 				kaizen_dir=''				
# 				for i in range(len(kaizen_dir_modified.split('/'))):
# 					if i== len(kaizen_dir_modified.split('/'))-1:
# 						kaizen_dir+=kaizen_dir_modified.split('/')[i]
# 					else:
# 						kaizen_dir+=kaizen_dir_modified.split('/')[i]+'-'
# 				print kaizen_dir,' ---- 4 1372 @Kaizen_History-directory to be saved for the saving the new updating pics.'
# 				print form['submit'],'dubmit valuee-------------'
# 				if form['submit']=='Save':
# 					flash('Kaizen Saved Sucessfully.','alert-success')
# 					print '5 --- ',{'kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'saved_time':datetime.datetime.now()},' kaizen save details'
# 					update_coll('Kaizen_details',{'_id':ObjectId(form['doc_id'])},{'$set':{'status':'Saved','kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'saved_time':datetime.datetime.now()}})
# 					# print 'kaizen saved and its path is ',kaizen_dir
# 					print 'document is saved'
# 				elif form['submit']=='Submit':
# 					approval_levels={'a_id':get_plant_head(user_details['plant_id']),'a_status':'current'}
# 					print {'kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'dept_head':dept_name['dept_chief_pid'],'dept_head_name':get_fullname(dept_name['dept_chief_pid']),'approval_levels':approval_levels,'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'status':'Submitted','submit_time':datetime.datetime.now()},' kaizen Submit details'
# 					update_coll('Kaizen_details',{'_id':ObjectId(form['doc_id'])},{'$set':{'status':'Submitted','kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'dept_head':dept_name['dept_chief_pid'],'dept_head_name':get_fullname(dept_name['dept_chief_pid']),'approval_levels':approval_levels,'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'status':'Submitted','submit_time':datetime.datetime.now()}})
# 					flash('Kaizen Submitted Sucessfully.','alert-success')
# 			else:
# 				print '@Kaizen_History-No Previous Docs To Save.'
# 				pass
# 			directories=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/')
# 			if kaizen_dir not in directories:
# 				os.mkdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
# 			files=request.files
# 			imp_users_pic=request.files.getlist('implementedby_img')

# 			existing_file_details_2={}
# 			files_existing_2=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
# 			filepath_before_after='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
# 			for iji in files_existing_2:
# 				existing_file_details_2.update({iji.split('.')[0]:iji.split('.')[1]})
# 			if before_after_status[0]=='1' and 'before_imp' in existing_file_details_2.keys():
# 				os.remove('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/before_imp.'+existing_file_details_2['before_imp'])
# 			if before_after_status[1]=='1' and 'after_imp' in existing_file_details_2.keys():
# 				os.remove('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/after_imp.'+existing_file_details_2['after_imp'])
# 			# try:
# 			# For saving the new updatedimages into the server.
# 			for img in range(len(imp_users_pic)):
# 				filename = secure_filename(imp_users_pic[img].filename)
# 				if form['imp_type']=='ind_imp':
# 					if filename!='':
# 						imp_users_pic[img].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+filename)
# 						filename_path='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
# 						# print filename_path,' file path', filename,'  -- filename'
# 						os.rename(filename_path+'/'+filename,filename_path+'/'+emp_pic_rename[img]+'.'+filename.split('.')[1])
# 					else:
# 						print '@Kaizen_History-File Name Is Empty.'
# 			# except:
# 			# 	pass
# 			# Saving Before & After Implementation images.
# 			try:
# 				filename_path='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
# 				imp_pics=request.files.getlist('imp_pics')
# 				before_pic=secure_filename(imp_pics[0].filename)
# 				before_imp=imp_pics[0].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+before_pic)
# 				if before_imp!='':
# 					os.rename(filename_path+'/'+before_pic,filename_path+'/'+'before_imp'+'.'+before_pic.split('.')[1])
# 			except:
# 				print '@Kaizen_History-Before Implementation Image is Not Available.'
# 				pass
# 			try:
# 				after_pic = secure_filename(imp_pics[1].filename)
# 				after_imp=imp_pics[1].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+after_pic)
# 				os.rename(filename_path+'/'+after_pic,filename_path+'/'+'after_imp'+'.'+after_pic.split('.')[1])
# 			except:
# 				print '@Kaizen_History-After Implementation Image is Not Available.'
# 				pass

# 			files_saved=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
# 			image_files=[]
# 			emp_images=[]
# 			before_imp=''
# 			after_imp=''
# 			display_imgs=['','','','','','']
# 			for iii in files_saved:
# 				image_files.append('/static/Kaizens/'+kaizen_dir+'/'+iii)
# 				emp_images.append(iii.split('.')[0])
# 				# if iii.split('.')[0]=='before_imp':
# 				# 	# print '-----------------------'
# 				# 	display_imgs.
# 				# elif iii.split('.')[0]=='after_imp':
# 				# 	# print '#######################'
# 				# 	after_imp='yes'

# 			# print image_files,' final image files'
# 			print emp_images,' @Kaizen_History-uploaded images list from server'
# 			# print before_imp,' @Kaizen_History-',after_imp
# 			my_kaizens=find_in_collection_sort('Kaizen_details',{'username':user_details['username']},[("kaizen_isodate",-1)])
# 			# return render_template('Kaizen_history_view.html',user_details=user_details,my_kaizens=my_kaizens,image_files=image_files,emp_images=emp_images,before_imp=before_imp,after_imp=after_imp)
# 		return render_template('Kaizen_history_view.html',user_details=user_details,my_kaizens=my_kaizens)
# 	return render_template('Kaizen_history_view.html',user_details=user_details,my_kaizens=my_kaizens)

# moved from development on 11 oct 2017
@zen_reports.route('/Kaizen_History/', methods = ['GET', 'POST'])
@login_required
def Kaizen_History():
	user_details = base()
	# my_kaizens=find_in_collection('Kaizen_details',{'username':user_details['username']})
	my_kaizens=find_in_collection_sort('Kaizen_details',{'username':user_details['username']},[("kaizen_isodate",-1)])
	if request.method=='POST':
		form=request.form
		print form,'kaizen historyyyyyyyyyyyyyyyyyyyyy'
		if form['submit']=='Edit':
			print 'into viewwwwwwwwwwwwwwwwwwwwwwww'
			kaizen_details=find_one_in_collection('Kaizen_details',{'_id':ObjectId(form['prid'])})
			kaizen_dir_modified=kaizen_details['kaizen_id']
			kaizen_dir_path=''				
			for i in range(len(kaizen_dir_modified.split('/'))):
				if i== len(kaizen_dir_modified.split('/'))-1:
					kaizen_dir_path+=kaizen_dir_modified.split('/')[i]
				else:
					kaizen_dir_path+=kaizen_dir_modified.split('/')[i]+'-'
			# print kaizen_dir_path,' ---- @Kaizen_History-kaizen_dir_path 1267'
			files_saved=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir_path)
			image_files=[]
			emp_images=[]
			before_imp=''
			after_imp=''
			kaizen_files={}
			for iii in files_saved:
				image_files.append('/static/Kaizens/'+kaizen_dir_path+'/'+iii)
				emp_images.append(iii.split('.')[0])
				kaizen_files.update({iii.split('.')[0]:iii.split('.')[1]})
			ran_num=random.randint(1,10000000000000000000000000000001)
			# print kaizen_files,' ---- @Kaizen_History-kaizenfiles @1280'
			return render_template('Kaizen_Entryform_edit.html',user_details=user_details,kaizen_details=kaizen_details,kaizen_dir_path=kaizen_dir_path,kaizen_files=kaizen_files,image_files=image_files,emp_images=emp_images,ran_num=ran_num)
		if form['submit']=='Save' or form['submit']=='Submit':
			print form,'----form 1233'
			user_palnt=form['plant']
			kaizen_date=form['kaizen_date']
			print kaizen_date,' kaiden date'
			prob_identified=form['prob_identified']
			countermeasure=form['Countermeasure']
			try:
				implementation_cost=int(form['imp_cost'])
			except:
				implementation_cost=0
			result=form['result']
			try:
				tangible_cost=int(form['tangible_amnt'])
			except:
				tangible_cost=0
			# implementation_cost=int(form['imp_cost'])
			# result=form['result']
			# tangible_cost=int(form['tangible_amnt'])
			kaizen_dir_modified=form['kaizen_id']
			before_after_status=form.getlist('before_after_status')
			kaizen_dir=''				
			for i in range(len(kaizen_dir_modified.split('/'))):
				if i== len(kaizen_dir_modified.split('/'))-1:
					kaizen_dir+=kaizen_dir_modified.split('/')[i]
				else:
					kaizen_dir+=kaizen_dir_modified.split('/')[i]+'-'
			print kaizen_dir,' ---- 1 @Kaizen_History-save/submit-kaizen_dir 1314'
			if 'InTangible_option' in form and form['InTangible_option']=='checked':
				Intangible_sel_option=form['InTangible_sel_option']
				Intangible_desc=form['InTangible_desc']
				Intangible='checked'
				InTangible_details={'Intangible_sel_option':Intangible_sel_option,'Intangible_desc':Intangible_desc}
			else:
				Intangible_sel_option=""
				Intangible_desc=""
				Intangible='unchecked'
				InTangible_details={}
			if form['imp_type']=='ind_imp':
				new_emp_ids=form.getlist('emp_id_new')
				new_emp_name=form.getlist('emp_name_new')
				emp_changed=form.getlist('emp_id_changed')
				emp_ids=form.getlist('emp_id')
				emp_desigs=form.getlist('desig')
				implementation_type='individual'
				imp_details=[]
				emp_pic_rename=[]
				existing_file_details={}
				print new_emp_ids,' ---- 2 @kaizenDetails-New-Employee-ids'
				print emp_ids,' ---- 3 @kaizenDetails-old-Employee_ids'
				# retriving all the files already available in the kaizen dir
				files_existing=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
				for ij in files_existing:
					existing_file_details.update({ij.split('.')[0]:ij.split('.')[1]})
				for i in range(len(emp_ids)):
					if emp_changed[i]=="1":
						try:
							print '/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+new_emp_ids[i]+'.'+existing_file_details[new_emp_ids[i]]
							os.remove('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+new_emp_ids[i]+'.'+existing_file_details[new_emp_ids[i]])
							print '@Kaizen_History-'+new_emp_ids[i]+'.'+existing_file_details[new_emp_ids[i]]+'File Has Been Overwritten'
						except:
							print '@Kaizen_History-no previous file to Overwrite.'
						imp_details.append({'employee_id':emp_ids[i],'employee_name':get_fullname(emp_ids[i]),'emp_designation':emp_desigs[i]})
						emp_pic_rename.append(emp_ids[i])
					else:
						imp_details.append({'employee_id':new_emp_ids[i],'employee_name':new_emp_name[i],'emp_designation':emp_desigs[i]})
						emp_pic_rename.append(new_emp_ids[i])
				# imp_details=[,{'employee_id':emp_ids[1],'employee_name':get_fullname(emp_ids[1]),'emp_designation':emp_desigs[1]},{'employee_id':emp_ids[2],'employee_name':get_fullname(emp_ids[2]),'emp_designation':emp_desigs[2]},{'employee_id':emp_ids[3],'employee_name':get_fullname(emp_ids[3]),'emp_designation':emp_desigs[3]}]
			elif form['imp_type']=='team_imp':
				team_imp_desc=form['team_imp_desc']
				implementation_type='team'
				imp_details=[{'Implementation_desc':team_imp_desc},{},{},{}]
			# final_details={'plant_id':user_palnt,'kaizen_isodate':datetime.datetime.now().date(),'kaizen_date':kaizen_date,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details}
			dept_name=find_one_in_collection('departmentmaster',{'dept_pid':user_details['department_id']})
			saved_details=find_one_in_collection('Kaizen_details',{'username':user_details['username'],'kaizen_date':kaizen_date,'$or':[{'status':'Saved'},{'status':'Re-Submit'}]})
			
			if saved_details:
				kaizen_dir_modified=saved_details['kaizen_id']
				kaizen_dir=''				
				for i in range(len(kaizen_dir_modified.split('/'))):
					if i== len(kaizen_dir_modified.split('/'))-1:
						kaizen_dir+=kaizen_dir_modified.split('/')[i]
					else:
						kaizen_dir+=kaizen_dir_modified.split('/')[i]+'-'
				print kaizen_dir,' ---- 4 1372 @Kaizen_History-directory to be saved for the saving the new updating pics.'
				print form['submit'],'dubmit valuee-------------'
				if form['submit']=='Save':
					flash('Kaizen Saved Sucessfully.','alert-success')
					print '5 --- ',{'kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'saved_time':datetime.datetime.now()},' kaizen save details'
					update_coll('Kaizen_details',{'_id':ObjectId(form['doc_id'])},{'$set':{'status':'Saved','kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'saved_time':datetime.datetime.now()}})
					# print 'kaizen saved and its path is ',kaizen_dir
					print 'document is saved'
				elif form['submit']=='Submit':
					approval_levels={'a_id':get_plant_head(user_details['plant_id'],user_details['department_id']),'a_status':'current'}
					print {'kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'dept_head':dept_name['dept_chief_pid'],'dept_head_name':get_fullname(dept_name['dept_chief_pid']),'approval_levels':approval_levels,'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'status':'Submitted','submit_time':datetime.datetime.now()},' kaizen Submit details'
					update_coll('Kaizen_details',{'_id':ObjectId(form['doc_id'])},{'$set':{'status':'Submitted','kaizen_path':kaizen_dir,'kaizen_isodate':datetime.datetime.strptime(kaizen_date,'%d-%m-%Y'),'dept_head':dept_name['dept_chief_pid'],'dept_head_name':get_fullname(dept_name['dept_chief_pid']),'approval_levels':approval_levels,'username':user_details['username'],'plant_id':user_palnt,'kaizen_date':kaizen_date,'problem_identified':prob_identified,'countermeasure':countermeasure,'implementation_cost':implementation_cost,'result':result,'tangible_cost':tangible_cost,'Intangible_status':Intangible,'implementation_type':implementation_type,'InTangible_details':InTangible_details,'Implementation_details':imp_details,'status':'Submitted','submit_time':datetime.datetime.now()}})
					flash('Kaizen Submitted Sucessfully.','alert-success')
			else:
				print '@Kaizen_History-No Previous Docs To Save.'
				pass
			directories=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/')
			if kaizen_dir not in directories:
				os.mkdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
			files=request.files
			imp_users_pic=request.files.getlist('implementedby_img')

			existing_file_details_2={}
			files_existing_2=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
			filepath_before_after='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
			for iji in files_existing_2:
				existing_file_details_2.update({iji.split('.')[0]:iji.split('.')[1]})
			if before_after_status[0]=='1' and 'before_imp' in existing_file_details_2.keys():
				os.remove('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/before_imp.'+existing_file_details_2['before_imp'])
			if before_after_status[1]=='1' and 'after_imp' in existing_file_details_2.keys():
				os.remove('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/after_imp.'+existing_file_details_2['after_imp'])
			# try:
			# For saving the new updatedimages into the server.
			for img in range(len(imp_users_pic)):
				filename = secure_filename(imp_users_pic[img].filename)
				if form['imp_type']=='ind_imp':
					if filename!='':
						imp_users_pic[img].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+filename)
						filename_path='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
						# print filename_path,' file path', filename,'  -- filename'
						os.rename(filename_path+'/'+filename,filename_path+'/'+emp_pic_rename[img]+'.'+filename.split('.')[1])
					else:
						print '@Kaizen_History-File Name Is Empty.'
			# except:
			# 	pass
			# Saving Before & After Implementation images.
			try:
				filename_path='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir
				imp_pics=request.files.getlist('imp_pics')
				before_pic=secure_filename(imp_pics[0].filename)
				before_imp=imp_pics[0].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+before_pic)
				if before_imp!='':
					os.rename(filename_path+'/'+before_pic,filename_path+'/'+'before_imp'+'.'+before_pic.split('.')[1])
			except:
				print '@Kaizen_History-Before Implementation Image is Not Available.'
				pass
			try:
				after_pic = secure_filename(imp_pics[1].filename)
				after_imp=imp_pics[1].save('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+after_pic)
				os.rename(filename_path+'/'+after_pic,filename_path+'/'+'after_imp'+'.'+after_pic.split('.')[1])
			except:
				print '@Kaizen_History-After Implementation Image is Not Available.'
				pass

			files_saved=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
			image_files=[]
			emp_images=[]
			before_imp=''
			after_imp=''
			display_imgs=['','','','','','']
			for iii in files_saved:
				image_files.append('/static/Kaizens/'+kaizen_dir+'/'+iii)
				emp_images.append(iii.split('.')[0])
				# if iii.split('.')[0]=='before_imp':
				# 	# print '-----------------------'
				# 	display_imgs.
				# elif iii.split('.')[0]=='after_imp':
				# 	# print '#######################'
				# 	after_imp='yes'

			# print image_files,' final image files'
			print emp_images,' @Kaizen_History-uploaded images list from server'
			# print before_imp,' @Kaizen_History-',after_imp
			my_kaizens=find_in_collection_sort('Kaizen_details',{'username':user_details['username']},[("kaizen_isodate",-1)])
			# return render_template('Kaizen_history_view.html',user_details=user_details,my_kaizens=my_kaizens,image_files=image_files,emp_images=emp_images,before_imp=before_imp,after_imp=after_imp)
		return render_template('Kaizen_history_view.html',user_details=user_details,my_kaizens=my_kaizens)
	return render_template('Kaizen_history_view.html',user_details=user_details,my_kaizens=my_kaizens)



# end of kaizen history from development for testing



from fpdf import FPDF

def mySample(f):
	if len(f)<=300:
		sample=300-len(f)
		for i in range(sample):
			f=f+' '
		return str(f)
	elif len(f)==300:
		return str(f)

def textSample(f):
	if len(f)<=300:
		sample=300-len(f)
		for j in range(sample):
			f=f+' '
			# print f,' addedresult'
		return str(f)
	elif len(f)==300:
		return str(f)

def textSample2(f):
	if len(f)<=300:
		sample=300-len(f)
		for jj in range(sample):
			f=f+' '
			# print f,' addedresult'
		return str(f)
	elif len(f)==300:
		return str(f)

def pdf_generation(logo_img,format_no,report_date,kaizen_id,plant_id,userdetails,
	department,problemIdentified,countermeasure,result,tangible_benifit,intangible_benifit,intangible_benifit_desc,total_imp_cost,total_savings,img_before,img_after,HOD_name,PH_name,kaizen_dir,team_imp_desc,imp_type):
	# print 'result in pdf_generation',result
	pdf=FPDF()
	effective_page_width = pdf.w - 2*pdf.l_margin
	pdf.add_page()
	pdf.set_font("Arial","B",14)
	# pdf.set_top_margin(margin=float)
	# pdf.set_left_margin(margin=float)
	pdf.set_xy(10,8)
	pdf.cell(190,19,'',1,0,'C')
	pdf.set_xy(73,14)
	pdf.cell(0,0,'Penna Cement Industries Limited',0)
	pdf.set_xy(60,7)
	pdf.image(logo_img,60,7,13,12)
	# pdf.set_xy(65,6)
	# pdf.cell(10,5,'Penna Cement Industries Limited',0)
    
	pdf.set_font('','B',9)
	pdf.set_xy(80,20)
	pdf.cell(0,0,'KAIZEN-PROCESS IMPROVEMENT',0)

	pdf.set_xy(12,23)
	pdf.set_font('','',9)
	pdf.cell(7,4,'Format no:'+format_no,0)

	pdf.set_xy(163,23)
	pdf.cell(7,4,'Date:'+report_date,0)

	if imp_type=='individual':
		pdf.set_xy(12,36)
		pdf.cell(7,4,'Kaizen Tracking Number',0)
		pdf.set_xy(50,36)
		pdf.set_font('','',6)
		pdf.cell(43,4,kaizen_id,1)

		pdf.set_xy(95,30)
		pdf.set_font('','',9)
		pdf.cell(105,5,'Implemented By',0,'0','C')

		pdf.set_xy(12,43)
		pdf.set_font("","",9)
		pdf.cell(10,4,'Plant')
		pdf.set_font("","",6)
		pdf.set_xy(50,43)
		pdf.cell(43,4,plant_id,1)

		if 'user4' in userdetails:
			pdf.set_font('','',6)
			pdf.set_xy(95,36)
			pdf.multi_cell(24,3,userdetails['user4']['username'],0,'C')
			pdf.set_xy(95,36)
			pdf.multi_cell(26,6,'',1,'C',False)

		else:
			pdf.set_xy(95,36)
			pdf.cell(26,6,'',0,'0','C')

		if 'user3' in userdetails:
			pdf.set_font('','',6)
			pdf.set_xy(121,36)
			pdf.multi_cell(24,3,userdetails['user3']['username'],0,'C')
			pdf.set_xy(121,36)
			pdf.multi_cell(26,6,'',1,'C',False)

		else:
			pdf.set_xy(121,36)
			pdf.cell(26,6,'',0,'0','C')

		if 'user2' in userdetails:
			pdf.set_font('','',6)
			pdf.set_xy(147,36)
			pdf.multi_cell(24,3,userdetails['user2']['username'],0,'C')
			pdf.set_xy(147,36)
			pdf.multi_cell(26,6,'',1,'C',False)

		else:
			pdf.set_xy(147,36)
			pdf.cell(26,6,'',0,'0','C')

		if 'user1' in userdetails:
			pdf.set_font('','',6)
			pdf.set_xy(173,36)
			pdf.multi_cell(24,3,userdetails['user1']['username'],0,'C')
			pdf.set_xy(173,36)
			pdf.multi_cell(27,6,'',1,'C',False)

		else:
			pdf.set_xy(173,36)
			pdf.cell(26,6,'',0,'0','C')

		pdf.set_xy(12,50)
		pdf.set_font('','',9)
		pdf.cell(10,5,'Department')
		pdf.set_xy(50,50)
		pdf.set_font('','',6)
		pdf.set_xy(50,51)
		pdf.multi_cell(54,3,department,0)
		pdf.set_xy(50,50)
		pdf.multi_cell(43,4,'',1,'C',False)

		if 'user4' in userdetails:
			pdf.set_xy(95,42)
			pdf.cell(26,28,'',1)
			try:
				pdf.image(userdetails['user4']['image_src'],95,42,26,28)
			except:
				pdf.set_xy(95,42)
				pdf.cell(26,28,'No Image Available',1,'0','C')
		else:
			pdf.set_xy(95,42)
			pdf.set_font('','',6)
			pdf.cell(26,28,'',0,'0','C')
			# pdf.image('',95,46,26,28)

		if 'user3' in userdetails:
			pdf.set_xy(121,42)
			pdf.cell(26,28,'',1,'0','C')
			try:
				pdf.image(userdetails['user3']['image_src'],121,42,26,28)
			except:
				pdf.set_xy(121,42)
				pdf.cell(26,28,'No Image Available',1,'0','C')
		else:
			pdf.set_xy(121,42)
			pdf.set_font('','',6)
			pdf.cell(26,28,'',0,'0','C')
			# pdf.image('',95,46,26,28)

		if 'user2' in userdetails:
			pdf.set_xy(147,42)
			pdf.cell(26,28,'',1,'0','C')
			try:
				pdf.image(userdetails['user2']['image_src'],147,42,26,28)
			except:
				pdf.set_xy(147,42)
				pdf.cell(26,28,'No Image Available',1,'0','C')
		else:
			pdf.set_xy(147,42)
			pdf.set_font('','',6)
			pdf.cell(26,28,'',0,'0','C')
			# pdf.image('',95,46,26,28)

		if 'user1' in userdetails:
			pdf.set_xy(173,42)
			pdf.cell(27,28,'',1,'0','C')
			try:
				pdf.image(userdetails['user1']['image_src'],173,42,27,28)
			except:
				pdf.set_xy(173,42)
				pdf.cell(27,28,'No Image Available',1,'0','C')
		else:
			pdf.set_xy(173,42)
			pdf.set_font('','',6)
			pdf.cell(26,28,'',0,'0','C')
			# pdf.image('',95,46,26,28)
	elif imp_type=='team':
		pdf.set_xy(12,36)
		pdf.set_font('','',9)
		pdf.cell(7,4,'Kaizen Tracking Number',0)
		pdf.set_xy(50,36)
		pdf.set_font('','',6)
		pdf.cell(35,4,kaizen_id,1)

		pdf.set_xy(173,36)
		pdf.set_font('','',9)
		pdf.cell(26,5,'Implemented By',0,'0','C')


		pdf.set_xy(12,43)
		pdf.set_font("","",9)
		pdf.cell(10,4,'Plant')
		pdf.set_font("","",6)
		pdf.set_xy(50,43)
		pdf.cell(35,4,plant_id,1)


		pdf.set_font('','',6)
		pdf.set_xy(173,41)
		pdf.multi_cell(24, 3,"TEAM IMPLEMENTATION",0,'C')

		pdf.set_xy(173,41)
		pdf.multi_cell(26,6,'',1,'C',False)

		pdf.set_xy(12,50)
		pdf.set_font('','',9)
		pdf.cell(10,5,'Department')
		pdf.set_xy(50,50)
		pdf.set_font('','',6)
		pdf.cell(35,4,department,1)


		pdf.set_font('','',6)
		pdf.set_xy(173,55)
		pdf.multi_cell(24,3,team_imp_desc,0,'C')
		pdf.set_xy(173,47)
		pdf.multi_cell(26,28,'',1,False)
			

	pdf.set_xy(12,83)
	pdf.set_font('','',9)
	pdf.cell(10,7,'Problem Identified',0)

	problemIdentified=mySample(problemIdentified)
	# print len(problemIdentified)
	pdf.set_font('','',7)
	pdf.set_xy(50,83)
	pdf.multi_cell(effective_page_width/2+2, 4, problemIdentified,1)
	# pdf.multi_cell(76,11,"",1)
	pdf.set_font('','',9)
	pdf.set_xy(153,83)
	pdf.cell(12,5,'Tangible Benefit',0)
	pdf.set_xy(180,83)
	pdf.set_font('','',7)
	pdf.cell(20,4,tangible_benifit,1)
	pdf.set_xy(156,87)
	pdf.set_font('','',8)
	pdf.cell(10,4,'(Amount in Rs.)',0)

	pdf.set_xy(153,93)
	pdf.set_font('','',9)
	pdf.cell(10,5,'Intangible Benefit',0)   # intangible benifit value will be shown.
	pdf.set_xy(180,93)
	pdf.set_font('','',7)
	pdf.cell(20,4,intangible_benifit,1)

	if intangible_benifit_desc!='':
		pdf.set_font('','',7)
		pdf.set_xy(157,101)    #description of intagibleamount
		pdf.multi_cell(40,3,intangible_benifit_desc,0)
		pdf.set_xy(157,100)
		pdf.multi_cell(43,9,'',1,'C',False)
	else:
		pass
	pdf.set_font('','',9)
	pdf.set_xy(10,112)
	pdf.cell(90,5,'Counter Measures',1,0,'C')

	countermeasure=textSample(countermeasure)
	# print len(countermeasure)
	pdf.set_font('','',7)
	pdf.set_xy(10,117)
	effective_page_width = pdf.w - 2*pdf.l_margin
	pdf.multi_cell(effective_page_width/2-5, 5, countermeasure,0)
	pdf.set_xy(10,117)
	pdf.multi_cell(90,25,'',1,'L',False)


	result=textSample2(result)
	pdf.set_font('','',9)
	pdf.set_xy(110,112)
	pdf.cell(90,5,'Results',1,0,'C')
	pdf.set_xy(110,117)
	pdf.set_font('','',7)
	effective_page_width = pdf.w - 2*pdf.l_margin
	# print result,' result'
	pdf.multi_cell(effective_page_width/2-5, 5, result,0)
	pdf.set_xy(110,117)
	pdf.multi_cell(90,25,'',1,'L',False)

	pdf.set_font('','',8)
	pdf.set_xy(57,143)
	pdf.cell(10,4,'Total Imp Cost',0)
	pdf.set_xy(80,143)
	pdf.set_font('','',7)
	pdf.cell(20,4,total_imp_cost,1)

	pdf.set_font('','',8)
	pdf.set_xy(159,143)
	pdf.cell(10,4,'Total Savings',0)
	pdf.set_xy(180,143)
	pdf.set_font('','',7)
	pdf.cell(20,4,total_savings,1)

	# print img_before,' -----------------------------------------'
	pdf.set_font('','',9)
	pdf.set_xy(36,157)
	pdf.cell(10,4,'Before Kaizen Implementation',0)
	pdf.set_xy(10,162)
	pdf.cell(90,45,'',1)
	if img_before!='':
		pdf.image(img_before,10,162,90,45)
	else:
		pdf.set_xy(40,185)
		pdf.set_font('','',8)   #msg shown if before implementation image is not uploaded
		pdf.cell(0,0,'Image not uploaded',0,'C')

	
	pdf.set_font('','',9)
	pdf.set_xy(139,157)
	pdf.cell(10,4,'After Kaizen Implementation',0)
	pdf.set_xy(110,162)
	pdf.cell(90,45,'',1)

	if img_after!='':
		pdf.image(img_after,110,162,90,45)
	else:
		pdf.set_xy(143,185)
		pdf.set_font('','',8) #msg shown if After implementation image is not uploaded
		pdf.cell(0,0,'Image not uploaded',0,'C')


	pdf.set_font('','',9)
	pdf.set_xy(10,216)
	pdf.cell(12,4,'Reviewed by HOD',0)
	pdf.set_xy(50,216)
	pdf.set_font('','',6)
	pdf.cell(45,4,HOD_name,1)

	pdf.set_font('','',9)
	pdf.set_xy(10,227)
	pdf.cell(12,4,'Approved By Plant Head',0)
	pdf.set_xy(50,227)
	pdf.set_font('','',6)
	pdf.cell(45,4,PH_name,1)

	pdf.set_xy(70,272)
	pdf.set_font('','',6)
	pdf.cell(10,4,'* This is computer generated report and don\'t require stamps or signature *')
	pdf.output('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+kaizen_dir+'.pdf','F')
	print "PDF generated successfully in - ",kaizen_dir
	return "sucessfully generated PDF report."



@zen_reports.route('/Kaizen_Approvals/', methods = ['GET', 'POST'])
@login_required
def Kaizen_Approvals():
	user_details = base()
	kaizens_for_approval=find_in_collection('Kaizen_details',{'status':'Submitted','approval_levels.a_id':user_details['username']})
	if request.method=='POST':
		form=request.form
		print form,' in approvals'
		if form['submit']=='VIEW':
			# print form, ' in approvals'
			kaizen_details=find_one_in_collection('Kaizen_details',{'_id':ObjectId(form['prid'])})
			kaizen_dir=''
			for i in range(len(kaizen_details['kaizen_id'].split('/'))):
				if i== len(kaizen_details['kaizen_id'].split('/'))-1:
					kaizen_dir+=kaizen_details['kaizen_id'].split('/')[i]
				else:
					kaizen_dir+=kaizen_details['kaizen_id'].split('/')[i]+'-'

			directories=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/')
			file_details={}
			if kaizen_dir in directories:
				kaizen_files=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
				file_details={}
				for file in kaizen_files:
					file_details.update({file.split('.')[0]:file.split('.')[1]})
				# print kaizen_files,' kaien files'
				# print file_details,' file details'
			return render_template('Kaizen_approval_view.html',user_details=user_details,kaizen_details=kaizen_details,kaizen_files=file_details,kaizen_files_list=file_details.keys(),kaizen_dir=kaizen_dir)
		elif form['submit']=='Approve':
			print form,' checking for verified'
			kaizen_details_pdf=find_one_in_collection('Kaizen_details',{'_id':ObjectId(form['doc_id'])})
			kaizen_dir=''
			userdetails={}
			for i in range(len(kaizen_details_pdf['kaizen_id'].split('/'))):
				if i== len(kaizen_details_pdf['kaizen_id'].split('/'))-1:
					kaizen_dir+=kaizen_details_pdf['kaizen_id'].split('/')[i]
				else:
					kaizen_dir+=kaizen_details_pdf['kaizen_id'].split('/')[i]+'-'
			# userdetails={'user1':{'username':'username 1','image_src':userimage1},'user2':{'username':'username 2','image_src':userimage1},'user3':{'username':'username 3','image_src':userimage1},'user4':{'username':'username 4','image_src':userimage1}}
			# print kaizen_details_pdf['Implementation_details']
			if kaizen_details_pdf['implementation_type']=='individual':
				for ii in range(len(kaizen_details_pdf['Implementation_details'])):
					jj=ii+1
					if kaizen_details_pdf['Implementation_details'][ii]['employee_id'] !='':
						userdetails.update({'user'+str(jj):{'username':kaizen_details_pdf['Implementation_details'][ii]['employee_name'],'image_src':'/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/'+kaizen_details_pdf['Implementation_details'][ii]['employee_id']+'.jpg'}})
			image_list_dir=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
			userimage1='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/images/def_user_pic.jpg'
			logo_img='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/images/logo_pdf.png'
			format_no='PCIL/IMS/MR/19'
			report_date=kaizen_details_pdf['kaizen_date']
			kaizen_id=kaizen_details_pdf['kaizen_id']
			plant_id=kaizen_details_pdf['plant_id']
			plants_desc={'TC':'TALARICHERUVU','BP':'BOYAREDDIPALLI','GC':'GANESHPAHAD CEMENT','TA':'TANDUR','GP':'GANESHPAHAD POWER'}
			plant_id=plants_desc[plant_id]	
			department=kaizen_details_pdf['department']
			try:
				problemIdentified=kaizen_details_pdf['problem_identified']
			except:
				problemIdentified=''
			countermeasure=kaizen_details_pdf['countermeasure']
			result=kaizen_details_pdf['result']
			tangible_benifit=str(kaizen_details_pdf['tangible_cost'])			
			total_imp_cost=str(kaizen_details_pdf['implementation_cost'])
			total_savings=str(kaizen_details_pdf['tangible_cost'])
			if kaizen_details_pdf['Intangible_status']=='checked':
				intangible_benifit_desc=str(kaizen_details_pdf['InTangible_details']['Intangible_desc'])
				intangible_benifit=str(kaizen_details_pdf['InTangible_details']['Intangible_sel_option'])
			else:
				intangible_benifit_desc=''
				intangible_benifit=''

			files_saved=os.listdir('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir)
			images_ext={}
			for iijj in files_saved:
				images_ext.update({iijj.split('.')[0]:iijj.split('.')[1]})

			if 'before_imp' in images_ext.keys():
				img_before='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/before_imp.'+images_ext['before_imp']
			else:
				img_before=''

			if 'after_imp' in images_ext.keys():
				img_after='/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Kaizens/'+kaizen_dir+'/after_imp.'+images_ext['after_imp']
			else:
				img_after=''
			
			HOD_name=kaizen_details_pdf['dept_head_name']
			PH_name=get_fullname(kaizen_details_pdf['approval_levels']['a_id'])
			if kaizen_details_pdf['implementation_type']=='team':
				team_imp_desc=kaizen_details_pdf['Implementation_details'][0]['Implementation_desc']
			else:
				team_imp_desc="Not A Team Implementation."
			imp_type=kaizen_details_pdf['implementation_type']
			# print result,' result @1867'
			pdf_generation(logo_img,format_no,report_date,kaizen_id,plant_id,userdetails,department,problemIdentified,countermeasure,result,tangible_benifit,intangible_benifit,intangible_benifit_desc,total_imp_cost,total_savings,img_before,img_after,HOD_name,PH_name,kaizen_dir,team_imp_desc,imp_type)		
			verified_by_hod=''
			if 'verified_by_hod' in form:
				try:
					verified_by_hod=form['verified_by_hod']
				except:
					verified_by_hod=''
			update_coll('Kaizen_details',{'_id':ObjectId(form['doc_id'])},{'$set':{'approval_levels.a_status':'approved','status':'Approved','approved_time':datetime.datetime.now(),'verified_by_hod':verified_by_hod}})
			flash('Kaizen Is Approved Sucessfully.','alert-success');
			# return render_template('Kaizen_approval_view_list.html',user_details=user_details,kaizen_details=kaizens_for_approval)
			return redirect(url_for('zenreports.Kaizen_Approvals'))
		elif form['submit']=='Reject':
			flash('Kaizen Has been Rejected Sucessfully','alert-success')
			update_coll('Kaizen_details',{'_id':ObjectId(form['doc_id'])},{'$set':{'approval_levels.a_status':'rejected','status':'Re-Submit','rejected_time':datetime.datetime.now(),'reject':'X'}})
			return redirect(url_for('zenreports.Kaizen_Approvals'))
	# return render_template('Kaizen_approval_view.html',user_details=user_details)
	return render_template('Kaizen_approval_view_list.html',user_details=user_details,kaizen_details=kaizens_for_approval)


def excel_overall_report(from_date,to_date,details):
	row=1
	doc_path=find_one_in_collection('portal_settings',{'config':'kaizen_Reports_PATH'})
	plants_desc={'TC':'Talaricheruvu','BP':'Boyareddipalli','GC':'Ganeshpahad Cement','TA':'Tandur','GP':'Ganeshpahad Power'}
	doc_path=doc_path['directory']
	workbook = xlsxwriter.Workbook(doc_path+'overall_summary_report.xlsx')
	worksheet = workbook.add_worksheet()
	style_title_main=workbook.add_format({'bold': True,'font_color':'black','align': 'center','font_size':15})
	style_title_report=workbook.add_format({'bold': True,'font_color':'black','align': 'center','font_size':13})

	style_OD=workbook.add_format({'font_color':'black','bg_color':'#BDBDBD'})
	data_style=workbook.add_format({'border': 1})
	headings_style=workbook.add_format({'border': 1,'bold': True})
	bold = workbook.add_format({'bold': True})
	style_excluded = workbook.add_format({'font_color':'black','border':2,'border_color':'black','bg_color':'#BDBDBD'})
	worksheet.merge_range(row,0,row,11,'Penna Cement Industries Limited',style_title_main)
	row+=1
	worksheet.merge_range(row,0,row,11,'Kaizen Overall Report',style_title_report)
	row+=2
	worksheet.merge_range(row,0,row,11,'From Date : '+from_date)
	row+=1
	worksheet.merge_range(row,0,row,11,'To Date     : '+to_date)
	row+=2
	headings=['Plant','No Of Kaizens','Total Savings']
	for ih in range(len(headings)):
		worksheet.write(row,ih,headings[ih],headings_style)
	row+=1
	# kaizens={'kaizens':['PCIL/TC/STORES/2017/17', 'PCIL/TC/STORES/2017/16', 'PCIL/TC/STORES/2017/9', 'PCIL/TC/STORES/2017/13', 'PCIL/TC/STORES/2017/7', 'PCIL/TC/STORES/2017/8', 'PCIL/TC/STORES/2017/10', 'PCIL/TC/STORES/2017/2', 'PCIL/TC/STORES/2017/18', 'PCIL/TC/STORES/2017/5', 'PCIL/TC/STORES/2017/4', 'PCIL/TC/STORES/2017/11', 'PCIL/TC/STORES/2017/1', 'PCIL/TC/STORES/2017/3', 'PCIL/TC/PROJECTS/DESIGN&OPERATIONS/2017/1', 'PCIL/TC/STORES/2017/15', 'PCIL/TC/STORES/2017/14', 'PCIL/TC/STORES/2017/12', 'PCIL/TC/STORES/2017/6'], '_id': 'TC', 'total_savings': 183138}
	# details=[[{u'kaizens': [u'PCIL/TC/STORES/2017/17', u'PCIL/TC/STORES/2017/16', u'PCIL/TC/STORES/2017/9', u'PCIL/TC/STORES/2017/13', u'PCIL/TC/STORES/2017/7', u'PCIL/TC/STORES/2017/8', u'PCIL/TC/STORES/2017/10', u'PCIL/TC/STORES/2017/2', u'PCIL/TC/STORES/2017/18', u'PCIL/TC/STORES/2017/5', u'PCIL/TC/STORES/2017/4', u'PCIL/TC/STORES/2017/11', u'PCIL/TC/STORES/2017/1', u'PCIL/TC/STORES/2017/3', u'PCIL/TC/PROJECTS/DESIGN&OPERATIONS/2017/1', u'PCIL/TC/STORES/2017/15', u'PCIL/TC/STORES/2017/14', u'PCIL/TC/STORES/2017/12', u'PCIL/TC/STORES/2017/6'], u'_id': u'TC', u'total_savings': 183138}],
	 # [], 
	 # [], 
	 # [], 
	 # [], [183138, 19]]
	# plant_desc=['TC','TA','BP','GC','GP']
	# print details[0][0]['_id']
	# try:

	if len(details[1])!=0:
		worksheet.write(row,0,plants_desc[details[0][0]['_id']],data_style)
		worksheet.write(row,1,len(details[0][0]['kaizens']),data_style)
		worksheet.write(row,2,details[0][0]['total_savings'],data_style)
	else:
		worksheet.write(row,0,plants_desc['TC'],data_style)
		worksheet.write(row,1,0,data_style)
		worksheet.write(row,2,0,data_style)
	row+=1
	if len(details[1])!=0:
		worksheet.write(row,0,plants_desc[details[1][0]['_id']],data_style)
		worksheet.write(row,1,len(details[1][0]['kaizens']),data_style)
		worksheet.write(row,2,details[1][0]['total_savings'],data_style)
	else:
		worksheet.write(row,0,plants_desc['TA'],data_style)
		worksheet.write(row,1,0,data_style)
		worksheet.write(row,2,0,data_style)
	row+=1
	if len(details[2])!=0:
		worksheet.write(row,0,plants_desc[details[2][0]['_id']],data_style)
		worksheet.write(row,1,len(details[2][0]['kaizens']),data_style)
		worksheet.write(row,2,details[2][0]['total_savings'],data_style)
	else:
		worksheet.write(row,0,plants_desc['BP'],data_style)
		worksheet.write(row,1,0,data_style)
		worksheet.write(row,2,0,data_style)
	row+=1
	if len(details[3])!=0:
		worksheet.write(row,0,plants_desc[details[3][0]['_id']],data_style)
		worksheet.write(row,1,len(details[3][0]['kaizens']),data_style)
		worksheet.write(row,2,details[3][0]['total_savings'],data_style)
	else:
		worksheet.write(row,0,plants_desc['GC'],data_style)
		worksheet.write(row,1,0,data_style)
		worksheet.write(row,2,0,data_style)
	row+=1
	if len(details[4])!=0:
		worksheet.write(row,0,plants_desc[details[4][0]['_id']],data_style)
		worksheet.write(row,1,len(details[4][0]['kaizens']),data_style)
		worksheet.write(row,2,details[4][0]['total_savings'],data_style)
	else:
		worksheet.write(row,0,plants_desc['GP'],data_style)
		worksheet.write(row,1,0,data_style)
		worksheet.write(row,2,0,data_style)
	row+=1

	worksheet.write(row,0,'Total',headings_style)
	worksheet.write(row,1,details[5][1],headings_style)
	worksheet.write(row,2,details[5][0],headings_style)
	row+=1
	workbook.close()
	return 'success'

def excel_detailed_report(from_date,to_date,details):
	row=1
	doc_path=find_one_in_collection('portal_settings',{'config':'kaizen_Reports_PATH'})
	plants_desc={'TC':'TALARICHERUVU','BP':'BOYAREDDYPALLI','GC':'GANESHPAHAD CEMENT','TA':'TANDUR'}
	doc_path=doc_path['directory']
	workbook = xlsxwriter.Workbook(doc_path+'detailed_report.xlsx')
	worksheet = workbook.add_worksheet()
	style_title_main=workbook.add_format({'bold': True,'font_color':'black','align': 'center','font_size':15})
	style_title_report=workbook.add_format({'bold': True,'font_color':'black','align': 'center','font_size':13})
	style_OD=workbook.add_format({'font_color':'black','bg_color':'#BDBDBD'})
	data_style=workbook.add_format({'border': 1})
	format = workbook.add_format()
	format.set_text_wrap()
	headings_style=workbook.add_format({'border': 1,'bold': True})
	bold = workbook.add_format({'bold': True})
	style_excluded = workbook.add_format({'font_color':'black','border':2,'border_color':'black','bg_color':'#BDBDBD'})
	worksheet.merge_range(row,0,row,11,'Penna Cement Industries Limited',style_title_main)
	row+=1
	worksheet.merge_range(row,0,row,11,'Kaizen Detailed Report',style_title_report)
	row+=2
	worksheet.merge_range(row,0,row,11,'From Date : '+from_date)
	row+=1
	worksheet.merge_range(row,0,row,11,'To Date     : '+to_date)
	row+=2
	headings=['Plant','Date','Kaizen ID','Problem Identified','Countermeasure','Result','Intangible Benifit','Tangible Benifit','Employee Name']
	for ih in range(len(headings)):
		worksheet.write(row,ih,headings[ih],headings_style)
	row+=1

	
	# iih=0
	# worksheet.set_row(row, 40)
	for iiijjj in range(len(details)):
		# for iih in range(len(headings)):
		print details[iiijjj]['plant_id'],' details---------'
		worksheet.write(row,0,plants_desc[details[iiijjj]['plant_id']],data_style)
		worksheet.write(row,1,details[iiijjj]['kaizen_date'],data_style)
		worksheet.write(row,2,details[iiijjj]['kaizen_id'],data_style)
		worksheet.write(row,3,details[iiijjj]['problem_identified'],data_style)
		worksheet.write(row,4,details[iiijjj]['countermeasure'],data_style)
		worksheet.write(row,5,details[iiijjj]['result'],data_style)
		if details[iiijjj]['Intangible_status']=='checked':
			worksheet.write(row,6,details[iiijjj]['InTangible_details']['Intangible_sel_option'],data_style)
		else:
			worksheet.write(row,6,'',data_style)
		worksheet.write(row,7,details[iiijjj]['tangible_cost'],data_style)
		emp_implemented=''
		if details[iiijjj]['implementation_type']=='individual':
			for lj in range(4):
				if details[iiijjj]['Implementation_details'][lj]['employee_name']!='':
					emp_implemented+=str(details[iiijjj]['Implementation_details'][lj]['employee_name'])+','
			# pass
		elif details[iiijjj]['implementation_type']=='team':
			emp_implemented=details[iiijjj]['Implementation_details'][0]['Implementation_desc']
			pass
		worksheet.write(row,8,emp_implemented[:len(emp_implemented)],data_style)
		row+=1
	return 'success'

@zen_reports.route('/Kaizen_Report/', methods = ['GET', 'POST'])
@login_required
def Kaizen_Report():
	user_details = base()
	doc_path=find_one_in_collection('portal_settings',{'config':'kaizen_Reports_PATH'})
	db=dbconnect()
	if request.method=='POST':
		form=request.form
		try:
			search_dates=[form['from_date'],form['to_date']]
			to_date=datetime.datetime.strptime(form['to_date'],'%d-%m-%Y')
			from_date=datetime.datetime.strptime(form['from_date'],'%d-%m-%Y')
			if 'overall' in form:
				kaizens_TC=db.Kaizen_details.aggregate([{'$match':{'plant_id':'TC','status':'Approved','$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]}},{'$group':{'_id':'$plant_id','total_savings':{'$sum':'$tangible_cost'},'kaizens':{ '$addToSet': "$kaizen_id" }}}])
				kaizens_TC=list(kaizens_TC)
				
				kaizens_TA=db.Kaizen_details.aggregate([{'$match':{'plant_id':'TA','status':'Approved','$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]}},{'$group':{'_id':'$plant_id','total_savings':{'$sum':'$tangible_cost'},'kaizens':{ '$addToSet': "$kaizen_id" }}}])
				kaizens_TA=list(kaizens_TA)
				
				kaizens_BP=db.Kaizen_details.aggregate([{'$match':{'plant_id':'BP','status':'Approved','$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]}},{'$group':{'_id':'$plant_id','total_savings':{'$sum':'$tangible_cost'},'kaizens':{ '$addToSet': "$kaizen_id" }}}])
				kaizens_BP=list(kaizens_BP)
				
				kaizens_GC=db.Kaizen_details.aggregate([{'$match':{'plant_id':'GC','status':'Approved','$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]}},{'$group':{'_id':'$plant_id','total_savings':{'$sum':'$tangible_cost'},'kaizens':{ '$addToSet': "$kaizen_id" }}}])
				kaizens_GC=list(kaizens_GC)
				
				kaizens_GP=db.Kaizen_details.aggregate([{'$match':{'plant_id':'GP','status':'Approved','$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]}},{'$group':{'_id':'$plant_id','total_savings':{'$sum':'$tangible_cost'},'kaizens':{ '$addToSet': "$kaizen_id" }}}])
				kaizens_GP=list(kaizens_GP)

				total_savings=0
				total_kaizens=0
				if kaizens_TC:
					total_savings=total_savings+kaizens_TC[0]['total_savings']
					total_kaizens=total_kaizens+len(kaizens_TC[0]['kaizens'])
				if kaizens_TA:
					total_savings=total_savings+kaizens_TA[0]['total_savings']
					total_kaizens=total_kaizens+len(kaizens_TA[0]['kaizens'])
				if kaizens_BP:
					total_savings=total_savings+kaizens_BP[0]['total_savings']
					total_kaizens=total_kaizens+len(kaizens_BP[0]['kaizens'])
				if kaizens_GC:
					total_savings=total_savings+kaizens_GC[0]['total_savings']
					total_kaizens=total_kaizens+len(kaizens_GC[0]['kaizens'])
				if kaizens_GP:
					total_savings=total_savings+kaizens_GP[0]['total_savings']
					total_kaizens=total_kaizens+len(kaizens_GP[0]['kaizens'])

					# +kaizens_TA[0]['total_savings']+kaizens_BP[0]['total_savings']+kaizens_GC[0]['total_savings']+kaizens_GP[0]['total_savings']
				# total_kaizens=len(kaizens_TC[0]['kaizens'])+len(kaizens_TA[0]['kaizens'])+len(kaizens_BP[0]['kaizens'])+len(kaizens_GC[0]['kaizens'])+len(kaizens_GP[0]['kaizens'])
				kaizens=[kaizens_TC,kaizens_TA,kaizens_BP,kaizens_GC,kaizens_GP,[total_savings,total_kaizens]]
				data_show='overall'
				report_path=doc_path['sub_directory']+'overall_summary_report.xlsx'
				excel_report=excel_overall_report(form['from_date'],form['to_date'],kaizens)
				# print excel_report
			elif 'detailed_report' in form:
				kaizens=find_in_collection('Kaizen_details',{'status':'Approved','plant_id':form['plant_id'],'$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]})		
				# print len(kaizens)			
				excel_report=excel_detailed_report(form['from_date'],form['to_date'],kaizens)
				# print excel_report,' excelreport'
				data_show='detailed_report'
				report_path=doc_path['sub_directory']+'detailed_report.xlsx'
		except:
			flash('Please Select All Required Fields & Try Again.','alert-danger')
		return render_template('Kaizen_report.html',search_dates=search_dates,user_details=user_details,kaizens=kaizens,data_show=data_show,report_path=report_path)
	return render_template('Kaizen_report.html',user_details=user_details)

@zen_reports.route('/Kaizens_Download/', methods = ['GET', 'POST'])
@login_required
def Kaizens_Download():
	user_details = base()	
	if request.method=='POST':
		form=request.form
		print form,'main form'
		if form['submit']=='ajax_data':
			# print form,'ajaxform'
			department_list=find_and_filter('departmentmaster',{'plant':form['data']},{'_id':0,'dept_pid':1,'req_desc':1})
			return jsonify({'results':department_list})
			# ('kaizen_id', u''), ('department', u'50095218'), ('plant_id', u'TC'),
		if form['submit']=='View Kaizens':
			search_dates=[form['from_date'],form['to_date']]
			if form['to_date'] !='':
				to_date=datetime.datetime.strptime(form['to_date'],'%d-%m-%Y')
				from_date=datetime.datetime.strptime(form['from_date'],'%d-%m-%Y')
			else:
				to_date=''
				from_date=''
				pass
			kaizen_details=''
			if form['plant_id'] !='':
				# print 'plant selected'
				if form['department']!='' and form['kaizen_id']!='':
					# conjuction
					kaizen_details=find_in_collection_sort('Kaizen_details',{'status':'Approved','kaizen_id':form['kaizen_id']},[("kaizen_isodate",-1)])
					print '1'
				elif form['department']  !='' and form['kaizen_id'] =='' and form['to_date'] !='':
					# one or the other
					kaizen_details=find_in_collection_sort('Kaizen_details',{'status':'Approved','plant_id':form['plant_id'],'department_id':form['department'],'$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]},[("kaizen_isodate",-1)])
					print '2'
				elif form['department'] =='' and form['kaizen_id'] !='' and form['to_date'] !='':
					# one or the other
					kaizen_details=find_in_collection_sort('Kaizen_details',{'status':'Approved','kaizen_id':form['kaizen_id'],'$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]},[("kaizen_isodate",-1)])
					print '3'
				elif form['department'] =='' and form['kaizen_id'] =='' and form['to_date'] !='':
					# one or the other
					kaizen_details=find_in_collection_sort('Kaizen_details',{'status':'Approved','plant_id':form['plant_id'],'$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]},[("kaizen_isodate",-1)])
					print '4'
			elif form['plant_id'] =='' and form['kaizen_id'] !='':
				# only based on kaizen id
				kaizen_details=find_in_collection_sort('Kaizen_details',{'status':'Approved','kaizen_id':form['kaizen_id']},[("kaizen_isodate",-1)])
				print '5'
			elif form['plant_id'] =='' and form['kaizen_id'] =='' and form['to_date'] !='':
				kaizen_details=find_in_collection_sort('Kaizen_details',{'status':'Approved','$and':[{'kaizen_isodate':{'$gte':from_date}},{'kaizen_isodate':{'$lte':to_date}}]},[("kaizen_isodate",-1)])
			# print kaizen_details
			# kaizen_details=find_one_in_collection('Kaizen_details',{'_id':ObjectId(form['prid'])})
			return render_template('Kaizens_download.html',user_details=user_details,kaizen_details=kaizen_details,search_dates=search_dates)
	return render_template('Kaizens_download.html',user_details=user_details,kaizen_details=[])

@zen_reports.route('/get_plantemployees/', methods = ['GET', 'POST'])
def get_plantemployees():
	form = request.args
	regex_str=form['term']
	plant=form['plant_id']
	users=find_and_filter('Userinfo',{'employee_status':'active',"designation_id":{'$ne':'99999999'},'plant_id':plant,'$or':[{"username" : {'$regex': str(regex_str),'$options': 'i' }},{'f_name' : {'$regex': str(regex_str),'$options': 'i' }}]},{'_id':0,'username':1,'f_name':1,'l_name':1,'designation_id':1})
	return json.dumps({"results":users})


######################### End of Kaizens development ######################### 








#################3 MOved To Production on 26th June 2017 ############################
def formatDate(dtDateTime,strFormat="%d-%m-%Y"):
    # format a datetime object as YYYY-MM-DD string and return
    return dtDateTime.strftime(strFormat)

def mk_DateTime(dateString,strFormat="%d-%m-%Y"):# Expects "YYYY-MM-DD" string
    eSeconds = time.mktime(time.strptime(dateString,strFormat))
    return datetime.datetime.fromtimestamp(eSeconds)

def mkFirstOfMonth(dtDateTime):
    return mk_DateTime(formatDate(dtDateTime,"01-%m-%Y"))

def mkLastOfMonth(dtDateTime):
    dYear = dtDateTime.strftime("%Y")        #get the year
    dYear=str(int(dYear))
    dMonth = str(int(dtDateTime.strftime("%m"))%12+1)#get next month, watch rollover
    dDay = "1"                               #first day of next month
    nextMonth = mk_DateTime("%s-%s-%s"%(dDay,dMonth,dYear))#make a datetime obj for 1st of next month
    delta = datetime.timedelta(seconds=1)    #create a delta of 1 second
    return nextMonth - delta



@zen_reports.route('/Mines_Report/', methods = ['GET', 'POST'])
@login_required
def Mines_Report():
	debug_flag=find_one_in_collection('miv_config',{'status':'debug'})
	if debug_flag:
		debug_flag=debug_flag['debug']['daily_mines_report']
	else:
		debug_flag="off"
	user_details=base()
	user_plant=user_details["plant_id"]
	plant_desc={'TC':'1001','GC':'1002','BP':'1003','TA':'1004','GP':'2001'}
	user_plant_id=plant_desc[user_plant]
	uid = current_user.username
	equipment_details=[]
	prev_date=find_in_collection_sort('DMR_Operations_data',{'$or':[{'status':'Submitted'},{'status':'Approved'}],'plant':user_plant},[('_id',-1)])
	if prev_date:
		# print prev_date[0]['Date'],' -- ',str(mkLastOfMonth(mk_DateTime(prev_date[0]['Date']))).split(' ')[0].split('-')[2]
		if prev_date[0]['Date'].split('-')[0]==str(mkLastOfMonth(mk_DateTime(prev_date[0]['Date']))).split(' ')[0].split('-')[2]:
			# print 'the previous day is the last date of a month'
			min_date=[int(01),int(prev_date[0]['Date'].split('-')[1]),int(prev_date[0]['Date'].split('-')[2])]
		else:
			min_date=[int(prev_date[0]['Date'].split('-')[0])+1,int(prev_date[0]['Date'].split('-')[1])-1,int(prev_date[0]['Date'].split('-')[2])]
		max_date=[int(str(datetime.datetime.now()).split(' ')[0].split('-')[0]),int(str(datetime.datetime.now()).split(' ')[0].split('-')[1])-1,int(str(datetime.datetime.now()).split(' ')[0].split('-')[2])]
		# print max_date
	debug='nottesting'
	if request.method == 'POST':
		form=request.form
		if debug_flag=='on':
			logging.info('-----'+str(form)+'--- @ '+str(datetime.datetime.now()))
		date_of_report=form['date_of_report']
		try:
			new_date_of_report=datetime.datetime.strptime(date_of_report,'%d-%m-%Y')
		except:
			new_date_of_report=date_of_report
		check_prev_details={'Loading':'Drilling','Transportation':'Loading','Crusher':'Transportation','Dozing':'Crusher','Blasting':'Dozing','Excavator_Loading':'Blasting','Transportation_Dumper':'Excavator_Loading','Production':'Transportation_Dumper','Others':'Production'}
		if 'equip_type' in form:
			old_date_docs=datetime.datetime(int(form['date_of_report'].split('-')[2]),int(form['date_of_report'].split('-')[1]),int(form['date_of_report'].split('-')[0]))-timedelta(1)
			old_date_docs=datetime.datetime.strftime(old_date_docs,'%d-%m-%Y').split(' ')[0]
			prev_day_exists=find_and_filter('DMR_Operations_data',{'status':'Approved','plant':user_plant,'Date':old_date_docs},{'_id':0,'equipment_details':1,'plant':1,'Date':1})
			# print prev_day_exists,' prev details'
			########### code to check wheather previous day data is submitted or not.
			if debug_flag !='default_data':
				if not prev_day_exists:
					# msg will be shown as table_1 in others else.
				# 	# flash('Previous Day Details Are Not Filled.Please Fill The Previous Day Details First.','alert-danger')
					return jsonify({'prev_day_exists':'0','results':'test','result':'Previous Day Details Are Not Filled.Please Fill The Previous Day Details First (Not Default).'})
			
			### chceking for any details saved on that given day.
			equipment_details=find_one_in_collection('portal_settings',{'config':'equipment_category'})
			equipment_details_default=find_one_in_collection('portal_settings',{'config':'equipment_category'})
			saved_details=find_and_filter('DMR_Operations_data',{'plant':user_plant,'Date':form['date_of_report']},{'_id':0,'equipment_details':1,'plant':1,'Date':1,'status':1})
			# print saved_details,'saved details'
			if saved_details:
				equipment_details=saved_details[0]['equipment_details']
				count=len(equipment_details)
				view_type='saved'
				if saved_details[0]['status']=='Submitted' or saved_details[0]['status']=='Approved':
					view_type='submitted'
					return jsonify({'results':equipment_details[form['equip_type']],'equip_type':form['equip_type'],'view_type':view_type,'plant_id_view':user_plant})
				else:
					if form['equip_type'] in equipment_details:
						return jsonify({'results':equipment_details[form['equip_type']],'equip_type':form['equip_type'],'view_type':view_type,'plant_id_view':user_plant})
					else:
						old_date_equip=datetime.datetime(int(form['date_of_report'].split('-')[2]),int(form['date_of_report'].split('-')[1]),int(form['date_of_report'].split('-')[0]))-timedelta(1)
						prev_equipment_details_equip=find_and_filter('DMR_Operations_data',{'status':'Approved','plant':user_plant,'Date':datetime.datetime.strftime(old_date_equip,'%d-%m-%Y').split(' ')[0]},{'_id':0,'equipment_details':1,'plant':1,'Date':1})
						if prev_equipment_details_equip==[]:
							view_type='no_prev_doc'
							if debug_flag=='on':
								logging.info('----- '+'Previous Day Details Are Not Available For Plant '+user_details['plant_id']+' .Assigning Default Data. --- @ '+str(datetime.datetime.now()))
							return jsonify({'prev_day_exists':'0','equip_type':form['equip_type'],'view_type':view_type,'results':equipment_details_default['equipments'][user_plant][form['equip_type']],'result':'Previous Day Details Are Not Filled.Please Fill The Previous Day Details First.','plant_id_view':user_plant})
							# return jsonify({'prev_day_exists':'0','results':'test','result':'Previous Day Details Are Not Filled.Please Fill The Previous Day Details First.'})
							equipment_details=equipment_details['equipments'][user_plant][form['equip_type']]
						else:
							if new_date_of_report.date().day ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
								view_type='new_month'
								equipment_details=prev_equipment_details_equip[0]['equipment_details'][form['equip_type']]
								if debug_flag=='on':
									logging.info('----- '+view_type+' --- @ '+str(datetime.datetime.now()))
							elif new_date_of_report.date().day ==1 and new_date_of_report.date().month ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
								view_type='new_year'
								equipment_details=prev_equipment_details_equip[0]['equipment_details'][form['equip_type']]
								if debug_flag=='on':
									logging.info('----- '+view_type+' --- @ '+str(datetime.datetime.now()))
							else:
								view_type='prev_doc'
								equipment_details=prev_equipment_details_equip[0]['equipment_details'][form['equip_type']]
								# print equipment_details
								if debug_flag=='on':
									logging.info('----- '+view_type+' --- @ '+str(datetime.datetime.now()))
								# print 'assigning previousday to current for current day equipment'
			else:
				if debug_flag=='default_data':
					debug_details={'employee_id':user_details["username"],'employee_name':user_details["f_name"]+' '+user_details["l_name"],"Date" :date_of_report ,"plant" :user_details['plant_id'] ,'status':'saved',"equipment_details":{ "Drilling" : [ 
					{
					"equipment" : "400000000110",
					"remarks" : "",
					"ideal_hours" : {
					"YTD" : 0,
					"MTD" : 0,
					"ONDAY" : 0
					},
					"description" : "REVATHI DRILL MACHINE",
					"year" : "year",
					"date" : "date",
					"breakdown_hours" : {
					"YTD" : 0,
					"MTD" : 0,
					"ONDAY" : 0
					},
					"running_hours" : {
					"YTD" : 0,
					"MTD" : 0,
					"ONDAY" : 0
					}
					}
					]}}
					save_collection('DMR_Operations_data',debug_details)
				## if no data is avalable for given date.
				## checking for a new month
				if new_date_of_report.date().day ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
					count=len(equipment_details)
					old_date=datetime.datetime(int(form['date_of_report'].split('-')[2]),int(form['date_of_report'].split('-')[1]),int(form['date_of_report'].split('-')[0]))-timedelta(1)
					prev_equipment_details=find_and_filter('DMR_Operations_data',{'status':'Approved','plant':user_plant,'Date':datetime.datetime.strftime(old_date,'%d-%m-%Y').split(' ')[0]},{'_id':0,'equipment_details':1,'plant':1,'Date':1})
					## if previous day details are also not available.
					if prev_equipment_details==[]:
						view_type='no_prev_doc'
						# print equipment_details
						return jsonify({'prev_day_exists':'0','results':'test','result':'Previous Day Details Are Not Filled.Please Fill The Previous Day Details First.','plant_id_view':user_plant})
						equipment_details=equipment_details['equipments'][user_plant][form['equip_type']]
					else:
						## resetting the MTD 
						view_type='new_month'						
						equipment_details=prev_equipment_details[0]['equipment_details'][form['equip_type']]
					if debug_flag=='on':
						logging.info('----- view_type - '+view_type+' --- @ '+str(datetime.datetime.now()))
				elif new_date_of_report.date().day ==1 and new_date_of_report.date().month ==1:
					count=len(equipment_details)
					old_date=datetime.datetime(int(form['date_of_report'].split('-')[2]),int(form['date_of_report'].split('-')[1]),int(form['date_of_report'].split('-')[0]))-timedelta(1)
					prev_equipment_details=find_and_filter('DMR_Operations_data',{'status':'Approved','plant':user_plant,'Date':datetime.datetime.strftime(old_date,'%d-%m-%Y').split(' ')[0]},{'_id':0,'equipment_details':1,'plant':1,'Date':1})
					## if previous day details are also not available.
					if prev_equipment_details==[]:
						view_type='no_prev_doc'
						# print equipment_details
						return jsonify({'prev_day_exists':'0','results':'test','result':'Previous Day Details Are Not Filled.Please Fill The Previous Day Details First.','plant_id_view':user_plant})
						equipment_details=equipment_details['equipments'][user_plant][form['equip_type']]
					else:
						## resetting the MTD 
						view_type='new_year'						
						equipment_details=prev_equipment_details[0]['equipment_details'][form['equip_type']]
					if debug_flag=='on':
						logging.info('----- view_type'+ view_type+' --- @ '+str(datetime.datetime.now()))
				### given date is not the first day of the month.
				else:
					logging.info('current date is not start date of a month or year -- in else.')
					count=len(equipment_details)
					old_date=datetime.datetime(int(form['date_of_report'].split('-')[2]),int(form['date_of_report'].split('-')[1]),int(form['date_of_report'].split('-')[0]))-timedelta(1)
					prev_equipment_details=find_and_filter('DMR_Operations_data',{'status':'Approved','plant':user_plant,'Date':datetime.datetime.strftime(old_date,'%d-%m-%Y').split(' ')[0]},{'_id':0,'equipment_details':1,'plant':1,'Date':1})
					def_data_assign=find_and_filter('DMR_Operations_data',{'status':'saved','plant':user_plant,'Date':datetime.datetime.strftime(old_date,'%d-%m-%Y').split(' ')[0]},{'_id':0,'equipment_details':1,'plant':1,'Date':1})
					if prev_equipment_details==[] and def_data_assign !=[]:
						view_type='no_prev_doc'
						# print 'no previous day details are available'
						return jsonify({'prev_day_exists':'0','equip_type':form['equip_type'],'view_type':view_type,'results':equipment_details['equipments'][user_plant][form['equip_type']],'result':'Previous Day Details Are Not Filled.Please Fill The Previous Day Details First.','plant_id_view':user_plant})
						equipment_details=equipment_details['equipments'][user_plant][form['equip_type']]
					else:
						view_type='prev_doc'
						equipment_details=prev_equipment_details[0]['equipment_details'][form['equip_type']]
					if debug_flag=='on':
						logging.info('----- view_type '+view_type+'--- @ '+str(datetime.datetime.now()))
			if debug_flag=='on':
				logging.info('----- EndOf Ajax Response view_type - '+view_type+'equipment_details - '+str(equipment_details)+' --- @ '+str(datetime.datetime.now()))
			return jsonify({'results':equipment_details,'equip_type':form['equip_type'],'view_type':view_type,'plant_id_view':user_plant})
		elif 'submit' in form  and form['submit']=='Save Details':
			# print 'in saved Details'
			if form['equip_category'] in ['Drilling','Loading','Transportation','Crusher','Dozing','Others']:			
				date_of_report=form['date_of_report']
				equipments_list=form.getlist('equipment')
				description_list=form.getlist('description')
				RH_ONDAY=form.getlist('RH_ONDAY')
				RH_MTD=form.getlist('RH_MTD')
				RH_YTD=form.getlist('RH_YTD')
				BDH_ONDAY=form.getlist('BDH_ONDAY')
				BDH_MTD=form.getlist('BDH_MTD')
				BDH_YTD=form.getlist('BDH_YTD')
				IDH_ONDAY=form.getlist('IDH_ONDAY')
				IDH_MTD=form.getlist('IDH_MTD')
				IDH_YTD=form.getlist('IDH_YTD')
				remarks=form.getlist('equip_remarks')
				# print form,'queried tosaavethedetails'
				consolidate_details=[]
				for j in range(len(equipments_list)):
					new_date_of_report=datetime.datetime.strptime(date_of_report,'%d-%m-%Y')
					if new_date_of_report.date().day ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
						RH_MTD_N=0
						BDH_MTD_N=0
						IDH_MTD_N=0
						if new_date_of_report.date()==datetime.datetime(datetime.datetime.now().year,1,1):
							RH_YTD_N=0
							BDH_YTD_N=0
							IDH_YTD_N=0
						else:
							RH_YTD_N=RH_YTD[j]
							BDH_YTD_N=BDH_YTD[j]
							IDH_YTD_N=IDH_YTD[j]
					else:
						RH_MTD_N=RH_MTD[j]
						BDH_MTD_N=BDH_MTD[j]
						IDH_MTD_N=IDH_MTD[j]
						RH_YTD_N=RH_YTD[j]
						BDH_YTD_N=BDH_YTD[j]
						IDH_YTD_N=IDH_YTD[j]

					if RH_ONDAY[j]=='':
						RH_MTD_F=float(RH_MTD_N)
						RH_YTD_F=float(RH_YTD_N)
						# print "1"
					else:
						if form['submit'] == 'Save Details':
							RH_MTD_F=float(RH_MTD_N)
							RH_YTD_F=float(RH_YTD_N)
						elif form['submit'] == 'Submit':
							RH_MTD_F=float(RH_ONDAY[j])+float(RH_MTD_N)
							RH_YTD_F=float(RH_ONDAY[j])+float(RH_YTD_N)
						# print "2"
					if BDH_ONDAY[j]=='':
						BDH_MTD_F=float(BDH_MTD_N)
						BDH_YTD_F=float(BDH_YTD_N)
						# print "3"
					else:
						if form['submit'] == 'Save Details':
							BDH_MTD_F=float(BDH_MTD_N)
							BDH_YTD_F=float(BDH_YTD_N)
						elif form['submit'] == 'Submit':							
							BDH_MTD_F=float(BDH_ONDAY[j])+float(BDH_MTD_N)
							BDH_YTD_F=float(BDH_ONDAY[j])+float(BDH_YTD_N)
						# print "4"
					if IDH_ONDAY[j]=='':
						IDH_MTD_F=float(IDH_MTD_N)
						IDH_YTD_F=float(IDH_YTD_N)
						# print "5"
					else:
						if form['submit'] == 'Save Details':
							IDH_MTD_F=float(IDH_MTD_N)
							IDH_YTD_F=float(IDH_YTD_N)
						elif form['submit'] == 'Submit':							
							IDH_MTD_F=float(IDH_ONDAY[j])+float(IDH_MTD_N)
							IDH_YTD_F=float(IDH_ONDAY[j])+float(IDH_YTD_N)
						# print "6"
				
					consolidate_details.append({'equipment':equipments_list[j],'description':description_list[j],"date" : "date","year" : "year",
				"running_hours" : {"ONDAY" : float(RH_ONDAY[j]),"MTD" :RH_MTD_F,"YTD" :RH_YTD_F},
				"breakdown_hours" : {"ONDAY":float(BDH_ONDAY[j]),"MTD" :BDH_MTD_F,"YTD" :BDH_YTD_F },
            	"ideal_hours" : {"ONDAY":float(IDH_ONDAY[j]),"MTD" :IDH_MTD_F,"YTD" :IDH_YTD_F },
            	"remarks" : remarks[j]
            	})

				if form['submit']=='Save Details':
					update_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report},{'$set':{'equipment_details.'+form['equip_category']:consolidate_details,'employee_id':user_details["username"],'employee_name':user_details["f_name"]+' '+user_details["l_name"],'status':'saved','saved_time':datetime.datetime.now()}})
					flash('Details Of Equipment Type - '+form['equip_category']+' Updated Sucessfully.','alert-success')
			elif form['equip_category'] in ['Blasting']:
				date_of_report=form['date_of_report']
				equipments_list=form.getlist('equipment')
				descriptions_list=form.getlist('description')
				# Booster_OD=form.getlist('Booster_OD')	
				# Booster_MTD=form.getlist('Booster_MTD')
				# # print Booster_MTD,'od booster'
				# Columcharge_OD=form.getlist('Columcharge_OD')
				# Columcharge_MTD=form.getlist('Columcharge_MTD')
				# # print Columcharge_MTD,'columncharge'
				# ANFO_OD=form.getlist('ANFO_OD')
				# ANFO_MTD=form.getlist('ANFO_MTD')
				# print ANFO_MTD,'anfo'
				Consumption_OD=form.getlist('Consumption_OD')
				Consumption_MTD=form.getlist('Consumption_MTD')
				remarks=form.getlist('Remarks')
				consolidate_details=[]
				for jj in range(len(descriptions_list)):
					new_date_of_report=datetime.datetime.strptime(date_of_report,'%d-%m-%Y')
					if new_date_of_report.date().day ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
						# Booster_MTD_N=Booster_OD[jj]
						# Columcharge_MTD_N=Columcharge_OD[jj]
						# ANFO_MTD_N=ANFO_OD[jj]
						Consumption_OD_N=Consumption_OD[jj]
						Consumption_MTD_N=Consumption_OD[jj]
					else:
						# print Booster_MTD,'------------'
						if form['submit']=='Submit':
							# Booster_MTD_N=float(Booster_OD[jj])+float(Booster_MTD[jj])
							# Columcharge_MTD_N=float(Columcharge_OD[jj])+float(Columcharge_MTD[jj])
							# ANFO_MTD_N=float(ANFO_OD[jj])+float(ANFO_MTD[jj])
							Consumption_OD_N=float(Consumption_OD[jj])
							Consumption_MTD_N=float(Consumption_OD[jj])+float(Consumption_MTD[jj])
						elif form['submit']=='Save Details':
							# Booster_MTD_N=float(Booster_MTD[jj])
							# Columcharge_MTD_N=float(Columcharge_MTD[jj])
							# ANFO_MTD_N=float(ANFO_MTD[jj])
							Consumption_OD_N=float(Consumption_OD[jj])
							Consumption_MTD_N=float(Consumption_MTD[jj])
					consolidate_details.append({'equipment':equipments_list[jj],'description':descriptions_list[jj],"date" : "date","year" : "year",
				# "Booster" : {"ONDAY" : float(Booster_OD[jj]),"MTD" :float(Booster_MTD_N)},
				# "Columcharge" : {"ONDAY":float(Columcharge_OD[jj]),"MTD" :float(Columcharge_MTD_N)},
    #         	"ANFO" : {"ONDAY":float(ANFO_OD[jj]),"MTD" :float(ANFO_MTD_N)},
            	'consumption':{"ONDAY":float(Consumption_OD_N),"MTD":float(Consumption_MTD_N)},
            	"remarks" : remarks[jj]
						})
				if form['submit']=='Save Details':
					update_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report},{'$set':{'equipment_details.'+form['equip_category']:consolidate_details,'status':'saved','saved_time':datetime.datetime.now()}})
					flash('Details Of Equipment Type - '+form['equip_category']+' Updated Sucessfully.','alert-success')
			elif form['equip_category'] in ['Excavator_Loading','Transportation_Dumper']:
				date_of_report=form['date_of_report']
				equipments_list=form.getlist('equipment')
				descriptions_list=form.getlist('description')
				Limestone=form.getlist('Limestone')
				Laterite=form.getlist('Laterite')			 
				Clay=form.getlist('Clay')
				# print Booster_MTD,'od booster'
				Waste=form.getlist('Waste')
				Misc=form.getlist('Misc')
				# print Columcharge_MTD,'columncharge'
				Trips_OD=form.getlist('Trips_OD')
				Trips_MTD=form.getlist('Trips_MTD')
				# print ANFO_MTD,'anfo'
				remarks=form.getlist('Remarks')
				consolidate_details=[]
				for jjj in range(len(descriptions_list)):
					new_date_of_report=datetime.datetime.strptime(date_of_report,'%d-%m-%Y')
					if new_date_of_report.date().day ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
						Trips_MTD_N=float(Trips_OD[jjj])
						Trips_OD_N=float(Trips_OD[jjj])						
					else:
						# print Booster_MTD,'------------'
						if form['submit']=='Submit':
							Trips_MTD_N=float(Limestone[jjj])+float(Laterite[jjj])+float(Clay[jjj])+float(Waste[jjj])+float(Misc[jjj])+float(Trips_MTD[jjj])
							Trips_OD_N=float(Limestone[jjj])+float(Laterite[jjj])+float(Clay[jjj])+float(Waste[jjj])+float(Misc[jjj])
						elif form['submit']=='Save Details':
							Trips_MTD_N=float(Trips_MTD[jjj])
							Trips_OD_N=float(Limestone[jjj])+float(Laterite[jjj])+float(Clay[jjj])+float(Waste[jjj])+float(Misc[jjj])
					# print Trips_MTD_N,'5056'
					consolidate_details.append({'equipment':equipments_list[jjj],'description':descriptions_list[jjj],"date" : "date","year" : "year",
				"Limestone" : {"ONDAY" :float(Limestone[jjj])},
				"Laterite" : {"ONDAY" :float(Laterite[jjj])},
				"Clay" : {"ONDAY" :float(Clay[jjj])},
				"Waste" : {"ONDAY" :float(Waste[jjj])},
				"Misc" : {"ONDAY" :float(Misc[jjj])},
				"Trips_OD" : Trips_OD_N,
				"Trips_MTD" :float(Trips_MTD_N) ,"remarks" : remarks[jjj],
						})
				if form['submit']=='Save Details':
					update_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report},{'$set':{'equipment_details.'+form['equip_category']:consolidate_details,'status':'saved','saved_time':datetime.datetime.now()}})
					flash('Details Of Equipment Type - '+form['equip_category']+' Updated Sucessfully.','alert-success')
			elif form['equip_category'] in ['Production']:
				date_of_report=form['date_of_report']
				crusher_data=find_one_in_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report})
				try:
					crusher_onday=crusher_data['equipment_details']['Crusher'][0]['running_hours']['ONDAY']
					# crusher_mtd=crusher_data['equipment_details']['Crusher'][0]['running_hours']['MTD']
					# crusher_ytd=crusher_data['equipment_details']['Crusher'][0]['running_hours']['YTD']
				except:
					crusher_onday=0
					# crusher_mtd=0
					# crusher_ytd=0
				equipments_list=form.getlist('equipment')
				descriptions_list=form.getlist('description')
				ONDAY=form.getlist('ONDAY')
				MTD=form.getlist('MTD')	
				YTD=form.getlist('YTD')
				# print crusher_data,' crusher running hours value'
				if crusher_onday >0:
					ONDAY[3]=round(float(ONDAY[1])/crusher_onday,2) # limestone in MT/Cruhser RH
				else:
					ONDAY[3]=0
				# try:
				# 	MTD[3]=round(float(MTD[1])/crusher_mtd,2)
				# except:
				# 	MTD[3]=0.0
				# try:
				# 	YTD[3]=round(float(YTD[1])/crusher_mtd,2)
				# except:
				# 	YTD[3]=0.0
				# print ONDAY,' in combination of common'
				
				MTD_CUMM=form.getlist('MTD_CUMM')	
				YTD_CUMM=form.getlist('YTD_CUMM')
				remarks=form.getlist('Remarks')
				consolidate_details=[]
				for jjjj in range(len(descriptions_list)):
					new_date_of_report=datetime.datetime.strptime(date_of_report,'%d-%m-%Y')
					if new_date_of_report.date().day ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
						if new_date_of_report.date()==datetime.datetime(datetime.datetime.now().year,1,1):
							MTD_N=0.0							
							ONDAY_N=float(ONDAY[jjjj])
							YTD_N=0.0
							if descriptions_list[jjjj]=='Power consumption in units/MT':
								MTD_CUMM_N=0.0
								YTD_CUMM_N=0
						else:
							MTD_N=0.0
							
							ONDAY_N=float(ONDAY[jjjj])
							YTD_N=float(YTD[jjjj])
							if descriptions_list[jjjj]=='Power consumption in units/MT':
								MTD_CUMM_N=0.0
								YTD_CUMM_N=float(YTD_CUMM[jjjj])
								print 'in line 464',MTD_CUMM_N						
					else:
						ONDAY_N=float(ONDAY[jjjj])
						MTD_N=float(MTD[jjjj])
						YTD_N=float(YTD[jjjj])
						if descriptions_list[jjjj]=='Power consumption in units/MT':
							MTD_CUMM_N=float(MTD_CUMM[jjjj])
							YTD_CUMM_N=float(YTD_CUMM[jjjj])
						# print Booster_MTD,'------------'
							print 'inline 473',MTD_CUMM_N
					if form['submit']=='Submit':
						pass
					elif form['submit']=='Save Details':
						ONDAY_N=float(ONDAY[jjjj])
						MTD_N=float(MTD[jjjj])
						YTD_N=float(YTD[jjjj])
						if descriptions_list[jjjj]=='Power consumption in units/MT':
							MTD_CUMM_N=MTD_CUMM_N
							YTD_CUMM_N=YTD_CUMM_N

					if descriptions_list[jjjj]=='Power consumption in units/MT':
						consolidate_details.append({'equipment':equipments_list[jjjj],'description':descriptions_list[jjjj],"date" : "date","year" : "year",
							"ONDAY":ONDAY_N,"MTD":MTD_N,"YTD":YTD_N,"MTD_CUMM":MTD_CUMM_N,"YTD_CUMM":YTD_CUMM_N,
					"remarks" : remarks[jjjj],
							})
					else:
						consolidate_details.append({'equipment':equipments_list[jjjj],'description':descriptions_list[jjjj],"date" : "date","year" : "year",
							"ONDAY":ONDAY_N,"MTD":MTD_N,"YTD":YTD_N,
					"remarks" : remarks[jjjj],
							})
				if form['submit']=='Save Details':
					update_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report},{'$set':{'equipment_details.'+form['equip_category']:consolidate_details,'status':'saved','saved_time':datetime.datetime.now()}})
					flash('Details Of Equipment Type - '+form['equip_category']+' Updated Sucessfully.','alert-success')
		elif 'submit' in form  and form['submit']=='Submit':
			# if form['equip_category'] in ['Drilling','Loading','Transportation','Crusher','Dozing','Others']:
			saved_details=find_and_filter('DMR_Operations_data',{'status':'to_be_verified','plant':user_plant,'Date':form['date_of_report']},{'_id':0,'equipment_details':1,'plant':1,'Date':1,'status':1})
			if saved_details:
				test_data=saved_details[0]['equipment_details']
				for eqip in ['Drilling','Loading','Transportation','Crusher','Dozing','Others','Blasting','Excavator_Loading','Transportation_Dumper','Production']:
					if eqip not in test_data:
						# return jsonify({'prev_day_exists':'0','results':'test','result':'Previous Day Details Are Not Filled.Please Fill The Previous Day Details First.'})
						flash( 'Please save '+eqip+' Equipment Type values and submit the details.','alert-danger')
						return redirect(url_for('zenreports.Mines_Report'))
				if 'levels' not in saved_details[0]:
					levels=[]
					plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"chief_pid":1,"_id":0})
					dept_cheif_pid=find_and_filter("departmentmaster",{"plant_id":plant_pid[0]['plant_id1'],"dept":'MINES'},{"dept_chief_pid":1,"_id":0})
					dept_chief_Eid=find_and_filter('Userinfo',{"designation_id":dept_cheif_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
					plant_chief_Eid=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
					if dept_cheif_pid and dept_chief_Eid and plant_chief_Eid:						
						levels.append({"a_status":"current","a_id":dept_chief_Eid[0]['username'],"a_time":"None"})						
						levels.append({"a_status":"waiting","a_id":plant_chief_Eid[0]['username'],"a_time":"None"})
						update_collection('DMR_Operations_data',{'plant':user_details["plant_id"],'Date':date_of_report},{'$set':{'levels':levels}})
					else:
						flash('Could Not Submit The Data As Dept Chief/Plant Chief Data Is Not Available In Department Master/Userinfo.Please Consult IT-Team.','alert-danger')
						logging.info('Could Not Submit The Data As Dept Chief/Plant Chief Data Is Not Available In Department Master/Userinfo.Please Consult IT-Team.')
						# logging.info('Dept Chief - '+str(dept_chief_Eid[0]['username'])+' ,Plant Chief -'+str(plant_chief_Eid[0]['username']))
						return redirect(url_for('zenreports.Mines_Report'))
				if debug_flag=='on':
					logging.info('----- '+'adding approval levels to saved data'+' --- @ '+str(datetime.datetime.now()))
				update_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report},{'$set':{'status':'Submitted','submit_time':datetime.datetime.now()}})
				flash('Daily Mines Operation Report is submitted for the day '+date_of_report+' .','alert-success')
				notification(current_user.username,'Daily Mines Operation Report is submitted for the day '+date_of_report+' .')
				notification(dept_chief_Eid[0]['username'],'Daily Mines Operation Report is submitted for day'+date_of_report+' and needs approval')
			else:
				logging.info('No Data is available to save for date '+form['date_of_report']+' .please save the individual data and then submit.')
				# print 'No Data is available to save for date '+form['date_of_report']+' .please save the individual data and then submit.'
		elif 'submit' in form and form['submit']=='Save & Verify':
			if form['equip_category'] in ['Production']:				
				date_of_report=form['date_of_report']
				crusher_data=find_one_in_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report})
				if crusher_data:
					crusher_onday=crusher_data['equipment_details']['Crusher'][0]['running_hours']['ONDAY']
					# crusher_mtd=crusher_data['equipment_details']['Crusher'][0]['running_hours']['MTD']
					# crusher_ytd=crusher_data['equipment_details']['Crusher'][0]['running_hours']['YTD']
				else:
					crusher_onday=0
					# crusher_mtd=0
					# crusher_ytd=0
				equipments_list=form.getlist('equipment')
				descriptions_list=form.getlist('description')  
				ONDAY=form.getlist('ONDAY')
				MTD=form.getlist('MTD')	
				YTD=form.getlist('YTD')
				if crusher_onday >0:
					ONDAY[3]=round(float(ONDAY[1])/crusher_onday,2) # limestone in MT/Cruhser RH
				else:
					ONDAY[3]=0
				# try:
				# 	MTD[3]=round(float(MTD[1])/crusher_mtd,2)
				# except:
				# 	MTD[3]=0.0
				# try:
				# 	YTD[3]=round(float(YTD[1])/crusher_mtd,2)
				# except:
				# 	YTD[3]=0.0
					
				MTD_CUMM=form.getlist('MTD_CUMM')	
				YTD_CUMM=form.getlist('YTD_CUMM')
				remarks=form.getlist('Remarks')
				consolidate_details=[]
				for jjjj in range(len(descriptions_list)):
					new_date_of_report=datetime.datetime.strptime(date_of_report,'%d-%m-%Y')
					if new_date_of_report.date().day ==1 and new_date_of_report.date().year ==datetime.datetime.now().year:
						if new_date_of_report.date()==datetime.datetime(datetime.datetime.now().year,1,1):
							MTD_N=0.0							
							ONDAY_N=float(ONDAY[jjjj])
							YTD_N=0.0
							if descriptions_list[jjjj]=='Power consumption in units/MT':
								MTD_CUMM_N=0.0
								YTD_CUMM_N=0
						else:
							MTD_N=0.0							
							ONDAY_N=float(ONDAY[jjjj])
							YTD_N=float(YTD[jjjj])
							if descriptions_list[jjjj]=='Power consumption in units/MT':
								MTD_CUMM_N=0.0
								YTD_CUMM_N=float(YTD_CUMM[jjjj])
						# print '1111111111-----------------------'						
					else:
						ONDAY_N=float(ONDAY[jjjj])
						MTD_N=float(MTD[jjjj])
						YTD_N=float(YTD[jjjj])
						if descriptions_list[jjjj]=='Power consumption in units/MT':
							MTD_CUMM_N=float(MTD_CUMM[jjjj])
							YTD_CUMM_N=float(YTD_CUMM[jjjj])
						# print Booster_MTD,'------------'
					if form['submit']=='Submit':
						pass
					elif form['submit']=='Save Details':
						ONDAY_N=float(ONDAY[jjjj])
						MTD_N=float(MTD[jjjj])
						YTD_N=float(YTD[jjjj])
						if descriptions_list[jjjj]=='Power consumption in units/MT':
							MTD_CUMM_N=MTD_CUMM_N
							YTD_CUMM_N=YTD_CUMM_N

					if descriptions_list[jjjj]=='Power consumption in units/MT':
						consolidate_details.append({'equipment':equipments_list[jjjj],'description':descriptions_list[jjjj],"date" : "date","year" : "year",
							"ONDAY":ONDAY_N,"MTD":MTD_N,"YTD":YTD_N,"MTD_CUMM":MTD_CUMM_N,"YTD_CUMM":YTD_CUMM_N,
					"remarks" : remarks[jjjj],
							})
					else:
						consolidate_details.append({'equipment':equipments_list[jjjj],'description':descriptions_list[jjjj],"date" : "date","year" : "year",
							"ONDAY":ONDAY_N,"MTD":MTD_N,"YTD":YTD_N,
					"remarks" : remarks[jjjj],
							})
						
				if form['submit']=='Save & Verify':
					update_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report},{'$set':{'equipment_details.'+form['equip_category']:consolidate_details,'status':'saved','saved_time':datetime.datetime.now()}})
					# flash('Details Of Equipment Type - '+form['equip_category']+' Updated Sucessfully And Verify Total Data For Submission.','alert-success')
			if debug_flag=='on':
				logging.info('----- '+str(form)+' --- @ '+str(datetime.datetime.now()))		
			date_of_report=form['date_of_report']
			saved_details=find_and_filter('DMR_Operations_data',{'plant':user_plant,'Date':form['date_of_report']},{'_id':0,'equipment_details':1,'plant':1,'Date':1,'status':1})
			equipment_details=saved_details[0]['equipment_details']
			update_collection('DMR_Operations_data',{'plant':user_plant,'Date':date_of_report},{'$set':{'status':'to_be_verified','to_be_verified_time':datetime.datetime.now()}})
			return render_template('DMR_EntryForm_verify.html',user_details=user_details,equipment_details=equipment_details,Date=form['date_of_report'])
		elif 'submit' in form and form['submit']=='Back':
			if debug_flag=='on':
				logging.info('----- '+str(form)+' --- @ '+str(datetime.datetime.now()))
			return redirect(url_for('zenreports.Mines_Report'))
		if prev_date:
			return render_template('DMR_Entryform.html',user_details=user_details,equipment_details=equipment_details,min_date=min_date,max_date=max_date,status='prev')
		else:
			return render_template('DMR_Entryform.html',user_details=user_details,equipment_details=equipment_details,status='no_prev')
	# return render_template('daily_mines_report.html',user_details=user_details,equipment_details=equipment_details)
	if prev_date:
		return render_template('DMR_Entryform.html',user_details=user_details,equipment_details=equipment_details,min_date=min_date,max_date=max_date,status='prev')
	else:
		return render_template('DMR_Entryform.html',user_details=user_details,equipment_details=equipment_details,status='no_prev')

@zen_reports.route('/Mines_Report_History/', methods=['GET', 'POST'])
@login_required
def Mines_Report_History():
	user_details = base()
	doc_history=find_in_collection_sort('DMR_Operations_data',{'$or':[{'status':'Submitted'},{'status':'Approved'}],'employee_id':current_user.username,'plant':user_details["plant_id"]},[("_id",-1)])
	if request.method=='POST':
		form=request.form
		if form['submit']=='VIEW':
			saved_doc=find_and_filter('DMR_Operations_data',{'_id':ObjectId(form['prid'])},{"_id":1,"levels":1,"equipment_details":1,"employee_id":1,"Date":1,'plant':1,'status':1})
			filename='DMR-'+saved_doc[0]['plant']+'-'+datetime.datetime.strptime(saved_doc[0]['Date'],'%d-%m-%Y').strftime('%d%m%Y')
			return render_template('DMR_submitted_view.html',filename=filename,user_details=user_details,report_status=saved_doc[0]['status'],equipment_details=saved_doc[0]['equipment_details'],Date=saved_doc[0]['Date'],doc_plant=saved_doc[0]['plant'],doc_id=saved_doc[0]['_id'])
	# for i in doc_history:
	# 	print i['Date']
	return render_template('DMR_submitted_history.html',status='submit_history',user_details=user_details,app_docs=doc_history)

@zen_reports.route('/Mines_Report_HO/', methods=['GET', 'POST'])
@login_required
def Mines_Report_HO():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	doc_history=find_in_collection_sort('DMR_Operations_data',{'$or':[{'status':'Approved'}],},[("_id",-1)])
	# doc_history=find_in_collection_sort('DMR_Operations_data',{'$or':[{'status':'Submitted'},{'status':'Approved'}],'employee_id':current_user.username,'plant':user_details["plant_id"]},[("_id",-1)])
	# doc_history=db.DMR_Operations_data.aggregate([{'$match' :{"status":"Approved",'plant':user_details["plant_id"],'employee_id':{'$exists':1}}},
	# 	{ '$unwind': '$levels'},
 #         {'$match' : {'levels.a_id' : uid,'$or':[{'levels.a_status':'approved'}]}},
 #         { '$sort' : { '_id' : -1} }])	
	# doc_history=list(doc_history)
	if request.method=='POST':
		form=request.form
		if form['submit']=='VIEW':
			saved_doc=find_and_filter('DMR_Operations_data',{'_id':ObjectId(form['prid'])},{"_id":1,"levels":1,"equipment_details":1,"employee_id":1,"Date":1,'plant':1,'status':1})
			filename='DMR-'+saved_doc[0]['plant']+'-'+datetime.datetime.strptime(saved_doc[0]['Date'],'%d-%m-%Y').strftime('%d%m%Y')
			return render_template('DMR_submitted_view.html',filename=filename,user_details=user_details,report_status=saved_doc[0]['status'],equipment_details=saved_doc[0]['equipment_details'],Date=saved_doc[0]['Date'],doc_plant=saved_doc[0]['plant'],doc_id=saved_doc[0]['_id'])
	return render_template('DMR_submitted_history.html',status='approval_history',user_details=user_details,app_docs=doc_history,user_type='HO')

@zen_reports.route('/DMR_Approval_History/', methods=['GET', 'POST'])
@login_required
def DMR_Approval_History():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	# doc_history=find_in_collection_sort('DMR_Operations_data',{'$or':[{'status':'Submitted'},{'status':'Approved'}],'employee_id':current_user.username,'plant':user_details["plant_id"]},[("_id",-1)])
	doc_history=db.DMR_Operations_data.aggregate([{'$match' :{"status":"Approved",'plant':user_details["plant_id"],'employee_id':{'$exists':1}}},
		{ '$unwind': '$levels'},
         {'$match' : {'levels.a_id' : uid,'$or':[{'levels.a_status':'approved'}]}},
         { '$sort' : { '_id' : -1} }])	
	doc_history=list(doc_history)
	if request.method=='POST':
		form=request.form
		if form['submit']=='VIEW':
			saved_doc=find_and_filter('DMR_Operations_data',{'_id':ObjectId(form['prid'])},{"_id":1,"levels":1,"equipment_details":1,"employee_id":1,"Date":1,'plant':1,'status':1})
			filename='DMR-'+saved_doc[0]['plant']+'-'+datetime.datetime.strptime(saved_doc[0]['Date'],'%d-%m-%Y').strftime('%d%m%Y')
			return render_template('DMR_submitted_view.html',filename=filename,user_details=user_details,report_status=saved_doc[0]['status'],equipment_details=saved_doc[0]['equipment_details'],Date=saved_doc[0]['Date'],doc_plant=saved_doc[0]['plant'],doc_id=saved_doc[0]['_id'])
	return render_template('DMR_submitted_history.html',status='approval_history',user_details=user_details,app_docs=doc_history)

def generate_excel_report(doc_obj_id):
	doc_details=find_one_in_collection('DMR_Operations_data',{'_id':ObjectId(doc_obj_id)})
	doc_path=find_one_in_collection('portal_settings',{'config':'DMR_PATH'})
	plants_desc={'TC':'TALARICHERUVU','BP':'BOYAREDDYPALLI','GC':'GANESHPAHAD CEMENT','TA':'TANDUR'}
	test_data=doc_details['equipment_details']
	row=1
	data=[]
	row=1

	doc_name='DMR-'+doc_details['plant']+'-'+datetime.datetime.strptime(doc_details['Date'],'%d-%m-%Y').strftime('%d%m%Y')
	workbook = xlsxwriter.Workbook(doc_path['directory']+doc_name+'.xlsx')
	worksheet = workbook.add_worksheet()
	bold = workbook.add_format({'bold': True,'border': 1})
	style_title_main=workbook.add_format({'font_color':'black','align': 'center','bold': True,'font_size':13})
	style_title_report=workbook.add_format({'font_color':'black','align': 'center','bold': True,'font_size':15})
	style_title=workbook.add_format({'font_color':'black','align': 'center','bold': True})
	style_subtitle=workbook.add_format({'font_color':'black','bold': True,'font_size':12})
	data_style=workbook.add_format({'border': 1})
	style_sub=workbook.add_format({'font_color':'black','bg_color':'#AED581'})
	style_date=workbook.add_format({'align': 'right'})
	style_date1=workbook.add_format({'align': 'left'})
	worksheet.merge_range(row,0,row,11,'Penna Cement Industries Limited',style_title_main)
	row+=1

	worksheet.merge_range(row,0,row,11,'Daily Mines Operation Report',style_title_report)
	row+=2
	report_date=doc_details['Date']
	report_plant=plants_desc[doc_details['plant']]
	worksheet.merge_range(row,0,row,11,'Date : '+report_date,style_subtitle)
	row+=1
	worksheet.merge_range(row,0,row,11,'Plant : '+report_plant,style_subtitle)
	row+=2
	headings=['Equipment','Description','Running Hours(On Day)','Running Hours(MTD)','Running Hours(YTD)','Breakdown Hours(On Day)','Breakdown Hours(MTD)','Breakdown Hours(YTD)','Ideal Hours(On Day)','Ideal Hours(MTD)','Ideal Hours(YTD)','Remarks']
	headings_others=['Equipment','Description','Running Hours/Kms(On Day)','Running Hours/Kms(MTD)','Running Hours/Kms(YTD)','Breakdown Hours/Kms(On Day)','Breakdown Hours/Kms(MTD)','Breakdown Hours/Kms(YTD)','Ideal Hours/Kms(On Day)','Ideal Hours/Kms(MTD)','Ideal Hours/Kms(YTD)','Remarks']
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Drilling',style_sub)
	row+=1
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1      
	for j in test_data['Drilling']:
	    data=[j['equipment'],j['description'],j['running_hours']['ONDAY'],j['running_hours']['MTD'],j['running_hours']['YTD'],j['breakdown_hours']['ONDAY'],j['breakdown_hours']['MTD'],j['breakdown_hours']['YTD'],j['ideal_hours']['ONDAY'],j['ideal_hours']['MTD'],j['ideal_hours']['YTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type DRILLING is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Loading',style_sub)
	row+=1
	fields={'Equipment':'equipment','Description':'description','Running Hours Onday':'RH_ONDAY','Running hours MTD':'RH_MTD','Running hours YTD':'RH_YTD','Breakdown Hours Onday':'BDH_ONDAY','Breakdown hours MTD':'BDH_MTD','Breakdown hours YTD':'BDH_YTD','Ideal Hours Onday':'IDH_ONDAY','Ideal hours MTD':'IDH_MTD','Ideal hours YTD':'IDH_YTD','Remarks':'remarks'}
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1
	for j in test_data['Loading']: 
	    data=[j['equipment'],j['description'],j['running_hours']['ONDAY'],j['running_hours']['MTD'],j['running_hours']['YTD'],j['breakdown_hours']['ONDAY'],j['breakdown_hours']['MTD'],j['breakdown_hours']['YTD'],j['ideal_hours']['ONDAY'],j['ideal_hours']['MTD'],j['ideal_hours']['YTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type LOADING is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Transportation',style_sub)
	row+=1
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1
	for j in test_data['Transportation']:
	    data=[j['equipment'],j['description'],j['running_hours']['ONDAY'],j['running_hours']['MTD'],j['running_hours']['YTD'],j['breakdown_hours']['ONDAY'],j['breakdown_hours']['MTD'],j['breakdown_hours']['YTD'],j['ideal_hours']['ONDAY'],j['ideal_hours']['MTD'],j['ideal_hours']['YTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type TRANSPORTATION is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Crusher',style_sub)
	row+=1
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1
	for j in test_data['Crusher']:
	    data=[j['equipment'],j['description'],j['running_hours']['ONDAY'],j['running_hours']['MTD'],j['running_hours']['YTD'],j['breakdown_hours']['ONDAY'],j['breakdown_hours']['MTD'],j['breakdown_hours']['YTD'],j['ideal_hours']['ONDAY'],j['ideal_hours']['MTD'],j['ideal_hours']['YTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type CRUSHER is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Dozing',style_sub)
	row+=1
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1
	for j in test_data['Dozing']:
	    data=[j['equipment'],j['description'],j['running_hours']['ONDAY'],j['running_hours']['MTD'],j['running_hours']['YTD'],j['breakdown_hours']['ONDAY'],j['breakdown_hours']['MTD'],j['breakdown_hours']['YTD'],j['ideal_hours']['ONDAY'],j['ideal_hours']['MTD'],j['ideal_hours']['YTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info( 'Equipment type DOZING is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Other Equipments',style_sub)
	row+=1
	for k in range(len(headings)):
	    worksheet.write(row,k,headings_others[k],bold)
	row+=1
	for j in test_data['Others']:
	    data=[j['equipment'],j['description'],j['running_hours']['ONDAY'],j['running_hours']['MTD'],j['running_hours']['YTD'],j['breakdown_hours']['ONDAY'],j['breakdown_hours']['MTD'],j['breakdown_hours']['YTD'],j['ideal_hours']['ONDAY'],j['ideal_hours']['MTD'],j['ideal_hours']['YTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type OTHERS is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Blasting',style_sub)
	row+=1
	headings=['Equipment','Description',"Total Consumption(On Day)","Total Consumption(MTD)",'Remarks']

	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1
	for j in test_data['Blasting']:
	    data=[j['equipment'],j['description'],j['consumption']['ONDAY'],j['consumption']['MTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	worksheet.write(row,0,'Powder Factor(MT/KG)')
	try:
		worksheet.write(row,1,round(test_data['Blasting'][1]['consumption']['ONDAY']/test_data['Blasting'][0]['consumption']['ONDAY'],2))
	except:
		worksheet.write(row,1,0)
		logging.info('Blasted Quantity Exception zero division error')
	# print test_data['Blasting'][1]/test_data['Blasting'][0],' powder factor'
	row+=2
	logging.info('Equipment type BLASTING is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Loading Excavator',style_sub)
	row+=1

	headings=['Equipment','Description','Limestone','Laterite','Clay','Waste','Misc','Trips(On Day)','Trips(MTD)','Remarks']
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1
	for j in test_data['Excavator_Loading']:
	    data=[j['equipment'],j['description'],j['Limestone']['ONDAY'],j['Laterite']['ONDAY'],j['Clay']['ONDAY'],j['Waste']['ONDAY'],j['Misc']['ONDAY'],j['Trips_OD'],j['Trips_MTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type EXCAVATOR LOADING is completed')
	#---------------------------------------------------------------------------
	worksheet.merge_range(row,0,row,11,'Loading Dumper',style_sub)
	row+=1
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1
	for j in test_data['Transportation_Dumper']:
	    data=[j['equipment'],j['description'],j['Limestone']['ONDAY'],j['Laterite']['ONDAY'],j['Clay']['ONDAY'],j['Waste']['ONDAY'],j['Misc']['ONDAY'],j['Trips_OD'],j['Trips_MTD'],j['remarks']]
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type TRANSPORTATION DUMPER is completed')
	#---------------------------------------------------------------------------
	headings=['Description','Onday','MTD','YTD','Remarks']
	worksheet.merge_range(row,0,row,11,'Production Details',style_sub)
	row+=1
	for k in range(len(headings)):
	    worksheet.write(row,k,headings[k],bold)
	row+=1

	for j in test_data['Production']:
	    data=[j['description'],j['ONDAY'],j['MTD'],j['YTD'],j['remarks']]    
	    for jj in range(len(data)):
	        worksheet.write(row,jj,data[jj],data_style)
	    row+=1
	row+=2
	logging.info('Equipment type PRODUCTION is completed')
	    
	row+=3
	workbook.close()
	return 'Success'

def get_full_name(userid):
	userdetails=find_one_in_collection('Userinfo',{'username':userid})
	name=userdetails['f_name']+" "+userdetails['l_name']
	email=userdetails['official_email']
	result={'name':name,'email':email}
	return result

def DMR_send_mail(receivers_list,filename,DMR_date):
	curr_user=current_user.username
	Name=get_full_name(curr_user)['name']
	receiver=get_full_name(curr_user)['email']
	# developer='portalsupport1@pennacement.com'
	form=request.form
	receivers=receivers_list
	# receivers=receivers.strip().split(',')
	path=os.path.abspath("/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/daily_mines_report/"+filename+".xlsx")
	for r in receivers:
		USERNAME='myportal@pennacement.com'
		PASSWORD='Penna@123'
		RECEIVER=receiver
		msg = MIMEMultipart()
		msg['Subject'] = "*** This is a system generated email, please do not reply ***"
		# msg['preamble']='testing msg'
		server = smtplib.SMTP("smtp.office365.com:587")
		server.starttls()
		server.login(USERNAME,PASSWORD)

		body = email.mime.Text.MIMEText("Daily Mines Report Dated - "+DMR_date+" .")
		msg.attach(body)

		filename=filename+'.xlsx'
		fp=open(path,'rb')
		att = email.mime.application.MIMEApplication(fp.read(),_subtype="xlsx")
		fp.close()
		att.add_header('Content-Disposition','attachment',filename=filename)
		msg.attach(att)
		try:
			server.sendmail(USERNAME,r,msg.as_string())
			server.quit()
			R="success"
		except Exception,R:
			print R
	logging.info("Daily Mines Report Dated - "+DMR_date+"  @ " +str(datetime.datetime.now()))
	result={'msg':R}
	return jsonify(result)

@zen_reports.route('/Mines_Report_Approval/', methods=['GET', 'POST'])
@login_required
def Mines_Report_Approval():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	user_plant=user_details["plant_id"]
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
	basedetails=find_one_in_collection('portal_settings',{'config':'DMR_basevalues'})
	# app_docs=find_and_filter('DMR_Operations_data',{},{"_id":1,"approval_levels":1,"equipment_details":1,"employee_id":1,"Date":1,'plant':1})
	app_docs=db.DMR_Operations_data.aggregate([{'$match' :{"status":"Submitted",'plant':user_details["plant_id"],'employee_id':{'$exists':1}}},
		{ '$unwind': '$levels' },
         {'$match' : {'levels.a_id' : uid,'$or':[{'levels.a_status':'current'},{'levels.a_status':'waiting'}]}}])	
	app_docs=list(app_docs)
	if request.method=='POST':
		form=request.form
		if form['submit']=='VIEW':
			# print 'in view',form
			# print app_docs[0]['Date']
			saved_doc=find_and_filter('DMR_Operations_data',{'_id':ObjectId(form['prid'])},{"_id":1,"levels":1,"equipment_details":1,"employee_id":1,"Date":1,'plant':1})
			return render_template('DMR_approval_view_doc.html',user_details=user_details,equipment_details=saved_doc[0]['equipment_details'],Date=saved_doc[0]['Date'],doc_plant=saved_doc[0]['plant'],doc_id=saved_doc[0]['_id'])
		elif 'submit' in form and form['submit']=='Approve':
			# print 'in Approve'
			a_levels = find_one_in_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])})
			count_a_levels=len(a_levels['levels'])
			updt_var=''			
			for i in range(0,count_a_levels):
				if a_levels['levels'][i]['a_status']=="current" and a_levels['levels'][i]['a_id']==current_user.username:
					j=i
					if j==count_a_levels-1:
						array3={"levels."+str(i)+".a_status":"approved","levels."+str(i)+".a_time":datetime.datetime.now(),"status":"Approved",'Approved_time':datetime.datetime.now()}
						for i in a_levels['equipment_details'].keys():    # loading
							row=0
							if i in ['Drilling','Loading','Transportation','Crusher','Dozing','Others']:
								for j in a_levels['equipment_details'][i]:
									ID_ONDAY=j['ideal_hours']['ONDAY']
									ID_MTD=j['ideal_hours']['MTD']+j['ideal_hours']['ONDAY']
									ID_YTD=j['ideal_hours']['YTD']+j['ideal_hours']['ONDAY']

									BDH_ONDAY=j['breakdown_hours']['ONDAY']
									BDH_MTD=j['breakdown_hours']['ONDAY']+j['breakdown_hours']['MTD']
									BDH_YTD=j['breakdown_hours']['ONDAY']+j['breakdown_hours']['YTD']

									RH_ONDAY=j['running_hours']['ONDAY']
									RH_MTD=j['running_hours']['ONDAY']+j['running_hours']['MTD']
									RH_YTD=j['running_hours']['ONDAY']+j['running_hours']['YTD']

									consolidate_details={'equipment':j['equipment'],'description':j['description'],"date" : "date","year" : "year",
									"running_hours" : {"ONDAY" :RH_ONDAY,"MTD" :RH_MTD,"YTD" :RH_YTD},
									"breakdown_hours" : {"ONDAY" :BDH_ONDAY,"MTD" :BDH_MTD,"YTD" :BDH_YTD},
									"ideal_hours" : {"ONDAY" :ID_ONDAY,"MTD" :ID_MTD,"YTD" :ID_YTD},
									'remarks':j['remarks']}
									update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set':{'equipment_details.'+i+'.'+str(row):consolidate_details,'status':'Submitted','submit_time':datetime.datetime.now()}})
									row+=1
								logging.info('Updated sucessfully '+str(i))
								pass
							elif i in ['Blasting']:
								row2=0
								for j1 in a_levels['equipment_details'][i]:
									# Booster_ONDAY=j1['Booster']['ONDAY']
									# Booster_MTD=j1['Booster']['MTD']+j1['Booster']['ONDAY']

									# Columcharge_ONDAY=j1['Columcharge']['ONDAY']
									# Columcharge_MTD=j1['Columcharge']['MTD']+j1['Columcharge']['ONDAY']

									# ANFO_ONDAY=j1['ANFO']['ONDAY']
									# ANFO_MTD=j1['ANFO']['MTD']+j1['ANFO']['ONDAY']

									Consumption_ONDAY=j1['consumption']['ONDAY']
									Consumption_MTD=Consumption_ONDAY+j1['consumption']['MTD']

									consolidate_details={'equipment':j1['equipment'],'description':j1['description'],"date" : "date","year" : "year",
						# "Booster" : {"ONDAY" :Booster_ONDAY ,"MTD" :Booster_MTD},
						# "Columcharge" : {"ONDAY":Columcharge_ONDAY,"MTD" :Columcharge_MTD},
			   #      	"ANFO" : {"ONDAY":ANFO_ONDAY,"MTD" :ANFO_MTD},
			        	'consumption':{"ONDAY":Consumption_ONDAY,"MTD":Consumption_MTD},
			        	"remarks" : j1['remarks'],
								}
									update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set':{'equipment_details.'+i+'.'+str(row2):consolidate_details,'status':'Submitted','submit_time':datetime.datetime.now()}})
									row2+=1
								logging.info('updated blasting successfully')
								pass
							elif i in ['Excavator_Loading','Transportation_Dumper']:
								row3=0
								for j2 in a_levels['equipment_details'][i]:
									Limestone_ONDAY=j2['Limestone']['ONDAY']
									Laterite_ONDAY=j2['Laterite']['ONDAY']
									Clay_ONDAY=j2['Clay']['ONDAY']
									Waste_ONDAY=j2['Waste']['ONDAY']
									Misc_ONDAY=j2['Misc']['ONDAY']
									Trips_ONDAY=Limestone_ONDAY+Laterite_ONDAY+Clay_ONDAY+Waste_ONDAY+Misc_ONDAY
									Trips_MTD=j2['Trips_MTD']+Trips_ONDAY

									consolidate_details={'equipment':j2['equipment'],'description':j2['description'],"date" : "date","year" : "year",
							"Limestone" : {"ONDAY" :Limestone_ONDAY},
							"Laterite" : {"ONDAY" :Laterite_ONDAY},
							"Clay" : {"ONDAY" :Clay_ONDAY},
							"Waste" : {"ONDAY" :Waste_ONDAY},
							"Misc" : {"ONDAY" :Misc_ONDAY},
							"Trips_OD" : Trips_ONDAY,
							"Trips_MTD" :Trips_MTD ,
							"remarks" : j2['remarks'],
									}
									update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set':{'equipment_details.'+i+'.'+str(row3):consolidate_details,'status':'Submitted','submit_time':datetime.datetime.now()}})
									row3+=1
								logging.info('updated E_L_D successfully')
								pass
							elif i in ['Production']:
								row4=0
								crusher_mtd=0
								crusher_ytd=0
								crusher_data=find_one_in_collection('DMR_Operations_data',{'plant':user_plant,'Date':a_levels['Date']})
								if crusher_data:
									# crusher_onday=crusher_data['equipment_details']['Crusher'][0]['running_hours']['ONDAY']
									# print crusher_data['equipment_details']['Crusher'][0],' crusher data'
									crusher_mtd=crusher_data['equipment_details']['Crusher'][0]['running_hours']['MTD']+crusher_data['equipment_details']['Crusher'][0]['running_hours']['ONDAY']
									crusher_ytd=crusher_data['equipment_details']['Crusher'][0]['running_hours']['YTD']+crusher_data['equipment_details']['Crusher'][0]['running_hours']['ONDAY']
								for j3 in a_levels['equipment_details'][i]:
									# print j3,' j3 description'
									ONDAY=j3['ONDAY']
									# limestone_mtd=0
									# limestone_ytd=0
									# MTD=j3['MTD']+j3['ONDAY']
									# YTD=j3['YTD']+j3['ONDAY']
									if j3['description']=="Limestone production in MT":
										MTD=j3['MTD']+j3['ONDAY']
										YTD=j3['YTD']+j3['ONDAY']
										limestone_mtd=MTD
										limestone_ytd=YTD
									elif j3["description"]=="Crusher TPH":
										try:
											MTD=round((limestone_mtd/crusher_mtd),2)
										except:
											MTD=0.0
										try:
											YTD=round((limestone_ytd/crusher_ytd),2)
										except:
											YTD=0.0
									elif j3["description"]=="Power consumption in units/MT":
										# baseday=1
										# basemonth=6
										baseday=basedetails['base_day']
										basemonth=basedetails['base_month']
										if int(a_levels['Date'].split('-')[0])==1:
											if int(a_levels['Date'].split('-')[0])==1 and int(a_levels['Date'].split('-')[1])==1:
												MTD_CUMM=j3['ONDAY']
												no_of_days_entered=int(a_levels['Date'].split('-')[0]) # base day
												MTD=j3['ONDAY']/no_of_days_entered
												YTD_CUMM=j3['ONDAY']
												no_of_months_entered=int(a_levels['Date'].split('-')[1]) #base month
												YTD=YTD_CUMM/no_of_months_entered
												# print YTD,': YTD AVG - ', MTD ,' : MTD AVG'
											else:												
												MTD_CUMM=j3['ONDAY']
												no_of_days_entered=int(a_levels['Date'].split('-')[0]) # base day
												MTD=j3['ONDAY']/no_of_days_entered
												MTD=round(MTD,2)
												YTD_CUMM=j3['YTD_CUMM']+j3['ONDAY']
												no_of_months_entered=int(a_levels['Date'].split('-')[1])-basemonth+1 #base month
												YTD=YTD_CUMM/no_of_months_entered
												YTD=round(YTD,2)
												# print YTD,': YTD AVG - ', MTD ,' : MTD AVG'
										else:											
											MTD_CUMM=j3['MTD_CUMM']+j3['ONDAY']
											print MTD_CUMM,' MTD cumulative'
											no_of_days_entered=int(a_levels['Date'].split('-')[0])-baseday+1 # base day
											MTD=MTD_CUMM/no_of_days_entered
											YTD_CUMM=j3['YTD_CUMM']+j3['ONDAY']											
											no_of_months_entered=int(a_levels['Date'].split('-')[1])-basemonth+1 #base month
											YTD=YTD_CUMM/no_of_months_entered
											# print YTD,': YTD AVG - ', MTD ,' : MTD AVG'
									else:
										MTD=j3['MTD']+j3['ONDAY']
										YTD=j3['YTD']+j3['ONDAY']

									if j3["description"]=="Power consumption in units/MT":
										consolidate_details={'equipment':j3['equipment'],'description':j3['description'],"date" : "date","year" : "year",
										"ONDAY":ONDAY,"MTD":MTD,"YTD":YTD,"MTD_CUMM":MTD_CUMM,"YTD_CUMM":YTD_CUMM,
								"remarks" : j3['remarks'],
										}
									else:
										consolidate_details={'equipment':j3['equipment'],'description':j3['description'],"date" : "date","year" : "year",
										"ONDAY":ONDAY,"MTD":MTD,"YTD":YTD,
								"remarks" : j3['remarks'],										
										}
										# if j3['description']=='Crusher TPH':
										# 	# print consolidate_details
									update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set':{'equipment_details.'+i+'.'+str(row4):consolidate_details,'status':'Submitted','submit_time':datetime.datetime.now()}})
									row4+=1
								pass

						flash('Report Approved Successfully','alert-success')
						notification(a_levels['employee_id'],'Daily Mines Operation Report is approved by Plant Head for day '+a_levels['Date']+' .')
					else:
						array3={"levels."+str(i)+".a_status":"approved","levels."+str(i)+".a_time":datetime.datetime.now(),"levels."+str(i+1)+".a_status":"current"}
						flash('Report Approved Successfully And Sent For Next Level Of Approval.','alert-success')
						notification(a_levels['levels'][1]['a_id'],'Daily Mines Operation Report is approved by HOD for day '+a_levels['Date']+' and needs approval.')
					update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set' :array3})
					Excel_generate=generate_excel_report(form['prid'])
					try:
						if Excel_generate=='Success' and 'DMR_EMAILID' in user_details:
							# Email_status=DMR_send_mail([user_details['DMR_EMAILID']],'MR-'+a_levels['plant']+'-'+datetime.datetime.strptime(a_levels['Date'],'%d-%m-%Y').strftime('%d%m%Y'),a_levels['Date'])
							# print 'Email sent successfully.'
							pass
							update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set' :{'email_status':'success'}})
					except:
						logging.info('Email error '+str(user_details['DMR_EMAILID']))
						update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set' :{'email_status':'error'}})
				else:
					# print 'in else'
					pass
			# print form,' in approve'
			# update_collection('DMR_Operations_data',{'_id':ObjectId(form['prid'])},{'$set':{'status':'Approved','Approved_time':datetime.datetime.now()}})
			# print 'updated'
			return redirect(url_for('zenreports.Mines_Report_Approval'))
		elif 'submit' in form and form['submit']=='Reject':
			a_levels = find_one_in_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])})
			update_collection('DMR_Operations_data',{"_id":ObjectId(form['prid'])},{'$set' :{'status':'saved','reject':'X','rejected_by':current_user.username}})
			notification(a_levels['employee_id'],'Daily Mines Operation Report is rejected for day'+a_levels['Date']+' by Department Head/ Plant Head.')
			flash('Report Has Been Rejected Sucessfully.','alert-danger')
			return redirect(url_for('zenreports.Mines_Report_Approval'))
	return render_template('DMR_approval.html',user_details=user_details,app_docs=app_docs)


################################ End Of DMR ##############################################


################################ Mines Equipment daily status moved on 17th july 2017 ##############################################
#MINING EQUIPMENT ENTERY FORM METHOD
@zen_reports.route('/minisequipmentreport/', methods = ['GET', 'POST'])
@login_required
def minisequipmentreport():
	user_details = base()
	uid = current_user.username
	mtype={"Y","N"}
	db=dbconnect()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id":1,"plant_id1":1,"plant_desc":1,"chief_pid":1,"_id":0})
	
	mm=find_and_filter('Minesequipmentmaster',{"plant_id":plant_pid[0]['plant_id1']},{"_id":0})

	if request.method == 'POST':
		form=request.form
		dept_cheif_pid=find_and_filter("departmentmaster",{"plant_id":plant_pid[0]['plant_id1'],"dept":'MINES'},{"dept_chief_pid":1,"_id":0})
		
		equipment=form.getlist('equipment')
		quipcode=form.getlist('quipcode')
		model=form.getlist('model')
		capacity=form.getlist('capacity')
		condition=form.getlist('condition')
		Remarks=form.getlist('Remarks')
		plant=form.getlist('plant')
		# HO=form.getlist('HO')
		date=form['entry_date']	
		from_date=datetime.datetime.strptime(form['entry_date'], '%d/%m/%Y')
		to_date=datetime.datetime.strptime(form['entry_date'], '%d/%m/%Y')
		
		data=db.Minesdailyreport.aggregate([{'$match' : {"CreateDate":{'$gte':from_date.strftime("%d/%m/%Y"),'$lte':to_date.strftime("%d/%m/%Y")},"plant_id":plant_pid[0]['plant_id1'],"employee_id":uid}},
			{'$unwind': '$equipmentdetails'}])
	
		levels=[]
		dept_chief_Eid=find_and_filter('Userinfo',{"designation_id":dept_cheif_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":dept_chief_Eid[0]['username'],"a_time":"None"})
		plant_Eid=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"waiting","a_id":plant_Eid[0]['username'],"a_time":"None"})
		equipmentdetails=[]
		if form['submit'] == 'Save':
			if(not find_one_in_collection('Minesdailyreport',{"CreateDate":date,"plant_id":plant_pid[0]['plant_id1']})):
				for i in range(0,len(equipment)):
					equipmentdetails.append({"Equipment_Code":quipcode[i],"Equipment_Descp":equipment[i],"Model":model[i],"Capacity":capacity[i],"Condition":condition[i],"Remarks":Remarks[i],"plant_approval":plant[i],"status":'applied'})
				array={"CreateDate":date,"approval_levels":levels,"employee_id":uid,"equipmentdetails":equipmentdetails,"plant_id":plant_pid[0]['plant_id1'],"Saved_Indicator":"Yes","Submit_Indicator":"No"}
				save_collection("Minesdailyreport",array)
				
				msg="Your Records has been saved successfully"
			else:
				print "Same record"
				msg="With same date record already inserted"
			
		else:
			if(not find_one_in_collection('Minesdailyreport',{"CreateDate":date,"plant_id":plant_pid[0]['plant_id1']})):
				for i in range(0,len(equipment)):
					equipmentdetails.append({"Equipment_Code":quipcode[i],"Equipment_Descp":equipment[i],"Model":model[i],"Capacity":capacity[i],"Condition":condition[i],"Remarks":Remarks[i],"plant_approval":plant[i],"status":'applied'})
				array={"CreateDate":date,"approval_levels":levels,"employee_id":uid,"equipmentdetails":equipmentdetails,"plant_id":plant_pid[0]['plant_id1'],"Saved_Indicator":"Yes","Submit_Indicator":"Yes"}
				save_collection("Minesdailyreport",array)
			
				msg="Your Records has been submitted successfully"
			else:
				print "Same record"
				msg="With same date record already inserted"
			
		msg1="Mines Report is waiting for your approval"
		notification(uid,msg)
		notification(dept_chief_Eid[0]['username'],msg1)
		
		flash(msg,'alert-success')
	
	return render_template('mines_daily_performance_entry_form.html', user_details=user_details,plant_desc=plant_pid[0]['plant_id'],mtype=mtype,mm=mm)

#EQUIPMENT MASTER FETCHING FROM MONGODB AND RESPONSE SENDING THROUGH AJAX CALL
@zen_reports.route('/equipment', methods = ['GET', 'POST'])
@login_required
def equipment():
	q = request.args.get('term')
	
	db=dbconnect()
	user_details = base()
	
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	
	equip = db.equipmentmaster.aggregate([
                {'$match' : { "plant":plant_pid[0]['plant_id1'],'$and': [{'$or':[{"Desp_technical_object" : {'$regex': str(q),'$options': 'i' }},{'equipment_number' : {'$regex': str(q),'$options': 'i' }}] }]} }, 
                {'$project' : {'Desp_technical_object':1,'equipment_number':1,"cost_center":1,'functional_loc':1, "_id":0}}])
	return json.dumps({"results":list(equip)})

# MINES EQUIPMENT DAILY PERFORMANCE REPORT GENERATING METHOD ON PLANT WISE  
# Code modified on 17th July 2017
@zen_reports.route('/minesreport', methods = ['GET', 'POST'])
@login_required
def minesreport():
	user_details = base()
	db=dbconnect()
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id":1,"plant_id1":1,"plant_desc":1,"_id":0})
	
	plantid=plant_pid[0]['plant_id1']
	plantname=plant_pid[0]['plant_desc']
	if plantname == 'TALARICHERUVU':
		plantname='Talaricheruvu'
	elif plantname == 'BOYAREDDYPALLI':
		plantname= 'Boyareddypalli'
	elif plantname == 'GANESHPAHAD CEMENT':
		plantname='Ganeshpahad Cement'
	elif plantname == 'TANDUR':
		plantname='Tandur'
	elif plantname == 'GANESHPAHAD POWER':
		plantname='Ganeshpahad Power'
	data=find_in_collection('Minesdailyreport',{'plant_id':plantid,"Submit_Indicator":'No'})

	if request.method == 'POST':
		form=request.form
		uid = current_user.username
		equipcode=form.getlist('quipcode')
		equipdesc=form.getlist('equipment')
		capacity=form.getlist('capacity')
		model=form.getlist('equicode_Model')
		plant=form.getlist('plant')
		condition=form.getlist('condition')
		Remarks=form.getlist('Remarks')
		equipmentdetails=[]
		if form['submit'] == 'Save':
		
			for i in range(0,len(condition)):
				equipmentdetails.append({"Equipment_Code":equipcode[i],"Equipment_Descp":equipdesc[i],"Model":model[i],"Capacity":capacity[i],"Condition":condition[i],"Remarks":Remarks[i],"plant_approval":plant[i],"status":'applied'})
			array={"equipmentdetails":equipmentdetails}
		
			update_coll('Minesdailyreport',{'_id':ObjectId(form['prid'])},{'$set': array})
			msg="Your Records has been saved successfully"
		elif form['submit'] == 'VIEW':
			from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
			to_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
# 		
			fdate=from_date.strftime("%d/%m/%Y")
			tdate=to_date.strftime("%d/%m/%Y")
		
			data=db.Minesdailyreport.aggregate([{'$match' : {"CreateDate":{'$gte':from_date.strftime("%d/%m/%Y"),'$lte':to_date.strftime("%d/%m/%Y")},"plant_id":plantid,"employee_id":uid}},
			{'$unwind': '$equipmentdetails'}])
			data=list(data)
			logging.info('DATA -----------------------------------------')
			app_status=data[0]['equipmentdetails']['status']
			msg="Your Records has been saved successfully"
			return render_template('minisreport.html', user_details=user_details,lh=data,app_status=app_status,plantname=plantname,fdate=fdate,tdate=tdate,plant_desc=plant_pid[0]['plant_id'])
		else:
			for i in range(0,len(condition)):
				equipmentdetails.append({"Equipment_Code":equipcode[i],"Equipment_Descp":equipdesc[i],"Model":model[i],"Capacity":capacity[i],"Condition":condition[i],"Remarks":Remarks[i],"plant_approval":plant[i],"status":'applied'})
			array={"equipmentdetails":equipmentdetails,"Submit_Indicator":"Yes"}

			update_coll('Minesdailyreport',{'_id':ObjectId(form['prid'])},{'$set': array})
			msg="Your Records has been submitted successfully"
		flash(msg,'alert-success')
		data=find_in_collection('Minesdailyreport',{'plant_id':plantid,"Submit_Indicator":'No'})
	return render_template('daily_mines_report.html', user_details=user_details,list=data)

#FETCHING AND DISPLAYING MINES DAILY REPORT FOR APPROVAL
#Code modified on 17 July 2017 
@zen_reports.route('/minesreportlist', methods = ['GET', 'POST'])
@login_required
def minesreportlist():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id":1,"plant_id1":1,"plant_desc":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
	
	
	if request.method == 'POST':
			form=request.form
			from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
			to_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
			fdate= from_date.strftime("%d/%m/%Y")
			tdate=to_date.strftime("%d/%m/%Y")
		
			employee_att = db.Minesdailyreport.aggregate([{'$match' : {"CreateDate":{'$gte':from_date.strftime("%d/%m/%Y"),'$lte':to_date.strftime("%d/%m/%Y")},"plant_id":plantid}},
			{ '$unwind': '$approval_levels' },{'$unwind': '$equipmentdetails'},
            {'$match' : {'approval_levels.a_id' : uid,'$or':[{'equipmentdetails.status':'applied'},{'equipmentdetails.status':'hodapproved'},{'equipmentdetails.status':'finally_approved'}]}}])
			employee_att=list(employee_att)
			logging.info(len(employee_att))
			
			try:
				stat=employee_att[0]['approval_levels']['a_status']
			except:
				stat='not'
				
 			return render_template('mines_report_approval.html', user_details=user_details,mines=employee_att,mines_len=len(employee_att),stat=stat,fdate=fdate,tdate=tdate,plant_desc=plant_pid[0]['plant_id'])
	return render_template('daily_mines_report.html', user_details=user_details)

#APPROVAED LIST OF MINES DAILY REPORT SAVING INTO MONGODB
@zen_reports.route('/minesreportapproval', methods = ['GET', 'POST'])
@login_required
def minesreportapproval():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']

	if request.method == 'POST':
			msg2='default'
			form=request.form
			myresult=json.loads(form['formdata'])
			from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
			to_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
			fdate= from_date.strftime("%d/%m/%Y")
			tdate=to_date.strftime("%d/%m/%Y")
			fdate= from_date
			tdate=to_date
		
			c_pr =find_and_filter('Minesdailyreport',{"_id":ObjectId(form['prid'])},{"_id":0,"approval_levels":1,"equipmentdetails":1,"employee_id":1,"CreateDate":1})
			c_pr1=find_one_in_collection('Minesdailyreport',{"_id":ObjectId(form['prid'])})
			count_a_levels = len(c_pr[0]['approval_levels'])
			test_cases=find_in_collection('Minesdailyreport',{'plant_id':plantid,"CreateDate":{'$lte':from_date.strftime("%d/%m/%Y"),'$gte':to_date.strftime("%d/%m/%Y")}})
			test_cases=len(test_cases)
			eqp_count=len(c_pr[0]['equipmentdetails'])
			logging.info("Mines Approval levels"+str(count_a_levels))
			for i in range(0,count_a_levels):
				if c_pr[0]['approval_levels'][i]['a_status'] == 'current':
					j=i
					employee_name =	get_employee_name(c_pr[0]['employee_id'])
							
			if form['submit']== 'Approve':
				# logging.info(myresult)
				equp_len=len(myresult.keys())/5
				logging.info(str(equp_len)+'------------')
				for i in range(0,test_cases):
					if 'equipment'+str(i) in myresult:
						logging.info(myresult['equipment'+str(i)]+' - '+myresult['remarks'+str(i)]+' - '+myresult['prid'+str(i)]+' : item '+str(i))
				
						if j == count_a_levels-1:
							if c_pr[0]['approval_levels'][j]['a_status'] == 'current' and c_pr[0]['approval_levels'][j]['a_id'] == uid:
								for m in range(0,eqp_count):
									for n in range(0,equp_len):
										
										if c_pr[0]['equipmentdetails'][m]['Equipment_Code']	== myresult['equipment'+str(n)]:
											logging.info("true"+str(c_pr[0]['approval_levels'][0]['a_id']))
											arry5={"equipmentdetails."+str(m)+".Remarks":myresult['remarks'+str(n)],"equipmentdetails."+str(m)+".Condition":myresult['condition'+str(n)],"equipmentdetails."+str(m)+".plant_approval":myresult['plant'+str(n)],"equipmentdetails."+str(m)+".status": "finally_approved","status":"approved"}
											update_coll('Minesdailyreport',{'_id':ObjectId(myresult['prid'+str(i)])},{"$set":arry5})
								arry3={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j)+".a_time":datetime.datetime.now()}
								update_coll('Minesdailyreport',{'_id':ObjectId(myresult['prid'+str(i)])},{'$set': arry3})
								msg="You have finally approved"
								msg1="Mines report raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
							
								flash(msg,'alert-success')
								notification(c_pr[0]['employee_id'],msg1)
								notification(c_pr[0]['approval_levels'][0]['a_id'],msg1)
								return redirect(url_for('zenreports.minesreportlist'))
								# flash(msg,'alert-success')
								# return render_template('daily_mines_report.html', user_details=user_details)
						else:	
							if c_pr[0]['approval_levels'][j]['a_status'] == 'current' and c_pr[0]['approval_levels'][j]['a_id'] == uid:
								for m in range(0,eqp_count):
									for n in range(0,equp_len):
										
										if c_pr[0]['equipmentdetails'][m]['Equipment_Code']	== myresult['equipment'+str(n)]:
											logging.info("true"+str(c_pr[0]['approval_levels'][j]['a_id']))
											arry6={"equipmentdetails."+str(m)+".Remarks":myresult['remarks'+str(n)],"equipmentdetails."+str(m)+".Condition":myresult['condition'+str(n)],"equipmentdetails."+str(m)+".plant_approval":myresult['plant'+str(n)],"equipmentdetails."+str(m)+".status": "hodapproved"}
											update_coll('Minesdailyreport',{'_id':ObjectId(myresult['prid'+str(i)])},{"$set":arry6})
								arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
									"approval_levels."+str(j+1)+".a_time":"None","approval_levels."+str(j)+".a_time":datetime.datetime.now()}
								update_coll('Minesdailyreport',{'_id':ObjectId(myresult['prid'+str(i)])},{'$set': arry4})
								msg="You have approved and it is sent to Plant Head approval"
								msg1="Your raised Mines report is approved by HOD and waiting for Plant Head approval"
								msg2="Mines report raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is approved by HOD and waiting for your approval"
								flash(msg,'alert-success')
								notification(c_pr[0]['employee_id'],msg1)
								notification(c_pr[0]['approval_levels'][j+1]['a_id'],msg2)
								return redirect(url_for('zenreports.minesreportlist'))
								# flash(msg,'alert-success')
								# return render_template('daily_mines_report.html', user_details=user_details)	
					else:
						logging.info('equipment'+str(i))
					
	return render_template('daily_mines_report.html', user_details=user_details)

# MINES DAILY PERFORMANCE REPORT VIEW FOR HOD AND PLANT HEAD
@zen_reports.route('/minesdailyreportforhod', methods = ['GET', 'POST'])
@login_required
def minesdailyreportforhod():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
	plantname=plant_pid[0]['plant_desc']
	if request.method == 'POST':
		form=request.form
		from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
		to_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
		fdate= from_date.strftime("%d/%m/%Y")
		tdate=to_date.strftime("%d/%m/%Y")
		employee_att = db.Minesdailyreport.aggregate([{'$match' : {"CreateDate":{'$gte':from_date.strftime("%d/%m/%Y"),'$lte':to_date.strftime("%d/%m/%Y")}}},
		{ '$unwind': '$approval_levels' },{'$unwind': '$equipmentdetails'},
	            {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'approved'}]}}])
		employee_att=list(employee_att)
		try:
			app_status=employee_att[0]['equipmentdetails']['status']
		except:
			app_status='Not yet approved'
		return render_template('minisreport.html', user_details=user_details,lh=employee_att,app_status=app_status,plantname=plantname,fdate=fdate,tdate=tdate)
	return render_template('daily_mines_report.html', user_details=user_details)

#MINES DIALY PERFORMANCE REPORT VIEW FOR HO
@zen_reports.route('/minesdailyperformancereport', methods = ['GET', 'POST'])
@login_required
def minesdailyperformancereport():
 	user_details = base()
 	db=dbconnect()
 	uid = current_user.username
 	plantname=find_all_in_collection("Plants")
 	
 	if request.method == 'POST':
		form=request.form
		plant_pid=find_and_filter('Plants',{"plant_id1":form['plant']},{"plant_id1":1,"plant_desc":1,"_id":0})
		plantname1=plant_pid[0]['plant_desc']
		
 		from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
		to_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
		fdate= from_date.strftime("%d/%m/%Y")
		tdate=to_date.strftime("%d/%m/%Y")
 		data=db.Minesdailyreport.find({"plant_id":form['plant'],"CreateDate":{'$gte':from_date.strftime("%d/%m/%Y"),'$lte':to_date.strftime("%d/%m/%Y")}})
 		app_status=list(data)[0]['equipmentdetails']['status']
 		return render_template('minisreport.html', user_details=user_details,lh=data,app_status=app_status,plantname=plantname1,fdate=fdate,tdate=tdate)
 	return render_template('minesdailyperformancereport.html', user_details=user_details,plantname=plantname)

@zen_reports.route('/minesmaster/')
def minesmaster():
	c=os.path.abspath("zenapp/static/minesmaster.csv")
	with open(c) as csvfile:
		reader = csv.DictReader(csvfile)
		
		for e in reader:
			array={u"plant_id":str(e['plant']),u"EquipmentDesc":str(e['Equipdesc']),u"EquipmentCode":str(e['Equipcode']),u"Model":str(e['Model']),u"Capacity":str(e['Capacity'])}
			# print "Hello",array
			save_collection('Minesequipmentmaster',array)
		return "successfully"

# New Function added on 17 July 2017
#Mines Daily Status Report generating PDF
@zen_reports.route('/mines_pdf')
def mines_pdf():
	# user_details = base()
	db=dbconnect()
	# uid = current_user.username
	plants=find_in_collection('Plants',{})
	m = datetime.date.today() - timedelta(days=1)
	print "Mines daily status report called"
	for i in range(0,len(plants)):
		logging.info("PlantName:"+str(plants[i]['plant_id1']))
		data=db.Minesdailyreport.find({"plant_id":plants[i]['plant_id1'],"CreateDate":{'$gte':m.strftime("%d/%m/%Y"),'$lte':m.strftime("%d/%m/%Y")},'status':'approved'})		
		data=list(data)
		# logging.info(data)
		logging.info(len(data))
		if data != []:
			plant_pid=find_and_filter('Plants',{"plant_id1":plants[i]['plant_id1']},{"plant_id":1,"plant_desc":1,"_id":0})
			plantname=plant_pid[0]['plant_desc']
			pdf = FPDF('P', 'mm', 'A4')
			pdf.add_page()
			pdf.image('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/images/logo.png', 12, 10, 55)
			pdf.set_xy(70,20)
			pdf.set_font('Arial', 'B', 15)
			pdf.cell(16, 9, 'Mining Equipment Daily Status Report','C')
			pdf.set_xy(15,35)
			pdf.set_font('Arial', '', 11)
			pdf.cell(30, 9, 'Plant  :')
			pdf.set_xy(28,35)
			pdf.set_font('Arial', '', 10)
			pdf.cell(30, 9, str(plantname))
			pdf.set_xy(155,35)
			pdf.set_font('Arial', '', 11)
			pdf.cell(30, 9, 'Date  :')
			pdf.set_xy(170,35)
			pdf.set_font('Arial', '', 10)
			pdf.cell(30, 9, str(m.strftime("%d/%m/%Y")))
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(15,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(8, 6, 'SNo', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)	
			pdf.set_xy(23,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(63, 6, 'Equipment Desc', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(86,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(22, 6, 'Condition', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)	
			pdf.set_xy(108,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(65, 6, 'Remarks', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(173,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(20, 6, 'Action at', 1, 0, 'C',1)
			pdf.set_font('Arial', '', 9)
			y=51
			for i in range(0,len(data[0]['equipmentdetails'])):
				pdf.set_xy(15,y)
				pdf.cell(8, 6, str(i+1), 1, 1, 'C')
				pdf.set_xy(23,y)
				pdf.cell(63, 6, data[0]['equipmentdetails'][i]['Equipment_Descp'], 1, 1, 'L')
				pdf.set_xy(86,y)
				pdf.cell(22, 6, data[0]['equipmentdetails'][i]['Condition'], 1, 1, 'C')
				pdf.set_xy(108,y)
				pdf.cell(65, 6,data[0]['equipmentdetails'][i]['Remarks'], 1, 1, 'C')
				pdf.set_xy(173,y)
				pdf.cell(20, 6,data[0]['equipmentdetails'][i]['plant_approval'], 1, 1, 'C')
				y=y+6
				if i == 35:#adding new page
					y=22
					pdf.add_page()
					y=y+6
			date=m.strftime("%d-%m-%Y")
			pdf.output('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Mines_status/current_pdf/'+plantname+'-Mines Status Report-'+date+'.pdf','F')
			
		else:
			print "No data"
	
	return "success"
@zen_reports.route('/test_cronjob')
def test_cronjob():
	logging.info("Test cronjob working fine")
	print "Test cronjob working fine"
	print "ananth kumar testing this job"
	return "success"
#Mines report dowload
@zen_reports.route('/mines_pdf_download/', methods = ['GET', 'POST'])
def mines_pdf_download():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
	plantname=plant_pid[0]['plant_desc']

	if request.method == 'POST':
		form=request.form
		from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
	
		data=db.Minesdailyreport.find({"plant_id":plantid,"CreateDate":{'$gte':from_date.strftime("%d/%m/%Y"),'$lte':from_date.strftime("%d/%m/%Y")},'status':'approved'})
		
		data=list(data)
		
		if data != []:
			
			plant_pid=find_and_filter('Plants',{"plant_id1":plantid},{"plant_id":1,"plant_desc":1,"_id":0})
			plantname=plant_pid[0]['plant_desc']
			
			pdf = FPDF('P', 'mm', 'A4')
			pdf.add_page()
			pdf.image('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/images/logo.png', 12, 10, 55)
			pdf.set_xy(70,20)
			pdf.set_font('Arial', 'B', 15)
			pdf.cell(16, 9, 'Mining Equipment Daily Status Report','C')
			pdf.set_xy(15,35)
			pdf.set_font('Arial', '', 11)
			pdf.cell(30, 9, 'Plant  :')
			pdf.set_xy(28,35)
			pdf.set_font('Arial', '', 10)
			pdf.cell(30, 9, str(plantname))
			pdf.set_xy(155,35)
			pdf.set_font('Arial', '', 11)
			pdf.cell(30, 9, 'Date  :')
			pdf.set_xy(170,35)
			pdf.set_font('Arial', '', 10)
			pdf.cell(30, 9, str(from_date.strftime("%d/%m/%Y")))
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(15,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(8, 6, 'SNo', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)	
			pdf.set_xy(23,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(63, 6, 'Equipment Desc', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(86,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(22, 6, 'Condition', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)	
			pdf.set_xy(108,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(65, 6, 'Remarks', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(173,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(20, 6, 'Action at', 1, 0, 'C',1)
			pdf.set_font('Arial', '', 9)
			y=51
			for i in range(0,len(data[0]['equipmentdetails'])):
				pdf.set_xy(15,y)
				pdf.cell(8, 6, str(i+1), 1, 1, 'C')
				pdf.set_xy(23,y)
				pdf.cell(63, 6, data[0]['equipmentdetails'][i]['Equipment_Descp'], 1, 1, 'L')
				pdf.set_xy(86,y)
				pdf.cell(22, 6, data[0]['equipmentdetails'][i]['Condition'], 1, 1, 'C')
				pdf.set_xy(108,y)
				pdf.cell(65, 6,data[0]['equipmentdetails'][i]['Remarks'], 1, 1, 'C')
				pdf.set_xy(173,y)
				pdf.cell(20, 6,data[0]['equipmentdetails'][i]['plant_approval'], 1, 1, 'C')
				y=y+6
				if i == 35:#adding new page
					y=22
					pdf.add_page()
					y=y+6
			pdf.output('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Mines_status/prev_pdf/'+plantname+'-Mines Status Report.pdf','F')
			
			return render_template('mines_status_pdf.html',user_details=user_details,plantname=plantname)

		else:
			print "No data"
	return render_template('mines_pdf_download_serach.html', user_details=user_details)
#Mines report dowload For HO
@zen_reports.route('/mines_pdf_download_HO/', methods = ['GET', 'POST'])
def mines_pdf_download_HO():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
	plantname=plant_pid[0]['plant_desc']

	if request.method == 'POST':
		form=request.form
		from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
		plant=form['plant']
		data=db.Minesdailyreport.find({"plant_id":plant,"CreateDate":{'$gte':from_date.strftime("%d/%m/%Y"),'$lte':from_date.strftime("%d/%m/%Y")},'status':'approved'})
		
		data=list(data)
		
		if data != []:
			print "data vaialable",len(data[0]['equipmentdetails'])
		
			plant_pid=find_and_filter('Plants',{"plant_id1":plant},{"plant_id":1,"plant_desc":1,"_id":0})
			plantname=plant_pid[0]['plant_desc']
			
			pdf = FPDF('P', 'mm', 'A4')
			pdf.add_page()
			pdf.image('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/images/logo.png', 12, 10, 55)
			pdf.set_xy(70,20)
			pdf.set_font('Arial', 'B', 15)
			pdf.cell(16, 9, 'Mining Equipment Daily Status Report','C')
			pdf.set_xy(15,35)
			pdf.set_font('Arial', '', 11)
			pdf.cell(30, 9, 'Plant  :')
			pdf.set_xy(28,35)
			pdf.set_font('Arial', '', 10)
			pdf.cell(30, 9, str(plantname))
			pdf.set_xy(155,35)
			pdf.set_font('Arial', '', 11)
			pdf.cell(30, 9, 'Date  :')
			pdf.set_xy(170,35)
			pdf.set_font('Arial', '', 10)
			pdf.cell(30, 9, str(from_date.strftime("%d/%m/%Y")))
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(15,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(8, 6, 'SNo', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)	
			pdf.set_xy(23,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(63, 6, 'Equipment Desc', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(86,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(22, 6, 'Condition', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)	
			pdf.set_xy(108,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(65, 6, 'Remarks', 1, 0, 'C',1)
			pdf.set_font('Arial', 'B', 11)
			pdf.set_xy(173,45)
			pdf.set_fill_color(211,211,211)
			pdf.cell(20, 6, 'Action at', 1, 0, 'C',1)
			pdf.set_font('Arial', '', 9)
			y=51
			for i in range(0,len(data[0]['equipmentdetails'])):
				pdf.set_xy(15,y)
				pdf.cell(8, 6, str(i+1), 1, 1, 'C')
				pdf.set_xy(23,y)
				pdf.cell(63, 6, data[0]['equipmentdetails'][i]['Equipment_Descp'], 1, 1, 'L')
				pdf.set_xy(86,y)
				pdf.cell(22, 6, data[0]['equipmentdetails'][i]['Condition'], 1, 1, 'C')
				pdf.set_xy(108,y)
				pdf.cell(65, 6,data[0]['equipmentdetails'][i]['Remarks'], 1, 1, 'C')
				pdf.set_xy(173,y)
				pdf.cell(20, 6,data[0]['equipmentdetails'][i]['plant_approval'], 1, 1, 'C')
				y=y+6
				if i == 35:#adding new page
					y=22
					pdf.add_page()
					y=y+6
			pdf.output('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Mines_status/prev_pdf/'+plantname+'-Mines Status Report.pdf','F')
			print plantname
			return render_template('mines_status_pdf.html',user_details=user_details,plantname=plantname)

		else:
			msg="No PDF Available "
			flash(msg,'alert-danger')
	return render_template('mines_pdf_search.html', user_details=user_details)
# Mines equipment daily status report for HOD and Plant Head
@zen_reports.route('/mines_send_mail_plants/')
def mines_send_mail_plants():
	
	m = datetime.date.today() - timedelta(days=1)
	today=m.strftime("%d-%m-%Y")
	receiver='ananthkumar.p@pennacement.com'
	
	receivers=find_in_collection("Mines_mail_sending_list",{})
	
	for i in range(0,len(receivers)):
		USERNAME='myportal@pennacement.com'
		PASSWORD='Penna@123'
		RECEIVER=receiver
		msg = MIMEMultipart()
		msg['Subject'] = "Mines Equipment Daily Status Report-"+str(today)
		
		server = smtplib.SMTP("smtp.office365.com:587")
		server.starttls()
		server.login(USERNAME,PASSWORD)
		error=[]
		try:
			path=os.path.abspath("/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Mines_status/current_pdf/"+receivers[i]['plant_desc']+"-Mines Status Report-"+today+".pdf")
			body = email.mime.Text.MIMEText('Dear Sir,<br><br>Please find attached Mines Equipment Daily Status Report for'+today+' <br><br><br>Thank you.','html')
			msg.attach(body)

			filename='Mines Daily Status Report-'+receivers[i]['plant_desc']+'-'+today+'.pdf'
			fp=open(path,'rb')
			att = email.mime.application.MIMEApplication(fp.read(),_subtype="pdf")
			fp.close()
			att.add_header('Content-Disposition','attachment',filename=filename)
			msg.attach(att)
		except:
			error.append({"No Report"})

		try:
			if len(error) == 1:
				logging.info("No Records in system on  : "+str(datetime.datetime.now()))
				R="No Data Error"
				print R
			else:
				server.sendmail(USERNAME,receivers[i]['email_id'],msg.as_string())
				print "Mail Has Been Sent Successfully"
				server.quit()
				R="success"
		except Exception,R:
			print R
	logging.info("Report Generated By System at : "+str(datetime.datetime.now()))
	result={'msg':R}
	return jsonify(result)
	
#Mines equipment daily status report for HO
@zen_reports.route('/mines_send_mail_HO/')
def mines_send_mail_HO():
	
	m = datetime.date.today() - timedelta(days=1)
	today=m.strftime("%d-%m-%Y")
	receiver='ananthkumar.p@pennacement.com'
	plants=find_in_collection('Plants',{})
	
	receivers=find_and_filter('Mines_mail_sending_list',{"plant_desc":"CORPORATE OFFICE"},{"email_id":1,"_id":0})
	
	for i in range(0,len(receivers)):
		USERNAME='myportal@pennacement.com'
		PASSWORD='Penna@123'
		RECEIVER=receiver
		msg = MIMEMultipart()
		msg['Subject'] = "Mines Equipment Daily Status Report -"+str(today)
	
		server = smtplib.SMTP("smtp.office365.com:587")
		server.starttls()
		server.login(USERNAME,PASSWORD)
		
		body = email.mime.Text.MIMEText('Dear Sir,<br><br>Please find attached Mines Equipment Daily Status Report for '+today+' <br><br><br>Thank you.','html')
		msg.attach(body)
		error=[]
		for j in range(0,len(plants)):
			try:
				filename='Mines Daily Status Report-'+plants[j]['plant_desc']+'-'+str(today)+'.pdf'
				path=os.path.abspath("/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Mines_status/current_pdf/"+plants[j]['plant_desc']+"-Mines Status Report-"+today+".pdf")
				fp=open(path,'rb')
				att = email.mime.application.MIMEApplication(fp.read(),_subtype="pdf")
				fp.close()
				att.add_header('Content-Disposition','attachment',filename=filename)
				msg.attach(att)
			except:
				print "No Report"
				error.append({"No Report"})
		print len(error)	
		try:
			if len(error) == 6:
				logging.info("No Records in system on  : "+str(datetime.datetime.now()))
				R="No Data Error"
			else:
				server.sendmail(USERNAME,receivers[i]['email_id'],msg.as_string())
				print "Mail Has Been Sent Successfully"
				server.quit()
				R="success"
		except Exception,R:
			print R
	logging.info("Report Generated By System at : "+str(datetime.datetime.now()))
	result={'msg':R}
	return jsonify(result)

@zen_reports.route('/testcertificate/', methods = ['GET', 'POST'])
@login_required
def testcertificate():
 	user_details = base()
 	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"_id":0})
	plantname=plant_pid[0]['plant_desc']
	
 	if request.method == 'POST':
		form=request.form
		week=form['week']
		from_date=datetime.datetime.strptime(form['start_date'], '%d/%m/%Y')
		to_date=datetime.datetime.strptime(form['end_date'], '%d/%m/%Y')
		fdate= from_date.strftime("%d/%m/%Y")
		tdate=to_date.strftime("%d/%m/%Y")
		# if form['ctype'] == '1':
		# 	return render_template('test_certificate_opc53_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
		# if form['ctype'] == '2':
		# 	return render_template('test_certificate_opc43_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
		# if form['ctype'] == '3':
		# 	return render_template('test_certificate_ppc_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
		# if form['ctype'] == '4':
		# 	return render_template('test_certificate_psc_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
	
	#Code modified on 13-11-2017 to avoid duplication records
		if form['ctype'] == '1':
			cement_type='OPC:53Gr'
			if(not find_one_in_collection('TClabreports',{"week":form['week'],'plant_id':plant_pid[0]['plant_id1'],'TCyear':str(from_date.year),'cement_type':cement_type})):
				return render_template('test_certificate_opc53_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
			else:
				flash('Already record saved with same week number','alert-danger')
				return render_template('test_certificate.html', user_details=user_details)
		if form['ctype'] == '2':
			cement_type='OPC:43Gr'
			if(not find_one_in_collection('TClabreports',{"week":form['week'],'plant_id':plant_pid[0]['plant_id1'],'TCyear':str(from_date.year),'cement_type':cement_type})):
				return render_template('test_certificate_opc43_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
			else:
				flash('Already record saved with same week number','alert-danger')
				return render_template('test_certificate.html', user_details=user_details)
		if form['ctype'] == '3':
			cement_type='PPC'
			if(not find_one_in_collection('TClabreports',{"week":form['week'],'plant_id':plant_pid[0]['plant_id1'],'TCyear':str(from_date.year),'cement_type':cement_type})):
				return render_template('test_certificate_ppc_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
			else:
				flash('Already record saved with same week number','alert-danger')
				return render_template('test_certificate.html', user_details=user_details)
		if form['ctype'] == '4':
			cement_type='PSC'
			if(not find_one_in_collection('TClabreports',{"week":form['week'],'plant_id':plant_pid[0]['plant_id1'],'TCyear':str(from_date.year),'cement_type':cement_type})):
				return render_template('test_certificate_psc_form.html', user_details=user_details,week=week,fdate=fdate,tdate=tdate,plantname=plantname)
			else:
				flash('Already record saved with same week number','alert-danger')
				return render_template('test_certificate.html', user_details=user_details)	
 	return render_template('test_certificate.html', user_details=user_details)

@zen_reports.route('/testcertificateformsubmit', methods = ['GET', 'POST'])
@login_required
def testcertificateformsubmit():
 	user_details = base()
 	db=dbconnect()
 	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_desc":1,"chief_pid":1,"_id":0})
	plantname=plant_pid[0]['plant_desc']
	plantid=plant_pid[0]['plant_id1']
 	if request.method == 'POST':
		form=request.form
		dept_cheif_pid=find_and_filter("departmentmaster",{"plant_id":plant_pid[0]['plant_id1'],"dept":'PRODUCTION AND QUALITY CONTROL'},{"dept_chief_pid":1,"_id":0})
		a=[]
		currentYear=datetime.datetime.today()
		week=form['week']
		fdate=form['fdate']
		tdate=form['tdate']
		create_new=datetime.datetime.today()
		yr=currentYear.year
		levels=[]
		dept_chief_Eid=find_and_filter('Userinfo',{"designation_id":dept_cheif_pid[0]['dept_chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":dept_chief_Eid[0]['username'],"a_time":"None"})
		formula1="%.2f" % float(form['formula1'])
		formula2="%.2f" % float(form['formula2'])
		insoluble="%.2f" % float(form['insoluble'])
		magnesia="%.2f" % float(form['magnesia'])
		sulphuric="%.2f" % float(form['sulphuric'])
		sulphide="%.2f" % float(form['sulphide'])
		loss="%.2f" % float(form['loss'])
		chloride="%.3f" % float(form['chloride'])
		# one decimal convertion
		normal="%.1f" % float(form['normal'])
		lechat="%.2f" % float(form['lechat'])
		auto="%.2f" % float(form['auto'])
		roomtemp="%.1f" % float(form['temp'])
		humidity="%.1f" % float(form['humidity'])
		finen=int(float(form['finen']))
		initial=int(float(form['initial']))
		final=int(float(form['final']))
		slag="%.1f" % float(form['slag'])
		flyash="%.1f" % float(form['flyash'])
		try:
			deying="%.2f" % float(form['deying'])
		except:
			deying=form['deying']

		try:
			hour1="%.1f" % float(form['hour1'])
		except:
			hour1=form['hour1']
		
		try:
			hour2="%.1f" % float(form['hour2'])
		except:
			hour2=form['hour2']
		
		try:
			hour3="%.1f" % float(form['hour3'])
		except:
			hour3=form['hour3']
		a.append({"formula1":formula1,"formula2":formula2,"insoluble":insoluble,"magnesia":magnesia,"sulphuric":sulphuric,"sulphide":sulphide,"loss":loss,"chloride":chloride,"finen":finen,"normal":normal,"initial":initial,"final":final,"lechat":lechat,"auto":auto,"hour1":hour1,"hour2":hour2,"hour3":hour3,"slag":slag,"flyash":flyash,"deying":deying,"roomtemp":roomtemp,"humidity":humidity})
		array={"week":week,"cement_type":form['type'], "specifications":a,"plant_desc":plantname,"plant_id":plantid,"fromdate":fdate,"todate":tdate,"approvals":levels,"employee_id":uid,"status":"applied","created_date":create_new,"TCyear":str(yr)}
 		save_collection('TClabreports',array)
 		msg="Your Lab "+form['type']+" Record have been saved successfully"
		msg1="LAB "+form['type']+" Report is waiting for your approval"
		notification(uid,msg)
		notification(dept_chief_Eid[0]['username'],msg1)
		flash(msg,'alert-success')
		
 		labdata=db.TClabreports.find({"employee_id":uid,"week":week})
 		
 	return render_template('test_certificate_report_view.html', user_details=user_details,labdata=labdata)

@zen_reports.route('/testcertificatehistory/', methods = ['GET', 'POST'])
@login_required
def testcertificatehistory():
 	user_details = base()
 	db=dbconnect()
 	uid = current_user.username
 	labdata=db.TClabreports.aggregate([{'$match':{"employee_id":uid}},{'$unwind':'$specifications'}])
 	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"plant_address":1,"plant_contact":1,"plant_desc":1,"_id":0})
   	plantaddress=plant_pid[0]['plant_address']
   	plantcontact=plant_pid[0]['plant_contact']
   	plant_id=plant_pid[0]['plant_id1']
	if request.method == 'POST':
		form=request.form
		if form['submit'] == 'SEARCH':
			labdata=find_in_collection("TClabreports",{"cement_type":form['ctype'],"week":form['week'],"employee_id":uid,"TCyear":form['year']})
			return render_template('test_certificate_report_view.html', user_details=user_details,labdata=labdata)
		
		labdata=db.TClabreports.aggregate([{'$match':{"_id": ObjectId(form['prid'])}},{'$unwind':'$specifications'}])
		labdata=list(labdata)
		if form['submit'] == 'EDIT':
			for lab in labdata:
				if lab['cement_type'] == 'OPC:53Gr':
					return render_template('test_certificate_opc53_edit_form.html', user_details=user_details,labdata=labdata)
				if lab['cement_type'] == 'OPC:43Gr':
					return render_template('test_certificate_opc43_edit_form.html', user_details=user_details,labdata=labdata)
				if lab['cement_type'] == 'PPC':
					return render_template('test_certificate_ppc_edit_form.html', user_details=user_details,labdata=labdata)
				if lab['cement_type'] == 'PSC':
					return render_template('test_certificate_psc_edit_form.html', user_details=user_details,labdata=labdata)
		
		if form['submit'] == 'VIEW':
			plant_pid=find_and_filter('Plants',{"plant_desc":form['plantdesc']},{"plant_id":1,"plant_address":1,"plant_contact":1,"plant_desc":1,"_id":0})
   			plantaddress=plant_pid[0]['plant_address']
   	  		plantcontact=plant_pid[0]['plant_contact']
   	  		plant_id=plant_pid[0]['plant_id']
			for lab in labdata:
				if lab['cement_type'] == 'OPC:53Gr':
					return render_template('test_certificate_opc53_view.html', user_details=user_details,labdata=labdata,plantaddress=plantaddress,plantcontact=plantcontact,plantid=plant_id)
				if lab['cement_type'] == 'OPC:43Gr':
					return render_template('test_certificate_opc43_view.html', user_details=user_details,labdata=labdata,plantaddress=plantaddress,plantcontact=plantcontact,plantid=plant_id)
				if lab['cement_type'] == 'PPC':
					return render_template('test_certificate_ppc_view.html', user_details=user_details,labdata=labdata,plantaddress=plantaddress,plantcontact=plantcontact,plantid=plant_id)
				if lab['cement_type'] == 'PSC':
					return render_template('test_certificate_psc_view.html', user_details=user_details,labdata=labdata,plantaddress=plantaddress,plantcontact=plantcontact,plantid=plant_id)
		
				
		if form['submit'] == 'UPDATE':
			a=[]
			a.append({"formula1":form['formula1'],"formula2":form['formula2'],"insoluble":form['insoluble'],"magnesia":form['magnesia'],"sulphuric":form['sulphuric'],"sulphide":form['sulphide'],"loss":form['loss'],"chloride":form['chloride'],"finen":form['finen'],"normal":form['normal'],"initial":form['initial'],"final":form['final'],"lechat":form['lechat'],"auto":form['auto'],"hour1":form['hour1'],"hour2":form['hour2'],"hour3":form['hour3'],"slag":form['slag'],"flyash":form['flyash'],"deying":form['deying'],"roomtemp":form['temp'],"humidity":form['humidity']})
			update_coll('TClabreports',{'_id':ObjectId(form['prid'])},{'$set': {"specifications":a}})
			labdata=db.TClabreports.aggregate([{'$match':{"employee_id":uid}},{'$unwind':'$specifications'}])
			labdata=list(labdata)
			msg="Your record updated successfully"
			flash(msg,'alert-success')
			return redirect(url_for('.testcertificatehistory'))
		
	
		if form['submit'] == 'APPROVE':
			c_pr =find_and_filter('TClabreports',{"_id":ObjectId(form['prid'])},{"_id":0,"approvals":1,"employee_id":1})
			count_a_levels = len(c_pr[0]['approvals'])
			employee_name =	get_employee_name(c_pr[0]['employee_id'])
			a=[]
			a.append({"formula1":form['formula1'],"formula2":form['formula2'],"insoluble":form['insoluble'],"magnesia":form['magnesia'],"sulphuric":form['sulphuric'],"sulphide":form['sulphide'],"loss":form['loss'],"chloride":form['chloride'],"finen":form['finen'],"normal":form['normal'],"initial":form['initial'],"final":form['final'],"lechat":form['lechat'],"auto":form['auto'],"hour1":form['hour1'],"hour2":form['hour2'],"hour3":form['hour3'],"slag":form['slag'],"flyash":form['flyash'],"deying":form['deying'],"roomtemp":form['temp'],"humidity":form['humidity']})
			
			j=0
			update_coll('TClabreports',{'_id':ObjectId(form['prid'])},{'$set': {"specifications":a}})
			arry3={"approvals."+str(j)+".a_status":"approved","status":"finally_approved"}
			update_coll('TClabreports',{'_id':ObjectId(form['prid'])},{'$set': arry3})
			msg="You have finally approved"
			msg1="LAB report raised by MR "+get_employee_name(c_pr[0]['employee_id'])+" is finally approved by Plant Head"
			flash(msg,'alert-success')
			notification(c_pr[0]['employee_id'],msg1)
			notification(c_pr[0]['approvals'][0]['a_id'],msg1)
			return redirect(url_for('.testcertificatesapprovelist'))
	return render_template('test_certificate_report_view.html', user_details=user_details,labdata=labdata)

@zen_reports.route('/testcertificateview' , methods = ['GET', 'POST'])
@login_required
def testcertificateview():
	user_details = base()
  	uid = current_user.username
  	if request.method == 'POST':
		form=request.form
		labdata=find_in_collection("TClabreports",{"cement_type":form['ctype'],"week":form['week'],"plant_id":form['plant_desc'],"TCyear":form['year'],"status":"finally_approved"})
		return render_template('test_certificate_pdf_view.html', user_details=user_details,labdata=labdata)
 	return render_template('test_certificate_view.html', user_details=user_details)

@zen_reports.route('/testcertificatesapprovelist/', methods = ['GET', 'POST'])
@login_required
def testcertificatesapprovelist():
	user_details = base()
	db=dbconnect()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
	employee_att = db.TClabreports.aggregate([{'$match' : {"plant_id":plantid}},
		{ '$unwind': '$approvals' },
        {'$match' : {'approvals.a_id' : uid,'$or':[{'approvals.a_status':'current'},{'approvals.a_status':'waiting'},{'approvals.a_status':'hodapproved'}]}}])					
	
	employee_att=list(employee_att)		
			
				
	return render_template('test_certificate_approve.html', user_details=user_details,labdata=employee_att)

@zen_reports.route('/tceditforhod/', methods = ['GET', 'POST'])
@login_required
def tceditforhod():
	user_details = base()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
  	if request.method == 'POST':
		form=request.form
		labdata=find_in_collection("TClabreports",{"cement_type":form['ctype'],"week":form['week'],"plant_id":form['plant_desc'],"TCyear":form['year'],"status":"finally_approved"})
		return render_template('test_certificate_hod_view.html', user_details=user_details,labdata=labdata)
	return render_template('test_certificate_hod_edit_view.html', user_details=user_details,plantid=plantid)

# Plant Accident/Incident Report
@zen_reports.route('/accidentForm/', methods = ['GET', 'POST'])
@login_required
def accidentForm():
	user_details = base()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"chief_pid":1,"_id":0})
	plantid=plant_pid[0]['plant_id1']
	accident_incident_master=find_and_filter('Accident_Incident_Config_Master',{"plant_id":plantid},{"USA_USC":1,"category":1,"_id":0})
  	accident_incident_master.sort()
  	employee_list=get_plant_employees_list()
  	if request.method == 'POST':
		form=request.form
		
		Dateofincident=form.getlist('entry_date')
		Elocation=form.getlist('Elocation')
		selct1=form.getlist('selct1')
		contname=form.getlist('contname')
		personname=form.getlist('personname')
		contract_emp=form.getlist('employee')
		bdyinjury=form.getlist('bdyinjury')
		natinjury=form.getlist('natinjury')
		physinjury=form.getlist('physinjury')
		selct2=form.getlist('selct2')
		selct3=form.getlist('selct3')
		descpirtionofincident=form.getlist('descpirtionofincident')
		rootcause=form.getlist('rootcause')
		actionsuggested=form.getlist('actionsuggested')
		actionsuggafterreview=form.getlist('actionsuggafterreview')
		StatusofVictim=form.getlist('StatusofVictim')
		remarks=form.getlist('remarks')
		levels=[]
		plant_Eid=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":plant_Eid[0]['username'],"a_time":"None"})
		year = datetime.date.today().year
		m = datetime.date.today()
		mon='{:02d}'.format(m.month)
		finance_year=0
		
		val=4
		val='{:02d}'.format(val)
		if mon>=val:
			finance_year=year
			
		else:
			finance_year=(year-1)
			
		for i in range(0,len(Dateofincident)):
			if Dateofincident[i] == '':
				pass
			else:
				if selct1[i] == 'PCIL':
					arry={"Dateofincident":datetime.datetime.strptime(Dateofincident[i],"%d/%m/%Y"),"Elocation":Elocation[i],"selct1":selct1[i],"personname":personname[i],"contname":"","contract_employee":"","bdyinjury":bdyinjury[i],"natinjury":natinjury[i],"physinjury":physinjury[i],"selct2":selct2[i],"selct3":selct3[i],"descpirtionofincident":descpirtionofincident[i],"rootcause":rootcause[i],"actionsuggested":actionsuggested[i],"actionsuggafterreview":actionsuggafterreview[i],"StatusofVictim":StatusofVictim[i],"remarks":remarks[i],"Created_By":uid,"Created_Date":datetime.datetime.now(),"Update_By":uid,"Update_Date":datetime.datetime.now(),"plant_id":plantid,"approval_levels":levels,"Status":"applied","fin_year":finance_year,"month":int(mon)}
				else:
					arry={"Dateofincident":datetime.datetime.strptime(Dateofincident[i],"%d/%m/%Y"),"Elocation":Elocation[i],"selct1":selct1[i],"contname":contname[i],"contract_employee":contract_emp[i],"bdyinjury":bdyinjury[i],"natinjury":natinjury[i],"physinjury":physinjury[i],"selct2":selct2[i],"selct3":selct3[i],"descpirtionofincident":descpirtionofincident[i],"rootcause":rootcause[i],"actionsuggested":actionsuggested[i],"actionsuggafterreview":actionsuggafterreview[i],"StatusofVictim":StatusofVictim[i],"remarks":remarks[i],"Created_By":uid,"Created_Date":datetime.datetime.now(),"Update_By":uid,"Update_Date":datetime.datetime.now(),"plant_id":plantid,"approval_levels":levels,"Status":"applied","fin_year":finance_year,"month":int(mon)}
				save_collection('SafetyReport',arry)
		logging.info(str(Dateofincident)+':'+str(Elocation)+':'+str(selct1)+':'+str(contname)+':'+str(personname)+':'+str(bdyinjury)+':'+str(natinjury)+':'+str(physinjury)+':'+str(selct2)+':'+str(selct3)+':'+str(descpirtionofincident)+':'+str(rootcause)+':'+str(actionsuggested)+':'+str(actionsuggafterreview)+':'+str(StatusofVictim)+':'+str(remarks))
		msg="Form has been submitted and sent for Approval"
		flash(msg,'alert-success')
		
	return render_template('Accident_Incident_Form.html',user_details=user_details,el=employee_list,accident_incident_master=accident_incident_master[0],plantid=plantid)


#safety Approval screen
@zen_reports.route('/safety_approval/', methods = ['GET', 'POST'])
@login_required
def safetyapproval():
	user_details = base()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_desc":1,"plant_id1":1,"_id":0})
	plant_desc=plant_pid[0]['plant_desc']
	plant_id=plant_pid[0]['plant_id1']
	accident_incident_master=find_and_filter('Accident_Incident_Config_Master',{"plant_id":plant_id},{"USA_USC":1,"category":1,"_id":0})
  	accident_incident_master.sort()
  	employee_list=get_plant_employees_list()
  	db=dbconnect()
	safetydata=db.SafetyReport.aggregate([{'$match' :{"plant_id":plant_id,'$or':[{'Status':"applied"},{'Status':'saved'}]}},
	{ '$unwind': '$approval_levels' },
    {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'}]}}])
	safetydata=list(safetydata)
	rowno=len(safetydata)
	return render_template('Safety_Approval.html',user_details=user_details,safetydata=safetydata,rowno=rowno,el=employee_list,accident_incident_master=accident_incident_master[0],plantid=plant_id)


@zen_reports.route('/safety_approval_submit/', methods = ['GET', 'POST'])
@login_required
def safetyapprovalsubmit():
	user_details = base()
	uid = current_user.username
	
	if request.method == 'POST':
		form=request.form
		Dateofincident=form.getlist('entry_date')
		Elocation=form.getlist('Elocation')
		selct1=form.getlist('selct1')
		contname=form.getlist('contname')
		personname=form.getlist('personname')
		contract_emp=form.getlist('employee')
		bdyinjury=form.getlist('bdyinjury')
		natinjury=form.getlist('natinjury')
		physinjury=form.getlist('physinjury')
		selct2=form.getlist('selct2')
		selct3=form.getlist('selct3')
		descpirtionofincident=form.getlist('descpirtionofincident')
		rootcause=form.getlist('rootcause')
		actionsuggested=form.getlist('actionsuggested')
		actionsuggafterreview=form.getlist('actionsuggafterreview')
		StatusofVictim=form.getlist('StatusofVictim')
		remarks=form.getlist('remarks')
		prid=form.getlist('prid')
		check=form.getlist('checkone')
		levels=[]
		plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_id1":1,"chief_pid":1,"_id":0})
		plantid=plant_pid[0]['plant_id1']
		plant_Eid=find_and_filter('Userinfo',{"designation_id":plant_pid[0]['chief_pid']},{"username":1,"_id":0})
		levels.append({"a_status":"current","a_id":plant_Eid[0]['username'],"a_time":"None"})
		if form['submit'] == 'Save':

			for i in range(0,len(Dateofincident)):
				if (not find_one_in_collection('SafetyReport',{"_id":ObjectId(prid[i])})):
					pass #change has been made when it showed a indentation error on 10 Feb 2017
				else:
					update_collection('SafetyReport',{'_id':ObjectId(prid[i])},{"$set":{"Elocation":Elocation[i],"personname":personname[i],"contname":contname[i],"contract_employee":contract_emp[i],"bdyinjury":bdyinjury[i],"natinjury":natinjury[i],"physinjury":physinjury[i],"selct2":selct2[i],"selct3":selct3[i],"descpirtionofincident":descpirtionofincident[i],"rootcause":rootcause[i],"actionsuggested":actionsuggested[i],"actionsuggafterreview":actionsuggafterreview[i],"StatusofVictim":StatusofVictim[i],"remarks":remarks[i],"Update_By":uid,"Update_Date":datetime.datetime.now(),"Status":"saved"}})
				
			msg="Records has been saved"	
		elif form['submit'] == 'Delete':
			a=0
			for i in range(0,len(check)):
				update_collection('SafetyReport',{'_id':ObjectId(check[i])},{"$set":{"Status":"rejected","approval_levels."+str(a)+".a_status":"rejected"}})
			msg="Records has been deleted"
			flash(msg,'alert-danger')
			return redirect(url_for('.safetyapproval'))
			
		else:#submit
			print "submit block"
			a=0
			for i in range(0,len(Dateofincident)):
				if (not find_one_in_collection('SafetyReport',{"_id":ObjectId(prid[i])})):
					pass #change has been made when it showed a indentation error on 10 Feb 2017
				else:
					update_collection('SafetyReport',{'_id':ObjectId(prid[i])},{"$set":{"Elocation":Elocation[i],"personname":personname[i],"contname":contname[i],"contract_employee":contract_emp[i],"bdyinjury":bdyinjury[i],"natinjury":natinjury[i],"physinjury":physinjury[i],"selct2":selct2[i],"selct3":selct3[i],"descpirtionofincident":descpirtionofincident[i],"rootcause":rootcause[i],"actionsuggested":actionsuggested[i],"actionsuggafterreview":actionsuggafterreview[i],"StatusofVictim":StatusofVictim[i],"remarks":remarks[i],"Update_By":uid,"Update_Date":datetime.datetime.now(),"Status":"approved","approval_levels."+str(a)+".a_status":"approved"}})
			msg="Records has been apporved"
		flash(msg,'alert-success')
		return redirect(url_for('.safetyapproval'))
	return render_template('safety_report.html',user_details=user_details)


#Safety Report to Excel
@zen_reports.route('/safetyreport_generate/', methods = ['GET', 'POST'])
@login_required
def safetyreport_generate():
	user_details = base()
	uid = current_user.username
	plant_pid=find_and_filter('Plants',{"plant_id":user_details["plant_id"]},{"plant_desc":1,"plant_id1":1,"_id":0})
	plant_desc=plant_pid[0]['plant_desc']
	plant_id=plant_pid[0]['plant_id1']
	safety_config=find_and_filter('Accident_Incident_Config_Master',{"plant_id":plant_id},{"FormatNo":1,"RevNo":1,"_id":0})
	if request.method == 'POST':
		form=request.form
		sdate=datetime.datetime.strptime(form['start_date'], "%d/%m/%Y")
		edate=datetime.datetime.strptime(form['end_date'], "%d/%m/%Y")
		print "date",form['plant']
		if form['plant'] == '1001':
			plantdesc='TALARICHERUVU'
		elif form['plant'] == '1002':
			plantdesc='GANESHPAHAD CEMENT'
		elif form['plant'] == '1003':
			plantdesc='BOYAREDDYPALLI'
		elif form['plant'] == '1004':
			plantdesc='TANDUR'
		elif form['plant'] == '2001':
			plantdesc='GANESHPAHAD POWER'
		safetydata=find_in_collection_sort("SafetyReport",{'Dateofincident':{ '$gte':sdate,'$lte':edate},"plant_id":form['plant'],"Status":"approved"},[("Dateofincident",1)])
		safetydata=list(safetydata)
		safety_config=find_and_filter('Accident_Incident_Config_Master',{"plant_id":form['plant']},{"FormatNo":1,"RevNo":1,"_id":0})
		path=os.path.abspath("/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Safety/safetyReport_now.xlsx")
		workbook = xlsxwriter.Workbook(path)
		worksheet = workbook.add_worksheet()
		style_OD=workbook.add_format({'font_color':'black','bg_color':'#daf2d0'})
		bold = workbook.add_format({'bold': True})
		style_excluded = workbook.add_format({'font_color':'black','border':2,'border_color':'black','bg_color':'#daf2d0','align':'center'})
		worksheet.write(0,8,' PENNA CEMENT INDUSTRIES LIMITED',bold)
		bold1 = workbook.add_format({'bold': True})
		bold1.set_font_size(16)
		worksheet.write(1,8,'  LIST OF ACCIDENTS / INCIDENTS ',bold1)
		worksheet.write(3,0,'Plant:  '  +str(plantdesc)+'',bold)
		worksheet.write(3,7,'Format No :  '  +str(safety_config[0]['FormatNo'])+'',bold)
		worksheet.write(3,15,'Rev.No  :   '   +str(safety_config[0]['RevNo'])+'',bold)
		worksheet.set_column(0,0,6)
		worksheet.write(4,0,'S.NO.',style_excluded)
		worksheet.set_column(1,16,15)
		worksheet.write(4,1,'Date of Accident',style_excluded)
		worksheet.write(4,2,'Exact Location',style_excluded)
		worksheet.write(4,3,'Person Name',style_excluded)
		worksheet.write(4,4,'PCIL/Contractor',style_excluded)
		worksheet.write(4,5,'Name of the Contractor',style_excluded)
		worksheet.write(4,6,'Body Part Injured',style_excluded)
		worksheet.write(4,7,'Nature of Injury',style_excluded)
		worksheet.write(4,8,'Physical Cause of injury',style_excluded)
		worksheet.write(4,9,'USA/USC',style_excluded)
		worksheet.write(4,10,'Category ',style_excluded)
		worksheet.write(4,11,'Description of Accident',style_excluded)
		worksheet.write(4,12,'Root Cause',style_excluded)
		worksheet.write(4,13,'Corrective Action suggested',style_excluded)
		worksheet.write(4,14,'Status of Corrective Action after review',style_excluded)
		worksheet.write(4,15,'Status  of victim ',style_excluded)
		worksheet.write(4,16,'Remarks',style_excluded)
		row=4
		for i in range(len(safetydata)):
			worksheet.write(row+1,0,str(i+1))
			worksheet.write(row+1,1,str(safetydata[i]['Dateofincident'].strftime("%d.%m.%Y")))
			worksheet.write(row+1,2,str(safetydata[i]['Elocation']))
			if safetydata[i]['personname'] != '':
				presonname=safetydata[i]['personname']
				contname=""
			else:
				presonname=safetydata[i]['contract_employee']
				contname=safetydata[i]['contname']
			worksheet.write(row+1,3,str(presonname))
			worksheet.write(row+1,4,str(safetydata[i]['selct1']))
			worksheet.write(row+1,5,str(contname))
			worksheet.write(row+1,6,str(safetydata[i]['bdyinjury']))
			worksheet.write(row+1,7,str(safetydata[i]['natinjury']))
			worksheet.write(row+1,8,str(safetydata[i]['physinjury']))
			worksheet.write(row+1,9,str(safetydata[i]['selct2']))
			worksheet.write(row+1,10,str(safetydata[i]['selct3']))
			worksheet.write(row+1,11,str(safetydata[i]['descpirtionofincident']))
			worksheet.write(row+1,12,str(safetydata[i]['rootcause']))
			worksheet.write(row+1,13,str(safetydata[i]['actionsuggested']))
			worksheet.write(row+1,14,str(safetydata[i]['actionsuggafterreview']))
			worksheet.write(row+1,15,str(safetydata[i]['StatusofVictim']))
			worksheet.write(row+1,16,str(safetydata[i]['remarks']))
			row=row+1
		print "Row No",row
		bold1.set_font_size(12)
		worksheet.write(row+6,1,'Legend:',bold1)
		worksheet.write(row+7,1,'USA -',bold)
		worksheet.write(row+7,2,'Unsafe Act')
		worksheet.write(row+8,1,'USC -',bold)
		worksheet.write(row+8,2,'Unsafe Condition')
		bold1.set_font_size(12)
		worksheet.write(row+10,1,'Category:',bold1)
		worksheet.write(row+11,1,'Reportable Accident:',bold)
		worksheet.write(row+11,2,'Incident/Accident occurred and the person injured has not reported the duty to the next shift due to injury.')
		worksheet.write(row+12,1,'First Aid:',bold)
		worksheet.write(row+12,2,' Incident/Accident occurred and the person injured has taken first aid treatment and resumed his duty.')
		worksheet.write(row+13,1,'Near Miss:',bold)
		worksheet.write(row+13,2,'Incident occurred and no person injured.')

		workbook.close()
		if safetydata == []:
			msg="No data avialable."
			flash(msg,'alert-danger')
		else:
			msg="Safety Report generated successfully.Please click on Download Into Excel"
		# result={'msg':msg}
			flash(msg,'alert-success')
		return render_template('safety_excel_download.html',user_details=user_details)
	# return jsonify(result)
	return render_template('safety_download.html',user_details=user_details)


#summary report generation
@zen_reports.route('/safety_summary/')
def safety_summary():
	user_details = base()
	uid = current_user.username
	year = datetime.date.today().year
	day= datetime.date.today().day
	
	m = datetime.date.today()
	mon='{:02d}'.format(m.month)
	
	d = datetime.datetime.strptime(m.strftime("%d/%m/%Y"), "%d/%m/%Y")
	d2 = d.replace(day=1) - timedelta(days=1)
	
	prev = '{:01d}'.format(d2.month)
	prev1= '{:02d}'.format(d2.month)
	
	_,num_days = calendar.monthrange(year, int(prev))
	
	first_day = datetime.date(year, int(prev), 1)
	last_day = datetime.date(year, int(prev), num_days)
	
	start_date=datetime.datetime.strptime(first_day.strftime('%d/%m/%Y'), "%d/%m/%Y")
	end_date=datetime.datetime.strptime(last_day.strftime('%d/%m/%Y'), "%d/%m/%Y")
	
	logging.info(str(start_date)+':'+str(end_date))
	prmonth='{:02d}'.format(d2.month)
	logging.info('previous month'+str(prmonth))
	finance_year=0	
	val=4
	val='{:02d}'.format(val)
	if mon>val:
		finance_year=year
	else:
		finance_year=(year-1)
	
	num_days = calendar.monthrange(year, int(prev))[1]

	month_dates = [datetime.date(year, int(prev), day) for day in range(1, num_days+1)]
	
	plant_ids=["1001","1002","1003","1004","2001"]
	t=[]
	reportableaccident_per_month_ytd=0
	firstaid_per_month_ytd=0
	nearmiss_per_month_ytd=0
	for p in range(0,len(plant_ids)):
		
		reportable_accident_data=find_in_collection_sort("SafetyReport",{'Dateofincident':{ '$gte':start_date,'$lte':end_date},"plant_id":plant_ids[p],"selct3" : "Reportable Accident","fin_year":finance_year,"Status":"approved"},[("Dateofincident",1)])
		logging.info('plant'+str(plant_ids[p])+'reportable accident'+str(reportable_accident_data))
		reportableaccident_per_month=len(reportable_accident_data)
		
		first_aid_data=find_in_collection_sort("SafetyReport",{'Dateofincident':{ '$gte':start_date,'$lte':end_date},"plant_id":plant_ids[p],"selct3" : "First Aid","fin_year":finance_year,"Status":"approved"},[("Dateofincident",1)])
		firstaid_per_month=len(first_aid_data)
		near_miss_data=find_in_collection_sort("SafetyReport",{'Dateofincident':{ '$gte':start_date,'$lte':end_date},"plant_id":plant_ids[p],"selct3" : "Near Miss","fin_year":finance_year,"Status":"approved"},[("Dateofincident",1)])
		nearmiss_per_month=len(near_miss_data)
		
		if reportable_accident_data == []:
			for i in range(0,len(month_dates)):
				u=month_dates[0].strftime('%d/%m/%Y')
				t=month_dates[i].strftime('%d/%m/%Y')
				last_date_of_month=datetime.datetime.strptime(t, "%d/%m/%Y")
				first_date_of_month=datetime.datetime.strptime(u, "%d/%m/%Y")
			
			Incident_free_days=last_date_of_month.day
			
			if prmonth == val:
				fin_year=finance_year-1
			else:
				fin_year=finance_year
			
			c_pr =find_and_filter('safety_summary',{"fin_year":fin_year,"month":int(prev)-1,"plant_id":plant_ids[p]},{"_id":0,"Incident_free_days":1})
			
			Incident_free_days=Incident_free_days+c_pr[0]['Incident_free_days']
			logging.info("prev_incident_free_days"+str(Incident_free_days))
			array2={"plant_id":plant_ids[p],"month":int(prev),"fin_year":finance_year,"Incident_free_days":Incident_free_days,"reportableaccident_per_month":reportableaccident_per_month,"firstaid_per_month":firstaid_per_month,"nearmiss_per_month":nearmiss_per_month}
			
			if (not find_one_in_collection('safety_summary',{"plant_id":plant_ids[p],"month":int(prev),"fin_year":finance_year})):
				save_collection("safety_summary",array2)
			else:
				print "month record already"
				pass
			
		else:
			for i in range(0,len(reportable_accident_data)):
				last_dateof_incident=reportable_accident_data[i]['Dateofincident']
				logging.info("last_dateof_incident"+str(last_dateof_incident))
			for i in range(0,len(month_dates)):
				t=month_dates[i].strftime('%d/%m/%Y')
				last_date_of_month=datetime.datetime.strptime(t, "%d/%m/%Y")
			logging.info("last_date_of_month"+str(last_date_of_month))
			diff=last_date_of_month-last_dateof_incident
			Incident_free_days=diff.days
			logging.info("differance incident"+str(Incident_free_days))
			array={"plant_id":plant_ids[p],"month":int(prev),"fin_year":finance_year,"Incident_free_days":Incident_free_days,"reportableaccident_per_month":reportableaccident_per_month,"firstaid_per_month":firstaid_per_month,"nearmiss_per_month":nearmiss_per_month}
			
			if (not find_one_in_collection('safety_summary',{"plant_id":plant_ids[p],"month":int(prev),"fin_year":finance_year})):
				save_collection("safety_summary",array)
			else:
				print "month record already"
				pass
			
		
		prevmonth='{:02d}'.format(int(prev))
		
		if prevmonth==val:
		
			data=find_and_filter('safety_summary',{"plant_id":plant_ids[p],"fin_year":finance_year,"month":int(prev)},{"reportableaccident_per_month":1,"firstaid_per_month":1,"nearmiss_per_month":1,"_id":0})
			rytd=data[0]['reportableaccident_per_month']
			fytd=data[0]['firstaid_per_month']
			nytd=data[0]['nearmiss_per_month']

			array2={"plant_id":plant_ids[p],"fin_year":finance_year,"month":int(prev),"rytd":rytd,"fytd":fytd,"nytd":nytd}
			
			save_collection("safety_summary_month",array2)
		else:
			
			if (not find_one_in_collection('safety_summary_month',{"plant_id":plant_ids[p],"month":int(prev),"fin_year":finance_year})):
				data=find_and_filter('safety_summary',{"plant_id":plant_ids[p],"fin_year":finance_year,"month":int(prev)},{"reportableaccident_per_month":1,"firstaid_per_month":1,"nearmiss_per_month":1,"_id":0})
				data1=find_and_filter('safety_summary_month',{"plant_id":plant_ids[p],"fin_year":finance_year},{"rytd":1,"fytd":1,"nytd":1,"_id":1})
				rytd=data1[0]['rytd']+data[0]['reportableaccident_per_month']
				fytd=data1[0]['fytd']+data[0]['firstaid_per_month']
				nytd=data1[0]['nytd']+data[0]['nearmiss_per_month']
				array2={"month":int(prev),"rytd":rytd,"fytd":fytd,"nytd":nytd}
				
				update_coll('safety_summary_month',{'_id':ObjectId(data1[0]['_id'])},{'$set': array2})
			else:
				pass
	return render_template('safety_download.html',user_details=user_details)

@zen_reports.route('/safety_pdf/')
def safety_pdf():
	
	year = datetime.date.today().year
	m = datetime.date.today()
	# m=datetime.datetime.strptime("01/07/2017", "%d/%m/%Y") #temp	
	mon='{:02d}'.format(m.month)
	difference = date.today()
	
	ye=difference.strftime('%y')
	ye1=int(ye)+1
	finance_year=0	
	val=4
	val='{:02d}'.format(val)
	if mon>=val:
		finance_year=year
	else:
		finance_year=(year-1)
	finyear=str(finance_year-1)+'-'+str(finance_year) 
	
	if mon>val:
		fin_year=year
	else:
		fin_year=(year-1)
	arr_month=[4,5,6,7,8,9,10,11,12,1,2,3]
	current_date=difference.strftime("%d/%m/%Y")
	
	d = datetime.datetime.strptime(m.strftime("%d/%m/%Y"), "%d/%m/%Y")
	d2 = d.replace(day=1) - timedelta(days=1)
	prev = '{:01d}'.format(d2.month)
	
	# prev=prev_month(m)
	
	pdf = FPDF('L', 'mm', 'A4')
	pdf.add_page()
	pdf.image('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/images/logo.png', 12, 10, 55)
	pdf.set_font('Arial', '', 12)
	pdf.set_xy(102,15)
	pdf.set_font('Arial', 'B', 15)
	pdf.cell(55, 9, ' Safety Performance Summary Report','C')
	pdf.set_font('Arial', '', 11)
	pdf.set_xy(20,45)
	
	tlcdata_free_days=find_and_filter('safety_summary',{"plant_id":"1001","fin_year":fin_year,"month":int(prev)},{"Incident_free_days":1,"_id":0})
	
	Incident_free_days=tlcdata_free_days[0]['Incident_free_days']
	tlcdata=find_in_collection_sort("safety_summary",{"plant_id":"1001","fin_year":finance_year},[("month",1)])
	
	tlc_ytd_data=find_in_collection("safety_summary_month",{"plant_id":"1001","fin_year":finance_year})

	tlc_perv_ytd_data=find_and_filter('safety_summary_month',{"plant_id":"1001","fin_year":fin_year-1,"month":3},{"rytd":1,"fytd":1,"nytd":1,"_id":0})
	
	pdf.cell(41,7, 'Plant: Talaricheruvu')
	pdf.set_xy(215,35)
	pdf.cell(41,7, 'Date: ')
	pdf.set_xy(225,35)
	pdf.cell(41,7, str(current_date))
	pdf.set_xy(210,45)
	pdf.cell(41,7, 'Accident Free Days:')
	pdf.set_xy(247,45)
	pdf.cell(41,7,str(Incident_free_days))
	pdf.set_font('Arial', '', 9)
	pdf.set_y(55)
	pdf.cell(10, 7, 'S.No',1)
	pdf.cell(41,7, 'Performance Indicator',1, align = 'C')
	pdf.cell(20, 7,finyear,1,align = 'C')
	pdf.cell(16, 7,'Apr"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'May"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jun"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jul"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Aug"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Sep"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Oct"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Nov"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Dec"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jan"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Feb"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Mar"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'YTD',1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '1',1,align = 'C')
	pdf.cell(41,7, 'No. of Reportable Accidents',1)
	pdf.cell(20, 7,str(tlc_perv_ytd_data[0]['rytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == tlcdata[i]['month']:
				print "true"
				pdf.cell(16, 7,str(tlcdata[i]['reportableaccident_per_month']),1,align = 'C')
		except:
			print "false"
			pdf.cell(16, 7,'-',1,align = 'C')

	pdf.cell(16, 7,str(tlc_ytd_data[0]['rytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '2',1,align = 'C')
	pdf.cell(41,7, 'No. of First Aid Cases',1)
	pdf.cell(20, 7,str(tlc_perv_ytd_data[0]['fytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == tlcdata[i]['month']:
				pdf.cell(16, 7,str(tlcdata[i]['firstaid_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(tlc_ytd_data[0]['fytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '3',1,align = 'C')
	pdf.cell(41,7, 'No. of Near Miss',1)
	pdf.cell(20, 7,str(tlc_perv_ytd_data[0]['nytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == tlcdata[i]['month']:
				pdf.cell(16, 7,str(tlcdata[i]['nearmiss_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(tlc_ytd_data[0]['nytd']),1,align = 'C')

# 2nd table
	pdf.set_font('Arial', '', 11)
	pdf.set_xy(20,97)
	gpdata_free_days=find_and_filter('safety_summary',{"plant_id":"1002","fin_year":fin_year,"month":int(prev)},{"Incident_free_days":1,"_id":0})
	gpIncident_free_days=gpdata_free_days[0]['Incident_free_days']
	gpdata=find_in_collection_sort("safety_summary",{"plant_id":"1002","fin_year":finance_year},[("month",1)])
	
	gp_ytd_data=find_in_collection("safety_summary_month",{"plant_id":"1002","fin_year":finance_year})
	
	gp_perv_ytd_data=find_and_filter('safety_summary_month',{"plant_id":"1002","fin_year":fin_year-1,"month":3},{"rytd":1,"fytd":1,"nytd":1,"_id":0})
	
	pdf.cell(41,7, 'Plant: Ganeshpahad Cement')
	pdf.set_xy(210,97)
	pdf.cell(41,7, 'Accident Free Days:')
	pdf.set_xy(247,97)
	pdf.cell(41,7,str(gpIncident_free_days))
	pdf.set_font('Arial', '', 9)
	pdf.set_y(107)
	pdf.cell(10, 7, 'S.No',1)
	pdf.cell(41,7, 'Performance Indicator',1, align = 'C')
	pdf.cell(20, 7,finyear,1,align = 'C')
	pdf.cell(16, 7,'Apr"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'May"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jun"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jul"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Aug"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Sep"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Oct"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Nov"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Dec"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jan"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Feb"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Mar"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'YTD',1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '1',1,align = 'C')
	pdf.cell(41,7, 'No. of Reportable Accidents',1)
	pdf.cell(20, 7,str(gp_perv_ytd_data[0]['rytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == gpdata[i]['month']:
				pdf.cell(16, 7,str(gpdata[i]['reportableaccident_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(gp_ytd_data[0]['rytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '2',1,align = 'C')
	pdf.cell(41,7, 'No. of First Aid Cases',1)
	pdf.cell(20, 7,str(gp_perv_ytd_data[0]['fytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == gpdata[i]['month']:
				pdf.cell(16, 7,str(gpdata[i]['firstaid_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(gp_ytd_data[0]['fytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '3',1,align = 'C')
	pdf.cell(41,7, 'No. of Near Miss',1)
	pdf.cell(20, 7,str(gp_perv_ytd_data[0]['nytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == gpdata[i]['month']:
				pdf.cell(16, 7,str(gpdata[i]['nearmiss_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(gp_ytd_data[0]['nytd']),1,align = 'C')

	pdf.set_font('Arial', '', 11)
	pdf.set_xy(20,150)
	bpdata_free_days=find_and_filter('safety_summary',{"plant_id":"1003","fin_year":fin_year,"month":int(prev)},{"Incident_free_days":1,"_id":0})
	bpIncident_free_days=bpdata_free_days[0]['Incident_free_days']
	bpdata=find_in_collection_sort("safety_summary",{"plant_id":"1003","fin_year":finance_year},[("month",1)])
	
	bp_ytd_data=find_in_collection("safety_summary_month",{"plant_id":"1003","fin_year":finance_year})
	
	bp_perv_ytd_data=find_and_filter('safety_summary_month',{"plant_id":"1003","fin_year":fin_year-1,"month":3},{"rytd":1,"fytd":1,"nytd":1,"_id":0})
	
	pdf.cell(41,7, 'Plant: Boyareddypalli')
	pdf.set_xy(210,150)
	pdf.cell(41,7, 'Accident Free Days:')
	pdf.set_xy(247,150)
	pdf.cell(41,7,str(bpIncident_free_days))
	pdf.set_font('Arial', '', 9)
	pdf.set_y(160)
	pdf.cell(10, 7, 'S.No',1)
	pdf.cell(41,7, 'Performance Indicator',1, align = 'C')
	pdf.cell(20, 7,finyear,1,align = 'C')
	pdf.cell(16, 7,'Apr"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'May"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jun"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jul"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Aug"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Sep"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Oct"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Nov"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Dec"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jan"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Feb"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Mar"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'YTD',1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '1',1,align = 'C')
	pdf.cell(41,7, 'No. of Reportable Accidents',1)
	pdf.cell(20, 7,str(bp_perv_ytd_data[0]['rytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == bpdata[i]['month']:
				pdf.cell(16, 7,str(bpdata[i]['reportableaccident_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	
	pdf.cell(16, 7,str(bp_ytd_data[0]['rytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '2',1,align = 'C')
	pdf.cell(41,7, 'No. of First Aid Cases',1)
	pdf.cell(20, 7,str(bp_perv_ytd_data[0]['fytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == bpdata[i]['month']:
				pdf.cell(16, 7,str(bpdata[i]['firstaid_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	
	pdf.cell(16, 7,str(bp_ytd_data[0]['fytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '3',1,align = 'C')
	pdf.cell(41,7, 'No. of Near Miss',1)
	pdf.cell(20, 7,str(bp_perv_ytd_data[0]['nytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == bpdata[i]['month']:
				pdf.cell(16, 7,str(bpdata[i]['nearmiss_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(bp_ytd_data[0]['nytd']),1,align = 'C')

# next page
	pdf.add_page()

# 4th table
	pdf.set_font('Arial', '', 11)
	pdf.set_xy(20,17)
	tddata_free_days=find_and_filter('safety_summary',{"plant_id":"1004","fin_year":fin_year,"month":int(prev)},{"Incident_free_days":1,"_id":0})
	tdIncident_free_days=tddata_free_days[0]['Incident_free_days']
	tddata=find_in_collection_sort("safety_summary",{"plant_id":"1004","fin_year":finance_year},[("month",1)])
	
	td_ytd_data=find_in_collection("safety_summary_month",{"plant_id":"1004","fin_year":finance_year})
	
	td_perv_ytd_data=find_and_filter('safety_summary_month',{"plant_id":"1004","fin_year":fin_year-1,"month":3},{"rytd":1,"fytd":1,"nytd":1,"_id":0})
	
	pdf.cell(41,7, 'Plant: Tandur')
	pdf.set_xy(210,17)
	pdf.cell(41,7, 'Accident Free Days:')
	pdf.set_xy(247,17)
	pdf.cell(41,7,str(tdIncident_free_days))
	pdf.set_font('Arial', '', 9)
	pdf.set_y(27)
	pdf.cell(10, 7, 'S.No',1)
	pdf.cell(41,7, 'Performance Indicator',1, align = 'C')
	pdf.cell(20, 7,finyear,1,align = 'C')
	pdf.cell(16, 7,'Apr"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'May"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jun"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jul"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Aug"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Sep"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Oct"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Nov"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Dec"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jan"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Feb"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Mar"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'YTD',1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '1',1,align = 'C')
	pdf.cell(41,7, 'No. of Reportable Accidents',1)
	pdf.cell(20, 7,str(td_perv_ytd_data[0]['rytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == tddata[i]['month']:
				pdf.cell(16, 7,str(tddata[i]['reportableaccident_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(td_ytd_data[0]['rytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '2',1,align = 'C')
	pdf.cell(41,7, 'No. of First Aid Cases',1)
	pdf.cell(20, 7,str(td_perv_ytd_data[0]['fytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == tddata[i]['month']:
				pdf.cell(16, 7,str(tddata[i]['firstaid_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	
	pdf.cell(16, 7,str(td_ytd_data[0]['fytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '3',1,align = 'C')
	pdf.cell(41,7, 'No. of Near Miss',1)
	pdf.cell(20, 7,str(td_perv_ytd_data[0]['nytd']),1,align = 'C')

	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == tddata[i]['month']:
				pdf.cell(16, 7,str(tddata[i]['nearmiss_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(td_ytd_data[0]['nytd']),1,align = 'C')

#5th table
	pdf.set_font('Arial', '', 11)
	pdf.set_xy(20,71)
	gppdata_free_days=find_and_filter('safety_summary',{"plant_id":"2001","fin_year":fin_year,"month":int(prev)},{"Incident_free_days":1,"_id":0})
	gppIncident_free_days=gppdata_free_days[0]['Incident_free_days']
	gppdata=find_in_collection_sort("safety_summary",{"plant_id":"2001","fin_year":finance_year},[("month",1)])
	
	gpp_ytd_data=find_in_collection("safety_summary_month",{"plant_id":"2001","fin_year":finance_year})
	
	gpp_perv_ytd_data=find_and_filter('safety_summary_month',{"plant_id":"2001","fin_year":fin_year-1,"month":3},{"rytd":1,"fytd":1,"nytd":1,"_id":0})
	
	pdf.cell(41,7, 'Plant: Ganeshpahad Power')
	pdf.set_xy(210,71)
	pdf.cell(41,7, 'Accident Free Days:')
	pdf.set_xy(247,71)
	pdf.cell(41,7,str(gppIncident_free_days))
	pdf.set_font('Arial', '', 9)
	pdf.set_y(81)
	pdf.cell(10, 7, 'S.No',1)
	pdf.cell(41,7, 'Performance Indicator',1, align = 'C')
	pdf.cell(20, 7,finyear,1,align = 'C')
	pdf.cell(16, 7,'Apr"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'May"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jun"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jul"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Aug"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Sep"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Oct"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Nov"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Dec"'+str(ye),1,align = 'C')
	pdf.cell(16, 7,'Jan"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Feb"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'Mar"'+str(ye1),1,align = 'C')
	pdf.cell(16, 7,'YTD',1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '1',1,align = 'C')
	pdf.cell(41,7, 'No. of Reportable Accidents',1)
	pdf.cell(20, 7,str(gpp_perv_ytd_data[0]['rytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == gppdata[i]['month']:
				pdf.cell(16, 7,str(gppdata[i]['reportableaccident_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(gpp_ytd_data[0]['rytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '2',1,align = 'C')
	pdf.cell(41,7, 'No. of First Aid Cases',1)
	pdf.cell(20, 7,str(gpp_perv_ytd_data[0]['fytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == gppdata[i]['month']:
				pdf.cell(16, 7,str(gppdata[i]['firstaid_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	
	pdf.cell(16, 7,str(gpp_ytd_data[0]['fytd']),1,align = 'C')
	pdf.ln()
	pdf.cell(10, 7, '3',1,align = 'C')
	pdf.cell(41,7, 'No. of Near Miss',1)
	pdf.cell(20, 7,str(gpp_perv_ytd_data[0]['nytd']),1,align = 'C')
	for i in range(0,len(arr_month)):
		try:
			if arr_month[i] == gppdata[i]['month']:
				pdf.cell(16, 7,str(gppdata[i]['nearmiss_per_month']),1,align = 'C')
		except:
			pdf.cell(16, 7,'-',1,align = 'C')
	pdf.cell(16, 7,str(gpp_ytd_data[0]['nytd']),1,align = 'C')
	
	pdf.set_font('Arial', 'B', 12)
	pdf.set_y(120)
	pdf.cell(16, 7, 'Legend: ')
	pdf.set_font('Arial', 'B', 10)
	pdf.set_y(135)
	pdf.cell(16, 7, 'Reportable Accident: ')
	pdf.set_font('Arial', '', 10)
	pdf.set_xy(65,135)
	pdf.cell(16, 7, 'Incident/Accident occurred and the person injured has not reported the duty to the next shift due to injury.')
	pdf.set_font('Arial', 'B', 10)
	pdf.set_y(145)
	pdf.cell(16, 7, 'First Aid: ')
	pdf.set_font('Arial', '', 10)
	pdf.set_xy(65,145)
	pdf.cell(26, 7, ' Incident/Accident occurred and the person injured has taken first aid treatment and resumed his duty.')
	pdf.set_font('Arial', 'B', 10)
	pdf.set_y(155)
	pdf.cell(16, 7, 'Near Miss: ')
	pdf.set_font('Arial', '', 10)
	pdf.set_xy(65,155)
	pdf.cell(16, 7, ' Incident occurred and no person injured.')
	pdf.set_font('Arial', 'B', 10)
	pdf.set_y(165)
	pdf.cell(16, 7, 'Accident free days:')
	pdf.set_font('Arial', '', 10)
	pdf.set_xy(65,165)
	pdf.cell(16, 7, ' Count of total number of days from which the last reportable accident has occurred. It is one of the measures of safety performance.')
	pdf.set_xy(65,170)
	pdf.cell(16, 7, ' (Calculated from 1st April 2016) ')
	pdf.output('/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Safety/Safety_Summary_Report.pdf','F')
	
	return 'successful'

@zen_reports.route('/send_mail/')
def send_mail():
	# curr_user=current_user.username
	# Name=get_full_name(curr_user)['name']
	year = datetime.date.today().year
	date=datetime.date.today()
	# print "dateeeee",date
	d = datetime.datetime.strptime(date.strftime("%d/%m/%Y"), "%d/%m/%Y")
	d2 = d.replace(day=1) - timedelta(days=1)
	prev = '{:01d}'.format(d2.month)
	# print "mmmmm",int(prev)
	prevmon=int(prev)
	if prevmon == 1:
		month="January"
	elif prevmon == 2:
		month="February"
	elif prevmon == 3:
		month="March"
	elif prevmon == 4:
		month="April"
	elif prevmon == 5:
		month="May"
	elif prevmon == 6:
		month="June"
	elif prevmon == 7:
		month="July"
	elif prevmon == 8:
		month="August"
	elif prevmon == 9:
		month="September"
	elif prevmon == 10:
		month="October"
	elif prevmon == 11:
		month="November"
	elif prevmon == 12:
		month="December"
	receiver='ananthkumar.p@pennacement.com'
	
	receivers=find_in_collection("Safety_report_email_ids",{})
	print receivers
	
	# receivers=receivers.strip().split(',')
	path=os.path.abspath("/home/administrator/PCIL_MYPORTAL_PRODUCTION/pcil_myportal/PCIL_myPortal/zenapp/static/Safety/Safety_Summary_Report.pdf")
	# for r in receivers:
	for i in range(0,len(receivers)):
		USERNAME='myportal@pennacement.com'
		PASSWORD='Penna@123'
		RECEIVER=receiver
		msg = MIMEMultipart()
		msg['Subject'] = "Safety Performance Summary Report -"+str(month)+" "+str(year)
		# msg['preamble']='testing msg'
		server = smtplib.SMTP("smtp.office365.com:587")
		server.starttls()
		server.login(USERNAME,PASSWORD)

		body = email.mime.Text.MIMEText('Dear Sir,<br><br>Please find attached Safety Performance Summary Report of all Plants for '+str(month)+' '+str(year)+'<br><br><br>Thank you.','html')
		msg.attach(body)

		filename='Safety_Summary_Report.pdf'
		fp=open(path,'rb')
		att = email.mime.application.MIMEApplication(fp.read(),_subtype="pdf")
		fp.close()
		att.add_header('Content-Disposition','attachment',filename=filename)
		msg.attach(att)
		try:
			server.sendmail(USERNAME,receivers[i]['email_id'],msg.as_string())
			# print "Mail Has Been Sent Successfully"
			logging.info("Mail Has Been Sent Successfully"+str(receivers[i]['email_id']))
			server.quit()
			R="success"
		except Exception,R:
			print R
	logging.info("Report Generated By System at : "+str(datetime.datetime.now()))
	result={'msg':R}
	return jsonify(result)

@zen_reports.route('/pdf_download/')
def pdf_download():
	user_details = base()
	return render_template('Safety_pdf.html',user_details=user_details) 