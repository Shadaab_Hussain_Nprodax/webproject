from flask import Flask, render_template, redirect, request, flash, url_for,Response,jsonify
from zenapp import * 
from zenapp.modules.user.form import *
from zenapp.dbfunctions import *
import datetime
import hashlib
import gridfs
import uuid
from zenapp.modules.user.userfunctions import *
from flask_login import login_user, current_user
from flask_login import login_required, logout_user
from bson.objectid import ObjectId
import os
import csv
import zipfile
import StringIO
from calendar import monthrange
from pyrfc import *
import logging
from zenapp.modules.leave.lfunctions import *
zen_leave = Blueprint('zenleave', __name__, url_prefix='/leave')


def get_employee_email(e_id):
    data=find_and_filter('Userinfo',{"username":e_id},{"_id":0,'official_email':1})
    try:        
        email_official=data[0]['official_email']
    except:
        pass
    return email_official


###################### For Daily Mail Trigger f Leave Approvals
@zen_leave.route('/leavenotification/', methods = ['GET', 'POST'])
def leavenotification():
    user_details = base() 
    result={}
    managers=[]
    notifications={}
    notifications_waiting={}
    notify_msg_waiting=''
    notify_msg_current=''
    USERNAME='myportal@pennacement.com'
    PASSWORD='Penna@123'
    # return render_template('leavenotification.html',user_details=user_details)"start_date":{"$lte":to_date},"end_date":{"$gte":from_date}
    leavedetails=find_in_collection('Leaves',{'status':'applied','applied_time':{'$gte':datetime.datetime(2016,12,1),'$lte':datetime.datetime(2016,12,12,23,59,59)}})
    for ii in range(len(leavedetails)):
        # print leavedetails[i]['approval_levels'],'approval levesl'
        for jj in range(len(leavedetails[ii]['approval_levels'])):
            if leavedetails[ii]['approval_levels'][jj]['a_id'] not in managers:
                managers.append(leavedetails[ii]['approval_levels'][jj]['a_id'])
                notifications[leavedetails[ii]['approval_levels'][jj]['a_id']]=[]
                notifications_waiting[leavedetails[ii]['approval_levels'][jj]['a_id']]=[]
    for i in range(len(leavedetails)):
        curr_pos=0
        try:
            full_name=get_employee_name(leavedetails[i]['employee_id'])
        except:
            full_name=leavedetails[i]['employee_id']
        for j in range(len(leavedetails[i]['approval_levels'])):
            if leavedetails[i]['approval_levels'][j]['a_status']=='current':
                curr_pos=j+1
                # notifications_current[leavedetails[i]['approval_levels'][j]['a_id']].append({'employee_id':leavedetails[i]['employee_id'],'leave_type':leavedetails[i]['leave_type'],'applied_time':str(leavedetails[i]['applied_time'])})
                notify_msg_current=full_name+' Leave is pending for approval of leave type '+leavedetails[i]['leave_type']+' From '+str(leavedetails[i]['start_date'].date())+' To '+str(leavedetails[i]['end_date'].date())+' Applied on '+str(leavedetails[i]['applied_time'].date())+'.'
                notifications[leavedetails[i]['approval_levels'][j]['a_id']].append(notify_msg_current)
        for l in range(len(leavedetails[i]['approval_levels'])-curr_pos):
            # try:
                # notifications_waiting[leavedetails[i]['approval_levels'][curr_pos+l]['a_id']].append({'employee_id':leavedetails[i]['employee_id'],'leave_type':leavedetails[i]['leave_type'],'applied_time':str(leavedetails[i]['applied_time'])})
            notify_msg_waiting=full_name+' Leave is pending at '+get_employee_name(leavedetails[i]['approval_levels'][curr_pos-1]['a_id'])+' for approval of leave type '+leavedetails[i]['leave_type']+' From '+str(leavedetails[i]['start_date'].date())+' To '+str(leavedetails[i]['end_date'].date())+' Applied on '+str(leavedetails[i]['applied_time'].date())+'.you can approve it here http://myportal.pennacement.com/leave/approvals'
            notifications[leavedetails[i]['approval_levels'][curr_pos+l]['a_id']].append(notify_msg_waiting)
            # except:
                # pass
    for manager in managers:
        noty_msg=''
        for noty_len in range(len(notifications[manager])):
            noty_msg+=str(noty_len+1)+') '+notifications[manager][noty_len]+'\n\n'     
    msg = MIMEText('Dear '+get_employee_name(manager)+',\n\n'+noty_msg+'\n\n\n\nThanks,\nTeam-HR.')
    msg['Subject'] = 'MyPortal Leave Approval Reminders.'
    server = smtplib.SMTP("smtp.office365.com:587")
    server.starttls()
    server.login(USERNAME,PASSWORD)
    try:
        server.sendmail(USERNAME,get_employee_email(manager),str(msg))
        # server.sendmail(USERNAME,'portalsupport1@pennacement.com',str(msg))
        server.quit()
    except Exception,R:
        console.log(R)

    # result[i]={'employee_id':leavedetails[i]['employee_id'],'leave_type':leavedetails[i]['leave_type'],'status':leavedetails[i]['status'],'applied_time':str(leavedetails[i]['applied_time'])} 
    # return jsonify({'result':result,'result_current':notifications,'result_waiting':notifications_waiting,'managers':managers})
    logging.info('Sucessfully Trigerred Daily Email Notifications For Leave Approvals On '+str(datetime.datetime.now().date()))



########################################################## 
####### End of Daily Leave Approval Triggerings.



#get pdf from database and display it in browser
@zen_leave.route('/static/pdf/gridfs/<filename>')
@login_required
def gridfs_pdf(filename):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    thing = fs.get_last_version(filename=filename)
    return Response(thing, mimetype='application/pdf')

#saving pdf in databse
def file_save_gridfs(file_data,ctype,ctag):
    db = dbconnect()
    fs = gridfs.GridFS(db)
    uid = current_user.username #current employee id
    #check is there any previous docs 
    #if it is there remove it
    #save the latest doc
    userpic = find_one_in_collection('fs.files',{"uid" :uid ,"tag" :ctag})
    if userpic:
        del_doc('fs.chunks',{"files_id":userpic['_id']})
        del_doc('fs.files',{"_id":userpic['_id']})
    file_ext = file_data.filename.rsplit('.', 1)[1]
    fs.put(file_data.read(), content_type=ctype, filename = str(uuid.uuid4())+'.'+file_ext, uid = uid,
        tag = ctag)



#Code for Displaying holidays list
@zen_leave.route('/holidays/', methods = ['GET', 'POST'])
@login_required
def holidays():
    p =check_user_role_permissions('546f2038850d2d1b00023b21')
    if p: 
        user_details = base() 

        uid= current_user.get_id() # employee mongodb id
        uname=current_user.username # employee id
        #get required details from database
        user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid)},
            {"calendar_code":1,"designation_id":1,"_id":0,"plant_id":1})
        #get data to show holidays and weeekly halfs in calendar
        #print user_data
        current_year=datetime.datetime.now().year
        current_month=datetime.datetime.now().month
        #print user_data[0]['calendar_code']

        calendar=find_and_filter('Calendar',{"calendar_id" : str(user_data[0]['calendar_code']),
            "calendar_year" : str(current_year)},{"holidays":1,'_id':0})

        array_year=[]
        array_month=[]
        ##print calendar[0]['holidays']
        for e in calendar[0]['holidays']:
            new_date = datetime.datetime.strftime(e['date'],"%d/%m/%Y")
            array_year.append({'date':new_date,'holiday':e['holiday']})
            

            if current_month == e['date'].month:
                array_month.append({'date':datetime.datetime.strftime(e['date'],"%d/%m/%Y"),
                    'holiday':e['holiday']})
            else:
                continue
        
    return render_template('holidays_list.html', user_details=user_details,year_holidays=array_year,
        month_holidays=array_month,year=current_year)



# apply leave or attendance code moved on 10_June_2016
################## New Enhancement For Adding Permissions
@zen_leave.route('/', methods = ['GET', 'POST'])
@zen_leave.route('/apply/', methods = ['GET', 'POST'])
@login_required
def apply_leave():
                
    p =check_user_role_permissions('546f2038850d2d1b00023b21')
    if p:
        debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
        if debug_flag and debug_flag['debug']=='on':
            debug_flag=debug_flag['debug_status']['apply_leave']
        else:
            debug_flag="off" 
        user_details = base()  
        uid= current_user.get_id() # employee mongodb id
        uname=current_user.username # employee id
        #get required details from database
        user_data = find_and_filter('Userinfo',{"_id":ObjectId(uid)},{"calendar_code":1,"designation_id":1,"_id":0,"plant_id":1})
        #get data to show holidays and weeekly halfs in calendar
        calendar=get_calendar_data(uname)
        #leave quota of an employee
        leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":uname})
        if leave_quota:
            leave_quota['sls_balance']=leave_quota['available_sls']-leave_quota['sls_taken']
            leave_quota['els_balance']=leave_quota['available_els']-leave_quota['els_taken']
            leave_quota['cls_balance']=leave_quota['available_cls']-leave_quota['leaves_taken']
        else:
            flash('Leave Quota is not available at the moment.please try again after some time.','alert-danger')
            return redirect(url_for('zenuser.index'))
        permissions_details= count_permissions(user_data[0]['plant_id'],uname)
        in_btw_days = []
        if request.method == 'POST':
            form=request.form
            logging.info(str(form)+' --- Empid : '+str(uname)+' of Plant '+str(user_details['plant_id']))#for debuging info
            if "end_date" in form and form['end_date'] !='':
                end_date=form['end_date']
            else:
                end_date=form['start_date'] 
            if end_date.split('/')[2]==str(datetime.datetime.now().date().year):
                leave_att = form['leave']
                #check is there any halfdays
                if 'halfday' in form:
                    list1= form.getlist('halfcheck')
                else:
                    list1 = []
                #if it is a leave check these conditions
                if(leave_att == '1'):
                    previous_leaves=business_rule_7(uname,form['start_date'],form['end_date'])
                    if debug_flag=='on':
                       logging.info(str(previous_leaves)+' --- B7 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                    if previous_leaves > 0 :
                        flash('Previous leave dates are colliding with applied leave,Please select another date and apply','alert-danger')
                        return redirect(url_for('zenleave.apply_leave'))
                    # employee cannot apply a leave if same type of previous leave is under process
                    br1=business_rule_1(uname,form['leave_type'])
                    if debug_flag=='on':
                        logging.info(str(br1)+' --- B1 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                    if br1 != '0':
                        flash(br1,'alert-danger')
                        return redirect(url_for('zenleave.apply_leave'))
                    # Check CL business rule
                    if form['leave_type'] == 'CL':
                        employee_group=find_and_filter('Userinfo',{"_id":ObjectId(uid)},{"ee_group":1,'_id':0})
                        #no of CLs available to apply now 
                        leaves_available=business_rule_3(uname,employee_group[0]['ee_group'])
                        if debug_flag=='on':
                            logging.info(str(leaves_available)+' --- B3 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                        #total no of days in a applied leave excluding holidays and weekly halfs
                        total_days=total_no_of_days_leave(uname,form['start_date'],form['end_date'],list1)
                        #print "totaldays",total_days
                        if total_days > leaves_available:
                            flash('Please check your quota and retry','alert-danger')
                            return redirect(url_for('zenleave.apply_leave'))        
                        if employee_group[0]['ee_group'] == "T":
                            if total_days > 1.5:
                                flash('Casual Leave cannot be applied for more than 1.5 Days','alert-danger')
                                return redirect(url_for('zenleave.apply_leave'))
                        elif employee_group[0]['ee_group'] == "A" or employee_group[0]['ee_group'] == "S":
                            if total_days > 3:
                                flash('Casual Leave cannot be applied for more than 3 Days','alert-danger')
                                return redirect(url_for('zenleave.apply_leave'))
                            no_cls_before=business_rule_8(uname,form['start_date'])
                            if debug_flag=='on':
                                logging.info(str(no_cls_before)+' --- B8 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                            if no_cls_before+total_days > 3 :
                                flash('Casual Leave cannot be applied for more than 3 Days','alert-danger')
                                return redirect(url_for('zenleave.apply_leave'))
                            no_cls_after=business_rule_10(uname,form['end_date'])
                            if debug_flag=='on':
                                logging.info(str(no_cls_after)+' --- B10 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                            if no_cls_after+total_days > 3 :
                                flash('Casual Leave cannot be applied for more than 3 Days','alert-danger')
                                return redirect(url_for('zenleave.apply_leave'))                
                        #CL should not followed by SL or EL
                        allow_applied_leave=business_rule_5(uname,form['start_date'],form['end_date'])
                        if debug_flag=='on':
                            logging.info(str(allow_applied_leave)+' --- B5 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id'])) 
                        if allow_applied_leave > 0 :
                            flash('Casual leave should not be followed by Sick leave or Earned leave','alert-danger')
                            return redirect(url_for('zenleave.apply_leave'))
                    #check EL business rule
                    if form['leave_type'] == 'EL':
                        total_days=total_no_of_days_leave(uname,form['start_date'],form['end_date'],list1)
                        # employee cannot apply less than 3 ELs continously 
                        if total_days < 3:
                            flash('Minimum of 3 Earned Leaves should apply','alert-danger')
                            return redirect(url_for('zenleave.apply_leave'))
                        if 'lta' in form:
                            if total_days < 4:
                                flash('Minimum of 4 Earned Leaves should be applied for LTA','alert-danger')
                                return redirect(url_for('zenleave.apply_leave'))
                            else:
                                found=business_rule_11(uname)
                                if debug_flag=='on':
                                    logging.info(str(found)+' --- B11 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                                if found > 0:
                                    flash('LTA Can Applied Only Once For Financial Year','alert-danger')
                                    return redirect(url_for('zenleave.apply_leave'))
                                else:
                                    found1 = business_rule_12(uname)
                                    if debug_flag=='on':
                                        logging.info(str(found1)+' --- B12 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                                    if found1 > 0:
                                        flash('Minimum 10 Months Gap Should be Maintained for two Consecutive LTA','alert-danger')
                                        return redirect(url_for('zenleave.apply_leave'))
                        # the maximum of 4 spells can apply in a year
                        if business_rule_4(uname) >= 4:
                            flash('You have already applied Earned Leaves 4 times','alert-danger')
                            return redirect(url_for('zenleave.apply_leave'))
                        #EL should not followed by CL 
                        allow_applied_leave=business_rule_6(uname,form['start_date'],form['end_date'])
                        if debug_flag=='on':
                            logging.info(str(allow_applied_leave)+' --- B6 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                        if allow_applied_leave > 0 :
                            flash('Earned leave should not be followed by Casual leave','alert-danger')
                            return redirect(url_for('zenleave.apply_leave'))
                    if form['leave_type'] == 'SL' :        
                        #EL or SL should not followed by CL 
                        allow_applied_leave=business_rule_6(uname,form['start_date'],form['end_date'])
                        if debug_flag=='on':
                            logging.info(str(allow_applied_leave)+' --- B6 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id'])) 
                        if allow_applied_leave > 0 :
                            flash('Sick leave should not be followed by Casual leave','alert-danger')
                            return redirect(url_for('zenleave.apply_leave'))

                        leave_days=total_no_of_days_leave(uname,form['start_date'],form['end_date'],list1)
                        if leave_days > 3:
                            if request.files['doc']:
                                input_file = request.files['doc']
                                #save doctor certificate
                                file_save_gridfs(input_file,"application/pdf","doc")
                            else:
                                flash('Please update the Doctor Certificate','alert-danger')
                                return redirect(url_for('zenleave.apply_leave'))

                    br2=business_rule_2(uname,form['start_date'],form['end_date'],list1,form['leave_type'],'','')
                    if debug_flag=='on':
                        logging.info(str(br2)+' --- BR2 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                    if br2 != '0':
                        flash(br2,'alert-danger')
                        return redirect(url_for('zenleave.apply_leave'))
                    leave_type=get_leave_type(form['leave_type']) #get leave type from database
                    attribute="leave"
                    msg= 'Leave applied successfully'
                elif str(form['leave_type'])=='Permission':
                    # br2=business_rule_2(uname,form['start_date'],form['start_date'],list1,form['leave_type'],form['fromtime']+':00',form['totime']+':00')
                    # if br2!=0:
                    #     flash(br2,'alert-danger')
                    #     return redirect(url_for('zenleave.apply_leave'))
                    attribute='permission'
                    end_date=''
                    leave_type=form['leave_type']
                    permissions_count=business_rule_13(uname,str(form['start_date']),str(form['fromtime']),str(form['totime']),user_data[0]['plant_id']) 
                    if debug_flag=='on':
                        logging.info(str(permissions_count)+' --- B13 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                    if permissions_count[0]==1:
                        msg='You have sucessfully applied for '+leave_type+' on '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' .'
                    else:
                        flash(permissions_count[1],'alert-danger')
                        return redirect(url_for('zenleave.apply_leave'))
                else:               
                    if str(form['leave_type']) == "1":#on Tour
                        from_time=""
                        to_time=""
                        start_date=form['start_date']
                        end_date=form['end_date']
                    elif str(form['leave_type']) == "2" and form['end_date'] == "":#on Duty single day
                        start_date=str(form['start_date'])
                        end_date=str(form['start_date'])
                        from_time=form['fromtime']
                        to_time=str(form['totime'])
                    elif str(form['leave_type']) == "2" and str(form['multi_att']) == "3":
                        start_date=str(form['start_date'])
                        end_date=str(form['end_date'])
                        from_time=form['fromtime']
                        to_time=str(form['totime'])
                    br2=business_rule_2(uname,form['start_date'],form['start_date'],list1,form['leave_type'],from_time+':00',to_time+':00')
                    if debug_flag=='on':
                        logging.info(str(br2)+' --- BR_P_2 response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                    if br2 != '0':
                        flash(br2,'alert-danger')
                        return redirect(url_for('zenleave.apply_leave'))
                    previous_attendance=business_rule_7_1(uname,start_date,end_date,from_time,to_time)
                    if debug_flag=='on':
                        logging.info(str(previous_attendance)+' --- B7_1_per response of Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                    if previous_attendance[0] !=0:
                        if from_time != "" and end_date != start_date:#attendance onduty Multiple days
                            flash('You have already applied Leave/Attendance from '+ previous_attendance[4]+" and " +previous_attendance[5]+" from "+previous_attendance[2]+" to "+previous_attendance[3],'alert-danger')
                        elif from_time != "" and end_date == start_date:#OD attendance single day
                            flash('You have already applied Leave/Attendance on '+ form['start_date']+" from "+previous_attendance[2]+" to "+previous_attendance[3],'alert-danger')
                        elif from_time == "":
                            if previous_attendance[1]==previous_attendance[2]:
                            # flash('You have already applied attendance in between the fallowing days i.e... on '+ form['start_date']+" and "+form['end_date'] ,'alert-danger')
                                flash('You have already applied Leave/Attendance on '+previous_attendance[1],'alert-danger')
                            elif previous_attendance[1]!= previous_attendance[2]:
                                # flash('You have already applied attendance in between the fallowing days from '+previous_attendance[1]+" to "+previous_attendance[2],'alert-danger')
                                flash('You Have Applied Leave/Attendance Already,Please Select A Different Date And Try Again.','alert-danger')
                        else:
                            flash('exception leave not applied :(','alert-danger')
                        return redirect(url_for('zenleave.apply_leave'))
                    leave_type=get_attendance_type(form['leave_type']) #get attendance type from database
                    attribute="attendance"
                    msg=leave_type+' applied successfully'
                    if debug_flag=='on':
                        logging.info('Leave applied sucessfully by Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                # get employee approval levels
                b=find_and_filter('Positions',{"position_id":user_data[0]['designation_id']},{"_id":0,"levels":1})
                d=[]
                #get approval time of each level
                leave_time=get_leave_time(user_data[0]['plant_id'])
                if leave_time == 0:
                    time='None'
                else:
                    time=datetime.datetime.now()+datetime.timedelta(hours = leave_time)
                    #print "checking time delta",time
                    # get rm ids of this employee
                new_b=str(b)
                if 'levels' in new_b:
                    count_levels=len(b[0]['levels'])
                    for i in range(0,count_levels):
                        c=find_and_filter('Userinfo',{"designation_id":b[0]['levels'][i]},{"username":1,"employee_status":1,"_id":0})
                        for j in range(0,len(c)):
                            if c[j]['employee_status'] == 'active':
                                if i == 0:
                                    d= d+[{"a_status":"current","a_id":c[j]['username'],"a_discuss":'0',"a_time":time}]

                                else:
                                    d= d+[{"a_status":"waiting","a_id":c[j]['username'],"a_discuss":'0'}] 
                    
                    if 'multi_att' in form:      
                        array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                            "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                               "end_date":datetime.datetime.strptime(end_date,"%d/%m/%Y"),
                                               "applied_time":datetime.datetime.now(),
                            "halfdays":list1, "employee_id":current_user.username,"status":"applied","approval_levels":d,
                            "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"from_time":"00:00","to_time":"23:45","hold_status":"0"}
                    elif form['leave_type'] == "Permission":
                        array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                            "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                            "end_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                               "applied_time":datetime.datetime.now(),
                            "halfdays":list1, "employee_id":current_user.username,"status":"applied","approval_levels":d,
                            "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0"}
                    else:
                        array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                            "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                               "end_date":datetime.datetime.strptime(end_date,"%d/%m/%Y"),
                                               "applied_time":datetime.datetime.now(),
                            "halfdays":list1, "employee_id":current_user.username,"status":"applied","approval_levels":d,
                            "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0"}
                    #if attendance From time and To Time Need to be selected.
                    if len(form['fromtime'])!=0 and len(form['totime'])!=0:
                                        array['from_time']=form['fromtime']
                                        array['to_time']=form['totime']            
                                            #"in_between_days":in_btw_days
                                            #"start_date":form['start_date'],"end_date":form['end_date']
                                            #"start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),"end_date":datetime.datetime.strptime(form['end_date'],"%d/%m/%Y")
                    #save the applied leave or attendance 
                    if 'lta' in form:
                        array['lta']="1"                 
                    save_collection('Leaves',array)
                    if debug_flag=='on':
                        logging.info(str(array)+' --- saving leave details to DB - '+str(uname)+' of location '+str(user_details['plant_id']))
                    #get rm1 id
                    if leave_type=='Permission':
                        d=find_and_filter('Userinfo',{"designation_id":b[0]['levels'][0]},{"username":1,"_id":0})
                        message= get_employee_name(uname)+' applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+',it is pending for your approval.\n You can approve the leave by clicking on this link : http://myportal.pennacement.com/leave/approvals/'

                        message1= 'You have applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' .'
                        #send notification to employee
                        notification(uname,message1)
                        sms1='You have successfully applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' .'
                        #send email/sms to employee
                        get_employee_email_mobile(uname,'My Portal Leave Notification',message1,sms1)
                        #send notification to rm1
                        notification(d[0]['username'],message)
                        sms2=get_employee_name(uname)+' has applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' .'
                        #send email/sms to rm1
                        get_employee_email_mobile(d[0]['username'],'My Portal Leave Notification',message,sms2)
                        try:
                           notification(get_hr_id(user_data[0]['plant_id']),message)
                        except:
                            logging.info('Hr Has been changed for Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                        #send email/sms to hr
                        # get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message,sms2)
                    else: 
                        d=find_and_filter('Userinfo',{"designation_id":b[0]['levels'][0]},{"username":1,"_id":0})
                        message= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+end_date+'it is pending for your approval.\n You can approve the leave by clicking on this link : http://myportal.pennacement.com/leave/approvals/'

                        message1= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+end_date
                        #send notification to employee
                        notification(uname,message1)
                        sms1='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+end_date
                        #send email/sms to employee
                        get_employee_email_mobile(uname,'My Portal Leave Notification',message1,sms1)
                        #send notification to rm1
                        notification(d[0]['username'],message)
                        sms2=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+end_date
                        #send email/sms to rm1
                        get_employee_email_mobile(d[0]['username'],'My Portal Leave Notification',message,sms2)
                        #send notification to hr
                        try:
                            notification(get_hr_id(user_data[0]['plant_id']),message)
                        except:
                            logging.info('Hr Has been changed for Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                        #send email/sms to hr
                        # get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message,sms2)
                else:
                    #auto approval of leave/attendance if there are no approval levels
                    array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                        "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                           "end_date":datetime.datetime.strptime(form['end_date'],"%d/%m/%Y"),
                                           "applied_time":datetime.datetime.now(),
                        "halfdays":list1, "employee_id":current_user.username,"status":"approved","approval_levels":d,
                        "hr_id":user_data[0]['plant_id']} 
                                            #converted strt and end date to datetime here and above.
                    # save leave or attendance
                    if len(form['fromtime'])!=0 and len(form['totime'])!=0:
                                        array['from_time']=form['fromtime']
                                        array['to_time']=form['totime']
                    if 'lta' in form:
                        array['lta']="1"

                    save_collection('Leaves',array)
                    if debug_flag=='on':
                        logging.info('saving leave details to DB - '+str(uname)+'(user has no approval levesl) of location '+str(user_details['plant_id']))
                    message1= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+end_date
                    message2= get_employee_name(uname)+' '+leave_type+' approved '+' from '+form['start_date']+' to '+end_date
                    message3= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+end_date
                    message4= 'Your '+leave_type+' approved '+' from '+form['start_date']+' to '+end_date
                    #notification to employee
                    notification(uname,message3)
                    sms3='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+end_date
                    #email/sms to employee
                    get_employee_email_mobile(uname,'My Portal Leave Notification',message3,sms3)
                    notification(get_hr_id(user_data[0]['plant_id']),message1)
                    sms1=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+end_date
                    get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message1,sms1)
                    notification(uname,message4)
                    sms4='Your '+leave_type+' from '+form['start_date']+' to '+end_date+'has been approved'
                    get_employee_email_mobile(uname,'My Portal Leave Notification',message4,sms4)
                    try:
                        notification(get_hr_id(user_data[0]['plant_id']),message2)
                    except:
                        logging.info('Hr Has been changed for Empid - '+str(uname)+' of location '+str(user_details['plant_id']))
                    get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message2,sms4)
                flash(msg,'alert-success')
                return redirect(url_for('zenleave.apply_leave'))
            else:
                flash('Leaves Can Be Applied Only For Current Year At The Moment i.e '+str(datetime.datetime.now().date().year),'alert-danger')
                return redirect(url_for('zenleave.apply_leave'))
        return render_template('apply_leave.html', user_details=user_details,permissions_duration=permissions_details[3],calendar=calendar,leave_quota=leave_quota,permissions_details=permissions_details)
    else:
        return redirect(url_for('zenuser.index'))


@zen_leave.route('/approvals/', methods = ['GET', 'POST'])
@login_required
def leaveapprovals():
    p =check_user_role_permissions('546f204b850d2d1b00023b22')
    if p:
        debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
        if debug_flag and debug_flag['debug']=='on':
            debug_flag=debug_flag['debug_status']['apply_leave']
        else:
            debug_flag="off"
        user_details=base()
        uid = current_user.username
        db=dbconnect()
        employee_att = db.Leaves.aggregate([{'$match' : {'status': "applied",'hr_status':"unhold"}},
        { '$unwind': '$approval_levels' },
                {'$match' : {'approval_levels.a_id' : uid,'$or':[{'approval_levels.a_status':'current'},{'approval_levels.a_status':'waiting'}]}}])
        employee_att=list(employee_att)
        a=[]
        for e in employee_att:
            e['fullname']=get_employee_name(e['employee_id'])
            e['reason']=e['leave_reason']
            e['start_date']=datetime.datetime.strftime(e['start_date'],"%d/%m/%Y")
            e['end_date']=datetime.datetime.strftime(e['end_date'],"%d/%m/%Y")
            if e['attribute'] == 'attendance':
                e['leave']=get_attendance_type(e['leave_type'])
            elif e['attribute'] == 'permission':
                e['leave']='Permission'
            else:
                e['leave']=get_leave_type(e['leave_type'])
                if e['leave_type'] == 'SL':
                    leave_days=total_no_of_days_leave(e['employee_id'],e['start_date'],e['end_date'],e['halfdays'])
                    if leave_days > 3:
                        #print e['employee_id']
                        doc_cert = find_one_in_collection('fs.files',{"uid" :e['employee_id'],"tag":"doc"})
                        if doc_cert is not None:
                            e['doc_url']='/leave/static/pdf/gridfs/'+doc_cert['filename']
            a=a+[e]
        if request.method == 'POST':
            e_attribute=None
            form=request.form
            c_leave =find_and_filter('Leaves',{"_id":ObjectId(form['leaveid'])},{"_id":0,"approval_levels":1,"employee_id":1,"attribute":1,
                                                                                            "plant_id":1,"leave_type":1,"start_date":1,"end_date":1,"halfdays":1})
            count_a_levels = len(c_leave[0]['approval_levels'])
            for i in range(0,count_a_levels):
                if c_leave[0]['approval_levels'][i]['a_status'] == 'current':
                    j=i
                    employee_name = get_employee_name(c_leave[0]['employee_id'])
                    e_attribute = c_leave[0]['attribute']
                if c_leave[0]['approval_levels'][i]['a_id']==uid:
                    curr_app_id=[i,uid]
                    temp_j=i #for making the above approval levels status as approved
                    j=i
                # print c_leave[0]['approval_levels'][now]['a_status'],'original status'
            for le in range(int(curr_app_id[0])):
                if c_leave[0]['approval_levels'][le]['a_status']=='current':
                    logging.info('the leave is already approved by HOD'+str(j))
            if e_attribute == 'attendance':
                leave_type=get_attendance_type(c_leave[0]['leave_type'])
            elif e_attribute == 'permission':
                leave_type='Permission'
            else:
                leave_type=get_leave_type(c_leave[0]['leave_type']) 
            if form['submit']== 'Approve':
                for now in range(temp_j):
                    if c_leave[0]['approval_levels'][now]['a_status']=='current' or c_leave[0]['approval_levels'][now]['a_status']=='waiting':
                        arry_4={"approval_levels."+str(now)+".a_status":"approved","approval_levels."+str(now+1)+".a_time":datetime.datetime.now()}
                        update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry_4})
                if j == count_a_levels-1:
                    #will come to this loop at the final approval level
                                        # in the below code c_leave[0]['start_date'] and c_leave[0]['end_date'] converted to string format
                                        #because in DB they are stored as datetime.datetime format
                                        
                    arry3={"approval_levels."+str(j)+".a_status":"approved","status":"approved","sap":"1","approval_levels."+str(j)+".a_time":datetime.datetime.now()}
                    update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry3})
                    if debug_flag=='on':
                        logging.info('Leave details saved to DB successfully of employee : '+c_leave[0]['employee_id'])
                    #updated approved leaves to leave quota collection
                    if c_leave[0]['leave_type']== 'SL':
                        n_leaves=total_no_of_days_el(c_leave[0]['employee_id'],datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y"),
                                                                             datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y"),c_leave[0]['halfdays'])
                                                
                    else:
                        n_leaves=total_no_of_days_leave(c_leave[0]['employee_id'],datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y"),
                                                                                datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y"),c_leave[0]['halfdays'])
                        
                    update_approved_leaves_in_leave_quota(c_leave[0]['employee_id'],c_leave[0]['leave_type'],n_leaves)
                    logging.info('Leaves Updated To Leave Quota sucessfully for employee : '+c_leave[0]['employee_id'])
                    if e_attribute == 'leave':
                        get_leave_chart_data(c_leave[0]['employee_id'],c_leave[0]['start_date'],c_leave[0]['end_date'],c_leave[0]['halfdays'],c_leave[0]['leave_type'])
                                                
                    msg=get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been Approved'
                    message1='Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been Approved'
                    message= get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been approved by '+get_employee_name(uid)
                    notification(c_leave[0]['employee_id'],message1)
                    sms1='Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been approved by '+get_employee_name(uid)
                    get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message1,sms1)
                    try:
                        notification(get_hr_id(c_leave[0]['plant_id']),message)
                    except:
                        logging.info('Hr has been changed for employee : '+c_leave[0]['employee_id'])
                    sms2=get_employee_name(c_leave[0]['employee_id'])+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been approved by '+get_employee_name(uid)
                    get_employee_email_mobile(get_hr_id(c_leave[0]['plant_id']),'My Portal Leave Notification',message,sms2)
                else:### when more than 2 levels
                    leave_time=get_leave_time(get_plant_id(c_leave[0]['employee_id']))
                    if leave_time == 0:
                        time='None'
                    else:
                        leave=find_one_in_collection('Leaves',{'_id':ObjectId(form['leaveid'])})
                        try:
                            time=leave['approval_levels'][j]['a_time']
                        except:
                            time=''
                        
                    arry4={"approval_levels."+str(j)+".a_status":"approved","approval_levels."+str(j+1)+".a_status":"current",
                       "approval_levels."+str(j+1)+".a_time":time}
                    update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry4})
                    msg=get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been approved and is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
                    message2='Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been approved by '+get_employee_name(uid)+' and is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
                    message= employee_name+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been approved by '+get_employee_name(uid)+'and is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
                    notification(c_leave[0]['employee_id'],message2)
                    sms1='Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
                    get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message2,sms1)
                    try:
                        notification(get_hr_id(c_leave[0]['plant_id']),message)
                    except:
                        logging.info('Hr has Been Changed For The Employee : '+c_leave[0]['employee_id'])
                    sms2=get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' is pending for approval by '+get_employee_name(c_leave[0]['approval_levels'][j+1]['a_id'])
                    get_employee_email_mobile(get_hr_id(c_leave[0]['plant_id']),'My Portal Leave Notification',message,sms2)
                    message1= get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been approved by '+get_employee_name(uid)+'and it is pending for your approval.\n You can approve the leave by clicking on this link : http://myportal.pennacement.com/leave/approvals/'
                    notification(c_leave[0]['approval_levels'][j+1]['a_id'],message1)
                    sms3=get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' is pending for approval by you'
                    get_employee_email_mobile(c_leave[0]['approval_levels'][j+1]['a_id'],'My Portal Leave Notification',message1,sms3)
            elif form['submit'] == 'Reject':
                arry2={"approval_levels."+str(j)+".a_status":"rejected","status":"rejected"}
                update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry2})
                msg=employee_name+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been rejected'
                message1='Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been rejected by '+get_employee_name(uid)
                message= get_employee_name(c_leave[0]['employee_id'])+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been rejected by '+get_employee_name(uid)
                notification(c_leave[0]['employee_id'],message1)
                sms1='Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been rejected by '+get_employee_name(uid)
                get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message1,sms1)
                try:
                    notification(get_hr_id(c_leave[0]['plant_id']),message)
                except:
                    logging.info('Hr has Been Changed For The Employee : '+c_leave[0]['employee_id'])
                sms2=get_employee_name(c_leave[0]['employee_id']) +' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been rejected by '+get_employee_name(uid)
                get_employee_email_mobile(get_hr_id(c_leave[0]['plant_id']),'My Portal Leave Notification',message,sms2)
            elif form['submit'] == 'Discuss':
                arry1={"approval_levels."+str(j)+".a_discuss":"1"}
                update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': arry1})
                msg= 'You have been successfully raised a request to DISCUSS with '+ employee_name
                message= 'Meet '+get_employee_name(uid)+' to discuss about your '+leave_type
                notification(c_leave[0]['employee_id'],message)
                sms='Meet '+get_employee_name(uid)+' to discuss about your '+leave_type
                get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,sms)
                notification(uid,msg)
            flash(msg,'alert-success')
            return redirect(url_for('zenleave.leaveapprovals'))
        # print a,' employee details-----------------------------'
        return render_template('leave_approvals.html', user_details=user_details, employee_att=a)
    else:
        return redirect(url_for('zenuser.index'))

@zen_leave.route('/holdleaves/', methods = ['GET', 'POST'])
@login_required
def holdleaves():
    p =check_user_role_permissions('547c7e0157fbb30ed690336e')
    if p:
        user_details=base()
        uid = current_user.username
        employee_att=find_in_collection('Leaves',{"status": "applied","plant_id":get_plant_id(uid),"hold_status":"0"})
        a=[]
        for e in employee_att:
            e['fullname']=get_employee_name(e['employee_id'])
            e['reason']=e['leave_reason']
            if e['attribute'] == 'attendance':
                e['leave']=get_attendance_type(e['leave_type'])
            elif e['attribute'] == 'permission':
                e['leave']='Permission'
            else:
                e['leave']=get_leave_type(e['leave_type'])          
            a=a+[e]
        if request.method == 'POST':
            form=request.form
            c_leave =find_and_filter('Leaves',{"_id":ObjectId(form['leaveid'])},{"_id":0,"employee_id":1,"attribute":1,"approval_levels":1,"leave_type":1,"start_date":1,"end_date":1})
            employee_name = get_employee_name(c_leave[0]['employee_id'])
            e_attribute = c_leave[0]['attribute']
            if e_attribute == 'attendance':
                leave_type=get_attendance_type(c_leave[0]['leave_type'])
            elif e_attribute == 'permission':
                leave_type='Permission'
            else:
                leave_type=get_leave_type(c_leave[0]['leave_type']) 
            count_a_levels = len(c_leave[0]['approval_levels'])
            for i in range(0,count_a_levels):
                if c_leave[0]['approval_levels'][i]['a_status'] == 'current':
                    j=i
            if form['submit']== 'Hold':
                update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"hr_status":"hold","approval_levels."+str(j)+".a_hold_time":datetime.datetime.now()}})
                msg= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been hold successfully'
                message= 'Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been hold by HR'
                message1= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been hold by HR'
                notification(c_leave[0]['employee_id'],message)
                get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,message)
                notification(uid,message1)
                get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
            elif form['submit']== 'Unhold':
                update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"hr_status":"unhold","hold_status":"1","approval_levels."+str(j)+".a_unhold_time":datetime.datetime.now()}})
                msg= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been unhold successfully'
                message= 'Your '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been unhold by HR'
                message1= employee_name+' '+leave_type+' from '+c_leave[0]['start_date']+' to '+c_leave[0]['end_date']+' has been unhold by HR'
                notification(c_leave[0]['employee_id'],message)
                get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,message)
                notification(uid,message1)
                get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
            flash(msg,'alert-success')  
            return redirect(url_for('zenleave.holdleaves'))
        return render_template('hold_leaves.html', user_details=user_details, employee_att=a)
    else:
        return redirect(url_for('zenuser.index'))

@zen_leave.route('/history/')
@login_required
def leave_history():
    #print "Hello"
    p =check_user_role_permissions('546f2071850d2d1b00023b24')
    if p:
        user_details=base()
        uid = current_user.username
        #print "UID",uid
        current_year=datetime.datetime.now().year
        lh=find_in_collection('Leaves',{"employee_id":uid,'$or':[{"status":"approved"},{"status":"rejected"}]})
        a=[]
        for e in lh:
            e['start_date']=datetime.datetime.strftime(e['start_date'],"%d/%m/%Y")
            e['end_date']=datetime.datetime.strftime(e['end_date'],"%d/%m/%Y")
            e['reason']=e['leave_reason']
            if e['attribute'] == 'attendance':
                e['leave']=get_attendance_type(e['leave_type'])
            elif e['attribute'] == 'permission':
                e['leave']='Permission'
            else:
                e['leave']=get_leave_type(e['leave_type'])          
            a=a+[e]
        #chartdata=find_and_filter('ChartData',{"employee_id":uid,"year":current_year},{"_id":0,"chartdata":1})
        return render_template('leave_history.html', user_details=user_details,lh=a)#chartdata=chartdata[0]['chartdata']
    else:
        return redirect(url_for('zenuser.index'))

@zen_leave.route('/reports')
@login_required
def reports():
    p =check_user_role_permissions('5487ed0757fbb30f702709c0')
    if p:
        user_details=base()
        uid = current_user.username
        lh=find_in_collection('Leaves',{"plant_id":get_plant_id(uid),'$or':[{"status":"approved"},{"status":"rejected"}]})
        a=[]
        for e in lh:
            e['fullname']=get_employee_name(e['employee_id'])
            e['reason']=e['leave_reason']
            if e['attribute'] == 'attendance':
                e['leave']=get_attendance_type(e['leave_type'])
            elif e['attribute'] == 'permission':
                e['leave']='Permission'
            else:
                e['leave']=get_leave_type(e['leave_type'])          
            a=a+[e]
        return render_template('reports.html', user_details=user_details,lh=a)
    else:
        return redirect(url_for('zenuser.index'))


@zen_leave.route('/get_all_plants/', methods = ['GET', 'POST'])
def get_all_plants():
    plants=find_and_filter('Plants',{},{"plant_id":1,"_id":0,"plant_desc":1})
    result={"result":plants}
    return jsonify(result)


@zen_leave.route('/levels/', methods = ['GET', 'POST'])
@login_required
def levels():
    p =check_user_role_permissions('546f2097850d2d1b00023b26')
    q =check_user_role_permissions('573d5a183e85a7622d018684')
    user_details=base()
    uid= current_user.username
    plant_id=get_plant_id(uid)
    usr_no_pos=[]
    if p and plant_id != "HY":        
        p_employees = find_and_filter('Userinfo',{"plant_id":plant_id},
            {"_id":0,"username":1,"designation_id":1,"f_name":1,"l_name":1,"m1_pid":1,"m2_pid":1,"m3_pid":1,
                "m4_pid":1,"m5_pid":1,"m6_pid":1,"m7_pid":1,"m1_name":1,
                "m2_name":1,"m3_name":1,"m4_name":1,"m5_name":1,
                "m6_name":1,"m7_name":1})
        noRm={'$and':[{"m6_pid":""},{"m5_pid":""},{"m4_pid":""},{"m3_pid":""},{"m2_pid":""},{"m1_pid":""}]}
        employee_att=find_and_filter('Userinfo',noRm,{'_id':0,'username':1,'employee_status':1,'f_name':1,'l_name':1})
        a=[]
        for p in p_employees:
            # logging.info("Employee position:"+str(p['designation_id'])) 
            q=find_one_in_collection('Positions',{"position_id":p['designation_id']})
            # #print q
            if q:
                if 'levels' in q:    #will give all the employes who are having levels
                    p['levels']= q['levels']    #assigning levels of employee from positions coll to user coll
                a=a+[p] 
            else:
                usr_no_pos.append(str(p['designation_id']))

            #appending all users RM & levels info to array
            if request.method == 'POST':
                form=request.form
                for i in range(0,len(p_employees)):
                    e_id = p_employees[i]['username']
                    if e_id in form:
                        p_id = form[e_id]
                        if p_id in form:
                            update_collection('Positions',{"position_id":p_id},{'$set' : {"levels" : form.getlist(p_id)}})
                        else:
                            update_collection('Positions',{"position_id":p_id},{'$set' : {"levels" : []}})
                flash('Save changes successfully','alert-success')
                #return redirect(url_for('zenleave.levels'))
            if len(usr_no_pos)==0:
                pass
            else:
                flash(str(usr_no_pos)+ " " +"Dosent Have Active Position ID Please Update.",'alert-danger')
                return render_template('approval_levels.html', user_details=user_details, p_employees=a,employee_att=employee_att)
        return render_template('approval_levels.html', user_details=user_details, p_employees=a,employee_att=employee_att)

    elif q or plant_id=="HY":
        if p:
            result=1
        else:
            result=2
        if plant_id == "HY":
            pass
        p_employees=find_in_collection('Userinfo',{'employee_status':'active'})
        employee_att=find_and_filter('Positions',{"levels":{'$size':0}},{'_id':0,'position_id':1})
        employee_details=[]
        for i in employee_att:
            user_info=find_and_filter('Userinfo',{"designation_id":i['position_id']},{"_id":0,"username":1,"f_name":1,"l_name":1,"employee_status":1,"designation_id":1,"plant_id":1})
            if user_info:
                employee_details.append(user_info)

        a=[]
        for p in p_employees:
            # logging.info("Employee position:"+str(p['designation_id'])) 
            q=find_one_in_collection('Positions',{"position_id":p['designation_id']})
            if q:
                if 'levels' in q:    #will give all the employes who are having levels
                    p['levels']= q['levels']    #assigning levels of employee from positions coll to user coll
                a=a+[p] 
            else:
                usr_no_pos.append(str(p['designation_id'])) 
                
            
        if request.method == 'POST':
            form=request.form
            if 'plant_submit' in form:
                a=[]
                p_employees=find_in_collection('Userinfo',{"plant_id":form['select_plant']})
                for p in p_employees:
                    # logging.info("Employee position:"+str(p['designation_id'])) 
                    q=find_one_in_collection('Positions',{"position_id":p['designation_id']})
                    if q:
                        if 'levels' in q:    #will give all the employes who are having levels
                            p['levels']= q['levels']    #assigning levels of employee from positions coll to user coll
                            a=a+[p] 
                        else:
                            usr_no_pos.append(str(p['designation_id']))
                    selval=form['select_plant']
                if form['select_plant'] != "":
                    flash('Displaying '+selval + ' plant data','alert-success')
                else:
                    flash('Please Select a plant','alert-danger')
            else:
                for i in range(0,len(p_employees)):
                    e_id = p_employees[i]['username']
                    if e_id in form:
                        p_id = form[e_id]
                        if p_id in form:
                            update_collection('Positions',{"position_id":p_id},{'$set' : {"levels" : form.getlist(p_id)}})
                        else:
                            update_collection('Positions',{"position_id":p_id},{'$set' : {"levels" : []}})
                flash('Save changes successfully','alert-success')
            #return redirect(url_for('zenleave.levels'))
        if len(usr_no_pos)==0:
            pass
        else:
            flash(str(usr_no_pos)+ " " +"Dosent Have Active Position ID Please Update",'alert-danger')
            return render_template('approval_levels.html', user_details=user_details, p_employees=a,employee_att=employee_details,result=result)   
        return render_template('approval_levels.html', user_details=user_details,p_employees=a,employee_att=employee_details,result=result)
    else:
        return redirect(url_for('zenuser.index'))

@zen_leave.route('/mylog/', methods = ['GET', 'POST'])
@login_required
def mylog():
    user_details = base()
    uid= current_user.username
    mlog=find_and_filter('Notifications',{"e_id":uid},{"msg":1,"_id":0,"created_time":1})
    return render_template('mylog.html', user_details=user_details,mlog=mlog)
######################################################
@zen_leave.route('/time/', methods = ['GET', 'POST'])
@login_required
def time():
    p =check_user_role_permissions('54ae3abd507c00c54ba22299')
    if p:
        user_details = base()
        uid= current_user.username
        plant_id=get_plant_id(uid)
        plant_details=find_one_in_collection('Plants',{"plant_id":plant_id})
        auto_approval_details=find_one_in_collection('attendance_config',{"config":'leave_auto_approvals'})
        msg=''
        if request.method == 'POST':
            form=request.form
            if form['leave_per_configure']=='form_leave_time':       
                update_collection('Plants',{"plant_id":plant_id},{"$set":{"leave_time":int(form['time'])}})
                msg='Leave time updated successfully.'
            if form['leave_per_configure']=='form_per_time':
                update_collection('Plants',{"plant_id":plant_id},{"$set":{"permission_duration":int(form['per_time'])}})
                msg='Permission Duration has been updated sucessfully.'
            elif form['leave_per_configure']=='form_per_limit':
                update_collection('Plants',{"plant_id":plant_id},{"$set":{"permissions_limit":int(form['per_limit'])}})
                msg='permissions limit has been updated sucessfully.'
            elif form['leave_per_configure']=='auto_approval_config':
                if form['auto_approve_status']=='Enabled':
                    auto_status='Disabled'
                elif form['auto_approve_status']=='Disabled':
                    auto_status='Enabled'
                update_collection('attendance_config',{"config":'leave_auto_approvals'},{"$set":{"status":auto_status}})
                msg='Leave Auto Approval Has Been has been '+auto_status+' sucessfully.'
            flash(msg,'alert-success')
            return redirect(url_for('zenleave.time'))
        return render_template('time.html', user_details=user_details,plant_details=plant_details,auto_approval_details=auto_approval_details)
    else:
        return redirect(url_for('zenuser.index'))


@zen_leave.route('/ehistory/')
@login_required
def employee_leave_history():
    p =check_user_role_permissions('54b378d4507c00c54ba2229b')
    if p:
        user_details=base()
        employees_list=get_employees_under_manager()
        return render_template('employee_leave_history.html', user_details=user_details,el=employees_list)
    else:
        return redirect(url_for('zenuser.index'))

@zen_leave.route('/getleaves', methods = ['GET', 'POST'])
@login_required
def getleaves():
    deg = str(request.form.get('deg'))
    lh=find_in_collection('Leaves',{"employee_id":deg,'$or':[{"status":"approved"},{"status":"rejected"}]})
    a=[]
    if lh:
        for e in lh:
            e['_id']=str(e['_id'])
            e['reason']=e['leave_reason']
            if e['attribute'] == 'attendance':
                e['leave']=get_attendance_type(e['leave_type'])
            elif e['attribute'] == 'permission':
                e['leave']='Permission'
            else:
                e['leave']=get_leave_type(e['leave_type'])          
            a=a+[e]
    leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":deg})
    leave_quota['_id']=str(leave_quota['_id'])
    leave_quota['sls_balance']=leave_quota['available_sls']-leave_quota['sls_taken']
    leave_quota['els_balance']=leave_quota['available_els']-leave_quota['els_taken']
    leave_quota['cls_balance']=leave_quota['available_cls']-leave_quota['leaves_taken']
    return jsonify({"leaves":a,"leave_quota":leave_quota})



@zen_leave.route('/checkappliedleave', methods = ['GET', 'POST'])
@login_required
def checkappliedleave():
    form=request.form
    if 'employee' in form:
        uid=form['employee']
    else:
        uid= current_user.username  
    if 'halfday' in form:
        list1= form.getlist('halfcheck')
    else:
        list1 = []
    if form['end_date']=="":
        end_date=form['start_date']
    else:
        end_date=form['end_date']

    data=list_of_holidays_total_days(uid,form['start_date'],end_date,list1,form['leave_type'])
    
    return jsonify(data)


@zen_leave.route('/eapply/', methods = ['GET', 'POST'])
@login_required
def eapply_leave():
    debug_flag=find_one_in_collection('portal_settings',{'config':'debug_profile'})
    if debug_flag and debug_flag['debug']=='on':
        debug_flag=debug_flag['debug_status']['eapply_onbehalf']
    else:
        debug_flag="off"
    user_details = base()
    uid= current_user.get_id()
    crnt_user=current_user.username
    user_data1 = find_and_filter('Userinfo',{"_id":ObjectId(uid)},{"_id":0,"plant_id":1})
    employee_data=find_and_filter('Userinfo',{"plant_id":user_data1[0]['plant_id']},{"_id":0,"username":1,"f_name":1,"l_name":1})
    if request.method == 'POST':
        form=request.form
        # try:#appled for not selecting the employee on applying leave/Attendance onbhalf.
        logging.info('Applying On-Behalf For Employee '+form['employee']+' with form data '+str(form)) 
        end_date=form['end_date']
        uname=form['employee']
        user_data = find_and_filter('Userinfo',{"username":uname},{"calendar_code":1,"designation_id":1,"_id":0,"plant_id":1})
        leave_att = form['leave']
        if 'halfday' in form:
            list1= form.getlist('halfcheck')
        else:
            list1 = []
        if(leave_att == '1'):
            previous_leaves=business_rule_7(uname,form['start_date'],form['end_date'])
            logging.info('Response of b7 '+str(previous_leaves)+ ' ----- Employee : '+uname+' of plant '+user_data1[0]['plant_id'])
            if previous_leaves > 0:
                flash('Previous leave dates are colliding with applied leave,Please select another date and apply','alert-danger')
                return redirect(url_for('zenleave.eapply_leave'))
            # br1=business_rule_1(uname,form['leave_type']) #for giving permission to approve leave even if previous sl is not approved.
            # if br1 != '0':
            #     flash(br1,'alert-danger')
            #     return redirect(url_for('zenleave.eapply_leave'))
            total_days=total_no_of_days_leave(uname,form['start_date'],form['end_date'],list1)
           # HR can apply CL's more than availabe balance
            # if form['leave_type'] == 'CL':
            #     employee_group=find_and_filter('Userinfo',{"username":uname},{"ee_group":1,'_id':0})
            #     try:
            #         leaves_available=business_rule_3(uname,employee_group[0]['ee_group'])
            #         if total_days > leaves_available:
            #             flash('No of leaves applied is greater than available leaves','alert-danger')
            #             return redirect(url_for('zenleave.eapply_leave'))
            #     except:
            #         flash('Leave Quota is Not Available','alert-danger')
            #         return redirect(url_for('zenleave.eapply_leave'))
            # if form['leave_type'] == 'EL':
            #     if business_rule_4(uname) > 4:
            #         flash('You have already applied Earned Leaves 4 times','alert-danger')
            #         return redirect(url_for('zenleave.eapply_leave'))
            br2=business_rule_2(uname,form['start_date'],form['end_date'],list1,form['leave_type'],'','')
            if br2 != '0':
                flash(br2,'alert-danger')
                return redirect(url_for('zenleave.eapply_leave'))
            attribute="leave"
            leave_type=get_leave_type(form['leave_type'])
            # print form['submit']
            if form['submit']=="Apply":
                msg= 'Leave applied successfully'
            elif form['submit']=="Approve leave":
                msg="requested for approval"
                array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                    "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                    "end_date":datetime.datetime.strptime(form['end_date'],"%d/%m/%Y"),
                                    "applied_time":datetime.datetime.now(),"halfdays":list1, "employee_id":uname,
                                    "status":"approved","hr_status":"unhold","plant_id":user_data[0]['plant_id'],
                                    "hold_status":"0","appliedby":crnt_user,"test":"testing"}
        elif str(form['leave_type'])=='Permission':
                attribute='permission'
                end_date=''
                leave_type=form['leave_type']
                permissions_count=business_rule_13(uname,str(form['start_date']),str(form['fromtime']),str(form['totime']),user_data[0]['plant_id']) 
                if permissions_count[0]==1:
                    msg='You have sucessfully applied for '+leave_type+' on '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' .'
                else:
                    flash(permissions_count[1],'alert-danger')
                    return redirect(url_for('zenleave.eapply_leave'))
        else:
            if str(form['leave_type']) == "1":#on Tour
                from_time=""
                to_time=""
                start_date=form['start_date']
                end_date=form['end_date']
            elif str(form['leave_type']) == "2" and form['end_date'] == "":#on Duty single day
                start_date=str(form['start_date'])
                end_date=str(form['start_date'])
                from_time=form['fromtime']
                to_time=str(form['totime'])
            elif str(form['leave_type']) == "2" and str(form['multi_att']) == "3":
                start_date=str(form['start_date'])
                end_date=str(form['end_date'])
                from_time=form['fromtime']
                to_time=str(form['totime'])
            br2=business_rule_2(uname,form['start_date'],form['start_date'],list1,form['leave_type'],from_time+':00',to_time+':00')
            if br2 != '0':
                flash(br2,'alert-danger')
                return redirect(url_for('zenleave.eapply_leave'))
            previous_attendance=business_rule_7_1(uname,start_date,end_date,from_time,to_time)
            if previous_attendance[0] !=0:
                if from_time != "" and end_date != start_date:#attendance onduty Multiple days
                    flash('You have already applied Leave/Attendance from '+ previous_attendance[4]+" and " +previous_attendance[5]+" from "+previous_attendance[2]+" to "+previous_attendance[3],'alert-danger')
                elif from_time != "" and end_date == start_date:#OD attendance single day
                    flash('You have already applied Leave/Attendance on '+ form['start_date']+" from "+previous_attendance[2]+" to "+previous_attendance[3],'alert-danger')
                elif from_time == "":
                    if previous_attendance[1]==previous_attendance[2]:
                    # flash('You have already applied attendance in between the fallowing days i.e... on '+ form['start_date']+" and "+form['end_date'] ,'alert-danger')
                        # flash('You have already applied Leave/Attendance,Please Choose Another day and Apply '+previous_attendance[1],'alert-danger')
                        flash('You have already applied Leave/Attendance,Please Choose Another Date And Apply Again. ','alert-danger')
                    elif previous_attendance[1]!= previous_attendance[2]:
                        # flash('You have already applied attendance in between the fallowing days from '+previous_attendance[1]+" to "+previous_attendance[2],'alert-danger')
                        flash('You Have Applied Leave/Attendance Already,Please Select A Different Date And Apply Again.','alert-danger')
                else:
                    flash('Exception Leave Not Applied Please Try Again','alert-danger')
                return redirect(url_for('zenleave.eapply_leave'))
            attribute="attendance"
            leave_type=get_attendance_type(form['leave_type'])
            msg= 'Attendance applied successfully'
        if form['submit']=="Apply":
            b=find_and_filter('Positions',{"position_id":user_data[0]['designation_id']},{"_id":0,"levels":1})
            d=[]
            leave_time=get_leave_time(user_data[0]['plant_id'])
            if leave_time == 0:
                time='None'
            else:
                time=datetime.datetime.now()+datetime.timedelta(hours = leave_time)
            new_b=str(b)
            if 'levels' in new_b:
                count_levels=len(b[0]['levels'])
                for i in range(0,count_levels):
                    c=find_and_filter('Userinfo',{"designation_id":b[0]['levels'][i]},{"username":1,"_id":0})
                    if i == 0:
                        d= d+[{"a_status":"current","a_id":c[0]['username'],"a_discuss":'0',"a_time":time}]
                    else:
                        d= d+[{"a_status":"waiting","a_id":c[0]['username'],"a_discuss":'0'}]
                if 'multi_att' in form:
                    # print "multi_att"           
                    array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                        "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                           "end_date":datetime.datetime.strptime(end_date,"%d/%m/%Y"),
                                           "applied_time":datetime.datetime.now(),
                        "halfdays":list1, "employee_id":uname,"status":"applied","approval_levels":d,
                        "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"from_time":"00:00","to_time":"23:45","hold_status":"0","appliedby":crnt_user}
                elif form['leave_type'] == "Permission":
                    array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                        "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                        "end_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                           "applied_time":datetime.datetime.now(),
                        "halfdays":list1, "employee_id":uname,"status":"applied","approval_levels":d,'from_time':form['fromtime'],'to_time':form['totime'],
                        "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0","appliedby":crnt_user}
                else:
                    # print "no multi_att"
                    array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                        "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                           "end_date":datetime.datetime.strptime(end_date,"%d/%m/%Y"),
                                           "applied_time":datetime.datetime.now(),
                        "halfdays":list1, "employee_id":uname,"status":"applied","approval_levels":d,
                        "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0","appliedby":crnt_user}            
                # array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                #     "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                #                     "end_date":datetime.datetime.strptime(form['end_date'],"%d/%m/%Y"),"applied_time":datetime.datetime.now(),
                #     "halfdays":list1, "employee_id":uname,"status":"applied","approval_levels":d,
                #     "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0","appliedby":crnt_user}
                if len(form['fromtime'])!=0 and len(form['totime'])!=0:
                                    array['from_time']=form['fromtime']
                                    array['to_time']=form['totime']
                save_collection('Leaves',array)
                if request.files['doc']:
                    input_file = request.files['doc']
                    file_save_gridfs(input_file,"application/pdf","doc")
                if leave_type=='Permission':
                    d=find_and_filter('Userinfo',{"designation_id":b[0]['levels'][0]},{"username":1,"_id":0})
                    message= get_employee_name(uname)+' applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+',it is pending for your approval.\n You can approve the leave by clicking on this link : http://myportal.pennacement.com/leave/approvals/'
                    message1= 'You have applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' .'
                    notification(uname,message1)#send notification to employee
                    sms1='You have successfully applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' for Employee :'+get_employee_name(uname)+'.'
                    get_employee_email_mobile(uname,'My Portal Leave Notification',message1,sms1)#send email/sms to employee    
                    notification(d[0]['username'],message)#send notification to rm1
                    sms2=get_employee_name(uname)+' has applied for '+leave_type+' on '+form['start_date']+' from '+form['start_date']+'from '+form['fromtime']+' to '+form['totime']+' .'
                    get_employee_email_mobile(d[0]['username'],'My Portal Leave Notification',message,sms2)#send email/sms to rm1
                    notification(get_hr_id(user_data[0]['plant_id']),message)#send notification to hr
                    #send email/sms to hr
                    # get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message,sms2) 
                else:
                    d=find_and_filter('Userinfo',{"designation_id":b[0]['levels'][0]},{"username":1,"_id":0})
                    message= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                    message1= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                    notification(uname,message1)
                    sms1='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                    get_employee_email_mobile(uname,'My Portal Leave Notification',message1,sms1)
                    notification(d[0]['username'],message)
                    sms2=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                    get_employee_email_mobile(d[0]['username'],'My Portal Leave Notification',message,sms2)
                    notification(get_hr_id(user_data[0]['plant_id']),message)
                    get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message,sms2)
            else:#auto approval of leave/attendance if there are no approval levels
                array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                    "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                    "end_date":datetime.datetime.strptime(form['end_date'],"%d/%m/%Y"),"applied_time":datetime.datetime.now(),
                    "halfdays":list1, "employee_id":uname,"status":"applied","approval_levels":d,
                    "hr_id":user_data[0]['plant_id'],"appliedby":crnt_user}
                if len(form['fromtime'])!=0 and len(form['totime'])!=0:
                                    array['from_time']=form['fromtime']
                                    array['to_time']=form['totime']
                save_collection('Leaves',array)
                message1= get_employee_name(uname)+' applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                message2= get_employee_name(uname)+' '+leave_type+' approved '+' from '+form['start_date']+' to '+form['end_date']
                message3= 'You have applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                message4= 'Your '+leave_type+' approved '+' from '+form['start_date']+' to '+form['end_date']
                notification(uname,message3)
                sms3='You have successfully applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                get_employee_email_mobile(uname,'My Portal Leave Notification',message3,sms3)
                notification(get_hr_id(user_data[0]['plant_id']),message1)
                sms1=get_employee_name(uname)+' has applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message1,"")
                notification(uname,message4)
                sms4='Your '+leave_type+' from '+form['start_date']+' to '+form['end_date']+'has been approved'
                get_employee_email_mobile(uname,'My Portal Leave Notification',message4,sms4)
                notification(get_hr_id(user_data[0]['plant_id']),message2)
                get_employee_email_mobile(get_hr_id(user_data[0]['plant_id']),'My Portal Leave Notification',message2,sms4)
        elif form['submit']=="Approve":
            if attribute == 'attendance':
                leave_type=get_attendance_type(form['leave_type'])
            elif attribute == 'permission':
                leave_type='Permission'
            else:
                leave_type=get_leave_type(form['leave_type'])
            
            if form['leave_type']== 'SL':
                n_leaves=total_no_of_days_el(form['employee'],form['start_date'],end_date,list1)
            elif form['leave_type']== 'Permission':
                n_leaves=total_no_of_days_el(form['employee'],form['start_date'],form['start_date'],list1)                            
            else:
                n_leaves=total_no_of_days_leave(form['employee'],form['start_date'],end_date,list1)
            update_approved_leaves_in_leave_quota(form['employee'],form['leave_type'],n_leaves)
            if 'multi_att' in form:  
                array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                    "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                       "end_date":datetime.datetime.strptime(end_date,"%d/%m/%Y"),
                                       "applied_time":datetime.datetime.now(),
                    "halfdays":list1, "employee_id":uname,"status":"approved","a_id":crnt_user,"sap":'1',
                    "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"from_time":"00:00","to_time":"23:45","hold_status":"0","appliedby":crnt_user}
            elif form['leave_type'] == "Permission":
                    array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                        "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                        "end_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                           "applied_time":datetime.datetime.now(),
                        "halfdays":list1, "employee_id":uname,"status":"applied","a_id":crnt_user,"sap":'1',
                        "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0","appliedby":crnt_user,'from_time':form['fromtime'],'to_time':form['totime']}
            else:
                array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
                    "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
                                       "end_date":datetime.datetime.strptime(end_date,"%d/%m/%Y"),
                                       "applied_time":datetime.datetime.now(),
                    "halfdays":list1, "employee_id":uname,"status":"approved","a_status":"approved","a_id":crnt_user,
                    "hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0","appliedby":crnt_user,"sap":"1"}
            # array={"attribute":attribute,"leave_type":form['leave_type'],"leave_reason":form['reason'],
            #         "start_date":datetime.datetime.strptime(form['start_date'],"%d/%m/%Y"),
            #                         "end_date":datetime.datetime.strptime(form['end_date'],"%d/%m/%Y"),"applied_time":datetime.datetime.now(),
            #         "halfdays":list1, "employee_id":uname,"status":"approved","a_status":"approved","a_id":crnt_user,"hr_status":"unhold","plant_id":user_data[0]['plant_id'],"hold_status":"0","appliedby":crnt_user,"sap":"1"}
            save_collection('Leaves',array)
            # if attribute == 'leave':
            #     get_leave_chart_data(form['employee'],datetime.datetime.strtime(str(form['start_date']),"%d/%m/%Y"),datetime.datetime.strptime(str(form['start_date']),"%d/%m/%Y"),list1,form['leave_type'])
            b=find_and_filter('Positions',{"position_id":user_data[0]['designation_id']},{"_id":0,"levels":1})
            # print b
            d=find_and_filter('Userinfo',{"designation_id":b[0]['levels'][0]},{"username":1,"_id":0})
            employee_name = get_employee_name(form['employee'])
            if form['leave_type']=='Permission':
                msg=employee_name+' '+leave_type+' on '+form['start_date']+'has been applied and approved by '+get_employee_name(crnt_user)
                message1='Your '+leave_type+' on '+form['start_date']+' has been Applied and Approved '+get_employee_name(crnt_user)
                message= employee_name+' '+leave_type+' on '+form['start_date']+' has been applied and approved by '+get_employee_name(crnt_user)
                
                sms1='Your '+leave_type+' on '+form['start_date']+' has been applied and approved by '+get_employee_name(crnt_user)
                sms3=get_employee_name(form['employee'])+' has applied for '+leave_type+' for '+form['start_date']+' .'
                sms2=employee_name+' '+leave_type+' for '+form['start_date']+' has been applied and approved by '+get_employee_name(crnt_user)
            else:
                msg=employee_name+' '+leave_type+' from '+form['start_date']+' to '+form['end_date']+' has been applied and approved by '+get_employee_name(crnt_user)
                message1='Your '+leave_type+' from '+form['start_date']+' to '+form['end_date']+' has been Applied and Approved '+get_employee_name(crnt_user)
                message= employee_name+' '+leave_type+' from '+form['start_date']+' to '+form['end_date']+' has been applied and approved by '+get_employee_name(crnt_user)
                
                sms1='Your '+leave_type+' from '+form['start_date']+' to '+form['end_date']+' has been applied and approved by '+get_employee_name(crnt_user)
                sms3=get_employee_name(form['employee'])+' has applied for '+leave_type+' from '+form['start_date']+' to '+form['end_date']
                sms2=employee_name+' '+leave_type+' from '+form['start_date']+' to '+form['end_date']+' has been applied and approved by '+get_employee_name(crnt_user)
            notification(crnt_user,message)#notification for HR current user
            if d:
                notification(d[0]['username'],message)#notification for RM
                get_employee_email_mobile(d[0]['username'],'My Portal Leave Notification',message,sms3)
            notification(form['employee'],message1) #msg to employee
            get_employee_email_mobile(form['employee'],'My Portal Leave Notification',message1,sms1)
            get_employee_email_mobile(crnt_user,'My Portal Leave Notification',message,sms2)
        flash(msg,'alert-success')
        # except:
        #     flash('OOPS! Something Went Wrong.Please Try Again With All Fields Filled','alert-danger')
        #return redirect(url_for('zenleave.eapply_leave'))
    return render_template('e_apply_leave.html', user_details=user_details,employee_data=employee_data)


@zen_leave.route('/get_calendar_and_quota', methods = ['GET', 'POST'])
@login_required
def get_calendar_and_quota():
    deg = str(request.form.get('deg'))
    leave_quota=find_one_in_collection('LeaveQuota',{"employee_id":deg})
    leave_quota['_id']=str(leave_quota['_id'])
    leave_quota['sls_balance']=leave_quota['available_sls']-leave_quota['sls_taken']
    leave_quota['els_balance']=leave_quota['available_els']-leave_quota['els_taken']
    leave_quota['cls_balance']=leave_quota['available_cls']-leave_quota['leaves_taken']
    user_data = find_and_filter('Userinfo',{"username":deg},{"calendar_code":1,"designation_id":1,"_id":0,"plant_id":1})
    permissions_details= count_permissions(user_data[0]['plant_id'],deg)
    logging.info(str(permissions_details))
    calendar=get_calendar_data(deg)
    return jsonify({"leave_quota":leave_quota,"calendar":calendar,'permissions_details':permissions_details,'usr_plant_id':user_data[0]['plant_id']})

@zen_leave.route('/uploadleave/')
def uploadleave():
    c=os.path.abspath("zenapp/static/leave.csv")        
    with open(c) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            halfdays=[] 
            approval_levels=[]
            edate=datetime.datetime.strptime(row['TOLEAVEDATE'],"%Y-%m-%d")
            end_date=edate.strftime("%d/%m/%Y")
            sdate=datetime.datetime.strptime(row['FROMLEAVEDATE'],"%Y-%m-%d")
            start_date=sdate.strftime("%d/%m/%Y")
            if row['FROMLEAVETYPE']=="fhalf" and row['TOLEAVETYPE']=="fhalf":
                halfdays.append("1")
            elif row['FROMLEAVETYPE']=="shalf" and row['TOLEAVETYPE']=="shalf":
                halfdays.append("2")
            elif row['FROMLEAVETYPE']=="shalf" and row['TOLEAVETYPE']=="fhalf":
                halfdays.append("1")
                halfdays.append("2")
            array={"status" : "approved","approval_levels":approval_levels,"attribute" : "leave","hold_status" : "0","leave_reason" :row['REASON'],"hr_status" :"unhold", "employee_id":row['EMPID'],"end_date" :end_date,"halfdays" : halfdays,"applied_time":row['LEV_APPLY_DATE'] ,"leave_type" :row['LTID'],"plant_id" : "HY","start_date" :start_date, "sap":"upload"}
            #print array
            save_collection('Leaves',array)
    return 'success'

#Getting Employees Applied and Rejected Leave History based on Different types of Filters
#in the below code we are not converting the start_date and end_date fromat to datetime.date time because we already updated the dates in db to datetime.datetime
@zen_leave.route('/newreports/', methods = ['GET', 'POST'])
@login_required
def newreports():
    p =check_user_role_permissions('546f2071850d2d1b00023b24')    
    if p:
        user_details=base()
        uid = current_user.username
        current_year=datetime.datetime.now().year
        if request.method == 'POST':
            start_time_check=datetime.datetime.now()
            form=request.form
            if form['report_choice']=='sel_onbehalf_report':
                display_table='sel_onbehalf_report'
            else:
                display_table='general_report'
            from_date=datetime.datetime.strptime(form['start_date'],"%d/%m/%Y")
            to_date=datetime.datetime.strptime(form['end_date'], "%d/%m/%Y")
            if 'employee_id' in form:
                employee_id=str(form['employee_id'])
            leave_typ=str(form['ltype'])
            plant_id=str(form['plant_id'])
            # Below code is usefull to Filter the data according to user inputs
            if from_date != "" and to_date != "":
                if plant_id=="EMPID":
                    if employee_id !="" and leave_typ !="":
                        new_data=find_in_collection('Leaves',{'employee_id':str(form['employee_id']),"emp_status":{'$exists':0},'leave_type':leave_typ,'$or':[{"status":"approved"},{"status":"rejected"}],"start_date":{"$lte":to_date},"end_date":{"$gte":from_date}})
                    else:
                        new_data=find_in_collection('Leaves',{'employee_id':str(form['employee_id']),"emp_status":{'$exists':0},'$or':[{"status":"approved"},{"status":"rejected"}],"start_date":{"$lte":to_date},"end_date":{"$gte":from_date}})      
                elif plant_id == "" and leave_typ == "":
                    new_data=find_in_collection('Leaves',{'$or':[{"status":"approved"},{"status":"rejected"}],"start_date":{"$lte":to_date},"end_date":{"$gte":from_date}})              
                elif plant_id == "":
                    new_data=find_in_collection('Leaves',{"emp_status":{'$exists':0},'leave_type':leave_typ,'$or':[{"status":"approved"},{"status":"rejected"}],"start_date":{"$lte":to_date},"end_date":{"$gte":from_date}})
                elif leave_typ == "":
                    new_data=find_in_collection('Leaves',{'plant_id':plant_id,"emp_status":{'$exists':0},'$or':[{"status":"approved"},{"status":"rejected"}],"start_date":{"$lte":to_date},"end_date":{"$gte":from_date}}) 
                else:
                    new_data=find_in_collection('Leaves',{"emp_status":{'$exists':0},"leave_type":leave_typ,"plant_id":plant_id,'$or':[{"status":"approved"},{"status":"rejected"}],"start_date":{"$lte":to_date},"end_date":{"$gte":from_date}}) 
            else:
                flash("NO Data available for the values you have queried",'alert-danger')
                #return redirect(url_for('zenleave.newreports'))

            a=[]
            
            if new_data:
                for e in new_data:
                    if 'approval_levels' in e and len(e['approval_levels']) !=0:
                        e['approvedby']=e['approval_levels'][len(e['approval_levels'])-1]['a_id']
                    elif 'a_id' in e:
                        e['approvedby']=e['a_id']
                    else:
                        e['approvedby']='No Approval Levels'
                    e['halfdays']=[str(x) for x in e['halfdays']]
                    try:
                        e['fullname']=get_employee_name(e['employee_id'])
                    except:
                        e['fullname']='No Empdata In Userinfo'
                    e['reason']=str(e['leave_reason'])
                    if type(e['applied_time']) == datetime.datetime:
                        e['applied_time']= datetime.datetime.strftime(e['applied_time'],"%d/%m/%Y")
                    
                    e['start_date']=datetime.datetime.strftime(e['start_date'],"%d/%m/%Y")
                    e['end_date']=datetime.datetime.strftime(e['end_date'],"%d/%m/%Y")
                    if e['attribute'] == 'attendance':
                        e['leave']=get_attendance_type(e['leave_type'])
                    elif e['attribute'] == 'permission':
                        e['leave']='PERMISSION'
                    else:
                        e['leave']=get_leave_type(e['leave_type'])            
                    a=a+[e]
                return render_template('APPROVED_REJECTED_LEAVE_HISTORY.html', user_details=user_details,lh=a,display_table=display_table) #chartdata=chartdata[0]['chartdata']
            else:
                flash("NO data available for the values you have queried",'alert-danger')
                #return redirect(url_for('zenleave.newreports'))
            logging.info('Sucessfully generated HR leave Report by emp : '+uid +' of Plant : '+user_details['plant_id'])
    return render_template('APPROVED_REJECTED_LEAVE_HISTORY.html',user_details=user_details,display_table='general_report')

#This method is useful to hold the employees leaves.
#Below method filter the Hold leaves data according to User Inputs

@zen_leave.route('/newholdleaves/', methods = ['GET', 'POST'])
@login_required
def newholdleaves():
    p =check_user_role_permissions('546f2071850d2d1b00023b24')
    if p:
            user_details=base()
            uid = current_user.username
            #print "UID",uid
            current_year=datetime.datetime.now().year
            #Once Form is Posted it will redirect to the holdleavestest Method.
            if request.method == 'POST':
                form=request.form
                if 'employee_id' not in  form:
                    start_day_data=form['start_date'],
                    end_day_data=form['end_date'],
                    leave_type_data=form['ltype'],
                    plant_id_data=form['plant_id']
                    employee_id_data=None
                    return redirect(url_for('zenleave.holdleavestest',start_dayy=start_day_data,end_dayy=end_day_data,leave_typee=leave_type_data,plant_idd=plant_id_data,employee_idd=employee_id_data))
                else:
                    start_day_data=form['start_date'],
                    end_day_data=form['end_date'],
                    leave_type_data=form['ltype'],
                    plant_id_data=form['plant_id']
                    employee_id_data=form['employee_id']
                    return redirect(url_for('zenleave.holdleavestest',start_dayy=start_day_data,end_dayy=end_day_data,leave_typee=leave_type_data,plant_idd=plant_id_data,employee_idd=employee_id_data))

    else:
        return redirect(url_for('zenuser.index'))

    return render_template('HOLD_LEAVE_HISTORY_SEARCH.html', user_details=user_details)

#this Method Accepts the user data and Retrive the Data from the DB with user given filters.

@zen_leave.route('/holdleavestest/', methods = ['GET', 'POST'])
@login_required
def holdleavestest():
    p =check_user_role_permissions('547c7e0157fbb30ed690336e')
    if p:
        user_details=base()
        uid = current_user.username
        emp_id=request.args.get('employee_idd')
        type_of_leave=request.args.get('leave_typee')
        plant_id=request.args.get('plant_idd')
        start_date= request.args.get('start_dayy')
        end_date = request.args.get('end_dayy')
                
        if str(plant_id)=="EMPID":
            if len(emp_id)!=0 and len(type_of_leave)==0 and len(start_date)==0 and len(end_date)==0 :
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),"status":"applied","hold_status":"0"})
                
            elif len(type_of_leave)!=0 and len(start_date)!=0 and len(end_date)==0:
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),'leave_type':str(type_of_leave),"start_date":{"$gte":s_date},
                                                               "status":"applied","hold_status":"0"})

            elif len(type_of_leave)!=0 and len(start_date)==0 and len(end_date)!=0:
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                        
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),'leave_type':str(type_of_leave),"end_date":{"$lte":e_date},
                                                                   "status":"applied","hold_status":"0"})
                        
            elif len(type_of_leave)!=0 and len(start_date)==0 and len(end_date)==0:
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),'leave_type':str(type_of_leave),
                                                                   "status":"applied","hold_status":"0"})
                        
            elif len(type_of_leave)==0 and len(start_date)!=0 and len(end_date)!=0 :
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),"start_date":{"$lte":e_date},"end_date":{"$gte":s_date},
                                                               "status":"applied","hold_status":"0"})
                        
            elif len(type_of_leave)==0 and len(start_date)!=0 and len(end_date)==0  :
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),"start_date":{"$gte":s_date},
                                                                   "status":"applied","hold_status":"0"})
                        
            elif  len(type_of_leave)==0 and len(start_date)==0 and len(end_date)!=0:
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),"end_date":{"$lte":e_date},
                                                                   "status":"applied","hold_status":"0"})
            else:
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'employee_id':str(emp_id),"leave_type":str(type_of_leave),"start_date":{"$lte":e_date},"end_date":{"$gte":s_date},
                                                              "status":"applied","hold_status":"0"})   
                 
        else:
            if len(plant_id)!=0 and len(type_of_leave)!=0 and len(start_date)!=0 and len(end_date)==0:
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),"leave_type":str(type_of_leave),"start_date":{"$gte":s_date},"status":"applied","hold_status":"0"})  

                        
            elif len(plant_id)!=0 and len(type_of_leave)!=0 and len(start_date)==0 and len(end_date)!=0 :
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),"leave_type":str(type_of_leave),"end_date":{"$lte":e_date},"status":"applied","hold_status":"0"})  


            elif len(plant_id)!=0 and len(type_of_leave)!=0 and len(start_date)==0 and len(end_date)==0 :
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),"leave_type":str(type_of_leave),"status":"applied","hold_status":"0"})  
                

            elif len(plant_id)!=0 and len(type_of_leave)==0 and len(start_date)!=0 and len(end_date)!=0 :
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),"start_date":{"$lte":e_date},"end_date":{"$gte":s_date},"status":"applied","hold_status":"0"})  

                    
            elif len(plant_id)!=0 and len(type_of_leave)==0 and len(start_date)!=0 and len(end_date)==0 :
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),"start_date":{"$gte":s_date},"status":"applied","hold_status":"0"}) 
                        

            elif len(plant_id)!=0 and len(type_of_leave)==0 and len(start_date)==0 and len(end_date)!=0:
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),"end_date":{"$lte":e_date},"status":"applied","hold_status":"0"}) 


            elif len(plant_id)!=0 and len(type_of_leave)==0 and len(start_date)==0 and len(end_date)==0:
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),"status":"applied","hold_status":"0"}) 


            elif len(plant_id)==0 and len(type_of_leave)!=0 and len(start_date)!=0 and len(end_date)!=0:
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'leave_type':str(type_of_leave),"start_date":{"$lte":e_date},"end_date":{"$gte":s_date},"status":"applied","hold_status":"0"}) 


            elif len(plant_id)==0 and len(type_of_leave)!=0 and len(start_date)!=0 and len(end_date)==0:
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'leave_type':str(type_of_leave),"start_date":{"$gte":s_date},"status":"applied","hold_status":"0"}) 


            elif len(plant_id)==0 and len(type_of_leave)!=0 and len(start_date)==0 and len(end_date)!=0:
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'leave_type':str(type_of_leave),"end_date":{"$lte":e_date},"status":"applied","hold_status":"0"}) 

                        

            elif len(plant_id)==0 and len(type_of_leave)!=0 and len(start_date)==0 and len(end_date)==0:
                employee_att=find_in_collection('Leaves',{'leave_type':str(type_of_leave),"status":"applied","hold_status":"0"}) 


            elif len(plant_id)==0 and len(type_of_leave)==0 and len(start_date)!=0 and len(end_date)!=0:
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{"start_date":{"$lte":e_date},"end_date":{"$gte":s_date},"status":"applied","hold_status":"0"}) 


            elif len(plant_id)==0 and len(type_of_leave)==0 and len(start_date)!=0 and len(end_date)==0:
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{"start_date":{"$gte":s_date},"status":"applied","hold_status":"0"}) 


            elif len(plant_id)==0 and len(type_of_leave)==0 and len(start_date)==0 and len(end_date)!=0:
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{"end_date":{"$lte":e_date},"status":"applied","hold_status":"0"}) 


            elif len(plant_id)!=0 and len(type_of_leave)!=0 and len(start_date)!=0 and len(end_date)!=0 :
                s_date=datetime.datetime.strptime(request.args.get('start_dayy'), "%d/%m/%Y")
                e_date=datetime.datetime.strptime(request.args.get('end_dayy'), "%d/%m/%Y")
                employee_att=find_in_collection('Leaves',{'plant_id':str(plant_id),'leave_type':str(type_of_leave),
                                                                   "start_date":{"$lte":e_date},"end_date":{"$gte":s_date},"status":"applied","hold_status":"0"})
            else:
                flash("Please Enter Atleast One Search Criteria",'alert-danger')
                return redirect(url_for('zenleave.newholdleaves'))

                
                
                                
        a=[]
        if employee_att:
            for e in employee_att:
                e['halfdays']=[str(x) for x in e['halfdays']]
                e['fullname']=get_employee_name(e['employee_id'])
                e['reason']=e['leave_reason']
                e['applied_time']= datetime.datetime.strftime(e['applied_time'],"%d/%m/%Y")
                e['start_date']=datetime.datetime.strftime(e['start_date'],"%d/%m/%Y")
                e['end_date']=datetime.datetime.strftime(e['end_date'],"%d/%m/%Y")
                if e['attribute'] == 'attendance':
                    e['leave']=get_attendance_type(e['leave_type'])
                else:
                    e['leave']=get_leave_type(e['leave_type'])          
                a=a+[e]
                
            # Below code is usefull to hold and unhold leaves
            if request.method == 'POST':
                form=request.form
                #print form['leaveid'],form['form_emp_id']
                c_leave =find_and_filter('Leaves',{"_id":ObjectId(form['leaveid'])},{"_id":0,"employee_id":1,"attribute":1,"approval_levels":1,"leave_type":1,"start_date":1,"end_date":1})
                employee_name = get_employee_name(c_leave[0]['employee_id'])
                e_attribute = c_leave[0]['attribute']
                if e_attribute == 'attendance':
                    leave_type=get_attendance_type(c_leave[0]['leave_type'])
                else:
                    leave_type=get_leave_type(c_leave[0]['leave_type']) 
                    
                count_a_levels = len(c_leave[0]['approval_levels'])
                for i in range(0,count_a_levels):
                    if c_leave[0]['approval_levels'][i]['a_status'] == 'current':
                        j=i
                if form['submit']== 'Hold':
                    update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"hr_status":"hold","approval_levels."+str(j)+".a_hold_time":datetime.datetime.now()}})
                    msg= employee_name+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been hold successfully'
                    message= 'Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been hold by HR'
                    message1= employee_name+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been hold by HR'
                    notification(c_leave[0]['employee_id'],message)
                    get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,message)
                    notification(uid,message1)
                    get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
                elif form['submit']== 'Unhold':
                    update_coll('Leaves',{'_id':ObjectId(form['leaveid'])},{'$set': {"hr_status":"unhold","hold_status":"1","approval_levels."+str(j)+".a_unhold_time":datetime.datetime.now()}})
                    msg= employee_name+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been unhold successfully'
                    message= 'Your '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been unhold by HR'
                    message1= employee_name+' '+leave_type+' from '+datetime.datetime.strftime(c_leave[0]['start_date'],"%d/%m/%Y")+' to '+datetime.datetime.strftime(c_leave[0]['end_date'],"%d/%m/%Y")+' has been unhold by HR'
                    notification(c_leave[0]['employee_id'],message)
                    get_employee_email_mobile(c_leave[0]['employee_id'],'My Portal Leave Notification',message,message)
                    notification(uid,message1)
                    get_employee_email_mobile(uid,'My Portal Leave Notification',message1,message1)
                flash(msg,'alert-success')
                return redirect(url_for('zenleave.holdleavestest',start_dayy=start_date,end_dayy=end_date,leave_typee=type_of_leave,plant_idd=plant_id,employee_idd=emp_id))
        		
            return render_template('HOLD_LEAVE_HISTORY_SEARCH.html', user_details=user_details, employee_att=a)
        else:
            return redirect(url_for('zenleave.newholdleaves'))
    else:
        return redirect(url_for('zenuser.index'))


