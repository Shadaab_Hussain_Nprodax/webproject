from flask import Flask, render_template, url_for, session, request, redirect, jsonify
from flask_pymongo import PyMongo
import os
import hashlib
from bson.objectid import ObjectId

app = Flask(__name__)
app.secret_key = os.urandom(24)

app.config['MONGO_DBNAME'] = 'nprodax'
app.config['MONGO_URI'] = 'mongodb://shadaab:shadaab@localhost:27017/nprodax'

mongo = PyMongo(app)

@app.route('/', methods=['GET', 'POST'])
def index():
    genere = mongo.db.genere.find({})
    if request.method == 'POST':
        print request.form, ' request for related gener books'
        back_data = request.form['gen']
        book = mongo.db.bookstore.find({'genere_name': request.form['gen']},{'_id': 1, 'name': 1, 'author': 1, 'publication': 1})
        book = list(book)
        return jsonify({'data': book})
    if 'username' in session:
        return render_template('home.html', genere=list(genere))
    return render_template('index.html')



@app.route('/login', methods=['POST'])
def login():
	if request.method=='POST':
		users = mongo.db.users
		login_user = users.find_one({'name': request.form['username']})

		if login_user:
			if hashlib.sha1(request.form['pass']).hexdigest() == login_user['password']:
				session['username'] = request.form['username']
				return redirect(url_for('index'))

	return 'Invalid username/password combination'



@app.route('/loginSeller', methods=['POST'])
def loginSeller():
	if request.method=='POST':
		sellerLogin = mongo.db.sellerLogin
		login_seller = sellerLogin.find_one({'name': request.form['username']})

		if login_seller:
			if request.form['pass'] == login_seller['password']:
				session['username'] = request.form['username']
				return redirect(url_for('sellerApprovalPage'))

	return 'Invalid username/password combination'



@app.route('/loginLogistics', methods=['POST'])
def loginLogistics():
	if request.method=='POST':
		logisticsLogin = mongo.db.logisticsLogin
		login_logistics = logisticsLogin.find_one({'name': request.form['username']})

		if login_logistics:
			if request.form['pass'] == login_logistics['password']:
				session['username'] = request.form['username']
				return redirect(url_for('logisticsHomePage'))

	return 'Invalid username/password combination'



@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        users = mongo.db.users
        existing_user = users.find_one({'name': request.form['username']})

        if existing_user is None:
        	hashpass = hashlib.sha1(request.form['pass']).hexdigest()
        	users.insert({'name': request.form['username'], 'password': hashpass})
        	session['username'] = request.form['username']
        	return render_template('index.html')

        return 'That username already exists!'

    return render_template('register.html')



@app.route('/bookPurchase', methods=['POST', 'GET'])
def bookPurchase():
    bookdetails = mongo.db.bookstore
    orderdetails = mongo.db.orderStatus
    if request.method == 'POST':
    	print request.form['select_book'],' bookname'
        book = mongo.db.bookstore.find({'_id': int(request.form['select_book'])},{'_id': 1, 'name': 1, 'author': 1, 'publication': 1, 'status':1})
        book = list(book)
        for m in book:
        	if m['status'] == 'available':
        		n = 'ordered'
        		orderStatus = mongo.db.orderStatus.insert({'name': m['name'], 'author': m['author'], 'publication': m['publication'], 'status':n, 'book_id': m['_id']})
        return render_template('bookPurchase.html', details=book)



@app.route('/sellerApprovalPage', methods=['POST', 'GET'])
def sellerApprovalPage():
	if request.method=='POST':
		print request.form
		book_id = request.form['book_id']
		oid2 = ObjectId(book_id)
		print(repr(oid2))
		book1 = mongo.db.bookstore.find({})
		book2 = mongo.db.orderStatus.find({})
		book2 = list(book2)
		book1 = list(book1)
		for a in book1:
			for b in book2:
				if a['_id'] == b['book_id']:
					book_data = mongo.db.orderStatus.update({'$and':[{'status':'ordered'},{'_id':oid2}]},{'$set':{'status':'approved'}})
					book_data = mongo.db.orderStatus.find({})
					return render_template('sellerApprovalPage.html', book_details=book_data)
		return render_template('sellerApprovalPage.html')

	else:
		book1 = mongo.db.bookstore.find({})
		book2 = mongo.db.orderStatus.find({})
		book2 = list(book2)
		book1 = list(book1)
		for a in book1:
			for b in book2:
				if a['_id'] == b['book_id']:
					book_data = mongo.db.orderStatus.find({})
					return render_template('sellerApprovalPage.html', book_details=book_data)
		return render_template('sellerApprovalPage.html')



@app.route('/logisticsTracking', methods=['POST', 'GET'])
def logisticsTracking():
	if request.method=='POST':
		form=request.form['_id']
		order_status=[]
		orderdetails = mongo.db.orderStatus.find({'book_id':float(form['book_id'])})
		orderdetails=list(orderdetails)
		if orderdetails:
			order_status=orderdetails[0]['status']
		else:
			order_status=[]
	return render_template('logisticsTracking.html', order_status=order_status)



@app.route('/sellerLogisticsTracking', methods=['POST', 'GET'])
def sellerLogisticsTracking():
	if request.method=='POST':
		form=request.form
		order_status=[]
		orderdetails = mongo.db.orderStatus.find({})
		#.skip(mongo.db.orderStatus.count()-1)
		orderdetails=list(orderdetails)
		if orderdetails:
			order_status=orderdetails[0]['status']
		else:
			order_status=[]
	return render_template('sellerLogisticsTracking.html', order_status=order_status)



@app.route('/logisticsHomePage', methods=['POST', 'GET'])
def logisticsHomePage():
	if request.method=='POST':
		print request.form
		book_id = request.form['book_id']
		oid2 = ObjectId(book_id)
		print(repr(oid2))
		book1 = mongo.db.bookstore.find({})
		book2 = mongo.db.orderStatus.find({})
		book2 = list(book2)
		book1 = list(book1)
		for a in book1:
			for b in book2:
				if a['_id'] == b['book_id']:
					book_data = mongo.db.orderStatus.update({'$and':[{'status':'approved'},{'_id':oid2}]},{'$set':{'status':'delivered'}})
					book_data = mongo.db.orderStatus.find({})
					return render_template('logisticsHomePage.html', book_details=book_data)
		return render_template('logisticsHomePage.html')

	else:
		book1 = mongo.db.bookstore.find({})
		book2 = mongo.db.orderStatus.find({})
		book2 = list(book2)
		book1 = list(book1)
		for a in book1:
			for b in book2:
				if a['_id'] == b['book_id']:
					book_data = mongo.db.orderStatus.find({})
					return render_template('logisticsHomePage.html', book_details=book_data)
		return render_template('logisticsHomePage.html')


@app.route('/userTrackingPage', methods=['POST', 'GET'])
def userTrackingPage():
	if request.method=='POST':
		print request.form, "form data"
		book_id = request.form['book_id']
		oid2 = ObjectId(book_id)
		print(repr(oid2)), "oid2 data"
		book1 = mongo.db.bookstore.find({})
		book2 = mongo.db.orderStatus.find({})
		book2 = list(book2)
		book1 = list(book1)
		for a in book1:
			for b in book2:
				if a['_id'] == b['book_id']:
					book_data = mongo.db.orderStatus.update({'$and':[{'$or':[{'status':'ordered'},{'status':'approved'},{'status':'delivered'}]},{'_id':oid2}]},{'$set':{'status':'returned'}})
					book_data = mongo.db.orderStatus.find({})
					return render_template('userTrackingPage.html', book_details=book_data)
		return render_template('userTrackingPage.html')
		
	else:
		book1 = mongo.db.bookstore.find({})
		book2 = mongo.db.orderStatus.find({})
		book2 = list(book2)
		book1 = list(book1)
		for a in book1:
			for b in book2:
				if a['_id'] == b['book_id']:
					book_data = mongo.db.orderStatus.find({})
					return render_template('userTrackingPage.html', book_details=book_data)
		return render_template('userTrackingPage.html')


@app.route('/sellerLogin', methods=['POST', 'GET'])
def sellerLogin():
	if request.method=='POST':
		return render_template('sellerLogin.html')
	return render_template('sellerLogin.html')



@app.route('/logisticsLogin', methods=['POST', 'GET'])
def logisticsLogin():
	if request.method=='POST':
		return render_template('logisticsLogin.html')
	return render_template('logisticsLogin.html')


@app.route('/userTrackingPage1', methods=['POST', 'GET'])
def userTrackingPage1():
	if request.method=='POST':
		return redirect(url_for('userTrackingPage'))
	return render_template('userTrackingPage.html')



@app.route('/logout', methods=['POST', 'GET'])
def logout():
	session.pop('username', None)
	return redirect(url_for('index'))


if __name__ == '__main__':
	app.run(debug=True)
    #app.run(host='0.0.0.0', port=8080)

