from flask import Flask, render_template, url_for, session, request, redirect
from flask_pymongo import PyMongo
import os
import hashlib


app = Flask(__name__)
app.secret_key = os.urandom(24)

app.config['MONGO_DBNAME'] = 'nprodax'
app.config['MONGO_URI'] = 'mongodb://shadaab:shadaab@localhost:27017/nprodax'

mongo = PyMongo(app)


@app.route('/')
def index():
    if 'username' in session:
        return render_template('practice.html')

    return render_template('index.html')


@app.route('/login', methods = ['POST'])
def login():
	users = mongo.db.users
	login_user = users.find_one({'name' : request.form['username']})

	if login_user:
		if request.form['pass'] == login_user['password']:
			session['username'] = request.form['username']
			return redirect(url_for('index'))

	return 'Invalid username/password combination'

@app.route('/register', methods = ['POST', 'GET'])
def register():
	if request.method == 'POST':
		users = mongo.db.users
		existing_user = users.find_one({'name' : request.form['username']})

		if existing_user is None:
			newpass = request.form['pass']
			users.insert({'name' : request.form['username'],'password' : newpass})
			session['username'] = request.form['username']
			return render_template('index.html')

		return 'That username already exists!'

	return render_template('register.html')

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('index'))

@app.route('/', methods=['GET', 'POST'])
def get_all_docs():
	genere = mongo.db.genere
	print genere
	genere_list = [p['name'] for p in genere.find({"name": {"$type": "string"}})]
	return render_template("practice.html",
                           server_list=genere_list)

if __name__ == '__main__':
    app.run(debug=True)