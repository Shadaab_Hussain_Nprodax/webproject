try : 
    a = 10/0
    print a

except ArithmeticError : 
    print "This statement is raising exception"

else:
    print "Welcome"


finally :
    print "Inside finally block"
