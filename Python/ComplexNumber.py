class ComplexNumber:
    def __init__(self,r=0,i=0):
	   self.real = r
	   self.imag = i
    def getData(self):
        print("{0}+{1}j".format(self.real,self.imag))
		
c1 = ComplexNumber(5,6)
c1.getData()

c2 = ComplexNumber(11)
c2.attr = 12
(c2.real,c2.imag,c2.attr)
