def calc_average(a_list):
    avg = float(sum(a_list) / len(a_list))
    return avg

class Company(object):

    all_employees = []
    n = input("Enter number of employees you want : ")
    print "Enter employees details : "

    for i in range(0, n):
        emp_name = raw_input('Enter the name of employee : ')
        emp_dept = input('Enter the dept no : ')

        all_employees.append({
            'Name': emp_name,
            'Deptno': emp_dept
        })
    print all_employees

    employeeId = []
    employeeName = []

    for employee in all_employees:
        for key,value in employee.items():
            if type(value) == int:
                employeeId.append(value)
            else:
                employeeName.append(value)

    print employeeId
    print employeeName

    all_managers = []
    print "Enter Manager details : "

    for j in range(0,n):
        mana_name = raw_input("Enter name of manager : ")
        dept_no = input("Enter manager dept no : ")

        all_managers.append({
            'Name': mana_name,
            'Deptno': dept_no
        })
    print all_managers

    managerId = []
    managerName = []
    for employee in all_managers:
        for key, value in employee.items():
            if type(value) == int:
                managerId.append(value)
            else:
                managerName.append(value)
    print managerId
    print managerName

    add_benefits = []

    for i in range(0,n):
            print "Enter quarterly bonus for employee : "
            q1 = input("Enter quarter 1 bonus : ")
            q2 = input("Enter quarter 2 bonus : ")
            q3 = input("Enter quarter 3 bonus : ")
            add_benefits.append({
                "Q1" : q1,
                "Q2" : q2,
                "Q3" : q3
            })

    employees = []

    for bonus in add_benefits:
        for key,value in bonus.items():
            employees.append(value)
        print employees

    employee1 = employees[0:3]
    employee2 = employees[3:7]
    employee3 = employees[7:]

    while True:
        b = raw_input("Enter yes/no to get employee average : ")

        if b == "no":
            break

        else:
            a = raw_input("Enter employee name for whom you want to calculate the average : ")

            if a in employeeName:
                if a == employeeName[0]:
                    print "Average value for employee1 is : ", calc_average(employee1)
                elif a == employeeName[1]:
                    print "Average value for employee2 is : ", calc_average(employee2)
                elif a == employeeName[2]:
                    print "Average value for employee3 is : ", calc_average(employee3)
                else:
                    print "Please specify employee within department only"
            else:
                print "Please specify employee within department only"

    print
    print "Total average of all the employees are : ", calc_average(employees)
    print

    while True:
        d = raw_input("Enter yes/no to get employee manager name : ")

        if d == "no":
            break

        else:
            c = input("Enter employeeId to get manager : ")

            if c in managerId and c in employeeId:
                if c == managerId[0]:
                    print managerName[0]
                elif c == managerId[1]:
                    print managerName[1]
                elif c == managerId[2]:
                    print managerName[2]
                else:
                    print managerName[3]
            else:
                print "Please specify employeeId which is same as managerId"