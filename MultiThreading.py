import threading
import math
import time


def calc_squareRoot(numbers):
    for n in numbers:
        time.sleep(2)
        print (threading.currentThread().getName()+" : Square Root : " + str(math.sqrt(n)))

def calc_square(numbers):
    time.sleep(3)
    for n in numbers:
        print (threading.currentThread().getName()+" : Square : " + str((n*n)))

if __name__ == '__main__':
    arr = [2,3,4]
    p1 = threading.Thread(target=calc_squareRoot, args=(arr,))
    p2 = threading.Thread(target=calc_square, args=(arr,))

    p1.start()
    p2.start()

    p1.join()
    p2.join()
    print
    print("Done!")
