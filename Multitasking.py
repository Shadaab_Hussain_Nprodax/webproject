import multiprocessing
import time


def calc_square(numbers):
    for n in numbers:
        time.sleep(2)
        print ("Square " + str(n*n))

def calc_Cube(numbers):
    time.sleep(3)
    for n in numbers:
        print ("Cube " + str(n*n*n))

if __name__ == '__main__':
    arr = [2,3,4]
    p1 = multiprocessing.Process(target=calc_square, args=(arr,))
    p2 = multiprocessing.Process(target=calc_Cube, args=(arr,))

    p1.start()
    p2.start()

    p1.join()
    p2.join()
    print("Done!")


