import threading
import time

def employee(emp):

      print "Employee : %s started his work\n" % emp
      time.sleep(2)
      print
      print "Employee : %s finished his work\n" % emp
      return

def main():

    threads = []
    a_list = ["Engineer","Salesman","Manager"]
    for i in a_list:
        t = threading.Thread(target=employee, args=(i, ))
        threads.append(t)
        t.start()

    for t in threads:
        t.join()
    print "Done!"
main()