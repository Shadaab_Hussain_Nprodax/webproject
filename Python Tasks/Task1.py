class Employee:

    def __init__(self, firstName, lastName, email):
        self.firstName = firstName
        self.lastName = lastName
        self.email = email

    def displayDetails(self):
        print "First Name : ", self.firstName, "\nLast Name : ", self.lastName, "\nEmail : ", self.email


emp1 = Employee("Shadaab", "Hussain", "shadaab.hussain.npordax@gmail.com")
emp1.displayDetails()