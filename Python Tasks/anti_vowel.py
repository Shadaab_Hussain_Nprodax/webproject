def anti_vowel(text):
    t = ""
    for c in text:
        if c in "aeiouAEIOU":
            c = ""
        else:
            c = c
        t = t + c
    return t

print anti_vowel("Hey hello how are you!")
