a_list = [1, 2, [2, 3], 10, [1, 2, 5, 1]]

def addition_list(b_list):
    sum_list = 0
    for element in b_list:
        if type(element) == list:
            sum_list = sum_list + addition_list(element)
        else:
            sum_list = sum_list + element
    return sum_list

print addition_list(a_list)
