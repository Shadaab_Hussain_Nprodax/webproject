def nested_sum(L):
    total = 0  # don't use `sum` as a variable name
    for i in L:
        if isinstance(i, list):  # checks if `i` is a list
            total += nested_sum(i)
        else:
            total += i
    return total
    a_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, [2, 3]]


print nested_sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, [2, 3]])
